#!/bin/sh

LASH_DIRECTORY=$HOME/Projects/dev/src/tapas/external/lash-v0.92
export LASH_DIRECTORY

CFLAGS="-Wall -ggdb"

loc=$(dirname $0)
case "$loc" in
    "/"*)
	CONFIGURE=${loc}/configure
	;;
    *)
	CONFIGURE=${PWD}/${loc}/configure
esac


TOLOWER="tr '[A-Z]' '[a-z]'"
cpu=`(uname -p 2>/dev/null || uname -m 2>/dev/null || echo unknown) | \
        ${TOLOWER}`
os=`uname -s | ${TOLOWER}`

case "$os" in
    darwin*)
	LDFLAGS=-L/opt/local/lib
#	CFLAGS="${CFLAGS} -no_pie"
	;;
esac

case "$cpu" in
    unknown|*" "*)
        cpu=`uname -m | ${TOLOWER}` ;;
esac

BUILDDIR=${os}-${cpu}

[ -d ${BUILDDIR} ] || mkdir ${BUILDDIR}

cd ${BUILDDIR}

${CONFIGURE} CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" --prefix=$INST --disable-doxygen-doc "$@" --with-plugin-tests

#--with-gmp-lib=/opt/local/lib 
#CFLAGS="$CFLAGS" ${CONFIGURE} --prefix=$INST "$@" --with-gmp-lib=/opt/local/lib 

#exec make 
