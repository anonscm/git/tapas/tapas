dnl
dnl Enable or not ccl-memory debugging.
dnl
AC_DEFINE([CCL_DEBUG_MEMORY],[0],[Description])
AC_DEFUN([AX_CCL_DEBUG_MEMORY],
[
AC_ARG_ENABLE(ccl_debug_memory,
 AS_HELP_STRING([--enable-debug-memory],[enable CCL memory debugging (no)]),
 [enable_ccl_debug_memory=yes],)

if test "x$enable_ccl_debug_memory" = xyes; then
  AC_DEFINE([CCL_DEBUG_MEMORY],[1])
fi
])

