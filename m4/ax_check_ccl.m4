dnl
dnl Looks for the CCL library
dnl

AC_DEFUN([AX_CHECK_CCL],
[
  AC_CHECK_LIB(ccl,ccl_init,[],AC_MSG_ERROR([[can't find ccl library]]))
])

