/*
 * distiller.c -- add a comment about this file
 * 
 * This file is a part of the Alambic Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-init.h>
#include <ccl/ccl-assert.h>
#include <genepi/genepi.h>
#include <genepi/genepi-util.h>
#include <armoise/armoise.h>
#include <armoise/armoise-input.h>
#include <armoise/armoise-interp.h>
#include <alambic/alambic.h>

struct distiller_option
{
  char shortname;
  char *name;
  enum { BOOL_OPT, INT_OPT, STRING_OPT } kind;
  void *p_value;
  void *defvalue;
  char *helpmsg;
};

			/* --------------- */

#define HELP_MSG_TAB 22
#define NB_ENTRIES_IN_CACHE 1000

			/* --------------- */

static void
s_init (void);

static void
s_terminate (void);

static void
s_usage (ccl_log_type log, const char *cmd);

static void
s_parse_args (int argc, char **argv);

static genepi_solver *
s_create_solver (const char *pluginname);

static int
s_compile_stream (FILE *input, const char *filename, genepi_solver *solver,
		  int *p_emptyness);

static int
s_compile_file (const char *filename, genepi_solver *solver,
		int *p_emptyness);

static void
s_display_solvers (ccl_log_type log, int quiet);

			/* --------------- */

static const char *genepi_plugin_name_opt = NULL;
static const char *dump_filename_opt = NULL;
static ccl_list *filenames;
static int normal_forms_opt;
static int all_solutions_opt;
static int one_solution_opt;
static int data_structures_opt;
static int sizes_of_data_structures_opt;
static int help_opt;
static int debug_level_opt;
static int types_opt;
static int domains_opt;
static int emptyness_opt;
static int solver_type_opt;
static int quiet_opt;
static int genepi_times_opt;
static int monoid_opt;
static int solvers_opt;

static struct distiller_option OPTIONS[] = 
  {    
    { 'n', "normal-forms", BOOL_OPT, &normal_forms_opt, (void *) 0,
      "display normalized formulas" },
    { 's', "solutions", BOOL_OPT, &all_solutions_opt, (void *) 0, 
      "display all solutions" },
    { 'd', "data-structures", BOOL_OPT, &data_structures_opt, (void *) 0,
      "display the data structure used to encode the set of\n\t\t\tsolutions" },
    { 'z', "sizes", BOOL_OPT, &sizes_of_data_structures_opt, (void *) 0,
      "display the size of the internal data structure used to encode the set "
      "of\n\t\t\tsolutions" },
    { 'k', "types", BOOL_OPT, &types_opt, (void *) 0,
      "display the type of the formulas" },
    { 'v', "domains", BOOL_OPT, &domains_opt, (void *) 0,
      "display the domains of variables" },
    { 0, "debug", INT_OPT, &debug_level_opt, (void *) -1,
      "set the CCL debug level" },
    { 'e', "emptyness", BOOL_OPT, &emptyness_opt, (void *) 0,
      "check the emptyness of computed sets" },
    { 'q', "quiet", BOOL_OPT, &quiet_opt, (void *) 0,
      "display no message when checking for the emptyness of sets" },
    { 'x', "solver-type", BOOL_OPT, &solver_type_opt, (void *) 0,
      "returns the minimal solver type required to solve\n\t\t\tpredicates" },
    { 0, "one-solution", BOOL_OPT, &one_solution_opt, (void *) 0,
      "returns one element of the computed set (if not empty)" },
    { 0, "plugin", STRING_OPT, &genepi_plugin_name_opt, "prestaf",
      "specify which GENEPI plugin to use" },
    { 0, "dump", STRING_OPT, &dump_filename_opt, NULL,
      "specify a file containing a serialized genepi set." },
    { 't', NULL, BOOL_OPT, &genepi_times_opt, (void *) 0,
      "display execution times for each genepi operation." },
    { 'm', "monoid", BOOL_OPT, &monoid_opt, (void *) 0, 
      "computes the monoid" },
    { 0, "solvers", BOOL_OPT, &solvers_opt, (void *) 0, 
      "display solvers that are currently installed." },
    { 'h', "help", BOOL_OPT, &help_opt, (void *) 0, "this help message" },
    { 0, NULL }
  };


			/* --------------- */

int 
main (int argc, char **argv)
{
  int exitcode = 0;

  s_init ();
  filenames = ccl_list_create ();

  ccl_try (exception)
    {
      s_parse_args (argc, argv);

      if (help_opt)
	s_usage (CCL_LOG_DISPLAY, argv[0]);
      else if (solvers_opt)
	{
	  genepi_loader_load_default_plugins ();
	  s_display_solvers (CCL_LOG_DISPLAY, quiet_opt);
	}
      else
	{
	  int ok = 1;
	  int emptyness = 0;
	  genepi_solver *solver = NULL;

	  
	  if (debug_level_opt >= 0)
	    ccl_debug_max_level (debug_level_opt);

	  if (all_solutions_opt || one_solution_opt || data_structures_opt ||
	      sizes_of_data_structures_opt || emptyness_opt || 
	      (dump_filename_opt != NULL))
	    {
	      if (genepi_plugin_name_opt == NULL)
		{
		  ccl_error ("no GENEPI plugin is specified\n");
		  exitcode = 1;
		}
	      else
		{
		  solver = s_create_solver (genepi_plugin_name_opt);
		  ok = (solver != NULL);
		  if (ok)
		    {
		      const char *encoding = genepi_solver_get_name (solver);
		      ccl_assert (encoding != NULL);
	      
		      ccl_debug ("current set encoding is : %s\n", encoding);
		    }
		}
	    }

	  if (ok)
	    {
	      if (dump_filename_opt != NULL)		
		{
		  FILE *dump = fopen (dump_filename_opt, "r");
		  if (dump == NULL)
		    {
		      ccl_error ("can't open dump file '%s'\n", 
				 dump_filename_opt);
		      exitcode = 1;
		    }
		  else
		    {
		      genepi_set *set = NULL;
		      int err = genepi_set_read (solver, dump, &set);
		      fclose (dump);
		      
		      if (err < 0)
			{
			  ccl_error ("io error while read dump file '%s'\n", 
				     dump_filename_opt);
			  exitcode = 1;
			}
		      else
			genepi_set_display_all_solutions (solver, set, stdout, 
							  NULL);
		      genepi_set_del_reference (solver, set);
		    }
		}
	      else if (ccl_list_is_empty (filenames))
		ok = s_compile_stream (stdin, "<stdin>", solver, &emptyness);
	      else 
		{
		  ccl_pair *p;
		  emptyness = 1;
		  for (p = FIRST (filenames); p; p = CDR (p))
		    {
		      int le = 0;
		      char *filename = (char *) CAR(p);
		      ok = s_compile_file (filename, solver, &le);
		      emptyness = emptyness && le;
		    }
		}
	    }

	  ccl_zdelete (genepi_solver_delete, solver);

	  if (ok)
	    {
	      if (emptyness_opt)
		{
		  if (!emptyness)
		    {
		      if (!quiet_opt)
			ccl_display ("not ");

		      exitcode = 1;
		    }
		  if (!quiet_opt)
		    ccl_display ("empty set\n");
		}
	    }
	  else
	    {
	      exitcode = 1;
	    }
	}
    }
  ccl_catch
    {
      if (ccl_exception_is_raised (runtime_exception))
	ccl_rethrow ();
      else if (! ccl_exception_is_raised (armoise_interp_exception))
	{
	  ccl_error ("%s\n", ccl_exception_current_message);
	  s_usage (CCL_LOG_ERROR, argv[0]);
	  exitcode = 1;
	}
    }
  ccl_end_try;

  ccl_list_delete (filenames);
  s_terminate ();
  
  return exitcode;
}

			/* --------------- */

static void
s_usage (ccl_log_type log, const char *cmd)
{
  struct distiller_option *opt;

  ccl_log (log, 
	   "USAGE:%s [options] [armoise input filenames]\n"
	   "with options:\n", cmd);
  
  for (opt = &OPTIONS[0]; opt->shortname != 0 || opt->name != NULL; opt++)
    {
      int tab = 0;
      if (opt->shortname != 0)
	{
	  ccl_log (log, "-%c", opt->shortname);
	  tab += 2;
	  if (opt->kind == INT_OPT) 
	    { 
	      ccl_log (log, " <n>"); 
	      tab += 4; 
	    }
	  else if (opt->kind == STRING_OPT) 
	    { 
	      ccl_log (log, " <string>"); 
	      tab += 9; 
	    }
	  if (opt->name != NULL)
	    {
	      ccl_log (log, ", ");
	      tab += 2;
	    }
	}

      if (opt->name != NULL)
	{
	  tab += strlen (opt->name) + 2;
	  ccl_log (log, "--%s", opt->name);
	  if (opt->kind == INT_OPT) 
	    { 
	      ccl_log (log, "=<n>"); 
	      tab += 4; 
	    }
	  else if (opt->kind == STRING_OPT) 
	    { 
	      ccl_log (log, "=<string>"); 
	      tab += 9; 
	    }
	}
      for (; tab < HELP_MSG_TAB; tab++)
	ccl_log (log, " ");
      ccl_log (log, "\t%s", opt->helpmsg);
      if (opt->shortname != 'h')
	{
	  ccl_log (log, " (default=");
	  if (opt->kind == INT_OPT) 
	    ccl_log (log, "%d", (intptr_t) opt->defvalue);
	  else if (opt->kind == STRING_OPT) 
	    ccl_log (log, "%s", (char *) opt->defvalue);
	  else 
	    ccl_log (log, "%s", opt->defvalue ? "true" : "false");
	  ccl_log (log, ")");
	}
      ccl_log (log, ".\n");
    }
  ccl_log (log, "\n");
  genepi_loader_load_default_plugins ();
  s_display_solvers (log, 0);
}

			/* --------------- */

static void
s_listener (ccl_log_type type, const char *msg, void *data)
{
  if (type == CCL_LOG_DISPLAY)
    fprintf (stdout, "%s", msg);
  else if (type != CCL_LOG_DEBUG || debug_level_opt >= 0)
    fprintf (stderr, "%s", msg);
}

			/* --------------- */

static void
s_init (void)
{
  ccl_init ();
  ccl_log_add_listener (s_listener, NULL);

  armoise_init ();
  genepi_loader_init ();
}

			/* --------------- */

static void
s_terminate (void)
{
  genepi_loader_terminate ();
  armoise_terminate ();
  ccl_terminate ();
}

			/* --------------- */

static void
s_parse_args (int argc, char **argv)
{
  int i = 0;
  struct distiller_option *opt;

  for (opt = &OPTIONS[0]; opt->shortname != 0 || opt->name != NULL; opt++)
    {
      if (opt->kind == BOOL_OPT || opt->kind == INT_OPT)
	{
	  int *opt_addr = opt->p_value;
	  *opt_addr = (intptr_t) opt->defvalue;
	}
      else 
	{
	  char **opt_addr  = opt->p_value;
	  *opt_addr = opt->defvalue;
	}
    }

  for (i = 1; i < argc; i++)
    {
      if (argv[i][0] == '-')
	{
	  char *s;

	  if (argv[i][1] == '-')
	    {
	      int len;
	      char *optname = argv[i] + 2;

	      for (opt = &OPTIONS[0]; opt->shortname != 0 || opt->name != NULL;
		   opt++)
		{
		  if (opt->name != NULL)
		    {
		      len = strlen (opt->name);
		      if (strncmp (optname, opt->name,len) == 0)
			break;
		    }
		}

	      if (opt->shortname == 0 && opt->name == NULL)
		ccl_throw (exception, "unknown option\n");

	      if (opt->kind == INT_OPT || opt->kind == STRING_OPT)		
		{
		  if (optname[len] != '=' || optname[len + 1] == '\0')
		    {
		      ccl_error ("missing argument to option --%s.\n", 
				 opt->name);
		      ccl_throw_no_msg (exception);
		    }
		  optname += len + 1;
		}

	      if (opt->kind == BOOL_OPT)
		{
		  int *opt_addr = opt->p_value;
		  *opt_addr = ! (intptr_t) opt->defvalue;
		} 
	      else if (opt->kind == INT_OPT)
		{
		  int *opt_addr  = opt->p_value;
		  *opt_addr = atoi (optname);
		}
	      else
		{
		  char **opt_addr  = opt->p_value;
		  *opt_addr = optname;
		}
	    }
	  else
	    {
	      for (s = argv[i] + 1; *s; s++)
		{
		  struct distiller_option *opt;
		  
		  for (opt = &OPTIONS[0]; 
		       opt->shortname != 0 || opt->name != NULL; 
		       opt++)
		    {
		      if (*s == opt->shortname)
			break;
		    }

		  if (opt->shortname == 0 && opt->name == NULL)
		    ccl_throw (exception, "unknown option\n");

		  if ((opt->kind == INT_OPT || opt->kind == STRING_OPT) && 
		      i == argc - 1)
		    {
		      ccl_error ("missing argument to option -%c.\n",
				 opt->shortname);
		      ccl_throw_no_msg (exception);
		    }

		  if (opt->kind == BOOL_OPT)
		    {
		      int *opt_addr = opt->p_value;
		      *opt_addr = ! (intptr_t) opt->defvalue;
		    }
		  else if (opt->kind == INT_OPT)
		    {
		      int *opt_addr  = opt->p_value;
		      *opt_addr = atoi (argv[i+1]);
		      i++;
		    }
		  else
		    {
		      char **opt_addr  = opt->p_value;
		      *opt_addr = argv[i+1];
		      i++;
		    }
		}
	    }
	}
      else
	{
	  ccl_list_add (filenames, argv[i]);
	}
    }
}

			/* --------------- */

struct subtype_position 
{
  int pos;
  const struct subtype_position *prev;
};

			/* --------------- */

static void
s_generate_varnames_rec (armoise_variable *var, const armoise_type *type, 
			 char **varnames, int *pi, 
			 const struct subtype_position *ppos)
{
  if (armoise_type_is_scalar (type))
    {
      int i = *pi;

      varnames[i] = armoise_variable_get_name (var);
      varnames[i] = ccl_string_dup (varnames[i]);
      for(; ppos != NULL; ppos = ppos->prev)
	ccl_string_format_append (&(varnames[i]), "_%d", ppos->pos);
      *pi = i+1;
    }
  else
    {
      struct subtype_position pos;
      const int w = armoise_type_get_width (type);

      pos.prev = ppos;
      for (pos.pos = 0; pos.pos < w; pos.pos++)
	{
	  const armoise_type *st = armoise_type_get_subtype (type, pos.pos);
	  s_generate_varnames_rec (var, st, varnames, pi, &pos);
	}
    }
}

			/* --------------- */

static void
s_generate_varnames (armoise_variable *var, char **varnames, int *pi)
{
  armoise_type *type = armoise_variable_get_type (var);
  s_generate_varnames_rec (var, type, varnames, pi, NULL);
  armoise_type_del_reference (type);
}

			/* --------------- */

static int
s_gcd (int a, int b)
{
  if (a == 0)
    return b;

  while (b != 0)
    {
      if (a > b) 
	a = a - b;
      else b = b - a;
    }

  return a;
}

			/* --------------- */

static void
s_normalize (int *num, int *den)
{
  int gcd = s_gcd (*num, *den);

  *num /= gcd;
  *den /= gcd;
}

			/* --------------- */

static int
s_compile_predicate_tuple (ccl_tree *t, genepi_solver *solver, 
			   alambic_error *perror)
{
  int result = 1;

  if (ccl_tree_is_leaf (t))
    {
      armoise_predicate *P = t->object;
      armoise_normalized_predicate *nP = (armoise_normalized_predicate *) P;
    
      if (types_opt)
	{
	  armoise_type *type = armoise_predicate_get_type (P);
	  armoise_type_log (CCL_LOG_DISPLAY, type);
	  ccl_display ("\n");
	  armoise_type_del_reference (type);
	}

      if (domains_opt)
	{
	  armoise_domain *dom = armoise_predicate_get_domain (P);
	  armoise_domain_log (CCL_LOG_DISPLAY, dom);
	  ccl_display ("\n");
	  armoise_domain_del_reference (dom);
	}

      if (solver_type_opt)
	{
	  const char *solvertype = "R";
	  armoise_domain_attr attr = armoise_predicate_get_largest_domain (P);

	  if (attr == ARMOISE_DOMAIN_ATTR_FOR_NATURALS)
	    solvertype = "N";
	  else if (attr == ARMOISE_DOMAIN_ATTR_FOR_INTEGERS)
	    solvertype = "Z";
	  else if (attr == ARMOISE_DOMAIN_ATTR_FOR_POSITIVES)
	    solvertype = "P";
 	  ccl_display ("%s\n", solvertype); 
	}
      
      if (all_solutions_opt || one_solution_opt || data_structures_opt ||
	  sizes_of_data_structures_opt || emptyness_opt)
	{
	  genepi_set *S = alambic_compute_predicate (solver, nP, perror);

	  ccl_assert (ccl_imply (S == NULL, *perror != ALAMBIC_NO_ERROR));

	  if (S == NULL)
	    return 1;

	  result = (emptyness_opt && genepi_set_is_empty (solver, S));

	  if (monoid_opt) 
	    {
	      genepi_set *M = genepi_set_monoid (solver, S);
	      genepi_set_del_reference (solver, S);
	      S = M;
            }
	  
	  if (all_solutions_opt||one_solution_opt)
	    {
	      int w = genepi_set_get_width (solver, S);
	      char **varnames = ccl_new_array (char *, w);
	  
	      if (armoise_predicate_get_kind (P) == ARMOISE_P_SET)
		{
		  int i = 0;
		  ccl_pair *p;
		  ccl_list *variables = ccl_list_create ();
		  const armoise_term *proto = 
		    armoise_predicate_get_set_proto (P, NULL);
		  armoise_term_get_variables (proto, variables);

		  for (p = FIRST (variables); p; p = CDR (p))
		    {
		      armoise_variable *V = (armoise_variable *) CAR (p);
		      s_generate_varnames (V, varnames, &i);
		    }
		  ccl_list_delete (variables);
		}
	      else
		{
		  int i;
		  for (i = 0; i < w; i++)
		    {
		      varnames[i] = ccl_new_array (char, 100);
		      sprintf (varnames[i], "x%d", i);
		    }
		}
	      
	      if (all_solutions_opt)
		genepi_set_display_all_solutions (solver, S, stdout, 
						  (const char **)varnames);
	      if (one_solution_opt)
		{
		  int *num = ccl_new_array (int, w);
		  int den = genepi_set_get_one_solution (solver, S, num, w);
		  
		  if (den > 0)
		    {
		      int i;
		      for (i = 0; i < w; i++)
			{
			  int n = num[i];
			  int d = den;
			  s_normalize (&n, &d);

			  if (d == 1 || n == 0)
			    ccl_display ("%s=%d ", varnames[i], n);
			  else 
			    ccl_display ("%s=%d/%d ", varnames[i], n, d);
			}
		      ccl_display ("\n");
		    }
		  ccl_delete (num);
		}
	  
	      while (w--)
		ccl_delete (varnames[w]);
	      ccl_delete (varnames);
	    }

	  if (data_structures_opt)
	    genepi_set_display_data_structure (solver, S, stdout);
	  if (sizes_of_data_structures_opt)
	    {
	      ccl_display("data structure size = %d\n",
			  genepi_set_get_data_structure_size (solver, S));
	    }
	  genepi_set_del_reference (solver, S);      
	}
    }
  else
    {
      ccl_tree *c;

      for (c = t->childs; c && *perror == ALAMBIC_NO_ERROR; c = c->next)
	{
	  int lr = s_compile_predicate_tuple (c, solver, perror);
	  result = result && lr;
	}
    }

  return result;
}

			/* --------------- */

static int
s_compile_tree (const char *filename, armoise_tree *T, genepi_solver *solver,
		int *p_emptyness)
{   
  int result = 1;
  armoise_context *ctx = armoise_context_create_for_predicates (NULL);
  armoise_context *nctx = armoise_context_create_for_predicates (NULL);

  ccl_try (armoise_interp_exception)
    {
      while (T != NULL)
	{
	  alambic_error error = ALAMBIC_NO_ERROR;

	  if (T->node_type == ARMOISE_TREE_PREDICATE)
	    {
	      ccl_tree *t = armoise_interp_predicate (ctx, T);
	      ccl_tree *nt = alambic_normalize_predicate_tree (nctx, t, &error);

	      if (error == ALAMBIC_NO_ERROR)
		{
		  if (normal_forms_opt)
		    {
		      armoise_predicate_log_tree (CCL_LOG_DISPLAY, nt);
		      ccl_display ("\n");
		    }

		  *p_emptyness = s_compile_predicate_tuple (nt, solver, &error);
		}
	      ccl_zdelete (ccl_tree_del_reference, nt);
	      ccl_zdelete (ccl_tree_del_reference, t);
	    }
	  else if (T->node_type == ARMOISE_TREE_DEFINITION)
	    {
	      ccl_pair *p;
	      ccl_list *ids = ccl_list_create ();

	      ccl_try (armoise_interp_exception)
	        {
		  armoise_interp_definition (ctx, T, ids);
		} 
	      ccl_catch
		{
		  ccl_list_delete (ids);
		  ccl_rethrow ();
		}
	      ccl_end_try;

	      for (p = FIRST (ids); p && error == ALAMBIC_NO_ERROR; 
		   p = CDR (p))
		{
		  ccl_ustring id = CAR (p);
		  ccl_tree *t = armoise_context_get_predicate_tree (ctx, id);
		  ccl_tree *nt = 
		    alambic_normalize_predicate_tree (nctx, t, &error);

		  armoise_context_add_predicate_tree (nctx, id, nt);
		  ccl_tree_del_reference (nt);
		  ccl_tree_del_reference (t);
		}
	      ccl_list_delete (ids);
	    }
	  else 
	    ccl_throw (internal_error, "invalid node type");

	  switch (error)
	    {
	    case ALAMBIC_NO_ERROR:
	      break;
	    case ALAMBIC_REAL_NOT_SUPPORTED:
	      ccl_error ("reals are not supported.\n");
	      break;
	    case ALAMBIC_POSI_NOT_SUPPORTED:
	      ccl_error ("positive reals are not supported.\n");
	      break;
	    case ALAMBIC_INT_NOT_SUPPORTED:
	      ccl_error ("integers are not supported.\n");
	      break;
	    case ALAMBIC_INVALID_MUL_DIMENSIONS:
	    case ALAMBIC_INVALID_DIV_DIMENSIONS:
	    case ALAMBIC_INVALID_MOD_DIMENSIONS:
	      ccl_error ("incompatible dimensions in predicate "
			 "multiplication.\n");
	      break;
	    case ALAMBIC_INVALID_MUL_CARDINALITY:
	    case ALAMBIC_INVALID_DIV_CARDINALITY:
	    case ALAMBIC_INVALID_MOD_CARDINALITY:
	      ccl_error ("multiplication of two infinite sets.\n");
	      break;
	    case ALAMBIC_INVALID_MUL_FACTOR:
	      ccl_error ("use of non-linear terms\n");
	      break;
	    }
	  if (error != ALAMBIC_NO_ERROR)
	    result = 0;

	  T = T->next;
	}
    }
  ccl_catch
    {
      result = 0;
    }
  ccl_end_try;

  armoise_context_del_reference (ctx);
  armoise_context_del_reference (nctx);

  return result;
}

			/* --------------- */

static int
s_compile_stream (FILE *input, const char *filename, genepi_solver *solver,
		  int *p_emptyness)
{
  int result = 0;
  armoise_tree *T = armoise_read_stream (input, filename);

  if (T != NULL)
    {
      result = s_compile_tree (filename, T, solver, p_emptyness);
      ccl_parse_tree_delete_tree (T);
    }

  return result;
}

			/* --------------- */

static int
s_compile_file (const char *filename, genepi_solver *solver, int *p_emptyness)
{
  int result = 0;
  FILE *f = fopen (filename, "r");

  if (f == NULL)
    ccl_error ("can't open file '%s'.\n", filename);
  else
    {
      result = s_compile_stream (f, filename, solver, p_emptyness);
      fclose (f);
    }

  return result;
}

			/* --------------- */

static int
s_file_exists (const char *filename)
{
  FILE *f = fopen (filename, "r");

  if (f != NULL)
    fclose(f);

  return (f != NULL);
}

			/* --------------- */

static genepi_solver *
s_create_solver (const char *pluginname)
{
  genepi_solver *result = NULL;
  genepi_plugin *plugin;
      
  if (!s_file_exists (pluginname))
    genepi_loader_load_default_plugins ();

  plugin = genepi_loader_get_plugin (pluginname);

  if (plugin == NULL)
    ccl_error ("no plugin found.\n");
  else
    {
      int flags = 0;
      const char *encoding;

      if (genepi_times_opt)
	flags |= GENEPI_FLAG_SHOW_TIMERS;

      result = genepi_solver_create (plugin, flags, 0, 
				     NB_ENTRIES_IN_CACHE);
      encoding = genepi_solver_get_name (result);
      ccl_assert (encoding != NULL);
      genepi_plugin_del_reference (plugin);
    }

  return result;
}

			/* --------------- */

static void
s_display_solvers (ccl_log_type log, int quiet)
{
  int i;
  int nb_plugins;
  char **plugins_names;

  plugins_names = genepi_loader_get_plugins (&nb_plugins);
  
  if (!quiet)
    {
      const char *msg;

      if (nb_plugins == 0)
	msg = "no GENEPI plugin is available.\n";
      else 
	msg = "Available GENEPI plugins are: ";
      ccl_log (log, "%s", msg);
    }

  if (nb_plugins > 0)
    {
      const char *fmt = quiet ? " %s" : ", %s";

      ccl_log (log, "%s", plugins_names[0]);
      for (i = 1; i < nb_plugins; i++)
	ccl_log (log, fmt, plugins_names[i]);
      ccl_log (log, "\n");
    }
}
