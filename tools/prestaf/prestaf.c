/*
 * prestaf.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <stdio.h>
#include <ccl/ccl-init.h>
#include <sataf/sataf.h>
#include "prestaf-parser.h"
#include "prestaf-interp.h"
#include <prestaf/prestaf-batof.h>
#include <prestaf/prestaf-automata.h>

#ifndef MSA_OP_CACHE_SIZE
# define MSA_OP_CACHE_SIZE (1024*1024+41)
#endif /* MSA_OP_CACHE_SIZE */

#ifndef MAO_SHARING_CACHE_SIZE
# define MAO_SHARING_CACHE_SIZE 11229331
#endif /* ! MAO_SHARING_CACHE_SIZE */

#ifndef EA_UT_INIT_TABLE_SIZE
# define EA_UT_INIT_TABLE_SIZE 1403641
#endif /* ! EA_UT_INIT_TABLE_SIZE */

#ifndef EA_UT_FILL_DEGREE 
# define EA_UT_FILL_DEGREE 5
#endif /* ! EA_UT_FILL_DEGREE */

#ifndef SA_UT_INIT_TABLE_SIZE
# define SA_UT_INIT_TABLE_SIZE 1403641
#endif /* ! SA_UT_INIT_TABLE_SIZE */

#ifndef SA_UT_FILL_DEGREE
# define SA_UT_FILL_DEGREE 5
#endif /* ! SA_UT_FILL_DEGREE */

#ifndef MSA_UT_INIT_TABLE_SIZE 
# define MSA_UT_INIT_TABLE_SIZE 1403641
#endif /* ! MSA_UT_INIT_TABLE_SIZE */

#ifndef MSA_UT_FILL_DEGREE 
# define MSA_UT_FILL_DEGREE 5
#endif /* ! MSA_UT_FILL_DEGREE */

#define F_FLAG(i) (1<<(i))
#define F_PRETEST      F_FLAG(0)
#define F_DISP_FORMULA F_FLAG(1)
#define F_DOT          F_FLAG(2)
#define F_VARS         F_FLAG(3)
#define F_FORMULA_STAT F_FLAG(4)
#define F_SOLUTIONS    F_FLAG(5)
#define F_HELP         F_FLAG(6)
#define F_SF           F_FLAG(7)
#define F_TRACES       F_FLAG(8)
#define F_UNFOLD       F_FLAG(9)
#define F_UNFOLD2      F_FLAG(10)
#define F_FORMULA_STAT2 F_FLAG(11)
#define F_SF2          F_FLAG(12)
#define F_IS_TAUT      F_FLAG(13)
#define F_SYNTAX       F_FLAG(14)
#define F_DISP_AUT     F_FLAG(15)

			/* --------------- */

static void
s_init (void);

static void
s_terminate (void);

static void
s_test (void);

static void
s_usage (ccl_log_type log, const char *cmd);

static void
s_syntax (ccl_log_type log);

static void
s_show_vars (ccl_list *l);

static void
s_parse_args (int argc, char **argv);

			/* --------------- */

static char *filename = NULL;
static uint32_t flags = 0;

			/* --------------- */

int
main (int argc, char **argv)
{
  int result = 0;

  s_init ();

#if 0
  ccl_try (exception)
#endif
    {

      ccl_pair *p;
      ccl_parse_tree *T;
      ccl_list *formulas;      

      s_parse_args (argc, argv);
      if (flags & F_HELP)
	{
	  s_usage (CCL_LOG_DISPLAY, argv[0]);
	}
      else if (flags & F_SYNTAX)
	{
	  s_syntax (CCL_LOG_DISPLAY);
	}
      else
	{
	  if (flags & F_PRETEST)
	    {
	      s_test ();
	    }

	  if (filename != NULL)
	    {
	      T = prestaf_load_file (filename);
	      formulas = prestaf_interp (T);

	      ccl_parse_tree_delete_container (T);

	      for (p = FIRST (formulas); p; p = CDR (p))
		{
		  prestaf_formula *F = (prestaf_formula *) CAR (p);
		  prestaf_predicate *P;
		  
		  if (flags & F_DISP_FORMULA)
		    {
		      prestaf_formula_display (CCL_LOG_DISPLAY, F);
		      ccl_display ("\n");
		    }

		  P = prestaf_formula_solutions (F);
		  if (flags & F_IS_TAUT)
		    {
		      sataf_msa *rel = prestaf_predicate_get_relation (P);
		      if (! sataf_msa_is_one (rel))
			ccl_display ("not a ");
		      ccl_display ("tautology\n");
		      sataf_msa_del_reference (rel);
		    }

		  if (flags & F_VARS)
		    s_show_vars (prestaf_predicate_get_variables (P));

		  if (flags & F_DOT)
		    {
		      sataf_msa *rel = prestaf_predicate_get_relation (P);
		      sataf_msa_display_as_olddot (rel, CCL_LOG_DISPLAY, NULL);
		      sataf_msa_del_reference (rel);
		    }

		  if (flags & F_DISP_AUT)
		    prestaf_formula_display_automaton (F, CCL_LOG_DISPLAY);

		  if ((flags & F_UNFOLD) || (flags & F_UNFOLD2))
		    {
		      ccl_list *vars = prestaf_predicate_get_variables (P);
		      int nb_vars = ccl_list_get_size (vars);
		      sataf_msa *rel = prestaf_predicate_get_relation (P);
		      sataf_mao *relmao = 
			prestaf_crt_unfolding_automaton (nb_vars, rel);

		      if (flags & F_UNFOLD)
			prestaf_unfold_to_dot (CCL_LOG_DISPLAY, relmao, NULL, 
					       NULL, "digraph");
		      else
			sataf_mao_to_dot (CCL_LOG_DISPLAY, relmao, NULL, 
					  NULL, "digraph");

		      sataf_msa_del_reference (rel);
		      sataf_mao_del_reference (relmao);
		    }

		  if ((flags & F_FORMULA_STAT2))
		    {
		      ccl_list *vars = prestaf_predicate_get_variables (P);
		      int nb_vars = ccl_list_get_size (vars);
		      sataf_msa *rel = prestaf_predicate_get_relation (P);
		      sataf_mao *relmao = 
			prestaf_crt_unfolding_automaton (nb_vars, rel);
		      sataf_msa *msarelmao = sataf_msa_compute (relmao);
		      
		      sataf_msa_log_info (CCL_LOG_DISPLAY, msarelmao);
		      ccl_display ("\n");
		      sataf_msa_del_reference (rel);
		      sataf_mao_del_reference (relmao);
		      sataf_msa_del_reference (msarelmao);
		    }

		  if (flags & F_SOLUTIONS)
		    {
		      if (!prestaf_predicate_display_vectors (CCL_LOG_DISPLAY,
							      P))
			ccl_warning ("infinite solution set.\n");
		    }

		  if (flags & (F_SF | F_SF2))
		    prestaf_predicate_display_formula (CCL_LOG_DISPLAY, P,
						       flags & F_SF);

		  if (flags & F_FORMULA_STAT)
		    prestaf_formula_statistics (F, CCL_LOG_DISPLAY);

		  prestaf_predicate_del_reference (P);
		}
	      for (p = FIRST (formulas); p; p = CDR (p))
		prestaf_formula_del_reference (CAR (p));
	      ccl_list_delete (formulas);
	    }
	}
    }
#if 0
  ccl_catch
    {
      ccl_exception_print ();
      result = 1;
      exit (1);
    }
  ccl_end_try;
#endif

  s_terminate ();

  return result;
}

			/* --------------- */

static void
s_listener (ccl_log_type type, const char *msg, void *data)
{
  if (type == CCL_LOG_DISPLAY)
    fprintf (stdout, "%s", msg);
  else if (type != CCL_LOG_DEBUG || (flags & F_TRACES))
    fprintf (stderr, "%s", msg);
}

			/* --------------- */

static void
s_init (void)
{
  ccl_init ();
  sataf_init (MSA_OP_CACHE_SIZE, MAO_SHARING_CACHE_SIZE,
	      EA_UT_INIT_TABLE_SIZE, EA_UT_FILL_DEGREE,
	      SA_UT_INIT_TABLE_SIZE, SA_UT_FILL_DEGREE,
	      MSA_UT_INIT_TABLE_SIZE, MSA_UT_FILL_DEGREE);
  ccl_log_add_listener (s_listener, NULL);
}

			/* --------------- */

static void
s_terminate (void)
{
  sataf_terminate ();
  ccl_terminate ();
}

			/* --------------- */

static void
s_test (void)
{
  uint32_t a1_init = 1;
  uint32_t a2_init = 2;
  uint8_t a1_is_final[] = { 1, 0, 0, 1, 1 };
  uint8_t a2_is_final[] = { 0, 1, 0, 0, 0 };
  uint32_t a1_succ[] = { 4, 4, 2, 3, 1, 1, 4, 1, 3, 0 };
  uint32_t a2_succ[] = { 4, 4, 2, 2, 3, 1, 2, 4, 0, 3 };

  sataf_mao *A2 = sataf_mao_create_from_arrays (5, 2, a1_init,
							   a1_is_final,
							   a1_succ);
  sataf_mao *A1 = sataf_mao_create_from_arrays (5, 2, a2_init,
							   a2_is_final,
							   a2_succ);

  {
    sataf_msa *a1 = sataf_msa_compute (A1);
    sataf_msa *a2 = sataf_msa_compute (A2);
    sataf_msa *notA2 = sataf_msa_not (a2);
    sataf_msa *A1capnotA2 = sataf_msa_and (a1, notA2);

    sataf_msa_display_as_dot (A1capnotA2, 1, NULL, 1);

    sataf_msa_del_reference (notA2);
    sataf_msa_del_reference (A1capnotA2);
    sataf_msa_del_reference (a1);
    sataf_msa_del_reference (a2);
  }
  sataf_mao_del_reference (A1);
  sataf_mao_del_reference (A2);
}

			/* --------------- */

static void
s_show_vars (ccl_list *l)
{
  ccl_pair *p;

  for (p = FIRST (l); p; p = CDR (p))
    {
      ccl_log (CCL_LOG_DISPLAY, "%s ", CAR (p));
    }
  ccl_log (CCL_LOG_DISPLAY, "\n");
}

			/* --------------- */

static void
s_display_strings (ccl_log_type log, const char **strings)
{
  const char **s;
  for(s = strings; *s; s++)
    ccl_log (log, "%s\n", *s);
}

			/* --------------- */

static const char *help[] =
  {
   "USAGE:%s [options] input-filename\nwith options:",
   "-p : run a pre-test",
   "-f : display formulas",
   "-d : display solutions in dot file format",
   "-e : display solutions in automaton file format",
   "-v : display the list of variables for each formula",
   "-s : display solutions",
   "-m : display stats about formulas",
   "-a : display synthesized formula",
   "-b : display synthesized without the prefix tree",
   "-t : display debug traces",
   "-u : display solutions in dot file format using bit of vectors.",
   "-y : display solutions with an unfolded (dot) graph distinguishing ",
   "     principal states.",
   "-z : check if relation is a tautology.",
   "-w : display the input syntax.",
   "-h : this help message",
   NULL
  };

static void
s_usage (ccl_log_type log, const char *cmd)
{
  const char **s = help;
  ccl_log (log, *s, cmd);
  s_display_strings (log, ++s);
}

static const char *syntax[] =
  {
   "Syntax of PRESTAF files:",
   "------------------------\n",
   "prestaf ::= (statement;)*\n",
   "statement ::= formula",
   "           | definition\n",
   "definition ::= <IDENT> ::= formula",
   "            |  <IDENT> ::= load <STRING>\n",
   "formula ::= formula || formula",
   "         |  formula && formula",
   "         |  formula ^^ formula",
   "         |  formula => formula",
   "         |  formula <=> formula",
   "         |  linear_equation",
   "         |  quantified_formula",
   "         |  in_formula",
   "         |  ! formula",
   "         |  ( formula )",
   "         |  <IDENT>\n",
   "linear_equation ::= linear_term == linear_term",
   "                 |  linear_term <= linear_term",
   "                 |  linear_term >= linear_term",
   "                 |  linear_term < linear_term",
   "                 |  linear_term > linear_term\n",
   "linear_term ::= linear_term + factor",
   "             |  linear_term - factor",
   "             |  factor\n",
   "factor ::= <INTEGER> * <IDENT>",
   "        |  <IDENT> * <INTEGER>",
   "        |  <INTEGER>",
   "        |  <IDENT>\n",
   "quantified_formula ::= E. variables formula",
   "                    |  A. variable formula\n",
   "variables ::= <IDENT>",
   "           |  <IDENT> , variables\n",
   "in_formula ::= linear_term in [ linear_term , linear_term ]\n",
   "Syntax of automata (S states, N variables, F final states, E edges):",
   "--------------------------------------------------------------------\n",
   "var_1 var_2 ... var_N",
   "S initial final_1 ... final_F",
   "src_1 bit_1 tgt_1",
   "...",
   "src_E bit_E tgt_E\n",
   NULL
  };

static void
s_syntax (ccl_log_type log)
{
  s_display_strings (log, syntax);
}

			/* --------------- */

static void
s_parse_args (int argc, char **argv)
{
  int i = 0;

  for (i = 1; i < argc; i++)
    {
      if (argv[i][0] == '-')
	{
	  char *s;

	  for (s = argv[i] + 1; *s; s++)
	    {
	      switch (*s)
		{
		case 'h':
		  flags |= F_HELP;
		  break;
		case 'w':
		  flags |= F_SYNTAX;
		  break;
		case 'e':
		  flags |= F_DISP_AUT;
		  break;
		case 'p':
		  flags |= F_PRETEST;
		  break;
		case 'f':
		  flags |= F_DISP_FORMULA;
		  break;
		case 'd':
		  flags |= F_DOT;
		  break;
		case 'v':
		  flags |= F_VARS;
		  break;
		case 's':
		  flags |= F_SOLUTIONS;
		  break;
		case 'm':
		  flags |= F_FORMULA_STAT;
		  break;
		case 'n':
		  flags |= F_FORMULA_STAT2;
		  break;
		case 'a':
		  flags |= F_SF;
		  break;
		case 'b':
		  flags |= F_SF2;
		  break;
		case 'u':
		  flags |= F_UNFOLD;
		  break;
		case 'y':
		  flags |= F_UNFOLD2;
		  break;
		case 't':
		  flags |= F_TRACES;
		  if (s[1] != '\0')
		    {
		      ccl_debug_max_level (atoi (s + 1));
		      s[1] = 0;
		    }
		  break;
		case 'z':
		  flags |= F_IS_TAUT; break;
		  break;
		default:
		  ccl_throw (exception, "unknown option\n");
		};
	    }
	}
      else
	filename = argv[i];
    }
  if (filename == NULL && !(flags & (F_HELP | F_SYNTAX | F_PRETEST)))
    ccl_throw (exception, "no input file\n");
}

			/* --------------- */
