/*
 * prestaf-interp.h -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __PRESTAF_INTERP_H__
# define __PRESTAF_INTERP_H__

# include <ccl/ccl-parse-tree.h>
# include <ccl/ccl-list.h>
# include <prestaf/prestaf-formula.h>

typedef enum prestaf_ptree_node_enum {
  PRESTAF_FORMULA = 0,

  PRESTAF_DEFINITION, 

  PRESTAF_OR, PRESTAF_AND, PRESTAF_NOT, PRESTAF_XOR, 
  PRESTAF_EQUIV, PRESTAF_IMPLY, PRESTAF_NEQ,

  PRESTAF_IN,

  PRESTAF_FORALL,  PRESTAF_EXISTS,

  PRESTAF_LEQ,  PRESTAF_GEQ,   PRESTAF_LT, PRESTAF_GT, PRESTAF_EQ,
  PRESTAF_PLUS, PRESTAF_MINUS, PRESTAF_NEG,

  PRESTAF_FACTOR,  PRESTAF_IDENT, PRESTAF_INTEGER, 
  PRESTAF_STATEMENT_LIST,

  PRESTAF_LOADFILE, PRESTAF_STRING
} prestaf_ptree_node;

extern ccl_list *
prestaf_interp (ccl_parse_tree *tree);

#endif /* ! __PRESTAF_INTERP_H__ */
