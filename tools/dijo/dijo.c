/*
 * dijo.c -- This is the GENEPI trace runner
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <genepi/genepi.h>
#include <genepi/genepi-loader.h>
#include <genepi/genepi-trace.h>

#define CMDNAME "dijo"
#define READ_BUFFER_INIT_SIZE 10

			/* --------------- */

static int current_line = 0;
static int read_buffer_size;
static char *read_buffer;

			/* --------------- */

static int OPT_NB_ENTRIES_IN_CACHE = 0;
static int OPT_CHECK_SIZE = 0;
static int OPT_HELP = 0;
static int OPT_DEFAULT_DIR = 0;
static char *OPT_PLUGIN_NAME = NULL;
static char *OPT_TRACE_FILENAME = NULL;

			/* --------------- */

static void
s_help (FILE *out, const char *cmdname);

static void
s_usage (const char *cmdname);

static void
s_show_plugins (FILE *out);

static void
s_parse_argv (const char *cmdname, int argc, char **argv);

static int
s_run_trace (FILE * in, genepi_solver *solver);

static int
s_read_preamble (FILE * in, char **plugin);

static void
s_add_arg (int value, int *pargc, int **pargv, int *argv_size);

static int
s_read_command_line (FILE * in, int *pargc, int **pargv, int *argv_size);

static void
s_execute_command (genepi_solver *solver, genepi_trace_command cmd, int argc, 
		   int *argv, int check_size, int *nb_variables,
		   genepi_set *** variables, int *variables_size);

static void
s_ensure_read_buffer_size (int pos);

static char *
s_read_line (FILE * in);

static void
s_check_for_variable (int index, int *nb_variables, genepi_set *** variables,
		      int *variables_size, int mandatory);

static int
s_file_exists (const char *filename);

			/* --------------- */

int
main (int argc, char **argv)
{
  int result = 0;
  FILE *tracein;

  s_parse_argv (CMDNAME, argc, argv);

  if (OPT_HELP)
    s_help (stdout,CMDNAME);
  else if (OPT_DEFAULT_DIR)
    {
      const char *dir;

      genepi_loader_init ();
      dir = genepi_loader_get_default_directory ();
      printf ("%s\n", dir);
      genepi_loader_terminate ();
    }
  else
    {
      genepi_plugin *plugin = NULL;
      
      assert (OPT_PLUGIN_NAME != NULL);
      
      genepi_loader_init ();

      if (!s_file_exists (OPT_PLUGIN_NAME))
	  genepi_loader_load_default_plugins ();
      plugin = genepi_loader_get_plugin (OPT_PLUGIN_NAME);

      if (plugin == NULL)
	{
	  fprintf (stderr, "no plugin found.\n");
	  result = 1;
	}
      else
	{
	  genepi_solver *solver = 
	    genepi_solver_create (plugin, 
				  GENEPI_FLAG_SHOW_TIMERS|
				  GENEPI_FLAG_SHOW_CACHE_INFO, 0, 
				  OPT_NB_ENTRIES_IN_CACHE);
	  const char *encoding = genepi_solver_get_name (solver);

	  assert (encoding != NULL);

	  printf ("current set encoding is : %s\n", encoding);

	  if (OPT_TRACE_FILENAME == NULL)
	    {
	      int autotest_result;

	      printf ("No trace file is specified.\nstarting the autotest\n");
	      printf ("-- > auto test ");
	      autotest_result = genepi_solver_run_autotest (solver);

	      printf ("%s !\nauto test done.\n",
		      autotest_result?"passed":"failed");
	      result = !autotest_result?1:0;
	    }
	  else
	    {
	      tracein = fopen (OPT_TRACE_FILENAME, "r");
	      if (tracein == NULL)
		{
		  fprintf (stderr, "can't open file '%s'\n", 
			   OPT_TRACE_FILENAME);
		  result = 1;
		}
	      else
		{
		  read_buffer_size = READ_BUFFER_INIT_SIZE;
		  read_buffer = (char *) 
		    calloc (sizeof (char), read_buffer_size);
		  result = !s_run_trace (tracein, solver);
		  free (read_buffer);
		  fclose (tracein);
		}
	    }

	  genepi_solver_delete (solver);
	  genepi_plugin_del_reference (plugin);
	  genepi_loader_terminate ();
	}
    }

  return result;
}

			/* --------------- */

static void
s_help (FILE *out, const char *cmdname)
{
  fprintf (out, "usage: %s [-c] [-h] [-d] plugin [trace_file]\n", cmdname);
  fprintf (out, "'plugin' is either the internal name of the plugin or a "
	   "filename.\n");
  fprintf (out, "options meaning is:\n");
  fprintf (out,
	   " -c enable the structure size checking. This option works only if "
	   "the selected \n"
	   "    plugin is the one used to produce the trace.\n");
  fprintf (out, " -d display the directory of default plugins.\n");
  fprintf (out, " -e N the number of entries in GENEPI cache (default=%d).\n",
	   OPT_NB_ENTRIES_IN_CACHE);
  fprintf (out, " -h display this message.\n");
  fprintf (out, "\n");
  s_show_plugins (out);
}

			/* --------------- */

static void
s_usage (const char *cmdname)
{
  s_help (stderr, cmdname);

  exit (1);
}

			/* --------------- */

static void
s_parse_argv (const char *cmdname, int argc, char **argv)
{
  int i;

  for (i = 1; i < argc; i++)
    {
      if (argv[i][0] == '-')
	{
	  if (argv[i][1] == 'h')
	    OPT_HELP = 1;
	  else if (argv[i][1] == 'c')
	    OPT_CHECK_SIZE = 1;
	  else if (argv[i][1] == 'd')
	    OPT_DEFAULT_DIR = 1;
	  else if (argv[i][1] == 'e')
	    {
	      if (i == argc -1 )
		{
		  fprintf (stderr, "missing argument to option '%s'\n", 
			   argv[i]);
		  s_usage (cmdname);
		}
	      else
		{
		  i++;
		  OPT_NB_ENTRIES_IN_CACHE = atoi(argv[i]);
		}
	    }
	  else
	    {
	      fprintf (stderr, "unknown option '%s'\n", argv[i]);
	      s_usage (cmdname);
	    }
	}
      else if (OPT_PLUGIN_NAME == NULL)
	OPT_PLUGIN_NAME = argv[i];
      else if (OPT_TRACE_FILENAME == NULL)
	OPT_TRACE_FILENAME = argv[i];
      else
	{
	  fprintf (stderr, "warning: unknown argument '%s'\n", argv[i]);
	  s_usage (cmdname);
	}
    }

  if (!OPT_HELP && !OPT_DEFAULT_DIR)
    {
      if (OPT_PLUGIN_NAME == NULL)
	{
	  fprintf (stderr, "no GENEPI-plugin is defined.\n");
	  s_usage (cmdname);
	}
    }
}

			/* --------------- */

static int
s_run_trace (FILE * in, genepi_solver *solver)
{
  const char *name;
  int result = 1;
  char *oplugin = NULL;
  int nb_variables = 0;
  int variables_size = 1;
  genepi_set **variables = NULL;
  int *argv, argc, argv_size;
  int check_size;

  result = s_read_preamble (in, &oplugin);

  if (!result)
    return 0;

  printf ("plugin used to generate trace: %s\n", oplugin);
  name = genepi_solver_get_name (solver);
  assert (name != NULL);

  check_size = OPT_CHECK_SIZE && strcmp (name, oplugin) == 0;

  argv_size = 1;
  argv = (int *) calloc (sizeof (int), argv_size);
  argc = 0;

  variables = (genepi_set **) calloc (sizeof (genepi_set *), variables_size);

  while (s_read_command_line (in, &argc, &argv, &argv_size))
    s_execute_command (solver, (genepi_trace_command) argv[0], argc - 1, 
		       argv + 1, check_size, &nb_variables, &variables,
		       &variables_size);

  free (argv);
  free (variables);
  free (oplugin);

  return 1;
}

			/* --------------- */

static int
s_read_preamble (FILE * in, char **plugin)
{
  *plugin = s_read_line (in);

  if (*plugin == NULL)
    return 0;
  *plugin = strdup (*plugin);

  return 1;
}

			/* --------------- */

static void
s_add_arg (int value, int *pargc, int **pargv, int *argv_size)
{
  if (*pargc == *argv_size)
    {
      int new_size = *pargc * 2 + 1;

      *pargv = (int *) realloc (*pargv, sizeof (int) * new_size);
      *argv_size = new_size;
    }

  (*pargv)[*pargc] = value;
  (*pargc)++;
}

			/* --------------- */

static int
s_read_command_line (FILE * in, int *pargc, int **pargv, int *argv_size)
{
  char *cmd = s_read_line (in);
  int argc;
  int *argv;

  if (cmd == NULL)
    return 0;

  argc = 0;
  argv = *pargv;

  while (*cmd != '\0')
    {
      char *wsp = strchr (cmd, ' ');

      if (wsp != NULL)
	*wsp = '\0';

      s_add_arg (atoi (cmd), &argc, &argv, argv_size);
      if (wsp != NULL)
	cmd = wsp + 1;
      else
	cmd += strlen (cmd);
    }

  *pargc = argc;
  *pargv = argv;

  return 1;
}

			/* --------------- */

static void
s_execute_command (genepi_solver *solver, genepi_trace_command cmd, int argc, 
		   int *argv, int check_size, int *nb_variables,
		   genepi_set *** variables, int *variables_size)
{
  switch (cmd)
    {
    case GENEPI_TRACE_ADD_REF:
      assert (argc == 1);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    1);
      genepi_set_add_reference (solver, (*variables)[argv[0]]);
      break;

    case GENEPI_TRACE_DEL_REF:
      assert (argc == 1);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    1);
      genepi_set_del_reference (solver, (*variables)[argv[0]]);
      break;

    case GENEPI_TRACE_TOP:
      assert (argc == 2);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    0);
      (*variables)[argv[0]] = genepi_set_top (solver, argv[1]);
      break;

    case GENEPI_TRACE_TOP_N:
      assert (argc == 2);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    0);
      (*variables)[argv[0]] = genepi_set_top_N (solver, argv[1]);
      break;

    case GENEPI_TRACE_TOP_Z:
      assert (argc == 2);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    0);
      (*variables)[argv[0]] = genepi_set_top_Z (solver, argv[1]);
      break;

    case GENEPI_TRACE_TOP_P:
      assert (argc == 2);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    0);
      (*variables)[argv[0]] = genepi_set_top_P (solver, argv[1]);
      break;

    case GENEPI_TRACE_TOP_R:
      assert (argc == 2);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    0);
      (*variables)[argv[0]] = genepi_set_top_R (solver, argv[1]);
      break;

    case GENEPI_TRACE_BOT:
      assert (argc == 2);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    0);
      (*variables)[argv[0]] = genepi_set_bot (solver, argv[1]);
      break;

    case GENEPI_TRACE_LINEAR:
      assert (argc > 4);
      assert (argc == 4 + argv[3]);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    0);
      (*variables)[argv[0]] =
	genepi_set_linear_operation (solver, &argv[4], argv[3], argv[1], 
				     argv[2]);
      break;

    case GENEPI_TRACE_UNION:
      assert (argc == 3);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    0);
      s_check_for_variable (argv[1], nb_variables, variables, variables_size,
			    1);
      s_check_for_variable (argv[2], nb_variables, variables, variables_size,
			    1);
      (*variables)[argv[0]] =
	genepi_set_union (solver, (*variables)[argv[1]], 
			  (*variables)[argv[2]]);
      break;

    case GENEPI_TRACE_INTERSECTION:
      assert (argc == 3);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    0);
      s_check_for_variable (argv[1], nb_variables, variables, variables_size,
			    1);
      s_check_for_variable (argv[2], nb_variables, variables, variables_size,
			    1);
      (*variables)[argv[0]] =
	genepi_set_intersection (solver, (*variables)[argv[1]],
				 (*variables)[argv[2]]);
      break;

    case GENEPI_TRACE_COMPLEMENT:
      assert (argc == 2);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    0);
      s_check_for_variable (argv[1], nb_variables, variables, variables_size,
			    1);
      (*variables)[argv[0]] = genepi_set_complement (solver, 
						     (*variables)[argv[1]]);
      break;

    case GENEPI_TRACE_PROJECT:
      assert (argc > 3);
      assert (argc == 3 + argv[2]);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    0);
      s_check_for_variable (argv[1], nb_variables, variables, variables_size,
			    1);
      (*variables)[argv[0]] =
	genepi_set_project (solver, (*variables)[argv[1]], &argv[3], argv[2]);
      break;

    case GENEPI_TRACE_INVPROJECT:
      assert (argc > 3);
      assert (argc == 3 + argv[2]);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    0);
      s_check_for_variable (argv[1], nb_variables, variables, variables_size,
			    1);
      (*variables)[argv[0]] =
	genepi_set_invproject (solver, (*variables)[argv[1]], &argv[3], 
			       argv[2]);
      break;

    case GENEPI_TRACE_APPLY:
      assert (argc == 3);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    0);
      s_check_for_variable (argv[1], nb_variables, variables, variables_size,
			    1);
      s_check_for_variable (argv[2], nb_variables, variables, variables_size,
			    1);
      (*variables)[argv[0]] =
	genepi_set_apply (solver, (*variables)[argv[1]], 
			  (*variables)[argv[2]]);
      break;

    case GENEPI_TRACE_APPLYINV:
      assert (argc == 3);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    0);
      s_check_for_variable (argv[1], nb_variables, variables, variables_size,
			    1);
      s_check_for_variable (argv[2], nb_variables, variables, variables_size,
			    1);
      (*variables)[argv[0]] =
	genepi_set_applyinv (solver, (*variables)[argv[1]], 
			     (*variables)[argv[2]]);
      break;

    case GENEPI_TRACE_CHK_COMPARE:
      assert (argc == 4);
      s_check_for_variable (argv[1], nb_variables, variables, variables_size,
			    1);
      s_check_for_variable (argv[2], nb_variables, variables, variables_size,
			    1);
      {
	int res = genepi_set_compare (solver, (*variables)[argv[1]], argv[0],
				      (*variables)[argv[2]]);
	if ((res && !argv[3]) || (!res && argv[3]))
	  {
	    fprintf (stderr, "%s:%d: failed genepi_set_compare\n",
		     OPT_TRACE_FILENAME, current_line);
	    abort ();
	  }
      }
      break;

    case GENEPI_TRACE_CHK_EMPTY:
      assert (argc == 2);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    1);
      {
	int res = genepi_set_is_empty (solver, (*variables)[argv[0]]);
	if ((res && !argv[1]) || (!res && argv[1]))
	  {
	    fprintf (stderr, "%s:%d: failed genepi_set_is_empty\n",
		     OPT_TRACE_FILENAME, current_line);
	    abort ();
	  }
      }
      break;

    case GENEPI_TRACE_CHK_FULL:
      assert (argc == 2);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    1);
      {
	int res = genepi_set_is_full (solver, (*variables)[argv[0]]);
	if ((res && !argv[1]) || (!res && argv[1]))
	  {
	    fprintf (stderr, "%s:%d: failed genepi_set_full\n",
		     OPT_TRACE_FILENAME, current_line);
	    abort ();
	  }
      }
      break;

    case GENEPI_TRACE_CHK_FINITE:
      assert (argc == 2);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    1);
      {
	int res = genepi_set_is_finite (solver, (*variables)[argv[0]]);
	if ((res && !argv[1]) || (!res && argv[1]))
	  {
	    fprintf (stderr, "%s:%d: failed genepi_set_is_finite\n",
		     OPT_TRACE_FILENAME, current_line);
	    abort ();
	  }
      }
      break;

    case GENEPI_TRACE_CHK_WIDTH:
      assert (argc == 2);
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    1);
      if (genepi_set_get_width (solver, (*variables)[argv[0]]) != argv[1])
	{
	  fprintf (stderr, "%s:%d: failed genepi_set_get_width\n",
		   OPT_TRACE_FILENAME, current_line);
	  abort ();
	}
      break;

    case GENEPI_TRACE_CHK_DS_SIZE:
      assert (argc == 2);
      if (!check_size)
	break;
      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    1);
      if (genepi_set_get_data_structure_size (solver, (*variables)[argv[0]]) !=
	  argv[1])
	{
	  fprintf (stderr,
		   "%s:%d: failed genepi_set_get_data_structure_size\n",
		   OPT_TRACE_FILENAME, current_line);
	  abort ();
	}
      break;

    case GENEPI_TRACE_CHK_NB_VARS:
      assert (argc == 1);
      if (*nb_variables == argv[1])
	{
	  fprintf (stderr, "%s:%d: wrong number of created variables\n",
		   OPT_TRACE_FILENAME, current_line);
	  abort ();
	}
      break;

    case GENEPI_TRACE_CHK_SOLUTION:
      assert (argc >= 4);
      assert (argc == 4+argv[2]);

      s_check_for_variable (argv[0], nb_variables, variables, variables_size,
			    1);
      {
	int idop = argv[0];
	int result = argv[1];
	int x_size = argv[2];
	int xden = argv[3];
	const int *x = &argv[4];
	int res = genepi_set_is_solution (solver, (*variables)[idop], x, 
					  x_size, xden);

	if ((res && !result) || (!res && result))
	  {
	    fprintf (stderr, "%s:%d: failed genepi_set_is_solution\n",
		     OPT_TRACE_FILENAME, current_line);
	    abort ();
	  }

      }
      break;

    default:
      fprintf (stderr,
	       "%s:%d: there is something wrong in this trace file.\n",
	       OPT_TRACE_FILENAME, current_line);
      abort ();
    }
}

			/* --------------- */

static void
s_ensure_read_buffer_size (int pos)
{
  char *newbuf;

  if (pos < read_buffer_size)
    return;

  newbuf = (char *) calloc (sizeof (char), 2 * read_buffer_size + 1);

  if (newbuf == NULL)
    {
      fprintf (stderr, "not enough memory to allocate read buffer.\n");
      abort ();
    }

  memcpy (newbuf, read_buffer, sizeof (char) * read_buffer_size);
  free (read_buffer);
  read_buffer = newbuf;
  read_buffer_size = 2 * read_buffer_size + 1;
}

			/* --------------- */

static char *
s_read_line (FILE * in)
{
  int c;
  int pos = 0;

  current_line++;
  while ((c = fgetc (in)) != EOF)
    {
      if (((char) c) == '\n')
	break;

      s_ensure_read_buffer_size (pos);
      read_buffer[pos++] = (char) c;
    }

  if (c == EOF && pos == 0)
    return NULL;

  s_ensure_read_buffer_size (pos);
  read_buffer[pos] = '\0';

  return read_buffer;
}

			/* --------------- */

static void
s_check_for_variable (int index, int *nb_variables, genepi_set *** variables,
		      int *variables_size, int mandatory)
{
  if (index < *nb_variables)
    return;

  if (mandatory)
    {
      fprintf (stderr, "%s:%d: try to read a non-initialized variable\n",
	       OPT_TRACE_FILENAME, current_line);
      abort ();
    }
  else
    {
      while (index >= *variables_size)
	{
	  *variables_size = *variables_size * 2 + 1;
	  
	  *variables = (genepi_set **)
	    realloc (*variables, sizeof (genepi_set *) * (*variables_size));
	}
      (*variables)[index] = NULL;
      *nb_variables = index+1;
    }
}

			/* --------------- */

static int
s_file_exists (const char *filename)
{
  FILE *f = fopen (filename, "r");

  if (f != NULL)
    fclose(f);

  return (f != NULL);
}

			/* --------------- */

static void
s_show_plugins (FILE *out)
{
  int i;
  int nb_plugins;
  char **plugins;

  genepi_loader_init ();
  genepi_loader_load_default_plugins ();
  plugins = genepi_loader_get_plugins (&nb_plugins);

  if (nb_plugins == 0)    
    fprintf (out, "no plugin is installed");
  else
    fprintf (out, "current plugins are: ");
  for (i = 0; i < nb_plugins; i++)
    {
      const char *fmt = i < nb_plugins - 1 ? "%s, " : "%s";
      fprintf (out, fmt, plugins[i]);
      free (plugins[i]);
    }
  free (plugins);
  fprintf (out, "\n");
  fflush (out);

  genepi_loader_terminate ();
}
