\documentclass{article}

\usepackage{a4wide}
\usepackage{multirow}
\usepackage{here}
\usepackage{amssymb}
%\usepackage{tabularx}
\usepackage{pstricks}
\usepackage{pst-node}
%\usepackage{pst-tree}

\author{J.~Leroux, G. Point and G.~Sutre}
\title{The grammar of the ARMOISE language\\
       and its syntactic tree\\
       Version 1.00}
\date{\today}

\def\Yield{ $::=$ }
\def\NLabel#1{{\small\texttt{#1}}}
\def\T#1{{\bf\sc\texttt{#1}}}
\def\NT#1{{\em{#1}}}
\def\EMPTY{$\epsilon$}
\def\eline{\multicolumn{2}{c}{\ }\\ }
\def\sline#1{\hline \multicolumn{2}{c}{{\bf #1}}\\ \hline}
\def\Opt#1{{#1}$_{opt}$}

\newenvironment{bnf}%
{\begin{flushleft}\begin{tabular}{r@{ \Yield }l}}%
{\end{tabular}\end{flushleft}}


\def\OptSC{\Opt{\T{;}}\ }

\def\rground#1{\rnode{#1}{\pswedge[linewidth=.1pt,fillstyle=vlines,%
hatchsep=1pt,hatchwidth=.1pt](0,0){6pt}{-90}{90}}}
\def\bground#1{\rnode{#1}{\pswedge[linewidth=.1pt,fillstyle=vlines,%
hatchsep=1pt,hatchwidth=.1pt](0,0){6pt}{180}{0}}}
\def\rdots#1{\rnode{#1}{\ $\dots$\ }}

\newlength{\boxw}
%% \treenode{id}{label}
\newcommand\treenode[2]{%
\let\fsize=\tiny
\settowidth{\boxw}{\sc\fsize #2}%
\setlength{\boxw}{.5\boxw}%
\rnode{#1}{\begin{tabular}{|c|c|}%
\hline%
\multicolumn{2}{|c|}{\sc\fsize #2}\\%
\hline%
%{\tiny next} & {\tiny child}\\
\parbox{\boxw}{\centering  \rnode{#1-child}{$\bullet$}}%
&%
\parbox{\boxw}{\centering \rnode{#1-next}{$\bullet$}}\\%
\hline%
\end{tabular}}%
}

\makeatletter
\newpsobject{@lnkBT}{ncangle}{linewidth=\arrayrulewidth,angleA=-90,angleB=90}
\newpsobject{@lnkBL}{ncangle}{linewidth=\arrayrulewidth,angleA=-90,angleB=180}
\newpsobject{@lnkRL}{ncangle}{linewidth=\arrayrulewidth,angleA=0,angleB=180}
\newpsobject{@lnkOptRL}{@lnkRL}{linewidth=\arrayrulewidth,linestyle=dotted,dotsep=1pt}
\newpsobject{@lnkOptBT}{@lnkBT}{linewidth=\arrayrulewidth,linestyle=dotted,dotsep=1pt}
\newpsobject{@lnkOptBL}{@lnkBL}{linewidth=\arrayrulewidth,linestyle=dotted,dotsep=1pt}
\def\lnkBT{\@lnkBT{->}}
\def\lnkBL{\@lnkBL{->}}
\def\lnkRL{\@lnkRL{->}}
\def\lnkOptRL{\@lnkOptRL{->}}
\def\lnkOptBT{\@lnkOptBT{->}}
\def\lnkOptBL{\@lnkOptBL{->}}
\makeatother

\def\rpr#1{(\ref{#1}/\pageref{#1})}
\setcounter{tocdepth}{2}

\begin{document}

\maketitle
\tableofcontents
\ \\
\hrule
\ \\

%%
%%                                                                 Introduction
%%
This document presents the grammar of the ARMOISE language. Non-terminals are 
written using an \NT{italic} font. A \T{typewriter} font and small capitals are
used for terminals. The {\em opt} subscript is added to optional terminals or 
non-terminals.

This following example describes the two production rules for the non-terminal 
{\em definition-list}. {\em definition} is another non-terminal. The semi-colon
``{\tt ;}'' is an optional terminal.

\begin{bnf}
\NT{definition-list} & \NT{definition-list} \OptSC \NT{definition} \\
                     & \NT{definition} \\
\end{bnf}

In this document we also describe the syntactic tree which should be 
produced by a valid parser of the ARMOISE grammar. The following schema (Fig. 
\ref{example}) gives a graphical representation of a syntactic tree. A node of 
the tree is represented by a box containing a label (e.g. {\sc example}) and 
two points ($\bullet$). 

Labels written using an {\em italic} font (e.g. {\em abstract-child3}) are not 
actual labels; they are used for the sake of clarity in place of a set of 
actual labels. Some labels are followed by two numbers (e.g. {\sc child1} 1/2) 
which are respectively the section and the page number of the definition of
that node.

The left point in the box represents the pointer to the child of the node. The 
children of a node are chained using the right point. 

The right-hand side of the figure \ref{example} shows the {\sc ansi-c} 
structure used to store the nodes. An extra-field {\em value} (not shown on 
schemas) is used to store the value associated to some terminals: {\sc 
identifier} which the value is a character string and {\sc integer} which the 
value is an unsigned integer.

The {\sc null} pointer (terminating any node chain) is represented by a 
hatched half-circle. Dots depict a subtree which is not represented. Dotted 
arrows depict optional links; for example, on the figure \ref{example} the 
node labelled by {\sc child1} might be chained with {\sc null} or with a node 
labelled by {\sc child2}.

\begin{figure}[H]
\begin{tabular}{cc}
\begin{tabular}{clll}
 & & & \\
\treenode{example}{example} & \rground{rg} \\
\\
& \treenode{child1}{\label{child1}child1 (1/2)} & 
  \treenode{child2}{\rnode{lc2}{child2}} & \rdots{child2-rg}\\
&    \multicolumn{1}{r}{\rdots{child1-c}} & 
   \multicolumn{1}{r}{\rdots{child2-c}} \\
& \treenode{child3}{\em abstract-child3} \\
& \multicolumn{1}{r}{\rdots{child3-c}} \\
\end{tabular}
&
\begin{minipage}{.5\linewidth}
{\tt\begin{tabbing}
str\=uct\= node\_tree \{\kill
struct node\_tree \{\\
\>int \rnode{label}{\framebox{node\_label}};\\
\>struct node\_tree \rnode{n}{\framebox{*next}};\\
\>struct node\_tree \rnode{c}{\framebox{*child}}; \\
\>union \{\\
\>\>char *identifier;\\
\>\>int   integer;\\
\>\} value;\\
\};
\end{tabbing}
}
\end{minipage}
\end{tabular}
\caption{\label{example}}
\end{figure}
\nccurve[angleA=180,angleB=45,linewidth=.2pt]{->}{label}{lc2}
\nccurve[angleA=180,angleB=-35,linewidth=.2pt]{->}{n}{child2-next}
\nccurve[angleA=180,angleB=-45,linewidth=.2pt]{->}{c}{child2-child}
\lnkRL{example-next}{rg}
\lnkBL{example-child}{child1}
\lnkBL{example-child}{child3}
\lnkOptRL{child1-next}{child2}
\lnkBL{child1-child}{child1-c}
\lnkRL{child2-next}{child2-rg}
\lnkBL{child2-child}{child2-c}
\lnkBL{child3-child}{child3-c}

%%
%%                                                         ARMOISE description
%%
\section{ARMOISE description}

\begin{bnf}
\NT{armoise-description} & \NT{definition-or-predicate-list} \\
\eline
\NT{definition-or-predicate-list} 
    & \NT{definition-or-predicate} \NT{;} \NT{definition-or-predicate-list} \\
    & \NT{definition-or-predicate} \T{;} \\
\eline
\NT{definition-or-predicate} & \NT{predicate} \\
                             & \NT{predicate-definition} \\
\end{bnf}

\begin{tabular}{llcll}
\treenode{desc}{description} & \rground{rg}\\
\\
& \treenode{pred}{predicate \rpr{section:predicates}}  & & 
 \treenode{pred-e}{predicate} & \rdots{pred-e-n} \\
& \multicolumn{1}{r}{\rdots{pred-c}} & & 
   \multicolumn{1}{r}{\rdots{pred-e-c}} \\
& & \rdots{middle}  \\
& \treenode{preddef}{definition \rpr{section:preddef}} & &
  \treenode{preddef-e}{definition} & \rdots{preddef-e-n} \\
& \multicolumn{1}{r}{\rdots{preddef-c}} & & 
   \multicolumn{1}{r}{\rdots{preddef-e-c}} \\
\end{tabular} 
\lnkRL{desc-next}{rg}
\lnkBL{desc-child}{pred}
\lnkBL{desc-child}{preddef}
\lnkBL{pred-child}{pred-c}
\lnkRL{pred-next}{middle}
\lnkBL{preddef-child}{preddef-c}
\lnkRL{preddef-next}{middle}
\lnkRL{middle}{preddef-e}
\lnkRL{middle}{pred-e}

\lnkBL{pred-e-child}{pred-e-c}
\lnkRL{pred-e-next}{pred-e-n}
\lnkBL{preddef-e-child}{preddef-e-c}
\lnkRL{preddef-e-next}{preddef-e-n}


\section{Predicate definition}
\label{section:preddef}
\begin{bnf}
\NT{predicate-definition} & \NT{identifier-or-tuple} \T{:=} \NT{predicate} \\
\eline
\NT{identifier-or-tuple} & \NT{tuple-of-identifiers} \\
                         & \NT{identifier} \\
\eline
\NT{tuple-of-identifiers} & \T{<} \NT{list-of-tuples} \T{>} \\
\eline
\NT{list-of-tuples} & \NT{identifier-or-tuple} \T{,} \NT{list-of-tuples} \\
                    & \NT{identifier-or-tuple} \\
\eline
\NT{identifier} & {\em RegExp(}\verb+[a-zA-Z_][a-zA-Z_0-9]*+{\em )}\\
\end{bnf}

\section{Predicates}
\label{section:predicates}
 \subsection {Predicates with a local context}

\begin{bnf}
\NT{predicate} 
  & \NT{predicate-without-local-context} \\
  & \NT{local-context} \T{<} 
    \NT{list-of-predicate-without-local-context} \T{>} \\
  & \NT{local-context} \NT{predicate-without-local-context} \\
\eline
\NT{local-context} & \T{let} \NT{list-of-predicate-definitions} \T{in} \\
\eline
\NT{list-of-predicate-definitions} 
  & \NT{predicate-definition} \T{;} \NT{list-of-predicate-definitions} \\
  & \NT{predicate-definition} \T{;} \\
\\
\NT{list-of-predicate-without-local-context} & 
    \NT{predicate-without-local-context} \T{,} 
    \NT{list-of-predicate-without-local-context}  \\
  & \NT{predicate-without-local-context} \\
\end{bnf}

 \subsection {Predicates without local context}

\begin{bnf}
\NT{predicate-without-local-context} & \NT{boolean-combination-of-sets} \\
\eline
\NT{boolean-combination-of-sets} 
  & \NT{boolean-combination-of-sets} \verb+||+ \NT{intersection-of-sets} \\
  & \NT{boolean-combination-of-sets} \verb+\+ \NT{intersection-of-sets}\\
  & \NT{boolean-combination-of-sets} \verb+^+ \NT{intersection-of-sets}\\
  & \NT{intersection-of-sets}\\
\eline
\NT{intersection-of-sets} 
  & \NT{intersection-of-sets} \T{\&\&} \NT{complement-of-sets} \\
  & \NT{complement-of-sets} \\
\eline
\NT{complement-of-sets} & \T{!} \NT{complement-of-sets} \\
                        & \NT{addition-on-sets} \\
\end{bnf}

\begin{bnf}
\NT{addition-on-sets} 
  & \NT{addition-on-sets} \T{+} \NT{scalar-product-on-sets} \\
  & \NT{addition-on-sets} \T{-} \NT{scalar-product-on-sets} \\
  & \NT{scalar-product-on-sets} \\
\eline
\NT{scalar-product-on-sets} 
  & \NT{scalar-product-on-sets} \T{*} \NT{unary-sets} \\
  & \NT{scalar-product-on-sets} \T{/} \NT{unary-sets} \\
  & \NT{unary-sets} \\
\eline
\NT{unary-sets} & \T{-} \NT{unary-sets}  \\
                & \NT{atomic-sets} \\
\eline
\NT{atomic-sets} & \NT{predefined-sets} \\
                 & \NT{integer} \\
                 & \NT{cartesian-product} \\
                 & \T{(} \NT{predicate-without-local-context} \T{)} \\
                 & \NT{simple-set} \\
                 & \NT{list-element} \\
\end{bnf}
\begin{bnf}
\NT{integer} & {\em RegExp(}\verb+[1-9][0-9]*)|0+{\em )} \\
\eline
\NT{list-element} & \NT{identifier} \\
                  & \NT{list-element} \T{[} integer \T{]} \\
\eline
\NT{predefined-sets} & \T{nat} \\
                     & \T{int} \\
                     & \T{posi} \\
                     & \T{real} \\
\end{bnf}
\begin{bnf}
\NT{cartesian-product} & \T{(} \NT{list-of-boolean-combinations} \T{)} \\
\eline
\NT{list-of-boolean-combinations} 
  & \NT{boolean-combination-of-sets} \T{,} \NT{list-of-boolean-combinations} \\
  & \NT{boolean-combination-of-sets} \T{,} \NT{boolean-combination-of-sets} \\
\end{bnf}
\begin{bnf}
\NT{simple-set} & \T{\{} \NT{list-of-terms} \T{\}}  \\
                & \T{\{} \NT{vector-of-terms} \verb+|+ \NT{formula} \T{\}} \\
                & \T{\{} \NT{identifier} \verb+|+ \NT{formula} \T{\}} \\
\end{bnf}
\begin{bnf}
\NT{list-of-terms} & \NT{addition-term} \T{,} \NT{list-of-terms} \\
                   & \NT{addition-term} \\
\end{bnf}
\begin{bnf}
\NT{vector-of-terms} & \T{(} \NT{list-of-terms} \T{)} \\
\end{bnf}

\subsection{Formulas over $1^{\mbox{st}}$ order variables}
\begin{bnf}
\NT{formula} & \NT{disjunction} \\
\end{bnf}
\begin{bnf}
\NT{disjunction} 
   & \NT{conjunction} \T{|} \NT{disjunction} \\
   & \NT{conjunction} \T{or} \NT{disjunction} \\
   & \NT{conjunction} \\
\end{bnf}
\begin{bnf}
\NT{conjunction}
   & \NT{logical-comparison} \T{\&} \NT{conjunction} \\
   & \NT{logical-comparison} \T{and} \NT{conjunction}  \\
   & \NT{logical-comparison} \\
\end{bnf}
\begin{bnf}
\NT{logical-comparison} 
   & \NT{atomic-formula} \T{<=>} \NT{logical-comparison} \\
   & \NT{atomic-formula} \T{=>} \NT{logical-comparison} \\
   & \NT{atomic-formula} \\
\end{bnf}
\begin{bnf}
\NT{atomic-formula}
   & \NT{term-comparison} \\
   & \NT{quantified-formula} \\
   & \T{!} \NT{atomic-formula} \\
   & \T{not} \NT{atomic-formula} \\
   & \NT{addition-term} \T{in} \NT{predicate-without-local-context} \\
   & \T{(} \NT{atomic-formula} \T{)} \\
   & \NT{identifier} \NT{vector-of-terms} \\
\end{bnf}
\begin{bnf}
\NT{term-comparison} 
   & \NT{addition-term} \T{=} \NT{addition-term} \\
   & \NT{addition-term} \T{!=} \NT{addition-term} \\
   & \NT{addition-term} \T{<} \NT{addition-term} \\
   & \NT{addition-term} \T{>} \NT{addition-term} \\
   & \NT{addition-term} \T{<=} \NT{addition-term} \\
   & \NT{addition-term} \T{>=} \NT{addition-term} \\
\end{bnf}
\begin{bnf}
\NT{quantified-formula}
   & \T{exists} \NT{list-of-identifiers} \NT{atomic-formula} \\
   & \T{forall} \NT{list-of-identifiers} \NT{atomic-formula} \\
\end{bnf}
\begin{bnf}
\NT{list-of-identifiers} 
   & \NT{identifier} \T{,} \NT{list-of-identifiers}  \\
   & \NT{identifier} \\
\end{bnf}
\begin{bnf}
\NT{addition-term} 
   & \NT{addition-term} \T{+} \NT{multiplication-term} \\
   & \NT{addition-term} \T{-} \NT{multiplication-term} \\
   & \NT{multiplication-term} \\
\end{bnf}
\begin{bnf}
\NT{multiplication-term} 
   & \NT{multiplication-term} \T{*} \NT{unary-term} \\
   & \NT{multiplication-term} \T{/} \NT{unary-term} \\
   & \NT{multiplication-term} \T{\%} \NT{unary-term} \\
   & \NT{unary-term} \\
\end{bnf}
\begin{bnf}
\NT{unary-term} & \T{-} \NT{unary-term} \\
           & \NT{atomic-term} \\
\eline
\NT{atomic-term} & \NT{integer} \\
                 & \NT{vector-of-terms} \\
                 & \NT{identifier} \\
\end{bnf}
\end{document}
