/*
 * vec.h -- Vectors of rational numbers
 * 
 * This file is a part of the Talence Linear Algebra Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __VEC_H__
# define __VEC_H__

# include <ccl/ccl-assert.h>
# include <talalib/common.h>

BEGIN_C_DECLS

typedef struct tla_vec_st {
  int size;
  mpz_t *num;
  mpz_t den;
} tla_vec_t[1];

/*
 * Accessor to the _i th component of the vector. _i must belong to the 
 * range [1, n] where n is the size of the vector.
 */ 
# define TLA_V_NUM(_V, _i)						\
  (ccl_assert (1 <= (_i) && (_i) <= (_V)->size), (_V)->num[(_i) - 1])

# define TLA_V_DEN(_V) ((_V)->den)

extern void
tla_vec_init (tla_vec_t v, int size);
extern void
tla_vec_resize (tla_vec_t v, int size);
extern void
tla_vec_init_set (tla_vec_t v, const tla_vec_t other);
extern void
tla_vec_init_neg (tla_vec_t v, const tla_vec_t other);
extern void
tla_vec_clear (tla_vec_t v);
extern void
tla_vec_set_q (tla_vec_t v, mpq_t *vec);
extern void
tla_vec_set (tla_vec_t v, const tla_vec_t x);
extern void
tla_vec_get (mpq_t *vec, const tla_vec_t v);
extern void
tla_vec_get_num (tla_vec_t res, const tla_vec_t v);
extern void
tla_vec_swap_at (tla_vec_t v, int i, int j);
extern void
tla_vec_set_unity (tla_vec_t v, int i);
extern void
tla_vec_set_zero (tla_vec_t v);
extern void
tla_vec_canonicalize (tla_vec_t v);
extern int
tla_vec_get_size (const tla_vec_t v);
extern int
tla_vec_is_zero (const tla_vec_t v);
extern int
tla_vec_are_equal (const tla_vec_t v1, const tla_vec_t v2);
extern void
tla_vec_scalar_product (mpq_t res, const tla_vec_t v1, const tla_vec_t v2);
extern void
tla_vec_mul_2exp (tla_vec_t res, const int exp2, const tla_vec_t v);
extern void
tla_vec_q_mul (tla_vec_t res, mpq_t scale, const tla_vec_t v);
extern void
tla_vec_z_mul (tla_vec_t res, const mpz_t scale, const tla_vec_t v);
extern void
tla_vec_z_mul_si (tla_vec_t res, int scale, const tla_vec_t v);
extern void
tla_vec_qz_mul (tla_vec_t res, const tla_vec_t v, const mpz_t num, 
		const mpz_t den);
extern void
tla_vec_q_div (tla_vec_t res, const tla_vec_t v, mpq_t d);
extern void
tla_vec_z_div (tla_vec_t res, const tla_vec_t v, const mpz_t d);
extern void
tla_vec_z_div_si (tla_vec_t res, const tla_vec_t v, int d);
extern void
tla_vec_qz_div (tla_vec_t res, const tla_vec_t v, const mpz_t num, 
	       const mpz_t den);
extern void
tla_vec_add (tla_vec_t res, const tla_vec_t v1, const tla_vec_t v2);
extern void
tla_vec_add_z_mul (tla_vec_t res, const tla_vec_t x, const mpz_t a, 
		  const tla_vec_t y);
extern void
tla_vec_sub_z_mul (tla_vec_t res, const tla_vec_t x, const mpz_t a, 
		  const tla_vec_t y);
extern void
tla_vec_add_si_mul (tla_vec_t res, const tla_vec_t x, int a, const tla_vec_t y);
extern void
tla_vec_sub (tla_vec_t res, const tla_vec_t v1, const tla_vec_t v2);
extern void
tla_vec_gamma (tla_vec_t res, const tla_vec_t v, int b);
extern void
tla_vec_inv_gamma (tla_vec_t res, const tla_vec_t v, int b);
extern void
tla_vec_sub_ith_den (tla_vec_t res, const tla_vec_t v, int i);
extern void
tla_vec_div_2 (tla_vec_t res, const tla_vec_t v);
extern void
tla_vec_sub_last_den_div_2 (tla_vec_t res, const tla_vec_t v);
extern void
tla_vec_ith_div (tla_vec_t res, int i);
extern void
tla_vec_sub_ith_mul (tla_vec_t res, tla_vec_t x, int i, tla_vec_t y);
extern void
tla_vec_assign_sub_ith_mul (tla_vec_t res, int i0, tla_vec_t y);
extern unsigned int
tla_vec_hash (const tla_vec_t v);

END_C_DECLS

#endif /* ! __VEC_H__ */
