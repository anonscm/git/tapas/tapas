/*
 * matrix.c -- add a comment about this file
 * 
 * This file is a part of the Talence Linear Algebra Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include "matrix.h"
#include "talalib-p.h"

void
tla_matrix_init (tla_matrix_t M, int m, int n)
{
  M->m = m;
  M->n = n;
  tla_vec_init (M->coefs, m * n);
}

			/* --------------- */

void
tla_matrix_init_set (tla_matrix_t M, const tla_matrix_t N)
{
  M->m = N->m;
  M->n = N->n;
  tla_vec_init_set (M->coefs, N->coefs);
}

			/* --------------- */

void
tla_matrix_init_with_vec (tla_matrix_t M, const tla_vec_t v, int iscol)
{
  if (iscol)
    {
      M->m = tla_vec_get_size (v);
      M->n = 1;
    }
  else
    {
      M->m = 1;
      M->n = tla_vec_get_size (v);
    }
  tla_vec_init_set (M->coefs, v);
}

			/* --------------- */

void
tla_matrix_remove_last_column (tla_matrix_t R, tla_matrix_t M)
{  
  int i, j;

  ccl_pre (tla_matrix_get_nb_lines (R) == tla_matrix_get_nb_lines (M));
  ccl_pre (tla_matrix_get_nb_columns (R) == tla_matrix_get_nb_columns (M) - 1);
  tla_matrix_set_zero (R);
  for (i = 1; i <= tla_matrix_get_nb_lines (R); i++)
    {
      for (j = 1; j < tla_matrix_get_nb_columns (R); j++)
	{
	  mpz_set (MM (R, i, j), MM (M, i, j));
	}
    }
}

			/* --------------- */

void
tla_matrix_resize (tla_matrix_t M, int m, int n)
{
  if (M->m == m && M->n == n)
    tla_matrix_set_zero (M);
  else
    {
      M->m = m;
      M->n = n;
      tla_vec_resize (M->coefs, m * n);
    }
}

			/* --------------- */

void
tla_matrix_clear (tla_matrix_t M)
{
  tla_vec_clear (M->coefs);
}

			/* --------------- */

void
tla_matrix_canonicalize (tla_matrix_t M)
{
  tla_vec_canonicalize (M->coefs);
}

			/* --------------- */

void
tla_matrix_get_line (tla_matrix_t M, int i, tla_vec_t R)
{
  int j;

  ccl_pre (tla_vec_get_size (R) == tla_matrix_get_nb_columns (M));

  mpz_set (R->den, M->coefs->den);
  for (j = 0; j < M->n; j++)
    mpz_set (R->num[j], MM (M, i, j + 1));

  tla_vec_canonicalize (R);
}

			/* --------------- */

void
tla_matrix_get_column (tla_matrix_t M, int j, tla_vec_t R)
{
  int i;

  ccl_pre (tla_vec_get_size (R) == tla_matrix_get_nb_lines (M));

  mpz_set (R->den, M->coefs->den);
  for (i = 0; i < M->m; i++)
    mpz_set (R->num[i], MM (M, i + 1, j));

  tla_vec_canonicalize (R);
}

			/* --------------- */

/*
 * \pre v->size == n && R->size == m
 */
void 
tla_matrix_mul_vector (tla_vec_t R, const tla_matrix_t M, const tla_vec_t v)
{
#if 0
  int i, j;

  ccl_pre (v->size >= M->n && R->size >= M->m);

  for (i = 0; i < M->m; i++)
    {
      mpz_set_ui (R->num[i], 0);
      for (j = 0; j < M->n; j++)
	mpz_addmul (R->num[i], MM (M, i + 1, j + 1), v->num[j]);
    }

  mpz_mul (R->den, M->coefs->den, v->den);
#else
  tla_matrix_t tmp, r;
  tmp->m = tla_vec_get_size (v);
  tmp->n = 1;
  tmp->coefs[0] = v[0];
  tla_matrix_init (r, tla_vec_get_size (v), 1);
  tla_matrix_mul_matrix (r, M, tmp);
  tla_vec_set (R, r->coefs);
  tla_matrix_clear (r);
#endif
}

			/* --------------- */

void 
tla_matrix_mul_matrix (tla_matrix_t R, const tla_matrix_t M, 
		       const tla_matrix_t N)
{
  int i;
  tla_matrix_t tmp;
  struct tla_matrix_st *tmp_p;

  ccl_pre (tla_matrix_get_nb_columns (M) <= tla_matrix_get_nb_lines (N));
  ccl_pre (tla_matrix_get_nb_lines (R) >= tla_matrix_get_nb_lines (M));
  ccl_pre (tla_matrix_get_nb_columns (R) >= tla_matrix_get_nb_columns (N));

  if (R == M || R == N)
    {
      tla_matrix_init (tmp, tla_matrix_get_nb_lines (R), 
		       tla_matrix_get_nb_columns (R));
      tmp_p = tmp;
    }
  else
    tmp_p = R;
      
  for (i = 1; i <= tla_matrix_get_nb_lines (M); i++)
    {
      int j;

      for (j = 1; j <= tla_matrix_get_nb_columns (N); j++)
	{
	  int k;

	  mpz_set_ui (MM (tmp_p, i, j), 0);
	  for (k = 1; k <= tla_matrix_get_nb_columns (M); k++)
	    {
	      mpz_addmul (MM (tmp_p, i, j), MM (M, i, k), MM (N, k, j));
	    }
	}
    }

  mpz_mul (tmp_p->coefs->den, M->coefs->den, N->coefs->den);  

  if (R == M || R == N)
    tla_matrix_set (R, tmp);
}

			/* --------------- */

void
tla_matrix_set (tla_matrix_t M, const tla_matrix_t N)
{
  ccl_pre (M->m == N->m); 
  ccl_pre (M->n == N->n); 

  tla_vec_set (M->coefs, N->coefs);
}

			/* --------------- */

void
tla_matrix_z_mul (tla_matrix_t R, const mpz_t z, const tla_matrix_t M)
{
  tla_vec_z_mul (R->coefs, z, M->coefs);
}

			/* --------------- */

void
tla_matrix_set_identity (tla_matrix_t M)
{
  int i, max = M->m < M->n ? M->m : M->n;

  tla_matrix_set_zero (M);

  for (i = 1; i <= max; i++)
    mpz_set_ui (MM (M, i, i), 1);
}

			/* --------------- */

void
tla_matrix_set_zero (tla_matrix_t M)
{
  tla_vec_set_zero (M->coefs);
}

			/* --------------- */

int
tla_matrix_are_equal (const tla_matrix_t M1, const tla_matrix_t M2)
{
  return (M1->m == M2->m && M1->n == M2->n && 
	  tla_vec_are_equal (M1->coefs, M2->coefs));
}

			/* --------------- */

unsigned int
tla_matrix_hash (const tla_matrix_t M)
{
  return 13 * M->m + 19 * M->n + 17 * tla_vec_hash (M->coefs);
}

			/* --------------- */

// Return 1 if there exists an integral vector $x$ such that $M x = v$.
// In this case x is such a vector 
int
tla_matrix_Z_solution(const tla_matrix_t M,   tla_vec_t x, const tla_vec_t v)
{
  int result = 0;
  int m = tla_matrix_get_nb_lines (M);
  int n  = tla_matrix_get_nb_columns (M);
  
  ccl_pre (tla_matrix_get_nb_columns (M) == tla_vec_get_size (x));
  ccl_pre (tla_matrix_get_nb_lines (M) == tla_vec_get_size (v));
  
  tla_matrix_t V,H;
  tla_matrix_init (V, n, n);
  tla_matrix_init (H, m, n);
  tla_vec_t z;
  tla_vec_init_set(z,v);

  int dim;
  
  tla_matrix_compute_hnf (M, H, &dim , NULL, V);
  ccl_assert (dim == m);
  
  int i,j;
  
  /* We try to find w such that Hw = z*/
  /* We first multiply H and z to obtain integrals rather than rationals */
  mpz_t denH;
  mpz_init(denH);
  mpz_set(denH, H->coefs->den);
  mpz_t denz;
  mpz_init(denz);
  mpz_set(denz,z->den);
  mpz_set_ui(H->coefs->den,1);
  mpz_set_ui(z->den,1);
  tla_matrix_z_mul(H,denz,H);
  tla_vec_z_mul(z,denH,z);
  
  mpz_t aux1, aux2, q, r;
  tla_vec_t w;
  mpz_init (aux1);
  mpz_init (aux2);
  mpz_init (q);
  mpz_init (r);
  tla_vec_init (w, n);

  for (j = 1 ; j <= dim ; j++)
    {
      mpz_fdiv_qr (q, r, z->num[j - 1], MM (H, j, j));
      if (mpz_cmp_ui (r , 0) == 0) 
	{
	  mpz_set (w->num[j-1], q);
	  /* We must remove to z : q times the j column of H */
	  for (i = j ; i <= m ; i++)
	    {
	      mpz_mul (aux1, q, MM (H,i,j));
	      mpz_sub (aux2, z->num[i-1], aux1);
	      mpz_set (z->num[i-1], aux2);
	    }
	}
      else
	{
	  goto end;
	}
    }

  /* Check that z is zero */  
  if (! tla_vec_is_zero (z))
    goto end;
  
  /* Now x exists and x=V w is ok ! */
  tla_matrix_mul_vector (x, V, w);
  result = 1;

 end:
  tla_vec_clear(w);
  mpz_clear(r);
  mpz_clear(q);
  mpz_clear(aux2);
  mpz_clear(aux1);
  mpz_clear(denH);
  mpz_clear(denz);
  tla_vec_clear(z);
  tla_matrix_clear(H);
  tla_matrix_clear(V);

  return result;
}

			/* --------------- */

void
tla_matrix_swap_lines (tla_matrix_t A, int i1, int i2)
{
  int j;

  for (j = 1; j <= A->n; j++)
    mpz_swap (MM (A, i1, j), MM (A, i2, j));
}

			/* --------------- */

void
tla_matrix_swap_columns (tla_matrix_t A, int j1, int j2)
{
  int i;

  for (i = 1; i <= A->m; i++)
    mpz_swap (MM (A, i, j1), MM (A, i, j2));
}


			/* --------------- */

void
tla_matrix_neg_line (tla_matrix_t A, int i)
{
  int j;

  for (j = 1; j <= A->n; j++)
    mpz_neg (MM (A, i, j), MM (A, i, j));
}

			/* --------------- */

void
tla_matrix_neg_column (tla_matrix_t A, int j)
{
  int i;

  for (i = 1; i <= A->m; i++)
    mpz_neg (MM (A, i, j), MM (A, i, j));
}

			/* --------------- */

void
tla_matrix_submul_column (tla_matrix_t A, int j, int jpivot, mpz_t x)
{
  int i;

  ccl_assert (j != jpivot);

  for (i = 1; i <= A->m; i++)
    mpz_submul (MM (A, i, j), x, MM (A, i, jpivot));
}

			/* --------------- */

void
tla_matrix_sub_column (tla_matrix_t A, int j, int jpivot)
{
  int i;

  ccl_assert (j != jpivot);

  for (i = 1; i <= A->m; i++)
    mpz_sub (MM (A, i, j), MM (A, i, j),MM (A, i, jpivot));
}

			/* --------------- */

void
tla_matrix_addmul_column (tla_matrix_t A, int j, int jpivot, mpz_t x)
{
  int i;

  ccl_assert (j != jpivot);

  for (i = 1; i <= A->m; i++)
    mpz_addmul (MM (A, i, j), x, MM (A, i, jpivot));
}

			/* --------------- */

void
tla_matrix_submul_line (tla_matrix_t A, int i, int ipivot, mpz_t x)
{
  int j;

  ccl_assert (i != ipivot);

  for (j = 1; j <= A->n; j++)
    mpz_submul (MM (A, i, j), x, MM (A, ipivot, j));
}

			/* --------------- */

void
tla_matrix_addmul_line (tla_matrix_t A, int i, int ipivot, mpz_t x)
{
  int j;

  ccl_assert (i != ipivot);

  for (j = 1; j <= A->n; j++)
    mpz_addmul (MM (A, i, j), x, MM (A, ipivot, j));
}

			/* --------------- */

void
tla_matrix_add_line (tla_matrix_t A, int i, int ipivot)
{
  int j;

  ccl_assert (i != ipivot);

  for (j = 1; j <= A->n; j++)
    mpz_add (MM (A, i, j), MM (A, i, j), MM (A, ipivot, j));
}

/* return the column number j>=jmin of the least absolute coefficient H[i,j] */
/* return 0 if H[i,j]=0 for any j>=jmin */
int
tla_matrix_least_in_line (tla_matrix_t H, int i, int jmin)
{
  int j, pos = 0;
  mpz_t least;
  
  ccl_assert (jmin <= H->n);
  
  for (j = H->n; j > jmin; j--)
    {
      if (mpz_cmp_ui (MM (H, i, j), 0) == 0)
	continue;

      if (pos)
	{
	  if (mpz_cmpabs (MM (H, i, j), least) <= 0)
	    {
	      pos = j;
	      mpz_set (least, MM (H, i, j));
	    }
	}
      else
	{
	  pos = j;
	  mpz_init_set (least, MM (H, i, j));
	}
    }

  if (pos)
    mpz_clear (least);

  return pos;
}

			/* --------------- */


int
tla_matrix_is_identity (tla_matrix_t M)
{
  int i, j , result = 1;

  for (i = 1; i <= M->m && result; i++)
    {
      for (j = 1; j <= M->n && result; j++)
	{
	  if (i == j) 
	    result = (mpz_cmp_si (MM (M,i,j), 1) == 0);
	  else
	    result = (mpz_cmp_si (MM (M,i,j), 0) == 0);
	}
    }

  return result;
}

			/* --------------- */

