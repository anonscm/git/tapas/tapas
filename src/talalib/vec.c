/*
 * vec.c -- Vectors of rational numbers
 * 
 * This file is a part of the Talence Linear Algebra Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "vec.h"

			/* --------------- */

void
tla_vec_init (tla_vec_t v, int size)
{
  int i;

  ccl_pre (size > 0);

  v->size = size;
  v->num = ccl_new_array (mpz_t, v->size);
  for (i = 0; i < v->size; i++)
    mpz_init (v->num[i]);
  mpz_init_set_ui (v->den, 1);
}

			/* --------------- */

void
tla_vec_resize (tla_vec_t v, int size)
{
  ccl_pre (size > 0);

  if (v->size == size)
    tla_vec_set_zero (v);
  else
    {
      tla_vec_clear (v);
      tla_vec_init (v, size);
    }
}

			/* --------------- */

void
tla_vec_init_set (tla_vec_t v, const tla_vec_t other)
{
  int i;

  v->size = other->size;
  v->num = ccl_new_array (mpz_t, v->size);
  for (i = 0; i < v->size; i++)
    mpz_init_set (v->num[i], other->num[i]);
  mpz_init_set (v->den, other->den);
}

			/* --------------- */

void
tla_vec_init_neg (tla_vec_t v, const tla_vec_t other)
{
  int i;

  v->size = other->size;
  v->num = ccl_new_array (mpz_t, v->size);
  for (i = 0; i < v->size; i++)
    {
      mpz_init (v->num[i]);
      mpz_neg (v->num[i], other->num[i]);
    }
  mpz_init_set (v->den, other->den);
}

			/* --------------- */

void
tla_vec_clear (tla_vec_t v)
{
  int i;

  for (i = 0; i < v->size; i++)
    mpz_clear (v->num[i]);
  ccl_delete (v->num);
  mpz_clear (v->den);
}

			/* --------------- */

void
tla_vec_set_q (tla_vec_t v, mpq_t * vec)
{
  int i;
  mpz_t aux1, aux2, lcm;
  mpq_t *cvec;

  mpz_init (aux1);
  mpz_init (aux2);
  mpz_init (lcm);

  cvec = ccl_new_array (mpq_t, v->size);

  for (i = 0; i < v->size; i++)
    {
      mpq_init (cvec[i]);
      mpq_set (cvec[i], vec[i]);
      mpq_canonicalize (cvec[i]);
      mpq_get_den (aux1, cvec[i]);

      if (i == 0)
	mpz_set (lcm, aux1);
      else
	{
	  mpz_lcm (aux2, lcm, aux1);
	  mpz_set (lcm, aux2);
	}
      mpq_get_num (v->num[i], cvec[i]);
    }

  for (i = 0; i < v->size; i++)
    {
      mpz_mul (aux1, v->num[i], lcm);
      mpq_get_den (aux2, cvec[i]);
      mpz_div (v->num[i], aux1, aux2);
      mpq_clear (cvec[i]);
    }
  ccl_delete (cvec);

  mpz_clear (aux1);
  mpz_clear (aux2);

  mpz_set (v->den, lcm);
  mpz_clear (lcm);
}

			/* --------------- */

void
tla_vec_set (tla_vec_t v, const tla_vec_t x)
{
  int i;

  for (i = 0; i < v->size; i++)
    mpz_set (v->num[i], x->num[i]);
  mpz_set (v->den, x->den);
}

			/* --------------- */

void
tla_vec_get (mpq_t * vec, const tla_vec_t v)
{
  int i;

  for (i = 0; i < v->size; i++)
    {
      mpq_set_num (vec[i], v->num[i]);
      mpq_set_den (vec[i], v->den);
    }
}

			/* --------------- */

void
tla_vec_get_num (tla_vec_t res, const tla_vec_t v)
{
  int i;

  for (i = 0; i < res->size; i++)
    mpz_set (res->num[i], v->num[i]);
  mpz_set_ui (res->den, 1);
}

			/* --------------- */

void
tla_vec_swap_at (tla_vec_t v, int i, int j)
{
  ccl_pre (0 <= i && i < v->size);
  ccl_pre (0 <= j && j < v->size);

  if (i == j)
    return;

  mpz_swap (v->num[i], v->num[j]);
}

			/* --------------- */

void
tla_vec_set_unity (tla_vec_t v, int i)
{
  int k;

  ccl_pre (0 <= i && i < v->size);

  for (k = 0; k < v->size; k++)
    {
      if (k == i)
	mpz_set_ui (v->num[k], 1);
      else
	mpz_set_ui (v->num[k], 0);
    }

  mpz_set_ui (v->den, 1);
}

			/* --------------- */

void
tla_vec_set_zero (tla_vec_t v)
{
  int k;

  for (k = 0; k < v->size; k++)
    mpz_set_ui (v->num[k], 0);
  mpz_set_ui (v->den, 1);
}

			/* --------------- */

void
tla_vec_canonicalize (tla_vec_t v)
{
  int i;
  mpq_t *qs = ccl_new_array (mpq_t, v->size);

  for (i = 0; i < v->size; i++)
    {
      mpq_init (qs[i]);
      mpq_set_num (qs[i], v->num[i]);
      mpq_set_den (qs[i], v->den);
      mpq_canonicalize (qs[i]);
    }
  tla_vec_set_q (v, qs);

  for (i = 0; i < v->size; i++)
    mpq_clear (qs[i]);

  ccl_delete (qs);
}

			/* --------------- */

int
tla_vec_get_size (const tla_vec_t v)
{
  return v->size;
}

			/* --------------- */

int
tla_vec_is_zero (const tla_vec_t v)
{
  int i;

  for (i = 0; i < v->size && mpz_cmp_si (v->num[i], 0) == 0; i++)
    /* nop */ ;

  return i == v->size;
}

			/* --------------- */

int
tla_vec_are_equal (const tla_vec_t v1, const tla_vec_t v2)
{
  int result = 0;

  if (tla_vec_get_size (v1) == tla_vec_get_size (v2))
    {
      int i;

      result = 1;

      if (mpz_cmp (v1->den, v2->den) == 0)
	{
	  for (i = 0; result && i < v1->size; i++)
	    result = (mpz_cmp (v1->num[i], v2->num[i]) == 0);
	}
      else
	{
	  mpq_t aux1, aux2;

	  mpq_init (aux1);
	  mpq_init (aux2);
	  for (i = 0; result && i < v1->size; i++)
	    {
	      mpq_set_num (aux1, v1->num[i]);
	      mpq_set_den (aux1, v1->den);
	      mpq_canonicalize (aux1);
	      mpq_set_num (aux2, v2->num[i]);
	      mpq_set_den (aux2, v2->den);
	      mpq_canonicalize (aux2);
	      result = (mpq_cmp (aux1, aux2) == 0);
	    }
	  mpq_clear (aux1);
	  mpq_clear (aux2);
	}
    }

  return result;
}

			/* --------------- */

void
tla_vec_scalar_product (mpq_t res, const tla_vec_t v1, const tla_vec_t v2)
{
  int i;
  mpz_t num, den, aux1, aux2;

  ccl_pre (tla_vec_get_size (v1) == tla_vec_get_size (v2));

  mpz_init (num);
  mpz_init (den);
  mpz_init (aux1);
  mpz_init (aux2);

  for (i = 0; i < v1->size; i++)
    {
      mpz_mul (aux1, v1->num[i], v2->num[i]);
      mpz_add (aux2, num, aux1);
      mpz_set (num, aux2);
    }

  mpz_clear (aux1);
  mpz_clear (aux2);
  mpq_set_num (res, num);
  mpz_clear (num);

  mpz_mul (den, v1->den, v2->den);
  mpq_set_den (res, den);
  mpz_clear (den);
}

			/* --------------- */

void
tla_vec_mul_2exp (tla_vec_t res, const int exp2, const tla_vec_t v)
{
  int i;

  for (i = 0; i < v->size; i++)
    mpz_mul_2exp (res->num[i], v->num[i], exp2);

  mpz_set (res->den, v->den);
}

			/* --------------- */

void
tla_vec_q_mul (tla_vec_t res, mpq_t s, const tla_vec_t v)
{
  mpz_t num, den;

  mpz_init (num);
  mpz_init (den);
  mpq_canonicalize (s);
  mpq_get_num (num, s);
  mpq_get_den (den, s);
  tla_vec_qz_mul (res, v, num, den);
  mpz_clear (num);
  mpz_clear (den);
}

			/* --------------- */

void
tla_vec_z_mul (tla_vec_t res, const mpz_t s, const tla_vec_t v)
{
  int i;

  for (i = 0; i < v->size; i++)
    mpz_mul (res->num[i], v->num[i], s);
  if (res != v)
    mpz_set (res->den, v->den);
}

			/* --------------- */

void
tla_vec_z_mul_si(tla_vec_t res, int scale, const tla_vec_t v)
{
  int i;

  for (i = 0; i < v->size; i++)
    mpz_mul_si (res->num[i], v->num[i], scale);

  if (res != v)
    mpz_set (res->den, v->den);
}

			/* --------------- */

void
tla_vec_qz_mul (tla_vec_t res, const tla_vec_t v, const mpz_t num,
		const mpz_t den)
{
  tla_vec_z_mul (res, num, v);
  mpz_mul (res->den, res->den, den);
}

			/* --------------- */

void
tla_vec_q_div (tla_vec_t res, const tla_vec_t v, mpq_t s)
{
  mpz_t num, den;

  mpz_init (num);
  mpz_init (den);
  mpq_canonicalize (s);
  mpq_get_num (num, s);
  mpq_get_den (den, s);
  tla_vec_qz_mul (res, v, num, den);
  mpz_clear (num);
  mpz_clear (den);
}

			/* --------------- */

void
tla_vec_z_div (tla_vec_t res, const tla_vec_t v, const mpz_t s)
{
  if (res != v)
    tla_vec_set (res, v);
  mpz_mul (res->den, res->den, s);
}

			/* --------------- */

void
tla_vec_z_div_si (tla_vec_t res, const tla_vec_t v, int d)
{
  if (res != v)
    tla_vec_set (res, v);
  mpz_mul_si (res->den, res->den, d);
}



			/* --------------- */

void
tla_vec_assign_z_div_si (tla_vec_t res, int d)
{
  mpz_mul_si (res->den, res->den, d);
}



			/* --------------- */
void
tla_vec_qz_div (tla_vec_t res, const tla_vec_t v, const mpz_t num,
		const mpz_t den)
{
  tla_vec_qz_mul (res, v, den, num);
}

			/* --------------- */

void
tla_vec_add (tla_vec_t res, const tla_vec_t v1, const tla_vec_t v2)
{
  int i;
  mpz_t tmp;

  ccl_pre (tla_vec_get_size (res) >= tla_vec_get_size (v1));
  ccl_pre (tla_vec_get_size (v1) == tla_vec_get_size (v2));

  mpz_init (tmp);
  for (i = 0; i < v1->size; i++)
    {      
      mpz_mul (tmp, v1->num[i], v2->den);
      mpz_addmul (tmp, v2->num[i], v1->den);
      mpz_set (res->num[i], tmp);
    }
  mpz_mul (res->den, v1->den, v2->den);
  mpz_clear (tmp);
}

			/* --------------- */
void
tla_vec_add_z_mul(tla_vec_t res, const tla_vec_t x, const mpz_t a, 
		  const tla_vec_t y)
{
  if (res != x)
    tla_vec_set (res, x);

  if (mpz_cmp_ui (a, 0) != 0)
    {
      int i;
      mpz_t a_dy;

      mpz_init (a_dy);
      mpz_mul (a_dy, a, res->den);
      
      for (i = 0; i < res->size; i++)
	{
	  if (mpz_cmp_ui (y->den, 1) != 0)
	    mpz_mul (res->num[i], res->num[i], y->den);
	  mpz_addmul (res->num[i], a_dy, y->num[i]);
	}
      
      mpz_mul (res->den, res->den, y->den);
      mpz_clear (a_dy);
    }
}

			/* --------------- */

void
tla_vec_sub_z_mul (tla_vec_t res, const tla_vec_t x, const mpz_t a, 
		   const tla_vec_t y)
{
  mpz_t tmp;

  mpz_init (tmp);
  mpz_neg (tmp, a);
  tla_vec_add_z_mul (res, x, tmp, y);
  mpz_clear (tmp);
}

			/* --------------- */

void
tla_vec_add_si_mul (tla_vec_t res, const tla_vec_t x, int a, const tla_vec_t y)
{
  if (a != 0)
    {
      mpz_t aux;

      mpz_init_set_si (aux, a);
      tla_vec_add_z_mul (res, x, aux, y);
      mpz_clear (aux);
    }
}

			/* --------------- */

void
tla_vec_sub (tla_vec_t res, const tla_vec_t v1, const tla_vec_t v2)
{
  int i;
  mpz_t tmp;

  ccl_pre (tla_vec_get_size (res) >= tla_vec_get_size (v2));
  ccl_pre (tla_vec_get_size (v1) == tla_vec_get_size (v2));

  mpz_init (tmp);

  for (i = 0; i < v1->size; i++)
    {
      mpz_mul (tmp, v1->num[i], v2->den);
      mpz_submul (tmp, v2->num[i], v1->den);
      mpz_set (res->num[i], tmp);
    }

  mpz_clear (tmp);

  mpz_mul (res->den, v1->den, v2->den);
}

			/* --------------- */

void
tla_vec_gamma (tla_vec_t res, const tla_vec_t v, int b)
{
  int i;
  mpz_t aux;

  mpz_init (aux);

  mpz_mul_2exp (aux, v->num[v->size - 1], 1);

  if (b)
    mpz_add (aux, aux, v->den);

  for (i = res->size-1 ; i >= 1; i--)
    mpz_set (res->num[i], v->num[i - 1]);

  mpz_set (res->num[0], aux);
  mpz_set (res->den, v->den);
  mpz_clear (aux);
}

			/* --------------- */

void
tla_vec_inv_gamma (tla_vec_t res, const tla_vec_t v, int b)
{
  int i;
  mpz_t aux;

  mpz_mul_2exp (res->den, v->den, 1);

  mpz_init (aux);
  if (b)
    mpz_sub (aux, v->num[0], v->den);
  else
    mpz_set (aux, v->num[0]);

  for (i = 0; i < res->size - 1; i++)
    mpz_mul_2exp (res->num[i], v->num[i + 1], 1);
  mpz_set (res->num[i], aux);

  tla_vec_canonicalize (res);
}

			/* --------------- */

void
tla_vec_sub_ith_den (tla_vec_t res, const tla_vec_t v, int i)
{
  int k;

  for (k = 0; k < i; k++)
    mpz_set (res->num[k], v->num[k]);

  mpz_sub (res->num[i], v->num[i], v->den);

  for (k = i + 1; k < res->size; k++)
    mpz_set (res->num[k], v->num[k]);

  mpz_set (res->den, v->den);
}

			/* --------------- */

void
tla_vec_div_2 (tla_vec_t res, const tla_vec_t v)
{
  int k;

  for (k = 0; k < res->size && mpz_even_p (v->num[k]); k++)
    /* empty */ ;

  if (k == res->size)
    {
      for (k = 0; k < res->size; k++)
	mpz_cdiv_q_2exp (res->num[k], v->num[k], 1);
      mpz_set (res->den, v->den);
    }
  else
    {
      for (k = 0; k < res->size; k++)
	mpz_set (res->num[k], v->num[k]);
      mpz_mul_2exp (res->den, v->den, 1);
    }
}


			/* --------------- */

void
tla_vec_sub_last_den_div_2 (tla_vec_t res, const tla_vec_t v)
{
  int k;

  for (k = 0; k < res->size && mpz_even_p (v->num[k]); k++)
    /* empty */ ;

  if (k == res->size && mpz_even_p (v->den))
    {
      for (k = 0; k < res->size - 1; k++)
	mpz_cdiv_q_2exp (res->num[k], v->num[k], 1);
      mpz_sub (res->num[k], v->num[k], v->den);
      mpz_cdiv_q_2exp (res->num[k], res->num[k], 1);
      mpz_set (res->den, v->den);
    }
  else
    {
      for (k = 0; k < res->size - 1; k++)
	mpz_set (res->num[k], v->num[k]);
      mpz_sub (res->num[k], v->num[k], v->den);
      mpz_mul_2exp (res->den, v->den, 1);
    }
}

			/* --------------- */

void
tla_vec_ith_div (tla_vec_t res, int i)
{
  ccl_assert (0 <= i && i < res->size);

  if (mpz_cmp_si (res->num[i], 0) < 0)
    {
      int k;
      for (k = 0; k < res->size; k++)
	mpz_neg (res->num[k], res->num[k]);
      mpz_set (res->den, res->num[i]);
    }
  else
    {
      mpz_set (res->den, res->num[i]);
    }
}

			/* --------------- */

void
tla_vec_sub_ith_mul (tla_vec_t res, tla_vec_t x, int i0, tla_vec_t y)
{
  if (mpz_cmp_ui (x->num[i0], 0) == 0)
    tla_vec_set (res, x);
  else
    {
      int i;
      mpz_t xi0;

      mpz_init_set (xi0, x->num[i0]);

      if (mpz_cmp_ui (x->den, 1) == 0)
	{
	  if (mpz_cmp_ui (y->den, 1) == 0)
	    {
	      for (i = 0; i < res->size; i++)
		{
		  mpz_set (res->num[i], x->num[i]);
		  mpz_submul (res->num[i], xi0, y->num[i]);
		}
	      mpz_set_ui (res->den, 1);
	    }
	  else
	    {
	      for (i = 0; i < res->size; i++)
		{
		  mpz_mul (res->num[i], x->num[i], y->den);
		  mpz_submul (res->num[i], xi0, y->num[i]);
		}
	      mpz_set (res->den, y->den);
	    }
	}
      else if (mpz_cmp_ui (y->den, 1) == 0)
	{
	  for (i = 0; i < res->size; i++)
	    {
	      mpz_set (res->num[i], x->num[i]);
	      mpz_submul (res->num[i], xi0, y->num[i]);
	    }
	  mpz_set (res->den, x->den);
	}
      else
	{
	  for (i = 0; i < res->size; i++)
	    {
	      mpz_mul (res->num[i], x->num[i], y->den);
	      mpz_submul (res->num[i], xi0, y->num[i]);
	    }
	  mpz_mul (res->den, x->den, y->den);
	}

      mpz_clear (xi0);
    }
}

			/* --------------- */

void
tla_vec_assign_sub_ith_mul (tla_vec_t res, int i0, tla_vec_t y)
{
  if (mpz_cmp_ui (res->num[i0], 0) != 0)
    {
      int i;
      mpz_t xi0;

      mpz_init_set (xi0, res->num[i0]);

      if (mpz_cmp_ui (res->den, 1) == 0)
	{
	  if (mpz_cmp_ui (y->den, 1) == 0)
	    {
	      for (i = 0; i < res->size; i++)
		mpz_submul (res->num[i], xi0, y->num[i]);
	    }
	  else
	    {
	      for (i = 0; i < res->size; i++)
		{
		  mpz_mul (res->num[i], res->num[i], y->den);
		  mpz_submul (res->num[i], xi0, y->num[i]);
		}
	      mpz_set (res->den, y->den);
	    }
	}
      else if (mpz_cmp_ui (y->den, 1) == 0)
	{
	  for (i = 0; i < res->size; i++)
	    mpz_submul (res->num[i], xi0, y->num[i]);
	}
      else
	{
	  for (i = 0; i < res->size; i++)
	    {
	      mpz_mul (res->num[i], res->num[i], y->den);
	      mpz_submul (res->num[i], xi0, y->num[i]);
	    }
	  mpz_mul (res->den, res->den, y->den);
	}
      mpz_clear (xi0);
    }
}

			/* --------------- */

unsigned int
tla_vec_hash (const tla_vec_t v)
{
  int i;
  unsigned int result = (13 * v->size + 19 * mpz_get_ui (v->den));

  for (i = 0; i < v->size; i++)
    result = ((result << 3) + 11 * mpz_get_ui (v->num[i]));

  return result;
}

