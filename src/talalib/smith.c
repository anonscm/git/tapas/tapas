/*
 * smith.c -- add a comment about this file
 * 
 * This file is a part of the Talence Linear Algebra Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Universit� Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include "matrix.h"
#include "talalib-p.h"

/* La taille de U est n*n
 * La taille de V est n*n
 * La taille de X est m*m
 * La taille de Y est m*m
 * La taille de A est m*n
 * Transforme la matrice A en une matrice de Smith S
 * Et calcule quatre matrices d'entiers U, V, X et Y v�rifiant:
 * U.V = I, V.U=I, X.Y= I et Y.X=I ou I est la matrice identite
 * A=X.S.U
 * Si U==NULL alors elle n'est pas calcul�e.
 * Si V==NULL alors elle n'est pas calcul�e.
 * Si X==NULL alors elle n'est pas calcul�e.
 * Si Y==NULL alors elle n'est pas calcul�e.
 * si dim!=NULL alors *size est egal au rank de la matrice de Smith A
 */
void
tla_matrix_compute_snf (tla_matrix_t A, tla_matrix_t S, int *dim, 
			tla_matrix_t X, tla_matrix_t Y, tla_matrix_t U, 
			tla_matrix_t V)
{
  int i, j, p, imin, jmin, findout;
  mpz_t x, valmin;

  mpz_init (x);
  mpz_init (valmin);
    
  if (A != S)
    tla_matrix_set (S, A);

  if (U != NULL) 
    tla_matrix_set_identity (U);

  if (V != NULL)
    tla_matrix_set_identity (V);
  
  if (X != NULL)
    tla_matrix_set_identity (X);
  
  if (Y != NULL)
    tla_matrix_set_identity (Y);
  
  for(p=1; ((p <= S->m) && (p <= S->n)); p++)
    {
      /* The position of the minimal element in absolute value */
      /* not equal to 0 is denoted by (imin, jmin) */
      
      /* imin=1; */
      /* jmin=1; */
      findout = 0;
      for (i = p; i <= S->m ; i++)
	for (j = p; j <= S->n; j++)
	  if (mpz_sgn (MM (S, i, j)) != 0)
	    {
	      if (findout == 0)
		{
		  findout = 1;
		  imin = i;
		  jmin = j;
		  mpz_set(valmin, MM (S, imin, jmin));
		} 
	      else if (mpz_cmpabs (valmin, MM (S, i, j))>0)
		{
		  imin = i;
		  jmin = j;
		  mpz_set (valmin, MM (S, imin, jmin));
		}
	    }
      
      if (findout == 0)
	break;
      
      while (0 == 0)
	{
	  
	  /* We try to put 0 on collumn jmin	       */
	  findout = 0;
	  for (i = p; ((i <= S->m) && (findout == 0)) ; i++)
	    if ((i != imin) && (mpz_cmp_ui (MM (S, i, jmin), 0) != 0))
	      {
		mpz_fdiv_q (x, MM (S, i, jmin), valmin);
		/* On retranche � la ligne i la ligne imin fois x */
		tla_matrix_submul_line (S, i, imin, x);
		if (X != NULL)
		  tla_matrix_addmul_column (X, imin, i,x);
		if (Y != NULL)
		  tla_matrix_submul_line (Y, i, imin, x);
		/* We try to findout a smaller value */
		for (j = p ; j <= S->n ; j++)
		  if ((mpz_sgn (MM (S, i, j)) != 0) &&
		      (mpz_cmpabs (valmin, MM (S, i, j)) > 0))
		    {
		      imin = i;
		      jmin = j;
		      mpz_set (valmin, MM (S, imin, jmin));
		      findout = 1;
		    }
	      }
	  
	  if (findout == 1)
	    continue;
	  	  
	  /* We try to put 0 on line imin */
	  findout = 0;
	  for (j = p; ((j <= S->n) && (findout == 0)) ; j++)
	    {
	      if ((j != jmin) && (mpz_cmp_ui (MM (S, imin, j), 0) != 0))
		{
		  mpz_fdiv_q (x, MM (S, imin, j), valmin);
		  /* On retranche � la colonne j la colonne jmin fois x */
		  tla_matrix_submul_column (S, j, jmin, x);
		  if (U!=NULL)
		    tla_matrix_addmul_line (U, jmin, j, x);
		  if (V!=NULL)
		    tla_matrix_submul_column (V, j, jmin, x);
		  /* We try to findout a smaller value */
		  for (i = p ; i <= S->m ; i++)
		    if ((mpz_sgn (MM (S, i, j))!=0) &&
			(mpz_cmpabs (valmin, MM (S, i, j)) > 0))
		      {
			imin = i;
			jmin = j;
			mpz_set (valmin, MM (S, imin, jmin));
			findout = 1;
		      }
		}
	    }
	  if (findout==1)
	    continue;
	  
	  
	  
	  /* We try to find out a value that cannot be divided by valmin */
	  findout=0;
	  for (i=p; ((i<=S->m) && (findout==0)); i++)
	    for(j=p; ((j<=S->n) && (findout==0)); j++)
	      if ((i!=imin) && (j!=jmin))
		{
		  mpz_fdiv_r (x, MM (S, i, j), valmin);
		  if (mpz_cmp_ui (x, 0) != 0)
		    {
		      ccl_assert(i!=imin);
		      ccl_assert(j!=jmin);
		      /* We add to line i the line imin */
		      tla_matrix_add_line (S, i, imin);
		      if (X!=NULL)
			tla_matrix_sub_column (X, imin, i);
		      if (Y!=NULL)
			tla_matrix_add_line (Y, i, imin);
		      mpz_fdiv_q (x, MM (S, i, j), valmin);
		      /* We subtract to column j la colonne jmin x fois */
		      tla_matrix_submul_column (S, j, jmin,x);
		      if (U!=NULL)
			tla_matrix_addmul_line (U, jmin, j,x);
		      if (V!=NULL)
			tla_matrix_submul_column (V, j, jmin,x);
		      mpz_set(valmin, MM (S, i, j));
		      imin=i;
		      jmin=j;
		      findout=1;
		    } 
		}
	  if (findout==1)
	    continue;
	  else
	    break;
	}
      
      /* We swap lines imin and p */
      if (imin != p)
	{
	  tla_matrix_swap_lines (S, imin, p);
	  if (X != NULL)
	    tla_matrix_swap_columns (X, imin, p);
	  if (Y != NULL)
	    tla_matrix_swap_lines (Y, imin, p);	
	  imin = p;
	}
      
      /* We swap columns jmin and p */
      if (jmin != p)
	{
	  tla_matrix_swap_columns (S, jmin, p);
	  if (U != NULL)
	    tla_matrix_swap_lines (U, jmin, p);
	  if (V != NULL)
	    tla_matrix_swap_columns (V, jmin, p);	
	  jmin = p;
	}  
  
      /* We enforce valmin to be positive */
      if (mpz_cmp_ui (valmin, 0) < 0) 
	{
	  mpz_neg (valmin, valmin);
	  tla_matrix_neg_column (S, p);
	  if (U != NULL)
	    tla_matrix_neg_line (U, p);
	  if (V != NULL)
	    tla_matrix_neg_column (V, p);
	}
    }
  if (dim!=NULL)
    *dim=p-1;
  mpz_clear (valmin);
  mpz_clear (x);
}
