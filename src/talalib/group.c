/*
 * group.c -- Groups
 * 
 * This file is a part of the Talence Linear Algebra Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include "display.h"
#include "group.h"
#include "talalib-p.h"

void
tla_group_init (tla_group_t G, int size)
{
  ccl_pre (size > 0);
  
  G->dim = 0;
  tla_matrix_init (G->Hermite, size, size + 1);
}

			/* --------------- */
void
tla_group_clear (tla_group_t G)
{
  tla_matrix_clear (G->Hermite);
}

			/* --------------- */

void
tla_group_init_set (tla_group_t res, const tla_group_t G)
{  
  res->dim = G->dim;
  tla_matrix_init_set (res->Hermite, G->Hermite);
}

			/* --------------- */

void
tla_group_set(tla_group_t res, const tla_group_t G)
{  
  ccl_pre (tla_group_get_size (res) == tla_group_get_size (G));

  res->dim = G->dim;
  tla_matrix_set (res->Hermite, G->Hermite);
}

			/* --------------- */

void
tla_group_Z_cap_vsp(tla_group_t G, const tla_vsp_t vsp)
{
  int i, j, col;
  tla_matrix_t A, V;
  int dim = vsp->dim;
  int m = tla_group_get_size (G);
  mpz_t den;
  mpz_t pivot;
  tla_vec_t g;

  ccl_pre (tla_group_get_size (G) == tla_vsp_get_size (vsp));

  /* G is initialized to 0 */
  tla_matrix_set_zero (G->Hermite);

  /* We first compute the product of the denominators */
  mpz_init_set_ui (den, 1);
  for (j = 0 ; j < dim; j++)
    mpz_mul (den, den, vsp->gen[j].v->den);

  /* A is the null square matrix of size m */
  tla_matrix_init (A, m, m);
  
  /* A becomes -den.I */
  for (i = 1 ; i <= m; i++)
    mpz_sub (MM (A, i, i), MM (A, i, i), den);
  
  /* A becomes den.vsp - den.I ... An integral matrix */
  mpz_init(pivot);
  tla_vec_init (g, m);
  
  for (j = 0; j < dim; j++)
    {
      tla_vec_z_mul (g, den, vsp->gen[j].v);
      tla_vec_canonicalize (g);
      for(i = 0; i < m ; i++)
	mpz_add (MM (A, i + 1, vsp->gen[j].i + 1) , 
		 MM (A, i + 1, vsp->gen[j].i + 1),  g->num[i]);
    }
  tla_vec_clear (g);
  mpz_clear (pivot);
  mpz_clear (den);
  
  /* Now A is a matrix such that A.x = 0 iff x is in the vector space generated 
     by vsp */
  
  if (ccl_debug_is_on)
    {
      ccl_debug ("The matrix A such that A.x=0 <=> x in V\n");
      tla_display_matrix (CCL_LOG_DEBUG, A);
    }

  /* V is a square matrix of size m */
  tla_matrix_init (V, m, m);  
  tla_matrix_compute_hnf (A, A, &col, NULL, V);
  
  ccl_assert (col + dim == m);

  /* We put the vectors of V in collums col + 1, ..., m into the column of G */
  for (j = col + 1; j <= m ; j++)
    for (i = 1 ; i <= m; i++)
      mpz_set( MM (G->Hermite, i, j), MM (V, i, j));
  
  /* We destruct A and V */
  tla_matrix_clear (A);
  tla_matrix_clear (V);
  
  tla_matrix_compute_hnf (G->Hermite, G->Hermite, &G->dim, NULL, NULL);
  
  ccl_post (G->dim == dim);

  if (ccl_debug_is_on)
    {
      ccl_debug ("The group Z cap V\n");
      tla_display_matrix (CCL_LOG_DEBUG, G->Hermite);
    }
}

			/* --------------- */

static void 
s_abs_z_mul (tla_group_t R, const tla_group_t G, const mpz_t absscale)
{
  if (R != G)
    tla_group_set (R, G);

  if (mpz_cmp_ui (absscale, 0) == 0)
    {
      R->dim = 0;
      tla_matrix_set_zero (R->Hermite);
    }
  else
    {
      tla_matrix_z_mul (R->Hermite, absscale, R->Hermite);
    }
}
		       
			/* --------------- */

void 
tla_group_z_mul (tla_group_t R, const tla_group_t G, const mpz_t scale)
{
  mpz_t x;

  mpz_init(x);
  mpz_abs (x, scale);
  s_abs_z_mul (R, G, x);
  mpz_clear(x);
}
		       
			/* --------------- */

void
tla_group_z_mul_ui (tla_group_t R, const tla_group_t G, unsigned int scale)
{
  mpz_t x;

  mpz_init_set_ui (x, scale);
  s_abs_z_mul (R, G, x);
  mpz_clear(x);
}

			/* --------------- */

void
tla_group_add_generator(tla_group_t res, const tla_group_t G, 
			const tla_vec_t x)
{
  int i;

  ccl_pre (tla_group_get_size (G) == tla_vec_get_size (x));
  if (res != G)
    tla_group_set (res, G);

  /* Assign x into the column at index G->dim + 1 */
  for (i = 0 ; i < x->size ; i++)
    mpz_set (MM (res->Hermite, i + 1, res->dim + 1), x->num[i]);
  
  tla_matrix_compute_hnf (res->Hermite, res->Hermite, &(res->dim), NULL, NULL);
}

/*
 * Return 1 if x in G=[v_1...v_size,0]
 * lambda is either NULL or a vector of dimension the size of G.
 * Moreover in this case and if lambda!=null return lambda in Z^size
 * such that x=lambda[1].v_1 +...+ lambda[k].v_k
 */
int
tla_group_has (tla_group_t G, const tla_vec_t x, tla_vec_t lambda)
{
  int result = 1;
  mpz_t div;
  tla_vec_t v;
  tla_vec_t w;
  int i, j;
  int iprev,inext;
  const int Gsize = tla_group_get_size (G);

  ccl_pre (Gsize == tla_vec_get_size (x));
  /* x must be a vector of integers */
  ccl_pre ((mpz_cmp_ui (x->den, 1) == 0));
  
  if (lambda != NULL)
    {
      ccl_pre (tla_vec_get_size (lambda) == Gsize);
      tla_vec_set_zero (lambda);
    }
  
  mpz_init (div);
  tla_vec_init_set (v, x);
  tla_vec_init (w, x->size);
  
  iprev = -1;
  j = 0;
  while (iprev + 1 < Gsize)  
    {
      if (j == G->dim)
	inext = Gsize - 1;
      else 
	{
	  /* w become equal to the j-th column of G->Hermite */
	  mpz_set_ui (w->den, 1);
	  for (i = 0; i < Gsize; i++)
	    mpz_set (w->num[i], MM (G->Hermite, i+1, j+1));
	  /* i become equal to the first line such that G->Hermite[i,j] != 0 */
	  inext = iprev + 1;
	  while (mpz_sgn (w->num[inext]) == 0)
	    inext++;
	  mpz_fdiv_q (div, v->num[inext], w->num[inext]);
	  tla_vec_sub_z_mul (v, v, div, w);
	  if (lambda != NULL)
	    mpz_set (lambda->num[j], div);
	}

      for (i = iprev + 1; i <= inext; i++)	
	{
	  if (mpz_cmp_ui (v->num[i], 0) != 0)
	    {
	      result = 0;
	      goto end;
	    }
	}
      iprev = inext;
      j++;
    }

 end:
  tla_vec_clear (w);
  tla_vec_clear (v);
  mpz_clear (div);

  return result;
}


			/* --------------- */

unsigned int
tla_group_hash (const tla_group_t G)
{
  return 13 * G->dim + 111 * tla_matrix_hash (G->Hermite);
}

			/* --------------- */

int
tla_group_are_equal (const tla_group_t G, const tla_group_t GO)
{
  return ((G == GO) ||
	  (G->dim == GO->dim && tla_matrix_are_equal (G->Hermite, GO->Hermite)));
}

			/* --------------- */

/*
 * Return 1 if there exists a vector $x\in G$ such that $<\alpha,x> \leq c$
 * in this case $x \in G$ is a vector such that $<\alpha,x> \leq c and 
 * $<\alpha,x>$ is maximal 
 */
int
tla_group_alpha_min(const tla_group_t G, const tla_vec_t alpha, const mpz_t c, 
		    tla_vec_t x)
{
  int result=0;
  
  int m=G->Hermite->m;
  int n=G->Hermite->n;
  ccl_assert(m>=1);
  ccl_assert(m==alpha->size);
  ccl_assert(m==x->size);
  
  /* We assume that alpha is integral */
  ccl_assert( mpz_cmp_ui(alpha->den,1)==0 );
    
  /*
   * Group G is generated by collums of $G->Hermite$ 
   *
   * Compute $M$ the scalar product of $\alpha$ with each collum of $G->Hermite$
   */
  tla_matrix_t alphaline;
  tla_matrix_init_with_vec_line( alphaline , alpha );
  tla_matrix_t M;
  tla_matrix_init( M , 1 , n );
  tla_matrix_mul_matrix( M , alphaline , G->Hermite );
  
  mpz_t pgcd;
  mpz_init_set_ui(pgcd,0);
  mpz_t coef;
  mpz_init(coef);
  
  tla_vec_t y;
  tla_vec_init(y, n);

  /* Computes the pgcd of the elements in M */
    int j;
  for (j = 0 ; j < n ; j++)
    {
      if (mpz_cmp_ui(M->coefs->num[j],0)!=0) 
	{
	  if (mpz_cmp_ui(pgcd,0)==0)
	    mpz_set(pgcd , M->coefs->num[j]);
	  else
	    mpz_gcd(pgcd , pgcd , M->coefs->num[j]);
	}
    }

  /* case pgcd==0 */
  if (mpz_cmp_ui(pgcd,0)==0)
    {
      if (mpz_cmp_ui(c,0)>=0) 
	{
	  tla_vec_set_zero(x);
	  result=1;
        } 
      goto end;
    }
  
  mpz_abs(pgcd,pgcd);  
  /* Now pgcd>0 */
  
  /*
   * We take the greatest integer coef such that coef \leq c and coef dividable
   * by pgcd
   */
  mpz_fdiv_q(coef,c,pgcd);
  mpz_mul(coef,coef, pgcd);
 
  /* We solve M y = coef */
  tla_vec_t vcoef;
  tla_vec_init (vcoef, 1);
  mpz_set (vcoef->num[0], coef);
  result = tla_matrix_Z_solution (M, y , vcoef);
  tla_vec_clear (vcoef);

  /* We compute x=G->Hermite y */
  
  tla_matrix_mul_vector(x , G->Hermite , y);
  
 end:
  
  tla_vec_clear(y);
  mpz_clear(coef);
  mpz_clear(pgcd);
  tla_matrix_clear(M);
  tla_matrix_clear(alphaline);

  return result;
}



			/* --------------- */
