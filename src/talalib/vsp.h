/*
 * vsp.h -- Vector spaces
 * 
 * This file is a part of the Talence Linear Algebra Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __VSP_H__
# define __VSP_H__

# include <talalib/common.h>
# include <talalib/vec.h>
# include <talalib/matrix.h>

BEGIN_C_DECLS

typedef struct tla_vsp_st {
  int size;
  int dim;
  struct {
    int i;
    tla_vec_t v;
  } *gen;
} tla_vsp_t[1];

extern void
tla_vsp_init (tla_vsp_t vsp, int size);
extern void
tla_vsp_clear (tla_vsp_t vsp);
# define tla_vsp_get_size(vsp) ((vsp)->size)
# define tla_vsp_get_dim(vsp) ((vsp)->dim)
extern void
tla_vsp_canonicalize (tla_vsp_t vsp);
extern void
tla_vsp_set (tla_vsp_t vsp, const tla_vsp_t other);
extern void
tla_vsp_set_zero (tla_vsp_t vsp);
extern int
tla_vsp_are_equal (const tla_vsp_t vsp, const tla_vsp_t other);
extern void
tla_vsp_inv_gamma0 (tla_vsp_t res, const tla_vsp_t v);
extern void
tla_vsp_get_base (tla_vec_t *base, int *base_size, const tla_vsp_t V);
extern int
tla_vsp_add_generator (tla_vsp_t res, const tla_vsp_t vsp, const tla_vec_t x);
extern void
tla_vsp_to_alphas (const tla_vsp_t V, tla_matrix_t R);
extern int
tla_vsp_has (const tla_vsp_t V, const tla_vec_t x);
extern int
tla_vsp_is_included_in (const tla_vsp_t V1, const tla_vsp_t V2);
extern int
tla_vsp_get_integral (const tla_vsp_t V, const tla_vec_t x, tla_vec_t y);

extern void
tla_vsp_compute_y (tla_vec_t y, const tla_vsp_t V, const tla_vec_t x);

extern void
tla_vsp_add_generator_with_y (tla_vsp_t res, const tla_vsp_t vsp,
			      const tla_vec_t y, tla_vec_t yy);

END_C_DECLS

#endif /* ! __VSP_H__ */
