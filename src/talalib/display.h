/*
 * display.h -- add a comment about this file
 * 
 * This file is a part of the Talence Linear Algebra Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __DISPLAY_H__
# define __DISPLAY_H__

# include <ccl/ccl-log.h>
# include <talalib/vec.h>
# include <talalib/matrix.h>
# include <talalib/vsp.h>
# include <talalib/saff.h>
# include <talalib/group.h>

BEGIN_C_DECLS

extern void
tla_display_vec (ccl_log_type log, const tla_vec_t v);
extern void
tla_display_vec_reduced (ccl_log_type log, const tla_vec_t v);
extern void
tla_display_vec_ith_component (ccl_log_type log, const tla_vec_t v, int i);
extern void
tla_display_vec_ith_component_reduced (ccl_log_type log, const tla_vec_t v, 
				       int i);

extern void
tla_display_matrix (ccl_log_type log, const tla_matrix_t M);

extern void
tla_display_vsp (ccl_log_type log, const tla_vsp_t vsp);
extern void
tla_display_vsp_reduced(ccl_log_type log, const tla_vsp_t vsp);
extern void
tla_display_vsp_equations(ccl_log_type log, const tla_vsp_t vsp);

extern void
tla_display_saff (ccl_log_type log, const tla_saff_t saff);
extern void
tla_display_saff_reduced (ccl_log_type log, const tla_saff_t saff);
extern void
tla_display_saff_equations (ccl_log_type log, const tla_saff_t saff);

extern void
tla_display_group (ccl_log_type log, const tla_group_t G);

END_C_DECLS

#endif /* ! __DISPLAY_H__ */
