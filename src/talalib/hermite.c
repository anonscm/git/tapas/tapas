/*
 * hermite.c -- add a comment about this file
 * 
 * This file is a part of the Talence Linear Algebra Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Universit� Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include "matrix.h"
#include "talalib-p.h"

/* La taille de U est n*n
 * La taille de V est n*n
 * La taille de A est m*n
 * Transforme la matrice A en une matrice d'Hermite H
 * Et calcule deux matrices d'entiers U et V v�rifiant:
 * U.V = I et V.U=I ou I est la matrice identite
 * A=H.U
 * Si U==NULL alors elle n'est pas calcul�e.
 * Si V==NULL alors elle n'est pas calcul�e.
 * si dim!=NULL alors *size est egal au rank de la matrice d'Hermite H
 */
void
tla_matrix_compute_hnf (const tla_matrix_t A, tla_matrix_t H, int *dim, 
			tla_matrix_t U, tla_matrix_t V)
{
  /* Very important. Pas si s�r ! */
  /* ccl_assert(n>=m); */

  int j, p, l;
  int curcol;
  mpz_t pivot, x;
  mpz_init (pivot);
  mpz_init (x);
  
  if (A != H)
    tla_matrix_set (H, A);

  if (U != NULL)
    tla_matrix_set_identity (U);
  
  if (V!=NULL)
    tla_matrix_set_identity (V);
  
  curcol=1;
  for (p = 1; p <= A->m; p++) 
    {
      while ((j = tla_matrix_least_in_line (H, p, curcol))!=0) 
	{
	  if (j != curcol) 
	    {
	      tla_matrix_swap_columns (H, j, curcol);
	      if (U!=NULL)
		tla_matrix_swap_lines (U, j, curcol);
	      if (V!=NULL)
		tla_matrix_swap_columns (V, j, curcol);
	      j=curcol;
	    }
	  mpz_set (pivot, MM (H, p, j));
	  if (mpz_cmp_ui (pivot, 0) < 0) 
	    {
	      mpz_neg (pivot, pivot);
	      tla_matrix_neg_column (H, j);
	      if (U!=NULL)
		tla_matrix_neg_line (U, j);
	      if (V!=NULL)
		tla_matrix_neg_column (V, j);
	    }
	  for (l = j + 1; l <= H->n; l++)  
	    {
	      if (mpz_cmp_ui (MM (H, p, l), 0) == 0)
		continue;

	      mpz_fdiv_q (x, MM (H, p, l), pivot);
	      /* On retranche a la colonne l,  x fois la colonne j  */
	      tla_matrix_submul_column (H, l, j, x);
	      /* On ajoute a la ligne l, x fois la ligne j [A verifier] */
	      if (U != NULL)
		tla_matrix_addmul_line (U, j, l, x);
	      if (V != NULL)
		tla_matrix_submul_column (V, l, j, x);
	    }
	}

      mpz_set (pivot, MM (H, p, curcol));
      if (mpz_cmp_ui (pivot, 0) < 0)
	{
	  mpz_neg (pivot, pivot);
	  tla_matrix_neg_column (H, curcol);
	  if (U!=NULL)
	    tla_matrix_neg_line (U, curcol);
	  if (V!=NULL)
	    tla_matrix_neg_column (V, curcol);
	}

      if (mpz_cmp_ui (pivot, 0) != 0)
	{
	  for (l = 1; l <= curcol- 1; l++)
	    {
	      if (mpz_cmp_ui (MM (H, p, l), 0) == 0)
		continue;

	      mpz_fdiv_q (x, MM (H, p, l), pivot);
	      /* On retranche a la colonne l,  x fois la colonne curcol  */
	      tla_matrix_submul_column (H, l, curcol , x);
	      /* On ajoute a la ligne l, x fois la ligne cucol [A verifier] */
	      if (U != NULL)
		tla_matrix_addmul_line (U, curcol, l, x);
	      if (V != NULL)
		tla_matrix_submul_column (V, l, curcol, x);
	    }
	  curcol++;
	}
    }
  curcol--;
  if (dim != NULL)
    *dim = curcol;
  mpz_clear (pivot);
  mpz_clear (x);  
}


