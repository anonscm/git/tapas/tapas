/*
 * talalib-p.h -- Private routines & macros
 * 
 * This file is a part of the Talence Linear Algebra Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __TALALIB_P_H__
# define __TALALIB_P_H__

#define M(A,m,n,i,j) (ccl_assert(((n)*((i)-1)+((j)-1))<((m)*(n))), \
		      ((A)[(n)*((i)-1)+((j)-1)]))

#define MM(_m,_i,_j) M((_m)->coefs->num, (_m)->m, (_m)->n, _i, _j)

#define MPZ2SI(z) (ccl_assert (mpz_fits_sint_p ((z))), mpz_get_si ((z)))

#endif /* ! __TALALIB_P_H__ */
