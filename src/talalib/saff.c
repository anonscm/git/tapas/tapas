/*
 * saff.c -- add a comment about this file
 * 
 * This file is a part of the Talence Linear Algebra Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include "saff.h"


static int
s_has_vsp (const tla_saff_t saff, const tla_vsp_t vsp);

static void
s_set_to_size (tla_saff_t saff, int size);

			/* --------------- */

void
tla_saff_init (tla_saff_t saff, int m)
{
  saff->m = m;
  saff->size = 0;
  saff->V = ccl_new_array (tla_vsp_t, 1);
}

			/* --------------- */

void
tla_saff_clear (tla_saff_t saff)
{
  int i;

  for (i = 0; i < saff->size; i++)
    tla_vsp_clear (saff->V[i]);
  ccl_delete (saff->V);
}

			/* --------------- */

void
tla_saff_set (tla_saff_t res, const tla_saff_t other)
{
  int i;

  res->m = other->m;
  s_set_to_size (res, other->size);
  for (i = 0; i < other->size; i++)
    tla_vsp_set (res->V[i], other->V[i]);
}

			/* --------------- */

void
tla_saff_add_vsp (tla_saff_t res, const tla_saff_t saff, const tla_vsp_t V)
{
  if (res != saff)
    tla_saff_set (res, saff);

  if (s_has_vsp (res, V))
    return;
  s_set_to_size (res, res->size + 1);
  tla_vsp_set (res->V[res->size - 1], V);
}

			/* --------------- */

void
tla_saff_union (tla_saff_t res, const tla_saff_t saff, const tla_saff_t other)
{
  int i;

  if (res != saff)
    tla_saff_set (res, saff);

  for (i = 0; i < other->size; i++)
    tla_saff_add_vsp (res, res, other->V[i]);
}

			/* --------------- */

static int
s_has_vsp (const tla_saff_t saff, const tla_vsp_t vsp)
{
  int i;

  for (i = 0; i < saff->size; i++)
    if (tla_vsp_are_equal (saff->V[i], vsp))
      return 1;

  return 0;
}

			/* --------------- */

static void
s_set_to_size (tla_saff_t saff, int size)
{
  int i;

  if (saff->size == size)
    return;

  if (saff->size < size)
    {
      saff->V =
	(tla_vsp_t *) ccl_realloc (saff->V, sizeof (tla_vsp_t) * size);

      for (i = saff->size; i < size; i++)
	tla_vsp_init (saff->V[i], saff->m);
    }
  else if (saff->size > size)
    {
      for (i = size; i < saff->size; i++)
	tla_vsp_clear (saff->V[i]);

      saff->V =
	(tla_vsp_t *) ccl_realloc (saff->V, sizeof (tla_vsp_t) * size);
    }
  saff->size = size;
}
