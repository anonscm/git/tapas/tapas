/*
 * vsp.c -- add a comment about this file
 * 
 * This file is a part of the Talence Linear Algebra Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-bittable.h>
#include "vsp.h"
#include "talalib-p.h"
			/* --------------- */

static int
s_vsp_are_equal (const tla_vsp_t vsp, const tla_vsp_t other);

static void
s_compute_y (tla_vec_t y, const tla_vsp_t vsp, const tla_vec_t x);

static void
s_check_I_ordering (tla_vsp_t vsp);

			/* --------------- */

void
tla_vsp_init(tla_vsp_t vsp, int size)
{
  ccl_pre( size > 0 );

  vsp->size = size;
  vsp->dim = 0;
  vsp->gen = NULL;
}

			/* --------------- */

void
tla_vsp_clear(tla_vsp_t vsp)
{
  int i;

  for(i = 0; i < vsp->dim; i++)
    tla_vec_clear(vsp->gen[i].v);

  ccl_zdelete(ccl_delete,vsp->gen);
}

			/* --------------- */

void
tla_vsp_canonicalize(tla_vsp_t vsp)
{
  int i;

  for(i = 0; i < vsp->dim; i++)
    tla_vec_canonicalize(vsp->gen[i].v);
}

			/* --------------- */

void
tla_vsp_set(tla_vsp_t vsp, const tla_vsp_t other)
{
  int i;
  
  ccl_pre( vsp->size == other->size );

  if( other->dim <= vsp->dim )
    {
      for(i = 0; i < other->dim; i++)
	{
	  vsp->gen[i].i = other->gen[i].i;
	  tla_vec_set(vsp->gen[i].v,other->gen[i].v);
	}

      for(i = other->dim; i < vsp->dim; i++)
	tla_vec_clear(vsp->gen[i].v);

      if( vsp->dim != other->dim )
	vsp->gen = ccl_realloc(vsp->gen,sizeof(*(vsp->gen))*other->dim);
    }
  else 
    {
      vsp->gen = ccl_realloc(vsp->gen,sizeof(*(vsp->gen))*other->dim);
      for(i = 0; i < vsp->dim; i++)
	{
	  vsp->gen[i].i = other->gen[i].i;
	  tla_vec_set(vsp->gen[i].v,other->gen[i].v);
	}

      for(; i < other->dim; i++)
	{
	  vsp->gen[i].i = other->gen[i].i;
	  tla_vec_init_set(vsp->gen[i].v,other->gen[i].v);
	}
    }
  
  vsp->dim = other->dim;
}

			/* --------------- */

void
tla_vsp_set_zero(tla_vsp_t vsp)
{
  tla_vsp_clear(vsp);
  vsp->dim = 0;
  vsp->gen = NULL;
}

			/* --------------- */

int
tla_vsp_are_equal (const tla_vsp_t vsp, const tla_vsp_t other)
{
  int res = s_vsp_are_equal (vsp, other);

  return res;
}

			/* --------------- */

void
tla_vsp_inv_gamma0(tla_vsp_t res, const tla_vsp_t v)
{
  int i;
  int dim = v->dim;
  tla_vec_t vaux;

  tla_vec_init(vaux,v->size);
  tla_vsp_set_zero(res);

  for(i = 0; i < dim; i++)
    {
      tla_vec_inv_gamma(vaux,v->gen[i].v,0);
      tla_vsp_add_generator (res, res, vaux);
    }
  tla_vec_clear(vaux);
}

			/* --------------- */

int
tla_vsp_add_generator (tla_vsp_t res, const tla_vsp_t vsp, const tla_vec_t x)
{
  int result = 0;

  if (res != vsp)
    tla_vsp_set (res, vsp);

  if (tla_vsp_get_size (res) != tla_vsp_get_dim (res) && ! tla_vec_is_zero (x))
    {
      int sz = res->size;
      tla_vec_t y;

      tla_vec_init(y,sz);
      s_compute_y(y,res,x);

      if( ! tla_vec_is_zero(y) )
	{
	  int j0, j, k;

	  result = 1;

	  for(j0 = 0; j0 < sz; j0++)
	    {
	      if( mpz_cmp_si(y->num[j0],0) != 0 )
		break;
	    }

	  ccl_assert( j0 < sz );
	  tla_vec_ith_div(y,j0);

	  res->gen = ccl_realloc(res->gen,sizeof(*(res->gen))*(res->dim+1));
	  tla_vec_init(res->gen[res->dim].v,res->size);

	  for(k = 0; k < res->dim && res->gen[k].i < j0; k++)
	    tla_vec_assign_sub_ith_mul(res->gen[k].v,j0,y);
	  j = k;

	  for(k = res->dim; k > j; k--)
	    {
	      res->gen[k].i = res->gen[k-1].i;
	      tla_vec_sub_ith_mul(res->gen[k].v,res->gen[k-1].v,j0,y);
	    }

	  res->gen[j].i = j0;
	  tla_vec_set(res->gen[j].v,y);

	  res->dim++;
	  result = 1;
	  tla_vsp_canonicalize(res);
	}

      tla_vec_clear(y);
      if (ccl_debug_is_on)
	s_check_I_ordering(res);
    }

  return result;
}

			/* --------------- */

void
tla_vsp_to_alphas (const tla_vsp_t V, tla_matrix_t R)
{
  int i, k;
  mpz_t lcm, c;
  int m = V->size;
  int n = V->size - V->dim;
  ccl_bittable *I = ccl_bittable_create (V->size);

  ccl_pre (n * m > 0);

  tla_matrix_init (R, n, m);

  mpz_init (c);
  mpz_init_set (lcm, V->gen[0].v->den);
  ccl_bittable_set (I, V->gen[0].i);

  for (i = 1; i < V->dim; i++)
    {
      mpz_lcm (lcm, lcm, V->gen[i].v->den);
      ccl_bittable_set (I, V->gen[i].i);
    }

  for (i = 1, k = 0; k < m; k++)
    {
      if (! ccl_bittable_has (I, k))
	{
	  int j;
	  
	  mpz_set ( MM (R, i, k+1), lcm);
	  for (j = 0; j < V->dim; j++)
	    {
	      ccl_assert (V->gen[j].i != k);
	      mpz_mul (c, lcm, V->gen[j].v->num[k]);
	      mpz_cdiv_q (c, c, V->gen[j].v->den);
	      mpz_neg (MM (R, i, V->gen[j].i+1), c);
	    }
	  i++;
	}
    }
  ccl_post (i == n + 1);

  mpz_clear (lcm);
  mpz_clear (c);
  ccl_bittable_delete (I);
}

			/* --------------- */

int
tla_vsp_has (const tla_vsp_t V, const tla_vec_t x)
{
  int result = 0;

  if (tla_vsp_get_size (V) == tla_vsp_get_dim (V) || tla_vec_is_zero (x))
    result = 1;
  else 
    {
      tla_vec_t y;

      tla_vec_init (y, V->size);
      s_compute_y (y, V, x);

      if (tla_vec_is_zero (y))
	result = 1;
      tla_vec_clear (y);
    }

  return result;
}

			/* --------------- */

int
tla_vsp_is_included_in(const tla_vsp_t V1, const tla_vsp_t V2)
{
  int result;

  if (V1->size != V2->size)
    return 0;

  if (V1->dim >= V2->dim)
    result = s_vsp_are_equal (V1, V2);
  else if (V1->dim > V2->dim)
    result = 0;
  else
    {
      int i;

      result = 1;
      for (i = 0; i < V1->dim && result; i++)
	result = tla_vsp_has (V2, V1->gen[i].v);
    }

  return result;
}

			/* --------------- */

static int
s_vsp_are_equal (const tla_vsp_t vsp, const tla_vsp_t other)
{
  int i;

  if (vsp->size != other->size || vsp->dim != other->dim)
    return 0;

  for (i = 0; i < vsp->dim; i++)
    {
      if (vsp->gen[i].i != other->gen[i].i ||
	  ! tla_vec_are_equal (vsp->gen[i].v,other->gen[i].v))
	return 0;
    }

  return 1;
}

			/* --------------- */

static void
s_compute_y(tla_vec_t y, const tla_vsp_t vsp, const tla_vec_t x)
{
  int k;

  ccl_pre( tla_vec_is_zero(y) );

  tla_vec_get_num(y,x);

  for(k = 0; k < vsp->dim; k++)
    tla_vec_sub_z_mul(y, y, x->num[vsp->gen[k].i], vsp->gen[k].v);
}

			/* --------------- */

static void
s_check_I_ordering(tla_vsp_t vsp)
{
  int k;
  int i;

  if( vsp->dim == 0 )
    return;

  i = vsp->gen[0].i;
  for(k = 1; k < vsp->dim; k++)
    {
      ccl_assert( i < vsp->gen[k].i );
      i = vsp->gen[k].i;
    }
}

/*
 * return 1 if there exists an integral vector in $V+x$. In this case
 * y is such a vector.
 */
int
tla_vsp_get_integral (const tla_vsp_t Vec, const tla_vec_t x, tla_vec_t y)
{  
  int size = tla_vec_get_size (x);

  ccl_pre (tla_vec_get_size (y) == size);
  
  if (tla_vsp_get_size (Vec) == tla_vsp_get_dim (Vec))
    {
      tla_vec_set_zero (y);
      return 1;
    }

  /* Compute A such that V = {v | Av =0 } */
  tla_matrix_t A;
  tla_vsp_to_alphas (Vec, A); 

  ccl_assert (A->n ==  size);

  /* Compute v = Ax */
  tla_vec_t v;
  tla_vec_init (v, tla_matrix_get_nb_lines (A));
  
  tla_matrix_mul_vector (v, A, x);
  
  /* Compute y integral such that Ay=v$ */
  int result=tla_matrix_Z_solution(A,y,v);
  

  tla_vec_clear(v);	  
  tla_matrix_clear (A);

  return result; 
}

