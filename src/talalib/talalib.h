/*
 * talalib.h -- add a comment about this file
 * 
 * This file is a part of the Talence Linear Algebra Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \page talalib Talence Linear Algebra LIBrary
 * \brief A set of routines for linear algebra.
 */
#ifndef __TALALIB_H__
# define __TALALIB_H__

# include <talalib/common.h>
# include <talalib/vec.h>
# include <talalib/matrix.h>
# include <talalib/vsp.h>
# include <talalib/saff.h>
# include <talalib/group.h>
# include <talalib/display.h>

BEGIN_C_DECLS

extern int
tla_init (void);

extern int
tla_terminate (void);

END_C_DECLS

#endif /* ! __TALALIB_H__ */
