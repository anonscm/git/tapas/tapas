/*
 * saff.h -- Semi-affine spaces
 * 
 * This file is a part of the Talence Linear Algebra Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __SAFF_H__
# define __SAFF_H__

# include <talalib/vsp.h>

BEGIN_C_DECLS

typedef struct tla_saff_st {
  int m;
  int size;
  tla_vsp_t *V;
} tla_saff_t[1];

extern void
tla_saff_init(tla_saff_t saff, int m);
extern void
tla_saff_clear(tla_saff_t saff);
extern void
tla_saff_set(tla_saff_t res, const tla_saff_t other);
extern void
tla_saff_add_vsp(tla_saff_t res, const tla_saff_t saff, const tla_vsp_t V);
extern void
tla_saff_union (tla_saff_t res, const tla_saff_t saff, const tla_saff_t other);

END_C_DECLS

#endif /* ! __SAFF_H__ */
