/*
 * display.c -- add a comment about this file
 * 
 * This file is a part of the Talence Linear Algebra Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-bittable.h>
#include <ccl/ccl-assert.h>
#include "display.h"
#include "talalib-p.h"

void
tla_display_vec (ccl_log_type log, const tla_vec_t v)
{
  int i;
  char *tmp;

  ccl_log (log, "[ ");
  for (i = 0; i < v->size; i++)
    {
      tmp = mpz_get_str (NULL, 10, v->num[i]);
      ccl_log (log, (i < v->size - 1) ? "%s, " : "%s", tmp);
      ccl_delete (tmp);
    }

  tmp = mpz_get_str (NULL, 10, v->den);
  ccl_log (log, " ] / %s", tmp);
  ccl_delete (tmp);
}

			/* --------------- */

void
tla_display_vec_reduced (ccl_log_type log, const tla_vec_t v)
{
  int i;
  mpq_t aux;
  char *tmp;

  mpq_init (aux);
  ccl_log (log, "[ ");
  for (i = 0; i < v->size; i++)
    {

      mpq_set_num (aux, v->num[i]);
      mpq_set_den (aux, v->den);
      mpq_canonicalize (aux);
      tmp = mpq_get_str (NULL, 10, aux);

      ccl_log (log, (i < v->size - 1) ? "%s, " : "%s", tmp);

      ccl_delete (tmp);
    }
  ccl_log (log, " ]");
  mpq_clear (aux);
}

			/* --------------- */

void
tla_display_vec_ith_component (ccl_log_type log, const tla_vec_t v, int i)
{
  char *tmp1 = mpz_get_str (NULL, 10, v->num[i]);

  if (mpz_cmp_ui (v->den, 1) != 0 && mpz_cmp_ui (v->num[i], 0) != 0)
    {
      char *tmp2 = mpz_get_str (NULL, 10, v->den);
      ccl_log (log, "%s/%s", tmp1, tmp2);
      ccl_delete (tmp2);
    }
  else
    {
      ccl_log (log, "%s", tmp1);
    }
  ccl_delete (tmp1);
}

			/* --------------- */

void
tla_display_vec_ith_component_reduced (ccl_log_type log, const tla_vec_t v, 
				       int i)
{
  mpq_t aux;
  char *tmp;

  mpq_init (aux);
  mpq_set_num (aux, v->num[i]);
  mpq_set_den (aux, v->den);
  mpq_canonicalize (aux);
  tmp = mpq_get_str (NULL, 10, aux);
  ccl_log (log, "%s", tmp);
  ccl_delete (tmp);
  mpq_clear (aux);
}

			/* --------------- */

static void
s_display_matrix (ccl_log_type log, mpz_t * A, int m, int n, 
		  const mpz_t den)
{
  int i;

  ccl_log (log, "[");
  for (i = 1; i <= m; i++)
    {
      int j;

      if (i == 1)
	ccl_log (log, "[ ");
      else
	ccl_log (log, " [ ");
      for (j = 1; j <= n; j++)
	{
	  char *s = mpz_get_str (NULL, 10, M (A, m, n, i, j));
	  ccl_log (log, "%s ", s);
	  ccl_delete (s);
	}
      if (i < m)
	ccl_log (log, "]\n");
      else
	ccl_log (log, "]");
    }
  ccl_log (log, "]");

  if (den != NULL && mpz_cmp_si (den, 1) == 0)
    {
      char *s = mpz_get_str (NULL, 10, den);
      ccl_log (log, "/%s", s);
      ccl_delete (s);
    }
  ccl_log (log, "\n");
}

			/* --------------- */

void
tla_display_matrix (ccl_log_type log, const tla_matrix_t M)
{
  s_display_matrix (log, M->coefs->num, M->m, M->n, M->coefs->den);
}

			/* --------------- */

void
tla_display_vsp (ccl_log_type log, const tla_vsp_t vsp)
{
  int i;

  ccl_log(log,"[ ");
  for(i = 0; i < vsp->dim; i++)
    {
      tla_display_vec (log,vsp->gen[i].v);
      if( i < vsp->dim-1 )
	ccl_log(log,", ");
    }
  ccl_log(log," ]");
}

			/* --------------- */

void
tla_display_vsp_reduced (ccl_log_type log, const tla_vsp_t vsp)
{
  int i;

  ccl_log(log,"[ ");
  for(i = 0; i < vsp->dim; i++)
    {
      tla_display_vec_reduced(log,vsp->gen[i].v);
      if( i < vsp->dim-1 )
	ccl_log(log,", ");
    }
  ccl_log(log," ]");
}

			/* --------------- */

static void
s_display_factor (ccl_log_type log, mpz_t c, int varindex, int first)
{
  if (mpz_cmp_ui (c, 0) == 0 )
    return;

  if (!first && mpz_cmp_si(c, 0) > 0 )
    ccl_log (log, "+"); 
		
  if (mpz_cmp_si (c, 1) == 0)
    ccl_log (log, "x%d", varindex);
  else if (mpz_cmp_si (c, -1) == 0)
    ccl_log (log, "-x%d", varindex);
  else
    {
      char *tmp = mpz_get_str (NULL, 10, c);
      ccl_log (log, "%s*x%d", tmp, varindex);
      ccl_delete (tmp);
    }
}

/*
 * In non trivial cases we generate the system of equations describing the 
 * membership of a point \a x in V (cf. page 36 of reference paper).
 * \f$\vec{x}\f$ belongs to \f$V\f$ iff 
 * \f$\vec{x}-\Sigma_{j=0}^{dim}\vec{x}[i_j].\vec{v_{i_j}} = \vec{0}\f$ where 
 * \f$I=\{i_j\}\f$ is the ful rank set defining \f$V\f$.
 */
void
tla_display_vsp_equations (ccl_log_type log, const tla_vsp_t V)
{ 
  if (V->dim == 0)
    ccl_log (log, "empty");
  else if (V->dim == V->size)
    ccl_log (log, "all");
  else
    {
      int j;
      mpz_t lcm, c;
      ccl_bittable *I = ccl_bittable_create (V->size);

      mpz_init (c);
      mpz_init_set (lcm, V->gen[0].v->den);
      ccl_bittable_set (I, V->gen[0].i);

      for (j = 1; j < V->dim; j++)
	{
	  mpz_lcm (lcm, lcm, V->gen[j].v->den);
	  ccl_bittable_set (I, V->gen[j].i);
	}

      for (j = 0; j < V->size; j++)
	{
	  int k;

	  if (ccl_bittable_has (I, j))
	    continue;

	  s_display_factor (log, lcm, j, 1);
	  for (k = 0; k < V->dim; k++)
	    {
	      mpz_mul(c, lcm, V->gen[k].v->num[j]);
	      mpz_cdiv_q (c, c, V->gen[k].v->den);
	      mpz_neg (c, c);
	      
	      s_display_factor (log, c, V->gen[k].i, 0);
	    }
	  ccl_log (log, " = 0\n");
	}
      
      mpz_clear (lcm);
      mpz_clear (c);

      ccl_bittable_delete (I);
    }
}

			/* --------------- */

void
tla_display_saff (ccl_log_type log, const tla_saff_t saff)
{
  int i;

  for (i = 0; i < saff->size; i++)
    {
      if (i == 0)
	ccl_log (log, "  ");
      else
	ccl_log (log, "U ");

      tla_display_vsp (log, saff->V[i]);
      ccl_log (log, "\n");
    }
}

			/* --------------- */
void
tla_display_saff_reduced (ccl_log_type log, const tla_saff_t saff)
{
  int i;

  for (i = 0; i < saff->size; i++)
    {
      if (i == 0)
	ccl_log (log, "  ");
      else
	ccl_log (log, "U ");

      tla_display_vsp_reduced (log, saff->V[i]);
      ccl_log (log, "\n");
    }
}

			/* --------------- */

void
tla_display_saff_equations (ccl_log_type log, const tla_saff_t saff)
{
  int i;

  for (i = 0; i < saff->size; i++)
    {
      if (i == 0)
	ccl_log (log, "   ");
      else
	ccl_log (log, "|| ");

      tla_display_vsp_equations (log, saff->V[i]);
      ccl_log (log, "\n");
    }
}

			/* --------------- */

void
tla_display_group (ccl_log_type log, const tla_group_t G)
{
  tla_display_matrix (log, G->Hermite);
}

			/* --------------- */

