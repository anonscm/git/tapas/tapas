/*
 * matrix.h -- Matrices of rationals
 * 
 * This file is a part of the Talence Linear Algebra Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __MATRIX_H__
# define __MATRIX_H__

# include <talalib/common.h>
# include <talalib/vec.h>

BEGIN_C_DECLS

typedef struct tla_matrix_st {
  int m;
  int n;
  tla_vec_t coefs;
} tla_matrix_t[1];

			/* --------------- */

extern void
tla_matrix_init (tla_matrix_t M, int m, int n);

extern void
tla_matrix_init_set (tla_matrix_t M, const tla_matrix_t N);

extern void
tla_matrix_init_with_vec (tla_matrix_t M, const tla_vec_t v, int iscol);

# define tla_matrix_init_with_vec_column(M, v) \
  tla_matrix_init_with_vec (M, v, 1)
# define tla_matrix_init_with_vec_line(M, v) \
  tla_matrix_init_with_vec (M, v, 0)

/*
 * Accessor to the coefficient at line _i and column _j. _i and _j belong to
 * the range, respectively [1, m] and [1, n] where m is the number of lines
 * of the matrix _M and n it number of columns.
 */ 
# define TLA_M_NUM(_M, _i, _j) \
  (ccl_assert((((_M)->n)*((_i)-1)+((_j)-1))<(((_M)->m)*((_M)->n))), \
   ((_M)->coefs->num[((_M)->n)*((_i)-1)+((_j)-1)]))

# define TLA_M_DEN(_M) (_M)->coefs->den

extern void
tla_matrix_remove_last_line (tla_matrix_t R, tla_matrix_t M);

extern void
tla_matrix_resize (tla_matrix_t M, int m, int n);

extern void
tla_matrix_clear (tla_matrix_t M);

extern void
tla_matrix_canonicalize (tla_matrix_t M);

#define tla_matrix_get_nb_lines(M) ((M)->m)
#define tla_matrix_get_nb_columns(M) ((M)->n)

extern void
tla_matrix_get_line (tla_matrix_t M, int i, tla_vec_t R);

extern void
tla_matrix_get_column (tla_matrix_t M, int j, tla_vec_t R);

/*
 * \pre v->size == n && R->size == m
 */
extern void 
tla_matrix_mul_vector (tla_vec_t R, const tla_matrix_t M, const tla_vec_t v);

void 
tla_matrix_mul_matrix (tla_matrix_t R, const tla_matrix_t M, 
		       const tla_matrix_t N);

extern void
tla_matrix_set (tla_matrix_t M, const tla_matrix_t N);

extern void
tla_matrix_z_mul (tla_matrix_t R, const mpz_t z, const tla_matrix_t M);

extern void
tla_matrix_set_identity (tla_matrix_t M);

extern void
tla_matrix_set_zero (tla_matrix_t M);

extern int
tla_matrix_are_equal (const tla_matrix_t M1, const tla_matrix_t M2);

extern unsigned int
tla_matrix_hash (const tla_matrix_t M);

extern int
tla_matrix_Z_solution (const tla_matrix_t M,  tla_vec_t x, const tla_vec_t v);

/**
 * Transform A into a Smith matrice S such that A = X*S*U and U*V=I and X*Y=I
 * 
*/
extern void
tla_matrix_compute_snf (tla_matrix_t A, tla_matrix_t H, int *dim, 
			tla_matrix_t X, tla_matrix_t Y, tla_matrix_t U, 
			tla_matrix_t V);

/**
 * Transform A into an Hermite matrice H such that A = H*U and U*V=I
 * 
*/
extern void
tla_matrix_compute_hnf (const tla_matrix_t A, tla_matrix_t H, int *dim, 
			tla_matrix_t U, tla_matrix_t V);


extern void
tla_matrix_swap_lines (tla_matrix_t A, int i1, int i2);

extern void
tla_matrix_swap_columns (tla_matrix_t A, int j1, int j2);

extern void
tla_matrix_neg_line (tla_matrix_t A, int i);

extern void
tla_matrix_neg_column (tla_matrix_t A, int j);

extern void
tla_matrix_submul_column (tla_matrix_t A, int j, int jpivot, mpz_t x);

extern void
tla_matrix_sub_column (tla_matrix_t A, int j, int jpivot);

extern void
tla_matrix_addmul_column (tla_matrix_t A, int j, int jpivot, mpz_t x);

extern void
tla_matrix_submul_line (tla_matrix_t A, int i, int ipivot, mpz_t x);

extern void
tla_matrix_addmul_line (tla_matrix_t A, int i, int ipivot, mpz_t x);

extern void
tla_matrix_add_line (tla_matrix_t A, int i, int ipivot);

extern int
tla_matrix_least_in_line (tla_matrix_t H, int i, int jmin);

extern int
tla_matrix_is_identity (tla_matrix_t M);

END_C_DECLS

#endif /* ! __MATRIX_H__ */
