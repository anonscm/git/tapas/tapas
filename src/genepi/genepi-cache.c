/*
 * genepi-cache.c -- Implement a cache on GENEPI operations.
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <assert.h>
#include "genepi-p.h"

typedef enum genepi_cached_operation_enum {
  OP_NONE = 0,
  OP_TOP_N,
  OP_TOP_Z,
  OP_TOP_P,
  OP_UNARY,
  OP_BINARY,
  OP_LINEAR,
  OP_PROJECT,
  OP_INVPROJECT,
  OP_NB_CACHES
} genepi_cached_operation;

typedef struct genepi_record_st genepi_record;
struct genepi_record_st {
  genepi_set_impl *R;
  genepi_cached_operation op;

  union {
    int width;
    struct rec_linear {
      genepi_comparator op;
      int tabsize;
      int *tab;
      int c;
    } linear;

    struct rec_project {
      genepi_set_impl *X;
      int tabsize;
      int *tab;
    } project;

    struct rec_unary {
      genepi_func_id id;
      genepi_set_impl *X;
    } unary;

    struct rec_binary {
      genepi_func_id id;
      genepi_set_impl *X1;
      genepi_set_impl *X2;
    } binary;
  } u;
};

struct genepi_cache_st {
  genepi_solver *solver;
  genepi_cache_info info;
  int nb_entries_per_cache;
  genepi_record records[1];
};

			/* --------------- */

#define INDEX(_c,_op,_h) \
  ((((_op) - 1) * (_c)->nb_entries_per_cache) +	\
   ((_h) % (_c)->nb_entries_per_cache))

			/* --------------- */

static int
s_compute_number_of_entries (int cachesize, int number_of_entries);

static void
s_clean_record (genepi_solver *solver, genepi_record *rec);

static unsigned int
s_hash_int_tab (int tabsize, const int *tab);

static int
s_equal_int_tab (int tabsize, const int *tab1, const int *tab2);

static genepi_set_impl *
s_impl_add_reference (genepi_solver *s, genepi_set_impl *X);

static void
s_impl_del_reference (genepi_solver *s, genepi_set_impl *X);

			/* --------------- */

genepi_cache *
genepi_cache_create (genepi_solver *solver, int cachesize, 
		     int number_of_entries)
{
  int nb_entries = s_compute_number_of_entries (cachesize, number_of_entries);
  size_t memsize = sizeof (genepi_cache) + 
    (nb_entries - 1) * sizeof(genepi_record);   
  genepi_cache *result = (genepi_cache *) calloc(memsize,1);
  
  result->solver = solver;
  result->nb_entries_per_cache = nb_entries / (OP_NB_CACHES-1);
  if (result->nb_entries_per_cache == 0)
    result->nb_entries_per_cache = 1;
  result->info.number_of_entries = nb_entries;
  result->info.number_of_bytes = memsize;

  return result;
}

			/* --------------- */

void
genepi_cache_delete (genepi_cache *cache)
{
  int i;
  genepi_record *r;

  assert (cache != NULL);

  for (i = 0, r = cache->records; i < cache->info.number_of_entries; i++, r++)
    {
      if (r->op == OP_NONE)
	continue;

      s_clean_record (cache->solver, r);
    }

  free (cache);
}

			/* --------------- */

const genepi_cache_info *
genepi_cache_get_info (genepi_cache *cache)
{
  assert (cache != NULL);

  return &cache->info;
}

			/* --------------- */

#define HASH_TOP_N(_width) (13 * (_width) + 19 * OP_TOP_N)

genepi_set_impl *
genepi_cache_get_top_N (genepi_cache *cache, int width)
{
  genepi_set_impl *result = NULL;
  unsigned int index;
  genepi_record *r;

  if (cache == NULL)
    return NULL;

  cache->info.requests++;

  index = INDEX (cache, OP_TOP_N, HASH_TOP_N (width));
  assert (0 <= index && index < cache->info.number_of_entries);

  r = cache->records+index;
  if (r->op == OP_TOP_N && r->u.width == width)
    {
      cache->info.success++;
      result = s_impl_add_reference (cache->solver, r->R);
    }

  return result;
}

			/* --------------- */

void
genepi_cache_put_top_N (genepi_cache *cache, int width, genepi_set_impl *R)
{
  unsigned int index;
  genepi_record *r;

  if (cache == NULL)
    return;

  index = INDEX (cache, OP_TOP_N, HASH_TOP_N (width));
  assert (0 <= index && index < cache->info.number_of_entries);

  r = cache->records+index;
  if (r->op != OP_NONE)
    {
      cache->info.collisions++;
      s_clean_record (cache->solver, r);
    }
  else
    {
      cache->info.filled_entries++;
    }

  r->op = OP_TOP_N;
  r->R = s_impl_add_reference (cache->solver, R);
  r->u.width = width;
}

			/* --------------- */

#define HASH_TOP_Z(_width) (13 * (_width) + 19 * OP_TOP_Z)

genepi_set_impl *
genepi_cache_get_top_Z (genepi_cache *cache, int width)
{
  genepi_set_impl *result = NULL;
  unsigned int index;
  genepi_record *r;

  if (cache == NULL)
    return NULL;

  cache->info.requests++;

  index = INDEX (cache, OP_TOP_Z, HASH_TOP_Z (width));
  assert (0 <= index && index < cache->info.number_of_entries);

  r = cache->records+index;
  if (r->op == OP_TOP_Z && r->u.width == width)
    {
      cache->info.success++;
      result = s_impl_add_reference (cache->solver, r->R);
    }

  return result;
}

			/* --------------- */

void
genepi_cache_put_top_Z (genepi_cache *cache, int width, genepi_set_impl *R)
{
  unsigned int index;
  genepi_record *r;

  if (cache == NULL)
    return;

  index = INDEX (cache, OP_TOP_Z, HASH_TOP_Z (width));
  assert (0 <= index && index < cache->info.number_of_entries);

  r = cache->records+index;
  if (r->op != OP_NONE)
    {
      cache->info.collisions++;
      s_clean_record (cache->solver, r);
    }
  else
    {
      cache->info.filled_entries++;
    }

  r->op = OP_TOP_Z;
  r->R = s_impl_add_reference (cache->solver, R);
  r->u.width = width;
}

			/* --------------- */

#define HASH_TOP_P(_width) (13 * (_width) + 19 * OP_TOP_P)

genepi_set_impl *
genepi_cache_get_top_P (genepi_cache *cache, int width)
{
  genepi_set_impl *result = NULL;
  unsigned int index;
  genepi_record *r;

  if (cache == NULL)
    return NULL;

  cache->info.requests++;

  index = INDEX (cache, OP_TOP_P, HASH_TOP_P (width));
  assert (0 <= index && index < cache->info.number_of_entries);

  r = cache->records+index;
  if (r->op == OP_TOP_P && r->u.width == width)
    {
      cache->info.success++;
      result = s_impl_add_reference (cache->solver, r->R);
    }

  return result;
}

			/* --------------- */

void
genepi_cache_put_top_P (genepi_cache *cache, int width, genepi_set_impl *R)
{
  unsigned int index;
  genepi_record *r;

  if (cache == NULL)
    return;

  index = INDEX (cache, OP_TOP_P, HASH_TOP_P (width));
  assert (0 <= index && index < cache->info.number_of_entries);

  r = cache->records+index;
  if (r->op != OP_NONE)
    {
      cache->info.collisions++;
      s_clean_record (cache->solver, r);
    }
  else
    {
      cache->info.filled_entries++;
    }

  r->op = OP_TOP_P;
  r->R = s_impl_add_reference (cache->solver, R);
  r->u.width = width;
}

			/* --------------- */

#define HASH_UNARY(_s,_id,_X) \
  (19*OP_UNARY + (771 * (_s)->plugin->cache_hash ((_s)->plugin->impl, _X) + \
		  13 * (uintptr_t) (_id)))

genepi_set_impl *
genepi_cache_get_unary (genepi_cache *cache, genepi_func_id id,
			genepi_set_impl *X)
{
  genepi_set_impl *result = NULL;
  unsigned int index;
  genepi_record *r;

  if (cache == NULL || cache->solver->plugin->cache_hash == NULL ||
      cache->solver->plugin->cache_equal == NULL)
    return NULL;

  cache->info.requests++;

  index = INDEX (cache, OP_UNARY, HASH_UNARY (cache->solver, id, X));
  assert (0 <= index && index < cache->info.number_of_entries);

  r = cache->records+index;
  if (r->op == OP_UNARY && r->u.unary.id == id && 
      cache->solver->plugin->cache_equal (cache->solver->plugin->impl, X, 
					  r->u.unary.X))
    {
      cache->info.success++;
      result = s_impl_add_reference (cache->solver, r->R);
    }

  return result;
}

			/* --------------- */

void
genepi_cache_put_unary (genepi_cache *cache, genepi_func_id id,
			genepi_set_impl *X, genepi_set_impl *R)
{
  unsigned int index;
  genepi_record *r;

  if (cache == NULL || cache->solver->plugin->cache_hash == NULL ||
      cache->solver->plugin->cache_equal == NULL)
    return;

  index = INDEX (cache, OP_UNARY, HASH_UNARY (cache->solver, id, X));
  assert (0 <= index && index < cache->info.number_of_entries);

  r = cache->records+index;
  if (r->op != OP_NONE)
    {
      cache->info.collisions++;
      s_clean_record (cache->solver, r);
    }
  else
    {
      cache->info.filled_entries++;
    }

  r->op = OP_UNARY;
  r->R = s_impl_add_reference (cache->solver, R);
  r->u.unary.id = id;
  r->u.unary.X = s_impl_add_reference (cache->solver, X);
}

			/* --------------- */

#define HASH_BINARY(s,op,X1,X2)	\
  (19 * OP_BINARY + (HASH_UNARY (s,op,X1) + \
		     119 * (s)->plugin->cache_hash ((s)->plugin->impl, X2)))

genepi_set_impl *
genepi_cache_get_binary (genepi_cache *cache, genepi_func_id id,
			 genepi_set_impl *X1, genepi_set_impl *X2)
{
  genepi_set_impl *result = NULL;
  unsigned int index;
  genepi_record *r;

  if (cache == NULL || cache->solver->plugin->cache_hash == NULL ||
      cache->solver->plugin->cache_equal == NULL)
    return NULL;

  cache->info.requests++;

  index = INDEX (cache, OP_BINARY, HASH_BINARY (cache->solver, id, X1, X2));
  assert (0 <= index && index < cache->info.number_of_entries);

  r = cache->records+index;
  if (r->op == OP_BINARY && r->u.binary.id == id && 
      cache->solver->plugin->cache_equal (cache->solver->plugin->impl, X1, 
					  r->u.binary.X1) &&
      cache->solver->plugin->cache_equal (cache->solver->plugin->impl, X2, 
					  r->u.binary.X2))
    {
      cache->info.success++;
      result = s_impl_add_reference (cache->solver, r->R);
    }

  return result;
}

			/* --------------- */

void
genepi_cache_put_binary (genepi_cache *cache, genepi_func_id id,
			 genepi_set_impl *X1, genepi_set_impl *X2, 
			 genepi_set_impl *R)
{
  unsigned int index;
  genepi_record *r;

  if (cache == NULL || cache->solver->plugin->cache_hash == NULL ||
      cache->solver->plugin->cache_equal == NULL)
    return;

  index = INDEX (cache, OP_BINARY, HASH_BINARY (cache->solver, id, X1, X2));
  assert (0 <= index && index < cache->info.number_of_entries);

  r = cache->records+index;
  if (r->op != OP_NONE)
    {
      cache->info.collisions++;
      s_clean_record (cache->solver, r);
    }
  else
    {
      cache->info.filled_entries++;
    }

  r->op = OP_BINARY;
  r->R = s_impl_add_reference (cache->solver, R);
  r->u.binary.id = id;
  r->u.binary.X1 = s_impl_add_reference (cache->solver, X1);
  r->u.binary.X2 = s_impl_add_reference (cache->solver, X2);
}


			/* --------------- */

#define HASH_LINEAR(_asize,_a,_op,_c) \
  (19 * OP_LINEAR + (13 * s_hash_int_tab (_asize, _a) + \
		     19 * (_op) + 771 * (_c)))

genepi_set_impl *
genepi_cache_get_linear_operation (genepi_cache *cache, const int *alpha, 
				   const int alpha_size, genepi_comparator op, 
				   const int c)
{
  genepi_set_impl *result = NULL;
  unsigned int index;
  genepi_record *r;

  if (cache == NULL || cache->solver->plugin->cache_hash == NULL ||
      cache->solver->plugin->cache_equal == NULL)
    return NULL;

  cache->info.requests++;

  index = INDEX (cache, OP_LINEAR, HASH_LINEAR (alpha_size, alpha, op, c));
  assert (0 <= index && index < cache->info.number_of_entries);

  r = cache->records+index;
  if (r->op == OP_LINEAR && r->u.linear.op == op && 
      r->u.linear.tabsize == alpha_size &&
      r->u.linear.c == c && 
      s_equal_int_tab (alpha_size, r->u.linear.tab, alpha))
    {
      cache->info.success++;
      result = s_impl_add_reference (cache->solver, r->R);
    }

  return result;
}

			/* --------------- */

void
genepi_cache_put_linear_operation (genepi_cache *cache, const int *alpha, 
				   const int alpha_size, genepi_comparator op,
				   const int c, genepi_set_impl *R)
{
  int i;
  unsigned int index;
  genepi_record *r;

  if (cache == NULL || cache->solver->plugin->cache_hash == NULL ||
      cache->solver->plugin->cache_equal == NULL)
    return;

  cache->info.requests++;

  index = INDEX (cache, OP_LINEAR, HASH_LINEAR (alpha_size, alpha, op, c));
  assert (0 <= index && index < cache->info.number_of_entries);

  r = cache->records+index;
  if (r->op != OP_NONE)
    {
      cache->info.collisions++;
      s_clean_record (cache->solver, r);
    }
  else
    {
      cache->info.filled_entries++;
    }

  r->op = OP_LINEAR;
  r->R = s_impl_add_reference (cache->solver, R);
  r->u.linear.op = op;
  r->u.linear.tabsize = alpha_size;
  r->u.linear.c = c;
  r->u.linear.tab = (int *) calloc (sizeof (int), alpha_size);
  for (i = 0; i < alpha_size; i++)
    r->u.linear.tab[i] = alpha[i];
}

			/* --------------- */

#define HASH_PROJECT_(_op,_s,_X,_selsz,_sel)	     \
  ((_op) *19 + (13 * (_s)->plugin->cache_hash ((_s)->plugin->impl, _X) + \
		19 * s_hash_int_tab (_selsz, _sel)))

#define HASH_PROJECT(_s,_X,_selsz,_sel)			\
  HASH_PROJECT_ (OP_PROJECT, _s, _X, _selsz, _sel)
#define HASH_INVPROJECT(_s,_X,_selsz,_sel)		\
  HASH_PROJECT_ (OP_INVPROJECT, _s, _X, _selsz, _sel)

			/* --------------- */

genepi_set_impl *
genepi_cache_get_project (genepi_cache *cache, genepi_set_impl *X, 
			  const int *sel, int selsize, int inverse)
{
  genepi_set_impl *result = NULL;
  unsigned int index;
  genepi_record *r;

  if (cache == NULL || cache->solver->plugin->cache_hash == NULL ||
      cache->solver->plugin->cache_equal == NULL)
    return NULL;

  cache->info.requests++;

  if (inverse)
    {
      index = HASH_INVPROJECT (cache->solver, X, selsize, sel);
      index = INDEX(cache, OP_INVPROJECT, index);
    }
  else
    {
      index = HASH_PROJECT (cache->solver, X, selsize, sel);
      index = INDEX(cache, OP_PROJECT, index);
    }

  assert (0 <= index && index < cache->info.number_of_entries);
  r = cache->records+index;
  if ( ((r->op == OP_PROJECT && !inverse) ||
	(r->op == OP_INVPROJECT && inverse)) &&
       cache->solver->plugin->cache_equal (cache->solver->plugin->impl, 
					   r->u.project.X, X) &&
       r->u.project.tabsize == selsize &&
       s_equal_int_tab (selsize, r->u.project.tab, sel))
    {
      cache->info.success++;
      result = s_impl_add_reference (cache->solver, r->R);
    }

  return result;
}

			/* --------------- */

void
genepi_cache_put_project (genepi_cache *cache, genepi_set_impl *X, 
			  const int *sel, int selsize, int inverse, 
			  genepi_set_impl *R)
{
  int i;
  unsigned int index;
  genepi_record *r;

  if (cache == NULL || cache->solver->plugin->cache_hash == NULL ||
      cache->solver->plugin->cache_equal == NULL)
    return;

  cache->info.requests++;

  if (inverse)
    {
      index = HASH_INVPROJECT (cache->solver, X, selsize, sel);
      index = INDEX(cache, OP_INVPROJECT, index);
    }
  else
    {
      index = HASH_PROJECT (cache->solver, X, selsize, sel);
      index = INDEX(cache, OP_PROJECT, index);
    }
  assert (0 <= index && index < cache->info.number_of_entries);

  r = cache->records+index;
  if (r->op != OP_NONE)
    {
      cache->info.collisions++;
      s_clean_record (cache->solver, r);
    }
  else
    {
      cache->info.filled_entries++;
    }

  r->op = inverse?OP_INVPROJECT:OP_PROJECT;
  r->R = s_impl_add_reference (cache->solver, R);
  r->u.project.X = s_impl_add_reference (cache->solver, X);
  r->u.project.tabsize = selsize;
  r->u.project.tab = (int *) calloc (sizeof (int), selsize);
  for (i = 0; i < selsize; i++)
    r->u.project.tab[i] = sel[i];
}

			/* --------------- */


#define DIVISOR(n,p)   ((p/n)*n==p)

static int
s_is_prime (int p)
{
  int found = 0;
  int n;

  for(n = 3; (!found) && (n * n <= p); n+=2)
    found = DIVISOR (n, p);
 
  return ! found;
}

static int
s_next_prime (int n)
{
  if ((n % 2) == 0)
    n++;
  while (!s_is_prime (n))
    n += 2;

  return n;
}

			/* --------------- */

static int
s_compute_number_of_entries (int cachesize, int number_of_entries)
{
  int result = 0;

  if (cachesize > 0)
    result = s_next_prime (cachesize / sizeof (genepi_record) + 1);

  if (number_of_entries > 0)
    result = s_next_prime (number_of_entries);
  
  if (result > 0 && result < OP_NB_CACHES-1)
    result = OP_NB_CACHES;

  return result;
}

			/* --------------- */

static void
s_clean_record (genepi_solver *solver, genepi_record *r)
{
  assert (r->op != OP_NONE);

  s_impl_del_reference (solver, r->R);
  switch (r->op)
    {
    case OP_TOP_N:
      break;
    case OP_TOP_Z:
      break;
    case OP_TOP_P:
      break;
    case OP_UNARY:
      s_impl_del_reference (solver, r->u.unary.X);
      break;
    case OP_BINARY:
      s_impl_del_reference (solver, r->u.binary.X1);
      s_impl_del_reference (solver, r->u.binary.X2);
      break;
    case OP_LINEAR:
      free (r->u.linear.tab);
      break;
    case OP_PROJECT: case OP_INVPROJECT:
      s_impl_del_reference (solver, r->u.project.X);
      free (r->u.project.tab);
      break;
    default: 
      abort ();
      break;
    }
}

			/* --------------- */

static unsigned int
s_hash_int_tab (int tabsize, const int *tab)
{
  int i;
  unsigned int result = tabsize;

  for (i = 0; i < tabsize; i++)
    result = 13*result +19*tab[i];

  return result;
}

			/* --------------- */

static int
s_equal_int_tab (int tabsize, const int *tab1, const int *tab2)
{
  int i;

  for (i = 0; i < tabsize; i++)
    {
      if (tab1[i] != tab2[i])
	return 0;
    }

  return 1;
}

			/* --------------- */

static genepi_set_impl *
s_impl_add_reference (genepi_solver *s, genepi_set_impl *X)
{
  if (s->plugin->add_reference != NULL)
    return s->plugin->add_reference (s->plugin->impl, X);

  return X;
}

			/* --------------- */

static void
s_impl_del_reference (genepi_solver *s, genepi_set_impl *X)
{
  if (s->plugin->del_reference != NULL)
    s->plugin->del_reference (s->plugin->impl, X);
}

			/* --------------- */

