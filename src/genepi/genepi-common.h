/*
 * genepi-common.h -- Common macros
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

#ifndef __GENEPI_COMMON_H__
# define __GENEPI_COMMON_H__

# include <tapas-config.h>
# include <stdio.h>

# if TAPAS_HAVE_STRING_H
#  include <string.h>
# endif /* TAPAS_HAVE_STRING_H */

# if TAPAS_HAVE_STDLIB_H
#  include <stdlib.h>
# endif /* TAPAS_HAVE_STDLIB_H */

#ifdef TAPAS_HAVE_INTTYPES_H
# include <inttypes.h>
#endif

#ifdef TAPAS_HAVE_STDINT_H
# include <stdint.h>
#endif

#ifdef TAPAS_HAVE_UNISTD_H
# include <unistd.h>
#endif 

# ifndef BEGIN_C_DECLS
#  ifdef __cplusplus 
#   define BEGIN_C_DECLS extern "C" {
#   define END_C_DECLS }
#  else /* ! __cpluplus */
#   define BEGIN_C_DECLS 
#   define END_C_DECLS 
#  endif /* ! __cpluplus */
# endif /* ! BEGIN_C_DECLS */

#endif /* ! __GENEPI_COMMON_H__ */
