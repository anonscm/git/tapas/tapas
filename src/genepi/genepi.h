/*
 * genepi.h -- The GENEPI main header file
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*! 
 * \page genepi GENEPI
 * \section Introduction
 *
 * The GENEPI Library allows the manipulation of Presburger definable sets by
 * means of dynamically loaded solvers. The aim of the GENEPI library is 
 * twofold:
 * <ul>
 * <li>On one hand it can be used as a solver of Presburger formulas and, 
 * thus, use in softwares needing such tool. Basically GENEPI was designed
 * for the model-checker called FASTer (http://www.lsv.ens-cachan.fr/fast/) 
 * that implements acceleration techniques based on symbolic representation 
 * of state spaces for infinite systems. </li>
 * <li>On the other hand it can be used to compare solver performances. There 
 * currently exists 6 solvers compliant with GENEPI:
 *  <ul>
 *  <li>LASH: http://www.montefiore.ulg.ac.be/~boigelot/research/lash/</li>
 *  <li>LIRA: http://lira.gforge.avacs.org/</li>
 *  <li>MONA: http://www.brics.dk/~mona/</li>
 *  <li>Omega: http://www.cs.umd.edu/projects/omega/</li>
 *  <li>PresTAF: http://altarica.labri.fr/wiki/tools:tapas:prestaf</li>
 *  <li>PPL: http://www.cs.unipr.it/ppl/</li>
 *  </ul>
 * All plugins can be downloaded on http://tapas.labri.fr/.
 * </li>
 * </ul>
 */

/*!
 * \file genepi/genepi.h
 * \brief GENEPI Solver Interface
 *
 * This module is the part of GENEPI used to build Presburger definable 
 * \ref genepi_set "sets". Sets are built via a \ref genepi_solver "solver" 
 * which relates the client code of GENEPI to a dynamically loaded \ref 
 * genepi/genepi-plugin.h "plugin".
 *
 * \section Solvers
 *
 * Once a \ref genepi/genepi-plugin.h "plugin" has been selected and loaded 
 * with the GENEPI \ref genepi/genepi-loader.h "loader" a solver can be 
 * created using the \ref genepi_solver_create "create" function. GENEPI 
 * solvers can use a cache for some Presburger operations (each solver has its 
 * proper cache). The size of the cache is specified (in terms of the number of
 * bytes or entries of the table) at creation time.
 *
 * At any time the client code can indicate the solver to produce a trace
 * of GENEPI calls (cf. \ref genepi_solver_set_trace_file "set_trace_file"). 
 * The trace is generated in a FILE which can be reinterpreted by the DIJO 
 * tool. The description of the file generated by the solver is given in 
 * genepi/genepi-trace.h.
 * 
 * The client code can request GENEPI to verify the plugin by checking some
 * tautologies (cf. \ref genepi_solver_run_autotest "autotest"). This function
 * can be useful when integrating a new plugin.
 *
 * \section Sets
 *
 * A solver is then used to build sets of solutions of Presburger formulas. 
 * These sets are <em>n</em>-ary where \e n is the number of variables 
 * appearing in the formula. GENEPI sets are only relation which means that
 * no link exists between the column of relations and the variable of a 
 * formula. Indeed the notion of formula does not appear in GENEPI. The API
 * furnishes a minimal kernel of functions permitting the computation the 
 * solutions of a Presburger formula. 
 *
 * Since variable are not attached to columns of relation the client code has
 * to take care of formulas built over different sets of variables. For 
 * example, if one wants to compute the solutions of \f$x_1+x_2 = x_3 \vee  
 * x_2+x_4 = 0\f$ one can have two approaches:
 * \arg compute the solutions of each equation as relation of arity 4 and then
 * compute the union. 
 *
 * \arg compute the solutions of the first equation as a ternary relation and
 * the second as a binary relation. Then enlarge both relations to obtain
 * two relations of arity 4 and make the union. In this solution, when 
 * a relation is enlarged, one has to take care to put \f$x_i\f$ in the same
 * column for both relations. 
 *
 * The GENEPI interface offers the following functionalities (each one depends
 * on the abilities of the underlying plugin):
 *
 * \par Elementary relations are built with the following functions:
 * \arg \f$\emptyset^n\f$ if built using \ref genepi_set_bot "bot" function
 * \arg \f${\cal K}^n\f$ where \f${\cal K}\f$ is a domain for variables with 
 * the \ref genepi_set_top "top", \ref genepi_set_top_N "top_N", \ref 
 * genepi_set_top_Z "top_Z", \ref genepi_set_top_P "top_P" and \ref 
 * genepi_set_top_R "top_R".
 * \arg \f$\Sigma_i \alpha_i.x_i \propto \beta\f$ where \f$\propto\in\{ =, <,
 * \leq, >, \geq \}\f$ are built using \ref genepi_set_linear_operation 
 * "linear_operation". Equality can be built directly using \ref 
 * genepi_set_linear_equality "linear_equality".
 *
 * \par Boolean operations:
 * Basic operations on set are done with the functions:
 * \ref genepi_set_complement "complement", \ref genepi_set_union "union" and
 * \ref genepi_set_intersection "intersection".
 * 
 * \par Column manipulations use two functions:
 * \arg \ref genepi_set_project "project" which projects a relation on a subset
 * of its column. This function is used to implement existential 
 * quantification.
 * \arg \ref genepi_set_invproject "invproject" which add columns to a 
 * relation.
 *
 * \par <tt>pre</tt> and <tt>post</tt> computations:
 * Basically this library has been designed for the FASTer model-checker. Two
 * functions permit to compute for a given transition relation the successors
 * or the predecessors of a given set; set \ref genepi_set_apply "apply" and
 * \ref genepi_set_applyinv "applyinv".
 *
 * \par Some elementary properties can be checked on relation:
 * \arg inclusion and equality of two relations can be checked with, resp., 
 * \ref genepi_set_compare "compare" and  \ref genepi_set_equal "equal" 
 * functions.
 *
 * \arg emptiness, completeness and finiteness can be checked with the 
 * functions, respectively, \ref genepi_set_is_empty "is_empty", \ref 
 * genepi_set_is_full "is_full" and \ref genepi_set_is_finite "is_finite".
 *
 * \arg dependency to one or several columns is checked by \ref 
 * genepi_set_depend_on "depend_on".
 *
 * \par Explicit solutions:
 * It is possible to get one or more elements belonging to a computed set.
 * \arg \ref genepi_set_get_one_solution "get_one_solution" returns one element
 * if the set is not empty.
 * \arg \ref genepi_set_is_solution "is_solution" checks if a vector belongs
 * to a set.
 * \arg \ref genepi_set_get_solutions "get_solutions" is used to retrieve
 * all solutions in case of finite sets or a subset of solutions in case of
 * infinite sets.
 *
 * \par Serialization:
 * One can serialize any compute set into a standard C FILE-stream. Use
 * \ref genepi_set_write "write" to save a set and use  \ref 
 * genepi_set_read "read" to reload it. Of course these functions must not be
 * used with the DIJO \ref genepi-trace.h "traces".
 */
#ifndef __GENEPI_H__
# define __GENEPI_H__

# include <genepi/genepi-common.h>
# include <genepi/genepi-plugin-config.h>
# include <genepi/genepi-loader.h>

BEGIN_C_DECLS

/*!
 * \brief Type of GENEPI solver.
 * 
 * A GENEPI solver is used to encode Presburger definable sets by means of
 * \ref genepi_set "genepi_sets". The data structure used to encode such sets
 * depend on a \ref genepi/genepi-plugin.h "plugin" specified at the creation 
 * time of the solver.
 */
typedef struct genepi_solver_st genepi_solver;

/*!
 * \brief Abstraction of Presburger definable set.
 * 
 * Variables of this type are relations definable by a Presburger formula.
 * This type is manipulated using a GENEPI \ref genepi_solver "solver" and
 * the data structure used to encode sets depend on the \ref 
 * genepi/genepi-plugin.h "plugin" used by the solver.
 */
typedef struct genepi_set_st genepi_set;

/*!
 * \brief Request the solver to evaluate the total execution time of function
 * of the GENEPI API.
 *
 * If this flag is specified when a solver is created then each time a 
 * genepi_set_\a X function is called (only function requiring important 
 * computation time i.e. projection, union, ...), GENEPI maintains a counter
 * of number of calls and the total time used to execute the function. The
 * execution time is evaluated using the standard <tt>clock</tt> (3) function.
 *
 * When the solver is deleted all this informations are gathered and displayed
 * on standard output.
 *
 * \see genepi_solver_create
 */
# define GENEPI_FLAG_SHOW_TIMERS     (0x01<<0)

/*!
 * \brief Request the solver to display information on the cache usage.
 * 
 * Before the deletion of the solver (genepi_solver_delete), if its cache is 
 * enabled then some statistics related to the operation cache usage is 
 * displayed on the standard output.
 *
 * \see genepi_solver_create
 */
# define GENEPI_FLAG_SHOW_CACHE_INFO (0x01<<1)

/*!
 * \brief Comparison specifier.
 * 
 * This enumeration is used to indicate to certain function a comparison 
 * relation. \a EQ means "equal", \a LT means "less than", \a GT means "greater
 * than", \a LEQ means "less than" and \a GEQ means "greater than". Of course,
 * the interpretation depends on the underlying set.
 *
 * \see genepi_set_compare and genepi_set_linear_operation
 */
typedef enum {
  GENEPI_EQ,
  GENEPI_LT,
  GENEPI_GT,
  GENEPI_LEQ,
  GENEPI_GEQ
} genepi_comparator;

/*!
 * \brief Indicate the domain of variable used by a solver.
 * 
 * GENEPI_N_SOLVER indicates that variables are positive or null integers  
 * GENEPI_Z_SOLVER indicates that variables are are positive, negative or null
 * integer
 * GENEPI_P_SOLVER indicates that variables are positive or null real 
 * numbers
 * GENEPI_R_SOLVER indicates that variables are rational numbers
 */
typedef enum {
  GENEPI_N_SOLVER = 0,
  GENEPI_Z_SOLVER,
  GENEPI_P_SOLVER,
  GENEPI_R_SOLVER
} genepi_solver_type;

/*!
 * \brief Information string identifiers.
 *
 * This enumeration is used to retrieve informations about the underlying 
 * plugin used by a solver.
 *
 * \see genepi_solver_get_info
 */
typedef enum {
  GENEPI_INFO_COPYRIGHT = 0,
  GENEPI_INFO_AUTHORS,
  GENEPI_INFO_LICENSE,
  GENEPI_INFO_BRIEF_DESCRIPTION,
  GENEPI_INFO_FULL_DESCRIPTION
} genepi_info;

/*!
 * \brief Error codes returned by the functions \ref genepi_set_write and
 * \ref genepi_set_read.
 */
typedef enum {
  /*!
   * \brief An has occurred on the stream at the system level.
   */
  GENEPI_RW_ERR_IO         = -1,

  /*!
   * \brief The client code tries to load a set saved with a plugin different
   * of the one currently used.
   */
  GENEPI_R_ERR_BAD_SOLVER  = -2,

  /*!
   * \brief The client code tries to load a set saved with another version of
   * the plugin currently used.
   */
  GENEPI_R_ERR_BAD_VERSION = -3,

  /*!
   * \brief Malformed stream: data don't respect the serialization format 
   * expected by GENEPI or by the underlying plugin.
   */
  GENEPI_R_ERR_DATA_ERROR  = -4
} genepi_io_error;

/*!
 * \brief Creates a GENEPI solver.
 *
 * This function creates a solver based on the plugin (cf. 
 * genepi/genepi-plugin.h) used to encode the Presburger definable sets.
 *
 * The size of the operation cache can be specified in two ways either by 
 * the total size in bytes of the table, either by the number of its entries. 
 * The number of entries is converted into a number of bytes and then the 
 * biggest size is selected. If the cache size is 0 or if the underlying plugin
 * does not implement one of the function \ref genepi_plugin_cache_hash 
 * "cache_hash" or \ref genepi_plugin_cache_equal "cache_equal" then the
 * cache is disabled.
 *
 * \a flags are used to customize the behaviors of the solver and are 
 * formed by a bitwise disjunction of macros \ref GENEPI_FLAG_SHOW_TIMERS 
 * "GENEPI_FLAG_\a X" .
 *
 * \param plugin the plugin encoding the Presburger sets
 * \param flags customization flags
 * \param cachesize the size in bytes of the operation cache 
 * \param number_of_cache_entries the size in number of entries of the
 * operation cache
 * 
 * \return a pointer to a new solver using \a plugin as set encoding.
 */
extern genepi_solver *
genepi_solver_create (genepi_plugin *plugin, int flags, int cachesize,
		      int number_of_cache_entries);

/*!
 * \brief Releases resources allocated by this \a solver.
 *
 * The underlying plugin is not deleted (this operation depends on the 
 * \ref genepi/genepi-loader.h "loader"). If the \a flags specified when the
 * solver has been created contain \ref GENEPI_FLAG_SHOW_TIMERS "SHOW_TIMERS"
 * then the execution times for each function of the API is displayed. If 
 * \a flags contain \ref GENEPI_FLAG_SHOW_CACHE_INFO "SHOW_CACHE_INFO" then
 * information related to the cache on GENEPI operations is displayed.
 *
 * \param solver the GENEPI solver
 *
 * \pre solver != NULL 
 */
extern void
genepi_solver_delete (genepi_solver *solver);

/*!
 * \brief Gets the domain of variables used by the underlying plugin
 *
 * This function returns the domain of variables used by the solver.
 * If the plugin doesn't supply this information we assume that this domain
 * is the set of non-negative integers \f$\cal N\f$ (i.e. \ref 
 * genepi_solver_type "GENEPI_N_SOLVER" is returned).
 *
 * \param solver the GENEPI solver
 *
 * \pre solver != NULL
 *
 * \return the domain of variables of \a solver.
 */
extern genepi_solver_type 
genepi_solver_get_solver_type (genepi_solver *solver);

/*!
 * \brief 
 */
extern const genepi_plugconf *
genepi_solver_get_plugin_config (genepi_solver *solver);

/*!
 * \brief Gets the name of the underlying plugin.
 *
 * \param solver the GENEPI solver
 *
 * This function returns the name of the underlying GENEPI plugin.
 * 
 * \pre solver != NULL
 *
 * \return a pointer to the path to plugin binary file
 */
extern const char *
genepi_solver_get_name (genepi_solver *solver);

/*!
 * \brief Enables/Disables GENEPI trace generator.
 *
 * This procedure sets or changes the FILE where GENEPI traces are generated.
 * If a file is already in use then this stream is 'fflushe'd (but not 
 * 'fclose'd) and associated data structures are cleaned. The 'output' might
 * be NULL, in which case no more traces are generated.
 *
 * \param solver the GENEPI solver
 * \param output the stream on whoich traces are generated
 *
 * \pre solver != NULL
 */
extern void
genepi_solver_set_trace_file (genepi_solver *solver, FILE *output);

/*!
 * \brief Gets the current stream used for generate traces.
 *
 * \param solver the GENEPI solver
 * 
 * This function returns the FILE pointer currently used as output for the 
 * GENEPI trace generator. If the function returns NULL it means that no trace
 * is generated.
 *
 * \pre solver != NULL
 *
 * \return the pointer to the trace stream or NULL if the generator is 
 * disabled.
 */
extern FILE *
genepi_solver_get_trace_file (genepi_solver *solver);

/*!
 * \brief Checks some tautologies with \a solver.
 *
 * \param solver the GENEPI solver
 *
 * This function runs tests on the 'solver'. Two kind of tests are used. 
 * If the plugin provides its own test function, then this one is called.
 * Then, generic tests are executed using the GENEPI functions.
 *
 * \pre solver != NULL
 *
 * \return a non-null value is all tests are passed and 0 if one test fail.
 */
extern int
genepi_solver_run_autotest (genepi_solver *solver);

/*!
 * \brief This function increments the number of references to the X set.
 *
 * \param solver the GENEPI solver
 * \param X the considered set
 *
 * \pre solver != NULL
 * \pre X != NULL
 * \pre number of references to X > 0 
 *
 * \return X
 */
extern genepi_set *
genepi_set_add_reference (genepi_solver *solver, genepi_set *X);

/*!
 * \brief Removes a reference to \a X
 *
 * This function decrements the number of references to the X set. If this
 * is equal to 0 then all memory related to X is freed.
 *
 * \param solver the GENEPI solver
 * \param X the considered set
 *
 * \pre solver != NULL
 * \pre X != NULL
 * \pre number of references to X > 0 
 */
extern void
genepi_set_del_reference (genepi_solver *solver, genepi_set *X);

/*!
 * \brief Computes the complete relation of arity \a n 
 *
 * \param solver the GENEPI solver
 * \param n the arity of the computed relation
 *
 * \pre solver != NULL
 * \pre the plugin implements the \ref genepi_plugin_top "top" function
 * \pre n > 0
 *
 * \return \f${\cal N}^n\f$, \f${\cal Z}^n\f$, \f${\cal R^+}^n\f$ or \f${\cal 
 * R}^n\f$ depending on the result of \ref genepi_solver_get_solver_type 
 * "get_solver_type".
 */
extern genepi_set *
genepi_set_top (genepi_solver *solver, int n);

/*!
 * \brief Computes the restriction of the complete relation of arity \a n to
 * positive or null integers. If the underlying plugin does not implement
 * the \ref genepi_plugin_top_N "top_N" function then, depending on its 
 * \ref genepi_solver_type "solver type" the function may raise an exception
 * or not.
 *
 * \param solver the GENEPI solver
 * \param n the arity of the computed relation
 *
 * \pre solver != NULL
 * \pre n > 0
 *
 * \return \f${\cal N}^n \cap \mbox{\tt top(solver,n)}\f$
 */
extern genepi_set *
genepi_set_top_N (genepi_solver *solver, int n);

/*!
 * \brief Computes the restriction of the complete relation of arity \a n to
 * integers. If the underlying plugin does not implement the \ref 
 * genepi_plugin_top_Z "top_Z" function then, depending on its \ref 
 * genepi_solver_type "solver type" the function may raise an exception
 * or not.
 *
 * \param solver the GENEPI solver
 * \param n the arity of the computed relation
 *
 * \pre solver != NULL
 * \pre n > 0
 *
 * \return \f${\cal Z}^n \cap \mbox{\tt top(solver,n)}\f$
 */
extern genepi_set *
genepi_set_top_Z (genepi_solver *solver, int n);

/*!
 * \brief Computes the restriction of the complete relation of arity \a n to
 * positive real values. If the underlying plugin does not implement the \ref 
 * genepi_plugin_top_P "top_P" function then a generic function is used.
 *
 * \param solver the GENEPI solver
 * \param n the arity of the computed relation
 *
 * \pre solver != NULL
 * \pre n > 0
 *
 * \return \f${\cal R^+}^n \cap \mbox{\tt top(solver,n)}\f$
 */
extern genepi_set *
genepi_set_top_P (genepi_solver *solver, int n);

/*!
 * \brief Computes the restriction of the complete relation of arity \a n to
 * real values. 
 *
 * \param solver the GENEPI solver
 * \param n the arity of the computed relation
 *
 * \pre solver != NULL
 * \pre n > 0
 *
 * \return \f${\cal R}^n \cap \mbox{\tt top(solver,n)}\f$
 */
extern genepi_set *
genepi_set_top_R (genepi_solver *solver, int n);

/*!
 * \brief Computes the empty relation of arity \a n.
 *
 * \param solver the GENEPI solver
 * \param n the arity of the computed relation
 *
 * \pre the plugin implements the \ref genepi_plugin_bot "bot" function
 * \pre n > 0
 *
 * \return the encoding of \f$\emptyset^n\f$
 */
extern genepi_set *
genepi_set_bot (genepi_solver *solver, int n);

/*!
 * \brief Computes the set of solutions of a linear equality.
 *
 * This function is a shortcut for:
 *  #genepi_set_linear_operation (solver, alpha, alpha_size, GENEPI_EQ, cst)
 *
 * \param solver the GENEPI solver
 * \param alpha the array of coefficient of the linear operation
 * \param alpha_size the number of coefficients
 * \param cst the constant term of the operation
 *
 * \return the solutions of \f$ \Sigma_{i=0..alpha\_size} alpha[i]\times 
 * x_i = cst\f$
 */
extern genepi_set *
genepi_set_linear_equality (genepi_solver *solver, const int *alpha, 
			    int alpha_size, int cst);

/*!
 * \brief Computes the set of solutions of a linear operation.
 *
 * According to the value of \a op this function returns the set of solutions
 * of the linear operation: 
 * \f[ \Sigma_{i=0..alpha\_size} alpha[i]\times x_i\mbox{ op }c\f]
 * with \a op interpreted on natural numbers.
 *
 * \param solver the GENEPI solver
 * \param alpha the array of coefficient of the linear operation
 * \param alpha_size the number of coefficients
 * \param c the constant term of the operation
 * \param op the identifier of the operation
 *
 * \pre solver != NULL
 * \pre the plugin implements the \ref genepi_plugin_linear_operation
 * "linear_operation" function
 * \pre alpha != NULL && alpha_size > 0
 *
 * \return the solutions of the specified linear operation.
 */
extern genepi_set *
genepi_set_linear_operation (genepi_solver *solver, const int *alpha, 
			     int alpha_size, genepi_comparator op, int c);

/*!
 * \brief Computes the union of two relations.
 * 
 * This function has to compute \f$X_1 \cup X_2\f$ where relations \f$ X_i\f$
 * are assumed to be of same arity. 
 * 
 * \param solver the GENEPI solver
 * \param X1 the first operand of the union
 * \param X2 the second operand of the union
 *
 * \pre solver != NULL
 * \pre the plugin implements the \ref genepi_plugin_set_union "union" function
 * \pre genepi_set_get_width (solver, X1) == genepi_set_get_width (solver, X2)
 *
 * \return the encoding of \f$X_1 \cup X_2\f$
 */
extern genepi_set *
genepi_set_union (genepi_solver *solver, genepi_set *X1, genepi_set *X2);

/*!
 * \brief Computes the intersection of two relations.
 *
 * This function has to compute \f$X_1 \cap X_2\f$ where relations \f$ X_i\f$
 * are assumed to be of same arity. 
 *
 * \param solver the GENEPI solver
 * \param X1 the first operand of the intersection
 * \param X2 the second operand of the intersection
 *
 * \pre solver != NULL
 * \pre the plugin implements the \ref genepi_plugin_set_intersection 
 * "intersection" function
 * \pre genepi_set_get_width (solver, X1) == genepi_set_get_width (solver, X2)
 *
 * \return the encoding of \f$X_1 \cap X_2\f$
 */
extern genepi_set *
genepi_set_intersection (genepi_solver *solver, genepi_set *X1, 
			 genepi_set *X2);

/*!
 * \brief Computes the complement of the set \a X
 *
 * This function computes the completement of \a X in the domain of variables
 * i.e. either \f$N\f$ or \f$Z\f$.
 *
 * \param solver the GENEPI solver
 * \param X the set to complement.
 *
 * \pre solver != NULL
 * \pre the plugin implements the \ref genepi_plugin_set_complement 
 * "complement" function
 * \pre X != NULL
 *
 * \return the encoding of \f$\overline{X}\f$
 */
extern genepi_set *
genepi_set_complement (genepi_solver *solver, genepi_set *X);

/*!
 * \brief Projects a relation on a subset of its columns.
 *
 * This function computes the projection of \a X on the columns selected by
 * the array of Booleans \a selection. The function computes:
 * \f[
 *  project(X) = \{ (x_{i_1}, \dots, x_{i_n}) \mid \exists v\in X, v[i_1] = 
 * x_{i_1}\wedge \dots\wedge v[i_n] = x_{i_n} \}\f]
 * where \f$ 0 \leq i_1 \leq\dots\leq i_n < size\f$ and for \f$j=1\dots n, 
 * selection[i_j] =0 \f$. 
 *
 * \param solver the GENEPI solver
 * \param X the relation to project
 * \param selection the selection of columns of X which must be kept by the
 *        projection.
 * \param size the number of Booleans in \a selection.
 *
 * \pre the plugin implements the \ref genepi_plugin_project "project" function
 * \pre selection != NULL
 * \pre size == genepi_set_get_width (solver, X)
 *
 * \return the encoding of the projection of \a X according to the selected 
 * columns in \a selection.
 *
 */
extern genepi_set *
genepi_set_project (genepi_solver *solver, genepi_set *X, 
		    const int *selection, int size);

/*!
 * \brief Adds columns to the relation \a X.
 *
 * This function add columns to \a X according to the array of Booleans \a 
 * selection. The arity of the resulting relation is \a size and the relation
 * itself is defined by:
 * \f[
 *   invproject(X,selection,size) = \{ (x_1,\dots,x_{size}) | \exists v\in X, 
 *    v[1] = x_{i_1}\wedge \dots\wedge v[n] = x_{i_n} \}
 *   \}
 * \f]
 * where \a n is the arity of \a X, \f$ 0 \leq i_1 \leq\dots\leq i_n < 
 * size\f$ and for \f$j=1\dots n, selection[i_j] = 0 \f$. 
 *
 * \param solver the GENEPI solver
 * \param X the relation where new columns are added
 * \param sel the specification of new columns
 * \param size number of column in the result.
 *
 * \pre the plugin implements the \ref genepi_plugin_invproject "invproject" 
 * function
 * \pre selection != NULL
 * \pre size >= genepi_set_get_width (solver, X)
 *
 * \return the encoding of \a X extended with additionnal columns specified in
 * \a selection.
 */
extern genepi_set *
genepi_set_invproject (genepi_solver *solver, genepi_set *X, const int *sel, 
		       int size);

/*!
 * \brief Computes <tt>post (R, A)</tt>
 *
 * This function applies the relation \a R to the set \a A. If \a n
 * is the width of \a A, the function assumes that 2*\a n is the width of \a R.
 * The function computes the set of vectors defined by:
 * \f[apply(R,A) = \{ (x_1^\prime,\dots, x_n^\prime) \mid \exists x_1,\dots 
 * x_n, (x_1^\prime,x_1,\dots,x_n^\prime,x_n)\in R \wedge (x_1,\dots, x_n) \in A\}\f]
 * 
 * Note the interleaving of primed and not primed variables and also note that
 * the primed variable precedes its non-primed version.
 * 
 * \param solver the GENEPI solver
 * \param R the transition relation
 * \param A the relation on which R is applied.
 *
 * \pre genepi_set_get_width (solver, R) = 2 * genepi_set_get_width 
 * (solver, A)
 *
 * \return a pointer to the structure encoding \c post(R,A)
 */
extern genepi_set *
genepi_set_apply (genepi_solver *solver, genepi_set *R, genepi_set *A);

/*!
 * \brief Computes <tt>pre (R, A)</tt>
 * 
 * This function is similar to \ref genepi_plugin_apply "apply" but
 * instead of computing the image of \a A by the relation \a R, it computes
 * the set whose image is \a A by \a R. Formally, the function computes the
 * set:
 *
 * \f[applyinv(R,A) = \{ (x_1,\dots, x_n) \mid \exists x_1^\prime,
 * \dots x_n^\prime, (x_1^\prime,x_1,\dots,x_n^\prime,x_n)\in R \wedge (x_1^\prime,\dots, x_n^\prime)\in A\}\f]
 * 
 * Note the interleaving of primed and not primed variables and also note that
 * the primed variable precedes its non-primed version.
 *
 * \param solver the GENEPI solver
 * \param R the transition relation;
 * \param A the image by R of the computed relation.
 *
 * \pre genepi_set_get_width (solver, R) = 2 * genepi_set_get_width (solver, A)
 *
 * \return a pointer to the structure encoding \c pre(R,A)
 */
extern genepi_set *
genepi_set_applyinv (genepi_solver *solver, genepi_set *R, genepi_set *A);

/*!\brief Compares \a X1 and \a X2 according to \a op.
 *
 * The function returns true if \a X1 and \a X2 verify the relation \a op
 * where the #genepi_comparator \a op is interpreted in the context of sets
 * inclusion.
 *
 * \param solver the GENEPI solver
 * \param X1 first operand of the comparison
 * \param X2 second operand of the comparison
 * \param op comparison specifier
 *
 * \pre solver != NULL
 * \pre the plugin implements the \ref genepi_plugin_compare "compare" function
 * \pre genepi_set_get_width (solver, X1) == genepi_set_get_width (solver, X2)
 *
 * \return a non null value if \a X1 \a op \a X2
 */
extern int
genepi_set_compare (genepi_solver *solver, genepi_set *X1, 
		    genepi_comparator op, genepi_set *X2);

/*!
 * \brief Checks the equality (mutual inclusion) of \a X1 and \a X2
 *
 * \param solver the GENEPI solver
 * \param X1 first argument of the comparison
 * \param X2 second argument of the comparison
 *
 * \return a non null value is the two sets are equal.
 *
 * \remarks is strictly equivalent to : #genepi_set_compare (X1, GENEPI_EQ, X2)
 * \see genepi_set_compare
 */
extern int
genepi_set_equal (genepi_solver *solver, genepi_set *X1, genepi_set *X2);

/*!
 * \brief Checks if \a X is empty.
 *
 * This function checks if the set represented by \a X is the empty relation
 * of arity \ref genepi_set_get_width "get_width (X)".
 *
 * \param solver the GENEPI solver
 * \param X the considered set
 *
 * \pre solver != NULL
 * \pre the plugin implements the \ref genepi_plugin_is_empty is_empty function
 * \pre X != NULL
 *
 * \return a non null value if \a X is empty.
 */
extern int
genepi_set_is_empty (genepi_solver *solver, genepi_set *X);

/*!
 * \brief Checks if \a X is the complete relation.
 *
 * This function checks if the set represented by \a X is the complete relation
 * of arity \ref genepi_set_get_width "get_width (X)".
 *
 * \param solver the GENEPI solver
 * \param X the tested relation
 * 
 * \pre solver != NULL
 * \pre the plugin implements the \ref genepi_plugin_is_full "is_full" 
 * function
 * \pre X != NULL
 *
 * \return a non null value if \a X is complete
 */
extern int 
genepi_set_is_full (genepi_solver *solver, genepi_set *X);

/*!
 * \brief Checks if the set is finite
 *
 * \param solver the GENEPI solver
 * \param X the considered set
 * 
 * \pre solver != NULL
 * \pre the plugin implements the \ref genepi_plugin_is_finite "is_finite" 
 * function
 * \pre X != NULL
 *
 * \return a non null value is the set \a X is finite
 */
extern int 
genepi_set_is_finite (genepi_solver *solver, genepi_set *X);

/*! 
 * \brief Checks if the relation \a X depends on a subset of its column.
 * 
 * \a X is said to be dependent of columns \f$i_1, \dots, i_n\f$ (where 
 * \f$i_j\f$ is such that \f$sel[i_j] \not=0\f$) if the projection of \a X 
 * these columns is not the complete relation of arity \a n. 
 *
 * \param solver the GENEPI solver
 * \param X the relation on which we check dependency.
 * \param sel an array of Booleans identifying the columns on which the 
 * dependency is checked.
 * \param selsize the number of cells in sel.
 *
 * \pre solver != NULL
 * \pre sel != NULL
 * \pre selsize == genepi_set_get_width (solver, X)
 */
extern int
genepi_set_depend_on (genepi_solver *solver, genepi_set *X, const int *sel, 
		      int selsize);

/*!
 * \brief Computes an explicit representation of X.
 *
 * If the plugin implements the function \ref genepi_plugin_get_solutions
 * "get_solutions" then this function computes an array of vectors representing
 * elements of the set \a X. Each vector is an array of integers allocated by 
 * the function. If \a X is an infinite set then \a max parameter is used to 
 * indicate the maximal number of vectors to return. The way the function 
 * choose the vectors is not specifed.
 *
 * The vectors are returned via the \a psolutions parameter and the number of
 * vectors is set into the variable pointed by \a psize.
 *
 * If the \a max if negative then all the solutions are returned.
 *
 * \param solver the GENEPI solver
 * \param X the set X
 * \param psolutions the address where the vectors are stored
 * \param psize the address of the integer receiving the number of solution
 * \param max the maximal number of solutions.
 *
 * \pre solver != NULL
 * \pre X != NULL
 * \pre psolutions != NULL
 * \pre psize != NULL
 * \pre max >= 0 || genepi_set_is_finite (solver, X))
 */
extern void
genepi_set_get_solutions (genepi_solver *solver, genepi_set *X, 
			  int ***psolutions, int *psize, int max);

/*!
 * \brief Displays all solutions of the set \a X.
 *
 * If the underlying plugin implements the function \ref
 * genepi_plugin_display_all_solutions "display_all_solutions" then this 
 * function displays all elements of \a X. If \a X is infinite and the plugin
 * does not display a symbolic representation of \a X then the program can 
 * enter an infinite loop.
 *
 * \param solver the GENEPI solver
 * \param X the considered relation
 * \param output the stream on which the solutions are displayed. 
 * \param varnames the names labelling the column of the relation; the size 
 *        of this array must be equal to the arity of the relation.
 * 
 * \pre solver != NULL
 * \pre X != NULL
 * \pre output != NULL
 * \pre varnames != NULL
 */
extern void
genepi_set_display_all_solutions (genepi_solver *solver, genepi_set *X, 
				  FILE *output, const char * const*varnames);

/*!
 * \brief Displays internal representation of \a X.
 * 
 * If the underlying plugin implements the \ref 
 * genepi_plugin_display_data_structure "display_data_structure" then this
 * function displays the data structure used to encode \a X.
 *
 * \param solver the GENEPI solver
 * \param X the considered relation
 * \param output the stream on which the data structure is displayed
 *
 * \pre solver != NULL
 * \pre X != NULL
 * \pre output != NULL
 */
extern void
genepi_set_display_data_structure (genepi_solver *solver, genepi_set *X, 
				   FILE *output);

/*! 
 * \brief Arity of the relation represented by \a X.
 *
 * \param solver the GENEPI solver
 * \param X the considered relation.
 *
 * \pre solver != NULL
 * \pre the plugin implements the \ref genepi_plugin_get_width "get_width"
 * function
 * \pre X != NULL
 *
 * \return the number of columns of the relation \a X.
 */
extern int
genepi_set_get_width (genepi_solver *solver, genepi_set *X);

/*!
 * \brief Size of the data structure encoding \a X.
 *
 * The result of this function is an integer indicating a \e size of the
 * data structure required to encode \a X. This is not necessary a number of
 * bytes (which have no sense with shared structures) but if the size of \a X
 * is greater than the size of \a Y then \a X requires much memory resources 
 * than \a Y.
 *
 * \param solver the GENEPI solver
 * \param X the considered relation.
 *
 * \pre solver != NULL
 * \pre the plugin implements the \ref genepi_plugin_get_data_structure_size 
 * "get_data_structure_size" function
 * \pre X != NULL
 *
 * \return an integer representing a size of the underlying encoding of \a X.
 */
extern int
genepi_set_get_data_structure_size (genepi_solver *solver, genepi_set *X);

/*! 
 * \brief Checks if \a x/\a xden belongs to \a X. 
 *
 * \param solver the GENEPI solver
 * \param X the considered relation
 * \param x the vector whose belonging is tested.
 * \param x_size the size of \a x
 * \param xden a common denominator of components of \a x
 *
 * \pre solver != NULL
 * \pre x != NULL
 * \pre x_size == genepi_set_get_width (solver, X)
 * \pre xden > 0
 *
 * \return a non null value if \a x /\a xden\ is an element of \a X.
 */
extern int
genepi_set_is_solution (genepi_solver *solver, genepi_set *X, const int *x, 
			int x_size, int xden);

/*! 
 * \brief Gets one element in \a X.
 *
 * The function fills the given vector \a x with an element of \a X if it is 
 * not empty. The way the function selects \a x is not specified and depends 
 * on the plugin implementation.
 *
 * \param solver the GENEPI solver
 * \param X the considered set
 * \param x the vector which receives the element of \a X
 * \param x_size the size of \a x
 *
 * \pre solver != NULL
 * \pre the plugin implements the \ref genepi_plugin_get_one_solution 
 * "get_one_solution" function.
 * \pre x_size == genepi_set_get_width (solver, X)
 *
 * \post result >= 0
 *
 * \retval 0 if \a X is empty.
 * \retval the common denominator of the solution \a x
 */
extern int
genepi_set_get_one_solution (genepi_solver *solver, genepi_set *X, int *x, 
			     int x_size);

/*!
 * \brief Dump the set \a X onto the \a output stream
 * 
 * This function can be used to serialize a set \a X onto an \a output stream. 
 * The set \a X can be NULL; in this case the NULL value is written on the 
 * stream. The function returns the number of bytes actually written. If
 * the result is negative or null then an error has occured and the returned
 * value has to be interpreted as a \ref genepi_io_error.
 *
 * \param solver the current GENEPI solver
 * \param output the stream where data are written.
 * \param set the dumped GENEPI set
 *
 * \pre solver != NULL
 * \pre the underlying plugin must implement the \ref genepi_plugin_write_set
 * "write_set" function.
 * \pre output != NULL
 *
 * \return the number of bytes written on \a output or an error code
 *
 * \see genepi_set_read
 */
extern int
genepi_set_write (genepi_solver *solver, FILE *output, genepi_set *set);

/*!
 * \brief Load on the \a input stream a serialized set
 * 
 * This function is used to reload a \a genepi_set serialized with the
 * \ref genepi_set_write write function. The loaded set is putted in \a 
 * *pset. 
 * The function returns the number of bytes actually readed. If a negative or 
 * null value is returned an error has occurred and the returned value has to 
 * be interpreted as \ref genepi_io_error.
 *
 * \param solver the current GENEPI solver
 * \param input the stream where the set is loaded
 * \param pset a pointer to the variable receiving the loaded set.
 *
 * \pre solver != NULL
 * \pre the underlying plugin must implement the \ref genepi_plugin_read_set
 * "read_set" function.
 * \pre input != NULL
 * \pre pset != NULL
 *
 * \return the number of bytes read on \a input or an error code
 *
 * \see genepi_set_write
 */
extern int
genepi_set_read (genepi_solver *solver, FILE *input, genepi_set **pset);

/*!
 * \brief Transformation of the relation X
 *
 * This function produces a new relation Y from X in the following way:
 * - the arity of Y is nb_indices
 * - for all i in { 0, ..., nb_indices}:
 *   . if indices[i] >= genepi_set_get_width (X) then Y[i] is set to \
 *     \ref genepi_set_top top()
 *   . if 0 <= indices[i] < genepi_set_get_width (X) then Y[i] = X[indices[i]].
 *   . if indices[i] < 0 then Y[i] is to the constant cst[abs(indices[i])-1]
 *     given by the array \a cst.
 * 
 * \param solver the current GENEPI solver
 * \param X the relation to transform
 * \param indices specification of new columns
 * \param nb_indices width of the result
 * \param cst constants use in the specification of new columns
 * \param nb_cst number of constant used
 *
 * \pre solver!= NULL 
 * \pre X != NULL 
 * \pre for all i, indices[i] < 0 => -indices[i]-1 < nb_cst
 */
extern genepi_set *
genepi_set_transform (genepi_solver *solver, genepi_set *X, int *indices, 
		      int nb_indices, int *cst, int nb_cst);


/*!
 * \brief Evaluate the pre-order over sets.
 * 
 * This function implements the pre-order relation \f$\preceq\f$ such that
 * for all sets \f$S_1\f$, \f$S_2\f$ and \f$S_3\f$:
 * <ul>
 * <li>\f$S_1\preceq S_2 \wedge S_2\preceq S_1 \iff S_1 \mbox{ and } S_2 
 * \mbox{ represent the same set}\f$
 * <li>\f$S_1\preceq S_2 \wedge S_2\preceq S_3 \Longrightarrow S_1\preceq S_3\f$
 * <li>\f$S_1\preceq S_2 \vee S_1\preceq S_2\f$
 * </ul>
 * \param X1 first operand
 * \param X2 second operand
 *
 * \pre X1 != NULL
 * \pre X2 != NULL
 *
 * \return a non null value of \a X1 \f$\preceq\f$ \a X2
 */
extern int
genepi_set_preorder (genepi_solver *solver, genepi_set *X1, genepi_set *X2);

END_C_DECLS

#endif /* ! __GENEPI_H__ */
