/*
 * genepi-solver.c -- The generic Solver data structure
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <assert.h>
#include "genepi-trace.h"
#include "genepi-p.h"

# define INIT_TIMER(_s,_t) s_init_timer (_s, T_ ## _t, # _t)

static void
s_init_timer (genepi_solver *solver, genepi_timed_method t, const char *name);

static void
s_begin_trace (genepi_solver *solver);

static void
s_end_trace (genepi_solver *solver);


			/* --------------- */

genepi_solver *
genepi_solver_create (genepi_plugin *plugin, int flags, int cachesize,
		      int number_of_cache_entries)
{
  genepi_solver *result = malloc (sizeof (genepi_solver));

  assert (plugin != NULL);

  result->flags = flags;
  if (cachesize > 0 || number_of_cache_entries > 0)
    result->cache = genepi_cache_create (result, cachesize, 
					 number_of_cache_entries);
  else
    result->cache = NULL;
  result->trace_file = NULL;
  result->last_set_index = 0;
  result->free_sets = NULL;
  
  if (flags & GENEPI_FLAG_SHOW_TIMERS)
    {      
      INIT_TIMER(result,LINEAR_OPERATION);
      INIT_TIMER(result,UNION);
      INIT_TIMER(result,INTERSECTION);
      INIT_TIMER(result,COMPLEMENT);
      INIT_TIMER(result,PROJECT);
      INIT_TIMER(result,INVPROJECT);
      INIT_TIMER(result,APPLY);
      INIT_TIMER(result,APPLYINV);
      INIT_TIMER(result,IS_EMPTY);
      INIT_TIMER(result,IS_FULL);
      INIT_TIMER(result,COMPARE);
      INIT_TIMER(result,TOP);
      INIT_TIMER(result,BOT);
      INIT_TIMER(result,DS_SIZE);
      INIT_TIMER(result,IS_SOLUTION);
      INIT_TIMER(result,GET_ONE_SOLUTION);
      INIT_TIMER(result,TOTAL_TIME);
      INIT_TIMER (result, DISPLAY_ALL_SOLUTIONS);
      START_TIMER(result,T_TOTAL_TIME);
    }
  result->plugin = genepi_plugin_add_reference (plugin);

  return result;
}

			/* --------------- */

static int
s_cmp_timers (const void *t1, const void *t2)
{
  genepi_timer *T1 = (genepi_timer *)t1;
  genepi_timer *T2 = (genepi_timer *)t2;

  if (T1->cumulated_time < T2->cumulated_time) 
    return 1;
  if (T1->cumulated_time > T2->cumulated_time) 
    return -1;

  return T2->number_of_calls-T1->number_of_calls;
}

			/* --------------- */

void
genepi_solver_delete (genepi_solver *solver)
{
  if ((solver->flags & GENEPI_FLAG_SHOW_TIMERS) != 0)
    {
      int i;
      const char *name = genepi_solver_get_name (solver);

      assert (name != NULL);

      END_TIMER (solver,T_TOTAL_TIME);

      qsort (solver->timers, NB_TIMERS, sizeof (genepi_timer), s_cmp_timers);
      printf ("\n");
      printf ("ENGINE = %s\n", name);
      printf ("%25s %10s %10s %13s\n","Operation","# calls", "s", "ms/c");
      printf ("---------------------------------------------------------------"
	      "-------\n");
      for (i = 0; i < NB_TIMERS; i++)
	{
	  genepi_timer *T = &solver->timers[i];

	  if (T->number_of_calls)
	    {
	      double s = (double)T->cumulated_time / CLOCKS_PER_SEC;
	      double uspc = 1000.0*s / T->number_of_calls;
	      
	      printf ("%25s %10d % 10.3f % 13.3f\n", T->name,
		      T->number_of_calls, s, uspc);		
	    }
	}
      printf ("---------------------------------------------------------------"
	      "-------\n");
      printf ("\n");
    }

  if ((solver->flags & GENEPI_FLAG_SHOW_CACHE_INFO) != 0 && 
      solver->cache != NULL)
    {
      const genepi_cache_info *info = genepi_cache_get_info (solver->cache);

      printf ("GENEPI cache usage:\n"
	      "-------------------\n"
	      "number of entries : %d\n"
	      "requests          : %d\n"
	      "collisions        : %d\n"
	      "success           : %d\n"
	      "fill rate         : %f\n"
	      "memory usage      : %d bytes\n"
	      "\n",
	      info->number_of_entries, info->requests, info->collisions,
	      info->success, 
	      (float) info->filled_entries / (float) info->number_of_entries,
	      info->number_of_bytes);
	      
    }
  if (solver->trace_file != NULL)
    genepi_solver_set_trace_file (solver, NULL);

  if (solver->cache != NULL)
    genepi_cache_delete (solver->cache);

  genepi_plugin_del_reference (solver->plugin);
  
  free (solver);
}

		       /* --------------- */

genepi_solver_type 
genepi_solver_get_solver_type (genepi_solver *solver)
{
  assert (solver != NULL);

  if (solver->plugin->get_solver_type != NULL)
    return solver->plugin->get_solver_type (solver->plugin->impl);

  return GENEPI_N_SOLVER;
}

			/* --------------- */

const genepi_plugconf *
genepi_solver_get_plugin_config (genepi_solver *solver)
{
  assert (solver != NULL);
  
  return solver->plugin->conf;
}

			/* --------------- */

const char *
genepi_solver_get_name (genepi_solver *solver)
{
  assert (solver != NULL);

  return genepi_plugconf_get_entry (solver->plugin->conf, CONF_PLUGIN_NAME,
				    NULL);
}

			/* --------------- */

void
genepi_solver_set_trace_file (genepi_solver *solver, FILE *output)
{
  assert (solver != NULL);

  if (solver->trace_file != NULL)
    {
      genepi_set *s;
      genepi_set *next;

      s_end_trace (solver);
      for (s = solver->free_sets; s; s = next)
	{
	  next = s->next;
	  free (s);
	}
    }

  solver->trace_file = output;
  solver->last_set_index = 0;
  solver->free_sets = NULL;
  
  if (solver->trace_file != NULL)
    s_begin_trace (solver);
}

		       /* --------------- */

FILE *
genepi_solver_get_trace_file (genepi_solver *solver)
{
  assert (solver != NULL);

  return solver->trace_file;
}


			/* --------------- */

static void
s_init_timer(genepi_solver *solver, genepi_timed_method t, const char *name)
{
  genepi_timer *T = &solver->timers[t];

  T->name = name;
  T->chronometer = 0;
  T->number_of_calls = 0;
  T->cumulated_time = 0;
}

			/* --------------- */


static void
s_begin_trace (genepi_solver *solver)
{
  const char *id = genepi_solver_get_name (solver);

  assert (id != NULL);

  fprintf (solver->trace_file, "%s\n", id);
}

			/* --------------- */

static void
s_end_trace (genepi_solver *solver)
{
  fprintf (solver->trace_file, "%d %d\n", GENEPI_TRACE_CHK_NB_VARS, 
	   solver->last_set_index);
  fflush (solver->trace_file);     
}
