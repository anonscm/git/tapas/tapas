/*
 * genepi-loader.h -- Dynamic module loader for GENEPI.
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!\file genepi/genepi-loader.h
 * \brief Plugin Loader
 *
 * The GENEPI loader gathers functions used to dynamically load GENEPI 
 * plugins. A plugin is a piece of code implementing functionslisted into the 
 * file genepi-plugin.h. Since, at first, plugins were destinated to the
 * FASTer tool (http://altarica.labri.fr/Tools/FASTer) a subset of these 
 * functions is mandatory in order to not crash the tool.
 * 
 * Before any use, the loader has to be initialized using the \ref 
 * genepi_loader_init "init" function. This function has to be called prior 
 * to any module loading. Before the program termination you can use \ref 
 * genepi_loader_terminate "terminate" to clean-up all ressources used by the 
 * loader and the loaded plugins.
 *
 * The GENEPI package comes with a set of plugins. The installation of these 
 * plugins depends on the existence or not of external packages (e.g. 
 * MONA or PresTAF). If such package is present and the corresponding plugin
 * is compiled then it is installed in some specific directory (this 
 * directory can be obtained using the function \ref 
 * genepi_loader_get_default_directory "get_default_directory"). These plugins 
 * are the called \e default plugins of the GENEPI library. They are not loaded
 * at start-up by \ref genepi_loader_init "init"; they have to be explicitly 
 * loaded using the \ref genepi_loader_load_default_plugins 
 * "load_default_plugins" function.
 *
 * External GENEPI plugin can be loaded between \ref genepi_loader_init "init"
 * and \ref genepi_loader_terminate "terminate" calls. A plugin is loaded using
 * the \ref genepi_loader_get_plugin "get_plugin" function which returns an
 * handler (#genepi_plugin) to the plugin. This function is also used to 
 * retrieve the handler of an already loaded plugin (e.g. the default ones).
 * The names of all loaded plugins can be obtained using the \ref 
 * genepi_loader_get_plugins "get_plugins" function; each name returned by this
 * function can be used in conjunction with \ref genepi_loader_get_plugin 
 * "get_plugin" to retrieve the handler of a specific plugin.
 */
#ifndef __GENEPI_LOADER_H__
# define __GENEPI_LOADER_H__

BEGIN_C_DECLS

/*!
 * \brief The abstract type for a GENEPI plugin that implements sufficient
 * functions to resolve Presburger formulas.
 *
 * The structure \em genepi_plugin_st is defined by the plugin and
 * no particular constraint is set on its size or content.
 */
typedef struct genepi_plugin_st genepi_plugin;

/*! 
 * \brief Initialize the plugin loader.
 * 
 * This function initializes ressources required by the plugin loader of the
 * GENEPI library. At this stage no plugin is dynamically loaded. Several 
 * calls to this function can done but only one initialization is actually 
 * realized.
 *
 * \see genepi_loader_terminate, genepi_loader_load_default_plugins
 */
extern void
genepi_loader_init (void);

/*! 
 * \brief Release ressources allocated by the plugin loader.
 * 
 * This function releases any ressources allocated for loaded plugins. In 
 * particular the \ref genepi_plugin_terminate "terminate()" function 
 * is called on each plugin implementing it.
 * 
 * Since the loader tracks the number of calls to genepi_loader_init(), this
 * function has to be called as many times than \ref genepi_loader_init.
 *
 * \see genepi_loader_init
 */
extern void
genepi_loader_terminate (void);

/*! 
 * \brief Returns the (default) GENEPI plugin are installed.
 * 
 * The GENEPI library is packaged with some plugins (e.g Prestaf, Mona). These
 * plugins are installed into the directory 
 * <tt><em>installation-prefix</em>/lib/genepi</tt>. This function returns the 
 * path to this directory.
 *
 * \return The path to the plugin directory
 */
extern const char *
genepi_loader_get_default_directory (void);

/*! 
 * \brief Request the loader to load all default GENEPI plugins.
 *
 * This function is almost equivalent to the following code (except that it 
 * checks if the function has already been called) which requests the loader
 * to load all default plugins.
 * \code
 * char *dir = genepi_loader_get_default_directory ();
 * genepi_loader_load_directory (dir);
 * \endcode
 */
extern void
genepi_loader_load_default_plugins (void);

/*!
 * \brief Load the plugin specified by \a name.
 *
 * First the function looks for \a name into its internal list of currently
 * loaded plugins. If it finds a plugin with same name or filename then
 * the function returns it. If the \a name is not found then the loader tries
 * to dynamically load a module whose filename is \a name. If the file exists
 * then the module is loaded using the LTDL library (the Libtool wrapper to
 * \c dlopen). 
 *
 * If the module filename (i.e. \a name) is of the form
 * <tt>some/path/somename.la</tt> then the \c somename becomes the nickname of 
 * the plugin that can be used as a prefix when looking for a function is the
 * symbol table.
 *
 * Once the module is loaded into memory, the function checks that mandatory 
 * functions are actually implemented by the module (e.g 
 * \ref genepi_plugin_terminate "top()" or 
 * \ref genepi_plugin_terminate "linear_operation()"); if it is not
 * the case the module is considered as invalid and unloaded.
 * The module is also unloaded if it implements the \ref 
 * genepi_plugin_init "init()" function and this one returns a nul 
 * value.
 *
 * \param name the name or filename of the plugin.
 *
 * \return a pointer to the selected plugin or NULL if it can not be
 * found in existing ones or can not be loaded.
 *
 * \see The Libtool project: http://www.gnu.org/software/libtool/.
 * \see The Programming Interface of GENEPI plugins: genepi-plugin.h.
 */
extern genepi_plugin *
genepi_loader_get_plugin (const char *name);

/*!
 * \brief Load any plugin presents in the directory \a dirname.
 *
 * This functions looks for Libtool modules (i.e files terminated with the
 * <tt>.la</tt> extension) into the directory \a dirname. For each matching 
 * filename the loader applies genepi_loader_get_plugin ().
 * 
 * \param dirname the path to the directory
 *
 * \retval "a non-null integer" if the \a dirname has been successfully 
 *          opened and visited.
 * \retval 0 if an error occurs.
 */
extern int
genepi_loader_load_directory (const char *dirname);

/*!
 * \brief Returns the names of all currently loaded plugins. 
 * 
 * \param psize is a pointer to an integer that receives the size of the result
 * i.e. the number of loaded plugins.
 * 
 * \warning the array and each one of its elements have to be <tt>free</tt>d.
 *
 * \return a pointer to an array containing the names or filenames of the
 * loaded plugins.
 */
extern char **
genepi_loader_get_plugins (int *psize);

/*! 
 * \brief Returns the nickname of the given \a plugin.
 *
 * If the plugin has been loaded from a Libtool module then this function
 * returns the shortname associated by the LTDL library to this module.
 *
 * \param plugin the considered plugin
 *
 * \return the nickname of the \a plugin or NULL if the \a plugin has no 
 * nickname.
 *
 * \see genepi_loader_get_plugin()
 */
extern const char *
genepi_plugin_get_name(const genepi_plugin *plugin);

extern genepi_plugin *
genepi_plugin_add_reference (genepi_plugin *plugin);

extern void
genepi_plugin_del_reference (genepi_plugin *plugin);

END_C_DECLS

#endif /* ! __GENEPI_LOADER_H__ */
