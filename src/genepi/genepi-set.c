/*
 * genepi-set.c -- Implementation of genepi_set related functions.
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <time.h>
#include <stdlib.h>
#include <assert.h>
#include "genepi-trace.h"
#include "genepi-p.h"
#include "genepi-io.h"

# define have_same_width(solver, X1, X2) \
  (genepi_set_get_width (solver, X1) ==	genepi_set_get_width (solver, X2))


# define TIMED_STMT(_s,_t,_stmt)					\
  do									\
    {									\
      int timers_on__ = ((_s)->flags & GENEPI_FLAG_SHOW_TIMERS);	\
      if (timers_on__)							\
	START_TIMER (_s,_t);						\
      _stmt;								\
      if (timers_on__)							\
	END_TIMER (_s,_t);						\
    }									\
  while (0)
      


			/* --------------- */

static genepi_set *
s_allocate_set (genepi_solver *solver);

static void
s_delete_set (genepi_solver *solver, genepi_set *X);

static void
s_check_size (genepi_solver *solver, genepi_set *X);

			/* --------------- */

genepi_set *
genepi_set_add_reference (genepi_solver *solver, genepi_set *X)
{
  assert (solver != NULL);
  assert (X != NULL);
  assert (X->refcount > 0);

  if (solver->trace_file != NULL)
    {
      fprintf (solver->trace_file, "%d %d\n", GENEPI_TRACE_ADD_REF, X->index);
      s_check_size (solver,X);
    }

  X->refcount++;

  return X;
}

			/* --------------- */

void
genepi_set_del_reference (genepi_solver *solver, genepi_set *X)
{
  assert (solver != NULL);
  assert (X != NULL);
  assert (X->refcount > 0);


  if (solver->trace_file != NULL)
    {
      s_check_size (solver, X);
      fprintf (solver->trace_file, "%d %d\n", GENEPI_TRACE_DEL_REF, X->index);
    }

  X->refcount--;

  if (X->refcount == 0)
    {
      if (solver->plugin->del_reference != NULL)
	solver->plugin->del_reference (solver->plugin->impl, X->impl);
      s_delete_set (solver, X);
    }
}

			/* --------------- */

genepi_set *
genepi_set_top (genepi_solver *solver, int width)
{
  genepi_set *result = s_allocate_set (solver);

  check_method (solver, top);
  assert (width > 0);

  TIMED_STMT (solver, T_TOP, 
	      result->impl = solver->plugin->top (solver->plugin->impl, width));

  if (solver->trace_file != NULL)
    {
      fprintf (solver->trace_file, "%d %d %d\n", GENEPI_TRACE_TOP, 
	       result->index, width);
      s_check_size (solver, result);
    }
  
  return result;
}

			/* --------------- */

static genepi_set *
s_top_X (genepi_solver *solver, int width, 
	 genepi_set_impl *(*top_method)(genepi_plugin_impl *, int),
	 genepi_set_impl *(*cache_get)(genepi_cache *,int),
	 void (*cache_put)(genepi_cache *,int, genepi_set_impl *),
	 genepi_set *(generic_top) (genepi_solver *, int),
	 genepi_trace_command trace_cmd)
	 
{
  genepi_set_impl *impl;
  genepi_set *result;
  FILE *trace_file = solver->trace_file;

  assert (solver != NULL);
  assert (width > 0);

  solver->trace_file = NULL;

  impl = cache_get (solver->cache, width);
  if (impl != NULL)
    {
      result = s_allocate_set (solver);
      result->impl = impl;
    }
  else if (top_method != NULL)
    {
      result = s_allocate_set (solver);
      result->impl = top_method (solver->plugin->impl, width);
      cache_put (solver->cache, width, result->impl);
    }
  else
    {
      result = generic_top (solver, width);
      cache_put (solver->cache, width, result->impl);
    }

  solver->trace_file = trace_file;
  if (solver->trace_file != NULL)
    {
      fprintf (solver->trace_file, "%d %d %d\n", trace_cmd, result->index, 
	       width);
      s_check_size (solver, result);
    }

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_top_N(genepi_solver *solver, int width)
{
  return s_top_X (solver, width, solver->plugin->top_N, genepi_cache_get_top_N,
		  genepi_cache_put_top_N, genepi_set_generic_top_N,
		  GENEPI_TRACE_TOP_N);
}

			/* --------------- */

genepi_set *
genepi_set_top_Z(genepi_solver *solver, int width)
{
  return s_top_X (solver, width, solver->plugin->top_Z, genepi_cache_get_top_Z,
		  genepi_cache_put_top_Z, genepi_set_generic_top_Z,
		  GENEPI_TRACE_TOP_Z);
}

			/* --------------- */

genepi_set *
genepi_set_top_P(genepi_solver *solver, int width)
{
  return s_top_X (solver, width, solver->plugin->top_P, genepi_cache_get_top_P,
		  genepi_cache_put_top_P, genepi_set_generic_top_P,
		  GENEPI_TRACE_TOP_P);
}

			/* --------------- */

genepi_set *
genepi_set_top_R(genepi_solver *solver, int width)
{
  genepi_set *result = NULL;
  FILE *trace_file = solver->trace_file;

  assert (solver != NULL);
  assert (width > 0);

  solver->trace_file = NULL;
  result = genepi_set_top (solver, width);

  solver->trace_file = trace_file;
  if (solver->trace_file != NULL)
    {
      fprintf (solver->trace_file, "%d %d %d\n", GENEPI_TRACE_TOP_R, 
	       result->index, width);
      s_check_size (solver, result);
    }

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_bot (genepi_solver *solver, int width)
{
  genepi_set *result = s_allocate_set (solver);

  assert (solver->plugin->bot != NULL);
  assert (width > 0);

  TIMED_STMT (solver, T_BOT, 
	      result->impl = solver->plugin->bot (solver->plugin->impl, width));

  if (solver->trace_file != NULL)
    {
      fprintf (solver->trace_file, "%d %d %d\n", GENEPI_TRACE_BOT, 
	       result->index, width);
      s_check_size (solver, result);
    }

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_linear_equality (genepi_solver *solver, const int *alpha, 
			    int alpha_size, int cst)
{
  return 
    genepi_set_linear_operation (solver, alpha, alpha_size, GENEPI_EQ, cst);
}

			/* --------------- */

genepi_set *
genepi_set_linear_operation (genepi_solver *solver, const int *alpha, 
			     int alpha_size, genepi_comparator op, int cst)
{
  genepi_set *result;

  assert (solver->plugin->linear_operation != NULL);
  assert (alpha_size > 0);
  assert (alpha != NULL);

  result = s_allocate_set (solver);
  result->impl = 
    genepi_cache_get_linear_operation (solver->cache, alpha, alpha_size, op, 
				       cst);

  if (result->impl == NULL)
    {
      TIMED_STMT (solver, T_LINEAR_OPERATION,
		  result->impl = 
		  solver->plugin->linear_operation (solver->plugin->impl, 
						    alpha, 
						    alpha_size, 
						    op, cst));
      genepi_cache_put_linear_operation (solver->cache, alpha, alpha_size, op, 
					 cst, result->impl);
    }

  if (solver->trace_file != NULL)
    {
      int i;
      const char *fmt = "%d ";
      
      fprintf (solver->trace_file, "%d %d %d %d %d ", GENEPI_TRACE_LINEAR, 
	       result->index, op, cst, alpha_size);
      
      for (i = 0; i < alpha_size; i++)
	{
	  if (i == alpha_size-1)  
	    fmt = "%d";
	  fprintf (solver->trace_file, fmt, alpha[i]);
	}
      fprintf (solver->trace_file, "\n");
      s_check_size (solver, result);
    }

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_union (genepi_solver *solver, genepi_set *X1, genepi_set *X2)
{
  DECLARE_FUNC_ID(op_union);
  genepi_set *result;
  
  assert (solver->plugin->set_union != NULL);
  assert (have_same_width (solver, X1, X2));
  
  result = s_allocate_set (solver);
  result->impl = 
    genepi_cache_get_binary (solver->cache, op_union, X1->impl, X2->impl);
  
  if (result->impl == NULL )
    {
      TIMED_STMT (solver, T_UNION, 
		  result->impl = 
		  solver->plugin->set_union (solver->plugin->impl, 
					     X1->impl, 
					     X2->impl));
      genepi_cache_put_binary (solver->cache, op_union, X1->impl, X2->impl, 
			       result->impl);
    }


  if (solver->trace_file != NULL)
    {
      s_check_size (solver, X1);
      s_check_size (solver, X2);
      fprintf (solver->trace_file, "%d %d %d %d\n", GENEPI_TRACE_UNION, 
	       result->index, X1->index, X2->index);
      s_check_size (solver, result);
    }

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_intersection (genepi_solver *solver, genepi_set *X1, genepi_set *X2)
{
  DECLARE_FUNC_ID(op_inter);
  genepi_set *result;

  assert (solver->plugin->set_intersection != NULL);
  assert (have_same_width (solver, X1, X2));

  result = s_allocate_set (solver);
  result->impl = 
    genepi_cache_get_binary (solver->cache, op_inter, X1->impl, X2->impl);

  if (result->impl == NULL)
    {
      TIMED_STMT (solver, T_INTERSECTION,
		  result->impl = 
		  solver->plugin->set_intersection (solver->plugin->impl, 
						    X1->impl, 
						    X2->impl));
      genepi_cache_put_binary (solver->cache, op_inter, X1->impl, X2->impl, 
			       result->impl);
    }

  if (solver->trace_file != NULL)
    {
      s_check_size (solver, X1);
      s_check_size (solver, X2);
      fprintf (solver->trace_file, "%d %d %d %d\n", 
	       GENEPI_TRACE_INTERSECTION, result->index, X1->index, 
	       X2->index);
      s_check_size (solver, result);
    }

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_complement (genepi_solver *solver, genepi_set *X)
{
  DECLARE_FUNC_ID(op_comp);
  genepi_set *result;

  assert (solver->plugin->set_complement != NULL);
  assert (X != NULL);

  result = s_allocate_set (solver);
  result->impl = genepi_cache_get_unary (solver->cache, op_comp, X->impl);

  if (result->impl == NULL)
    {
      TIMED_STMT (solver, T_COMPLEMENT,
		  result->impl = 
		  solver->plugin->set_complement (solver->plugin->impl, 
						  X->impl));
      genepi_cache_put_unary (solver->cache, op_comp, X->impl, result->impl);
    }

  if (solver->trace_file != NULL)
    {
      s_check_size (solver, X);
      fprintf (solver->trace_file, "%d %d %d\n", GENEPI_TRACE_COMPLEMENT, 
	       result->index, X->index);
      s_check_size (solver, result);
    }

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_project (genepi_solver *solver, genepi_set *X, const int *sel, 
		    int selsize)
{
  genepi_set *result;

  assert (solver->plugin->project != NULL);
  assert (sel != NULL);
  assert (selsize == genepi_set_get_width (solver, X));

  result = s_allocate_set (solver);
  result->impl = 
    genepi_cache_get_project (solver->cache, X->impl, sel, selsize, 0);
  
  if (result->impl == NULL )
    {
      TIMED_STMT (solver, T_PROJECT, 
		  result->impl = 
		  solver->plugin->project (solver->plugin->impl, X->impl, sel, 
					   selsize));
      genepi_cache_put_project (solver->cache, X->impl, sel, selsize, 0, 
				result->impl);
    }

  if (solver->trace_file != NULL)
    {
      int i;
      const char *fmt = "%d ";

      s_check_size (solver, X);
      fprintf (solver->trace_file, "%d %d %d %d ", GENEPI_TRACE_PROJECT, 
	       result->index, X->index, selsize);
      for (i = 0; i < selsize; i++)
	{
	  if (i == selsize-1)
	    fmt = "%d";

	  fprintf (solver->trace_file, fmt, sel[i]);
	}
      fprintf (solver->trace_file, "\n");

      s_check_size (solver, result);
    }

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_invproject (genepi_solver *solver, genepi_set *X, const int *sel, 
		       int selsize)
{
  genepi_set *result;

  assert (solver->plugin->invproject != NULL);
  assert (sel != NULL);
  assert (selsize >= genepi_set_get_width (solver, X));

  result = s_allocate_set (solver);
  result->impl = 
    genepi_cache_get_project (solver->cache, X->impl, sel, selsize, 1);
  
  if (result->impl == NULL)
    {
      TIMED_STMT (solver, T_INVPROJECT, 
		  result->impl = 
		  solver->plugin->invproject (solver->plugin->impl, X->impl, 
					      sel, selsize));
      genepi_cache_put_project (solver->cache, X->impl, sel, selsize, 1, 
				result->impl);
    }

  if (solver->trace_file != NULL)
    {
      int i;
      const char *fmt = "%d ";
	  
      s_check_size (solver, X);
      fprintf (solver->trace_file, "%d %d %d %d ", GENEPI_TRACE_INVPROJECT,
	       result->index, X->index, selsize);
      for (i = 0; i < selsize; i++)
	{
	  if (i == selsize-1)
	    fmt = "%d";
	      
	  fprintf (solver->trace_file, fmt, sel[i]);
	}
      fprintf (solver->trace_file, "\n");
	  
      s_check_size (solver, result);
    }

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_apply (genepi_solver *solver, genepi_set *R, genepi_set *A)
{
  DECLARE_FUNC_ID(op_apply);
  genepi_set *result;
  genepi_set_impl *impl;
  FILE *trace_file = solver->trace_file;

  assert (solver != NULL);
  assert (genepi_set_get_width (solver, R) == 
	  2 * genepi_set_get_width (solver, A));

  solver->trace_file = NULL;

  impl = genepi_cache_get_binary (solver->cache, op_apply, R->impl, A->impl);
  if (impl == NULL)
    {
      if (solver->plugin->apply == NULL)
	{
	  result = genepi_set_generic_apply (solver, R, A);
	}
      else
	{
	  result = s_allocate_set (solver);
	  
	  TIMED_STMT(solver, T_APPLY,
		     result->impl = 
		     solver->plugin->apply (solver->plugin->impl, R->impl, 
					    A->impl));
	}
      genepi_cache_put_binary (solver->cache, op_apply, R->impl, A->impl, 
			       result->impl);
    }
  else
    {
      result = s_allocate_set (solver);
      result->impl = impl;
    }

  solver->trace_file = trace_file;
  if (solver->trace_file != NULL)
    {
      s_check_size (solver, R);
      s_check_size (solver, A);
      fprintf (solver->trace_file, "%d %d %d %d\n", GENEPI_TRACE_APPLY,
	       result->index, R->index, A->index);
      s_check_size (solver, result);
    }

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_applyinv (genepi_solver *solver, genepi_set *R, genepi_set *A)
{
  DECLARE_FUNC_ID(op_applyinv);
  genepi_set *result;
  genepi_set_impl *impl;
  FILE *trace_file = solver->trace_file;

  assert (solver != NULL);
  assert (genepi_set_get_width (solver, R) == 
	  2 * genepi_set_get_width (solver, A));

  solver->trace_file = NULL;

  impl = genepi_cache_get_binary (solver->cache, op_applyinv, R->impl, 
				  A->impl);
  if (impl == NULL)
    {
      if (solver->plugin->apply == NULL)
	{
	  result = genepi_set_generic_applyinv (solver, R, A);
	}
      else
	{
	  result = s_allocate_set (solver);
	  TIMED_STMT (solver, T_APPLYINV,
		      result->impl = 
		      solver->plugin->applyinv (solver->plugin->impl, R->impl, 
						A->impl));
	}
      genepi_cache_put_binary (solver->cache, op_applyinv, R->impl, A->impl, 
			       result->impl);
    }
  else
    {
      result = s_allocate_set (solver);
      result->impl = impl;
    }
	  
  solver->trace_file = trace_file;
  if (solver->trace_file != NULL)
    {
      s_check_size (solver, R);
      s_check_size (solver, A);
      fprintf (solver->trace_file, "%d %d %d %d\n", GENEPI_TRACE_APPLYINV, 
	       result->index, R->index, A->index);
      s_check_size (solver, result);
    }

  return result;
}

			/* --------------- */

int
genepi_set_compare (genepi_solver *solver, genepi_set *X1, 
		    genepi_comparator op, genepi_set *X2)
{
  int result;

  assert (solver != NULL);
  assert (solver->plugin->compare != NULL);
  assert (have_same_width (solver, X1, X2));

  TIMED_STMT (solver, T_COMPARE, 
	      result = solver->plugin->compare (solver->plugin->impl, X1->impl,
						op, X2->impl));

  if (solver->trace_file != NULL)
    {
      s_check_size (solver, X1);
      s_check_size (solver, X2);
      fprintf (solver->trace_file, "%d %d %d %d %d\n", 
	       GENEPI_TRACE_CHK_COMPARE, op, X1->index, X2->index, result?1:0);
    }

  return result;
}

			/* --------------- */

int
genepi_set_equal (genepi_solver *solver, genepi_set *X1, genepi_set *X2)
{
  return genepi_set_compare (solver, X1, GENEPI_EQ, X2);
}
			/* --------------- */

int
genepi_set_is_empty (genepi_solver *solver, genepi_set *X)
{
  int result;

  assert (solver != NULL);
  assert (solver->plugin->is_empty != NULL);
  assert (X != NULL);

  TIMED_STMT (solver, T_IS_EMPTY,
	      result = solver->plugin->is_empty (solver->plugin->impl, 
						 X->impl));

  if (solver->trace_file != NULL)
    {
      s_check_size (solver, X);      
      fprintf (solver->trace_file, "%d %d %d\n", GENEPI_TRACE_CHK_EMPTY, 
	       X->index, result?1:0);
    }

  return result;
}

			/* --------------- */

int 
genepi_set_is_full (genepi_solver *solver, genepi_set *X)
{
  int result;
  FILE *trace_file = solver->trace_file;

  assert (solver != NULL);
  assert (X != NULL);

  solver->trace_file = NULL;
  if (solver->plugin->is_full == NULL)
    result = genepi_set_generic_is_full (solver, X);
  else
    {
      TIMED_STMT (solver, T_IS_FULL,
		  result = solver->plugin->is_full (solver->plugin->impl, 
						    X->impl));
    }

  solver->trace_file = trace_file;
  if (solver->trace_file != NULL)
    {
      s_check_size (solver, X);
      fprintf (solver->trace_file, "%d %d %d\n", GENEPI_TRACE_CHK_FULL, 
	       X->index, result?1:0);
    }

  return result;
}

			/* --------------- */

int 
genepi_set_is_finite (genepi_solver *solver, genepi_set *X)
{
  int result;

  assert (solver != NULL);
  assert (solver->plugin->is_finite != NULL);
  assert (X != NULL);

  result = solver->plugin->is_finite (solver->plugin->impl, X->impl);

  if (solver->trace_file != NULL)
    {
      s_check_size (solver, X);
      fprintf (solver->trace_file, "%d %d %d\n", GENEPI_TRACE_CHK_FINITE, 
	       X->index, result?1:0);
    }

  return result;
}

			/* --------------- */

int
genepi_set_depend_on (genepi_solver *solver, genepi_set *X, const int *sel, 
		      int selsize)
{
  int result;
  FILE *trace_file = solver->trace_file;

  assert (solver != NULL);
  assert (sel != NULL);
  assert (selsize == genepi_set_get_width (solver, X));

  solver->trace_file = NULL;
  if (solver->plugin->depend_on == NULL )
    result = genepi_set_generic_depend_on (solver, X, sel, selsize);
  else
    result = solver->plugin->depend_on (solver->plugin->impl, X->impl, sel, 
					selsize);

  solver->trace_file = trace_file;

  return result;
}

			/* --------------- */

void
genepi_set_get_solutions (genepi_solver *solver, genepi_set *X, 
			  int ***psolutions, int *psize, 
			  int max)
{
  assert (solver != NULL);
  assert (X != NULL);
  assert (psolutions != NULL);
  assert (psize != NULL);
  assert (max >= 0 || genepi_set_is_finite (solver, X));

  if (solver->plugin->get_solutions != NULL)
    solver->plugin->get_solutions (solver->plugin->impl, X->impl, psolutions, 
				   psize, max);
  else
    {
      *psize = 0;
      *psolutions = 0;
    }
}

			/* --------------- */

void
genepi_set_display_all_solutions (genepi_solver *solver, genepi_set *X, 
				  FILE *output, const char * const *varnames)
{
  assert (solver != NULL);
  assert (X != NULL);
  assert (output != NULL);

  if (solver->plugin->display_all_solutions != NULL)
    {
      TIMED_STMT (solver, T_DISPLAY_ALL_SOLUTIONS,
		  solver->plugin->display_all_solutions (solver->plugin->impl, 
							 X->impl, 
							 output, varnames));
    }
}

			/* --------------- */

void
genepi_set_display_data_structure (genepi_solver *solver, genepi_set *X, 
				   FILE *output)
{
  assert (solver != NULL);
  assert (X != NULL);
  assert (output != NULL);

  if (solver->plugin->display_data_structure != NULL)
    solver->plugin->display_data_structure (solver->plugin->impl, X->impl, 
					    output);
}

			/* --------------- */

int
genepi_set_get_width (genepi_solver *solver, genepi_set *X)
{
  int result;

  assert (solver != NULL);
  assert (X != NULL);
  assert (solver->plugin->get_width != NULL);

  result = solver->plugin->get_width (solver->plugin->impl, X->impl);
  if (solver->trace_file != NULL)
    {
      s_check_size (solver, X);
      fprintf (solver->trace_file, "%d %d %d\n", GENEPI_TRACE_CHK_WIDTH, 
	       X->index, result);
    }

  return result;
}

			/* --------------- */

int
genepi_set_get_data_structure_size (genepi_solver *solver, genepi_set *X)
{
  int result;

  assert (solver != NULL);
  assert (X != NULL);
  assert (solver->plugin->get_data_structure_size != NULL);
  
  TIMED_STMT (solver, T_DS_SIZE,
	      result = 
	      solver->plugin->get_data_structure_size (solver->plugin->impl, 
						       X->impl));

  if (solver->trace_file != NULL)
    {
      s_check_size (solver, X);
    }

  return result;
}

			/* --------------- */


static int 
s_is_solution (genepi_solver *solver, genepi_set *X, const int *x, int x_size,
	       int xden)
{
  int result = 1;

  assert (solver != NULL);  
  assert (x != NULL);
  assert (x_size == genepi_set_get_width (solver, X));
  assert (xden > 0);

  if (genepi_solver_get_solver_type (solver) == GENEPI_N_SOLVER)
    {
      int i;
      
      for (i = 0; result && i < x_size; i++)
	if (x[i] < 0)
	  result = 0;
    }

  if (result)
    {
      if (solver->plugin->is_solution != NULL)
	{
	  TIMED_STMT (solver, T_IS_SOLUTION, 
		      result = 
		      solver->plugin->is_solution (solver->plugin->impl, 
						   X->impl, x, x_size,
						   xden));
	}
      else
	{
	  result = genepi_set_generic_is_solution (solver, X, x, x_size, xden);
	}
    }

  return result;
}

			/* --------------- */

int 
genepi_set_is_solution (genepi_solver *solver, genepi_set *X, const int *x, 
			int x_size, int xden)
{
  int result = s_is_solution (solver, X, x, x_size, xden);

  if (solver->trace_file != NULL)
    {
      int i;

      s_check_size (solver, X);      
      fprintf (solver->trace_file, "%d %d %d %d %d", GENEPI_TRACE_CHK_SOLUTION,
	       X->index, result?1:0, x_size, xden);
      for (i = 0; i < x_size; i++)
	fprintf (solver->trace_file, "%d ", x[i]);
      fprintf (solver->trace_file, "\n");
    }

  return result;
}

			/* --------------- */

int
genepi_set_get_one_solution (genepi_solver *solver, genepi_set *X, int *x, 
			     int x_size)
{
  int result;

  assert (solver != NULL); 
  assert (x != NULL);
  assert (x_size == genepi_set_get_width (solver, X));
  check_method (solver, get_one_solution);

  TIMED_STMT (solver, T_GET_ONE_SOLUTION, 
	      result = solver->plugin->get_one_solution (solver->plugin->impl, 
							 X->impl, x, x_size));
  assert (x_size == genepi_set_get_width (solver, X));
  /* TO BE MODIFIED FOR DIJO
    if (solver->trace_file != NULL)
    {
      s_check_size (solver, X);
      fprintf (solver->trace_file, "%d %d %d\n", GENEPI_TRACE_CHK_FULL, 
      X->index,
	       result?1:0);
    }
  */

  return result;
}

			/* --------------- */

int
genepi_set_write (genepi_solver *solver, FILE *output, genepi_set *set)
{
  int error = 0;
  int result;
  const char *solvername;

  assert (solver != NULL);
  check_method (solver, write_set);
  assert (output != NULL);

  solvername = genepi_solver_get_name (solver);
  assert (solvername != NULL);
  
  result  = genepi_io_write_string (output, solvername, &error);
  result += genepi_io_write_uint8 (output, set?0:1, &error);
  if (error)
    {
      result = error;
    }
  else if (set != NULL)
    {
      int c = solver->plugin->write_set (solver->plugin->impl, output, 
					 set->impl);

      if (c >= 0)
	result += c;
      else 
	result = c;
    }

  return result;
}

			/* --------------- */

int
genepi_set_read (genepi_solver *solver, FILE *input, genepi_set **pset)
{
  int error = 0;
  int result;
  uint8_t is_null = 0;
  const char *solvername;
  char *insolvername = NULL;

  assert (solver != NULL);
  check_method (solver, read_set);
  assert (input != NULL);

  solvername = genepi_solver_get_name (solver);
  assert (solvername != NULL);

  result = genepi_io_read_string (input, &insolvername, &error);
  result += genepi_io_read_uint8 (input, &is_null, &error);

  if (error)
    {
      result = error;
    }
  else if (strcmp (insolvername, solvername) == 0)
    {
      if (is_null)
	*pset = NULL;
      else
	{
	  genepi_set_impl *impl = NULL;
	  int c = solver->plugin->read_set (solver->plugin->impl, input, &impl);

	  if (c >= 0)
	    {
	      *pset = s_allocate_set (solver);
	      (*pset)->impl = impl;

	      result += c;
	    }
	  else 
	    result = c;
	}
    }
  else
    {
      result = GENEPI_R_ERR_BAD_SOLVER;
    }

  if (insolvername != NULL)
    free (insolvername);

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_transform (genepi_solver *solver, genepi_set *X,
		      int *indices, int nb_indices, int *cst, int nb_cst)
{
  int i;
  genepi_set *tmp;
  genepi_set *result;
  int Xwidth = genepi_set_get_width (solver, X);
  int width = Xwidth + nb_indices;
  int *sel = calloc (sizeof (int), width);

  assert (solver != NULL); 
  assert (X != NULL);
  assert (nb_indices >= 0 && (nb_indices == 0 ||  indices != NULL));

  for (i = 0; i < Xwidth; i++)
    sel[i] = 0;
  for (i = Xwidth; i < width; i++)
    sel[i] = 1;
  tmp = genepi_set_invproject (solver, X, sel, width);

  for (i = 0; i < width; i++)
    sel[i] = 0;

  for (i = 0; i < nb_indices; i++)
    {
      genepi_set *tmp1;
      genepi_set *tmp2;

      assert (indices[i] >= 0);
      
      if (indices[i] >= Xwidth)
	continue;

      if (indices[i] < 0)
	{
	  assert (-indices[i]-1 < nb_cst);

	  sel[Xwidth + i] = 1;	  
	  tmp1 = genepi_set_linear_equality (solver, sel, width, 
					     cst[-indices[i]-1]);
	  sel[Xwidth + i] = 0;
	}
      else
	{
	  sel[indices[i]] = -1;
	  sel[Xwidth + i] = 1;	  
	  tmp1 = genepi_set_linear_equality (solver, sel, width, 0);
	  sel[indices[i]] = 0;
	  sel[Xwidth + i] = 0;
	}
      tmp2 = genepi_set_intersection (solver, tmp1, tmp);
      genepi_set_del_reference (solver, tmp1);
      genepi_set_del_reference (solver, tmp);
      tmp = tmp2;
    }
  for (i = 0; i < Xwidth; i++)
    sel[i] = 1;
  for (i = Xwidth; i < width; i++)
    sel[i] = 0;

  result = genepi_set_project (solver, tmp, sel, width);
  free (sel);
  genepi_set_del_reference (solver, tmp);

  return result;
}

			/* --------------- */

int
genepi_set_preorder (genepi_solver *solver, genepi_set *X1, genepi_set *X2)
{
  assert (solver);
  assert (X1 != NULL && X2 != NULL);
  assert (solver->plugin->preorder != NULL);

  return solver->plugin->preorder (solver->plugin->impl, X1->impl, X2->impl);
}

			/* --------------- */

static genepi_set *
s_allocate_set (genepi_solver *solver)
{
  genepi_set *result;

  assert (solver != NULL);

  if (solver->trace_file == NULL || solver->free_sets == NULL)
    result = (genepi_set *) malloc (sizeof (genepi_set));
  else
    {
      result = solver->free_sets;
      solver->free_sets = result->next;
      assert (result->index >= 0);
    }

  result->refcount = 1;
  result->index = solver->last_set_index++;
  result->impl = NULL;

  return result;
}

			/* --------------- */

static void
s_delete_set (genepi_solver *solver, genepi_set *X)
{
  if (solver->trace_file != NULL)
    {
      X->next = solver->free_sets;
      solver->free_sets = X;
    }
  else
    {
      free (X);
    }
}

			/* --------------- */

static void
s_check_size (genepi_solver *solver, genepi_set *X)
{
  fprintf (solver->trace_file,"%d %d %d\n", GENEPI_TRACE_CHK_DS_SIZE, X->index,
	   solver->plugin->get_data_structure_size (solver->plugin->impl, 
						    X->impl));
}

			/* --------------- */

