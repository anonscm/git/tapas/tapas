/*
 * genepi-loader.c -- Implementation of the plugin loader for GENEPI.
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ltdl.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "genepi-common.h"
#include "genepi-p.h"
#include "genepi-loader.h"

#ifndef GENEPI_PLUGIN_DIRECTORY
# define GENEPI_PLUGIN_DIRECTORY "."
#endif

			/* --------------- */

#define PLUGIN_OFFSET(function) \
  ((uintptr_t) (&((genepi_plugin *) NULL)->function))

#define PLUGIN_SYMBOL(name) \
  { # name, PLUGIN_OFFSET (name) }

			/* --------------- */

typedef struct plugin_function_st {
  const char *name;
  int offset;
} plugin_function;

			/* --------------- */

static const char *
s_fill_function_table (genepi_plugin *plugin, const char *filename);

			/* --------------- */

static int init_counter = 0;
static int default_plugins_are_loaded;
static genepi_plugin *plugins;
static int nb_plugins;

static plugin_function functions[] = {
  PLUGIN_SYMBOL (bot),
  PLUGIN_SYMBOL (compare),
  PLUGIN_SYMBOL (get_data_structure_size),
  PLUGIN_SYMBOL (get_width),
  PLUGIN_SYMBOL (init),
  PLUGIN_SYMBOL (invproject),
  PLUGIN_SYMBOL (is_empty),
  PLUGIN_SYMBOL (linear_operation),
  PLUGIN_SYMBOL (project),
  PLUGIN_SYMBOL (set_complement),
  PLUGIN_SYMBOL (set_intersection),
  PLUGIN_SYMBOL (set_union),
  PLUGIN_SYMBOL (top),
  PLUGIN_SYMBOL (add_reference),
  PLUGIN_SYMBOL (apply),
  PLUGIN_SYMBOL (applyinv),
  PLUGIN_SYMBOL (autotest),
  PLUGIN_SYMBOL (depend_on),
  PLUGIN_SYMBOL (display_all_solutions),
  PLUGIN_SYMBOL (display_data_structure),
  PLUGIN_SYMBOL (get_solutions),
  PLUGIN_SYMBOL (get_solver_type),  
  PLUGIN_SYMBOL (is_finite),
  PLUGIN_SYMBOL (is_full),
  PLUGIN_SYMBOL (terminate),
  PLUGIN_SYMBOL (get_one_solution),
  PLUGIN_SYMBOL (is_solution),
  PLUGIN_SYMBOL (top_N),
  PLUGIN_SYMBOL (top_Z),
  PLUGIN_SYMBOL (top_P),
  PLUGIN_SYMBOL (top_R),
  PLUGIN_SYMBOL (cache_hash),
  PLUGIN_SYMBOL (cache_equal),
  PLUGIN_SYMBOL (del_reference),
  PLUGIN_SYMBOL (write_set),
  PLUGIN_SYMBOL (read_set),
  PLUGIN_SYMBOL (preorder)
};

#define function_table_size (sizeof (functions)/sizeof (functions[0]))

			/* --------------- */

void
genepi_loader_init (void)
{  
  if (init_counter == 0)
    {
      lt_dlinit ();

      default_plugins_are_loaded = 0;
      plugins = NULL;
      nb_plugins = 0;
      init_counter++;
    }
}

			/* --------------- */

static void
s_clear_plugin (genepi_plugin *plugin)
{
  genepi_plugconf_del_reference (plugin->conf);
  if (plugin->terminate != NULL)
    plugin->terminate (plugin->impl);
  if (plugin->hdl != NULL)
    lt_dlclose (plugin->hdl);
  free (plugin);
}

			/* --------------- */

void
genepi_loader_terminate (void)
{
  init_counter--;
  if (init_counter==0)
    {
      while (plugins != NULL)
	{
	  genepi_plugin *p;
	  for (p = plugins; p; p = p->next)
	    if (p->refcount == 1)
	      {
		genepi_plugin_del_reference (p);
		break;
	      }
	}
      lt_dlexit ();
      default_plugins_are_loaded = 0;
    }
}

			/* --------------- */

const char *
genepi_loader_get_default_directory (void)
{
  return GENEPI_PLUGIN_DIRECTORY;
}

			/* --------------- */

void
genepi_loader_load_default_plugins (void)
{
  if (!default_plugins_are_loaded)
    {
      const char *default_directory = genepi_loader_get_default_directory ();
      genepi_loader_load_directory (default_directory);
      default_plugins_are_loaded = 1;
    }
}

			/* --------------- */

genepi_plugin *
genepi_plugin_add_reference (genepi_plugin *plugin)
{
  plugin->refcount++;
  return plugin;
}

			/* --------------- */

void
genepi_plugin_del_reference (genepi_plugin *plugin)
{
  plugin->refcount--;
  if (plugin->refcount == 0)
    {
      genepi_plugin **p;

      for (p = &plugins; *p && *p != plugin; p = &((*p)->next))
	/* do nothing */ ;
      assert (*p == plugin);      
      *p = plugin->next;
      s_clear_plugin (plugin);
    }
}

			/* --------------- */

static int
s_load_plugin_methods (const char *filename, genepi_plugin *target,
		       genepi_plugconf *conf)
{  
  int result = 0;
  lt_dlhandle hdl = lt_dlopen (filename);
  const char *err = lt_dlerror();

  if (err != NULL)
    fprintf (stderr, "%s: error: %s\n", filename, err);
  else
    {
      int plugin_is_ok;
      target->hdl = hdl;
      err = s_fill_function_table (target, filename);
      plugin_is_ok = (err == NULL);

      if (plugin_is_ok && target->init != NULL)
	{	 
	  target->impl = target->init (conf);
	  if (target->impl == NULL)
	    {
	      fprintf (stderr, "%s: error: initialization error.\n", filename);
	      plugin_is_ok = 0;
	    }
	}

      if (plugin_is_ok)
	{
#if GENEPI_MAKE_PLUGINS_RESIDENT
	  lt_dlmakeresident (hdl);
#endif
	  result = 1;
	}
      else
	lt_dlclose (hdl);

      if (!result)
	target->terminate = NULL;
    }

  return result;
}

			/* --------------- */

static genepi_plugin *
s_load_plugin (const char *filename)
{  
  genepi_plugin *result = NULL;
  FILE *input = fopen (filename, "r");
  
  if (input == NULL)
    fprintf (stderr, "can't open file '%s'.\n", filename);
  else
    {
      genepi_plugconf *conf = genepi_plugconf_create ();
      
      if (genepi_plugconf_load (conf, input, filename))
	{
	  const char *name = 
	    genepi_plugconf_get_entry (conf, CONF_PLUGIN_NAME, NULL);
	  const char *code = 
	    genepi_plugconf_get_entry (conf, CONF_PLUGIN_CODE, NULL);

	  if (name == NULL)
	    fprintf (stderr, "%s: error: no '%s' property in plugin "
		     "specification file.\n", filename, CONF_PLUGIN_NAME);
	  else if (code == NULL)
	    fprintf (stderr, "%s: error: no '%s' property in plugin "
		     "specification file.\n", filename, CONF_PLUGIN_CODE);
	  else
	    {
	      result = calloc (sizeof (genepi_plugin), 1);
	      result->next = plugins;
	      plugins = result;
	      nb_plugins++;
	      result->refcount = 1;
	      result->conf = genepi_plugconf_add_reference (conf);
	    }
	}

      genepi_plugconf_del_reference (conf);
      fclose (input);
    }


  return result;
}

			/* --------------- */

genepi_plugin *
genepi_loader_get_plugin (const char *filename)
{  
  genepi_plugin *result = NULL;
  genepi_plugin *p = plugins;

  for(; p != NULL && result == NULL; p = p->next)
    {
      if (strcmp (filename, genepi_plugin_get_name (p)) == 0)
	result = p;
    }

  if (result == NULL)
    result = s_load_plugin (filename);

  if (result && result->impl == NULL)
    {
      const char *name = 
	genepi_plugconf_get_entry (result->conf, CONF_PLUGIN_NAME, NULL);
      const char *code = 
	genepi_plugconf_get_entry (result->conf, CONF_PLUGIN_CODE, NULL);

      if (s_load_plugin_methods (code, result, result->conf))
	result = genepi_plugin_add_reference (result);
      else
	{
	  fprintf (stderr, "error while loading code of plugin %s: %s\n",
		   name, code);
	  genepi_plugin_del_reference (result);
	  result = NULL;
	}

    }

  return result;
}

			/* --------------- */

static int 
s_is_valid_module_name (const char *name)
{
  const int minsz = strlen (CONF_FILE_EXTENSION);
  int len = strlen (name);

  if (len < minsz)
    return 0;

  if ((name = strrchr (name, '.')) == NULL)
    return 0;

  if (strcmp (name, CONF_FILE_EXTENSION) == 0)
    return 1;

  return 0;
}

			/* --------------- */

int
genepi_loader_load_directory (const char *dirname)
{
  struct dirent *e;
  char *plugin_name;
  int plugin_name_size, dnlen;
  DIR *dir = opendir (dirname);

  if (dir == NULL)
    return 0;
    
  dnlen = strlen (dirname);
  plugin_name_size = dnlen+1;
  plugin_name = (char *) calloc (sizeof (char), plugin_name_size);
  
  while ((e=readdir (dir)) != NULL)
    {
      int sz = dnlen+1;

      if (!s_is_valid_module_name (e->d_name))
	continue;
      
      sz += 1+strlen (e->d_name);
      if (sz > plugin_name_size)
	{
	  plugin_name = (char *) realloc (plugin_name, sizeof (char)*sz);
	  plugin_name_size = sz;
	}
      sprintf (plugin_name, "%s/%s", dirname,e->d_name);
      s_load_plugin (plugin_name);
    }
  free (plugin_name);

  closedir (dir);

  return 1;
}

			/* --------------- */

char **
genepi_loader_get_plugins (int *psize)
{
  int i;
  genepi_plugin *p;
  char **result = calloc (sizeof (char *), nb_plugins);

  *psize = nb_plugins;
  for (i = 0, p = plugins; p; p = p->next, i++)
    {
      const char *name = genepi_plugin_get_name (p);
      result[i] = strdup (name);
    }

  return result;
}

			/* --------------- */

const char *
genepi_plugin_get_name(const genepi_plugin *plugin)
{
  return genepi_plugconf_get_entry (plugin->conf, CONF_PLUGIN_NAME, NULL);
}

			/* --------------- */

const char *
genepi_plugin_get_filename(const genepi_plugin *plugin)
{
  const lt_dlinfo *info = lt_dlgetinfo(plugin->hdl);

  return info->filename;
}

			/* --------------- */

static unsigned int
s_default_cache_hash (genepi_plugin_impl *dummy, genepi_set_impl *X)
{
  return (uintptr_t) X;
}

static int 
s_default_cache_equal (genepi_plugin_impl *dummy, genepi_set_impl *X1, 
		       genepi_set_impl *X2)
{
  return X1 == X2;
}

			/* --------------- */

static const char *
s_fill_function_table (genepi_plugin *plugin, const char *filename)
{
  size_t i;
  const char *err = NULL;

  plugin->cache_equal = s_default_cache_equal;
  plugin->cache_hash = s_default_cache_hash;

  for(i = 0; i < function_table_size && err == NULL; i++)
    {
      void **funcaddr = (void **)(((char *)plugin)+functions[i].offset);
      void *symptr = lt_dlsym (plugin->hdl, functions[i].name);

      *funcaddr = symptr;
      lt_dlerror(); /* clean up error flag */
    }

  return err;
}


