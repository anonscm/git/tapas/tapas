/*
 * genepi-plugin-config.c -- Configuration table for plugins
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <assert.h>
#include "genepi-plugin-config.h"

#define READ_ERROR -1
#define READ_EOF 0
#define READ_NO_ERROR 1

typedef struct config_entry_st config_entry;
struct config_entry_st
{
  char *key;
  char *value;  
  config_entry *childs[2];
};

struct genepi_plugconf_st
{
  int refcount;
  int number_of_entries;
  config_entry *entries;
};

			/* --------------- */

static config_entry **
s_get_entry (genepi_plugconf *conf, const char *key);

static void
s_del_entry_rec (config_entry *e);

static void
s_display_entry_rec (config_entry *e, FILE *ouput);

static int
s_parse_conf_file (genepi_plugconf *conf, FILE *input, const char *location);

			/* --------------- */

genepi_plugconf *
genepi_plugconf_create (void)
{
  genepi_plugconf *result = (genepi_plugconf *) 
    malloc (sizeof (genepi_plugconf));

  result->refcount = 1;
  result->number_of_entries = 0;
  result->entries = NULL;

  return result;
}

			/* --------------- */

int
genepi_plugconf_load (genepi_plugconf *conf, FILE *input, const char *location)
{
  genepi_plugconf *tmp = genepi_plugconf_create ();
  int result = s_parse_conf_file (tmp, input, location);

  assert (conf != NULL);

  if (result)
    genepi_plugconf_merge (conf, tmp);
  genepi_plugconf_del_reference (tmp);
  
  return result;
}

			/* --------------- */

void
genepi_plugconf_save (genepi_plugconf *conf, FILE *output)
{
  if (conf->number_of_entries == 0)
    fprintf (output, "# empty configuration\n");
  else
    s_display_entry_rec (conf->entries, output);
}

			/* --------------- */

genepi_plugconf *
genepi_plugconf_add_reference (genepi_plugconf *conf)
{
  conf->refcount++;

  return conf;
}

			/* --------------- */

void
genepi_plugconf_del_reference (genepi_plugconf *conf)
{
  conf->refcount--;
  if (conf->refcount != 0)
    return;

  s_del_entry_rec (conf->entries);
  free (conf);
}

			/* --------------- */

static config_entry *
s_duplicate_entries_rec (const config_entry *tree)
{
  config_entry *result;

  if (tree == NULL)
    return NULL;
    
  result = (config_entry *) malloc (sizeof (config_entry));

  result->key = (char *) calloc (strlen (tree->key) + 1, sizeof (char));
  strcpy (result->key, tree->key);
  result->value = (char *) calloc (strlen (tree->value) + 1, sizeof (char));
  strcpy (result->value, tree->value);
  result->childs[0] = s_duplicate_entries_rec (tree->childs[0]);
  result->childs[1] = s_duplicate_entries_rec (tree->childs[1]);

  return result;
}

			/* --------------- */

genepi_plugconf * 
genepi_plugconf_duplicate (const genepi_plugconf *conf)
{
  genepi_plugconf *result = (genepi_plugconf *) 
    malloc (sizeof (genepi_plugconf));
  
  result->refcount = 1;
  result->number_of_entries = conf->number_of_entries;
  result->entries = s_duplicate_entries_rec (conf->entries);

  return result;
}

			/* --------------- */

static void
s_add_entries_rec (genepi_plugconf *conf, const config_entry *tree)
{
  if (tree == NULL)
    return;

  genepi_plugconf_add_entry (conf, tree->key, tree->value);
  s_add_entries_rec (conf, tree->childs[0]);
  s_add_entries_rec (conf, tree->childs[1]);
}

			/* --------------- */

void
genepi_plugconf_merge (genepi_plugconf *conf, const genepi_plugconf *other)
{
  s_add_entries_rec (conf, other->entries);
}

			/* --------------- */

void
genepi_plugconf_add_entry (genepi_plugconf *conf, const char *key, 
			   const char *value)
{
  int len;
  config_entry **pe = s_get_entry (conf, key);

  if (*pe == NULL)
    {
      *pe = malloc (sizeof (config_entry));
      len = strlen (key) + 1;
      (*pe)->key = calloc (len, sizeof (char));
      strcpy ((*pe)->key, key);
      (*pe)->childs[0] = NULL;
      (*pe)->childs[1] = NULL;
      conf->number_of_entries++;
    }
  else
    {
      free ((*pe)->value);
    }

  len = strlen (value) + 1;
  (*pe)->value = calloc (len, sizeof (char));
  strcpy ((*pe)->value, value);
}


			/* --------------- */

const char *
genepi_plugconf_get_entry (genepi_plugconf *conf, const char *key, 
			   const char *default_value)
{
  config_entry **pe;

  assert (conf != NULL);
  assert (key != NULL);

  pe = s_get_entry (conf, key);

  if (*pe != NULL)
    return (*pe)->value;

  return default_value;
}

			/* --------------- */

int
genepi_plugconf_get_bool (genepi_plugconf *conf, const char *key, 
			  int default_value)
{
  int result;
  const char *val = genepi_plugconf_get_entry (conf, key, NULL);

  if (val == NULL)
    result = default_value;
  else 
    result = (strcmp (val, "1") == 0 || strcmp (val, "true") == 0 ||
	      strcmp (val, "TRUE") == 0);

  return result;
}

			/* --------------- */

int
genepi_plugconf_get_int (genepi_plugconf *conf, const char *key,
			     int default_value)
{
  int result;
  const char *val = genepi_plugconf_get_entry (conf, key, NULL);

  if (val == NULL)
    result = default_value;
  else
    result = atoi (val);

  return result;
}

			/* --------------- */

unsigned int
genepi_plugconf_get_unsigned_int (genepi_plugconf *conf, const char *key,
				      unsigned int default_value)
{
  int res = genepi_plugconf_get_int (conf, key, -1);

  if (res < 0)
    res = default_value;

  return res;
}

			/* --------------- */

static config_entry **
s_get_entry (genepi_plugconf *conf, const char *key)
{  
  int stop = 0;
  config_entry **pe = &conf->entries;

  while (*pe != NULL && ! stop)
    {
      int c = strcmp ((*pe)->key, key);

      if (c < 0)
	pe = &((*pe)->childs[0]);
      else if (c > 0)
	pe = &((*pe)->childs[1]);
      else
	stop = 1;
    }
  
  return pe;
}

			/* --------------- */

static void
s_del_entry_rec (config_entry *e)
{
  if (e == NULL)
    return;

  free (e->key);
  free (e->value);
  s_del_entry_rec (e->childs[0]);
  s_del_entry_rec (e->childs[1]);
  free (e);
}

			/* --------------- */

static void
s_display_entry_rec (config_entry *e, FILE *output)
{
  if (e == NULL)
    return;

  fprintf (output, "%s = %s\n", e->key, e->value);
  s_display_entry_rec (e->childs[0], output);
  s_display_entry_rec (e->childs[1], output);
}

			/* --------------- */

static void
s_append_char (char **s, int *ssize, int *slen, char c)
{

  while (*slen + 2 >= *ssize)
    {
      *ssize <<= 1;
      *s = realloc (*s, *ssize * sizeof (char));
    }

  (*s)[*slen] = c;
  (*s)[*slen+1] = '\0';
  
  (*slen)++;  
}

			/* --------------- */

# define ISSPACE(c) (((c) == ' ') || ((c) == '\t'))

			/* --------------- */

static int
s_read_line (FILE *input, char **p_name, char **p_value, const char *location,
	     int *p_line)
{
  int c;
  int name_size = 0;
  int name_len = 0;
  int value_size = 0;
  int value_len = 0;
  enum { BOL, NAME, EQ, VALUE, COMMENT } st = BOL;
  
  *p_name = NULL;
  *p_value = NULL;
  for (;;)
    {
      c = fgetc (input);
      switch (st)
	{
	case BOL:
	  (*p_line)++;
	  if (c < 0)
	    return READ_EOF;
	  else if (c == '#')
	    st = COMMENT;
	  else if (c != '\n')
	    {
	      name_size = 2;
	      name_len = 1;
	      *p_name = calloc (2, sizeof (char));
	      **p_name = (char) c;
	      st = NAME;
	    }
	  break;
	  
	case NAME:
	  if (c < 0 || c == '\n' || c == '#')
	    {
	      fprintf (stderr, "%s:%d: ", location, *p_line);
	      if (c == '#') 
		fprintf (stderr, "malformed line.\n");
	      else if (c == '\n') 
		fprintf (stderr, "unexpected end of line.\n");
	      else 
		fprintf (stderr, "unexpected end of file.\n");

	      free (*p_name);
	      *p_name = NULL;
	      return READ_ERROR;
	    }
	  else if (!ISSPACE (c))
	    s_append_char (p_name, &name_size, &name_len, c);
	  else
	    st = EQ;
	  break;

	case EQ:
	  if (c == '=')
	    {
	      value_size = 1;
	      value_len = 0;
	      *p_value = calloc (1, sizeof (char));
	      st = VALUE;	    
	    }
	  else if (c < 0 || c == '\n' || c == '#' || ! ISSPACE (c))
	    {
	      fprintf (stderr, "%s:%d: ", location, *p_line);
	      if (c == '#' || ! ISSPACE (c))
		fprintf (stderr, "malformed line.\n");
	      else if (c == '\n') 
		fprintf (stderr, "unexpected end of line.\n");
	      else 
		fprintf (stderr, "unexpected end of file.\n");

	      free (*p_name);
	      *p_name = NULL;

	      return READ_ERROR;
	    }
	  break;

	case VALUE:
	  if (c < 0)
	    return READ_EOF;
	  else if (c == '\n')
	    return READ_NO_ERROR;
	  else if (c == '#')
	    st = COMMENT;
	  else if (c == '\\')
	    {
	      c = fgetc (input);
	      if (c < 0)
		{
		  s_append_char (p_value, &value_size, &value_len, '\\');
		  return READ_EOF;
		}
	      else if (c == '\n')
		(*p_line)++;
	      else 
		s_append_char (p_value, &value_size, &value_len, c);
	    }
	  else 
	    {
	      s_append_char (p_value, &value_size, &value_len, c);
	    }
	  break;

	case COMMENT:
	  if (c < 0)
	    return READ_EOF;
	  else if (c == '\n')
	    {
	      if (*p_name == NULL)
		st = BOL;
	      else
		return READ_NO_ERROR;
	    }
	  break;
	}
    }
}

			/* --------------- */

static void
s_remove_trailing_white_spaces (char *str)
{
  int lastpos = -1;
  int pos = 0;
  char *s;
  
  for (s = str; *s; s++, pos++)
    {
      if (*s == ' ')
	{
	  if (lastpos < 0) 
	    lastpos = pos;
	}
      else if (lastpos >= 0)
	lastpos = -1;
    }

  if (lastpos >= 0)
    str[lastpos] = '\0';
}

			/* --------------- */

static int
s_parse_conf_file (genepi_plugconf *conf, FILE *input, const char *location)
{
  int line = 0;
  int status = READ_NO_ERROR;

  while (status == READ_NO_ERROR)
    {
      char *name;
      char *value;

      status = s_read_line (input, &name, &value, location, &line);
      if (name != NULL)
	{
	  char *v = value;
	  s_remove_trailing_white_spaces (v);
	  while (*v == ' ')
	    v++;
	  genepi_plugconf_add_entry (conf, name, v);
	  free (name);
	  free (value);     
	}
    }

  return status != READ_ERROR;
}



