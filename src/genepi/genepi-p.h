/*
 * genepi-p.h -- Private definitions related to GENEPI solvers
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __GENEPI_P_H__
# define __GENEPI_P_H__

# include <ltdl.h>
# include <time.h>
# include "genepi.h"
# include "genepi-plugin.h"

			/* --------------- */

#define CONF_FILE_EXTENSION ".conf"
#define CONF_PLUGIN_NAME "name"
#define CONF_PLUGIN_CODE "code"

#define GENEPI_MAKE_PLUGINS_RESIDENT 1

			/* --------------- */

typedef struct genepi_cache_st genepi_cache;
typedef struct genepi_cache_info_st genepi_cache_info;
typedef void *genepi_func_id;

# define DECLARE_FUNC_ID(name) \
  static genepi_func_id name = (genepi_func_id)&name

# define check_method(solver,m)					\
  do {								\
    if ((solver)->plugin->m == NULL)				\
      {								\
	fprintf (stderr,					\
		 "invoking not implemented GENEPI function '"	\
		 # m						\
		 "'\n");					\
	abort ();						\
      }								\
  } while(0)

typedef struct genepi_timer_st genepi_timer;
typedef struct genepi_solver_methods_st genepi_solver_methods;
typedef enum genepi_timed_method_enum {
  T_LINEAR_OPERATION = 0,
  T_UNION,
  T_INTERSECTION,
  T_COMPLEMENT,
  T_PROJECT,
  T_INVPROJECT,
  T_APPLY,
  T_APPLYINV,
  T_IS_EMPTY,
  T_IS_FULL,
  T_COMPARE,
  T_TOP,
  T_BOT,
  T_DS_SIZE,
  T_IS_SOLUTION,
  T_GET_ONE_SOLUTION,
  T_DISPLAY_ALL_SOLUTIONS,
  T_TOTAL_TIME,
  NB_TIMERS
} genepi_timed_method;

			/* --------------- */

struct genepi_timer_st {
  const char *name;
  clock_t chronometer;
  unsigned int number_of_calls;
  unsigned int cumulated_time;
};


			/* --------------- */

struct genepi_solver_st {
  int flags;
  genepi_cache *cache;

  FILE *trace_file;
  int last_set_index;
  genepi_set *free_sets;

  genepi_timer timers[NB_TIMERS];
  genepi_plugin *plugin;
};

			/* --------------- */

struct genepi_cache_info_st {
  int requests;
  int collisions;
  int success;
  int filled_entries;
  int number_of_entries;
  int number_of_bytes;
};

struct genepi_set_st {
  genepi_set *next;
  int refcount;
  int index;
  genepi_set_impl *impl;
};

			/* --------------- */

struct genepi_plugin_st {
  genepi_plugin *next;
  int refcount;
  genepi_plugconf *conf;
  genepi_plugin_impl *impl;
  lt_dlhandle hdl;

  genepi_plugin_impl *(*init) (genepi_plugconf *params);
  void (*terminate) (genepi_plugin_impl *instance);

  int (*autotest) (genepi_plugin_impl *plugin);
  genepi_solver_type (*get_solver_type) (genepi_plugin_impl *plugin);

  genepi_set_impl *(*add_reference) (genepi_plugin_impl *plugin,
				     genepi_set_impl *X);
  void (*del_reference) (genepi_plugin_impl *plugin,
			 genepi_set_impl *X);
  genepi_set_impl *(*top) (genepi_plugin_impl *plugin, int n);
  genepi_set_impl *(*top_N) (genepi_plugin_impl *plugin, int n);
  genepi_set_impl *(*top_Z) (genepi_plugin_impl *plugin, int n);
  genepi_set_impl *(*top_P) (genepi_plugin_impl *plugin, int n);
  genepi_set_impl *(*top_R) (genepi_plugin_impl *plugin, int n);
  genepi_set_impl *(*bot) (genepi_plugin_impl *plugin, int n);
  genepi_set_impl *(*linear_operation) (genepi_plugin_impl *plugin, 
					const int *alpha, int alpha_size, 
					genepi_comparator op, int c);
  genepi_set_impl *(*set_union) (genepi_plugin_impl *plugin, 
				 genepi_set_impl *X1, genepi_set_impl *X2);
  genepi_set_impl *(*set_intersection) (genepi_plugin_impl *plugin, 
					genepi_set_impl *X1, 
					genepi_set_impl *X2);
  genepi_set_impl *(*set_complement) (genepi_plugin_impl *plugin, 
				      genepi_set_impl *X);
  genepi_set_impl *(*project) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
			       const int *selection, int size);
  genepi_set_impl *(*invproject) (genepi_plugin_impl *plugin, 
				  genepi_set_impl *X, const int *selection, 
				  int size);
  genepi_set_impl *(*apply) (genepi_plugin_impl *plugin, genepi_set_impl *R, 
			     genepi_set_impl *A);
  genepi_set_impl *(*applyinv) (genepi_plugin_impl *plugin, genepi_set_impl *R,
				genepi_set_impl *A);
  int (*compare) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
		  genepi_comparator op, genepi_set_impl *X2);
  int (*is_empty) (genepi_plugin_impl *plugin, genepi_set_impl *X);
  int (*is_full) (genepi_plugin_impl *plugin, genepi_set_impl *X);
  int (*is_finite) (genepi_plugin_impl *plugin, genepi_set_impl *X);
  int (*depend_on) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
		    const int *sel, int selsize);
  void (*get_solutions) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
			 int ***psolutions, int *psize, int max);
  void (*display_all_solutions) (genepi_plugin_impl *plugin, genepi_set_impl *X,
				 FILE *output, const char * const*varnames);
  void (*display_data_structure) (genepi_plugin_impl *plugin, 
				  genepi_set_impl *X, FILE *output);
  int (*get_width) (genepi_plugin_impl *plugin, genepi_set_impl *X);
  int (*get_data_structure_size) (genepi_plugin_impl *plugin, 
				  genepi_set_impl *X);
  int (*is_solution) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
		      const int *x, int x_size, int xden);
  int (*get_one_solution) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
			   int *x, int x_size);
  unsigned int (*cache_hash) (genepi_plugin_impl *plugin, genepi_set_impl *X);
  int (*cache_equal) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
		      genepi_set_impl *X2);

  int (*write_set) (genepi_plugin_impl *plugin, FILE *output, 
		    genepi_set_impl *X);
  int (*read_set) (genepi_plugin_impl *plugin, FILE *output, 
		   genepi_set_impl **X);
  int (*preorder) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
		   genepi_set_impl *X2);
};

			/* --------------- */

# define START_TIMER(_s, _t)			   \
  do						   \
    {						   \
      (_s)->timers[(_t)].number_of_calls++;	   \
      (_s)->timers[(_t)].chronometer = clock (); \
    }						   \
  while (0)

# define END_TIMER(_s, _t)			    \
  do						    \
    {						    \
      (_s)->timers[(_t)].cumulated_time += clock () \
	- (_s)->timers[(_t)].chronometer;	    \
    }						    \
  while (0)

			/* --------------- */

extern genepi_set *
genepi_set_generic_apply (genepi_solver *solver, genepi_set *R, genepi_set *A);

extern genepi_set *
genepi_set_generic_applyinv (genepi_solver *solver, genepi_set *R, 
			     genepi_set *A);

extern int
genepi_set_generic_depend_on (genepi_solver *solver, genepi_set *X, 
			      const int *sel, int selsize);

extern int 
genepi_set_generic_is_full (genepi_solver *solver, genepi_set *X);

extern genepi_set *
genepi_set_add_positive_constraint(genepi_solver *solver, int width,
				   genepi_set *X);

extern genepi_set *
genepi_set_generic_top_N(genepi_solver *solver, int width);

extern genepi_set *
genepi_set_generic_top_Z(genepi_solver *solver, int width);

extern genepi_set *
genepi_set_generic_top_P(genepi_solver *solver, int width);

extern int
genepi_set_generic_is_solution (genepi_solver *solver, genepi_set *X, 
				const int *x, int x_size, int xden);

			/* --------------- */

extern genepi_cache *
genepi_cache_create (genepi_solver *solver, int cachesize, 
		     int number_of_entries);

extern void
genepi_cache_delete (genepi_cache *cache);

extern const genepi_cache_info *
genepi_cache_get_info (genepi_cache *cache);

extern genepi_set_impl *
genepi_cache_get_top_N (genepi_cache *cache, int width);

extern void
genepi_cache_put_top_N (genepi_cache *cache, int width, genepi_set_impl *R);

extern genepi_set_impl *
genepi_cache_get_top_Z (genepi_cache *cache, int width);

extern void
genepi_cache_put_top_Z (genepi_cache *cache, int width, genepi_set_impl *R);

extern genepi_set_impl *
genepi_cache_get_top_P (genepi_cache *cache, int width);

extern void
genepi_cache_put_top_P (genepi_cache *cache, int width, genepi_set_impl *R);

extern genepi_set_impl *
genepi_cache_get_unary (genepi_cache *cache, genepi_func_id id,
			genepi_set_impl *X);

extern void
genepi_cache_put_unary (genepi_cache *cache, genepi_func_id id,
			genepi_set_impl *X, genepi_set_impl *R);

extern genepi_set_impl *
genepi_cache_get_binary (genepi_cache *cache, genepi_func_id id,
			 genepi_set_impl *X1, genepi_set_impl *X2);

extern void
genepi_cache_put_binary (genepi_cache *cache, genepi_func_id id, 
			 genepi_set_impl *X1, genepi_set_impl *X2, 
			 genepi_set_impl *R);

extern genepi_set_impl *
genepi_cache_get_linear_operation (genepi_cache *cache, const int *alpha, 
				   const int alpha_size, genepi_comparator op, 
				   const int c);

extern void
genepi_cache_put_linear_operation (genepi_cache *cache, const int *alpha, 
				   const int alpha_size, genepi_comparator op,
				   const int c, genepi_set_impl *R);

extern genepi_set_impl *
genepi_cache_get_project (genepi_cache *cache, genepi_set_impl *X, 
			  const int *sel, int selsize, int inverse);

extern void
genepi_cache_put_project (genepi_cache *cache, genepi_set_impl *X, 
			  const int *sel, int selsize, int inverse, 
			  genepi_set_impl *R);

#endif /* ! __GENEPI_P_H__ */
