/*
 * genepi-plugin.h -- The specification of GENEPI plugins
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file genepi/genepi-plugin.h
 * \brief Specification of Plugin Prototypes
 * 
 * This header file declares prototypes of functions that a plugin have to
 * implement. Each prototype is declared using a macro GENEPI_PLUGIN_FUNC()
 * whose aim is to prefix each function name with the name of the plugin.
 * GENEPI Plugin implementors must use this macro to define or to call the
 * functions of their plugin.
 * \attention This documentation has been automatically generated using
 * Doxygen (http://www/doxygen.org/) which does not support function prototypes
 * built using a macro-function like GENEPI_PLUGIN_FUNC(). For sake of 
 * readability this documentation has been produced by defining this 
 * macro-function as <tt>genepi_plugin_ ## _f</tt> i.e. it adds the prefix
 * <tt>genepi_plugin_</tt> to the given symbol \c _f by . Thus, in this 
 * documentation, any function name of the form 
 * <tt>genepi_plugin_</tt><em>some_name</em> has to be understood (and use) as 
 * GENEPI_PLUGIN_FUNC(<em>some_name</em>).
 *
 * The plugins are loaded (cf. genepi-loader.h) using the LTDL library and the
 * name of the generated <tt>.la</tt> file is used by the plugin loader. The
 * name of the file is the (nick)name of the module that has to be used to
 * prefix plugin exported functions (see GENEPI_PLUGIN_FUNC()). The template
 * plugin is distributed with a <tt>configure.ac</tt> file and a 
 * <tt>Makefile.am</tt> which can be helpful to build the plugin; the 
 * subdirectory <tt>m4</tt> contains a useful M4 macro for checking the 
 * existence of the GENEPI library on the system and to locate the directory
 * where plugins are installed.
 */
#ifndef __GENEPI_PLUGIN_H__
# define __GENEPI_PLUGIN_H__

# include <genepi/genepi.h>
# include <genepi/genepi-plugin-config.h>

BEGIN_C_DECLS

# ifndef GENEPI_PLUGIN_PREFIX
#  ifdef __GENEPI_P_H__
#   define GENEPI_PLUGIN_PREFIX(_f) genepi_plugin_ ## _f
#  else /* ! __GENEPI_P_H__ */
#   error "GENEPI_PLUGIN_PREFIX is not defined."
#  endif /* ! __GENEPI_P_H__ */
# endif /* ! GENEPI_PLUGIN_PREFIX */

/*!
 * \brief Macro used to prefix function symbols.
 *
 * This macro-function is used to prefix each declaration of a plugin's 
 * function by the name of the plugin. Indeed the actual symbol name of an
 * implemented function follows the form 
 * <em>plugin name</em><tt>_LTX_</tt><em>function name</em>. For example, if 
 * the plugin will be used under the name
 * \c myplugin then the function implementing the initialization of the plugin
 * will be actually called \c myplugin_LTX_init. 
 *
 * GENEPI_PLUGIN_FUNC() macro uses another macro called 
 * GENEPI_PLUGIN_PREFIX(_f) which is defined by the developper of the plugin.
 * This macro has to prefix the symbol specified by its argument \a _f with the
 * name of the plugin. The following lines present the code defining this macro
 * in the \e template plugin distributed with GENEPI:
 * \dontinclude template-plugin.c
 * \skipline GENEPI_PLUGIN_PREFIX
 * \until include <genepi/genepi-plugin.h>
 * 
 * In order to use the expect symbol name, the developper of a GENEPI plugin
 * has to use the GENEPI_PLUGIN_FUNC() to write the prototype of its function.
 * For example, the \ref genepi_plugin_get_solver_type 
 * "get_solver_type" function of the \e template plugin is defined this way:
 * \skipline genepi_solver_type
 * \until }
 */
# define GENEPI_PLUGIN_FUNC(_f) GENEPI_PLUGIN_PREFIX (_LTX_ ## _f)

/*!
 * \brief Abstraction of a GENEPI plugin instance
 */
typedef struct genepi_plugin_impl_st genepi_plugin_impl;

/*!
 * \brief Abstraction of a GENEPI set
 *
 * This is an abstraction of the implementation of the encoding of a
 * Presburger definable set. GENEPI uses it via pointers and no constraint 
 * exists on the underlying data structure contained into a #genepi_set_impl
 * (except that it must permits the implementation of function manipulating
 * specified by the API).
 */
typedef struct genepi_set_impl_st genepi_set_impl;

/*! 
 * \brief Initializes the plugin.
 * 
 * This function is called just after the plugin is loaded. It has to 
 * initialize ressources of the plugin. If the initialization fail the loader
 * unload the plugin.
 *
 * This function is ignored if not implemented.
 *
 * \warning A plugin might be loaded several times this this function has to
 * take care of that in order to not remove or reinitialize data.
 * 
 * \return a non null value if the plugin is successfully initialized.
 *
 * \see \ref genepi_plugin_terminate "terminate"
 */
extern genepi_plugin_impl *
GENEPI_PLUGIN_FUNC (init) (genepi_plugconf *conf);

/*! 
 * \brief Releases ressources allocated by the plugin.
 *  
 * This function destroys the ressources allocated by the module since its
 * initilization.
 *
 * This function has to handle the case multi-initialization of the plugin. 
 *
 * This function is ignored if not implemented.
 *
 * \see \ref genepi_plugin_init "init"
 */
extern void
GENEPI_PLUGIN_FUNC (terminate) (genepi_plugin_impl *plugin);


/*!
 * \brief Self-test
 *
 * This function is called on-demand by GENEPI clients to execute tests 
 * validating the plugin. 
 *
 * \return a non null value if the test is passed
 */
extern int
GENEPI_PLUGIN_FUNC (autotest) (genepi_plugin_impl *plugin);

			/* --------------- */

/*!
 * \brief Returns the domain of variables used by the solver.
 *
 * If this function is not implemented GENEPI assumes that the solver works 
 * on positive or null integers.
 *
 * \return the domain of variables used by the solver.
 *
 * \see genepi_solver_type, genepi_solver_get_solver_type()
 */
extern genepi_solver_type 
GENEPI_PLUGIN_FUNC (get_solver_type) (genepi_plugin_impl *plugin);

/*!
 * \brief Adds a new reference to the set \a X
 * 
 * This function is called to indicate the plugin that the set \a X is 
 * referenced another time. 
 * 
 * \param X a non null pointer to a set
 *
 * \pre X != NULL
 *
 * \return \a X in any case
 */
extern genepi_set_impl *
GENEPI_PLUGIN_FUNC (add_reference) (genepi_plugin_impl *plugin, 
                                    genepi_set_impl *X);

/*!
 * \brief Removes a reference to the set \a X
 *
 * This indicates the plugin that a reference has to be removed to \a X.
 * GENEPI assumes that \a X is valid while its number of references is 
 * positive. When the number of references falls to zero the plugin should
 * release or collect ressources allocated to \a X.
 *
 * \param X a non null pointer to a set
 *
 * \pre X != NULL
 */
extern void 
GENEPI_PLUGIN_FUNC (del_reference) (genepi_plugin_impl *plugin, 
                                    genepi_set_impl *X);

/*!
 * \brief Computes the complete relation of arity \a n.
 *
 * This function has to return the complete relation of arity \a n. Depending
 * on the type of solver variables the result is either \f${\cal N}^n\f$, 
 * \f${\cal Z}^n\f$, \f${\cal R+}^n\f$ or \f${\cal R}^n\f$.
 *
 * \param n the arity of the computed relation
 *
 * \pre n > 0
 *
 * \return the encoding of the complete relation of arity \a n.
 *
 * \ref genepi_plugin_get_solver_type "get_solver_type".
 */
extern genepi_set_impl *
GENEPI_PLUGIN_FUNC (top) (genepi_plugin_impl *plugin, int n);

/*!
 * \brief Computes the complete relation of arity \a n restricted to positive
 * or null integers.
 *
 * This function has to return the complete relation of arity \a n over 
 * positive or null integers \f${\cal N}\f$.
 *
 * \param n the arity of the computed relation
 * 
 * \pre n > 0
 *
 * \return \f${\cal N}^n \cap \mbox{\tt top(solver,n)}\f$
 */
extern genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_N) (genepi_plugin_impl *plugin, int n);

/*!
 * \brief Computes the complete relation of arity \a n restricted integers.
 *
 * \param n the arity of the computed relation
 * 
 * \pre n > 0
 *
 * \return \f${\cal Z}^n \cap \mbox{\tt top(solver,n)}\f$
 */
extern genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_Z) (genepi_plugin_impl *plugin, int n);

/*!
 * \brief Computes the complete relation of arity \a n restricted to positive
 * or null reals.
 *
 * \param n the arity of the computed relation
 * 
 * \pre n > 0
 *
 * \return \f${\cal R^+}^n \cap \mbox{\tt top(solver,n)}\f$
 */
extern genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_P) (genepi_plugin_impl *plugin, int n);

/*!
 * \brief Computes the complete relation of arity \a n restricted to reals.
 *
 * \param n the arity of the computed relation
 * 
 * \pre n > 0
 *
 * \return \f${\cal R}^n \cap \mbox{\tt top(solver,n)}\f$
 */
extern genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_R) (genepi_plugin_impl *plugin, int n);

/*!
 * \brief Computes the empty relation of arity \a n.
 *
 * \warning In order to be validated by the loader a plugin MUST implement this
 * function.
 *
 * \param n the arity of the computed relation
 *
 * \pre n > 0
 *
 * \return the encoding of \f$\emptyset^n\f$
 */
extern genepi_set_impl *
GENEPI_PLUGIN_FUNC (bot) (genepi_plugin_impl *plugin, int n);

/*!
 * \brief Computes the set of solutions of a linear operation.
 *
 * According to the value of \a op this function returns the set of solutions
 * of the linear operation: 
 * \f[ \Sigma_{i=0..alpha\_size} alpha[i]\times x_i\mbox{ op }c\f]
 * with \a op interpreted on integers.
 *
 * \warning In order to be validated by the loader a plugin MUST implement this
 * function.
 *
 * \param alpha the array of coefficient of the linear operation
 * \param alpha_size the number of coefficients
 * \param c the constant term of the operation
 * \param op the identifier of the operation
 *
 * \pre alpha != NULL && alpha_size > 0
 *
 * \return the solutions of the specified linear operation.
 */
extern genepi_set_impl *
GENEPI_PLUGIN_FUNC (linear_operation) (genepi_plugin_impl *plugin, 
                                       const int *alpha, int alpha_size, 
				       genepi_comparator op, int c);

/*!
 * \brief Computes the union of two relations.
 * 
 * This function has to compute \f$X_1 \cup X_2\f$ where relations \f$ X_i\f$
 * are assumed to be of same arity. 
 * 
 * \warning In order to be validated by the loader a plugin MUST implement this
 * function.
 *
 * \param X1 the first operand of the union
 * \param X2 the second operand of the union
 *
 * \pre \ref genepi_plugin_get_width 
 * "GENEPI_PLUGIN_FUNC (get_width) (X1)" = \ref 
 * genepi_plugin_get_width "GENEPI_PLUGIN_FUNC (get_width) (X2)"
 *
 * \return the encoding of \f$X_1 \cup X_2\f$
 */
extern genepi_set_impl *
GENEPI_PLUGIN_FUNC (set_union) (genepi_plugin_impl *plugin, 
                                genepi_set_impl *X1, genepi_set_impl *X2);

/*!
 * \brief Computes the intersection of two relations.
 *
 * This function has to compute \f$X_1 \cap X_2\f$ where relations \f$ X_i\f$
 * are assumed to be of same arity. 
 *
 * \warning In order to be validated by the loader a plugin MUST implement this
 * function.
 *
 * \param X1 the first operand of the intersection
 * \param X2 the second operand of the intersection
 *
 * \pre \ref genepi_plugin_get_width 
 * "GENEPI_PLUGIN_FUNC (get_width) (X1)" = \ref 
 * genepi_plugin_get_width "GENEPI_PLUGIN_FUNC (get_width) (X2)"
 *
 * \return the encoding of \f$X_1 \cap X_2\f$
 */
extern genepi_set_impl *
GENEPI_PLUGIN_FUNC (set_intersection) (genepi_plugin_impl *plugin, 
                                       genepi_set_impl *X1, 
				       genepi_set_impl *X2);

/*!
 * \brief Computes the complement of the set \a X
 *
 * This function computes the completement of \a X in the domain of variables
 * i.e. either \f$N\f$ or \f$Z\f$.
 *
 * \warning In order to be validated by the loader a plugin MUST implement this
 * function.
 *
 * \param X the set to complement.
 *
 * \pre X != NULL
 *
 * \return the encoding of \f$\overline{X}\f$
 */
extern genepi_set_impl *
GENEPI_PLUGIN_FUNC (set_complement) (genepi_plugin_impl *plugin, 
                                     genepi_set_impl *X);

/*!
 * \brief Projects a relation on a subset of its columns.
 *
 * This function computes the projection of \a X on the columns selected by
 * the array of Booleans \a selection. The function computes:
 * \f[
 *  project(X) = \{ (x_{i_1}, \dots, x_{i_n}) \mid \exists v\in X, v[i_1] = 
 * x_{i_1}\wedge \dots\wedge v[i_n] = x_{i_n} \}\f]
 * where \f$ 0 \leq i_1 \leq\dots\leq i_n \leq size\f$ and for \f$j=1\dots n, 
 * selection[i_j] = 0 \f$. The function may assumed that the arity of \a X
 * is equal or greater than \a size.
 *
 * \warning In order to be validated by the loader a plugin MUST implement this
 * function.
 * 
 * \param X the relation to project
 * \param selection the selection of columns of X which must be kept by the
 *        projection.
 * \param size the number of Booleans in \a selection.
 *
 * \pre X != NULL
 * \pre selection != NULL
 * \pre size == GENEPI_PLUGIN_FUNC(get_width) (X)
 *
 * \return the encoding of the projection of \a X according to the selected 
 * columns in \a selection.
 */
extern genepi_set_impl *
GENEPI_PLUGIN_FUNC (project) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
                              const int *selection, int size);

/*!
 * \brief Adds columns to the relation \a X.
 *
 * This function add columns to \a X according to the array of Booleans \a 
 * selection. The arity of the resulting relation is \a size and the relation
 * itself is defined by:
 * \f[
 *   invproject(X,selection,size) = \{ (x_1,\dots,x_{size}) | \exists v\in X, 
 *    v[1] = x_{i_1}\wedge \dots\wedge v[n] = x_{i_n} \}
 *   \}
 * \f]
 * where \a n is the arity of \a X, \f$ 0 \leq i_1 \leq\dots\leq i_n < 
 * size\f$ and for \f$j=1\dots n, selection[i_j] = 0 \f$. 
 *
 * \param X the relation where new columns are added
 * \param selection the specification of new columns
 * \param size number of column in the result.
 *
 * \pre X != NULL
 * \pre selection != NULL  
 * \pre size >= GENEPI_PLUGIN_FUNC(get_width) (X)
 *
 * \return the encoding of \a X extended with additionnal columns specified in
 * \a selection.
 */
extern genepi_set_impl *
GENEPI_PLUGIN_FUNC (invproject) (genepi_plugin_impl *plugin, genepi_set_impl *X,
				 const int *selection, int size);

/*!
 * \brief Computes <tt>post (R, A)</tt>
 *
 * This function applies the relation \a R to the set \a A. If \a n
 * is the width of \a A, the function assumes that 2*\a n is the width of \a R.
 * The function computes the set of vectors defined by:
 * \f[apply(R,A) = \{ (x_1,\dots, x_n)\in A \mid \exists x_1^\prime,\dots 
 * x_n^\prime, (x_1^\prime,x_1,\dots,x_n^\prime,x_n)\in R\}\f]
 * 
 * Note the interleaving of primed and not primed variables and also note that
 * the primed variable precedes its non-primed version.
 * 
 * \remarks If this function it is not implemented by the plugin, a generic 
 * function is used.
 *
 * \param R the transition relation
 * \param A the relation on which R is applied.
 *
 * \pre \ref genepi_plugin_get_width 
 * "GENEPI_PLUGIN_FUNC (get_width) (R)" = 2 * \ref 
 * genepi_plugin_get_width "GENEPI_PLUGIN_FUNC (get_width) (A)"
 *
 * \return a pointer to the structure encoding \c post(R,A)
 */
extern genepi_set_impl *
GENEPI_PLUGIN_FUNC (apply) (genepi_plugin_impl *plugin, genepi_set_impl *R, 
                            genepi_set_impl *A);

/*!
 * \brief Computes <tt>pre (R, A)</tt>
 * 
 * This function is similar to \ref genepi_plugin_apply "apply" but
 * instead of computing the image of \a A by the relation \a R, it computes
 * the set whose image is \a A by \a R. Formally, the function computes the
 * set:
 *
 * \f[applyinv(R,A) = \{ (x_1,\dots, x_n)\in A \mid \exists x_1,
 * \dots x_n, (x_1^\prime,x_1,\dots,x_n^\prime,x_n)\in R\}\f]
 * 
 * Note the interleaving of primed and not primed variables and also note that
 * the primed variable precedes its non-primed version.
 *
 * \remarks If this function is not implemented by the plugin, the \ref 
 * genepi-loader.h "loader" replaces it by a generic version.
 *
 * \param R the transition relation;
 * \param A the image by R of the computed relation.
 *
 * \pre \ref genepi_plugin_get_width 
 * "GENEPI_PLUGIN_FUNC (get_width) (R)" = 2 * \ref 
 * genepi_plugin_get_width "GENEPI_PLUGIN_FUNC (get_width) (A)"
 *
 * \return a pointer to the structure encoding \c pre(R,A)
 */
extern genepi_set_impl *
GENEPI_PLUGIN_FUNC (applyinv) (genepi_plugin_impl *plugin, genepi_set_impl *R, 
                               genepi_set_impl *A);

/*!
 * \brief Checks inclusion relation between \a X1 and \a X2
 * 
 * According to \a op the function returns a non null value if \a X1 and \a X2
 * satisfy \a X1 \a op \a X2 where \a op is interpreted on sets.
 *
 * \warning In order to be validated by the loader a plugin MUST implement this
 * function.
 *
 * \param X1 first operand of the comparison
 * \param op predicate over X1 and X2 to test
 * \param X2 second operand of the comparison
 *
 * \pre X1 != NULL
 * \pre X2 != NULL
 * \pre GENEPI_PLUGIN_FUNC (get_width) (X1) == GENEPI_PLUGIN_FUNC (get_width) 
 * (X2)
 *
 * \return a non null value if \a X1 op \a X2
 */
extern int 
GENEPI_PLUGIN_FUNC (compare) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
                              genepi_comparator op, genepi_set_impl *X2);

/*!
 * \brief Checks the emptiness of \a X.
 *
 * This function checks if the set represented by \a X is empty or not. 
 * 
 * \warning In order to be validated by the loader a plugin MUST implement this
 * function. 
 *
 * \param X the checked relation
 *
 * \pre X != NULL
 *
 * \return a non null value if \a X is the empty relation 
 */
extern int 
GENEPI_PLUGIN_FUNC (is_empty) (genepi_plugin_impl *plugin, genepi_set_impl *X);

/*!
 * \brief Checks is \a X is the complete relation.
 *
 * This function checks if the set represented by \a X is the complete relation
 * of arity \ref genepi_plugin_get_width 
 * "GENEPI_PLUGIN_FUNC (get_width) (X)".
 *
 * \remarks If it is not implemented by the plugin, the \ref genepi-loader.h 
 * "loader" replaces it by a generic version checking the emptyness of the 
 * complement of \a X.
 *
 * \param X the checked relation
 *
 * \pre X != NULL
 *
 * \return a non null value if \a X is the complete relation 
 */
extern int 
GENEPI_PLUGIN_FUNC (is_full) (genepi_plugin_impl *plugin, genepi_set_impl *X);

/*!
 * \brief Checks if the set is finite
 *
 * \remarks This function is not mandatory.
 *
 * \param X the checked relation
 *
 * \pre X != NULL
 * 
 * \return a non null value is the set is finite
 */
extern int 
GENEPI_PLUGIN_FUNC (is_finite) (genepi_plugin_impl *plugin, genepi_set_impl *X);

/*!
 * \brief Checks if the relation \a X depends on a subset of its column.
 * 
 * \a X is said to be dependent of columns \f$i_1, \dots, i_n\f$ (where 
 * \f$i_j\f$ is such that \f$sel[i_j] \not=0\f$) if the projection of \a X 
 * these columns is not the complete relation of arity \a n. 
 *
 * \remarks This function is not mandatory. If it is not implemented GENEPI
 * checks that \f$ X \not= invproject(project(sel,selsize),sel,selsize)\f$.
 *
 * \param X the relation on which we check dependency.
 * \param sel an array of Booleans identifying the columns on which the 
 * dependency is checked.
 * \param selsize the number of cells in sel.
 *
 * \pre X != NULL
 * \pre sel != NULL 
 * \pre selsize == GENEPI_PLUGIN_FUNC(get_width) (X)
 *
 * \return "a non null value" if \a X depends on selected columns.
 */
extern int
GENEPI_PLUGIN_FUNC (depend_on) (genepi_plugin_impl *plugin, genepi_set_impl *X,
                                const int *sel, int selsize);

/*!
 * \brief Computes an explicit representation of X.
 *
 * This function computes an array of vectors representing elements of the
 * set \a X. Each vector is an array of integers allocated by the function. 
 * If \a X is an infinite set then \a max parameter is used to indicate the
 * maximal number of vectors to return. The way the function choose the vectors
 * is not specifed.
 *
 * The vectors are returned via the \a psolutions parameter and the number of
 * vectors is set into the variable pointed by \a psize.
 *
 * If the \a max if negative then all the solutions are returned; in this case
 * the function can guess that X is finite.
 *
 * \remarks This function is not mandatory.
 *
 * \param X the set X
 * \param psolutions the address where the vectors are stored
 * \param psize the address of the integer receiving the number of solution
 * \param max the maximal number of solutions.
 * 
 * \pre X != NULL
 * \pre psolutions != NULL
 * \pre psize != NULL
 * \pre max >= 0 || GENEPI_PLUGIN_FUNC(is_finite) (X)
 */
extern void 
GENEPI_PLUGIN_FUNC (get_solutions) (genepi_plugin_impl *plugin, 
                                    genepi_set_impl *X, int ***psolutions, 
				    int *psize, int max);

/*!
 * \brief Displays all elements of \a X.
 *
 * This function displays all elements of \a X even if \a X is infinite.
 * The representation used to display \a X is not specified.
 *
 * \remarks This function is not mandatory.
 *
 * \param X the set to display
 * \param output the stream on which the result is displayed
 * \param varnames the names labelling the column of the relation; the size 
 *        of this array must be equal to the arity of the relation.
 *
 * \pre X != NULL
 * \pre output != NULL
 * \pre varnames != NULL
 */
extern void 
GENEPI_PLUGIN_FUNC (display_all_solutions) (genepi_plugin_impl *plugin, 
                                            genepi_set_impl *X, FILE *output,
					    const char * const *varnames);

/*!
 * \brief Displays the data structure used to encode \a X.
 *
 * \remarks This function is not mandatory.
 *
 * \param X the set to display
 * \param output the stream on which the result is displayed
 *
 * \pre X != NULL
 * \pre output != NULL
 */
extern void 
GENEPI_PLUGIN_FUNC (display_data_structure) (genepi_plugin_impl *plugin, 
                                             genepi_set_impl *X, FILE *output);

/*!
 * \brief Arity of \a X.
 *
 * This function returns the arity or width of the given \a X
 *
 * \warning In order to be validated by the loader a plugin MUST implement this
 * function.
 * 
 * \param X the considered relation
 *
 * \pre X != NULL
 *
 * \return the arity of \a X
 */
extern int 
GENEPI_PLUGIN_FUNC (get_width) (genepi_plugin_impl *plugin, genepi_set_impl *X);

/*!
 * \brief The size allocated to \a X
 *
 * This function returns the internal size allocated to the set \a X. The 
 * semantics of the integer returned by this function is not specified but
 * must be consistent with size of the underlying data structure; it can be
 * a number of nodes in a graph or the number of characters in a string 
 * conversion of the data structure.
 *
 * \warning In order to be validated by the loader a plugin MUST implement this
 * function.
 *
 * \param X the considered relation
 *
 * \pre X != NULL
 *
 * \return the size of the internal encoding of \a X
 */
extern int 
GENEPI_PLUGIN_FUNC (get_data_structure_size) (genepi_plugin_impl *plugin, 
                                              genepi_set_impl *X);

/*!
 * \brief Checks if \a v/\a vden belongs to \a X.
 *
 * \remarks This function is not mandatory. If the function is not implemented
 * then it is replaced by the checking of the non-emptiness of the intersection
 * \a X with the solutions of \f$\bigwedge_{i=0}^{v\_size} vden\times x_i = 
 * v[i]\f$.
 *
 * \param X the considered relation
 * \param v the vector whose belonging is tested.
 * \param v_size the size of \a v
 * \param vden the denominator of \a v
 *
 * \pre X != NULL
 * \pre v != NULL 
 * \pre vden > 0
 * \pre v_size == GENEPI_PLUGIN_FUNC (get_width) (X)
 *
 * \return a non null value if \a v /\a vden\ is an element of \a X.
 */
extern int
GENEPI_PLUGIN_FUNC (is_solution) (genepi_plugin_impl *plugin, 
                                  genepi_set_impl *X, const int *v, int v_size,
                                  int vden);

/*!
 * \brief Gets one element in \a X.
 *
 * This function fills the vector \a x with an element of \a X. If the 
 * components of \a x are not integers then components are set to the same
 * denominator \a d which is return by the function. The way the function 
 * selects \a x is not specified.
 *
 * \remarks This function is not mandatory.
 *
 * \param X the considered relation
 * \param x the vector receiving the element of \a X
 * \param x_size the size of \a x
 *
 * \pre X != NULL
 * \pre x != NULL
 * \pre x_size == GENEPI_PLUGIN_FUNC (get_width) (X)
 * 
 * \post result >= 0
 *
 * \retval 0 if \a X is empty.
 * \retval the common denominator of the solution \a x
 */
extern int
GENEPI_PLUGIN_FUNC (get_one_solution) (genepi_plugin_impl *plugin, 
                                       genepi_set_impl *X, int *x, int x_size);

/*!
 * \brief Computes an hash value for \a X
 * 
 * \remarks This function is not mandatory but indicates to GENEPI whether
 * the data structure used by the plugin supports cache facility.
 *
 * \param X the considered set
 *
 * \pre X != NULL
 * 
 * \return an hash value of \a X
 */
extern unsigned int
GENEPI_PLUGIN_FUNC (cache_hash) (genepi_plugin_impl *plugin, 
                                 genepi_set_impl *X);

/*!
 * \brief Checks that two sets are equal in the cache.
 *  
 * \remarks This function is not mandatory but indicates to GENEPI whether
 * the data structure used by the plugin supports cache facility.
 *
 * \param X1 first operand of the comparison
 * \param X2 second operand of the comparison
 *
 * \pre X1 != NULL
 * \pre X2 != NULL
 *
 * \remarks This function is not mandatory.
 */
extern int
GENEPI_PLUGIN_FUNC (cache_equal) (genepi_plugin_impl *plugin, 
                                  genepi_set_impl *X1, genepi_set_impl *X2);

/*!
 * \brief Dump the set \a X onto the \a output stream
 * 
 * This function can be used to serialize a set o\a X nto an \a output stream. 
 * The function returns the number of bytes actually written on the stream. If
 * the result is negative or null then an error has occured and the returned
 * has to be interpreted as a \ref genepi_io_error.
 *
 * \param output the stream where data are written.
 * \param set the dumped GENEPI set
 *
 * \pre output != NULL
 * \pre set != NULL
 *
 * \return the number of bytes written on \a output or an error code
 *
 * \see genepi_plugin_read_set
 */
extern int
GENEPI_PLUGIN_FUNC (write_set) (genepi_plugin_impl *plugin, FILE *output, 
                                genepi_set_impl *set);

/*!
 * \brief Load on the \a input stream a serialized set
 * 
 * This function is used to reload a set serialized with the
 * \ref genepi_plugin_write_set write function. The loaded set is putted in \a 
 * *pset. 
 * The function returns the number of bytes actually readed. If a negative or 
 * null value is returned an error has occurred and the returned value has to 
 * be interpreted as \ref genepi_io_error.
 *
 * \param input the stream where the set is loaded
 * \param pset a pointer to the variable receiving the loaded set.
 *
 * read_set function.
 * \pre input != NULL
 * \pre pset != NULL
 *
 * \return the number of bytes read on \a input or an error code
 *
 * \see genepi_plugin_write_set
 */
extern int
GENEPI_PLUGIN_FUNC (read_set) (genepi_plugin_impl *plugin, FILE *input, 
                               genepi_set_impl **pset);

/*!
 * \brief Evaluate the pre-order over sets.
 * 
 * This function implements the pre-order relation \f$\preceq\f$ such that
 * for all sets \f$S_1\f$, \f$S_2\f$ and \f$S_3\f$:
 * <ul>
 * <li>\f$S_1\preceq S_2 \wedge S_2\preceq S_1 \iff S_1 \mbox{ and } S_2 
 * \mbox{ represent the same set}\f$
 * <li>\f$S_1\preceq S_2 \wedge S_2\preceq S_3 \Longrightarrow S_1\preceq S_3\f$
 * <li>\f$S_1\preceq S_2 \vee S_1\preceq S_2\f$
 * </ul>
 * \param X1 first operand
 * \param X2 second operand
 *
 * \pre X1 != NULL
 * \pre X2 != NULL
 *
 * \return a non null value of \a X1 \f$\preceq\f$ \a X2
 */
extern int
GENEPI_PLUGIN_FUNC (preorder) (genepi_plugin_impl *plugin, genepi_set_impl *X1,
                               genepi_set_impl *X2);

END_C_DECLS

#endif /* ! __GENEPI_PLUGIN_H__ */
