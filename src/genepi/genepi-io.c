/*
 * genepi-io.c -- Serialization routines
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <assert.h>
#include "genepi.h"
#include "genepi-io.h"

static int
s_write_compressed_uint32 (FILE *out, uint32_t i, int *perr);

static int
s_read_compressed_uint32 (FILE *in, uint32_t *pi, int *perr);

			/* --------------- */

int
genepi_io_write_uint8 (FILE *out, uint8_t i, int *perr)
{
  return s_write_compressed_uint32 (out, i, perr);
}

			/* --------------- */

int
genepi_io_write_int8 (FILE *out, int8_t i, int *perr)
{
  return genepi_io_write_uint8 (out, i, perr);
}

			/* --------------- */

int
genepi_io_write_uint16 (FILE *out, uint16_t i, int *perr)
{
  return s_write_compressed_uint32 (out, i, perr);
}

			/* --------------- */

int
genepi_io_write_int16 (FILE *out, int16_t i, int *perr)
{
  return genepi_io_write_uint16 (out, i, perr);
}

			/* --------------- */

int
genepi_io_write_uint32 (FILE *out, uint32_t i, int *perr)
{
  return s_write_compressed_uint32 (out, i, perr);
}

			/* --------------- */

int
genepi_io_write_int32 (FILE *out, int32_t i, int *perr)
{
  return genepi_io_write_uint32 (out, i, perr);
}

			/* --------------- */

int
genepi_io_write_string (FILE *out, const char *s, int *perr)
{
  int result;


  if (*perr < 0)
    return 0;
  
  if (s == NULL)
    result = genepi_io_write_uint8 (out, 0, perr);
  else
    {
      uint32_t len = strlen(s)+1;

      result = genepi_io_write_uint32 (out, len, perr);
      result += genepi_io_write_buffer (out, s, len, perr);
    }

  return result;
}

			/* --------------- */

int
genepi_io_write_buffer (FILE *out, const void *buf, size_t size, int *perr)
{
  assert (out != NULL);
  assert (buf != NULL);
  assert (perr != NULL);

  if (*perr < 0)
    return 0;

  if (fwrite (buf, 1, size, out) != size)
    {
      *perr = GENEPI_RW_ERR_IO;
      return 0;
    }

  return size;
}

			/* --------------- */

int
genepi_io_read_uint8 (FILE *in, uint8_t *pi, int *perr)
{
  uint32_t u;
  int result = genepi_io_read_uint32 (in, &u, perr);

  if (result > 0)
    {
      if (u > 0xFF)
	{
	  result = 0;
	  *perr = GENEPI_R_ERR_DATA_ERROR;
	}
      else
	{
	  *pi = (uint8_t) u;
	}
    }

  return result;
}

			/* --------------- */

int
genepi_io_read_int8 (FILE *in, int8_t *pi, int *perr)
{
  return genepi_io_read_uint8 (in, (uint8_t *)pi, perr);
}

			/* --------------- */

int
genepi_io_read_uint16 (FILE *in, uint16_t *pi, int *perr)
{
  uint32_t u;
  int result = genepi_io_read_uint32 (in, &u, perr);

  if (result > 0)
    {
      if (u > 0xFF)
	{
	  result = 0;
	  *perr = GENEPI_R_ERR_DATA_ERROR;
	}
      else
	{
	  *pi = (uint16_t) u;
	}
    }

  return result;
}

			/* --------------- */

int
genepi_io_read_int16 (FILE *in, int16_t *pi, int *perr)
{
  return genepi_io_read_uint16 (in, (uint16_t *)pi, perr);}

			/* --------------- */

int
genepi_io_read_uint32 (FILE *in, uint32_t *pi, int *perr)
{
  return s_read_compressed_uint32 (in, pi, perr);
}

			/* --------------- */

int
genepi_io_read_int32 (FILE *in, int32_t *pi, int *perr)
{
  return genepi_io_read_uint32 (in, (uint32_t *)pi, perr);
}

			/* --------------- */

int
genepi_io_read_string (FILE *in, char **ps, int *perr)
{
  uint32_t len;
  int result = genepi_io_read_uint32 (in, &len, perr);
  
  if (result > 0)
    {
      if (len == 0)
	*ps = NULL;
      else
	{
	  *ps = calloc( sizeof (char), len);
	  if (genepi_io_read_buffer (in, *ps, len, perr) == len)
	    {
	      result += len;
	    }
	  else
	    {
	      free (*ps);
	      *ps = NULL;
	      result = 0;
	    }
	}
    }

  return result;
}

			/* --------------- */

int
genepi_io_read_buffer (FILE *in, void *buf, size_t size, int *perr)
{
  assert (in != NULL);
  assert (buf != NULL);
  assert (perr != NULL);

  if (*perr < 0)
    return 0;

  if (fread (buf, 1, size, in) != size)
    {
      *perr = GENEPI_RW_ERR_IO;
      return 0;
    }

  return size;
}

			/* --------------- */

static int
s_write_compressed_uint32 (FILE *out, uint32_t i, int *perr)
{
  uint8_t buf[5];
  uint8_t nb_to_write;
  int result;

  assert (out != NULL);
  assert (perr != NULL);

  if (*perr < 0)
    return 0;

  if( i < 192 ) 
    {
      nb_to_write = 1;
      buf[0] = (uint8_t)i;
    } 
  else if( 192 <= i && i < 8384 ) 
    {
      nb_to_write = 2;
      i -= 192;
      buf[0] = ((uint8_t)((i)>>8))+192;
      buf[1] = (uint8_t)i;
    } 
  else 
    {
      nb_to_write = 5;
      buf[0] = (uint8_t)0xFF;
      buf[1] = (uint8_t)((i)>>24);
      buf[2] = (uint8_t)((i)>>16);
      buf[3] = (uint8_t)((i)>>8);
      buf[4] = (uint8_t)((i));
    }
  result = fwrite (buf, sizeof (uint8_t), nb_to_write, out);
  if (result != nb_to_write)
    {
      *perr = GENEPI_RW_ERR_IO;
      result = 0;
    }

  return result;  
}

			/* --------------- */

static int
s_read_compressed_uint32 (FILE *in, uint32_t *pi, int *perr)
{
  uint8_t b[4];
  int result;

  assert (in != NULL);
  assert (pi != NULL);
  assert (perr != NULL);

  if (*perr < 0)
    return 0;

  *pi = 0;
  
  if ((result = fread (&b[0], sizeof (uint8_t), 1, in)) == 1 )
    {
      if (b[0] < 192)
	{
	  *pi = (uint32_t)b[0];
	}
      else if (b[0] == 255)
	{
	  if ((result = fread (b, sizeof (uint8_t), 4, in)) == 4)
	    {
	      *pi = ((((uint32_t)(b[0]))<<24) |
		     (((uint32_t)(b[1]))<<16) |
		     (((uint32_t)(b[2]))<<8) |
		     (((uint32_t)(b[3]))));
	      result++;
	    }
	  else
	    {
	      result = 0;
	    }
	}
      else 
	{
	  if ((result = fread (&b[1], sizeof (uint8_t), 1, in)) == 1)
	    {
	      *pi = ((((uint32_t)b[0])-192)<<8)+((uint32_t)b[1])+192;
	      result++;
	    }
	  else
	    result = 0;
	}      

    }
  else
    {
      result = 0;
    }

  if (result == 0)
    {
      *pi = 0;
      *perr = GENEPI_RW_ERR_IO;
    }

  return result;
}
