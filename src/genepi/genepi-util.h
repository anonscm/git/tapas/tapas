/*
 * genepi-util.h --  Generic operations on GENEPI sets
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file genepi/genepi-util.h
 * \brief Generic operations on GENEPI sets
 */
#ifndef __GENEPI_UTIL_H__
# define __GENEPI_UTIL_H__

# include <genepi/genepi.h>

BEGIN_C_DECLS

/*!
 * \brief Computes \f$\overline{S1} \cup S2\f$
 */
extern genepi_set *
genepi_set_imply (genepi_solver *solver, genepi_set *S1, genepi_set *S2);

/*!
 * \brief Computes \f$\overline{S1}\cap\overline{S2} \cup S1\cap S2\f$
 */
extern genepi_set *
genepi_set_equiv (genepi_solver *solver, genepi_set *S1, genepi_set *S2);

/*!
 * \brief Computes \f$i \cap t \cup \overline{i} \cap e\f$
 */
extern genepi_set *
genepi_set_if_then_else (genepi_solver *solver, genepi_set *i, genepi_set *t,
			 genepi_set *e);

/*!
 * \brief Computes \f$\{ z \mid \exists x_{i}\in X[i]\mbox{for i =} 1\dots n, 
 * z = \Sigma_{i=1}^n \alpha[i].x_i + c \}\f$
 */
extern genepi_set *
genepi_set_linear_transform (genepi_solver *solver, int n, int *alpha, int c, 
			     genepi_set **X);

/*!
 * \brief Computes \f$\{ z \mid \exists x \in X, \exists x \in Y, z = a.x+b.y 
 * \}\f$
 */
static inline genepi_set *
genepi_set_aX_plus_bY (genepi_solver *solver, int a, genepi_set *X, int b, 
		       genepi_set *Y);

/*!
 * \brief Computes \f$\{ z \mid \exists x \in X, \exists x \in Y, z = x+y \}\f$
 */
static inline genepi_set *
genepi_set_add (genepi_solver *solver, genepi_set *X, genepi_set *Y);

/*!
 * \brief Computes \f$\{ z \mid \exists x \in X, 
 * cden . z[i] = cden.a.x[i] + c[i] \}\f$
 */
extern genepi_set *
genepi_set_add_constant (genepi_solver *solver, int a, genepi_set *X, int *c, 
			 int cden);

/*!
 * \brief Computes \f$\{ z \mid \exists x \in X, \exists x \in Y, z = x-y \}\f$
 */
static inline genepi_set *
genepi_set_sub (genepi_solver *solver, genepi_set *X, genepi_set *Y);

/*!
 * \brief Computes \f$\{ s.x \mid s\in {\cal Z} \wedge x \in X \}\f$
 */
static inline genepi_set *
genepi_set_scale (genepi_solver *solver, int s, genepi_set *X);

/*!
 * \brief Given a set \a X of scalar values and a \a vector/\a den, computes 
 * the set \f$\{ \vec{z} \mid \exists x \in X den.\vec{z} = x.\vec{vector} \}\f$
 */
extern genepi_set *
genepi_set_mul_vector (genepi_solver *solver, genepi_set *X, int width, 
		       const int *vector, int den);

/*!
 * \brief Computes \f$\{ -x \mid x \in X \}\f$
 */
static inline genepi_set *
genepi_set_neg (genepi_solver *solver, genepi_set *X);

/*!
 * \brief Computes \f$X\times Y\f$
 */
extern genepi_set *
genepi_set_cartesian_product (genepi_solver *solver, genepi_set *X, 
			      genepi_set *Y);

/*!
 * \brief Computes \f$\{ (vector[0]/den,\dots,vector[width-1]/den)\}\f$
 */
extern genepi_set *
genepi_set_singleton (genepi_solver *solver, int width, const int *vector, 
		      int den);

/*!
 * \brief Computes \f$X \setminus Y \cup Y \setminus X \f$
 */
extern genepi_set *
genepi_set_delta (genepi_solver *solver, genepi_set *X, genepi_set *Y);

/*!
 * \brief Computes \f$X \setminus Y\f$
 */
extern genepi_set *
genepi_set_diff (genepi_solver *solver, genepi_set *X, genepi_set *Y);

/*!
 * \brief Computes a genepi_set encoding the interleaving of columns of X[i]s
 */
extern genepi_set *
genepi_set_interleave (genepi_solver *solver, int nb_X, genepi_set **X);

/*!
 * \brief Computes \f$\{ (x_1,x_1+x_2,x_2) \in X\times (X+X)\times X | x_1+x_2\not\in X \f$
 */
extern genepi_set *
genepi_set_instable (genepi_solver *solver, genepi_set *X);

/*!
 * \brief Computes \f$X^*\f$
 */
extern genepi_set *
genepi_set_monoid (genepi_solver *solver, genepi_set *X);


END_C_DECLS

# include <genepi/genepi-util-inline.h>

#endif /* ! __GENEPI_UTIL_H__ */
