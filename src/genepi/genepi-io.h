/*
 * genepi-io.h -- Header for serialization routines
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */
/*!
 * \file genepi/genepi-io.h
 * \brief Useful functions used to serialize GENEPI sets.
 * 
 * This module gathers functions that write some primitive C type into a FILE
 * stream. In particular these functions take care of endianness of integers.
 * 
 * Each function returns the number of bytes actually written on or readed from
 * the stream. If an error occurs then a \ref genepi_io_error value is stored
 * into \a *perr. If a function is called with a \a *perr that is negative
 * then the function simply returns 0.
 *
 * \remark The number of bytes written on the stream is not necessarily equal 
 * to the size of the data.
 */
#ifndef __GENEPI_IO_H__
# define __GENEPI_IO_H__

# include <stdio.h>
# include <genepi/genepi-common.h>

BEGIN_C_DECLS

/*!
 * \brief Write an 8-bit unsigned integer on the \a out stream.
 * \param out the ouput stream
 * \param i the written integer
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes written on the stream \a out
 */
extern int
genepi_io_write_uint8 (FILE *out, uint8_t i, int *perr);

/*!
 * \brief Write an 8-bit signed integer on the \a out stream.
 * \param out the ouput stream
 * \param i the written integer
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes written on the stream \a out
 */
extern int
genepi_io_write_int8 (FILE *out, int8_t i, int *perr);

/*!
 * \brief Write a 16-bit unsigned integer on the \a out stream.
 * \param out the ouput stream
 * \param i the written integer
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes written on the stream \a out
 */
extern int
genepi_io_write_uint16 (FILE *out, uint16_t i, int *perr);

/*!
 * \brief Write a 16-bit signed integer on the \a out stream.
 * \param out the ouput stream
 * \param i the written integer
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes written on the stream \a out
 */
extern int
genepi_io_write_int16 (FILE *out, int16_t i, int *perr);

/*!
 * \brief Write a 32-bit unsigned integer on the \a out stream.
 * \param out the ouput stream
 * \param i the written integer
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes written on the stream \a out
 */
extern int
genepi_io_write_uint32 (FILE *out, uint32_t i, int *perr);

/*!
 * \brief Write a 32-bit signed integer on the \a out stream.
 * \param out the ouput stream
 * \param i the written integer
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes written on the stream \a out
 */
extern int
genepi_io_write_int32 (FILE *out, int32_t i, int *perr);

/*!
 * \brief Write a string on the \a out stream.
 * The string \a s can be null in which case the NULL value will be reloaded
 * by the \ref genepi_io_read_string "read_string" function.
 *
 * \param out the ouput stream
 * \param s the string
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes written on the stream \a out
 */
extern int
genepi_io_write_string (FILE *out, const char *s, int *perr);

/*!
 * \brief Write the content of the buffer \a buf on the \a out stream.
 * Oppositely to strings the given buffer must not be NULL.
 *
 * \param out the ouput stream
 * \param buf the written buffer
 * \param size the size of \a buffer, in bytes 
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre buf != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes written on the stream \a out
 */
extern int
genepi_io_write_buffer (FILE *out, const void *buf, size_t size, int *perr);

/*!
 * \brief Read an 8-bit unsigned integer on the \a in stream. The signedness
 * of the integer is not checked.
 *
 * \param in the input stream
 * \param pi a pointer to the variable which receive the readed value
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes readed on the stream \a in
 */
extern int
genepi_io_read_uint8 (FILE *in, uint8_t *pi, int *perr);

/*!
 * \brief Read an 8-bit signed integer on the \a in stream. The signedness
 * of the integer is not checked.
 *
 * \param in the input stream
 * \param pi a pointer to the variable which receive the readed value
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes readed on the stream \a in
 */
extern int
genepi_io_read_int8 (FILE *in, int8_t *pi, int *perr);

/*!
 * \brief Read a 16-bit unsigned integer on the \a in stream. The signedness
 * of the integer is not checked.
 *
 * \param in the input stream
 * \param pi a pointer to the variable which receive the readed value
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes readed on the stream \a in
 */
extern int
genepi_io_read_uint16 (FILE *in, uint16_t *pi, int *perr);

/*!
 * \brief Read a 16-bit signed integer on the \a in stream. The signedness
 * of the integer is not checked.
 *
 * \param in the input stream
 * \param pi a pointer to the variable which receive the readed value
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes readed on the stream \a in
 */
extern int
genepi_io_read_int16 (FILE *in, int16_t *pi, int *perr);

/*!
 * \brief Read a 32-bit unsigned integer on the \a in stream. The signedness
 * of the integer is not checked.
 *
 * \param in the input stream
 * \param pi a pointer to the variable which receive the readed value
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes readed on the stream \a in
 */
extern int
genepi_io_read_uint32 (FILE *in, uint32_t *pi, int *perr);

/*!
 * \brief Read a 32-bit signed integer on the \a in stream. The signedness
 * of the integer is not checked.
 *
 * \param in the input stream
 * \param pi a pointer to the variable which receive the readed value
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes readed on the stream \a in
 */
extern int
genepi_io_read_int32 (FILE *in, int32_t *pi, int *perr);

/*!
 * \brief Read a string on the \a in stream. The readed string might be NULL.
 *
 * \param in the input stream
 * \param ps a pointer to the variable which receive the readed string
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes readed on the stream \a in
 */
extern int
genepi_io_read_string (FILE *in, char **ps, int *perr);

/*!
 * \brief Read \a size bytes the \a in stream. 
 *
 * \param in the input stream
 * \param buf a pointer to buffer receiving the readed bytes
 * \param size the number of bytes to read.
 * \param perr a pointer to the error flag
 * \pre out != NULL
 * \pre perr != NULL
 *
 * \returns the number of bytes readed on the stream \a in
 */
extern int
genepi_io_read_buffer (FILE *in, void *buf, size_t size, int *perr);

END_C_DECLS

#endif /* ! __GENEPI_IO_H__ */
