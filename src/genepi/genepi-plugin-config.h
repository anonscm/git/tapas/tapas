/*
 * genepi-plugin-config.h -- Configuration tables for plugin
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file genepi/genepi-plugin-config.h
 * \brief Configuration tables for plugin
 * 
 */
#ifndef __GENEPI_PLUGIN_CONFIG_H__
# define __GENEPI_PLUGIN_CONFIG_H__

# include <stdio.h>
# include <genepi/genepi-common.h>

BEGIN_C_DECLS

typedef struct genepi_plugconf_st genepi_plugconf;

extern genepi_plugconf *
genepi_plugconf_create (void);

extern int
genepi_plugconf_load (genepi_plugconf *conf, FILE *input, const char *location);

extern void
genepi_plugconf_save (genepi_plugconf *conf, FILE *output);

extern genepi_plugconf *
genepi_plugconf_add_reference (genepi_plugconf *conf);

extern void
genepi_plugconf_del_reference (genepi_plugconf *conf);

extern genepi_plugconf * 
genepi_plugconf_duplicate (const genepi_plugconf *conf);

extern void
genepi_plugconf_merge (genepi_plugconf *conf, const genepi_plugconf *other);

extern void
genepi_plugconf_add_entry (genepi_plugconf *conf, const char *key, 
			   const char *value);

extern const char *
genepi_plugconf_get_entry (genepi_plugconf *conf, const char *key,
			   const char *default_value);

extern int
genepi_plugconf_get_bool (genepi_plugconf *conf, const char *key, 
			  int default_value);

extern int
genepi_plugconf_get_int (genepi_plugconf *conf, const char *key,
			 int default_value);

extern unsigned int
genepi_plugconf_get_unsigned_int (genepi_plugconf *conf, const char *key,
				  unsigned int default_value);

END_C_DECLS

#endif /* ! __GENEPI_PLUGIN_CONFIG_H__ */
