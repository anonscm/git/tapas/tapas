/*
 * genepi-util.c -- Generic GENEPI routines 
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <assert.h>
#include "genepi-util.h"

genepi_set *
genepi_set_imply (genepi_solver *solver, genepi_set *S1, genepi_set *S2)
{
  genepi_set *tmp1 = genepi_set_complement (solver, S1);
  genepi_set *result = genepi_set_union (solver, tmp1, S2);
  genepi_set_del_reference (solver, tmp1);

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_equiv (genepi_solver *solver, genepi_set *S1, genepi_set *S2)
{
  genepi_set *tmp1 = genepi_set_imply (solver, S1, S2);
  genepi_set *tmp2 = genepi_set_imply (solver, S2, S1);
  genepi_set *result = genepi_set_intersection (solver, tmp1, tmp2);
  genepi_set_del_reference (solver, tmp1);
  genepi_set_del_reference (solver, tmp2);

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_if_then_else (genepi_solver *solver, genepi_set *i, genepi_set *t,
			 genepi_set *e)
{
  genepi_set *i_and_t = genepi_set_intersection (solver, i, t);
  genepi_set *not_i = genepi_set_complement (solver, i);
  genepi_set *not_i_and_e = genepi_set_intersection (solver, not_i, e);
  genepi_set *result = genepi_set_intersection (solver, i_and_t, not_i_and_e);
  genepi_set_del_reference (solver, i_and_t);
  genepi_set_del_reference (solver, not_i);
  genepi_set_del_reference (solver, not_i_and_e);

  return result;
}

			/* --------------- */

/*
genepi_set *
genepi_set_linear_transform (genepi_solver *solver, int nb_sets,
			     int *coefficients, genepi_set **sets)
{
  int i, j;
  int *sel;
  genepi_set *tmp[3];
  genepi_set *result = NULL;
  int width = genepi_set_get_width (solver, sets[0]);
  int total_width = (nb_sets + 1) * width;
  int *selection = (int *) calloc (sizeof (int), total_width);

  for (i = 0; i < total_width; i++)
    selection[i] = 1;

  for (sel = selection, j = 0; j < width; j++, sel += nb_sets + 1)
    sel[0] = 0;
  
  result = genepi_set_invproject (solver, sets[0], selection, total_width);
      
  for (sel = selection, j = 0; j < width; j++, sel += nb_sets + 1)
    sel[0] = 1;

  for (i = 1; i < nb_sets; i++)
    {
      for (sel = selection, j = 0; j < width; j++, sel += nb_sets + 1)
	sel[i] = 0;

      tmp[0] = genepi_set_invproject (solver, sets[i], selection, total_width);
      tmp[1] = genepi_set_intersection (solver, result, tmp[0]);
      genepi_set_del_reference (solver, tmp[0]);
      genepi_set_del_reference (solver, result);
      result = tmp[1];

      for (sel = selection, j = 0; j < width; j++, sel += nb_sets + 1)
	sel[i] = 1;
    }

  for (i = 0; i < total_width; i++)
    selection[i] = 0;

  tmp[2] = NULL;

  for (sel = selection, i = 0; i < width ; i++, sel += nb_sets + 1)
    {
      for (j = 0; j < nb_sets; j++)
	sel[j] = - coefficients[j];
      sel[j] = 1;

      tmp[0] = genepi_set_linear_equality (solver, selection, total_width, 0);
      if (tmp[2] == NULL)
	tmp[2] = tmp[0];
      else
	{
	  tmp[1] = genepi_set_intersection (solver, tmp[2], tmp[0]);
	  genepi_set_del_reference (solver, tmp[2]);
	  genepi_set_del_reference (solver, tmp[0]);
	  tmp[2] = tmp[1];
	}

      for (j = 0; j < nb_sets; j++)
	sel[j] = 0;
      sel[j] = 0;
    }
  
  tmp[0] = genepi_set_intersection (solver, result, tmp[2]);
  genepi_set_del_reference (solver, result);
  genepi_set_del_reference (solver, tmp[2]);
  result = tmp[0];

  for (sel = selection, i = 0; i < width ; i++, sel += nb_sets + 1)
    {
      for (j = 0; j < nb_sets; j++)
	sel[j] = 1;
      sel[j] = 0;
    }

  tmp[0] = genepi_set_project (solver, result, selection, total_width);
  genepi_set_del_reference (solver, result);
  result = tmp[0];
  free (selection);

  assert (genepi_set_get_width (solver, result) == width);

  return result;
}
*/

genepi_set *
genepi_set_linear_transform (genepi_solver *solver, int nb_sets, 
			     int *coefficients, int c, genepi_set **sets)
{
  int i;
  genepi_set *tmp[3];
  genepi_set *result = NULL;
  int width = genepi_set_get_width (solver, sets[0]);
  int total_width = nb_sets * width;
  int *selection = (int *) calloc (sizeof (int), total_width + 1);

  result = genepi_set_interleave (solver, nb_sets, sets);

  for (i = 0; i < width; i++)
    {      
      int j;

      /* add the column to compute the ith linear term */
      selection[i + nb_sets] = 1;
      tmp[0] = genepi_set_invproject (solver, result, selection, 
				      total_width + 1);
      genepi_set_del_reference (solver, result);
      selection[i + nb_sets] = 0;

      /* compute the linear term */
      for (j = 0; j < nb_sets; j++)
	selection[i + j] = - coefficients[j];
      selection[i + j] = 1;
      tmp[1] = genepi_set_linear_equality (solver, selection, total_width + 1, 
					   -c);
      tmp[2] = genepi_set_intersection (solver, tmp[0], tmp[1]);
      genepi_set_del_reference (solver, tmp[0]);
      genepi_set_del_reference (solver, tmp[1]);

      /* project on the column of the value */
      for (j = 0; j < nb_sets; j++)
	selection[i + j] = 1;
      selection[i + j] = 0;
      result = genepi_set_project (solver, tmp[2], selection, total_width + 1);
      genepi_set_del_reference (solver, tmp[2]);
      total_width -= nb_sets - 1;

      assert (genepi_set_get_width (solver, result) == 
	      (i + 1) + (width - i - 1) * nb_sets);
      for (j = 0; j < nb_sets; j++)
	selection[i + j] = 0;      
    }
  free (selection);

  assert (genepi_set_get_width (solver, result) == width);

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_add_constant (genepi_solver *solver, int a, genepi_set *X, int *c, 
			 int cden)
{
  int i;
  genepi_set *tmp[3];
  genepi_set *result;
  int width = genepi_set_get_width (solver, X);
  int *selection = calloc (sizeof (int), 2 * width); 

  for (i = 0; i < width; i++)
    {
      selection[2 * i] = 0;
      selection[2 * i + 1] = 1;
    }
  result = genepi_set_invproject (solver, X, selection, 2 * width);

  for (i = 0; i < width; i++)
    selection[2 * i] = selection[2 * i + 1] = 0;

  a = -a *cden;

  for (tmp[2] = NULL, i = 0; i < width; i++)
    {
      selection[2 * i] = a;
      selection[2 * i + 1] = cden;
      tmp[0] = genepi_set_linear_equality (solver, selection, 2 * width, c[i]);
      if (tmp[2] == NULL)
	tmp[2] = tmp[0];
      else
	  {
	    tmp[1] = genepi_set_intersection (solver, tmp[2], tmp[0]);
	    genepi_set_del_reference (solver, tmp[0]);
	    genepi_set_del_reference (solver, tmp[2]);
	    tmp[2] = tmp[1];
	  }
      selection[2 * i] = 0;
      selection[2 * i + 1] = 0;
    }

  tmp[0] = genepi_set_intersection (solver, result, tmp[2]);
  genepi_set_del_reference (solver, result);
  genepi_set_del_reference (solver, tmp[2]);
  
  for (i = 0; i < width; i++)
    {
      selection[2 * i] = 1;
      selection[2 * i + 1] = 0;
    }
  result = genepi_set_project (solver, tmp[0], selection, 2 * width);
  genepi_set_del_reference (solver, tmp[0]);
  free (selection);

  return result;
}

			/* --------------- */

static int
s_is_zero_vector (int width, const int *vector)
{
  int i;

  for (i = 0; i < width; i++)
    if (vector[i])
      return 0;

  return 1;
}

			/* --------------- */

genepi_set *
genepi_set_mul_vector (genepi_solver *solver, genepi_set *X, int width, 
		       const int *vector, int den)
{
  int i;
  int *sel;
  genepi_set *R;
  genepi_set *tmp[2];

  assert (genepi_set_get_width (solver, X) == 1);
  assert (den != 0);

  if (genepi_set_is_empty (solver, X))
    return genepi_set_bot (solver, width);
  if (s_is_zero_vector (width, vector))
    return genepi_set_singleton (solver, width, vector, den);
  
  sel = calloc (sizeof (int), width + 1);

  /* first we expand X with width additional columns; X is the first one. */
  for (i = 0; i < width; i++)
    sel[i + 1] = 1;
  R = genepi_set_invproject (solver, X, sel, width + 1);
  for (i = 0; i < width; i++)
    sel[i + 1] = 0;

  /* for each column y of the result, compute vector[i]*X - den * y = 0 */
  
  for (i = 0; i < width; i++)
    {
      sel[0] = vector[i];
      sel[i + 1] = -den;
      tmp[0] = genepi_set_linear_equality (solver, sel, width + 1, 0);
      tmp[1] = genepi_set_intersection (solver, R, tmp[0]);
      genepi_set_del_reference (solver, tmp[0]);
      genepi_set_del_reference (solver, R);
      R = tmp[1];
      sel[i + 1] = 0;
    }
  /* remove the first (i.e. X) column */
  sel[0] = 1;
  tmp[0] = genepi_set_project (solver, R, sel, width + 1);
  genepi_set_del_reference (solver, R);
  R = tmp[0];
  free (sel);

  assert (genepi_set_get_width (solver, R) == width);

  return R;
}

			/* --------------- */

genepi_set *
genepi_set_cartesian_product (genepi_solver *solver, genepi_set *X, 
			      genepi_set *Y)
{
  int i;
  genepi_set *tmp[2];
  genepi_set *result;
  int w1 = genepi_set_get_width (solver, X);
  int width = w1 + genepi_set_get_width (solver, Y);
  int *selection = (int *) calloc (sizeof (int), width);

  for (i = 0; i < w1; i++)
    selection[i] = 0;
  for (i = w1; i < width; i++)
    selection[i] = 1;    
  tmp[0] = genepi_set_invproject (solver, X, selection, width);

  for (i = 0; i < w1; i++)
    selection[i] = 1;
  for (i = w1; i < width; i++)
    selection[i] = 0;
  tmp[1] = genepi_set_invproject (solver, Y, selection, width);
  free (selection);

  result = genepi_set_intersection (solver, tmp[0], tmp[1]);
  genepi_set_del_reference (solver, tmp[0]);
  genepi_set_del_reference (solver, tmp[1]);

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_singleton (genepi_solver *solver, int width, const int *vector, 
		      int den)
{
  int i;
  int *alpha = (int *) calloc (sizeof (int), width);
  genepi_set *result = genepi_set_top (solver, width);
  genepi_set *tmp[2];

  assert (den != 0);

  for (i = 0; i < width; i++)
    {
      alpha[i] = den;      
      tmp[0] = genepi_set_linear_equality (solver, alpha, width, vector[i]);
      tmp[1] = genepi_set_intersection (solver, result, tmp[0]);
      genepi_set_del_reference (solver, result);
      genepi_set_del_reference (solver, tmp[0]);
      result = tmp[1];
      alpha[i] = 0; 
    }
  free (alpha);

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_diff (genepi_solver *solver, genepi_set *S1, genepi_set *S2)
{
  genepi_set *result = NULL;
  
  if (genepi_set_is_empty (solver, S2) || genepi_set_is_empty (solver, S1))
    result = genepi_set_add_reference (solver, S1);
  else if (genepi_set_is_full (solver, S2))
    result = genepi_set_bot (solver, genepi_set_get_width (solver, S1));
  else 
    {
      genepi_set *tmp = genepi_set_complement (solver, S2);
      
      if (genepi_set_is_full (solver, S1))
	result = tmp; 
      else 
	{
	  result = genepi_set_intersection (solver, S1, tmp);
	  genepi_set_del_reference (solver, tmp);
	}
    }

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_delta (genepi_solver *solver, genepi_set *S1, genepi_set *S2)
{
  genepi_set *result;
  
  if (genepi_set_equal (solver, S1, S2))
    result = genepi_set_bot (solver, genepi_set_get_width (solver, S1));
  else
    {
      genepi_set *d1 = genepi_set_diff (solver, S1, S2);
      genepi_set *d2 = genepi_set_diff (solver, S2, S1);
      result = genepi_set_union (solver, d1, d2);
      assert (! genepi_set_is_empty (solver, result));
      genepi_set_del_reference (solver, d1);
      genepi_set_del_reference (solver, d2);
    }

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_interleave (genepi_solver *solver, int nb_X, genepi_set **X)
{
  int i, j;
  int *sel;
  genepi_set *tmp[2];
  genepi_set *result;
  int width = genepi_set_get_width (solver, X[0]);
  int total_width = nb_X * width;
  int *selection = (int *) calloc (sizeof (int), total_width);

  for (i = 0; i < total_width; i++) 
    selection[i] = 1;

  for (sel = selection, j = 0; j < width; j++, sel += nb_X) 
    sel[0] = 0;
  
  result = genepi_set_invproject (solver, X[0], selection, total_width);
      
  for (sel = selection, j = 0; j < width; j++, sel += nb_X)
    sel[0] = 1;

  for (i = 1; i < nb_X; i++)
    {
      for (sel = selection, j = 0; j < width; j++, sel += nb_X)
	sel[i] = 0;

      tmp[0] = genepi_set_invproject (solver, X[i], selection, total_width);
      tmp[1] = genepi_set_intersection (solver, result, tmp[0]);
      genepi_set_del_reference (solver, tmp[0]);
      genepi_set_del_reference (solver, result);
      result = tmp[1];

      if (i != nb_X - 1)
	{
	  for (sel = selection, j = 0; j < width; j++, sel += nb_X) 
	    sel[i] = 1;
	}
    }
  free (selection);

  return result;
}

			/* --------------- */




/*!
 * \brief Computes \f$X^*\f$
 */
extern genepi_set *
genepi_set_monoid (genepi_solver *solver, genepi_set *X)
{
  int width=genepi_set_get_width(solver,X);
  genepi_set * sets[2];
  //int solution[width];
  int vector[width];
  //int i;
  
 
  genepi_set *K;
  // First $K=\{(0,\ldots,0)\}$    
  {		  
    int i;
    for (i=0; i<width;i++)
      vector[i]=0;
    K=genepi_set_singleton (solver, width, vector, 1);
  }
  
  // Compute $K=\bigcup_{i=0}^{width-1}\underbrace{Z+\cdots+Z}_{i\text{ times}}$
  if (width>1) {
    //We compute $N=X\cup \{(0,\ldots 0)\}$
    genepi_set *N = genepi_set_union(solver,X,K);
    int i;
    for(i=0; i<width ; i++) {
      sets[0]=K; 
      sets[1]=N; 
      int alpha[2]={1,1};
      genepi_set *tmp
	=genepi_set_linear_transform(solver, 2, alpha, 0, sets);
      genepi_set_del_reference(solver,K);
      K=tmp;	  	
    }
    genepi_set_del_reference(solver,N);
  }
  
  // Compute $R=X+K$
  genepi_set *R;
  {
    sets[0]=X; 
    sets[1]=K; 
    int alpha[2]= {1,1};
    R = genepi_set_linear_transform(solver, 2, alpha, 0, sets);
  }
  
  genepi_set *M=K;
  genepi_set_add_reference(solver,M);
  genepi_set *ball=R;
  genepi_set_add_reference(solver,ball);
  int max=-1; // means infinity
  int denx,denr;
  int r[width], x[width];
  
  while (1) {
    
    //we first compute a solution with respect to the max bound
    genepi_set *invM=genepi_set_complement(solver, M);      
    
    { 
      genepi_set *tmp=genepi_set_intersection(solver,ball, invM);
      denr=genepi_set_get_one_solution(solver,tmp,r,width);
      genepi_set_del_reference(solver, tmp);
    }
    
    if (denr==0) {
      genepi_set *tmp=genepi_set_intersection(solver,R, invM);
      denr=genepi_set_get_one_solution(solver,tmp,r,width);
      genepi_set_del_reference(solver, tmp);
      
      if (denr==0) {
	genepi_set_del_reference(solver,invM);
	genepi_set_del_reference(solver,ball);	
	genepi_set_del_reference(solver,K);
	genepi_set_del_reference(solver,R);
	return M;
      }
    }
    
    genepi_set_del_reference(solver,invM);
    
    int newmax=0;
    int i;
    for( i=0 ; i<width; i++) {
      int coef=r[i];
      if (coef<0)
	coef=-coef;
      if (coef > newmax)
	newmax=coef;
    }

    if (newmax!=max) {
      max=newmax;
      genepi_set_del_reference(solver,ball);
      ball=R;
      genepi_set_add_reference(solver,ball);
      //we compute the new ball
      //ie  ball=\{r in R | ||r||_\infty \leq max \}
      int i;
      for (i=0 ;i < width ; i++) { 
	vector[i]=1;
	genepi_set *tmp1;
	genepi_set *tmp2;
	
	tmp1= genepi_set_linear_operation(solver,vector,width,GENEPI_LEQ,max);
	tmp2= genepi_set_intersection(solver,tmp1,ball);
	genepi_set_del_reference(solver,ball);
	genepi_set_del_reference(solver,tmp1);
	ball=tmp2;
	
	tmp1= genepi_set_linear_operation(solver,vector,width,GENEPI_GEQ,-max);
	tmp2= genepi_set_intersection(solver,tmp1,ball);
	genepi_set_del_reference(solver,ball);
	genepi_set_del_reference(solver,tmp1);
	ball=tmp2;
	
	vector[i]=0;
      }
     

    }
    

    
#if 0    
    printf("\nr=(");
    for(i=0 ; i<width; i++)
      printf(" %d ",r[i]);
    printf(")/%d\n",denr);
#endif    

    
    // We compute x in X \cap (r-K)
    {
      //We first compute the set tmp={r/denr};
      genepi_set *singleton=genepi_set_singleton (solver, width, r, denr);
      sets[0]=singleton; 
      sets[1]=K;
      int alpha[2] ={1,-1};
      genepi_set *tmp
	=genepi_set_linear_transform(solver, 2, alpha, 0, sets);
      genepi_set_del_reference(solver,singleton);
      genepi_set* tmp2=genepi_set_intersection(solver,tmp, X);
      genepi_set_del_reference(solver,tmp);
      denx=genepi_set_get_one_solution(solver,tmp2,x,width);
      genepi_set_del_reference(solver,tmp2);
    }
#if 0    
    printf("\nx=(");
    for(i=0 ; i<width; i++)
      printf(" %d ",x[i]);
    printf(")/%d\n",denx);
#endif    


    // we replace M by M+x^*
    

   
    
    {
      genepi_set * topN=genepi_set_top_N(solver,1);	       
      genepi_set *monoidVector
	=genepi_set_mul_vector(solver, topN, width, x, denx);
      genepi_set_del_reference(solver,topN);
      
      sets[0]=monoidVector; 
      sets[1]=M;
      int alpha[2]={1,1};
      genepi_set *tmp
	=genepi_set_linear_transform(solver, 2, alpha, 0, sets);
      
      
      genepi_set_del_reference(solver,monoidVector);
      genepi_set_del_reference(solver,M);
      M=tmp;
    }
 
    
  }
}


                        /* --------------- */
