/*
 * genepi-generic.c -- Generic functions for plugin methods replacement
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <assert.h>
#include "genepi-p.h"

genepi_set *
genepi_set_generic_apply (genepi_solver *solver, genepi_set *R, genepi_set *A)
{
  int i;
  genepi_set *X0, *X1, *X2;
  int m = genepi_set_get_width (solver, A);
  int *selection = (int *) calloc (sizeof (int), 2*m);

  for (i = 0; i < m; i++)
    {
      selection[2*i] = 1;
      selection[2*i+1] = 0;
    }
  
  X0 = genepi_set_invproject (solver, A, selection, 2*m);
  X1 = genepi_set_intersection (solver, R, X0);
  genepi_set_del_reference (solver, X0);

  for (i = 0; i < m; i++)
    {
      selection[2*i] = 0;
      selection[2*i+1] = 1;
    }

  X2 = genepi_set_project (solver, X1, selection, 2*m);
  genepi_set_del_reference (solver, X1);
  free (selection);

  return X2;
}

			/* --------------- */

genepi_set *
genepi_set_generic_applyinv (genepi_solver *solver, genepi_set *R, 
			     genepi_set *A)
{
  int i;
  genepi_set *X0, *X1, *X2;
  int m = genepi_set_get_width (solver, A);
  int *selection = (int *) calloc (sizeof (int),2*m);

  for (i = 0; i < m; i++)
    {
      selection[2*i] = 0;
      selection[2*i+1] = 1;
    }

  X0 = genepi_set_invproject (solver, A, selection, 2*m);
  X1 = genepi_set_intersection (solver, R, X0);
  genepi_set_del_reference (solver, X0);

  for (i = 0; i < m; i++)
    {
      selection[2*i] = 1;
      selection[2*i+1] = 0;
    }
  X2 = genepi_set_project (solver, X1, selection, 2*m);
  genepi_set_del_reference (solver, X1);

  free (selection);

  return X2; 
}

			/* --------------- */

int
genepi_set_generic_depend_on (genepi_solver *solver, genepi_set *X, 
			      const int *sel, int selsize)
{
  int result;
  genepi_set *proj = genepi_set_project (solver, X, sel, selsize);
  genepi_set *unproj = genepi_set_invproject (solver, proj, sel, selsize);

  genepi_set_del_reference (solver, proj);
  result = ! genepi_set_equal (solver, X, unproj);
  genepi_set_del_reference (solver, unproj);
  
  return result;
}

			/* --------------- */

int 
genepi_set_generic_is_full (genepi_solver *solver, genepi_set *X)
{
  genepi_set *notX = genepi_set_complement (solver, X);
  int result = genepi_set_is_empty (solver, notX);
  genepi_set_del_reference (solver, notX);

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_add_positive_constraint(genepi_solver *solver, int width,
				   genepi_set *X)
{
  int i;
  int *alpha = (int *) calloc (sizeof (int), width);
  genepi_set *result = genepi_set_add_reference (solver, X);

  for (i = 0; i < width; i++)
    {
      genepi_set *set2;
      genepi_set *set1 = result;
      
      alpha[i] = 1;
      set2 = genepi_set_linear_operation (solver, alpha, width, GENEPI_GEQ, 0);
      result = genepi_set_intersection (solver, set1, set2);
      genepi_set_del_reference (solver, set1);
      genepi_set_del_reference (solver, set2);
      alpha[i] = 0;
    }
  free (alpha);

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_generic_top_N(genepi_solver *solver, int width)
{
  genepi_set *aux;
  genepi_set *result = NULL;

  switch (genepi_solver_get_solver_type (solver))
    {
    case GENEPI_N_SOLVER:
      result = genepi_set_top (solver, width);
      break;
    case GENEPI_Z_SOLVER:
      aux = genepi_set_top (solver, width);
      result = genepi_set_add_positive_constraint (solver, width, aux);
      genepi_set_del_reference (solver, aux);
      break;
    case GENEPI_P_SOLVER: 
    case GENEPI_R_SOLVER:
      aux = genepi_set_top_Z (solver, width);
      result = genepi_set_add_positive_constraint (solver, width, aux);
      genepi_set_del_reference (solver, aux);
      break;
    default:
      abort();
    }

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_generic_top_Z(genepi_solver *solver, int width)
{
  genepi_set *result = NULL;

  switch (genepi_solver_get_solver_type (solver))
    {
    case GENEPI_N_SOLVER:
    case GENEPI_Z_SOLVER:
      result = genepi_set_top (solver,width);
      break;
    case GENEPI_P_SOLVER:
    case GENEPI_R_SOLVER:
      check_method (solver, top_Z);
    default:
      abort();
    }

  return result;
}

			/* --------------- */

genepi_set *
genepi_set_generic_top_P(genepi_solver *solver, int width)
{
  genepi_set *aux;
  genepi_set *result = NULL;

  switch (genepi_solver_get_solver_type (solver))
    {
    case GENEPI_P_SOLVER:
    case GENEPI_N_SOLVER:
      result = genepi_set_top (solver,width);
      break;
    case GENEPI_Z_SOLVER:
      result = genepi_set_top_N (solver,width);
      break;
    case GENEPI_R_SOLVER:
      aux = genepi_set_top (solver, width);
      result = genepi_set_add_positive_constraint (solver, width, aux);
      genepi_set_del_reference (solver, aux);
      break;
    default:
      abort();
    }

  return result;
}

/*!
 * We check the emptyness of the intersection between X and the solutions
 * of equations vden*x_i = v[i] for i=0...v_size-1.
 */
int
genepi_set_generic_is_solution (genepi_solver *solver, genepi_set *X, 
				const int *v, int v_size, int vden)
{
  int i;
  genepi_set *tmp2;
  genepi_set *tmp = NULL;
  int *alpha = (int *) calloc (sizeof (int), v_size);
  int result;
  
  for (i = 0; i < v_size; i++)
    {
      alpha[i] = vden;
      tmp2 = genepi_set_linear_equality (solver, alpha, v_size, v[i]);

      if (tmp == NULL)
	tmp = genepi_set_add_reference (solver, tmp2);
      else
	{
	  genepi_set *tmp3 = genepi_set_intersection (solver, tmp, tmp2);
	  genepi_set_del_reference (solver, tmp);
	  tmp = tmp3;
	}
      genepi_set_del_reference (solver, tmp2);

      alpha[i] = 0;
    }
  
  assert (tmp != NULL);
  tmp2 = genepi_set_intersection (solver, tmp, X);
  genepi_set_del_reference (solver, tmp);

  result = ! genepi_set_is_empty (solver, tmp2);
  genepi_set_del_reference (solver, tmp2);
  free (alpha);

  return result;
}
