/*
 * genepi-autotest.c -- Small test suite for loaded GENEPI plugins
 *
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include "genepi-common.h"
#include <assert.h>
#include "genepi-p.h"
#include "genepi-util.h"

#define TEST_FILENAME "serialisation.tst"

static int
s_check_serialization (genepi_solver *solver, genepi_set *X);

static int 
s_run_generic_test (genepi_solver *solver);

static genepi_set *
s_x_is_even (genepi_solver *solver);

static genepi_set *
s_x_is_even_without_0 (genepi_solver *solver);

static genepi_set *
s_x_p_equals_x_plus_2 (genepi_solver *solver);

static genepi_set *
s_x_p_equals_x_minus_n (genepi_solver *solver, int n);

static genepi_set *
s_x_equals_n (genepi_solver *solver, int n);

static int
s_check_apply_vs_gen_apply (genepi_solver *solver, genepi_set *R, 
			    genepi_set *A, genepi_set *Res);

static genepi_set *
s_x_in_nat (genepi_solver *solver);

static int
s_check_depend_on (genepi_solver *solver, genepi_set *X, int n);

static int
s_check_top_N (genepi_solver *solver);

static int
s_check_is_solution (genepi_solver *solver);

static int
s_check_get_one_solution (genepi_solver *solver);

static int
s_check_mul_vector (genepi_solver *solver);

			/* --------------- */

int
genepi_solver_run_autotest (genepi_solver *solver)
{
  int result = 1;

  assert (solver != NULL);

  if (solver->plugin->autotest != NULL)
    {
      result = solver->plugin->autotest (solver->plugin->impl);
      if (!result)
	{
	  fprintf (stderr, "%s: internal autotest has failed.\n", 
		   genepi_solver_get_name (solver));	  
	}
    }
    
  if (result && solver->cache != NULL)
    {
      genepi_cache *cache = solver->cache;
      solver->cache = NULL;

      result = s_run_generic_test (solver);
      if (!result)
	{
	  fprintf (stderr, "%s: generic autotest without cache has failed.\n", 
		   genepi_solver_get_name (solver));	  
	}
      solver->cache = cache;
    }

  if (result)
    {
      result = s_run_generic_test (solver);
      if (!result)
	{
	  fprintf (stderr, "%s: generic autotest has failed.\n", 
		   genepi_solver_get_name (solver));	  
	}
    }

  return result;
}

			/* --------------- */

static int
s_run_generic_test (genepi_solver *solver)
{
  int result = 1;
  genepi_set *x_eq_2 = s_x_equals_n (solver, 2);
  genepi_set *x_eq_0 = s_x_equals_n (solver, 0);
  genepi_set *xp_eq_x_plus_2 = s_x_p_equals_x_plus_2 (solver);
  genepi_set *x_is_even = s_x_is_even (solver);
  genepi_set *x_is_even_without_0 = s_x_is_even_without_0 (solver);
  genepi_set *x_in_nat = s_x_in_nat (solver);
  genepi_set *xp_eq_x_minus_1 = s_x_p_equals_x_minus_n (solver, 1);
  genepi_set *x_neq_0 = genepi_set_complement (solver, x_eq_0);
  genepi_set *xp_eq_x_minus_4 = s_x_p_equals_x_minus_n (solver, 4);


  result = result && s_check_mul_vector (solver);
  if (! result)
    {
      fprintf(stderr, "fail on s_check_mul_vector ()\n");
      goto end;
    }

  result = result && 
    s_check_apply_vs_gen_apply (solver, xp_eq_x_plus_2, x_eq_0, x_eq_2);
  if (! result)
    {
      fprintf(stderr, "fail on apply(xp_eq_x_plus_2,x_eq_0,x_eq_2)\n");
      goto end;
    }

  result = result && 
    s_check_apply_vs_gen_apply (solver, xp_eq_x_plus_2, x_is_even, 
				x_is_even_without_0);
  if (! result) 
    {
      fprintf(stderr,"fail on apply(xp_eq_x_plus_2,x_is_even," 
	      "x_is_even_without_0)\n");
      goto end;
    }

  result = result && 
    s_check_apply_vs_gen_apply (solver, xp_eq_x_minus_1, x_in_nat, x_in_nat); 
  if (! result)
    {
      fprintf(stderr, "fail on apply(xp_eq_x_minus_1,x_in_nat,x_in_nat)\n"); 
      goto end;
    }


  result = result && 
    s_check_apply_vs_gen_apply (solver, xp_eq_x_minus_1, x_neq_0, x_in_nat);
  if (! result)
    {
      fprintf(stderr, "fail on apply(xp_eq_x_minus_1,x_neq_0,x_in_nat)\n"); 
      goto end;
    }

  result = result && 
    s_check_apply_vs_gen_apply (solver, xp_eq_x_minus_4, x_in_nat, x_in_nat);
  
  if (! result)
    {
      fprintf(stderr, "fail on apply(xp_eq_x_minus_4,x_in_nat,x_in_nat)\n"); 
      goto end;
    }


  result = result && s_check_depend_on (solver, xp_eq_x_minus_4,2);
  if (! result)
    {
      fprintf(stderr, "fail on s_check_depend_on (xp_eq_x_minus_4,2)\n"); 
      goto end;
    }

  result = result && s_check_top_N (solver);
  if (! result)
    {
      fprintf(stderr, "fail on s_check_top_N ()\n");
      goto end;
    }

  if (solver->plugin->get_one_solution != NULL)
    {      
      result = result && s_check_get_one_solution (solver);
      if (! result)
	{
	  fprintf(stderr, "fail on s_check_get_one_solution ()\n");
	  goto end;
	  }
	  
      result = result && s_check_is_solution (solver);
      if (! result)
	{
	  fprintf(stderr, "fail on s_check_is_solution ()\n");
	  goto end;
	}
    }

 end:
  genepi_set_del_reference (solver, xp_eq_x_minus_4); 
  genepi_set_del_reference (solver, x_neq_0); 
  genepi_set_del_reference (solver, xp_eq_x_minus_1); 
  genepi_set_del_reference (solver, x_in_nat); 
  genepi_set_del_reference (solver, x_is_even_without_0); 
  genepi_set_del_reference (solver, x_is_even); 
  genepi_set_del_reference (solver, xp_eq_x_plus_2); 
  genepi_set_del_reference (solver, x_eq_0); 
  genepi_set_del_reference (solver, x_eq_2); 

  return result;
}

			/* --------------- */

static genepi_set *
s_x_is_even (genepi_solver *solver)
{
  int coefs[2] = { 1, -2 };
  genepi_set *a = genepi_set_linear_equality (solver, coefs, 2, 0);
  int coefs1[2] = { 1, 0 };
  genepi_set *tmp1 = 
    genepi_set_linear_operation (solver, coefs1, 2, GENEPI_GEQ, 0);
  int coefs2[2] = { 0, 1 };
  genepi_set *tmp2 = 
    genepi_set_linear_operation (solver, coefs2, 2, GENEPI_GEQ, 0);
  genepi_set *tmp3 = genepi_set_intersection (solver, a, tmp1);
  genepi_set *tmp4 = genepi_set_intersection (solver, tmp3, tmp2);
  int sel[2] = { 0, 1 };
  genepi_set *R = genepi_set_project (solver, tmp4, sel, 2);

  genepi_set_del_reference (solver, a);
  genepi_set_del_reference (solver, tmp1);
  genepi_set_del_reference (solver, tmp2);
  genepi_set_del_reference (solver, tmp3);
  genepi_set_del_reference (solver, tmp4);

  return R;
}

			/* --------------- */


static genepi_set *
s_x_is_even_without_0 (genepi_solver *solver)
{
  int coefs[2] = { 1, -2 };
  genepi_set *a = genepi_set_linear_equality (solver, coefs, 2, 2);
  int coefs1[2] = { 1, 0 };
  genepi_set *tmp1 = 
    genepi_set_linear_operation (solver, coefs1, 2, GENEPI_GEQ, 0);
  int coefs2[2] = { 0, 1 };
  genepi_set *tmp2 = 
    genepi_set_linear_operation (solver, coefs2, 2, GENEPI_GEQ, 0);
  genepi_set *tmp3 = genepi_set_intersection (solver, a, tmp1);
  genepi_set *tmp4 = genepi_set_intersection (solver, tmp3, tmp2);
  int sel[2] = { 0, 1 };
  genepi_set *R = genepi_set_project (solver, tmp4, sel, 2);

  genepi_set_del_reference (solver, a);
  genepi_set_del_reference (solver, tmp1);
  genepi_set_del_reference (solver, tmp2);
  genepi_set_del_reference (solver, tmp3);
  genepi_set_del_reference (solver, tmp4);

  return R;
}

			/* --------------- */

/* xp-x = 2 */
static genepi_set *
s_x_p_equals_x_plus_2 (genepi_solver *solver)
{
  int coefs[2] = { 1, -1 };
  int coefs1[2] = { 1, 0 };
  int coefs2[2] = { 0, 1 };  
  genepi_set *s1 = genepi_set_linear_equality (solver, coefs, 2, 2);
  genepi_set *s2 = 
    genepi_set_linear_operation (solver, coefs1, 2, GENEPI_GEQ, 0);
  genepi_set *s3 = 
    genepi_set_linear_operation (solver, coefs2, 2, GENEPI_GEQ, 0);
  genepi_set *s1_and_s2 = genepi_set_intersection (solver, s1, s2);
  genepi_set *s1_and_s2_and_s3 = 
    genepi_set_intersection (solver, s1_and_s2, s3);
  genepi_set_del_reference (solver, s1);
  genepi_set_del_reference (solver, s2);
  genepi_set_del_reference (solver, s3);
  genepi_set_del_reference (solver, s1_and_s2);

  return s1_and_s2_and_s3;
}

			/* --------------- */


/* xp-x = -n */
static genepi_set *
s_x_p_equals_x_minus_n (genepi_solver *solver, int n)
{
  int coefs[2] = { 1, -1 };
  int coefs1[2] = { 1, 0 };
  int coefs2[2] = { 0, 1 };
  genepi_set *s1 = genepi_set_linear_equality (solver, coefs, 2, -n);
  genepi_set *s2 = 
    genepi_set_linear_operation (solver, coefs1, 2, GENEPI_GEQ, 0);
  genepi_set *s3 = 
    genepi_set_linear_operation (solver, coefs2, 2, GENEPI_GEQ, 0);
  genepi_set *s1_and_s2 = genepi_set_intersection (solver, s1, s2);
  genepi_set *s1_and_s2_and_s3 = 
    genepi_set_intersection (solver, s1_and_s2, s3);
  genepi_set_del_reference (solver, s1);
  genepi_set_del_reference (solver, s2);
  genepi_set_del_reference (solver, s3);
  genepi_set_del_reference (solver, s1_and_s2);

  return s1_and_s2_and_s3;
}

			/* --------------- */

static genepi_set *
s_x_equals_n (genepi_solver *solver, int n)
{
  int coefs[1] = { 1 };

  return genepi_set_linear_equality (solver, coefs, 1, n);
}

			/* --------------- */

static genepi_set *
s_another_apply (genepi_solver *solver, genepi_set *R, genepi_set *A)
{
  int i;
  int w = genepi_set_get_width (solver, A);
  int *indices = calloc (sizeof (int), 2*w);
  genepi_set *tmp;
  genepi_set *result;

  for (i = 0; i < w; i++) 
    {
      indices[2 * i] = w;
      indices[2 * i + 1] = i;
    }
  tmp = genepi_set_transform (solver, A, indices, 2*w, NULL, 0);
  result = genepi_set_intersection (solver, tmp, R);
  genepi_set_del_reference (solver, tmp);

  for (i = 0; i < w; i++) 
    indices[i] = 2*i;
  tmp = genepi_set_transform (solver, result, indices, w, NULL, 0);
  genepi_set_del_reference (solver, result);

  free (indices);

  return tmp;
}

			/* --------------- */

static int
s_check_apply_vs_gen_apply (genepi_solver *solver, genepi_set *R, 
			    genepi_set *A, genepi_set *Res)
{
  genepi_set *R0 = genepi_set_generic_apply (solver, R,A);
  genepi_set *R1 = genepi_set_apply (solver, R,A);
  genepi_set *R2 = s_another_apply (solver, R,A);
  int result = genepi_set_equal (solver, R0, Res) ;

  result = (result 
	    && genepi_set_equal (solver, R0,R1) 
	    && genepi_set_equal (solver, R0,R2)
	    );
  genepi_set_del_reference (solver, R1);
  genepi_set_del_reference (solver, R0);
  genepi_set_del_reference (solver, R2);

  return result;
}

			/* --------------- */

static genepi_set *
s_x_in_nat (genepi_solver *solver)
{
  int c = 1;
  return genepi_set_linear_operation (solver, &c, 1, GENEPI_GEQ, 0);
}

			/* --------------- */

static int
s_check_depend_on (genepi_solver *solver, genepi_set *X, int n)
{
  int i;
  int result = s_check_serialization (solver, X);
  genepi_set *XP;
  int *sel;

  if (!result)
    return 0;

  sel = (int *) calloc (sizeof (int), n);
  for (i = 0; i < n && result; i++)
    {
      sel[i] = 1;
      if (! genepi_set_depend_on (solver, X, sel, n))
	result = 0;
      sel[i] = 0;
    }
  free (sel);
  if( ! result )
    return 0;

  sel = (int *)calloc(sizeof (int), 2*n+1);
  for (i = 0; i < 2*n+1; i += 2)
    sel[i] = 1;
  XP = genepi_set_invproject (solver, X, sel, 2*n+1);

  if (genepi_set_depend_on (solver, XP, sel, 2*n+1))
    result = 0;
  else
    {
      for (i = 0; i < 2*n+1; i += 2)
	sel[i] = 0;
      
      for (i = 0; i < 2*n+1 && result; i++)
	{
	  sel[i] = 1;
	  if ((i % 2) == 0)
	    {
	      if (genepi_set_depend_on (solver, XP, sel, 2*n+1))
		result = 0;	  
	    }
	  else
	    {
	      if (! genepi_set_depend_on (solver, XP, sel, 2*n+1))
		result = 0;
	    }
	  sel[i] = 0;
	}
    }
  free (sel);
  genepi_set_del_reference (solver, XP);

  return result;
}

			/* --------------- */


static int
s_check_top_N (genepi_solver *solver)
{
  int i;
  int result = 1;

  for (i = 1; i < 65 && result; i++)
    {
      genepi_set *top = genepi_set_top (solver, i);
      genepi_set *top_N = genepi_set_top_N (solver, i);

      if (genepi_solver_get_solver_type (solver) == GENEPI_N_SOLVER) 
	result = genepi_set_compare (solver, top, GENEPI_EQ, top_N);
      else
	result = genepi_set_compare (solver, top, GENEPI_GT, top_N);
      genepi_set_del_reference (solver, top);
      genepi_set_del_reference (solver, top_N);
    }

  return result;
}

			/* --------------- */

static genepi_set *
s_crt_vector(genepi_solver *solver, int *x, int xsize)
{
  int i;
  genepi_set *aux1;
  genepi_set *aux2;
  genepi_set *result = genepi_set_top_N (solver, xsize);
  int *alpha = calloc (sizeof (int), xsize);

  for (i = 0; i < xsize; i++)
    {
      alpha[i] = 1;
      aux1 = genepi_set_linear_equality (solver, alpha, xsize, x[i]);
      aux2 = genepi_set_intersection (solver, aux1, result);
      genepi_set_del_reference (solver, result);
      genepi_set_del_reference (solver, aux1);
      result = aux2;
      alpha[i] = 0;
    }

  free (alpha);

  return result;
}

			/* --------------- */

static genepi_set *
s_sub(genepi_solver *solver, genepi_set *X1, genepi_set *X2)
{
  genepi_set *notX2 = genepi_set_complement (solver, X2);
  genepi_set *result = genepi_set_intersection (solver, X1, notX2);
  genepi_set_del_reference (solver, notX2);

  return result;
}

			/* --------------- */

static genepi_set *
s_generate_vectors  (genepi_solver *solver, int **V, int nb_vectors, int width)
{
  int i;
  genepi_set *aux1;
  genepi_set *aux2;
  genepi_set *result = genepi_set_bot (solver, width);

  for (i = 0; i < nb_vectors; i++)
    {
      int j;
      int *v = V[i] = calloc (sizeof (int), width);
      
      for (j = 0; j < width; j++)
	v[j] = (random () % 1033)+1;
      aux1 = s_crt_vector (solver, v, width);
      aux2 = genepi_set_union (solver, result, aux1);
      genepi_set_del_reference (solver, aux1);
      genepi_set_del_reference (solver, result);
      result = aux2;
    }

  return result;
}

			/* --------------- */

static int
s_check_is_solution (genepi_solver *solver)
{
  int w;
  int result = 1;
  const int min_width = 1;
  const int max_width = 33;
  const int nb_vectors = 5;
  int **vectors = calloc (sizeof (int *), nb_vectors);

  for (w = min_width; result && w < max_width; w++)
    {
      int i;
      genepi_set *V = s_generate_vectors (solver, vectors, nb_vectors, w);

      result = result && s_check_serialization (solver, V);
      for (i = 0; i < nb_vectors; i++)
	{
	  result = (result && 
		    genepi_set_is_solution (solver, V,vectors[i], w, 1));
	  free (vectors[i]);
	}
      genepi_set_del_reference (solver, V);
    }
  free (vectors);

  return result;
}

			/* --------------- */

static int
s_get_vector_index (int **vectors, int nb_vectors, int *x, int xsize)
{
  int i;
  int result = -1;

  for (i = 0; i < nb_vectors; i++)
    {
      int j;

      if (vectors[i] == NULL)
	continue;

      for (j = 0; j < xsize; j++)
	{
	  if (vectors[i][j] != x[j])
	    break;
	}

      if (j == xsize)
	{
	  result = i;
	  break;
	}
    }

  return result;
}

			/* --------------- */

static int
s_check_get_one_solution (genepi_solver *solver)
{
  int w;
  int result = 1;
  const int min_width = 1;
  const int max_width = 33;
  const int nb_vectors = 5;
  int **vectors = calloc (sizeof (int *), nb_vectors + 1);

  for (w = min_width; result && w < max_width; w++)
    {      
      int i;
      genepi_set *V = s_generate_vectors (solver, vectors, nb_vectors, w);

      result = result && s_check_serialization (solver, V);
      vectors [nb_vectors] = calloc (sizeof (int), w);
      while (result && !genepi_set_is_empty (solver, V))
	{
	  int den = 
	    genepi_set_get_one_solution (solver, V, vectors[nb_vectors], w);

	  result = (den == 1);
	  if (result)
	    {
	      int k = s_get_vector_index (vectors, nb_vectors, 
					  vectors[nb_vectors], w);
	      
	      result = (k >= 0);
	      if (result)
		{
		  genepi_set *aux1 = s_crt_vector (solver, vectors[k], w);
		  genepi_set *aux2 = s_sub (solver, V, aux1);
		  genepi_set_del_reference (solver, aux1);
		  genepi_set_del_reference (solver, V);

		  V = aux2;
		  
		  while (k >= 0)
		    {
		      free (vectors[k]);
		      vectors[k] = NULL;

		      k = s_get_vector_index (vectors, nb_vectors, 
					      vectors[nb_vectors], w);
		    }
		}
	    }
	}
      
      for (i = 0; i < nb_vectors+1; i++)
	{
	  if (vectors[i] != NULL)
	    free (vectors[i]);
	}
      genepi_set_del_reference (solver, V);
    }
  free (vectors);

  return result;
}

			/* --------------- */

static int
s_check_serialization (genepi_solver *solver, genepi_set *X)
{
  FILE *f;
  int nb_readed, nb_written, result;
  genepi_set *XR = NULL;

  if (solver->plugin->write_set == NULL && solver->plugin->read_set == NULL)
    return 1;

  result = 1;

  if ((f = fopen (TEST_FILENAME,"w")) == NULL)
    {
      fprintf (stderr, "can't create file '%s'\n",TEST_FILENAME);
      result = 0;
    }
  else
    {
      nb_written = genepi_set_write (solver, f, X);            
      fflush (f);
      fclose (f);
      if (nb_written < 0)
	result = 0;
      if(0) printf("write %d bytes\nresult=%d\n", nb_written, result);
    }

  if (result)
    {
      if ((f = fopen (TEST_FILENAME,"r")) == NULL)
	{
	  fprintf (stderr, "can't read file '%s'\n",TEST_FILENAME);
	  result = 0;
	}
      else
	{
	  nb_readed = genepi_set_read (solver, f, &XR);
	  if(0) printf("read %d bytes\n", nb_readed);
	  if (nb_readed < 0) result = 0;
	  else 
	    {
	      assert (XR != NULL);
	      if (nb_readed != nb_written || 
		  ! genepi_set_compare (solver, X, GENEPI_EQ, XR))
		result = 0;
	    }
	  fclose (f);
	}
    }

  if (XR != NULL)
    genepi_set_del_reference (solver, XR);

  if (!result)
    fprintf (stderr, "serialization test error.\n");

  return result;
}

			/* --------------- */

static int
s_check_mul_vector (genepi_solver *solver)
{
  int i;
  int result = 1;
  const int COEFS[] = { 1, 2, 3, 11, 49 };
  genepi_set *nat;
  char *vars[] = { "x", "y", "z" };

  nat = genepi_set_top_N (solver, 1);

  for (i = 0; result && i < sizeof(COEFS)/sizeof(COEFS[0]); i++)
    {
      genepi_set *tmp[4];
      int coeffs[2];

      coeffs[0] = COEFS[i];
      coeffs[1] = COEFS[i];
      tmp[0] = genepi_set_mul_vector (solver, nat, 2, coeffs, 1);
      
      coeffs[0] = 1;
      coeffs[1] = -1;
      tmp[1] = genepi_set_linear_equality (solver, coeffs, 2, 0);
      tmp[2] = genepi_set_scale (solver, COEFS[i], nat);
      tmp[3] = genepi_set_cartesian_product (solver, tmp[2], tmp[2]);
      genepi_set_del_reference (solver, tmp[2]);
      tmp[2] = genepi_set_intersection (solver, tmp[1], tmp[3]);
      genepi_set_del_reference (solver, tmp[1]);
      genepi_set_del_reference (solver, tmp[3]);

      result = genepi_set_equal (solver, tmp[0], tmp[2]);
      genepi_set_del_reference (solver, tmp[0]);
      genepi_set_del_reference (solver, tmp[2]);
    }
  genepi_set_del_reference (solver, nat);

  return result;
}
