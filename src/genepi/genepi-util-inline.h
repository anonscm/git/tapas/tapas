/*
 * genepi-util-inline.h -- Generic functions implemented with GENEPI routines
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __GENEPI_UTIL_INLINE_H__
# define __GENEPI_UTIL_INLINE_H__

BEGIN_C_DECLS

			/* --------------- */

static inline genepi_set *
genepi_set_aX_plus_bY (genepi_solver *solver, int a, genepi_set *X, int b, 
		       genepi_set *Y)
{
  int coeff[2];
  genepi_set *args[2];
  coeff[0] = a;
  coeff[1] = b;
  args[0] = X;
  args[1] = Y;

  return genepi_set_linear_transform (solver, 2, coeff, 0, args);
}

			/* --------------- */

static inline genepi_set *
genepi_set_add (genepi_solver *solver, genepi_set *X, genepi_set *Y)
{
  return genepi_set_aX_plus_bY (solver, 1, X, 1, Y);
}

			/* --------------- */

static inline genepi_set *
genepi_set_sub (genepi_solver *solver, genepi_set *X, genepi_set *Y)
{
  return genepi_set_aX_plus_bY (solver, 1, X, -1, Y);
}

			/* --------------- */

static inline genepi_set *
genepi_set_scale (genepi_solver *solver, int s, genepi_set *X)
{
  return genepi_set_linear_transform (solver, 1, &s, 0, &X);
}

			/* --------------- */

static inline genepi_set *
genepi_set_neg (genepi_solver *solver, genepi_set *X)
{
  return genepi_set_scale (solver, -1, X);
}

END_C_DECLS

#endif /* ! __GENEPI_UTIL_INLINE_H__ */
