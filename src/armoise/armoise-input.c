/*
 * armoise-input.c -- 
 * Copyright (C) 2007 J. Leroux, G. Point, LaBRI, CNRS UMR 5800, 
 * Universite Bordeaux I
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */
/*
 * $Author: point $
 * $Revision: 1.2 $
 * $Date: 2007/07/12 07:41:58 $
 * $Id: armoise-input.c,v 1.2 2007/07/12 07:41:58 point Exp $
 */

/*!
 * \file
 * \brief
 * 
 */
#include "armoise-input-p.h"
#include "armoise-input.h"

armoise_tree *
armoise_read_file (const char *filename)
{
  armoise_tree *result = NULL;
  FILE *input_stream = fopen (filename, "r");

  if (input_stream != NULL)
    {
      result = armoise_read_stream (input_stream, filename);
      fclose (input_stream);
    }

  return result;
}

			/* --------------- */

armoise_tree *
armoise_read_stream (FILE *stream, const char *input_name)
{
  armoise_tree *result;

  armoise_number_of_errors = 0;
  result = armoise_syntax_tree = armoise_syntax_tree_container = NULL;
  armoise_init_lexer (stream, input_name);
  if (armoise_parse () == 0)
    result = armoise_syntax_tree;
  else
    ccl_parse_tree_delete_container (armoise_syntax_tree_container);
  
  armoise_terminate_lexer ();

  return result;
}

			/* --------------- */

extern armoise_tree *
armoise_read_string (const char *input)
{
  armoise_tree *result;

  armoise_number_of_errors = 0;
  result = armoise_syntax_tree = armoise_syntax_tree_container = NULL;
  armoise_init_lexer (NULL, input);
  if (armoise_parse () == 0)
    result = armoise_syntax_tree;
  else
    ccl_parse_tree_delete_container (armoise_syntax_tree_container);
  
  armoise_terminate_lexer ();

  return result;
}

			/* --------------- */

extern char *armoise_text;

void
armoise_error (const char *message)
{
  armoise_number_of_errors++; 

  ccl_error("%s:%d: %s (tok='%s')\n",
	    armoise_current_filename,
	    armoise_current_line,
	    message,
	    armoise_text);
}

			/* --------------- */

int
armoise_wrap (void)
{
  return 1;
}

			/* --------------- */

