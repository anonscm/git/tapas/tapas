/*
 * armoise.h -- add a comment about this file
 * 
 * This file is a part of the ARMOISE language library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file armoise/armoise.h
 * \page armoise API for the manipulation of ARMOISE predicates 
 */
#ifndef __ARMOISE_H__
# define __ARMOISE_H__

# include <limits.h>
# include <ccl/ccl-string.h>
# include <ccl/ccl-tree.h>
# include <ccl/ccl-hash.h>
# include <ccl/ccl-list.h>
# include <ccl/ccl-log.h>

BEGIN_C_DECLS

/*!
 * \brief Abstract type representing the context of a predicate.
 *
 * An \ref armoise_context of a predicate stores all definition
 * usable by a predicate. It is mainly the "in" part of a predicate.
 * It inherits definitions of parent contexts. A context can be used to store
 * either predicate symbols or first-order variables. Predicates are stored
 * has \ref ccl_tree and first-order variables as \ref armoise_variable.
 *
 * \see 
 *  armoise_context_create_for_predicates,
 *  armoise_context_create_for_variables
 */
typedef struct armoise_context_st armoise_context;

/*!
 * \brief Abstract of an ARMOISE predicate.
 */
typedef struct armoise_predicate_st armoise_predicate;

/*!
 * \brief Kind of predicates
 *
 * \see armoise_predicate_get_kind
 */
typedef enum {
  ARMOISE_P_EMPTY,
  ARMOISE_P_NAT,
  ARMOISE_P_INT,
  ARMOISE_P_POSI,   
  ARMOISE_P_REAL,
  ARMOISE_P_UNION,   
  ARMOISE_P_INTERSECTION,
  ARMOISE_P_DIFF,   
  ARMOISE_P_DELTA,
  ARMOISE_P_COMPLEMENT,
  ARMOISE_P_ADD,
  ARMOISE_P_SUB,
  ARMOISE_P_MUL,
  ARMOISE_P_DIV,
  ARMOISE_P_MOD,
  ARMOISE_P_NEG,
  ARMOISE_P_PRODUCT,
  ARMOISE_P_SET,
  ARMOISE_P_ENUM,
  ARMOISE_P_INDIRECTION,
  ARMOISE_P_PAREN
} armoise_predicate_kind;

/*!
 * \brief Abstract type for a first-order formula
 */
typedef struct armoise_formula_st armoise_formula;

/*!
 * \brief Kind of formula
 * \see armoise_formula_get_kind
 */
typedef enum {
  ARMOISE_F_CONSTANT,
  ARMOISE_F_IN,
  ARMOISE_F_NOT,
  ARMOISE_F_EQ,   
  ARMOISE_F_NEQ,
  ARMOISE_F_LEQ,   
  ARMOISE_F_GEQ,
  ARMOISE_F_LT,   
  ARMOISE_F_GT,
  ARMOISE_F_AND,
  ARMOISE_F_OR,
  ARMOISE_F_EQUIV,
  ARMOISE_F_IMPLY,
  ARMOISE_F_EXISTS,
  ARMOISE_F_FORALL,
  ARMOISE_F_PAREN
} armoise_formula_kind;

/*!
 * \brief Abstract type for a first-order variable
 */
typedef struct armoise_variable_st armoise_variable;

/*!
 * \brief Abstract type for a (first-order) term
 */
typedef struct armoise_term_st armoise_term;

/*!
 * \brief Kind of term
 * \see armoise_term_get_kind
 */
typedef enum {
  ARMOISE_T_VARIABLE,
  ARMOISE_T_CONSTANT,
  ARMOISE_T_VECTOR,
  ARMOISE_T_ELEMENT,
  ARMOISE_T_ADD,
  ARMOISE_T_SUB,
  ARMOISE_T_MUL,
  ARMOISE_T_DIV,
  ARMOISE_T_MOD,
  ARMOISE_T_NEG
} armoise_term_kind;

/*!
 * \brief Abstract type for variable or predicate types
 * 
 * A type can be unknown, scalar or a vector of types.
 * 0 for the scalar type and the number of sub-types for the others.
 */
typedef struct armoise_type_st armoise_type;

/*!
 * \brief Abstract type for domains of variables, terms and predicates
 */
typedef struct armoise_domain_st armoise_domain;

/*!
 * \brief Attributes of an armoise_domain
 */
typedef uint32_t armoise_domain_attr;

/*!
 * \brief Attributes of an armoise_domain
 */
# define ARMOISE_DOMAIN_HAS_POSITIVES (((uint32_t)0x1) << 0)
# define ARMOISE_DOMAIN_HAS_NEGATIVES (((uint32_t)0x1) << 1)
# define ARMOISE_DOMAIN_IS_FINITE    (((uint32_t)0x1) << 2)
# define ARMOISE_DOMAIN_IN_INTEGERS  (((uint32_t)0x1) << 3)

/*!
 * \brief ARMOISE library initializer.
 * 
 * This function must be call prior to any other ARMOISE function call.
 * 
 * \return a non-null value if the initialization process is successful.
 */
extern int
armoise_init (void);

/*!
 * \brief ARMOISE library finalizer.
 *
 * This function should be call when the ARMOISE library is used any more 
 * (typically before the program exists) to release allocated ressources.
 */
extern void
armoise_terminate (void);

/*!
 * \brief Constructor for contexts storing predicate symbols.
 * 
 * The context does not store predicates as \ref armoise_predicate but as
 * \ref ccl_tree trees. 
 *
 * \param parent the parent context from which the result inherits 
 *        definitions. It can be NULL.
 * 
 * \return a newly created context
 * \see armoise_context_add_predicate
 */
extern armoise_context *
armoise_context_create_for_predicates (armoise_context *parent);

/*!
 * \brief Constructor for contexts storing first order symbols.
 *
 * \param parent the parent context from which the result inherits 
 *        definitions. It can be NULL.
 * 
 * \return a newly created context
 * \see armoise_context_add_variable
 */
extern armoise_context *
armoise_context_create_for_variables (armoise_context *parent);

/*!
 * \brief Adds a new reference to the context \a ctx
 * 
 * This function increments the counter of references to \a ctx.
 *
 * \param ctx the context newly referenced
 * \pre ctx != NULL
 *
 * \return the pointer to \a ctx
 */
extern armoise_context *
armoise_context_add_reference (armoise_context *ctx);

/*!
 * \brief Suppresses a reference to the context \a ctx
 * 
 * This function decrements the counter of references to \a ctx. If this counter
 * falls to 0 then the ressources allocated to \a ctx are released.
 *
 * \param ctx the unreferenced context 
 * \pre ctx != NULL
 * \pre the number of references to \a ctx > 0
 */
extern void
armoise_context_del_reference (armoise_context *ctx);

/*! 
 * \brief Returns the parent context of \a ctx.
 * \param ctx the context 
 * \pre ctx != NULL
 * \return a new reference to the parent context of \a ctx or NULL if \a ctx
 * has no parent.
 */
extern armoise_context *
armoise_context_get_parent (armoise_context *ctx);

/*!
 * \brief Checks if the symbol \a id exists in the context \a ctx.
 * \param ctx the considered context 
 * \param id the looked for symbol
 * \pre ctx != NULL
 * \return "a non-null value" if \a id belongs to \a ctx or one of its parent
 * contexts.
 */
extern int
armoise_context_has (armoise_context *ctx, ccl_ustring id);

/*!
 * \brief Checks if the symbol \a id is defined in the context \a ctx.
 *
 * The existence test is not propagate to parent contexts.
 *
 * \param ctx the considered context 
 * \param id the looked for symbol.
 * \pre ctx != NULL
 * \return "a non-null value" if \a id is defined by \a ctx.
 */
extern int
armoise_context_has_locally (armoise_context *ctx, ccl_ustring id);

/*!
 * \brief Returns the object associated with the symbol \a id.
 *
 * The function looks for \a id in the symbol table of \a ctx and those of 
 * its parents. If \a id is found then the object associated with it is 
 * returned.
 *
 * \param ctx the considered context 
 * \param id the looked for symbol.
 * \pre ctx != NULL
 * \return a pointer to the object defining the symbol \a id or NULL if \a id 
 *         is not an existing symbol.
 */
extern void *
armoise_context_get (armoise_context *ctx, ccl_ustring id);

/*!
 * \brief Defines symbol \a id in the context \a ctx.
 * 
 * This function assaciates in the symbol table of \a ctx the symbol \a id and
 * the object (predicate or variable) \a object. The symbol must not exists
 * in the table (i.e. it can not be redefined locally) however it can exist 
 * a parent context of \a ctx (i.e. the definition locally replaces the one of 
 * the parent).
 *
 * \param ctx the considered context
 * \param id the defined symbol
 * \param object the object associated with \a id.
 * \pre ! armoise_context_has_locally (ctx, id)
 * \pre object != NULL
 */
extern void
armoise_context_add (armoise_context *ctx, ccl_ustring id, void *object);

/*!
 * \brief Returns the symbol table of \a ctx.
 * 
 * The returned \ref ccl_hash table associates \ref ccl_ustring identifiers
 * and \ref armoise_predicate or \ref armoise_variable objects. This table
 * must not be modified.
 * 
 * \param ctx the considered context
 * \pre ctx != NULL
 * \return the internal symbol table of \a ctx
 */
extern ccl_hash *
armoise_context_get_table (armoise_context *ctx);

/*!
 * \brief Returns the list of symbol locally defined in \a ctx.
 * 
 * The returned \ref ccl_list contains the identifiers locally defined in \a ctx
 * in their definition order. This list must not be modified.
 * 
 * \param ctx the considered context
 * \pre ctx != NULL
 * \return the internal list of symbols of \a ctx
 */
extern ccl_list *
armoise_context_get_ids (armoise_context *ctx);

/*!
 * \brief Checks the existences of a predicate tree \a id in \a ctx.
 * \see armoise_context_has
 */
extern int
armoise_context_has_predicate_tree (armoise_context *ctx, ccl_ustring id);

/*!  
 * \brief Retrieves the predicate tree identified by \a id in \a ctx.
 * \return a new reference to the \ref ccl_tree or NULL
 * \see armoise_context_get 
 */
extern ccl_tree *
armoise_context_get_predicate_tree (armoise_context *ctx, ccl_ustring id);

/*!  
 * \brief Associates in \a ctx the identifier \a id and the tree of predicates
 * \a tree.
 * A new reference is added to \a tree.
 * \see armoise_context_add
 */
extern void
armoise_context_add_predicate_tree (armoise_context *ctx, ccl_ustring id, 
				    ccl_tree *tree);

/*!  
 * \brief Retrieve the predicate corresponding to a leaf of tree of predicates
 * defined in \a ctx by the symbol \a id.
 *
 * This function assumes that a tree corresponding to a parallel definition 
 * of predicates exists in \a ctx. Then the function searches through the tree
 * according to \a indices which indicates the index of child nodes to follow.
 *
 * \pre armoise_context_get_predicate_tree (ctx, id) != NULL
 * \return a new reference to the predicate associated with \a id 
 * [\a indices[0]] ... [\a indices[\a nb_indices-1]]
 */
extern armoise_predicate *
armoise_context_get_predicate (armoise_context *ctx, ccl_ustring id, 
			       int nb_indices, const int *indices);

/*!  
 * \brief Behaves like \ref armoise_context_get_predicate but the result
 * can not be changed by the client code.
 */
extern const armoise_predicate *
armoise_context_get_cst_predicate (armoise_context *ctx, ccl_ustring id, 
				   int nb_indices, const int *indices);

/*!
 * \brief Checks the existences of a first-order variable identified by \a id 
 * in \a ctx.
 * \see armoise_context_has
 */
extern int
armoise_context_has_variable (armoise_context *ctx, ccl_ustring id);

/*!  
 * \brief Associates in \a ctx the identifier \ref armoise_variable_get_name(\a
 * var) and the first-order variable \a var.
 * A new reference is added to \a var.
 * \see armoise_context_add
 */
extern void
armoise_context_add_variable (armoise_context *ctx, armoise_variable *var);

/*!  
 * \brief Retrieves the variable identified by \a id in \a ctx.
 * \return a new reference to the \ref armoise_variable or NULL
 * \see armoise_context_get 
 */
extern armoise_variable *
armoise_context_get_variable (armoise_context *ctx, ccl_ustring id);

/*!
 * \brief Constructor for a first-order variable defined by its \a name and
 * its \a type.
 *
 * \param name the identifier of the variable
 * \param type the type of the variable 
 * \return a newly create variable
 */
extern armoise_variable *
armoise_variable_create (ccl_ustring name, armoise_type *type);

/*
 * \brief Adds a new reference to \a variable.
 * 
 * This function increments the counter of references to \a variable.
 *
 * \param variable the variable newly referenced
 * \pre variable != NULL
 *
 * \return the pointer to \a variable
 */
extern armoise_variable *
armoise_variable_add_reference (armoise_variable *variable);

/*!
 * \brief Suppresses a reference to \a variable
 * 
 * This function decrements the counter of references to \a variable. If this 
 * counter falls to 0 then the ressources allocated to \a variable are released.
 *
 * \param variable the unreferenced variable 
 * \pre variable != NULL
 * \pre the number of references to \a variable > 0
 */
extern void
armoise_variable_del_reference (armoise_variable *variable);

/*!
 * \brief Returns the identifier of \a variable 
 * \param variable the considered variable
 * \pre variable != NULL 
 * \return the identifier used to create \a variable
 */
extern ccl_ustring 
armoise_variable_get_name (armoise_variable *variable);

/*!
 * \brief Returns the type of \a variable 
 * \param variable the considered variable
 * \pre variable != NULL 
 * \return a new reference to the type of \a variable 
 */
extern armoise_type *
armoise_variable_get_type (armoise_variable *variable);

/*!
 * \brief Returns the domain of \a variable
 * 
 * \param variable the considered variable
 * \pre variable != NULL
 * \return the domain of \a variable
 */
extern const armoise_domain *
armoise_variable_get_domain (armoise_variable *variable);

/*!
 * \brief Change the domain \a D of \a variable
 * 
 * The current domain assigned to \a variable is unreferenced. \a D is then
 * duplicated and assigned to \a variable.
 *
 * \param variable the considered variable
 * \param D the domain assigned to \a variable
 * \pre variable != NULL
 * \pre D != NULL;
 */
extern void
armoise_variable_set_domain (armoise_variable *variable, 
			     const armoise_domain *D);

/*!
 * \brief Display the \a variable on the \a log stream.
 * \param log the stream where the variable is displayed
 * \param variable the variable send on \a log
 * \pre variable != NULL
 */
extern void
armoise_variable_log (ccl_log_type log, armoise_variable *variable);

/*!
 * \brief Constructor for an empty and untyped relation. 
 *
 * The type of the predicate is set to \e unknown (cf. armoise_type_is_unknown).
 *
 * \param ctx the context 
 * \pre ctx != NULL
 * \return a predicate of kind ARMOISE_P_EMPTY
 */
extern armoise_predicate *
armoise_predicate_create_empty (armoise_context *ctx, armoise_type *type);

/*!
 * \brief Constructor for the predicate representing natural numbers 
 * (\f$\cal N\f$).
 * The type of the predicate is set to \e scalar (cf. armoise_type_is_scalar).
 * \param ctx the context 
 * \pre ctx != NULL
 * \return a predicate of kind ARMOISE_P_NAT
 */
extern armoise_predicate *
armoise_predicate_create_naturals (armoise_context *ctx);

/*!
 * \brief Constructor for the predicate representing integers (\f$\cal Z\f$).
 * The type of the predicate is set to \e scalar (cf. armoise_type_is_scalar).
 * \param ctx the context 
 * \pre ctx != NULL
 * \return a predicate of kind ARMOISE_P_INT
 */
extern armoise_predicate *
armoise_predicate_create_integers (armoise_context *ctx);

/*!
 * \brief Constructor for the real numbers restricted to positive 
 * (\f${\cal R}^+\f$).
 * The type of the predicate is set to \e scalar (cf. armoise_type_is_scalar).
 * \param ctx the context 
 * \pre ctx != NULL
 * \return a predicate of kind ARMOISE_P_POSI
 */
extern armoise_predicate *
armoise_predicate_create_positives (armoise_context *ctx);

/*!
 * \brief Constructor for the real numbers (\f$\cal R\f$).
 * The type of the predicate is set to \e scalar (cf. armoise_type_is_scalar).
 * \param ctx the context 
 * \pre ctx != NULL
 * \return a predicate of kind ARMOISE_P_REAL
 */
extern armoise_predicate *
armoise_predicate_create_reals (armoise_context *ctx);

/*!
 * \brief Constructor for a predicate that is a reference to another predicate.
 * 
 * The predicate returned by this function refers to \a name [\a indices[0]] ...
 * [\a indices[nb_indices-1]] where \a name is the name of a parallel definition
 * of predicates. Of course, \a nb_indices can be zero if \a name refers to
 * simple predicate definition.
 *
 * The newly created predicate inherits the type of the predicate it makes a 
 * reference. The array \a indices is duplicated.
 *
 * \param ctx the context of the newly created predicate
 * \param name the identifier of a predicate or \ref ccl_tree defined in \a ctx.
 * \param nb_indices depth of search the \ref ccl_tree referred by \a name
 * \param indices array of indices indicating the path to follow in the 
 *        \a ccl_tree pointed by \a name.
 * \pre name != NULL
 * \pre index >= 0
 * \return a predicate of kind ARMOISE_P_INDIRECTION equal to 
 * \a name [\a indices[0]] ... [\a indices[nb_indices-1]] 
 */
extern armoise_predicate *
armoise_predicate_create_indirection (armoise_context *ctx, ccl_ustring name, 
				      int nb_indices, const int *indices);

/*!
 * \brief Creates a predicate representing the union of relations \a P1 and
 * \a P2
 *
 * The result inherits its type from \a P1 or \a P2 (or is set to \e unknown)
 * if none is typed.
 *
 * \param ctx the context of the newly created predicate
 * \param P1 first operand
 * \param P2 second operand
 * \pre P1 != NULL, P2 != NULL
 * \return a predicate of kind ARMOISE_P_UNION encoding \f$P1\cup P2\f$
 */
extern armoise_predicate *
armoise_predicate_create_union (armoise_context *ctx, armoise_predicate *P1, 
				armoise_predicate *P2);

/*!
 * \brief Creates a predicate representing the intersection of relations \a P1 
 * and \a P2
 *
 * The result inherits its type from \a P1 or \a P2 (or is set to \e unknown
 * if none is typed).
 *
 * \param ctx the context of the newly created predicate
 * \param P1 first operand
 * \param P2 second operand
 * \pre P1 != NULL, P2 != NULL
 * \return a predicate of kind ARMOISE_P_INTERSECTION encoding \f$P1\cap P2\f$
 */
extern armoise_predicate *
armoise_predicate_create_intersection (armoise_context *ctx, 
				       armoise_predicate *P1, 
				       armoise_predicate *P2);

/*!
 * \brief Creates a predicate representing the symetric difference of \a P1 
 * and \a P2
 *
 * The result inherits its type from \a P1 or \a P2 (or is set to \e unknown
 * if none is typed).
 *
 * \param ctx the context of the newly created predicate
 * \param P1 first operand
 * \param P2 second operand
 * \pre P1 != NULL, P2 != NULL
 * \return a predicate of kind ARMOISE_P_DELTA encoding 
 * \f$P1\Delta P2 = P1\setminus P2 \cup P2\setminus P1\f$
 */
extern armoise_predicate *
armoise_predicate_create_delta (armoise_context *ctx, armoise_predicate *P1, 
				armoise_predicate *P2);

/*!
 * \brief Creates a predicate representing the difference of \a P1 and \a P2
 *
 * The result inherits its type from \a P1 or \a P2 (or is set to \e unknown
 * if none is typed).
 *
 * \param ctx the context of the newly created predicate
 * \param P1 first operand
 * \param P2 second operand
 * \pre P1 != NULL, P2 != NULL
 * \return a predicate of kind ARMOISE_P_DIFF encoding \f$P1\setminus P2\f$
 */
extern armoise_predicate *
armoise_predicate_create_diff (armoise_context *ctx, armoise_predicate *P1, 
			       armoise_predicate *P2);

/*!
 * \brief Creates a predicate representing the complement of \a P.
 *
 * P is complemented relatively to \f$\cal R\f$.
 * The result inherits its type from \a P.
 *
 * \param ctx the context of the newly created predicate
 * \param P the complemented predicate
 * \pre P != NULL
 * \return the predicate of kind ARMOISE_P_COMPLEMENT encoding
 * \f$\overline{P}\f$
 */
extern armoise_predicate *
armoise_predicate_create_complement (armoise_context *ctx, 
				     armoise_predicate *P);

/*!
 * \brief Creates a predicate representing the arithmetical addition of \a
 * P1 and \a P2.
 *
 * The result inherits its type from \a P1 or \a P2 (or is set to \e unknown
 * if none is typed).
 *
 * \param ctx the context of the newly created predicate
 * \param P1 first operand
 * \param P2 second operand
 * \pre P1 != NULL, P2 != NULL
 * \return the predicate of kind ARMOISE_P_ADD encoding 
 * \f$P1+P2 = \{ p \mid \exists p_1\in P1, \exists p_2\in P2, p = p_1+p_2 \} \f$
 */
extern armoise_predicate *
armoise_predicate_create_add (armoise_context *ctx, armoise_predicate *P1, 
			      armoise_predicate *P2);

/*!
 * \brief Creates a predicate representing the arithmetical difference of \a
 * P1 and \a P2.
 *
 * The result inherits its type from \a P1 or \a P2 (or is set to \e unknown
 * if none is typed).
 *
 * \param ctx the context of the newly created predicate
 * \param P1 first operand
 * \param P2 second operand
 * \pre P1 != NULL, P2 != NULL
 * \return the predicate of kind ARMOISE_P_SUB encoding 
 * \f$P1-P2 = \{ p \mid \exists p_1\in P1, \exists p_2\in P2, p = p_1-p_2 \} \f$
 */
extern armoise_predicate *
armoise_predicate_create_sub (armoise_context *ctx, armoise_predicate *P1, 
			      armoise_predicate *P2);

/*!
 * \brief Creates a predicate representing the multiplicaton of \a
 * P1 and \a P2.
 *
 * The result inherits its type from \a P1 or \a P2 (or is set to \e unknown
 * if none is typed).
 *
 * \param ctx the context of the newly created predicate
 * \param P1 first operand
 * \param P2 second operand
 * \pre P1 != NULL, P2 != NULL
 * \return the predicate of kind ARMOISE_P_MUL encoding
 * \f$P1 \times P2 = \{ p \mid \exists p_1\in P1, \exists p_2\in P2, p = p_1
 * \times p_2 \} \f$
 */
extern armoise_predicate *
armoise_predicate_create_mul (armoise_context *ctx, armoise_predicate *P1, 
			      armoise_predicate *P2);

/*!
 * \brief Creates a predicate representing the division of \a P1 by \a P2.
 *
 * The result inherits its type from \a P1 or \a P2 (or is set to \e unknown
 * if none is typed).
 *
 * \param ctx the context of the newly created predicate
 * \param P1 first operand
 * \param P2 second operand
 * \pre P1 != NULL, P2 != NULL
 * \return the predicate of kind ARMOISE_P_DIV encoding \f$P1 \times P2 = \{ p
 * \mid \exists p_1\in P1, \exists p_2\in P2\setminus \{ 0 \}, p = p_1 / p_2 \}
 * \f$
 */
extern armoise_predicate *
armoise_predicate_create_div (armoise_context *ctx, armoise_predicate *P1, 
			      armoise_predicate *P2);

/*!
 * \brief Creates a predicate representing the modulo of \a P1 by \a P2.
 *
 * The result inherits its type from \a P1 or \a P2 (or is set to \e unknown
 * if none is typed).
 *
 * \param ctx the context of the newly created predicate
 * \param P1 first operand
 * \param P2 second operand
 * \pre P1 != NULL, P2 != NULL
 * \return the predicate of kind ARMOISE_P_MOD encoding \f$P1 \times P2 = \{ p
 * \mid \exists p_1\in P1, \exists p_2\in P2\setminus \{ 0 \}, p = p_1 \% p_2 
 * \} \f$
 */
extern armoise_predicate *
armoise_predicate_create_mod (armoise_context *ctx, armoise_predicate *P1, 
			      armoise_predicate *P2);

/*!
 * \brief Creates a predicate representing the negation of \a P
 *
 * The result inherits its type from \a P.
 *
 * \param ctx the context of the newly created predicate
 * \param P the operand
 * \pre P != NULL
 * \return the predicate of kind ARMOISE_P_NEG encoding \f$ -P = \{ p \mid
 * \exists p^\prime\in P, p = -p^\prime \} \f$
 */
extern armoise_predicate *
armoise_predicate_create_neg (armoise_context *ctx, armoise_predicate *P);

/*!
 * \brief Creates a predicate representing the cartesian product 
 * \a comps[0] \f$\times\dots\times\f$ \a comps[nb_comp-1].
 *
 * The type of the result is the product of types of \a comps[i]'s.
 *
 * \param ctx the context of the newly created predicate
 * \param nb_comp the number of components of the product
 * \param comps the components
 * \return the predicate of kind ARMOISE_P_PRODUCT encoding 
 * \a comps[0] \f$\times\dots\times\f$ \a comps[nb_comp-1].
 */
extern armoise_predicate *
armoise_predicate_create_product (armoise_context *ctx, int nb_comp, 
				  armoise_predicate **comps);

/*!
 * \brief Creates the singleton predicate \f$\{ i \}\f$
 *
 * The type of the result is set to \e scalar.
 *
 * \param ctx the context of the newly created predicate
 * \param i the single element in the resulting predicate.
 * \return the predicate of kind ARMOISE_P_ENUM encoding \f$\{ i \}\f$
 */
extern armoise_predicate *
armoise_predicate_create_integer (armoise_context *ctx, int i);

/*!
 * \brief Creates a predicate enclosed by parenthesis \f$(P)\f$
 *
 * The result inherits its type from \a P.
 *
 * \param ctx the context of the newly created predicate
 * \param P the predicate
 * \return the predicate of kind ARMOISE_P_PAREN encoding \f$(P)\f$
 */
extern armoise_predicate *
armoise_predicate_create_paren (armoise_context *ctx, armoise_predicate *P);

/*!
 * \brief Creates a predicate encoding a finite set of \a values
 *
 * The result inherits its type from the first typed element in \a values.
 *
 * \param ctx the context of the newly created predicate
 * \param nb_terms the number of values in the set
 * \param values the constant terms that define the values
 * \return the predicate of kind ARMOISE_P_ENUM encoding 
 * \f$\{ values[0], \dots, values[\mbox{nb\_terms}-1] \}\f$
 */
extern armoise_predicate *
armoise_predicate_create_finite_set (armoise_context *ctx, int nb_terms, 
				     armoise_term **values);

/*!
 * \brief Creates a predicate encoding a set defined by a first-order formula.
 *
 * The result inherits its type from the prototyping term \a proto. \a formula
 * is a first-order formula over variables appearing in \a proto.
 *
 * \param ctx the context of the newly created predicate
 * \param proto a term encoding the prototype of the encoded relation
 * \param P a predicate that is the domain of proto
 * \param def the FO formula defining the elements of the relation
 * \return the predicate of kind ARMOISE_P_SET encoding \f$\{ proto | def \}\f$
 */
extern armoise_predicate *
armoise_predicate_create_simple_set (armoise_context *ctx, armoise_term *proto,
				     armoise_predicate *P, 
				     armoise_formula *def); 

/*!
 * \brief Adds a new reference to the predicate \a P
 * 
 * This function increments the counter of references to \a P.
 *
 * \param P the predicate newly referenced
 * \pre P != NULL
 *
 * \return the pointer to \a P
 */
extern armoise_predicate *
armoise_predicate_add_reference (armoise_predicate *P);

/*!
 * \brief Suppresses a reference to the predicate \a P
 * 
 * This function decrements the counter of references to \a P. If this counter
 * falls to 0 then the ressources allocated to \a P are released.
 *
 * \param P the unreferenced predicate
 * \pre P != NULL
 * \pre the number of references to \a P > 0
 */
extern void
armoise_predicate_del_reference (armoise_predicate *P);

/*!
 * \brief Computes the smallest reference set (i.e. \f$\cal N\f$, \f$\cal Z\f$,
 * \f$\cal R^+\f$, \f$\cal R\f$) that contains all elements of \a P.
 * \param P the predicate
 * \pre P != NULL
 * \return the attributes of the reference set
 */
extern armoise_domain_attr
armoise_predicate_get_largest_domain (const armoise_predicate *P);

/*!
 * \brief Returns the number of columns of \a P.
 *
 * This function returns the width of the type of \a P. 
 * 
 * \param P the predicate
 * \pre P != NULL
 * \return the width of the type of \a P
 * \see armoise_type, armoise_predicate_get_type
 */
extern int
armoise_predicate_get_arity (armoise_predicate *P);

/*!
 * \brief Returns the type of \a P
 * 
 * \param P the predicate
 * \pre P != NULL
 * \return the width of the type of \a P
 * \see armoise_type
 */
extern armoise_type *
armoise_predicate_get_type (const armoise_predicate *P);

/*!
 * \brief Returns a new reference to the context of \a P
 * 
 * \param P the predicate
 * \pre P != NULL
 * \post result != NULL
 * \return the context used to create \a P
 */
extern armoise_context *
armoise_predicate_get_context (const armoise_predicate *P);

/*!
 * \brief Display the predicate \a P on the \a log stream.
 * \param log the stream where the predicate is displayed
 * \param P the predicate send on \a log
 * \pre P != NULL
 */
extern void
armoise_predicate_log (ccl_log_type log, const armoise_predicate *P);

/*!
 * \brief Display the tree of predicates \a t on the \a log stream.
 * \param log the stream where the variable is displayed
 * \param t the predicate tree send on \a log
 * \pre t != NULL
 */
extern void
armoise_predicate_log_tree (ccl_log_type log, ccl_tree *t);

/*!
 * \brief Computes the domain of \a P 
 * 
 * This function tries to determine the finest domain of columns of \a P
 *
 * \param P the predicate
 * \pre P != NULL
 * \see armoise_domain
 */
extern void
armoise_predicate_compute_domain (armoise_predicate *P);

/*!
 * \brief Collects free and bound variables in \a P.
 * 
 * The function goes through the structure of \a P and collects encountered
 * \ref armoise_variable's. Each collected variable is added (once) to the 
 * list \a variables.
 *
 * \param P the predicate
 * \param variables the list receiving the variables
 * \pre P != NULL
 * \pre variables != NULL
 */
extern void
armoise_predicate_get_variables (armoise_predicate *P, ccl_list *variables);

/*!
 * \brief Returns the kind of \a P.
 *
 * This function returns an enumeration indicating what kind of predicate is 
 * pointed by \a P.
 * 
 * \param P the predicate
 * \pre P != NULL
 * \return the kind of \a P
 */
extern armoise_predicate_kind
armoise_predicate_get_kind (const armoise_predicate *P);

/*!
 * \brief Returns the \a i th operand of \a P.
 *
 * For predicates that are a composition of one or more predicates, this
 * function returns the \a i th operand of the composition. In case of 
 * \ref armoise_predicate_create_indirection "indirection", the predicate is 
 * considered to have one operand.
 *
 * \param P the predicate
 * \param i the index of the operand
 * \pre 0 <= i && i < armoise_predicate_get_nb_operands (P)
 * \return the \a i th operand of \a P
 * \remark The counter of references of the result is not increment. Use \ref 
 * armoise_predicate_get_operand in this case.
 */
extern const armoise_predicate *
armoise_predicate_get_operand (const armoise_predicate *P, int i);

/*!
 * \brief Similar to \ref armoise_predicate_get_operand but the counter of 
 * references of the returned operand is incremented.
 */
extern armoise_predicate *
armoise_predicate_get_operand_at (const armoise_predicate *P, int i);

/*!
 * \brief Returns the number of operands used to build \a P
 * \param P the predicate
 * \pre P != NULL
 * \return the number of operands used to build \a P
 */
extern int 
armoise_predicate_get_nb_operands (const armoise_predicate *P);

/*!
 * \brief Returns the domain of \a P
 * 
 * This function returns the \ref armoise_domain "domain" of \a P computed by 
 * \ref armoise_predicate_compute_domain. The domain of \a P is not defined
 * until the call to \ref armoise_predicate_compute_domain.
 *
 * \param P the predicate
 * \pre P != NULL
 * \post result != NULL
 *
 * \return the domain of \a P
 */
extern armoise_domain *
armoise_predicate_get_domain (const armoise_predicate *P);

/*!
 * \brief Returns the number of elements in a finite set \a P.
 * 
 * If \a P is of kind ARMOISE_P_ENUM (i.e. has been built using \ref 
 * armoise_predicate_create_finite_set), this function returns the number of 
 * terms used in the set defined by \a P.
 * 
 * \param P the predicate
 * \pre armoise_predicate_kind (P) == ARMOISE_P_ENUM
 * \return the number of values used to build \a P.
 */
extern int
armoise_predicate_get_nb_elements (const armoise_predicate *P);

/*!
 * \brief Returns the \a i th element of a finite set \a P
 * \param P the predicate
 * \param i the index of the value
 * \pre 0 <= i && i < armoise_predicate_get_nb_elements (P) 
 * \return the number of values used to build \a P.
 */
extern const armoise_term *
armoise_predicate_get_element (const armoise_predicate *P, int i);

/*!
 * \brief Returns the prototype of the relation \a P defined by a first-order 
 * formula.
 * \param P the predicate
 * \pre armoise_predicate_get_kind (P) == ARMOISE_P_SET
 * \return \e proto if \a P is defined by { \e proto | \e formula }
 */
extern const armoise_term *
armoise_predicate_get_set_proto (const armoise_predicate *P,
				 armoise_predicate **resultP);

/*!
 * \brief Returns the formula of the relation \a P defined by a first-order 
 * formula.
 * \param P the predicate
 * \pre armoise_predicate_get_kind (P) == ARMOISE_P_SET
 * \return \e proto if \a P is defined by { \e proto | \e formula }
 */
extern const armoise_formula *
armoise_predicate_get_set_formula (const armoise_predicate *P);

/*!
 * \brief Retrieves indirection informations pointed by \a P.
 * \param P the predicate
 * \param p_name the identifier of a \ref ccl_tree definied in the context of
 *        \a P
 * \param p_nb_indices the depth of indirection
 * \param p_indices an array used to store the indices of nodes to visit in the
 *        \ref ccl_tree.
 * \pre armoise_predocate_get_kind (P) == ARMOISE_P_INDIRECTION
 */
extern void
armoise_predicate_get_indirection (const armoise_predicate *P, 
				   ccl_ustring *p_name, int *p_nb_indices, 
				   const int **p_indices);

/*!
 * \brief Adds a new reference to the formula \a F
 * 
 * This function increments the counter of references to \a F.
 *
 * \param F the formula newly referenced
 * \pre F != NULL
 *
 * \return the pointer to \a F
 */
extern armoise_formula * 
armoise_formula_add_reference (armoise_formula *F);

/*!
 * \brief Suppresses a reference to the formula \a F
 * 
 * This function decrements the counter of references to \a F. If this counter
 * falls to 0 then the ressources allocated to \a f are released.
 *
 * \param F the unreferenced formula 
 * \pre F != NULL
 * \pre the number of references to \a F > 0
 */
extern void 
armoise_formula_del_reference (armoise_formula *F);

/*!
 * \brief Returns the kind of formula is pointed by \a F
 * \param F the formula
 * \pre F != NULL
 * \return the kind of formula of \a F
 */
extern armoise_formula_kind 
armoise_formula_get_kind (const armoise_formula *F);

/*!
 * \brief Creates a formula encoding the Boolean \a value 
 * 
 * The function creates a formula that encode either \e true if \a value
 * is not 0 or false if \a value is null.
 *
 * \param value the Boolean value
 *
 * \return a newly created formula encoding \e true or \e false of kind 
 * ARMOISE_F_CONSTANT
 */
extern armoise_formula *
armoise_formula_create_constant (int value);

/*!
 * \brief Creates a formula encoding membership checking of \a t in \a P.
 * 
 * \param P the predicate.
 * \param t the value that the membership is checked
 * \pre t != NUL
 * \pre P != NUL
 *
 * \return a formula of kind ARMOISE_F_IN encoding \f$t \in P\f$
 */
extern armoise_formula *
armoise_formula_create_in (armoise_term *t, armoise_predicate *P);

/*!
 * \brief Creates a formula encoding the disjunction of \a F1 and \a F2.
 * 
 * \param F1 the first operand
 * \param F2 the second operand
 * \pre F1 != NULL
 * \pre F2 != NULL
 *
 * \return a formula of kind ARMOISE_F_OR encoding \f$F1 \vee F2\f$
 */
extern armoise_formula *
armoise_formula_create_or (armoise_formula *F1, armoise_formula *F2);

/*!
 * \brief Creates a formula encoding the conjunction of \a F1 and \a F2.
 * 
 * \param F1 the first operand
 * \param F2 the second operand
 * \pre F1 != NULL
 * \pre F2 != NULL
 *
 * \return a formula of kind ARMOISE_F_AND encoding \f$F1 \wedge F2\f$
 */
extern armoise_formula *
armoise_formula_create_and (armoise_formula *F1, armoise_formula *F2);

/*!
 * \brief Creates a formula encoding the equivalence of \a F1 and \a F2.
 * 
 * \param F1 the first operand
 * \param F2 the second operand
 * \pre F1 != NULL
 * \pre F2 != NULL
 *
 * \return a formula of kind ARMOISE_F_EQUIV encoding \f$f1\Leftrightarrow F2\f$
 */
extern armoise_formula *
armoise_formula_create_equiv (armoise_formula *F1, armoise_formula *F2);

/*!
 * \brief Creates a formula encoding the implication of \a F2 by \a F1.
 * 
 * \param F1 the first operand
 * \param F2 the second operand
 * \pre F1 != NULL
 * \pre F2 != NULL
 *
 * \return a formula of kind ARMOISE_F_IMPLY encoding \f$F1\Rightarrow F2\f$
 */
extern armoise_formula *
armoise_formula_create_imply (armoise_formula *F1, armoise_formula *F2);

/*!
 * \brief Creates a formula encoding the negation of \a F.
 * 
 * \param F the operand
 * \pre F != NULL
 *
 * \return a formula of kind ARMOISE_F_NOT encoding \f$\neg F\f$
 */
extern armoise_formula *
armoise_formula_create_not (armoise_formula *F);

/*!
 * \brief Creates a formula encoding the equality of \a t1 and \a t2.
 * 
 * \param t1 the first operand
 * \param t2 the second operand
 * \pre t1 != NULL
 * \pre t2 != NULL
 *
 * \return a formula of kind ARMOISE_F_EQ encoding \f$t1\Rightarrow t2\f$
 */
extern armoise_formula *
armoise_formula_create_eq (armoise_term *t1, armoise_term *t2);

/*!
 * \brief Creates a formula encoding the not-equality of \a t1 and \a t2.
 * 
 * \param t1 the first operand
 * \param t2 the second operand
 * \pre t1 != NULL
 * \pre t2 != NULL
 *
 * \return a formula of kind ARMOISE_F_NEQ encoding \f$t1\not= t2\f$
 */
extern armoise_formula *
armoise_formula_create_neq (armoise_term *t1, armoise_term *t2);

/*!
 * \brief Creates a formula encoding the inequality of \a t1 <= \a t2.
 * 
 * \param t1 the first operand
 * \param t2 the second operand
 * \pre t1 != NULL
 * \pre t2 != NULL
 *
 * \return a formula of kind ARMOISE_F_LEQ encoding \f$t1\leq t2\f$
 */
extern armoise_formula *
armoise_formula_create_leq (armoise_term *t1, armoise_term *t2);

/*!
 * \brief Creates a formula encoding the inequality of \a t1 >= \a t2.
 * 
 * \param t1 the first operand
 * \param t2 the second operand
 * \pre t1 != NULL
 * \pre t2 != NULL
 *
 * \return a formula of kind ARMOISE_F_GEQ encoding \f$t1\geq t2\f$
 */
extern armoise_formula *
armoise_formula_create_geq (armoise_term *t1, armoise_term *t2);

/*!
 * \brief Creates a formula encoding the inequality of \a t1 < \a t2.
 * 
 * \param t1 the first operand
 * \param t2 the second operand
 * \pre t1 != NULL
 * \pre t2 != NULL
 *
 * \return a formula of kind ARMOISE_F_LT encoding \f$t1 < t2\f$
 */
extern armoise_formula *
armoise_formula_create_lt (armoise_term *t1, armoise_term *t2);

/*!
 * \brief Creates a formula encoding the inequality of \a t1 < \a t2.
 * 
 * \param t1 the first operand
 * \param t2 the second operand
 * \pre t1 != NULL
 * \pre t2 != NULL
 *
 * \return a formula of kind ARMOISE_F_GT encoding \f$t1 > t2\f$
 */
extern armoise_formula *
armoise_formula_create_gt (armoise_term *t1, armoise_term *t2);

/*!
 * \brief Creates a formula enclosed with parenthesis (\a F).
 * 
 * \param F the operand
 * \pre F != NULL
 *
 * \return a formula of kind ARMOISE_F_PAREN encoding \f$ (F)\f$
 */
extern armoise_formula *
armoise_formula_create_paren (armoise_formula *F);

/*!
 * \brief Quantifies existentially the formula \a F by the \a nb_vars variables 
 * given in \a qvars.
 * 
 * \param nb_qvars the number of variables to quantify
 * \param qvars the variables to quantify
 * \param F the quantified formula
 * \pre nb_qvars > 0
 * \pre qvars != NULL
 * \pre F != NULL
 *
 * \return a formula of kind ARMOISE_F_EXISTS encoding \f$ \exists qvars[0], 
 * \dots, \exists qvars[nb\_qvars-1] F\f$
 */
extern armoise_formula *
armoise_formula_create_exists (armoise_term *qvars, armoise_predicate *dom, 
			       armoise_formula *F);

/*!
 * \brief Quantifies universally the formula \a F by the \a nb_vars variables 
 * given in \a qvars.
 * 
 * \param nb_qvars the number of variables to quantify
 * \param qvars the variables to quantify
 * \param F the quantified formula
 * \pre nb_qvars > 0
 * \pre qvars != NULL
 * \pre F != NULL
 *
 * \return a formula of kind ARMOISE_F_FORALL encoding \f$ \forall qvars[0], 
 * \dots, \forall qvars[nb\_qvars-1] F\f$
 */
extern armoise_formula *
armoise_formula_create_forall (armoise_term *qvars, armoise_predicate *dom, 
			       armoise_formula *F);

/*!
 * \brief Display the formula \a F on the \a log stream.
 * \param log the stream where the formula is displayed
 * \param F the formula send on \a log
 * \pre F != NULL
 */
extern void
armoise_formula_log (ccl_log_type log, armoise_formula *F);

/*!
 * \brief Collects free and bound variables in \a F.
 * 
 * The function goes through the structure of \a F and collects encountered
 * \ref armoise_variable's. Each collected variable is added (once) to the 
 * list \a variables.
 *
 * \param F the formula
 * \param variables the list receiving the variables
 * \pre F != NULL
 * \pre variables != NULL
 */
extern void
armoise_formula_get_variables (const armoise_formula *F, ccl_list *variables);

/*!
 * \brief Returns the value of the Boolean constant encoded by \a F.
 *
 * \param F the formula
 * \pre armoise_formula_get_kind (F) == ARMOISE_F_CONSTANT
 * \return a non null value if F encodes true
 */
extern int
armoise_formula_get_value (const armoise_formula *F);

/*!
 * \brief Returns the \a i th operands of \a F
 *
 * \param F the formula
 * \param i the index of the expected operand
 * \pre 0 <= i && i < armoise_formula_get_nb_operands (F)
 * \return a non null value if F encodes true
 */
extern const armoise_formula *
armoise_formula_get_operand (const armoise_formula *F, int i);

/*!
 * \brief Returns the number of operands of \a F
 *
 * \param F the formula
 * \pre F != NULL
 * \return the number of operands of \a F
 */
extern int
armoise_formula_get_nb_operands (const armoise_formula *F);

/*!
 * \brief Returns the formula part of a quantified formula \a F
 * 
 * \param F the formula
 * \pre (armoise_formula_get_kind (F) == ARMOISE_F_EXISTS || 
 *       armoise_formula_get_kind (F) == ARMOISE_F_FORALL)
 *
 * \return the sub-formula of \a F that is quantified 
 */
extern const armoise_formula *
armoise_formula_get_quantified_formula (const armoise_formula *F);

/*!
 * \brief Returns the quantified variables of the quantified formula pointed 
 * by \a F
 * 
 * \param F the formula 
 * \param p_nb_vars a pointer to the integer receiving the number of quantified
 *        variables of \a F
 * \pre (armoise_formula_get_kind (F) == ARMOISE_F_EXISTS || 
 *       armoise_formula_get_kind (F) == ARMOISE_F_FORALL)
 *
 * \return the quantified variables defining \a F
 */
extern armoise_term *
armoise_formula_get_quantified_variables (const armoise_formula *F, 
					  armoise_predicate **p_domain);


/*!
 * \brief Returns the term of a membership formula \a F.
 * \param F the formula
 * \pre armoise_formula_get_kind (F) == ARMOISE_F_IN
 * \return the term t of the membership encoded by \a F.
 */
extern armoise_term *
armoise_formula_get_in_term (const armoise_formula *F);

/*!
 * \brief Returns the term of a membership checking formula \a F.
 * \param F the formula
 * \pre armoise_formula_get_kind (F) == ARMOISE_F_IN
 * \return the term t of the membership checking formula \a F.
 */
extern armoise_predicate *
armoise_formula_get_in_predicate (const armoise_formula *F);

/*!
 * \brief Returns the left operand of the comparison encoded by \a F
 * \param F the comparison formula
 * \pre F != NULL
 * \return the left operand of \a F.
 */
extern armoise_term *
armoise_formula_get_cmp_left_term (const armoise_formula *F);

/*!
 * \brief Returns the righr operand of the comparison encoded by \a F
 * \param F the comparison formula
 * \pre F != NULL
 * \return the right operand of \a F.
 */
extern armoise_term *
armoise_formula_get_cmp_right_term (const armoise_formula *F);

/*!
 * \brief Adds a new reference to the term \a t
 * 
 * This function increments the counter of references to \a t.
 *
 * \param t the term newly referenced
 * \pre t != NULL
 *
 * \return the pointer to \a t
 */
extern armoise_term *
armoise_term_add_reference (armoise_term *t);

/*!
 * \brief Suppresses a reference to the term \a t
 * 
 * This function decrements the counter of references to \a t. If this counter
 * falls to 0 then the ressources allocated to \a t are released.
 *
 * \param t the unreferenced term 
 * \pre t != NULL
 * \pre the number of references to \a t > 0
 */
extern void
armoise_term_del_reference (armoise_term *t);

/*!
 * \brief Returns the kind of \a t.
 *
 * This function returns an enumeration indicating what kind of term is 
 * pointed by \a t.
 * 
 * \param t the term
 * \pre t != NULL
 * \return the kind of \a t
 */
extern armoise_term_kind
armoise_term_get_kind (const armoise_term *t);

/*!
 * \brief Returns the dimension of the term \a t
 * \param t the term
 * \pre t != NULL
 * return the width of the type of \a t
 * \see armoise_type, armoise_type_get_width
 */
extern int
armoise_term_get_dim (const armoise_term *t);

/*!
 * \brief Returns the number of operand of \a t
 * 
 * The function returns the number of operands of \a t. If \a t is a constant 
 * or a variable then the result is 0. 
 * \param t the term
 * \pre t != NULL
 * return the number of operand of \a t 
 */
extern int
armoise_term_get_arity (const armoise_term *t);

/*!
 * \brief Check if the \a t is constant or not
 * 
 * The function returns a non-null value if \a t contains no variable.
 *
 * \param t the term
 * \pre t != NULL
 * \return a non null value if \a t is constant
 */
extern int
armoise_term_is_constant (const armoise_term *t);

/*!
 * \brief Returns the type of \a t
 * \param t the term
 * \pre t!= NULL
 * \return the type of \a t
 */
extern armoise_type *
armoise_term_get_type (const armoise_term *t);

/*!
 * \brief Returns the \ref armoise_variable underlying \a t
 * \param t the term
 * \pre armoise_term_get_kind (t) == ARMOISE_T_VARIABLE
 * \return the type of \a t
 */
extern armoise_variable *
armoise_term_get_variable (const armoise_term *t);

/*!
 * \brief Retrieves the value of the constant encoded by  \a t
 * \param t the term
 * \param num the address of the integer receiving the numerator
 * \param den the address of the integer receiving the denominator
 * \pre armoise_term_get_kind (t) == ARMOISE_T_CONSTANT
 */
extern void
armoise_term_get_value (const armoise_term *t, int *num, int *den);

/*!
 * \brief Returns a new reference to the \a i th operand of \a t.
 *
 * For terms that are a composition of one or more terms, this
 * function returns the \a i th operand of the composition. 
 *
 * \param t the term
 * \param i the index of the operand
 * \pre 0 <= i && i < armoise_term_get_nb_operands (t)
 * \return a new reference to the \a i th operand of \a P
 */
extern armoise_term *
armoise_term_get_operand_at (armoise_term *t, int i);

/*!
 * \brief Creates a new term encoding the variable \a v 
 * \param v the variable
 * \pre v != NULL
 * \return a term of kind ARMOISE_T_VARIABLE encoding the variable \a v
 */
extern armoise_term *
armoise_term_create_variable (armoise_variable *v);

/*!
 * \brief Creates a new term encoding the \a index th element of the vector 
 * \a variable
 * \param variable the vectorial variable
 * \param index the index in the vector
 * \pre variable != NULL
 * \return a term of kind ARMOISE_T_ELEMENT encoding the 
 * <em>variable[index]</em>
 */
extern armoise_term *
armoise_term_create_vector_element (armoise_term *variable, int index);

/*!
 * \brief Creates a new term encoding the rational number \a num / \a den
 * \remark the fration \a num / \a den is reduced in such a way that
 * the denominator becomes > 0.
 *
 * \param num the numerator
 * \param den the denominator
 * \pre den != 0
 * \return a term of kind ARMOISE_T_CONSTANT encoding the \a num / \a den
 */
extern armoise_term *
armoise_term_create_constant (int num, int den);

/*!
 * \brief Creates a term encoding a vector of \a terms 
 * 
 * If width == 1 then terms[0] is returned.
 *
 * \param width the number of components of the vector
 * \param terms the components of the vector
 * \pre width >= 1
 * \return a term of kind ARMOISE_T_VECTOR encoding the vector 
 * <em>(terms[0], ..., terms[width-1])</em>
 */
extern armoise_term *
armoise_term_create_vector (int width, armoise_term **terms);

/*!
 * \brief Creates a term encoding the opposite of \a t
 * \param t the term
 * \pre t != NULL
 * \return a term of kind ARMOISE_T_NEG encoding the \f$-t\f$
 */
extern armoise_term *
armoise_term_create_neg (armoise_term *t);

/*!
 * \brief Creates a term encoding the multiplication of \a t1 and \a t2.
 * \param t1 the first operand
 * \param t2 the second operand
 * \pre t1 != NULL
 * \pre t2 != NULL
 * \return a term of kind ARMOISE_T_MUL encoding the \f$t1 \times t2\f$
 */
extern armoise_term *
armoise_term_create_mul (armoise_term *t1, armoise_term *t2);

/*!
 * \brief Creates a term encoding the division of \a t1 by \a t2.
 * \param t1 the first operand
 * \param t2 the second operand
 * \pre t1 != NULL
 * \pre t2 != NULL
 * \return a term of kind ARMOISE_T_DIV encoding the \f$t1 / t2\f$
 */
extern armoise_term *
armoise_term_create_div (armoise_term *t1, armoise_term *t2);

/*!
 * \brief Creates a term encoding the modulo of \a t1 by \a t2.
 * \param t1 the first operand
 * \param t2 the second operand
 * \pre t1 != NULL
 * \pre t2 != NULL
 * \return a term of kind ARMOISE_T_MOD encoding the \f$t1 \% t2\f$
 */
extern armoise_term *
armoise_term_create_mod (armoise_term *t1, armoise_term *t2);

/*!
 * \brief Creates a term encoding the addition of \a t1 and \a t2.
 * \param t1 the first operand
 * \param t2 the second operand
 * \pre t1 != NULL
 * \pre t2 != NULL
 * \return a term of kind ARMOISE_T_ADD encoding the \f$t1 + t2\f$
 */
extern armoise_term *
armoise_term_create_add (armoise_term *t1, armoise_term *t2);

/*!
 * \brief Creates a term encoding the difference of \a t1 and \a t2.
 * \param t1 the first operand
 * \param t2 the second operand
 * \pre t1 != NULL
 * \pre t2 != NULL
 * \return a term of kind ARMOISE_T_SUB encoding the \f$t1 - t2\f$
 */
extern armoise_term *
armoise_term_create_sub (armoise_term *t1, armoise_term *t2);

/*!
 * \brief Display the term \a t on the \a log stream.
 * \param log the stream where the term is displayed
 * \param t the term send on \a log
 * \pre t != NULL
 */
extern void
armoise_term_log (ccl_log_type log, armoise_term *t);

/*!
 * \brief Collects free and bound variables in \a t.
 * 
 * The function goes through the structure of \a t and collects encountered
 * \ref armoise_variable's. Each collected variable is added (once) to the 
 * list \a variables.
 *
 * \param t the term
 * \param variables the list receiving the variables
 * \pre t != NULL
 * \pre variables != NULL
 */
extern void
armoise_term_get_variables (const armoise_term *t, ccl_list *variables);

/*!
 * \brief Returns the number of operands of \a t
 * \param t the term
 * \pre t != NULL
 * \return the number of operands of \a t
 */
extern int
armoise_term_get_nb_operands (const armoise_term *t);

/*!
 * \brief Returns the \a i th operand of \a P.
 *
 * For terms that are a composition of one or more terms, this function returns
 * the \a i th operand of the composition. 
 *
 * \param t the term
 * \param i the index of the operand
 * \pre 0 <= i && i < armoise_term_get_nb_operands (P)
 * \return the \a i th operand of \a P
 * \remark The counter of references of the result is not increment. Use \ref 
 * armoise_predicate_get_operand in this case.
 */
extern const armoise_term *
armoise_term_get_operand (const armoise_term *t, int i);

/*!
 * \brief Returns the variable referenced by a vector element term \a t
 * \param t the term of the form <em>var[i1]...[in]</em>
 * \pre armoise_term_get_kind (t) == ARMOISE_T_ELEMENT
 * \return the vectorial term of the vector element term \a t
 */
extern const armoise_term *
armoise_term_get_vec_element_variable (const armoise_term *t);

/*!
 * \brief Returns the index used by a vector element term \a t
 * \param t the term of the form <em>var[i]</em>
 * \pre armoise_term_get_kind (t) == ARMOISE_T_ELEMENT
 * \return the index used by the vector element term \a t
 */
extern int
armoise_term_get_vec_element_index (const armoise_term *t);

/*!
 * \brief Creates the scalar type
 */
extern armoise_type *
armoise_type_create_scalar (void);

/*!
 * \brief Creates a composite type made of \a nb_subtypes \a subtypes.
 * \param subtypes the sub-types
 * \param nb_subtypes the number of subtypes
 * \pre nb_subtypes > 0
 * \return the type subtypes[0] * ... * subtypes[nb_subtypes-1] 
 */
extern armoise_type *
armoise_type_create_product (int nb_subtypes, armoise_type **subtypes);

/*!
 * \brief Returns a non-null value if \a type is scalar
 * \param type the type
 * \pre type != NULL
 * \return a non null value if the \a type is the scalar type
 */
extern int
armoise_type_is_scalar (const armoise_type *type);

/*!
 * \brief Adds a new reference to \a type
 * 
 * This function increments the counter of references to \a tye.
 *
 * \param type the newly referenced type
 * \pre type != NULL
 *
 * \return the pointer to \a type
 */
extern armoise_type *
armoise_type_add_reference (armoise_type *type);

/*!
 * \brief Suppresses a reference to \a type
 * 
 * This function decrements the counter of references to \a type. If this 
 * counter falls to 0 then the ressources allocated to \a type are released.
 *
 * \param type the unreferenced type
 * \pre type != NULL
 * \pre the number of references to \a type > 0
 */
extern void
armoise_type_del_reference (armoise_type *type);

/*!
 * \brief Returns the number of component of \a type
 * 
 * \param type the type
 * \pre type != NULL
 * \return the number of component of \a type
 */
extern int
armoise_type_get_width (const armoise_type *type);

/*!
 * \brief Returns the number of leaves composing \a type
 * 
 * This function counts the number of elementary type (i.e. scalar or unknown)
 * composing \a type and its sub-types.
 *
 * \param type the type
 * \pre type != NULL
 * \return the number of component of \a type
 */
extern int
armoise_type_get_depth_width (const armoise_type *type);

/*!
 * \brief Display the \a type on the \a log stream.
 * \param log the stream where the type is displayed
 * \param type the type send on \a log
 * \pre type != NULL
 */
extern void
armoise_type_log (ccl_log_type log, const armoise_type *type);

/*!
 * \brief Check whether \a type and \a other are equal.
 * \param type the first type
 * \param other the second type
 * \pre type != NULL && other != NULL
 * \return a non null value if \a type and \a other are equal.
 */
extern int
armoise_type_equals (const armoise_type *type, const armoise_type *other);

/*!
 * \brief Returns the reference of the \a i th subtype of \a type
 * \param type the type
 * \param i the index of the sub-type
 * \pre 0 <= i && i < armoise_type_get_width (type)
 * \return a non null value if \a type and \a other are equal.
 */
extern armoise_type **
armoise_type_get_subtype_ref (armoise_type *type, int i);

/*!
 * \brief Returns the \a i th subtype of \a type
 * \param type the type
 * \param i the index of the sub-type
 * \pre 0 <= i && i < armoise_type_get_width (type)
 * \return a non null value if \a type and \a other are equal.
 */
extern const armoise_type *
armoise_type_get_subtype (const armoise_type *type, int i);

/*!
 * \brief Creates a copy of \a type
 * \param type the type
 * \pre type != NULL
 * \return a copy of \a type
 */
extern armoise_type *
armoise_type_duplicate (const armoise_type *type);

/*!
 * \brief Attributes for the domain \f$\cal R\f$
 */
extern const armoise_domain_attr ARMOISE_DOMAIN_ATTR_FOR_REALS;

/*!
 * \brief Attributes for the domain \f$\cal Z\f$
 */
extern const armoise_domain_attr ARMOISE_DOMAIN_ATTR_FOR_INTEGERS;

/*!
 * \brief Attributes for the domain \f${\cal R}^+\f$
 */
extern const armoise_domain_attr ARMOISE_DOMAIN_ATTR_FOR_POSITIVES;

/*!
 * \brief Attributes for the domain \f${\cal N}\f$
 */
extern const armoise_domain_attr ARMOISE_DOMAIN_ATTR_FOR_NATURALS;

/*!
 * \brief Creates a domain with given attibutes \a attr
 * \param attr the attributes
 * \return a domain with given attibutes \a attr
 */
extern armoise_domain *
armoise_domain_create_with_attr (const armoise_domain_attr attr);

/*!
 * \brief Creates a domain containing real numbers
 * \return a domain containing real numbers
 */
extern armoise_domain *
armoise_domain_create (void);

/*!
 * \brief Creates an empty domain
 * \return an empty domain
 */
extern armoise_domain *
armoise_domain_create_empty (void);

/*!
 * \brief Creates a domain composed of \a width \a domains
 * \param width the number of components
 * \param domains the components of the result
 * \pre width >= 1
 * \pre domains != NULL
 * \return the compound domain \f$domains[0]\times\dots\times 
 * domains[width-1]\f$
 */
extern armoise_domain *
armoise_domain_create_compound_domain (int width, armoise_domain **domains);

/*!
 * \brief Adds a new reference to the domain \a d
 * 
 * This function increments the counter of references to \a d.
 *
 * \param d the domain newly referenced
 * \pre d != NULL
 *
 * \return the pointer to \a d
 */
extern armoise_domain *
armoise_domain_add_reference (armoise_domain *d);

/*!
 * \brief Suppresses a reference to the domain \a d
 * 
 * This function decrements the counter of references to \a d. If this counter
 * falls to 0 then the ressources allocated to \a ctx are released.
 *
 * \param d the unreferenced domain 
 * \pre d != NULL
 * \pre the number of references to \a d > 0
 */
extern void
armoise_domain_del_reference (armoise_domain *d);

/*!
 * \brief Returns the number of components of \a d
 * \param d the domain
 * \pre d != NULL
 * \return the number of components of \a d
 */
extern int
armoise_domain_get_width (const armoise_domain *d);

/*!
 * \brief Checks if \a d is empty
 * \param d the domain
 * \pre d != NULL
 * \return a non null value if \a d contains no element.
 */
extern int
armoise_domain_is_empty (const armoise_domain *d);

/*!
 * \brief Returns the \a i th component of \a d
 * \param d the domain
 * \param i the index of the component
 * \pre d != NULL 
 * \pre 0 <= i && i < armoise_domain_get_width (d)
 * \return the \a i th component of \a d
 */
extern const armoise_domain *
armoise_domain_get_component (const armoise_domain *d, int i);

/*!
 * \brief Changes the \a i th component of \a d with \a di
 * \param d the domain
 * \param i the index of the component
 * \param di the new component
 * \pre d != NULL
 * \pre di != NULL
 * \pre 0 <= i && i <= armoise_domain_get_width (d)
 */
extern void
armoise_domain_set_component (armoise_domain *d, int i,
			      const armoise_domain *di);

/*!
 * \brief Returns the lower bound of \a d
 * \param d the domain
 * \pre d != NULL
 * \return the lower bound of \a d
 */
extern int
armoise_domain_get_min (const armoise_domain *d);

/*!
 * \brief Returns the upper bound of \a d
 * \param d the domain
 * \pre d != NULL
 * \return the upper bound of \a d
 */
extern int
armoise_domain_get_max (const armoise_domain *d);

/*!
 * \brief Changes the attributes of \a d
 * \param d the domain
 * \param attr the new attributes for \a d
 * \pre d != NULL
 * \return the attributes defining \a d
 */
extern void
armoise_domain_set_attr (armoise_domain *d, const armoise_domain_attr attr);

/*!
 * \brief Returns attributes defining \a d
 * \param d the domain
 * \pre d != NULL
 * \return the attributes defining \a d
 */
extern armoise_domain_attr 
armoise_domain_get_attr (const armoise_domain *d);

/*!
 * \brief Checks if \a d is finite
 * \param d the domain
 * \pre d != NULL
 * \return a non null value if \a d contains a finite number of elements
 */
extern int
armoise_domain_is_finite (const armoise_domain *d);

/*!
 * \brief Checks if \a d is a subset of integers
 * \param d the domain
 * \pre d != NULL
 * \return a non null value if \a d is a subset of integers
 */
extern int
armoise_domain_is_restricted_to_integers (const armoise_domain *d);

/*!
 * \brief Display the \a domain on the \a log stream.
 * \param log the stream where the domain is displayed
 * \param d the domain send on \a log
 * \pre d != NULL
 */
extern void
armoise_domain_log (ccl_log_type log, const armoise_domain *d);

/*!
 * \brief Return a copy of \a d
 * \param d the domain
 * \pre d != NULL
 * \return a copy of \a d
 */
extern armoise_domain *
armoise_domain_duplicate (const armoise_domain *d);

/*!
 * \brief Checks if \a d1 and \a d2 are equal
 * \param d1 the first domain
 * \param d2 the second domain
 * \pre d1 != NULL
 * \pre d2 != NULL
 * \return a non null value if \a d1 = \a d2
 */
extern int
armoise_domain_equals (const armoise_domain *d1, const armoise_domain *d2);

/*!
 * \brief Checks if \a d contains only one element
 * \param d the domain
 * \pre d != NULL
 * \return a non null value if \a d is a singleton
 */
extern int
armoise_domain_is_singleton (const armoise_domain *d);

END_C_DECLS

#endif /* ! __ARMOISE_H__ */
