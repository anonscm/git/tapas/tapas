/*
 * armoise.c -- add a comment about this file
 * 
 * This file is a part of the ARMOISE language library
. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "armoise.h"

int
armoise_init (void)
{
  return 1;
}

			/* --------------- */

void
armoise_terminate (void)
{
  
}
