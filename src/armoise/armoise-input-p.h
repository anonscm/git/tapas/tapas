/*
 * armoise-input-p.h -- 
 * Copyright (C) 2007 J. Leroux, G. Point, LaBRI, CNRS UMR 5800, 
 * Universite Bordeaux I
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */
/*
 * $Author: point $
 * $Revision: 1.1 $
 * $Date: 2007/07/09 14:35:35 $
 * $Id: armoise-input-p.h,v 1.1 2007/07/09 14:35:35 point Exp $
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __ARMOISE_INPUT_P_H__
# define __ARMOISE_INPUT_P_H__

# include <stdio.h>
# include "armoise-tree.h"
# include "armoise-input.h"

extern ccl_parse_tree_value armoise_token_value;
extern int armoise_current_line;
extern const char *armoise_current_filename;
extern FILE *armoise_in;
extern armoise_tree *armoise_syntax_tree;
extern armoise_tree *armoise_syntax_tree_container;
extern int armoise_number_of_errors;

extern void
armoise_init_lexer (FILE *stream, const char *input_name);

extern void
armoise_terminate_lexer (void);

extern int
armoise_lex (void);

extern int
armoise_parse (void);

extern void
armoise_error (const char *message);

extern int
armoise_wrap (void);

#endif /* ! __ARMOISE_INPUT_P_H__ */
