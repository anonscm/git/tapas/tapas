/*
 * armoise-tree-p.h --
 * Copyright (C) 2007 J. Leroux, G. Point, LaBRI, CNRS UMR 5800, 
 * Universite Bordeaux I
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */
/*
 * $Author: point $
 * $Revision: 1.1 $
 * $Date: 2007/07/24 10:14:32 $
 * $Id: armoise-tree-p.h,v 1.1 2007/07/24 10:14:32 point Exp $
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __ARMOISE_TREE_P_H__
# define __ARMOISE_TREE_P_H__

# include "armoise-tree.h"

# define IS_LABELLED_WITH(t,id) ((t)->node_type == ARMOISE_TREE_ ## id)

# define INVALID_NODE() ccl_throw (internal_error, "invalid node type")

#endif /* ! __ARMOISE_TREE_P_H__ */
