/**
 * $Revision: 1.10 $
 * $Date: 2009/04/02 13:53:36 $
 * $Id: armoise-tree.c,v 1.10 2009/04/02 13:53:36 point Exp $
 */
#include <ccl/ccl-assert.h>
#include "armoise-tree-p.h"

static void
s_log_tab (ccl_log_type log, int length);

static void
s_log_predicate (int tab, ccl_log_type log, armoise_tree *t);

static void
s_log_definition (int tab, ccl_log_type log, armoise_tree *t);

			/* --------------- */

void
armoise_tree_log (ccl_log_type log, armoise_tree *t)
{
  for (; t; t = t->next)
    {
      if (IS_LABELLED_WITH (t, DEFINITION))
	s_log_definition (0, log, t);
      else
	{
	  ccl_assert (IS_LABELLED_WITH (t, PREDICATE));
	  s_log_predicate (0, log, t);
	}
      ccl_log (log, ";\n");
    }
}

			/* --------------- */


static void
s_log_tab (ccl_log_type log, int length)
{
  while (length--)
    ccl_log (log, " ");
}

			/* --------------- */

static int
s_log_identifier_or_tuple (ccl_log_type log, armoise_tree *t)
{
  int result;

  if (IS_LABELLED_WITH (t, IDENTIFIER))
    {
      ccl_log (log, "%s", t->value.id_value);
      result = strlen (t->value.id_value);
    }
  else
    {
      ccl_assert (IS_LABELLED_WITH (t, ID_TUPLE));

      result = 2;
      ccl_log (log, "<");
      for (t = t->child; t; t = t->next)
	{
	  result += s_log_identifier_or_tuple (log, t);
	  if (t->next != NULL)
	    {
	      result++;
	      ccl_log (log, ", ");
	    }
	}
      ccl_log (log, ">");
    }

  return result;
}

			/* --------------- */

static void
s_log_definition (int tab, ccl_log_type log, armoise_tree *t)
{
  int l;

  ccl_pre (IS_LABELLED_WITH (t, DEFINITION));

  l = s_log_identifier_or_tuple (log, t->child);
  ccl_log (log, " := ");
  if (IS_LABELLED_WITH (t->child->next->child, PREDICATE_CONTEXT))
    {
      ccl_log (log, "\n");
      l = 2;
      s_log_tab (log, l + tab);
    }
  else
    {
      l += 4;
    }
  s_log_predicate (l + tab, log, t->child->next);
}

			/* --------------- */

static void
s_log_predicate_tuple (int tab, ccl_log_type log, armoise_tree *t);


static void
s_log_predicate (int tab, ccl_log_type log, armoise_tree *t)
{
  int ltab = 0;
  ccl_pre (IS_LABELLED_WITH (t, PREDICATE));

  t = t->child;
  if (IS_LABELLED_WITH (t, PREDICATE_CONTEXT))
    {
      armoise_tree *defs;

      ccl_log (log, "let\n");
      ltab = 2;
      for (defs = t->child; defs; defs = defs->next)
	{
	  s_log_tab (log, tab + ltab);
	  s_log_definition (tab + ltab, log, defs);
	  ccl_log (log, ";\n");
	}
      s_log_tab (log, tab);
      ccl_log (log, "in\n");
      s_log_tab (log, tab + ltab);
      t = t->next;
    }
  s_log_predicate_tuple (tab + ltab, log, t);
}

			/* --------------- */

static void
s_log_predicate_without_context (int tab, ccl_log_type log, armoise_tree *t);

static void
s_log_predicate_tuple (int tab, ccl_log_type log, armoise_tree *t)
{
  if (IS_LABELLED_WITH (t, PREDICATE_TUPLE))
    {
      ccl_log (log, "< ");
      for (t = t->child; t; t = t->next)
	{
	  s_log_predicate_tuple (tab, log, t);
	  if (t->next != NULL)
	    ccl_log (log, ", ");
	}
      ccl_log (log, " >");
    }
  else
    {
      s_log_predicate_without_context (tab, log, t);
    }
}

			/* --------------- */

static void
s_log_set (ccl_log_type log, armoise_tree *t);

static void
s_log_set_of_vectors (ccl_log_type log, armoise_tree *t);

			/* --------------- */

static void
s_log_predicate_without_context (int tab, ccl_log_type log, armoise_tree *t)
{
  switch (t->node_type)
    {
    case ARMOISE_TREE_UNION:
    case ARMOISE_TREE_DIFFERENCE:
    case ARMOISE_TREE_XOR:
    case ARMOISE_TREE_INTERSECTION:
    case ARMOISE_TREE_PLUS:
    case ARMOISE_TREE_MINUS:
    case ARMOISE_TREE_MUL:
    case ARMOISE_TREE_DIV:
      {
	const char *op = NULL;

	s_log_predicate_without_context (0, log, t->child);
	switch (t->node_type)
	  {
	  case ARMOISE_TREE_UNION: op = "||"; break;
	  case ARMOISE_TREE_DIFFERENCE: op = "\\"; break;
	  case ARMOISE_TREE_XOR: op = "^"; break;
	  case ARMOISE_TREE_INTERSECTION: op = "&&"; break;
	  case ARMOISE_TREE_PLUS: op = "+"; break;
	  case ARMOISE_TREE_MINUS: op = "-"; break;
	  case ARMOISE_TREE_MUL: op = "*"; break;
	  case ARMOISE_TREE_DIV: op = "/"; break;
	  }
	ccl_assert (op != NULL);
	ccl_log (log, " %s ", op);
	s_log_predicate_without_context (0, log, t->child->next);
      }
      break;

    case ARMOISE_TREE_COMPLEMENT: 
    case ARMOISE_TREE_NEG:
      if (IS_LABELLED_WITH (t, NEG))
	ccl_log (log, "-"); 
      else
	ccl_log (log, "!");
      s_log_predicate_without_context (0, log, t->child);
      break;

    case ARMOISE_TREE_NATURALS:
      ccl_log (log, "nat");
      break;
    case ARMOISE_TREE_INTEGERS:
      ccl_log (log, "int");
      break;
    case ARMOISE_TREE_POSITIVES:
      ccl_log (log, "posi");
      break;
    case ARMOISE_TREE_REALS:
      ccl_log (log, "real");
      break;

    case ARMOISE_TREE_INTEGER:
      ccl_log (log, "%d", t->value.int_value);
      break;

    case ARMOISE_TREE_LIST_ELEMENT:
      s_log_predicate_without_context (0, log, t->child);      
      ccl_assert (IS_LABELLED_WITH (t->child->next, INTEGER));
      ccl_log (log, "[%d]", t->child->next->value.int_value);
      break;

    case ARMOISE_TREE_IDENTIFIER:
      ccl_log (log, "%s", t->value.id_value);
      break;

    case ARMOISE_TREE_PAREN:
      ccl_log (log, "(");
      s_log_predicate_without_context (0, log, t->child);
      ccl_log (log, ")");
      break;

    case ARMOISE_TREE_CARTESIAN_PRODUCT:
      ccl_log (log, "(");
      for (t = t->child; t; t = t->next)
	{
	  s_log_predicate_without_context (0, log, t);
	  if (t->next != NULL)
	    ccl_log (log, ", ");
	}
      ccl_log (log, ")");
      break;
    default:
      ccl_assert (IS_LABELLED_WITH (t, SET) ||
		  IS_LABELLED_WITH (t, ENUMERATED_SET));
      if (IS_LABELLED_WITH (t, SET))
	s_log_set (log, t);
      else
	s_log_set_of_vectors (log, t);
    }
}

			/* --------------- */

static void
s_log_vector (ccl_log_type log, armoise_tree *t);

static void
s_log_formula (ccl_log_type log, armoise_tree *t);

static void
s_log_set (ccl_log_type log, armoise_tree *t)
{
  ccl_pre (IS_LABELLED_WITH (t, SET));

  t = t->child;
  ccl_log (log, "{ ");
  if (IS_LABELLED_WITH (t, IDENTIFIER))
      ccl_log (log, "%s", t->value.id_value);
  else if (IS_LABELLED_WITH (t, IN))
    s_log_formula (log, t);
  else
    s_log_vector (log, t);
  ccl_log (log, " | ");

  s_log_formula (log, t->next);
  ccl_log (log, " }");
}

			/* --------------- */


static void
s_log_term (ccl_log_type log, armoise_tree *t);

static void
s_log_set_of_vectors (ccl_log_type log, armoise_tree *t)
{
  int singleton = (t->child->next == NULL);

  ccl_pre (IS_LABELLED_WITH (t, ENUMERATED_SET));

  if (! singleton)
    ccl_log (log, "{ ");
  for (t = t->child; t; t = t->next)
    {
      s_log_term (log, t);
      if (t->next != NULL)
	ccl_log (log, ", ");
    }
  if (! singleton)
    ccl_log (log, " }");
}

			/* --------------- */


static void
s_log_vector (ccl_log_type log, armoise_tree *t)
{
  ccl_pre (IS_LABELLED_WITH (t, VECTOR));

  ccl_log (log, "(");
  for (t = t->child; t; t = t->next)
    {
      s_log_term (log, t);
      if (t->next != NULL)
	ccl_log (log, ", ");
    }
  ccl_log (log, ")");
}

			/* --------------- */

static void
s_log_formula (ccl_log_type log, armoise_tree *t)
{
  armoise_tree *aux;

  switch (t->node_type)
    {
    case ARMOISE_TREE_OR:
    case ARMOISE_TREE_AND:
    case ARMOISE_TREE_EQUIV:
    case ARMOISE_TREE_IMPLY:
      s_log_formula (log, t->child);
      switch (t->node_type)
	{
	case ARMOISE_TREE_OR: ccl_log (log, " | "); break;
	case ARMOISE_TREE_AND: ccl_log (log, " & "); break;
	case ARMOISE_TREE_EQUIV: ccl_log (log, " <=> "); break;
	case ARMOISE_TREE_IMPLY: ccl_log (log, " => "); break;
	}
      s_log_formula (log, t->child->next);
      break;

    case ARMOISE_TREE_EQ:
    case ARMOISE_TREE_NEQ:
    case ARMOISE_TREE_GEQ:
    case ARMOISE_TREE_LEQ:
    case ARMOISE_TREE_GT:
    case ARMOISE_TREE_LT:
      s_log_term (log, t->child);
      switch (t->node_type)
	{
	case ARMOISE_TREE_EQ: ccl_log (log, " = "); break;
	case ARMOISE_TREE_NEQ: ccl_log (log, " != "); break;
	case ARMOISE_TREE_GEQ: ccl_log (log, " >= "); break;
	case ARMOISE_TREE_LEQ: ccl_log (log, " <= "); break;
	case ARMOISE_TREE_GT: ccl_log (log, " > "); break;
	case ARMOISE_TREE_LT: ccl_log (log, " < "); break;
	}
      s_log_term (log, t->child->next);
      break;

    case ARMOISE_TREE_EXISTS:
    case ARMOISE_TREE_FORALL:
      if (IS_LABELLED_WITH (t, EXISTS))
	ccl_log (log, "exists ");
      else
	ccl_log (log, "forall ");
      for (aux = t->child->next; aux; aux = aux->next)
	{
	  ccl_assert (IS_LABELLED_WITH (aux, IDENTIFIER));
	  ccl_log (log, "%s", aux->value.id_value);
	  if (aux->next != NULL)
	    ccl_log (log, ", ");
	}
      ccl_log (log, " ");
      s_log_formula (log, t->child);
      break;
      
    case ARMOISE_TREE_NOT:
      ccl_log (log, "! ");
      s_log_formula (log, t->child);
      break;

    case ARMOISE_TREE_IN:
      s_log_term (log, t->child);
      ccl_log (log, " in ");
      s_log_predicate_without_context (0, log, t->child->next);
      break;

    case ARMOISE_TREE_PAREN:
      ccl_log (log, "(");
      s_log_formula (log, t->child);
      ccl_log (log, ")");
      break;

    case ARMOISE_TREE_TRUE:
      ccl_log (log, "true");
      break;

    case ARMOISE_TREE_FALSE:
      ccl_log (log, "false");
      break;

    default :
      ccl_assert (IS_LABELLED_WITH (t,CALL));
      ccl_assert (IS_LABELLED_WITH (t->child, IDENTIFIER));
      ccl_log (log, "%s ", t->child->value.id_value);
      s_log_vector (log, t->child->next);
      break;
    }
}

			/* --------------- */

static void
s_log_term (ccl_log_type log, armoise_tree *t)
{
  switch (t->node_type)
    {
    case ARMOISE_TREE_PLUS:
    case ARMOISE_TREE_MINUS:
    case ARMOISE_TREE_MUL:
    case ARMOISE_TREE_DIV:
    case ARMOISE_TREE_MOD:
      s_log_term (log, t->child);
      switch (t->node_type)
	{
	case ARMOISE_TREE_PLUS: ccl_log (log, " + "); break;
	case ARMOISE_TREE_MINUS: ccl_log (log, " - "); break;
	case ARMOISE_TREE_MUL: ccl_log (log, " * "); break;
	case ARMOISE_TREE_DIV: ccl_log (log, " / "); break;
	case ARMOISE_TREE_MOD: ccl_log (log, " %x "); break;
	}
      s_log_term (log, t->child->next);
      break;

    case ARMOISE_TREE_NEG:
      {
	int needpar = !(IS_LABELLED_WITH (t->child, IDENTIFIER) || 
			IS_LABELLED_WITH (t->child, INTEGER));
	ccl_log (log, "-");
	if (needpar)
	  ccl_log (log, "(");	
	s_log_term (log, t->child);
	if (needpar)
	  ccl_log (log, ")");
      }
      break;

    case ARMOISE_TREE_INTEGER:
      ccl_log (log, "%d", t->value.int_value);
      break;

    case ARMOISE_TREE_IDENTIFIER:
      ccl_log (log, "%s", t->value.id_value);
      break;
    case ARMOISE_TREE_LIST_ELEMENT:
      s_log_term (log, t->child);
      ccl_assert (IS_LABELLED_WITH (t->child->next, INTEGER));
      ccl_log (log, "[%d]", t->child->next->value.int_value);
      break;

    default:
      s_log_vector (log, t);
    }
}


			/* --------------- */

