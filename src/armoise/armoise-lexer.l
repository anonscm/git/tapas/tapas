/*
 * armoise-lexer.l -- 
 * Copyright (C) 2007 J. Leroux, G. Point, LaBRI, CNRS UMR 5800, 
 * Universite Bordeaux I
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */
/*
 * $Author: point $
 * $Revision: 1.5 $
 * $Date: 2007/11/27 16:42:24 $
 * $Id: armoise-lexer.l,v 1.5 2007/11/27 16:42:24 point Exp $
 */

/*!
 * \file
 * \brief
 * 
 */
%{
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-string.h>
#include "armoise/armoise-input-p.h"
#include "armoise/armoise-syntax.h"

#define YY_NO_UNPUT
/* Some systems define fileno as a preprocessor macro. */
#ifndef fileno
extern int fileno (FILE *);
#endif

ccl_parse_tree_value armoise_token_value;
int armoise_current_line;
const char *armoise_current_filename;
static int c_comment_start_line = 0;
%}

unsigned_integer  ([1-9][0-9]*)|0
simple_identifier [a-zA-Z_][a-zA-Z_0-9]*
cpp_comment       "//".*
blank_character   [ \t\r]
newline           [\n]

%x c_comment

%%

"/*" { 
  BEGIN(c_comment);
  c_comment_start_line = armoise_current_line; 
}



":=" { return TOK_ASSIGN; }
"let" { return TOK_LET; }
"in" { return TOK_IN; }
"!=" { return TOK_NEQ; }
"<=" { return TOK_LEQ; }
">=" { return TOK_GEQ; }
"=>" { return TOK_IMPLY; }
"<=>" { return TOK_EQUIV; }
"||" { return TOK_UNION; } 
"&&" { return TOK_INTERSECTION; }
"or" { return TOK_OR; } 
"and" { return TOK_AND; }
"not" { return TOK_NOT; }
"exists" { return TOK_EXISTS; }
"forall" { return TOK_FORALL; }
"posi" { return TOK_POSITIVES; }
"int" { return TOK_INTEGERS; }
"nat" { return TOK_NATURALS; }
"empty" { return TOK_EMPTY; }
"real" { return TOK_REALS; }
"true" { return TOK_TRUE; }
"false" { return TOK_FALSE; }
"..." { return TOK_DOTS; }

{unsigned_integer}  { 
  armoise_token_value.int_value = atoi (yytext);
  return TOK_UINT; 
}

{simple_identifier} { 
  armoise_token_value.id_value = ccl_string_make_unique (yytext);
  return TOK_IDENTIFIER;
}

{cpp_comment} { ((void)0); }
{blank_character}+ { ((void)0); }

<c_comment>[^*\n]*      { /* empty */ }
<c_comment>"*"+[^*/\n]* { /* empty */ }
<c_comment>\n           { armoise_current_line++; }
<c_comment>"*"+"/"      { BEGIN (INITIAL); }
<c_comment><<EOF>>      { return TOK_ERROR; }

{newline} { armoise_current_line++; }

.  { return (int)yytext[0]; }

%%

void
armoise_init_lexer (FILE *stream, const char *input_name)
{
  armoise_current_line = 1;
  armoise_current_filename = input_name;
  armoise_in = stream;

  if (stream != NULL)
    yyrestart (stream);
  else
    {
      YY_BUFFER_STATE s = yy_scan_string (input_name);
      armoise_current_filename = "internal_string";
      yy_switch_to_buffer (s);
    }
}

			/* --------------- */

void
armoise_terminate_lexer (void)
{  
  if (YY_CURRENT_BUFFER != NULL)
     yy_delete_buffer (YY_CURRENT_BUFFER);
  
  armoise_in = NULL;
  armoise_current_line = -1;
  armoise_current_filename = NULL;
}

			/* --------------- */

