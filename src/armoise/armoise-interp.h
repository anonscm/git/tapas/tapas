/*
 * armoise-interp.h -- add a comment about this file
 * 
 * This file is a part of the ARMOISE language library
. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file armoise/armoise-interp.h
 * \brief Interpreter of ARMOISE syntactic trees
 *
 * This module proposes two functions that allows to interpret the syntactic 
 * trees of ARMOISE definitions or predicates into data structures defined
 * in armoise.h.
 *
 * Each function may raise an \ref armoise_interp_exception if the \ref 
 * armoise_tree is inconsistent with some parts of the static semantics 
 * of ARMOISE.
 */
#ifndef __ARMOISE_INTERP_H__
# define __ARMOISE_INTERP_H__

# include <ccl/ccl-exception.h>
# include <armoise/armoise-tree.h>
# include <armoise/armoise.h>

BEGIN_C_DECLS

/*!
 * \brief Exception raised on static semantics error.
 */
CCL_DECLARE_EXCEPTION (armoise_interp_exception, exception);

/*!
 * \brief Interpretation of the ARMOISE definition \a tdef and integration to
 * the context \a context. 
 *
 * This function interprets the syntactic tree \a tdef as a definition or 
 * parallel definition of predicates. Each definition is interpreted under
 * \a context (and its parents). Each newly created predicate is added to 
 * \a context. If \a ids is not null then identifiers of predicates are stored
 * in this list.
 *
 * \param context the context in which the syntactic tree is interpreted.
 * \param tdef the syntactic tree of the definition
 * \param ids the list that receives identifiers of created predicates
 *
 * \pre context != NULL 
 * \pre tdef != NULL
 * \pre tdef->node_type == ARMOISE_TREE_DEFINITION
 *
 * \throw armoise_interp_exception if the syntactic tree is not consistent with
 *        the static semantics of the language.
 */
extern void
armoise_interp_definition (armoise_context *context, armoise_tree *tdef,
			   ccl_list *ids)
  CCL_THROW (armoise_interp_exception);

/*!
 * \brief Interpretation of the ARMOISE predicate \a P under the context \a 
 * context.
 *
 * This function interprets the syntactic tree \a P as a predicate and return
 * it into a \ref ccl_tree. \a P is interpreted under \ref context (and its 
 * parents. 
 *
 * The function returns a \ref ccl_tree whose leaves are 
 * \ref{armoise_predicate}s. The structure of the ccl_tree corresponds to 
 * the structure of the parallel definition (using <.> construction) of the
 * predicate (s).
 *
 * \param context the context in which the syntactic tree is interpreted.
 * \param P the syntactic tree of the predicate(s)
 *
 * \pre P != NULL
 * \pre P->node_type == ARMOISE_TREE_PREDICATE
 * 
 * \throw armoise_interp_exception if the syntactic tree is not consistent with
 *        the static semantics of the language.
 *
 * \return a \ref ccl_tree encoding the predicate P.
 */
extern ccl_tree *
armoise_interp_predicate (armoise_context *context, armoise_tree *P)
  CCL_THROW (armoise_interp_exception);

END_C_DECLS

#endif /* ! __ARMOISE_INTERP_H__ */
