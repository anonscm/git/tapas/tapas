/*
 * armoise-interp.c -- add a comment about this file
 * 
 * This file is a part of the ARMOISE language library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file armoise-interp.c
 * \brief Interpreter of ARMOISE syntactic trees
 */
#include <ccl/ccl-assert.h>
#include <ccl/ccl-list.h>
#include "armoise-tree-p.h"
#include "armoise-p.h"
#include "armoise-interp-p.h"

#define non_null(_result) \
  (((_result)==NULL)?(ccl_rethrow(),NULL):(_result))

CCL_DEFINE_EXCEPTION (armoise_interp_exception, exception);

static ccl_tree *
s_interp_predicate_tuple (armoise_context *ctx, armoise_tree *Pt);

static void
s_unify_identifier_and_predicates (armoise_context *ctx, armoise_tree *ids,
				   ccl_tree *predicates, ccl_list *nids);

static armoise_predicate *
s_interp_predicate (armoise_context *ctx, armoise_context *vctx,
		    armoise_tree *Pt);
				   
			/* --------------- */

void
armoise_interp_definition (armoise_context *ctx, armoise_tree *tdef,
			   ccl_list *ids)
{
  ccl_tree *predicates = NULL;

  ccl_pre (ctx != NULL);
  ccl_pre (IS_LABELLED_WITH (tdef, DEFINITION));

  predicates = armoise_interp_predicate (ctx, tdef->child->next);      
  ccl_try (armoise_interp_exception) 
    {
      s_unify_identifier_and_predicates (ctx, tdef->child, predicates, ids);
    } 
  ccl_catch 
    {
      ccl_tree_del_reference (predicates);
      ccl_rethrow ();
    } 
  ccl_end_try;
  ccl_tree_del_reference (predicates);
}

			/* --------------- */

ccl_tree *
armoise_interp_predicate (armoise_context *ctx, armoise_tree *Pt)
{
  ccl_tree *result = NULL;
  armoise_context *lctx = armoise_context_create_for_predicates (ctx);

  ccl_pre (IS_LABELLED_WITH (Pt, PREDICATE));

  ccl_try (armoise_interp_exception)
    { 
      Pt = Pt->child;
      if (IS_LABELLED_WITH (Pt, PREDICATE_CONTEXT))
	{
	  armoise_tree *t;
	  
	  for (t = Pt->child; t; t = t->next)
	    armoise_interp_definition (lctx, t, NULL);
	  Pt = Pt->next;
	}

      result = s_interp_predicate_tuple (lctx, Pt);
    }
  ccl_no_catch;

  armoise_context_del_reference (lctx);

  return non_null (result);
}

			/* --------------- */

ccl_ustring
armoise_interp_identifier (armoise_tree *t)
{
  ccl_pre (IS_LABELLED_WITH (t, IDENTIFIER));

  return t->value.id_value;
}

			/* --------------- */

static ccl_tree *
s_interp_predicate_tuple (armoise_context *ctx, armoise_tree *Pt)
{
  ccl_tree *result = NULL;

  if (IS_LABELLED_WITH (Pt, PREDICATE_TUPLE))
    {
      result = ccl_tree_create (NULL, CCL_NO_DELETE_PROC);

      ccl_try (armoise_interp_exception)
        {
	  for (Pt = Pt->child; Pt; Pt = Pt->next)
	    {
	      ccl_tree *t = s_interp_predicate_tuple (ctx, Pt);
	      ccl_tree_add_child (result, t);
	      ccl_tree_del_reference (t);
	    }
	}
      ccl_catch
	{
	  ccl_tree_del_reference (result);
	  ccl_rethrow ();
	}
      ccl_end_try;
    }
  else
    {      
      armoise_predicate *P = s_interp_predicate (ctx, NULL, Pt);
          
      armoise_predicate_compute_domain (P);
      result = ccl_tree_create (P, (ccl_delete_proc *) 
				armoise_predicate_del_reference);
    }

  return result;
}

			/* --------------- */

static void
s_unify_identifier_and_predicates (armoise_context *ctx, armoise_tree *ids,
				   ccl_tree *predicates, ccl_list *nids)
{
  ccl_pre (IS_LABELLED_WITH (ids, IDENTIFIER) || 
	   IS_LABELLED_WITH (ids, ID_TUPLE));

  if (IS_LABELLED_WITH (ids, IDENTIFIER))
    {
      if (armoise_context_has_locally (ctx, ids->value.id_value))
	{
	  ccl_error ("%s:%d: error: redefinition of predicate '%s'.\n", 
		     ids->filename, ids->line, ids->value.id_value);
	  ccl_throw_no_msg (armoise_interp_exception);
	}
      else
	{
	  if (armoise_context_has (ctx, ids->value.id_value))
	    ccl_warning ("%s:%d: error: local predicate '%s' shadows another "
			 "predicate.\n", 
			 ids->filename, ids->line, ids->value.id_value);
	  armoise_context_add_predicate_tree (ctx, ids->value.id_value, 
					      predicates);
	  if (nids != NULL)
	    {
	      ccl_assert (! ccl_list_has (nids, ids->value.id_value));
	      ccl_list_add (nids, ids->value.id_value);
	    }
	}
    }
  else
    {
      int nb_ids, nb_predicates;

      ccl_assert (IS_LABELLED_WITH (ids, ID_TUPLE));

      nb_ids = ccl_parse_tree_count_siblings (ids->child);
      nb_predicates = ccl_tree_get_nb_childs (predicates);
	
      if (nb_ids != nb_predicates)
	{
	  ccl_error ("%s:%d: error: wrong size in vector assignment (%d against"
		     " %d)\n", ids->filename, ids->line, nb_ids, nb_predicates);
	  ccl_throw_no_msg (armoise_interp_exception);
	}

      for (ids = ids->child, predicates = predicates->childs; 
	   ids; ids = ids->next, predicates = predicates->next)
	s_unify_identifier_and_predicates (ctx, ids, predicates, nids);
    }
}

			/* --------------- */

static armoise_predicate *
s_interp_list_element_predicate (armoise_context *ctx, armoise_tree *Pt);

static armoise_predicate *
s_interp_cartesian_product (armoise_context *ctx, armoise_context *vctx, 
			    armoise_tree *Pt);

static armoise_predicate *
s_interp_power (armoise_context *ctx, armoise_context *vctx, armoise_tree *Pt);

static armoise_predicate *
s_interp_enumerated_set (armoise_context *ctx, armoise_context *vctx, 
			 armoise_tree *Pt);

static armoise_predicate *
s_interp_set (armoise_context *ctx, armoise_context *vctx, armoise_tree *Pt);

static armoise_term *
s_interp_term (armoise_context *vctx, armoise_tree *Tt);

static armoise_formula *
s_interp_formula (armoise_context *ctx, armoise_context *vctx, 
		  armoise_tree *Ft);

static armoise_predicate *
s_interp_predicate (armoise_context *ctx, armoise_context *vctx, 
		    armoise_tree *Pt)	    
{
  armoise_predicate *op[2] = { NULL, NULL };
  armoise_predicate *result = NULL;

  ccl_try (armoise_interp_exception)
    {
      switch (Pt->node_type)
	{
	case ARMOISE_TREE_NATURALS:
	  result = armoise_predicate_create_naturals (ctx);
	  break;

	case ARMOISE_TREE_INTEGERS:
	  result = armoise_predicate_create_integers (ctx);
	  break;

	case ARMOISE_TREE_POSITIVES:
	  result = armoise_predicate_create_positives (ctx);
	  break;

	case ARMOISE_TREE_REALS:
	  result = armoise_predicate_create_reals (ctx);
	  break;

	case ARMOISE_TREE_IDENTIFIER:
	case ARMOISE_TREE_LIST_ELEMENT:
	  result = s_interp_list_element_predicate (ctx, Pt);
	  break;

	case ARMOISE_TREE_UNION:
	case ARMOISE_TREE_DIFFERENCE:
	case ARMOISE_TREE_XOR:
	case ARMOISE_TREE_INTERSECTION:
	case ARMOISE_TREE_PLUS:
	case ARMOISE_TREE_MINUS:
	case ARMOISE_TREE_MUL:
	case ARMOISE_TREE_DIV:
	case ARMOISE_TREE_MOD:
	  op[1] = s_interp_predicate (ctx, vctx, Pt->child->next);
	case ARMOISE_TREE_COMPLEMENT:
	case ARMOISE_TREE_NEG:
	  op[0] = s_interp_predicate (ctx, vctx, Pt->child);

	  switch (Pt->node_type)
	    {
	    case ARMOISE_TREE_UNION:
	      result = armoise_predicate_create_union (ctx, op[0], op[1]);
	      break;
	    case ARMOISE_TREE_DIFFERENCE:
	      result = armoise_predicate_create_diff (ctx, op[0], op[1]);
	      break;
	    case ARMOISE_TREE_XOR:
	      result = armoise_predicate_create_delta (ctx, op[0], op[1]);
	      break;
	    case ARMOISE_TREE_INTERSECTION:
	      result = 
		armoise_predicate_create_intersection (ctx, op[0], op[1]);
	      break;
	    case ARMOISE_TREE_PLUS:
	      result = armoise_predicate_create_add (ctx, op[0], op[1]);
	      break;
	    case ARMOISE_TREE_MINUS:
	      result = armoise_predicate_create_sub (ctx, op[0], op[1]);
	      break;
	    case ARMOISE_TREE_MUL:	      
	      result = armoise_predicate_create_mul (ctx, op[0], op[1]);
	      break;
	    case ARMOISE_TREE_DIV:
	      result = armoise_predicate_create_div (ctx, op[0], op[1]);
	      break;
	    case ARMOISE_TREE_MOD:
	      result = armoise_predicate_create_mod (ctx, op[0], op[1]);
	      break;
	    case ARMOISE_TREE_COMPLEMENT:
	      result = armoise_predicate_create_complement (ctx, op[0]);
	      break;
	    case ARMOISE_TREE_NEG:
	      result = armoise_predicate_create_neg (ctx, op[0]);
	      break;
	    default:
	      INVALID_NODE ();
	      break;
	    }
	  break;

	case ARMOISE_TREE_CARTESIAN_PRODUCT:
	  result = s_interp_cartesian_product (ctx, vctx, Pt);
	  break;

	case ARMOISE_TREE_POWER:
	  result = s_interp_power (ctx, vctx, Pt);
	  break;

	case ARMOISE_TREE_INTEGER:
	  result = armoise_predicate_create_integer (ctx, Pt->value.int_value);
	  break;

	case ARMOISE_TREE_ENUMERATED_SET:
	  result = s_interp_enumerated_set (ctx, vctx, Pt);
	  break;

	case ARMOISE_TREE_SET:
	  result = s_interp_set (ctx, vctx, Pt);
	  break;

	case ARMOISE_TREE_PAREN:
	  op[0] = s_interp_predicate (ctx, vctx, Pt->child);
	  result = armoise_predicate_create_paren (ctx, op[0]);
	  break;

	case ARMOISE_TREE_EMPTY:
	default:
	  INVALID_NODE ();
	}
    }
  ccl_no_catch;
    
  ccl_zdelete (armoise_predicate_del_reference, op[0]);
  ccl_zdelete (armoise_predicate_del_reference, op[1]);

  return non_null (result);
}

			/* --------------- */

static void
s_interp_generic_list_element_predicate (armoise_tree *tree, ccl_ustring *p_id,
					 int *p_nb_indices, int **p_indices);

static armoise_predicate *
s_interp_list_element_predicate (armoise_context *ctx, armoise_tree *Pt)
{
  int *indices;
  int nb_indices;
  armoise_predicate *result = NULL;
  ccl_ustring id;
  ccl_tree *t;

  s_interp_generic_list_element_predicate (Pt, &id, &nb_indices, &indices);
  t = armoise_context_get_predicate_tree (ctx, id);
  ccl_try (armoise_interp_exception)
    {
      int i;
      ccl_tree *aux;

      if (t == NULL)
	{
	  ccl_error ("%s:%d: error: undefined predicate symbol '%s'.\n",
		     Pt->filename, Pt->line, id);
	  ccl_throw_no_msg (armoise_interp_exception);
	}

      i = 0;
      aux = ccl_tree_add_reference (t);
      while (! ccl_tree_is_leaf(aux) && i < nb_indices)
	{
	  ccl_tree *tmp;

	  if (indices[i] >= ccl_tree_get_nb_childs (aux))
	    {
	      ccl_tree_del_reference (aux);
	      ccl_error ("%s:%d: error: array index (%d) out of bounds.\n",
			 Pt->filename, Pt->line, indices[i]);
	      ccl_throw_no_msg (armoise_interp_exception);
	    }
	  tmp = ccl_tree_get_child (aux, indices[i]);
	  ccl_tree_del_reference (aux);
	  aux = tmp;
	  i++;
	}      

      if (! ccl_tree_is_leaf(aux) || i < nb_indices)
	{
	  ccl_error ("%s:%d: error: ", Pt->filename, Pt->line);

	  if (! ccl_tree_is_leaf(aux))
	    {
	      if (indices == NULL)
		ccl_error ("vectorial predicates are not allowed as terms.\n");
	      else
		ccl_error ("array index (%d) out of bounds.\n", indices[i]);
	    }
	  else 
	    {
	      ccl_error ("array index (%d) used with a not vectorial "
			 "definition '%s'.\n", indices[i], id);
	    }
	  ccl_tree_del_reference (aux);
	  ccl_throw_no_msg (armoise_interp_exception);
	}
      ccl_tree_del_reference (aux);
      result = armoise_predicate_create_indirection (ctx, id, nb_indices, 
						     indices);
    }
  ccl_no_catch;

  ccl_zdelete (ccl_delete, indices);
  ccl_zdelete (ccl_tree_del_reference, t);

  return non_null (result);
}

			/* --------------- */

static void
s_interp_generic_list_element_predicate (armoise_tree *tree, ccl_ustring *p_id, 
					 int *p_nb_indices, int **p_indices)
{
  int *indices;
  armoise_tree *t = tree;
  int nb_indices = 0;

  for (t = tree; ! IS_LABELLED_WITH (t, IDENTIFIER); t = t->child)
    nb_indices++;

  *p_id = t->value.id_value;

  if (nb_indices > 0)
    indices = ccl_new_array (int, nb_indices);
  else 
    indices = NULL;
  *p_nb_indices = nb_indices;
  *p_indices = indices;

  for (t = tree, nb_indices--; ! IS_LABELLED_WITH (t, IDENTIFIER); 
       t = t->child, nb_indices--)
    {
      ccl_assert (IS_LABELLED_WITH (t->child->next, INTEGER));
      indices[nb_indices] = t->child->next->value.int_value;
    }
}

			/* --------------- */

static armoise_predicate *
s_interp_cartesian_product (armoise_context *ctx, armoise_context *vctx, 
			    armoise_tree *Pt)
{
  int i;
  int nb_components = ccl_parse_tree_count_siblings (Pt->child);
  armoise_predicate **predicates = 
    ccl_new_array (armoise_predicate *, nb_components);
  armoise_predicate *result = NULL;

  ccl_try (armoise_interp_exception)
    {      
      for (Pt = Pt->child, i = 0; i < nb_components; i++, Pt = Pt->next)
	predicates[i] = s_interp_predicate (ctx, vctx, Pt);
      result = 
	armoise_predicate_create_product (ctx, nb_components, predicates);
    }
  ccl_no_catch;

  for (i = 0; i < nb_components; i++)
    ccl_zdelete (armoise_predicate_del_reference, predicates[i]);
  ccl_delete (predicates);

  return non_null (result);
}

			/* --------------- */

static armoise_predicate *
s_interp_power (armoise_context *ctx, armoise_context *vctx, armoise_tree *Pt)
{
  armoise_predicate *result = NULL;

  ccl_try (armoise_interp_exception)
    {      
      int i;
      armoise_predicate *P = s_interp_predicate (ctx, vctx, Pt->child);
      int nb_components = Pt->child->next->value.int_value;
      armoise_predicate **predicates = 
	ccl_new_array (armoise_predicate *, nb_components);

      for (i = 0; i < nb_components; i++) 
	predicates[i] = P;
      result = 
	armoise_predicate_create_product (ctx, nb_components, predicates);
      armoise_predicate_del_reference (P);
      ccl_delete (predicates);
    }
  ccl_no_catch;

  return non_null (result);
}

			/* --------------- */

static armoise_predicate *
s_interp_enumerated_set (armoise_context *ctx, armoise_context *vctx, 
			 armoise_tree *Pt)
{
  int i;
  int nb_values = ccl_parse_tree_count_siblings (Pt->child);
  armoise_term **values = ccl_new_array (armoise_term *, nb_values);
  armoise_predicate *result = NULL;

  ccl_try (armoise_interp_exception)
    {
      armoise_tree *t = Pt->child;

      values[0] = s_interp_term (vctx, t);

      for (i = 1, t = t->next; i < nb_values; i++, t = t->next)
	values[i] = s_interp_term (vctx, t);
      result = armoise_predicate_create_finite_set (ctx, nb_values, values);
    }
  ccl_no_catch;

  for (i = 0; i < nb_values; i++)
    ccl_zdelete (armoise_term_del_reference, values[i]);
  ccl_delete (values);

  return non_null (result);
}

			/* --------------- */

static armoise_term *
s_interp_typed_variables (armoise_context *vctx, armoise_tree *Pt, 
			  armoise_type *type, const armoise_domain *dom, 
			  ccl_list *varnames)
{
  armoise_term *result = NULL;

  if (IS_LABELLED_WITH (Pt, IDENTIFIER))
    {
      ccl_ustring varname = Pt->value.id_value;

      if (ccl_list_has (varnames, varname))
	{
	  ccl_error ("%s:%d: error: variable '%s' is declared twice here.\n",
		     Pt->filename, Pt->line, varname);
	  ccl_throw_no_msg (armoise_interp_exception);	  
	}
      else
	{
	  armoise_variable *var = armoise_variable_create (varname, type);

	  if (armoise_context_has_variable (vctx, varname))
	    {
	      ccl_warning ("%s:%d: warning: variable '%s' shadows another "
			   "variable with the same name.\n", Pt->filename, 
			   Pt->line, varname);
	    }

	  ccl_list_add (varnames, varname);
	  armoise_variable_set_domain (var, dom);
	  armoise_context_add_variable (vctx, var);
	  result = armoise_term_create_variable (var);
	  armoise_variable_del_reference (var);
	}
    }
  else if (IS_LABELLED_WITH (Pt, VECTOR))
    {
      int i;
      int nb_components = ccl_parse_tree_count_siblings (Pt->child);
      armoise_term **components = ccl_new_array (armoise_term *, nb_components);
      
      ccl_try (armoise_interp_exception)
        {
	  armoise_tree *t = Pt->child;

	  if (nb_components != armoise_type_get_width (type))
	    {
	      ccl_error ("%s:%d: error: wrong vector size in prototype of the "
			 "set.\n", Pt->filename, Pt->line);
	      ccl_throw_no_msg (armoise_interp_exception);
	    }

	  for (i = 0; i < nb_components; i++, t = t->next)
	    {
	      armoise_type *subtype = *armoise_type_get_subtype_ref (type, i);
	      const armoise_domain *subdom = 
		armoise_domain_get_component (dom, i);
	      components[i] = 
		s_interp_typed_variables (vctx, t, subtype, subdom, varnames);
	    }
	  result = armoise_term_create_vector (nb_components, components);
	}
      ccl_no_catch;

      for (i = 0; i < nb_components; i++)
	ccl_zdelete (armoise_term_del_reference, components[i]);
      ccl_delete (components);
    }
  else
    {
      ccl_error ("%s:%d: error: invalid prototype in set definition.\n",
		 Pt->filename, Pt->line);
      ccl_throw_no_msg (armoise_interp_exception);
    }

  return non_null (result);
}

			/* --------------- */

static void
s_interp_variables_declaration (armoise_context *ctx, armoise_context *vctx, 
				armoise_tree *decl, armoise_term **p_vars, 
				armoise_predicate **p_dom)
{
  armoise_type *type = NULL;
  armoise_domain *dom = NULL;
  ccl_list *varnames = ccl_list_create ();
  armoise_predicate *P = NULL;
  armoise_term *vars = NULL;

  ccl_try (armoise_interp_exception)
    {     
      ccl_exception_local_variable (armoise_type_del_reference, &type);
      ccl_exception_local_variable (armoise_domain_del_reference, &dom);
      ccl_exception_local_variable (armoise_predicate_del_reference, &P);
      ccl_exception_local_variable (armoise_term_del_reference, &vars);
      ccl_exception_local_variable (ccl_list_delete, &varnames);

      P = s_interp_predicate (ctx, vctx, decl->child->next);
      type = armoise_predicate_get_type (P);
      dom = armoise_predicate_get_domain (P);
      vars = s_interp_typed_variables (vctx, decl->child, type, dom, varnames);
      *p_dom = P; 
      P = NULL;
      *p_vars = vars;
      vars = NULL;
    }
  ccl_catch_rethrow;
}

			/* --------------- */

static armoise_predicate *
s_interp_set (armoise_context *ctx, armoise_context *vctx, armoise_tree *Pt)
{
  armoise_predicate *P = NULL;
  armoise_term *proto = NULL;
  armoise_formula *def = NULL;
  armoise_predicate *result = NULL;
  armoise_context *lvctx = armoise_context_create_for_variables (vctx);
  armoise_context *lctx = armoise_context_create_for_predicates (ctx);

  ccl_assert (IS_LABELLED_WITH (Pt->child, IN));

  ccl_try (armoise_interp_exception)
    {
      s_interp_variables_declaration (lctx, lvctx, Pt->child, &proto, &P);
      def = s_interp_formula (lctx, lvctx, Pt->child->next);      
      result = armoise_predicate_create_simple_set (ctx, proto, P, def);
    }
  ccl_no_catch;

  ccl_zdelete (armoise_term_del_reference, proto);
  ccl_zdelete (armoise_predicate_del_reference, P);
  ccl_zdelete (armoise_formula_del_reference, def);
  armoise_context_del_reference (lvctx);
  armoise_context_del_reference (lctx);

  return non_null (result);
}

			/* --------------- */

static armoise_formula *
s_interp_quantifier (armoise_context *ctx, armoise_context *vctx, 
		     armoise_tree *Ft);

static int
s_terms_have_same_type (armoise_term *t1, armoise_term *t2)
{
  armoise_type *type1 = armoise_term_get_type (t1);
  armoise_type *type2 = armoise_term_get_type (t2);
  int result = armoise_type_equals (type1, type2);

  armoise_type_del_reference (type1);
  armoise_type_del_reference (type2);

  return result;
}

			/* --------------- */

static void
s_check_terms_have_same_type (armoise_tree *loc, armoise_term *t1, 
			      armoise_term *t2)
{
  if (s_terms_have_same_type (t1, t2))
    return;

  ccl_error ("%s:%d: error: terms '", loc->filename, loc->line);
  armoise_term_log (CCL_LOG_ERROR, t1);
  ccl_error ("' and '");
  armoise_term_log (CCL_LOG_ERROR, t2);
  ccl_error ("' differ in their types.\n");
  ccl_throw_no_msg (armoise_interp_exception);
}

			/* --------------- */

static int
s_term_and_predicate_have_same_type (armoise_term *t1, armoise_predicate *P)
{
  armoise_type *type1 = armoise_term_get_type (t1);
  armoise_type *type2 = armoise_predicate_get_type (P);
  int result = armoise_type_equals (type1, type2);

  armoise_type_del_reference (type1);
  armoise_type_del_reference (type2);

  return result;
}

			/* --------------- */

static armoise_formula *
s_interp_formula (armoise_context *ctx, armoise_context *vctx, armoise_tree *Ft)
{
  armoise_predicate *P = NULL;
  armoise_formula *f_op[2] = { NULL, NULL };
  armoise_term *t_op[2] = { NULL, NULL };
  armoise_formula *result = NULL;

  ccl_try (armoise_interp_exception)
    {
      switch (Ft->node_type)
	{
	case ARMOISE_TREE_OR:
	case ARMOISE_TREE_AND:
	case ARMOISE_TREE_EQUIV:
	case ARMOISE_TREE_IMPLY:
	  f_op[1] = s_interp_formula (ctx, vctx, Ft->child->next);
	case ARMOISE_TREE_NOT:
	  f_op[0] = s_interp_formula (ctx, vctx, Ft->child);
	  switch (Ft->node_type)
	    {
	    case ARMOISE_TREE_OR:
	      result = armoise_formula_create_or (f_op[0], f_op[1]);
	      break;
	    case ARMOISE_TREE_AND:
	      result = armoise_formula_create_and (f_op[0], f_op[1]);
	      break;
	    case ARMOISE_TREE_EQUIV:
	      result = armoise_formula_create_equiv (f_op[0], f_op[1]);
	      break;
	    case ARMOISE_TREE_IMPLY:
	      result = armoise_formula_create_imply (f_op[0], f_op[1]);
	      break;
	    case ARMOISE_TREE_NOT:
	      result = armoise_formula_create_not (f_op[0]);
	      break;
	    }
	  break;

	case ARMOISE_TREE_EQ:
	case ARMOISE_TREE_NEQ:
	case ARMOISE_TREE_LEQ:
	case ARMOISE_TREE_GEQ:
	case ARMOISE_TREE_LT:
	case ARMOISE_TREE_GT:
	  t_op[0] = s_interp_term (vctx, Ft->child);
	  t_op[1] = s_interp_term (vctx, Ft->child->next);
	  
	  s_check_terms_have_same_type (Ft, t_op[0], t_op[1]);

	  switch (Ft->node_type)
	    {
	    case ARMOISE_TREE_EQ:
	      result = armoise_formula_create_eq (t_op[0], t_op[1]);
	      break;
	    case ARMOISE_TREE_NEQ:
	      result = armoise_formula_create_neq (t_op[0], t_op[1]);
	      break;
	    case ARMOISE_TREE_LEQ:
	      result = armoise_formula_create_leq (t_op[0], t_op[1]);
	      break;
	    case ARMOISE_TREE_GEQ:
	      result = armoise_formula_create_geq (t_op[0], t_op[1]);
	      break;
	    case ARMOISE_TREE_LT:
	      result = armoise_formula_create_lt (t_op[0], t_op[1]);
	      break;
	    case ARMOISE_TREE_GT:
	      result = armoise_formula_create_gt (t_op[0], t_op[1]);
	      break;
	    }
	  break;

	case ARMOISE_TREE_PAREN:
	  f_op[0] = s_interp_formula (ctx, vctx, Ft->child);
	  result = armoise_formula_create_paren (f_op[0]);
	  break;

	case ARMOISE_TREE_TRUE: 
	case ARMOISE_TREE_FALSE:
	  result = 
	    armoise_formula_create_constant (IS_LABELLED_WITH (Ft, TRUE));
	  break;

	case ARMOISE_TREE_IN:
	case ARMOISE_TREE_CALL:
	  {
	    int error = 0;
	    armoise_context *lvctx = 
	      armoise_context_create_for_variables (NULL);
	    ccl_try (exception)
	      {
		if (IS_LABELLED_WITH (Ft, IN))
		  {
		    t_op[0] = s_interp_term (vctx, Ft->child);
		    P = s_interp_predicate (ctx, lvctx, Ft->child->next);
		  }
		else
		  {
		    t_op[0] = s_interp_term (vctx, Ft->child->next);
		    P = s_interp_predicate (ctx, lvctx, Ft->child);
		  }

		if (! s_term_and_predicate_have_same_type (t_op[0], P))
		  {
		    ccl_error ("%s:%d: error: term '", Ft->filename, Ft->line);
		    armoise_term_log (CCL_LOG_ERROR, t_op[0]);
		    ccl_error ("' and predicate '");
		    armoise_predicate_log (CCL_LOG_ERROR, P);
		    ccl_error ("' haven't the same type.\n");
		    ccl_throw_no_msg (armoise_interp_exception);
		  }
		result = armoise_formula_create_in (t_op[0], P);
	      }
	    ccl_catch
	      {
		error = 1;
	      }
	    ccl_end_try;
	    armoise_context_del_reference (lvctx);
	    if (error)
	      ccl_rethrow ();
	  }
	  break;

	case ARMOISE_TREE_FORALL:
	case ARMOISE_TREE_EXISTS:
	  result = s_interp_quantifier (ctx, vctx, Ft);
	  break;
	}
    }
  ccl_no_catch;

  ccl_zdelete (armoise_predicate_del_reference, P);

  ccl_zdelete (armoise_formula_del_reference, f_op[0]);
  ccl_zdelete (armoise_formula_del_reference, f_op[1]);

  ccl_zdelete (armoise_term_del_reference, t_op[0]);
  ccl_zdelete (armoise_term_del_reference, t_op[1]);

  return non_null (result);
}

			/* --------------- */

static armoise_term *
s_interp_vector (armoise_context *vctx, armoise_tree *Tt);

			/* --------------- */

static int
s_term_is_scalar (armoise_term *t)
{
  armoise_type *type = armoise_term_get_type (t);
  int result = armoise_type_is_scalar (type);
  armoise_type_del_reference (type);

  return result;
}

			/* --------------- */

static void
s_check_one_term_is_scalar (armoise_tree *loc, armoise_term *t1, 
			    armoise_term *t2)
{
  if (s_term_is_scalar (t1) || s_term_is_scalar (t2))
    return;

  ccl_error ("%s:%d: error: neither '", loc->filename, loc->line);
  armoise_term_log (CCL_LOG_ERROR, t1);
  ccl_error ("' nor '");
  armoise_term_log (CCL_LOG_ERROR, t2);
  ccl_error ("' is a scalar term for multiplication.\n");
  ccl_throw_no_msg (armoise_interp_exception);
}

			/* --------------- */

static armoise_term *
s_interp_term (armoise_context *vctx, armoise_tree *Tt)
{
  armoise_term *op[2] = { NULL, NULL };
  armoise_term *result = NULL;

  ccl_try (armoise_interp_exception)
    {
      switch (Tt->node_type)
	{
	case ARMOISE_TREE_PLUS:
	case ARMOISE_TREE_MINUS:
	case ARMOISE_TREE_MUL:
	case ARMOISE_TREE_DIV:
	case ARMOISE_TREE_MOD:
	  op[1] = s_interp_term (vctx, Tt->child->next);
	case ARMOISE_TREE_NEG:
	  op[0] = s_interp_term (vctx, Tt->child);

	  switch (Tt->node_type)
	    {
	    case ARMOISE_TREE_PLUS:
	      s_check_terms_have_same_type (Tt, op[0], op[1]); 
	      result = armoise_term_create_add (op[0], op[1]);
	      break;
	    case ARMOISE_TREE_MINUS:
	      s_check_terms_have_same_type (Tt, op[0], op[1]); 
	      result = armoise_term_create_sub (op[0], op[1]);
	      break;
	    case ARMOISE_TREE_MUL:
	      s_check_one_term_is_scalar (Tt, op[0], op[1]); 
	      result = armoise_term_create_mul (op[0], op[1]); 
	      break;
	    case ARMOISE_TREE_DIV:
	    case ARMOISE_TREE_MOD:
	      if (!s_term_is_scalar (op[1]))
		{
		  ccl_error ("%s:%d: error: neither '", Tt->filename, Tt->line);
		  armoise_term_log (CCL_LOG_ERROR, op[1]);
		  ccl_error ("is not a scalar term.\n");
		  ccl_throw_no_msg (armoise_interp_exception);
		}
	      if (Tt->node_type == ARMOISE_TREE_DIV)
		result = armoise_term_create_div (op[0], op[1]);
	      else 
		result = armoise_term_create_mod (op[0], op[1]);
	      break;
	    case ARMOISE_TREE_NEG:
	      result = armoise_term_create_neg (op[0]);
	      break;
	    }
	  break;

	case ARMOISE_TREE_INTEGER:
	  result = armoise_term_create_constant (Tt->value.int_value, 1);
	  break;
	case ARMOISE_TREE_VECTOR:
	  result = s_interp_vector (vctx, Tt);
	  break;

	case ARMOISE_TREE_IDENTIFIER:
	  if (! armoise_context_has_variable (vctx, Tt->value.id_value))
	    {
	      ccl_error ("%s:%d: error: undefined variable '%s'.\n",
			 Tt->filename, Tt->line, Tt->value.id_value);
	      ccl_throw_no_msg (armoise_interp_exception);
	    }
	  else
	    {
	      armoise_variable *var = 
		armoise_context_get_variable (vctx, Tt->value.id_value);
	      result = armoise_term_create_variable (var);
	      armoise_variable_del_reference (var);
	    }
	  break;
#if 0
	default:
	  abort();
#else
	case ARMOISE_TREE_LIST_ELEMENT:
	  {
	    armoise_type *type;
	    int i = Tt->child->next->value.int_value;

	    op[0] = s_interp_term (vctx, Tt->child);
	    type = armoise_term_get_type (op[0]);

	    if (!(0 <= i && i < armoise_type_get_width (type)))
	      {
		armoise_type_del_reference (type);
		ccl_error ("%s:%d: error: vector index (%d) out of bounds.\n",
			   Tt->filename, Tt->line, i);
		ccl_throw_no_msg (armoise_interp_exception);
	      }

	    armoise_type_del_reference (type);
	    result = armoise_term_create_vector_element (op[0], i);
	  }
	  break;
#endif
	}
    }
  ccl_no_catch;
  
  ccl_zdelete (armoise_term_del_reference, op[0]);
  ccl_zdelete (armoise_term_del_reference, op[1]);

  return non_null (result);
}

			/* --------------- */

static armoise_formula *
s_interp_quantifier (armoise_context *ctx, armoise_context *vctx, 
		     armoise_tree *Ft)
{
  armoise_term *qvars = NULL;
  armoise_predicate *domain = NULL;
  armoise_formula *qF = NULL;
  armoise_formula *result = NULL;
  armoise_context *lvctx = armoise_context_create_for_variables (vctx);

  ccl_try (armoise_interp_exception)
    {
      s_interp_variables_declaration (ctx, lvctx, Ft->child->next, &qvars, 
				      &domain);
      qF = s_interp_formula (ctx, lvctx, Ft->child);
      if (Ft->node_type == ARMOISE_TREE_FORALL)
	result = armoise_formula_create_forall (qvars, domain, qF);
      else
	result = armoise_formula_create_exists (qvars, domain, qF); 
    }
  ccl_no_catch;

  ccl_zdelete (armoise_term_del_reference, qvars);
  ccl_zdelete (armoise_predicate_del_reference, domain);
  ccl_zdelete (armoise_formula_del_reference, qF);
  armoise_context_del_reference (lvctx);

  return non_null (result);
}

			/* --------------- */

static armoise_term *
s_interp_vector (armoise_context *vctx, armoise_tree *Tt)
{
  int i;
  armoise_tree *tc;
  int nb_components = ccl_parse_tree_count_siblings (Tt->child);
  armoise_term **components = ccl_new_array (armoise_term *, nb_components);
  armoise_term *result = NULL;

  ccl_try (armoise_interp_exception)
    {
      for (tc = Tt->child, i =0;  tc; tc = tc->next, i++)
	components[i] = s_interp_term (vctx, tc);
      result = armoise_term_create_vector (nb_components, components);
    }
  ccl_no_catch;

  for (i = 0; i < nb_components; i++)
    ccl_zdelete (armoise_term_del_reference, components[i]);
  ccl_delete (components);

  return non_null (result);
}
