/*
 * armoise-domain.c -- add a comment about this file
 * 
 * This file is a part of the ARMOISE language library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include "armoise-p.h"


struct armoise_domain_st 
{
  int refcount;  
  int width;
  union
  {
    armoise_domain_attr dom;
    armoise_domain **subdoms;
  } c;
};

			/* --------------- */

const armoise_domain_attr ARMOISE_DOMAIN_ATTR_FOR_REALS =
  (ARMOISE_DOMAIN_HAS_POSITIVES | ARMOISE_DOMAIN_HAS_NEGATIVES);

			/* --------------- */

const armoise_domain_attr ARMOISE_DOMAIN_ATTR_FOR_POSITIVES =
  ARMOISE_DOMAIN_HAS_POSITIVES;

			/* --------------- */

const armoise_domain_attr ARMOISE_DOMAIN_ATTR_FOR_INTEGERS = 
  (ARMOISE_DOMAIN_HAS_POSITIVES | ARMOISE_DOMAIN_HAS_NEGATIVES |
   ARMOISE_DOMAIN_IN_INTEGERS);

			/* --------------- */

const armoise_domain_attr ARMOISE_DOMAIN_ATTR_FOR_NATURALS = 
  (ARMOISE_DOMAIN_HAS_POSITIVES | ARMOISE_DOMAIN_IN_INTEGERS);


			/* --------------- */


armoise_domain *
armoise_domain_create_empty (void)
{
  armoise_domain *result = ccl_new (armoise_domain);

  result->refcount = 1;
  result->width = -1;
  result->c.dom = ARMOISE_DOMAIN_IS_FINITE;
  
  return result;
}

			/* --------------- */

armoise_domain *
armoise_domain_create_with_attr (const armoise_domain_attr attr)
{
  armoise_domain *result = ccl_new (armoise_domain);

  result->refcount = 1;
  result->width = 0;
  result->c.dom = attr;

  return result;
}

			/* --------------- */

armoise_domain *
armoise_domain_create (void)
{
  return armoise_domain_create_with_attr (ARMOISE_DOMAIN_ATTR_FOR_REALS);
}

			/* --------------- */

extern armoise_domain *
armoise_domain_create_compound_domain (int width, armoise_domain **domains)
{
  int i;
  armoise_domain *result = ccl_new (armoise_domain);

  ccl_pre (width >= 1);
  ccl_pre (domains != NULL);

  result->refcount = 1;
  result->width = width;
  result->c.subdoms = ccl_new_array (armoise_domain *, width);
  for (i = 0; i < width; i++)
    result->c.subdoms[i] = armoise_domain_add_reference (domains[i]);

  return result;
}

			/* --------------- */


armoise_domain *
armoise_domain_add_reference (armoise_domain *d)
{
  ccl_pre (d != NULL);
  
  d->refcount++;

  return d;
}

			/* --------------- */

void
armoise_domain_del_reference (armoise_domain *d)
{
  ccl_pre (d != NULL);
  ccl_pre (d->refcount > 0);

  d->refcount--;
  if (d->refcount == 0)
    {
      int i;

      if (d->width > 0)
	{
	  for (i = 0; i < d->width; i++)
	    armoise_domain_del_reference (d->c.subdoms[i]);
	  ccl_delete (d->c.subdoms);
	}
      ccl_delete (d);
    }
}

			/* --------------- */

int
armoise_domain_get_width (const armoise_domain *d)
{
  ccl_pre (d != NULL);

  return d->width;
}

			/* --------------- */

int
armoise_domain_is_empty (const armoise_domain *d)
{
  ccl_pre (d != NULL);

  return d->width == -1;
}

			/* --------------- */

const armoise_domain *
armoise_domain_get_component (const armoise_domain *d, int i)
{
  ccl_pre (d != NULL);
  ccl_pre (i < d->width);

  return d->c.subdoms[i];
}

			/* --------------- */

void
armoise_domain_set_component (armoise_domain *d, int i,
			      const armoise_domain *di)
{
  ccl_pre (d != NULL);
  ccl_pre (d->width >= 1);
  ccl_pre (i < d->width);

  armoise_domain_del_reference (d->c.subdoms[i]);
  d->c.subdoms[i] = armoise_domain_duplicate (di);
}

			/* --------------- */

int
armoise_domain_is_finite (const armoise_domain *d)
{
  int result;

  ccl_pre (d != NULL);

  if (d->width > 0)

    {
      int i;

      result = 1;
      for (i = 0; result && i < d->width; i++)
	result = armoise_domain_is_finite (d->c.subdoms[i]);
    }
  else
    {
      result = (d->c.dom & ARMOISE_DOMAIN_IS_FINITE) != 0;
    }

  return result;
}

			/* --------------- */

int
armoise_domain_is_restricted_to_integers (const armoise_domain *d)
{
  ccl_pre (d != NULL);

  return d->width == 0 && ((d->c.dom & ARMOISE_DOMAIN_IN_INTEGERS) != 0);
}

			/* --------------- */

void
armoise_domain_set_attr (armoise_domain *d, 
			 const armoise_domain_attr attr)
{
  ccl_pre (d != NULL);
  ccl_pre (d->width == 0);

  d->c.dom = attr;
}

			/* --------------- */

armoise_domain_attr 
armoise_domain_get_attr (const armoise_domain *d)
{
  ccl_pre (d != NULL);
  ccl_pre (d->width == 0);

  return d->c.dom;
}

			/* --------------- */

void
armoise_domain_log (ccl_log_type log, const armoise_domain *d)
{
  if (d->width > 0)
    {
      int i;

      ccl_log (log, "(");
      armoise_domain_log (log, d->c.subdoms[0]);
      for (i = 1; i < d->width; i++)
	{
	  ccl_log (log, ", ");
	  armoise_domain_log (log, d->c.subdoms[i]);
	}
      ccl_log (log, ")");
    }
  else if (d->width == -1)
    ccl_log (log, "empty");
  else if ((d->c.dom & ARMOISE_DOMAIN_IS_FINITE) != 0)
    {
      const char *uds;
      int ud = d->c.dom & ~ARMOISE_DOMAIN_IS_FINITE;

      if (ud == ARMOISE_DOMAIN_ATTR_FOR_REALS)
	uds = "reals";
      else if (ud == ARMOISE_DOMAIN_ATTR_FOR_INTEGERS)
	uds = "int";
      else if (ud == ARMOISE_DOMAIN_ATTR_FOR_POSITIVES)
	uds = "posi";
      else if (ud == ARMOISE_DOMAIN_ATTR_FOR_NATURALS)
	uds = "nat";
      else if (d->c.dom & ~(ARMOISE_DOMAIN_HAS_POSITIVES | 
			    ARMOISE_DOMAIN_HAS_NEGATIVES))
	uds = "0";
      else
	uds = "negative int/real";
      ccl_log (log, "finite(%s)", uds);
    }  
  else
    {
      if (d->c.dom == ARMOISE_DOMAIN_ATTR_FOR_INTEGERS)
	ccl_log (log, "int");
      else if (d->c.dom == ARMOISE_DOMAIN_ATTR_FOR_NATURALS)
	ccl_log (log, "nat");
      else if (d->c.dom == ARMOISE_DOMAIN_ATTR_FOR_REALS)
	ccl_log (log, "reals");
      else if (d->c.dom == ARMOISE_DOMAIN_ATTR_FOR_POSITIVES)
	ccl_log (log, "posi");
      else 
	ccl_log (log, "negative int/real");
    }
}

			/* --------------- */

armoise_domain *
armoise_domain_duplicate (const armoise_domain *d)
{
  armoise_domain *result;

  if (armoise_domain_is_empty (d))
    result = armoise_domain_create_empty ();
  else if (armoise_domain_get_width (d) == 0)
    {
      result = armoise_domain_create ();
      *result = *d;
      result->refcount = 1;
    }
  else
    {
      int i;
      result = ccl_new (armoise_domain);

      result->refcount = 1;
      result->width = d->width;
      result->c.subdoms = ccl_new_array (armoise_domain *, d->width);
      for (i = 0; i < d->width; i++)
	result->c.subdoms[i] = armoise_domain_duplicate (d->c.subdoms[i]);
    }

  return result;
}

			/* --------------- */

int
armoise_domain_equals (const armoise_domain *d1, const armoise_domain *d2)
{
  int result;
  int i;

  if (d1->width != d2->width)
    result = 0;
  else if (d1->width >= 1)
    {
      result = 1;
      for (i = 0; result && i < d1->width; i++)
	result = armoise_domain_equals (d1->c.subdoms[i], d2->c.subdoms[i]);
    }
  else if (d1->width == 0)
    {
      result = (d1->c.dom ==d2->c.dom);		
    }

  return result;
}

			/* --------------- */

int
armoise_domain_attr_to_gendom (const armoise_domain_attr attr)
{
  int result = R_DOM;

  if ((attr & ARMOISE_DOMAIN_IN_INTEGERS))
    {
      if (!(attr & ARMOISE_DOMAIN_HAS_NEGATIVES))
	result = N_DOM;
      else 
	result = Z_DOM;	
    }
  else if (!(attr & ARMOISE_DOMAIN_HAS_NEGATIVES))
    result = P_DOM;

  return result;
}

			/* --------------- */

int
armoise_domain_get_largest_dom (const armoise_domain *dom)
{
  int result;

  if (dom->width < 0)
    result = N_DOM;
  else if (dom->width == 0)
    result = armoise_domain_attr_to_gendom (dom->c.dom);
  else
    {
      int i;
      result = armoise_domain_get_largest_dom (dom->c.subdoms[0]);
      for (i = 1; i < dom->width; i++)
	{
	  int d = armoise_domain_get_largest_dom (dom->c.subdoms[i]);
	  if (result < d)
	    result = d;
	}
    }

  return result;
}
