/*
 * armoise-term.c -- add a comment about this file
 * 
 * This file is a part of the ARMOISE language library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "armoise-p.h"

static armoise_term *
s_allocate_term (armoise_term_kind kind);

static void
s_delete_term (armoise_term *t);

static armoise_term *
s_create_n_ary_term (armoise_term_kind kind, int arity, armoise_term **t);

static armoise_term *
s_create_binary_term (armoise_term_kind kind, armoise_term *t1, 
		      armoise_term *t2);

			/* --------------- */

armoise_term *
armoise_term_add_reference (armoise_term *t)
{
  ccl_pre (t != NULL);

  t->refcount++;

  return t;
}

			/* --------------- */

void
armoise_term_del_reference (armoise_term *t)
{
  ccl_pre (t != NULL);
  ccl_pre (t->refcount > 0);

  t->refcount--;
  if (t->refcount == 0)
    s_delete_term (t);  
}

			/* --------------- */

armoise_term_kind
armoise_term_get_kind (const armoise_term *t)
{
  ccl_pre (t != NULL);

  return t->kind;
}

			/* --------------- */

int
armoise_term_get_dim (const armoise_term *t)
{
  ccl_pre (t != NULL);
  ccl_pre (t->type != NULL);

  return armoise_type_get_width (t->type);
}

			/* --------------- */

int
armoise_term_get_arity (const armoise_term *t)
{
  ccl_pre (t != NULL);

  if (t->kind == ARMOISE_T_VARIABLE || t->kind == ARMOISE_T_CONSTANT)
    return 0;

  return t->c.operands.nb_op;
}

			/* --------------- */

int
armoise_term_is_constant (const armoise_term *t)
{
  int result;

  switch (armoise_term_get_kind (t))
    {
    case ARMOISE_T_VARIABLE:
    case ARMOISE_T_ELEMENT:
      result = 0;
      break;

    case ARMOISE_T_CONSTANT:
      result = 1;
      break;

    default:
      {
	int i;
	int nb_operands = armoise_term_get_arity (t);
	result = 1;

	for (i = 0; result && i < nb_operands; i++)
	  {
	    const armoise_term *op = armoise_term_get_operand (t, i);
	    result = armoise_term_is_constant (op);
	  }
      }
      break;
    }

  return result;
}

			/* --------------- */

armoise_type *
armoise_term_get_type (const armoise_term *t)
{
  ccl_pre (t != NULL);
  ccl_pre (t->type != NULL);

  return armoise_type_add_reference (t->type);
}

			/* --------------- */

armoise_variable *
armoise_term_get_variable (const armoise_term *t)
{
  ccl_pre (armoise_term_get_kind (t) == ARMOISE_T_VARIABLE);

  return armoise_variable_add_reference (t->c.variable);
}

			/* --------------- */

void
armoise_term_get_value (const armoise_term *t, int *p_num, int *p_den)
{
  ccl_pre (armoise_term_get_kind (t) == ARMOISE_T_CONSTANT);

  *p_num = t->c.value.num;
  *p_den = t->c.value.den;
}

			/* --------------- */

armoise_term *
armoise_term_get_operand_at (armoise_term *t, int i)
{
  ccl_pre (armoise_term_get_kind (t) != ARMOISE_T_CONSTANT);
  ccl_pre (armoise_term_get_kind (t) != ARMOISE_T_VARIABLE);

  ccl_pre (0 <= i && i < t->c.operands.nb_op);

  return armoise_term_add_reference (t->c.operands.terms[i]);
}

			/* --------------- */

armoise_term *
armoise_term_create_variable (armoise_variable *variable)
{
  armoise_term *result = s_allocate_term (ARMOISE_T_VARIABLE);

  ccl_pre (variable != NULL);

  result->type = armoise_variable_get_type (variable);
  result->c.variable = armoise_variable_add_reference (variable);

  return result;
}

			/* --------------- */

armoise_term *
armoise_term_create_vector_element (armoise_term *variable, int index)
{  
  armoise_term *result = s_allocate_term (ARMOISE_T_ELEMENT);

  ccl_pre (variable != NULL);

  result->c.vec_element.index = index;
  result->c.vec_element.variable = armoise_term_add_reference (variable);
  result->type = *armoise_type_get_subtype_ref (variable->type, index);
  result->type = armoise_type_add_reference (result->type);

  return result;
}

			/* --------------- */

static int
s_gcd (int a, int b)
{
  if (a < 0) 
    a = -a;

  while (b != 0)
    {
      if (a > b)
	a = a -b;
      else
	b = b - a;
    }

  return a;
}

			/* --------------- */

static void
s_normalize_rational (int *p_num, int *p_den)
{
  if (*p_den < 0)
    {
      *p_num = -(*p_num);
      *p_den = -(*p_den);
    }

  if (*p_den != 1)
    {
      int gcd = s_gcd (*p_num, *p_den);
      *p_num /= gcd;
      *p_den /= gcd;
    }
}


			/* --------------- */

armoise_term *
armoise_term_create_constant (int num, int den)
{
  armoise_term *result = s_allocate_term (ARMOISE_T_CONSTANT);

  ccl_pre (den != 0);

  result->type = armoise_type_create_scalar ();
  s_normalize_rational (&num, &den);
  result->c.value.num = num;
  result->c.value.den = den;

  return result;
}

			/* --------------- */

armoise_term *
armoise_term_create_vector (int width, armoise_term **terms)
{
  armoise_term *result;

  ccl_pre (width >= 1);

  if (width == 1)
    result = armoise_term_add_reference (*terms);
  else
    {
      int i;
      armoise_type **subtypes = ccl_new_array (armoise_type *, width);
      result = s_create_n_ary_term (ARMOISE_T_VECTOR, width, terms);
      
      for (i = 0; i < width; i++)
	subtypes[i] = terms[i]->type;
      result->type = armoise_type_create_product (width, subtypes);
      ccl_delete (subtypes);
    }

  return result;
}

			/* --------------- */

armoise_term *
armoise_term_create_neg (armoise_term *t)
{
  armoise_term *result;

  if (t->kind == ARMOISE_T_NEG)
    result = armoise_term_get_operand_at (t, 0);
  else
    {
      result = s_create_n_ary_term (ARMOISE_T_NEG, 1, &t);
      result->type = armoise_type_add_reference (t->type);
    }

  return result;
}

			/* --------------- */

static armoise_term *
s_gather_operands (armoise_term_kind kind , armoise_term *t1, 
		   armoise_term *t2)
{
  int nb_args;
  armoise_term **args;
  armoise_term *result;

  if (0 && (t1->kind == kind || t2->kind == kind))
    {
      int i, k = 0;

      if (t1->kind == kind && t2->kind == kind)
	{
	  nb_args = t1->c.operands.nb_op + t2->c.operands.nb_op;
	  args = ccl_new_array (armoise_term *, nb_args);
	  for (i = 0; i < t1->c.operands.nb_op; i++, k++)
	    args[k] = t1->c.operands.terms[i];
	  for (i = 0; i < t2->c.operands.nb_op; i++, k++)
	    args[k] = t2->c.operands.terms[i];
	}
      else if (t1->kind == kind)
	{
	  nb_args = t1->c.operands.nb_op + 1;
	  args = ccl_new_array (armoise_term *, nb_args);
	  for (i = 0; i < t1->c.operands.nb_op; i++, k++)
	    args[k] = t1->c.operands.terms[i];
	  args[k++] = t2;
	}
      else if (t2->kind == kind)
	{
	  nb_args = t2->c.operands.nb_op + 1;
	  args = ccl_new_array (armoise_term *, nb_args);
	  args[k++] = t1;
	  for (i = 0; i < t2->c.operands.nb_op; i++, k++)
	    args[k] = t2->c.operands.terms[i];
	}
    }  
  else 
    {
      args = ccl_new_array (armoise_term *, 2);
      args[0] = t1;
      args[1] = t2;
      nb_args = 2;
    }

  result = s_create_n_ary_term (kind, nb_args, args);
  ccl_delete (args);

  return result;
}

			/* --------------- */

armoise_term *
armoise_term_create_mul (armoise_term *t1, armoise_term *t2)
{
  armoise_term *result = s_gather_operands (ARMOISE_T_MUL, t1, t2);

  if (armoise_type_is_scalar (t1->type))
    result->type = armoise_type_add_reference (t2->type);
  else
    result->type = armoise_type_add_reference (t1->type);

  return result;
}
			/* --------------- */

armoise_term *
armoise_term_create_div (armoise_term *t1, armoise_term *t2)
{
  armoise_term *result = s_gather_operands (ARMOISE_T_DIV, t1, t2);
  
  result->type = armoise_type_add_reference (t1->type);

  return result;
}

			/* --------------- */

armoise_term *
armoise_term_create_mod (armoise_term *t1, armoise_term *t2)
{
  armoise_term *result = s_create_binary_term (ARMOISE_T_MOD, t1, t2);

  result->type = armoise_type_add_reference (t1->type);

  return result;
}

			/* --------------- */

armoise_term *
armoise_term_create_add (armoise_term *t1, armoise_term *t2)
{
  armoise_term *result;

  if (t1->kind == ARMOISE_T_CONSTANT && t1->c.value.num == 0)
    result = armoise_term_add_reference (t2);
  else if (t2->kind == ARMOISE_T_CONSTANT && t2->c.value.num == 0)
    result = armoise_term_add_reference (t1);
  else 
    {
      result = s_gather_operands (ARMOISE_T_ADD, t1, t2);
      result->type = armoise_type_add_reference (t1->type);
    }

  return result;
}

			/* --------------- */

armoise_term *
armoise_term_create_sub (armoise_term *t1, armoise_term *t2)
{
  armoise_term *result;

  if (t1->kind == ARMOISE_T_CONSTANT && t1->c.value.num == 0)
    result = armoise_term_create_neg (t2);
  else if (t2->kind == ARMOISE_T_CONSTANT && t2->c.value.num == 0)
    result = armoise_term_add_reference (t1);
  else
    {
      result = s_create_binary_term (ARMOISE_T_SUB, t1, t2);
      result->type = armoise_type_add_reference (t1->type);
    }

  return result;
}

			/* --------------- */

static int
s_is_factor (armoise_term *t)
{
  if (t->kind == ARMOISE_T_CONSTANT || t->kind == ARMOISE_T_VARIABLE ||
      t->kind == ARMOISE_T_ELEMENT)
    return 1;

  if (t->kind != ARMOISE_T_MUL)
    return 0;

  if (t->c.operands.nb_op != 2)
    return 0;

  if (t->c.operands.terms[0]->kind != ARMOISE_T_CONSTANT && 
      t->c.operands.terms[1]->kind != ARMOISE_T_CONSTANT)
    return 0;

  return 1;
}

			/* --------------- */

static void
s_display_rational_number (ccl_log_type log, int num, int den, int first)
{
  s_normalize_rational (&num, &den);

  if (num < 0)
    {
      num = -num;
      ccl_log (log, "-");
    }
  else if (num > 0 && !first)
    {
      ccl_log (log, "+");
    }

  if (!first)
    ccl_log (log, " ");
  if (den == 1)
    ccl_log (log, "%d", num);
  else
    ccl_log (log, "%d/%d", num, den);
}

			/* --------------- */

static void
s_display_constant (ccl_log_type log, armoise_term *t, int sign, int first)
{
  int num = sign * t->c.value.num;
  int den = t->c.value.den;
  s_display_rational_number (log, num, den, first);
}

			/* --------------- */

static void
s_display_ax (ccl_log_type log, armoise_term *t, int sign, int first)
{
  ccl_pre (s_is_factor (t));

  if (t->kind == ARMOISE_T_CONSTANT)
    {
      s_display_constant (log, t, sign, first);

      return;
    }

  if (t->kind == ARMOISE_T_VARIABLE || t->kind == ARMOISE_T_ELEMENT)
    {
      if (sign < 0)
	ccl_log (log, first ? "-" : "- ");
      else if (!first && sign > 0) 
	ccl_log (log, "+ ");
      armoise_term_log (log, t);

      return;
    }
  
  if (t->c.operands.terms[0]->kind == ARMOISE_T_CONSTANT && 
      t->c.operands.terms[1]->kind == ARMOISE_T_CONSTANT)
    {
      int num = sign * (t->c.operands.terms[0]->c.value.num * 
			t->c.operands.terms[1]->c.value.num);
      int den = (t->c.operands.terms[0]->c.value.den * 
		 t->c.operands.terms[1]->c.value.den);
      s_display_rational_number (log, num, den, first);
    }
  else
    {
      int index = 0;
      int num, den;
      armoise_term *a;
      armoise_term *x;

      if (t->c.operands.terms[1]->kind == ARMOISE_T_CONSTANT)
	index = 1;
      a = t->c.operands.terms[index];
      x = t->c.operands.terms[(index + 1) % 2];
      num = sign * a->c.value.num;
      den = a->c.value.den;
      s_normalize_rational (&num, &den);

      if ((num == -1 || num == 1) && den == 1)
	s_display_ax (log, x, num, first);
      else
	{
	  s_display_rational_number (log, num, den, first);
	  ccl_log (log, " * ");      

	  if (x->kind == ARMOISE_T_VARIABLE || x->kind == ARMOISE_T_ELEMENT)
	    armoise_term_log (log, x);
	  else
	    {
	      ccl_log (log, "(");
	      armoise_term_log (log, x);
	      ccl_log (log, ")");
	    }
	}
    }
}

			/* --------------- */

static int
s_is_linear_term (armoise_term *t)
{
  int result = 0;

  if (s_is_factor (t) || t->kind == ARMOISE_T_CONSTANT)
    result = 1;
  else if (t->kind == ARMOISE_T_ADD || t->kind == ARMOISE_T_SUB || 
	   t->kind == ARMOISE_T_NEG)
    {
      int i;

      result = 1;      
      for (i = 0; result && i < t->c.operands.nb_op; i++)
	if (! s_is_linear_term (t->c.operands.terms[i]))
	  result = 0;	
    }

  return result;
}

			/* --------------- */

static void
s_log_linear_term_rec (ccl_log_type log, armoise_term *t, int sign, int first)
{
  if (s_is_factor (t))
    s_display_ax (log, t, sign, first);
  else if (t->kind == ARMOISE_T_CONSTANT)
    s_display_constant (log, t, sign, first);
  else if (t->kind == ARMOISE_T_NEG)
    s_log_linear_term_rec (log, t->c.operands.terms[0], -sign, first);
  else 
    {
      int i;

      s_log_linear_term_rec (log, t->c.operands.terms[0], sign, first);
      if (t->kind == ARMOISE_T_SUB)
	sign = -sign;
      for (i = 1; i < t->c.operands.nb_op; i++)
	{
	  ccl_log (log, " ");
	  s_log_linear_term_rec (log, t->c.operands.terms[i], sign, 0);
	}
    }
}

			/* --------------- */

static void
s_log_linear_term (ccl_log_type log, armoise_term *t, int sign)
{
  s_log_linear_term_rec (log, t, sign, 1);
}

			/* --------------- */

void
armoise_term_log (ccl_log_type log, armoise_term *t)
{
  int i;

  ccl_pre (t != NULL);

  switch (t->kind)
    {
    case ARMOISE_T_VARIABLE :
      armoise_variable_log (log, t->c.variable);
      break;
    case ARMOISE_T_CONSTANT :
      if (t->c.value.den == 1)
	ccl_log (log, "%d", t->c.value);
      else
	ccl_log (log, "%d/%d", t->c.value.num, t->c.value.den);
      break;
    case ARMOISE_T_ELEMENT :
      armoise_term_log (log, t->c.vec_element.variable);
      ccl_log (log, "[%d]", t->c.vec_element.index);
      break;
    case ARMOISE_T_VECTOR :      
      ccl_log (log, "(");
      for (i = 0; i < t->c.operands.nb_op - 1; i++)
	{
	  armoise_term_log (log, t->c.operands.terms[i]);
	  ccl_log (log, ", ");
	}
      armoise_term_log (log, t->c.operands.terms[i]);
      ccl_log (log, ")");
      break;

    case ARMOISE_T_NEG :
      ccl_log (log, "-");
      if (t->c.operands.terms[0]->kind != ARMOISE_T_CONSTANT &&
	  t->c.operands.terms[0]->kind != ARMOISE_T_ELEMENT &&
	  t->c.operands.terms[0]->kind != ARMOISE_T_VARIABLE)
	ccl_log (log, "(");
      armoise_term_log (log, t->c.operands.terms[0]);
      if (t->c.operands.terms[0]->kind != ARMOISE_T_CONSTANT &&
	  t->c.operands.terms[0]->kind != ARMOISE_T_ELEMENT &&
	  t->c.operands.terms[0]->kind != ARMOISE_T_VARIABLE)
	ccl_log (log, ")");
      break;

    case ARMOISE_T_ADD: 
    case ARMOISE_T_SUB:
    case ARMOISE_T_MUL:
    case ARMOISE_T_DIV:
    case ARMOISE_T_MOD: 
      if (s_is_linear_term (t)) 
	{
	  s_log_linear_term (log, t, 1);
	}
      else 
	{
	  int i;

	  const char *op;
	  if (t->kind == ARMOISE_T_ADD) op = "+";
	  else if (t->kind == ARMOISE_T_SUB) op = "-";
	  else if (t->kind == ARMOISE_T_MUL) op = "*";
	  else if (t->kind == ARMOISE_T_DIV) op = "/";
	  else 
	    { 
	      ccl_assert (t->kind == ARMOISE_T_MOD);
	      op = "%";
	    }
	  if (t->c.operands.terms[0]->kind != ARMOISE_T_CONSTANT &&
	      t->c.operands.terms[0]->kind != ARMOISE_T_ELEMENT &&
	      t->c.operands.terms[0]->kind != ARMOISE_T_VARIABLE)
	    ccl_log (log, "(");
	  armoise_term_log (log, t->c.operands.terms[0]);
	  if (t->c.operands.terms[0]->kind != ARMOISE_T_CONSTANT &&
	      t->c.operands.terms[0]->kind != ARMOISE_T_ELEMENT &&
	      t->c.operands.terms[0]->kind != ARMOISE_T_VARIABLE)
	    ccl_log (log, ")");
	  for (i = 1; i < t->c.operands.nb_op; i++)
	    {
	      ccl_log (log, " %s ", op);
	      if (t->c.operands.terms[i]->kind != ARMOISE_T_CONSTANT &&
		  t->c.operands.terms[i]->kind != ARMOISE_T_ELEMENT &&
		  t->c.operands.terms[i]->kind != ARMOISE_T_VARIABLE)
		ccl_log (log, "(");
	      armoise_term_log (log, t->c.operands.terms[i]);
	      if (t->c.operands.terms[i]->kind != ARMOISE_T_CONSTANT &&
		  t->c.operands.terms[i]->kind != ARMOISE_T_ELEMENT &&
		  t->c.operands.terms[i]->kind != ARMOISE_T_VARIABLE)
		ccl_log (log, ")");
	    }
	}
      break;      
    }
}

			/* --------------- */

void
armoise_term_get_variables (const armoise_term *t, ccl_list *variables)
{
  int i;

  switch (t->kind)
    {
    case ARMOISE_T_CONSTANT:
      break;

    case ARMOISE_T_VARIABLE:
      if (!ccl_list_has (variables, t->c.variable))
	ccl_list_add (variables, t->c.variable);
      break;

    case ARMOISE_T_ELEMENT:
      armoise_term_get_variables (t->c.vec_element.variable, variables);
      break;

    case ARMOISE_T_VECTOR:
    case ARMOISE_T_ADD:
    case ARMOISE_T_SUB:
    case ARMOISE_T_MUL:
    case ARMOISE_T_DIV:
    case ARMOISE_T_MOD:
    case ARMOISE_T_NEG:
      for (i = 0; i < t->c.operands.nb_op; i++)
	armoise_term_get_variables (t->c.operands.terms[i], variables);
      break;
    }
}

			/* --------------- */

int
armoise_term_get_nb_operands (const armoise_term *t)
{
  ccl_pre (t != NULL);

  return t->c.operands.nb_op;
}

			/* --------------- */

const armoise_term *
armoise_term_get_operand (const armoise_term *t, int index)
{
  ccl_pre (0 <= index && index < armoise_term_get_nb_operands (t));

  return t->c.operands.terms[index];
}

			/* --------------- */

const armoise_term *
armoise_term_get_vec_element_variable (const armoise_term *t)
{
  ccl_pre (armoise_term_get_kind (t) == ARMOISE_T_ELEMENT);

  return t->c.vec_element.variable;
}

			/* --------------- */

int
armoise_term_get_vec_element_index (const armoise_term *t)
{
  ccl_pre (t != NULL);

  return t->c.vec_element.index;
}

static armoise_term *
s_allocate_term (armoise_term_kind kind)
{
  armoise_term *result = ccl_new (armoise_term);

  result->refcount = 1;
  result->kind = kind;
  result->type = NULL;

  return result;
}

			/* --------------- */

static void
s_delete_term (armoise_term *t)
{
  int i;

  ccl_pre (t != NULL);
  ccl_pre (t->refcount == 0);

  switch (t->kind)
    {
    case ARMOISE_T_VARIABLE :
      armoise_variable_del_reference (t->c.variable);
      break;
    case ARMOISE_T_CONSTANT :
      break;
    case ARMOISE_T_ELEMENT :
      armoise_term_del_reference (t->c.vec_element.variable);
      break;
    case ARMOISE_T_VECTOR :      
    case ARMOISE_T_ADD :
    case ARMOISE_T_SUB :
    case ARMOISE_T_MUL :
    case ARMOISE_T_DIV :
    case ARMOISE_T_MOD :
    case ARMOISE_T_NEG :
      for (i = 0; i < t->c.operands.nb_op; i++)
	armoise_term_del_reference (t->c.operands.terms[i]);
      ccl_delete (t->c.operands.terms);
    }

  armoise_type_del_reference (t->type);
  ccl_delete (t);
}

			/* --------------- */

static armoise_term *
s_create_n_ary_term (armoise_term_kind kind, int arity, armoise_term **t)
{
  int i;
  armoise_term *result = s_allocate_term (kind);

  ccl_pre (arity > 0);

  result->c.operands.nb_op = arity;
  result->c.operands.terms = ccl_new_array (armoise_term *, arity);

  for (i = 0; i < arity; i++)
    result->c.operands.terms[i] = armoise_term_add_reference (t[i]);

  return result;
}

			/* --------------- */

static armoise_term *
s_create_binary_term (armoise_term_kind kind, armoise_term *t1, 
		      armoise_term *t2)
{
  armoise_term *operands[2];

  ccl_pre (t1 != NULL && t2 != NULL);

  operands[0] = t1;
  operands[1] = t2;

  return s_create_n_ary_term (kind, 2, operands);
}

			/* --------------- */

