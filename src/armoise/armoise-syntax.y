/*
 * armoise-syntax.y -- add a comment about this file
 * 
 * This file is a part of the ARMOISE language library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
%{
#include <ccl/ccl-memory.h>
#include "armoise/armoise-input-p.h"

#define YYDEBUG 1
#define YYSTYPE armoise_tree *

#define ZN ((armoise_tree *) NULL)

#define NN(_type,_vtype,_child,_next) _NN (_type, #_type, _vtype, _child, _next)

#define NE(_type,_child,_next) NN (_type, CCL_PARSE_TREE_EMPTY, _child, _next)
#define U_NE(_type,_child) NE (_type,_child, ZN)
#define B_NE(_result, _type,_child1,_child2)	\
  do { \
    armoise_tree *aux_ = U_NE (_type, _child1); \
    (_child1)->next = (_child2); \
    (_result) = aux_; \
  } while (0)

#define NID()  NN (ARMOISE_TREE_IDENTIFIER, CCL_PARSE_TREE_IDENT, ZN, ZN)
#define NINT() NN (ARMOISE_TREE_INTEGER, CCL_PARSE_TREE_INT, ZN, ZN)
#define NSTR() NN (ARMOISE_TREE_STRING, CCL_PARSE_TREE_STRING, ZN, ZN)

# define SL(_x_) do { (_x_)->line = armoise_current_line; } while (0)

#define malloc  ccl_malloc
#define realloc ccl_realloc
#define calloc  ccl_calloc
#define free    ccl_free

armoise_tree *armoise_syntax_tree;
armoise_tree *armoise_syntax_tree_container;

# define NODES armoise_syntax_tree_container

# define MAX_NB_SYNTAX_ERROR 100

 int armoise_number_of_errors;

static armoise_tree *
_NN (int type, const char *type_string, ccl_parse_tree_value_type vtype, 
     armoise_tree *child, armoise_tree *next)
{
  NODES = 
    ccl_parse_tree_create (type, type_string, vtype, 
			   (child == ZN 
			    ? armoise_current_line 
			    : child->line),
			   (child == ZN
			    ? armoise_current_filename
			    : child->filename),
			   child, next, NODES);
  return NODES;
}

static armoise_tree *
s_make_range (int min, int max, int in_nat);

static armoise_tree *
s_make_rewrite_simple_set_prototype (armoise_tree *proto, armoise_tree *dom,
                                     armoise_tree *formula);

%}

%token TOK_IDENTIFIER /* [a-zA-Z_][a-zA-Z_0-9]*+ */
%token TOK_ASSIGN /* := */
%token TOK_LET /* let */
%token TOK_IN /* in */
%token TOK_UINT /* [0]|[1-9][0-9]* */
%token TOK_NATURALS /* nat */ TOK_INTEGERS /* int */ 
%token TOK_POSITIVES /* posi */ TOK_REALS /* real */
%token TOK_LEQ TOK_GEQ TOK_NEQ TOK_IMPLY TOK_EQUIV 
%token TOK_UNION TOK_INTERSECTION
%token TOK_AND TOK_OR TOK_NOT TOK_ERROR
%token TOK_TRUE TOK_FALSE 
%token TOK_EMPTY TOK_DOTS

%left TOK_UNION '\\' '^'
%left TOK_INTERSECTION
%right '!'
%left '+' '-'
%left '*' '/' '%'
%left '$'
%left UMINUS UPLUS

%left '|' TOK_OR
%left '&' TOK_AND
%left TOK_EQUIV TOK_IMPLY
%left TOK_NOT
%left '=' TOK_NEQ
%left '<' TOK_LEQ '>' TOK_GEQ
%left TOK_EXISTS TOK_FORALL 
%left UMINUS_ARITH UPLUS_ARITH

%start grammar_start_rule

%%

grammar_start_rule : 
  armoise_description 
{ armoise_syntax_tree = $1; }
| /* empty */
{ armoise_syntax_tree = ZN; }
;

armoise_description : 
  definition_or_predicate_list 
{ $$ = $1; }
;

definition_or_predicate_list :
  definition_or_predicate ';' definition_or_predicate_list 
{ $$ = $1; $1->next = $3; }
| definition_or_predicate ';'
{ $$ = $1; }
;

definition_or_predicate : 
  predicate 
{ $$ = $1; }
| predicate_definition 
{ $$ = $1; }
;

/*                                                  Definition of predicates */
predicate_definition : 
  identifier_or_tuple TOK_ASSIGN predicate 
{ B_NE ($$, ARMOISE_TREE_DEFINITION, $1, $3);  }
;

identifier_or_tuple :
  tuple_of_identifiers
{ $$ = $1; }
| identifier
{ $$ = $1; }
;

tuple_of_identifiers : 
 '<' list_of_tuples '>'
{ $$ = U_NE (ARMOISE_TREE_ID_TUPLE, $2);  }
;

list_of_tuples :
  identifier_or_tuple ',' list_of_tuples
{ $$ = $1; $1->next = $3; }
| identifier_or_tuple
{ $$ = $1; }
;

identifier :
  TOK_IDENTIFIER
  { $$ = NID (); $$->value = armoise_token_value; SL ($$); }
;

/*                                                                Predicates */
predicate :
  predicate_without_local_context 
{ $$ = U_NE (ARMOISE_TREE_PREDICATE, $1); }
| local_context tuple_of_predicates_without_local_context 
{ B_NE ($$, ARMOISE_TREE_PREDICATE, $1, $2); }
| local_context predicate_without_local_context 
{ B_NE ($$, ARMOISE_TREE_PREDICATE, $1, $2); }
;

local_context :
  TOK_LET list_of_predicate_definitions TOK_IN 
{ $$ = U_NE (ARMOISE_TREE_PREDICATE_CONTEXT, $2); }
;

list_of_predicate_definitions : 
  predicate_definition ';' list_of_predicate_definitions 
{ $$ = $1; $1->next = $3; }
| predicate_definition ';'
{ $$ = $1; }
;

tuple_of_predicates_without_local_context : 
  '<' list_of_tuple_of_predicates '>'
{ $$ = U_NE (ARMOISE_TREE_PREDICATE_TUPLE, $2); }
;

list_of_tuple_of_predicates :
  predicate_without_local_context ',' list_of_tuple_of_predicates
{ $$ = $1; $1->next = $3; }
| predicate_without_local_context 
{ $$ = $1; }
;


predicate_without_local_context :
  set
{ $$ = $1; }
;

set : 
  set TOK_UNION set
{ B_NE ($$, ARMOISE_TREE_UNION, $1, $3); }
| set '\\' set
{ B_NE ($$, ARMOISE_TREE_DIFFERENCE, $1, $3); }
| set '^' set
{ B_NE ($$, ARMOISE_TREE_XOR, $1, $3); }
| set TOK_INTERSECTION set
{ B_NE ($$, ARMOISE_TREE_INTERSECTION, $1, $3); }
| '!' set
{ $$ = U_NE (ARMOISE_TREE_COMPLEMENT, $2); }
|  set '+' set
{ B_NE ($$, ARMOISE_TREE_PLUS, $1, $3); }
| set '-' set
{ B_NE ($$, ARMOISE_TREE_MINUS, $1, $3); }
| set '*' set
{ B_NE ($$, ARMOISE_TREE_MUL, $1, $3); }
| set '/' set
{ B_NE ($$, ARMOISE_TREE_DIV, $1, $3); }
| set '%' set
{ B_NE ($$, ARMOISE_TREE_MOD, $1, $3); }
| '-' set %prec UMINUS
{ $$ = U_NE (ARMOISE_TREE_NEG, $2); }
| '(' list_of_boolean_combinations ')'
{ $$ = U_NE (ARMOISE_TREE_CARTESIAN_PRODUCT, $2); }
| '(' set ')'
{ $$ = U_NE (ARMOISE_TREE_PAREN, $2); }
| simple_set
{ $$ = $1; }
| list_element
{ $$ = $1; }
| range
{ $$ = $1; }
| set '$' integer
{ B_NE ($$, ARMOISE_TREE_POWER, $1, $3); }
| integer
{ $$ = $1; }
| '+' integer %prec UPLUS
{ $$ = $2; }
| TOK_NATURALS
{ $$ = NE (ARMOISE_TREE_NATURALS, ZN, ZN); }
| TOK_INTEGERS
{ $$ = NE (ARMOISE_TREE_INTEGERS, ZN, ZN); }
| TOK_POSITIVES
{ $$ = NE (ARMOISE_TREE_POSITIVES, ZN, ZN); }
| TOK_REALS
{ $$ = NE (ARMOISE_TREE_REALS, ZN, ZN); }
|  TOK_EMPTY
{ $$ = NE (ARMOISE_TREE_EMPTY, ZN, ZN); }
;

integer :
  TOK_UINT
{ $$ = NINT (); $$->value = armoise_token_value; SL($$); }
;

list_element :
  identifier 
{ $$ = $1; }
| list_element '[' integer ']'
{ B_NE ($$, ARMOISE_TREE_LIST_ELEMENT, $1, $3); }
;

list_of_boolean_combinations :
  set ',' list_of_boolean_combinations 
{ $$ = $1; $1->next = $3; }
| set ',' set
{ $$ = $1; $1->next = $3; }
;  

simple_set :
  '{' list_of_terms '}' 
  { $$ = U_NE (ARMOISE_TREE_ENUMERATED_SET, $2); }
| '{' typed_identifier '|' formula '}'
  { B_NE($$, ARMOISE_TREE_SET, $2, $4); }
| '{' typed_identifier '}'
  { armoise_tree *F = U_NE(ARMOISE_TREE_TRUE, ZN);
    B_NE($$, ARMOISE_TREE_SET, $2, F); }
;

range:
  '[' integer TOK_DOTS integer ']'
{ $$ = s_make_range ($2->value.int_value, $4->value.int_value, 1); 
  ccl_parse_tree_delete_tree ($2); ccl_parse_tree_delete_tree ($4); }
| '[' integer ',' integer ']'
{ $$ = s_make_range ($2->value.int_value, $4->value.int_value, 0); 
  ccl_parse_tree_delete_tree ($2); ccl_parse_tree_delete_tree ($4); }

list_of_terms : 
  term ',' list_of_terms 
{ $$ = $1; $1->next = $3; }
| term
{ $$ = $1; }
;

vector_of_terms : 
 '(' list_of_terms ')'
{ $$ = U_NE (ARMOISE_TREE_VECTOR, $2); }
;

formula : 
  formula '|' formula
{ B_NE ($$, ARMOISE_TREE_OR, $1, $3); }
| formula TOK_OR formula  
{ B_NE ($$, ARMOISE_TREE_OR, $1, $3); }
| formula '&' formula
{ B_NE ($$, ARMOISE_TREE_AND, $1, $3); }
| formula TOK_AND formula
{ B_NE ($$, ARMOISE_TREE_AND, $1, $3); }
| formula TOK_EQUIV formula
{ B_NE ($$, ARMOISE_TREE_EQUIV, $1, $3); }
| formula TOK_IMPLY formula
{ B_NE ($$, ARMOISE_TREE_IMPLY, $1, $3); }
| term_comparison
{ $$ = $1; }
| quantified_formula
{ $$ = $1; }
| '!' formula %prec TOK_NOT
{ $$ = NE (ARMOISE_TREE_NOT, $2, ZN); }
| TOK_NOT formula
{ $$ = NE (ARMOISE_TREE_NOT, $2, ZN); }
| term TOK_IN set
{ B_NE ($$, ARMOISE_TREE_IN, $1, $3); }
| '(' formula ')'
{ $$ = U_NE (ARMOISE_TREE_PAREN, $2); }
| identifier vector_of_terms
{ B_NE ($$, ARMOISE_TREE_CALL, $1, $2); }
| TOK_TRUE 
{ $$ = U_NE (ARMOISE_TREE_TRUE, ZN); }
| TOK_FALSE
{ $$ = U_NE (ARMOISE_TREE_FALSE, ZN); }
;

term_comparison :
  term '=' term
{ B_NE ($$, ARMOISE_TREE_EQ, $1, $3); }
| term TOK_NEQ term
{ B_NE ($$, ARMOISE_TREE_NEQ, $1, $3); }
| term '<' term
{ B_NE ($$, ARMOISE_TREE_LT, $1, $3); }
| term '>' term
{ B_NE ($$, ARMOISE_TREE_GT, $1, $3); }
| term TOK_LEQ term
{ B_NE ($$, ARMOISE_TREE_LEQ, $1, $3); }
| term TOK_GEQ term
{ B_NE ($$, ARMOISE_TREE_GEQ, $1, $3); }
;

quantified_formula  :
  TOK_EXISTS typed_identifier formula
{ B_NE ($$, ARMOISE_TREE_EXISTS, $3, $2); }
| TOK_FORALL typed_identifier formula
{ B_NE ($$, ARMOISE_TREE_FORALL, $3, $2); }
;

typed_identifier :
  identifier TOK_IN predicate_without_local_context
  { B_NE ($$, ARMOISE_TREE_IN, $1, $3); }
| vector_of_terms TOK_IN predicate_without_local_context
  { B_NE ($$, ARMOISE_TREE_IN, $1, $3); }
  ;

/*                                       Arithmetic on first order variables */
term : 
  term '+' term 
{ B_NE ($$, ARMOISE_TREE_PLUS, $1, $3); }
| term '-' term 
{ B_NE ($$, ARMOISE_TREE_MINUS, $1, $3); }
| term '*' term
{ B_NE ($$, ARMOISE_TREE_MUL, $1, $3); }
| term '/' term
{ B_NE ($$, ARMOISE_TREE_DIV, $1, $3); }
| term '%' term
{ B_NE ($$, ARMOISE_TREE_MOD, $1, $3); }
| '-' term %prec UMINUS_ARITH
{ $$ = U_NE (ARMOISE_TREE_NEG, $2); }
| vector_of_terms
{ $$ = $1; } 
| list_element /* identifier */
| integer
{ $$ = $1; } 
| '+' integer %prec UPLUS_ARITH
{ $$ = $2; }
;
 
%%

static armoise_tree *
s_make_range (int minv, int maxv, int in_nat)
{
  armoise_tree *proto = NID ();
  armoise_tree *min = NINT ();
  armoise_tree *max = NINT ();
  armoise_tree *min_leq_x = NID ();
  armoise_tree *x_leq_max = NID ();
  armoise_tree *result;
  armoise_tree *dom;

  proto->value.id_value = ccl_string_make_unique ("_x");
  min->value.int_value = minv;
  max->value.int_value = maxv;

  min_leq_x->value.id_value = ccl_string_make_unique ("_x");
  B_NE (min_leq_x, ARMOISE_TREE_LEQ, min, min_leq_x);

  x_leq_max->value.id_value = ccl_string_make_unique ("_x");
  B_NE (x_leq_max, ARMOISE_TREE_LEQ, x_leq_max, max);

  B_NE (result, ARMOISE_TREE_AND, min_leq_x, x_leq_max); 
  if (in_nat)
    dom = NE (ARMOISE_TREE_NATURALS, ZN, ZN);
  else if (minv >= 0)
    dom = NE (ARMOISE_TREE_POSITIVES, ZN, ZN);
  else
    dom = NE (ARMOISE_TREE_REALS, ZN, ZN);

  result = s_make_rewrite_simple_set_prototype (proto, dom, result);

  return result;
}

			/* --------------- */

static armoise_tree *
s_make_rewrite_simple_set_prototype (armoise_tree *proto, armoise_tree *dom,
                                     armoise_tree *formula)
{
  armoise_tree *result;
  armoise_tree *in;

  B_NE (in, ARMOISE_TREE_IN, proto, dom);
  if (formula == NULL)
    formula = U_NE(ARMOISE_TREE_TRUE, ZN);
  B_NE(result, ARMOISE_TREE_SET, in, formula);

  return result;
}
