/*
 * armoise-variable.c -- add a comment about this file
 * 
 * This file is a part of the ARMOISE language library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "armoise-p.h"

armoise_variable *
armoise_variable_create (ccl_ustring name, armoise_type *type)
{
  armoise_variable *result = ccl_new (armoise_variable);

  ccl_pre (type != NULL);

  result->refcount = 1;
  result->name = name;  
  result->type = armoise_type_add_reference (type);

  return result;
}

			/* --------------- */

armoise_variable *
armoise_variable_add_reference (armoise_variable *variable)
{
  ccl_pre (variable != NULL);
  
  variable->refcount++;

  return variable;
}

			/* --------------- */

void
armoise_variable_del_reference (armoise_variable *variable)
{
  ccl_pre (variable != NULL);
  ccl_pre (variable->refcount > 0);

  variable->refcount--;
  if (variable->refcount == 0)
    {
      armoise_type_del_reference (variable->type);
      ccl_zdelete (armoise_domain_del_reference, variable->domain);
      ccl_delete (variable);
    }
}

			/* --------------- */

ccl_ustring 
armoise_variable_get_name (armoise_variable *variable)
{
  ccl_pre (variable != NULL);

  return variable->name;
}

			/* --------------- */

armoise_type *
armoise_variable_get_type (armoise_variable *variable)
{
  armoise_type *result;

  ccl_pre (variable != NULL);

  result = armoise_type_add_reference (variable->type);

  return result;
}

			/* --------------- */

const armoise_domain *
armoise_variable_get_domain (armoise_variable *variable)
{
  ccl_pre (variable != NULL);

  return variable->domain;
}

			/* --------------- */

void
armoise_variable_set_domain (armoise_variable *variable, 
			     const armoise_domain *D)
{
  ccl_pre (variable != NULL);
  ccl_pre (D != NULL);

  ccl_zdelete (armoise_domain_del_reference, variable->domain);
  variable->domain = armoise_domain_duplicate (D);
}

			/* --------------- */

void
armoise_variable_log (ccl_log_type log, armoise_variable *variable)
{
  ccl_pre (variable != NULL);

  ccl_log (log, "%s", variable->name);
}

			/* --------------- */
