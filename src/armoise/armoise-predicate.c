/*
 * armoise-predicate.c -- add a comment about this file
 * 
 * This file is a part of the ARMOISE language library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "armoise-p.h"

static armoise_predicate *
s_allocate_predicate (armoise_context *ctx, armoise_predicate_kind kind);

static void
s_delete_predicate (armoise_predicate *P);

static armoise_predicate *
s_create_nary (armoise_context *ctx, armoise_predicate_kind kind, int nb_op, 
		 armoise_predicate **operands);

static armoise_predicate *
s_create_unary (armoise_context *ctx, armoise_predicate_kind kind, 
		armoise_predicate *P);

static armoise_predicate *
s_create_binary (armoise_context *ctx, armoise_predicate_kind kind, 
		 armoise_predicate *P1, armoise_predicate *P2);
		   

			/* --------------- */

armoise_predicate *
armoise_predicate_create_empty (armoise_context *ctx, armoise_type *type)
{
  armoise_predicate *result = s_allocate_predicate (ctx, ARMOISE_P_EMPTY);

  result->type = armoise_type_add_reference (type);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_naturals (armoise_context *ctx)
{
  armoise_predicate *result = s_allocate_predicate (ctx, ARMOISE_P_NAT);

  result->type = armoise_type_create_scalar();
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_integers (armoise_context *ctx)
{
  armoise_predicate *result = s_allocate_predicate (ctx, ARMOISE_P_INT);

  result->type = armoise_type_create_scalar();
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_positives (armoise_context *ctx)
{
  armoise_predicate *result = s_allocate_predicate (ctx, ARMOISE_P_POSI);

  result->type = armoise_type_create_scalar();
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_reals (armoise_context *ctx)
{
  armoise_predicate *result = s_allocate_predicate (ctx, ARMOISE_P_REAL);

  result->type = armoise_type_create_scalar();
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_indirection (armoise_context *ctx, ccl_ustring name, 
				      int nb_indices, const int *indices)
{
  armoise_predicate *aux;
  armoise_predicate *result = s_allocate_predicate (ctx, ARMOISE_P_INDIRECTION);

  ccl_pre (name != NULL); 
  ccl_pre (nb_indices >= 0);

  result->c.indirection.name = name;
  result->c.indirection.nb_indices = nb_indices;
  result->c.indirection.indices = ccl_new_array (int, nb_indices);
  ccl_memcpy (result->c.indirection.indices, indices, 
	      nb_indices * sizeof (int));

  aux = armoise_context_get_predicate (ctx, name, nb_indices, indices);
  result->type = armoise_predicate_get_type (aux);
  armoise_predicate_del_reference (aux);
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_union (armoise_context *ctx, armoise_predicate *P1, 
				armoise_predicate *P2)
{
  armoise_predicate *result = s_create_binary (ctx, ARMOISE_P_UNION, P1, P2);

  ccl_assert (armoise_type_equals (P1->type, P2->type));

  result->type = armoise_predicate_get_type (P1);
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_intersection (armoise_context *ctx, 
				       armoise_predicate *P1, 
				       armoise_predicate *P2)
{
  armoise_predicate *result = 
    s_create_binary (ctx, ARMOISE_P_INTERSECTION, P1, P2);

  ccl_assert (armoise_type_equals (P1->type, P2->type));
  result->type = armoise_predicate_get_type (P1);
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_delta (armoise_context *ctx, armoise_predicate *P1, 
				armoise_predicate *P2)
{
  armoise_predicate *result = s_create_binary (ctx, ARMOISE_P_DELTA, P1, P2);

  ccl_assert (armoise_type_equals (P1->type, P2->type));
  result->type = armoise_predicate_get_type (P1);
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_diff (armoise_context *ctx, armoise_predicate *P1, 
			       armoise_predicate *P2)
{
  armoise_predicate *result = s_create_binary (ctx, ARMOISE_P_DIFF, P1, P2);

  ccl_assert (armoise_type_equals (P1->type, P2->type));
  result->type = armoise_predicate_get_type (P1);
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_complement (armoise_context *ctx, 
				     armoise_predicate *P)
{
  armoise_predicate *result = s_create_unary (ctx, ARMOISE_P_COMPLEMENT, P);

  result->type = armoise_predicate_get_type (P);
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_add (armoise_context *ctx, armoise_predicate *P1, 
			      armoise_predicate *P2)
{
  armoise_predicate *result = s_create_binary (ctx, ARMOISE_P_ADD, P1, P2);

  result->type = armoise_predicate_get_type (P1);
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_sub (armoise_context *ctx, armoise_predicate *P1, 
			      armoise_predicate *P2)
{
  armoise_predicate *result = s_create_binary (ctx, ARMOISE_P_SUB, P1, P2);

  result->type = armoise_predicate_get_type (P1);
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_mul (armoise_context *ctx, armoise_predicate *P1, 
			      armoise_predicate *P2)
{
  armoise_predicate *result = s_create_binary (ctx, ARMOISE_P_MUL, P1, P2);

  if (! armoise_type_is_scalar (P1->type))
    result->type = armoise_predicate_get_type (P1);
  else 
    result->type = armoise_predicate_get_type (P2);
  armoise_predicate_compute_domain (result);
  
  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_div (armoise_context *ctx, armoise_predicate *P1, 
			      armoise_predicate *P2)
{
  armoise_predicate *result = s_create_binary (ctx, ARMOISE_P_DIV, P1, P2);

  result->type = armoise_predicate_get_type (P1);
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_mod (armoise_context *ctx, armoise_predicate *P1, 
			      armoise_predicate *P2)
{
  armoise_predicate *result = s_create_binary (ctx, ARMOISE_P_MOD, P1, P2);

  result->type = armoise_predicate_get_type (P1);
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_neg (armoise_context *ctx, armoise_predicate *P)
{
  armoise_predicate *result = s_create_unary (ctx, ARMOISE_P_NEG, P);

  result->type = armoise_predicate_get_type (P);
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_product (armoise_context *ctx, int nb_comp,
				  armoise_predicate **comps)
{
  int i;
  armoise_type **subtypes = ccl_new_array (armoise_type *, nb_comp);
  armoise_predicate *result = 
    s_create_nary (ctx, ARMOISE_P_PRODUCT, nb_comp, comps);
  
  for (i = 0; i < nb_comp; i++)
    subtypes[i] = comps[i]->type;
  result->type = armoise_type_create_product (nb_comp, subtypes);
  ccl_delete (subtypes);
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_integer (armoise_context *ctx, int i)
{
  armoise_term *value = armoise_term_create_constant (i, 1);
  armoise_predicate *result = 
    armoise_predicate_create_finite_set (ctx, 1, &value);
  armoise_term_del_reference (value);
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_paren (armoise_context *ctx, armoise_predicate *P)
{
  armoise_predicate *result = s_allocate_predicate (ctx, ARMOISE_P_PAREN);

  result->type = armoise_predicate_get_type (P);
  result->c.P = armoise_predicate_add_reference (P);
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_finite_set (armoise_context *ctx, int nb_terms,
				     armoise_term **values)
{
  int i;
  armoise_predicate *result = s_allocate_predicate (ctx, ARMOISE_P_ENUM);
  result->c.elements.card = nb_terms;
  result->c.elements.set = ccl_new_array (armoise_term *, nb_terms);
  result->type = armoise_term_get_type (values[0]);

  for (i = 0; i < nb_terms; i++)
    result->c.elements.set[i] = armoise_term_add_reference (values[i]);

  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_create_simple_set (armoise_context *ctx, armoise_term *proto,
				     armoise_predicate *P,
				     armoise_formula *def)
{
  armoise_predicate *result = s_allocate_predicate (ctx, ARMOISE_P_SET);

  ccl_pre (proto != NULL);
  ccl_pre (def != NULL);

  result->c.set.proto = armoise_term_add_reference (proto);
  result->c.set.P = armoise_predicate_add_reference (P);
  result->c.set.def = armoise_formula_add_reference (def);
  result->type = armoise_term_get_type (proto);
  armoise_predicate_compute_domain (result);

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_add_reference (armoise_predicate *P)
{
  ccl_pre (P != NULL);

  P->refcount++;

  return P;
}

			/* --------------- */

void
armoise_predicate_del_reference (armoise_predicate *P)
{
  ccl_pre (P != NULL);
  ccl_pre (P->refcount > 0);

  P->refcount--;
  if (P->refcount == 0)
    s_delete_predicate (P);
}

			/* --------------- */

static int 
s_get_largest_domain (const armoise_predicate *P);

static int 
s_get_largest_domain_for_formula (const armoise_formula *F, int dflt)
{
  int result;

  switch (armoise_formula_get_kind (F)) 
    {
    case ARMOISE_F_CONSTANT:
      result = dflt;
      break;
      
    case ARMOISE_F_IN:
      {
	armoise_predicate *P = armoise_formula_get_in_predicate (F);
	result = s_get_largest_domain (P);
	armoise_predicate_del_reference (P);
      }
      break;

    case ARMOISE_F_NOT:
    case ARMOISE_F_AND:
    case ARMOISE_F_OR:
    case ARMOISE_F_EQUIV:
    case ARMOISE_F_IMPLY:
      {
	int i;
	int nb_operands = armoise_formula_get_nb_operands (F);
	const armoise_formula *op = armoise_formula_get_operand (F, 0);
	result = s_get_largest_domain_for_formula (op, dflt);
	for (i = 1; i < nb_operands && result != R_DOM; i++)
	  {
	    int res;
	    op = armoise_formula_get_operand (F, i);
	    res = s_get_largest_domain_for_formula (op, dflt);
	    if (res > result)
	      result = res;
	  }
      }
      break;

    case ARMOISE_F_EQ:
    case ARMOISE_F_NEQ:
    case ARMOISE_F_LEQ: 
    case ARMOISE_F_GEQ:
    case ARMOISE_F_LT:
    case ARMOISE_F_GT:
      {
	ccl_pair *p;
	ccl_list *vars = ccl_list_create ();
	
	armoise_formula_get_variables (F, vars);
	result = N_DOM;
	for (p = FIRST (vars); p && result != R_DOM; p = CDR(p))
	  {
	    armoise_variable *var = (armoise_variable *) CAR (p);
	    int res = armoise_domain_get_largest_dom (var->domain);
	    if (res > result)
	      result = res;
	  }
	ccl_list_delete (vars);
      }
      break;

    case ARMOISE_F_EXISTS:
    case ARMOISE_F_FORALL:
      {
	armoise_domain *dom = armoise_predicate_get_domain (F->c.q.domain);
	int res = armoise_domain_get_largest_dom (dom);
	armoise_domain_del_reference (dom);	
	result = s_get_largest_domain_for_formula (F->c.q.f, dflt);
	if (res > result)
	  result = res;
      }
      break;

    case ARMOISE_F_PAREN:
      result = s_get_largest_domain_for_formula (F->c.f, dflt);
      break;
    }

  return result;
}

			/* --------------- */

static int 
s_get_largest_domain (const armoise_predicate *P)
{
  int result = armoise_domain_get_largest_dom (P->domain);

  switch (P->kind)    
    {
    case ARMOISE_P_REAL:
    case ARMOISE_P_EMPTY:
    case ARMOISE_P_NAT:
    case ARMOISE_P_INT:
    case ARMOISE_P_POSI:
    case ARMOISE_P_ENUM:
      break;

    case ARMOISE_P_SET:
      result = s_get_largest_domain (P->c.set.P);
      result = s_get_largest_domain_for_formula (P->c.set.def, result);
      break;

    default:
      {
	int i;
	int nb_operands = armoise_predicate_get_nb_operands (P);

	for (i = 0; i < nb_operands && result != R_DOM; i++)
	  {
	    const armoise_predicate *op = armoise_predicate_get_operand (P, i);
	    int res = s_get_largest_domain (op);
	    if (res > result)
	      result = res;
	  }
      }
      break;
    }

  return result;
}

			/* --------------- */

armoise_domain_attr
armoise_predicate_get_largest_domain (const armoise_predicate *P)
{
  int dom = s_get_largest_domain (P);
  armoise_domain_attr result = ARMOISE_DOMAIN_ATTR_FOR_REALS;
 
  switch (dom) {
  case N_DOM: result = ARMOISE_DOMAIN_ATTR_FOR_NATURALS; break;
  case Z_DOM: result = ARMOISE_DOMAIN_ATTR_FOR_INTEGERS; break;
  case P_DOM: result = ARMOISE_DOMAIN_ATTR_FOR_POSITIVES; break;
  default: 
    ccl_assert (dom == R_DOM);
    break;
  }
  return result;
}

			/* --------------- */

armoise_context *
armoise_predicate_get_context (const armoise_predicate *P)
{
  ccl_pre (P != NULL);
  
  return armoise_context_add_reference (P->context);
}

			/* --------------- */

int
armoise_predicate_get_arity (armoise_predicate *P)
{
  ccl_pre (P != NULL);
  
  return armoise_type_get_width (P->type);
}

			/* --------------- */

armoise_type *
armoise_predicate_get_type (const armoise_predicate *P)
{
  ccl_pre (P != NULL);
  ccl_pre (P->type != NULL);

  return armoise_type_add_reference (P->type);
}

			/* --------------- */

static void
s_tab (ccl_log_type log, int tab)
{
  while (tab--)
    ccl_log (log, " ");
}

			/* --------------- */

static void
s_log_tree (ccl_log_type log, ccl_tree *t, int tab, int ctx);


void
armoise_predicate_log_tree (ccl_log_type log, ccl_tree *t)
{
  s_log_tree (log, t, 0, 1);
}

			/* --------------- */

static void
s_log_predicate (ccl_log_type log, const armoise_predicate *P, int tab, 
		 int ctx);

void
armoise_predicate_log (ccl_log_type log, const armoise_predicate *P)
{
  s_log_predicate (log, P, 0, 1);
}

			/* --------------- */

static int
s_has_context (const armoise_predicate *P)
{
  return ccl_hash_get_size (armoise_context_get_table (P->context)) != 0;
}

			/* --------------- */

static void
s_log_predicate_context (ccl_log_type log, armoise_context *ctx, int tab)
{
  ccl_pair *p;
  ccl_hash *predicates;
  ccl_list *ids = armoise_context_get_ids (ctx);

  if (ccl_list_get_size (ids) == 0)
    return;

  predicates = armoise_context_get_table (ctx);
  s_tab (log, tab);
  ccl_log (log, "let\n");
  for (p = FIRST (ids); p; p = CDR (p))
    {
      int ltab = tab + 2;
      ccl_tree *tP;
      ccl_ustring id = (ccl_ustring) CAR (p);

      ccl_hash_find (predicates, id);
      tP = (ccl_tree *) ccl_hash_get (predicates);

      s_tab (log, tab + 1);
      ccl_log (log, "%s := ", id);

      if (!ccl_tree_is_leaf (tP) || 
	  s_has_context ((armoise_predicate *)tP->object))
	ccl_log (log, "\n");
      else
	ltab = 0;

      s_log_tree (log, tP, ltab, 1);
      ccl_log (log, ";\n");
    }
  s_tab (log, tab);
  ccl_log (log, "in\n ");
}

			/* --------------- */

static armoise_context *
s_look_for_context (ccl_tree *t)
{
  armoise_context *result;

  if (ccl_tree_is_leaf (t))
    result = ((armoise_predicate *)(t->object))->context;
  else
    result = s_look_for_context (t->childs);

  return result;
}

			/* --------------- */

static void
s_log_tree (ccl_log_type log, ccl_tree *t, int tab, int ctx)
{
  if (ccl_tree_is_leaf (t))
    s_log_predicate (log, t->object, tab, ctx);
  else
    {
      ccl_tree *c;
      armoise_context *context = s_look_for_context (t);
      
      s_log_predicate_context (log, context, tab);
      s_tab (log, tab);
      ccl_log (log, "< ");      
      for (c = t->childs; c->next; c = c->next)
	{
	  s_log_tree (log, c, 0, 0);
	  ccl_log (log, ", ");
	}
      s_log_tree (log, c, 0, 0);
      ccl_log (log, " >");
    }
}

			/* --------------- */

static void
s_log_predicate (ccl_log_type log, const armoise_predicate *P, int tab, int ctx)
{
  int i;
  const char *op;

  if (ctx)
    s_log_predicate_context (log, P->context, tab);
  s_tab (log, tab);

  switch (P->kind)
    {
    case ARMOISE_P_EMPTY:
      ccl_log (log, "empty");
      break;
    case ARMOISE_P_NAT:
      ccl_log (log, "nat");
      break;
    case ARMOISE_P_INT:
      ccl_log (log, "int");
      break;
    case ARMOISE_P_POSI:   
      ccl_log (log, "posi");
      break;
    case ARMOISE_P_REAL:
      ccl_log (log, "real");
      break;

    case ARMOISE_P_UNION:   
    case ARMOISE_P_INTERSECTION:
    case ARMOISE_P_DIFF:   
    case ARMOISE_P_DELTA:
    case ARMOISE_P_ADD:
    case ARMOISE_P_SUB:
    case ARMOISE_P_MUL:
    case ARMOISE_P_DIV:
    case ARMOISE_P_MOD:
    case ARMOISE_P_COMPLEMENT:
    case ARMOISE_P_NEG:
    case ARMOISE_P_PRODUCT:

      if (P->kind == ARMOISE_P_COMPLEMENT)
	ccl_log (log, "!");
      else if (P->kind == ARMOISE_P_NEG)
	ccl_log (log, "-");
      else if (P->kind == ARMOISE_P_PRODUCT)
	ccl_log (log, "(");

      switch (P->kind)
	{
	case ARMOISE_P_UNION: op = " ||"; break;
	case ARMOISE_P_INTERSECTION: op = " &&"; break;
	case ARMOISE_P_DIFF: op = " \\";  break;
	case ARMOISE_P_DELTA: op = " ^"; break;
	case ARMOISE_P_ADD: op = " +"; break;
	case ARMOISE_P_SUB: op = " -"; break;
	case ARMOISE_P_MUL: op = " *"; break;
	case ARMOISE_P_DIV: op = " /"; break;
	case ARMOISE_P_MOD: op = " %"; break;
	case ARMOISE_P_PRODUCT: op = ","; break;
	default: op = NULL; break;
	}

      for (i = 0; i < P->c.operands.nb_op-1; i++)
	{
	  s_log_predicate (log, P->c.operands.predicates[i], 0, 0);
	  ccl_log (log, "%s ", op);
	}
      s_log_predicate (log, P->c.operands.predicates[i], 0, 0);

      if (P->kind == ARMOISE_P_PRODUCT)
	ccl_log (log, ")");      
      break;

    case ARMOISE_P_ENUM:
      if (P->c.elements.card == 1)
	armoise_term_log (log, P->c.elements.set[0]);
      else
	{
	  ccl_log (log, "{ ");
	  for (i = 0; i < P->c.elements.card-1; i++)
	    {
	      armoise_term_log (log, P->c.elements.set[i]);
	      ccl_log (log, ", ");
	    }
	  armoise_term_log (log, P->c.elements.set[i]);
	  ccl_log (log, " }");
	}
      break;

    case ARMOISE_P_SET:
      ccl_log (log, "{ ");
      armoise_term_log (log, P->c.set.proto);
      ccl_log (log, " in ");
      armoise_predicate_log (log, P->c.set.P);
      ccl_log (log, " | ");
      armoise_formula_log (log, P->c.set.def);
      ccl_log (log, " }");
      break;

    case ARMOISE_P_INDIRECTION:
      ccl_log (log, "%s", P->c.indirection.name);
      for (i = 0; i < P->c.indirection.nb_indices; i++)
	ccl_log (log, "[%d]", P->c.indirection.indices[i]);
      break;

    case ARMOISE_P_PAREN:
      ccl_log (log, "(");
      s_log_predicate (log, P->c.P, 0, 0);
      ccl_log (log, ")");
      break;
    }
}

			/* --------------- */

void
armoise_predicate_get_variables (armoise_predicate *P, ccl_list *variables)
{
  switch (P->kind)
    {
    case ARMOISE_P_EMPTY:
    case ARMOISE_P_NAT:
    case ARMOISE_P_INT:
    case ARMOISE_P_POSI:
    case ARMOISE_P_REAL:
      break;
    case ARMOISE_P_UNION:
    case ARMOISE_P_INTERSECTION:
    case ARMOISE_P_DIFF:
    case ARMOISE_P_DELTA:
    case ARMOISE_P_COMPLEMENT:
    case ARMOISE_P_ADD:
    case ARMOISE_P_SUB:
    case ARMOISE_P_MUL:
    case ARMOISE_P_DIV:
    case ARMOISE_P_MOD:
    case ARMOISE_P_NEG:
    case ARMOISE_P_PRODUCT:
      {
	int i;
	int nb_op = P->c.operands.nb_op;
	armoise_predicate **operands = P->c.operands.predicates;

	for (i = 0; i < nb_op; i++)
	  armoise_predicate_get_variables (operands[i], variables);
      }
      break;

    case ARMOISE_P_ENUM:
      {
	int i;
	for (i = 0; i < P->c.elements.card; i++)
	  armoise_term_get_variables (P->c.elements.set[i], variables);
      }
      break;

    case ARMOISE_P_SET:
      armoise_term_get_variables (P->c.set.proto, variables);
      armoise_formula_get_variables (P->c.set.def, variables);
      break;
      
    case ARMOISE_P_INDIRECTION:
      {
	armoise_predicate *Pi = 
	  armoise_context_get_predicate (P->context, 
					 P->c.indirection.name,
					 P->c.indirection.nb_indices,
					 P->c.indirection.indices);
	armoise_predicate_get_variables (Pi, variables);
	armoise_predicate_del_reference (Pi);
      }
      break;

    case ARMOISE_P_PAREN:
      armoise_predicate_get_variables (P->c.P, variables);
      break;
    }
}

			/* --------------- */

armoise_predicate_kind
armoise_predicate_get_kind (const armoise_predicate *P)
{
  ccl_pre (P != NULL);

  return P->kind;
}

			/* --------------- */

const armoise_predicate *
armoise_predicate_get_operand (const armoise_predicate *P, int i)
{
  const armoise_predicate *result;

  ccl_pre (0 <= i  && i < armoise_predicate_get_nb_operands (P));

  if (P->kind == ARMOISE_P_PAREN)
    result = P->c.P;
  else if (P->kind == ARMOISE_P_INDIRECTION)
    result = armoise_context_get_cst_predicate (P->context, 
						P->c.indirection.name,
						P->c.indirection.nb_indices,
						P->c.indirection.indices);
  else
    result = P->c.operands.predicates[i];

  return result;
}

			/* --------------- */

armoise_predicate *
armoise_predicate_get_operand_at (const armoise_predicate *P, int i)
{
  armoise_predicate *result;

  ccl_pre (0 <= i  && i < armoise_predicate_get_nb_operands (P));

  if (P->kind == ARMOISE_P_PAREN)
    result = armoise_predicate_add_reference (P->c.P);
  else if (P->kind == ARMOISE_P_INDIRECTION)
    result = armoise_context_get_predicate (P->context, 
					    P->c.indirection.name,
					    P->c.indirection.nb_indices,
					    P->c.indirection.indices);
  else
    result = armoise_predicate_add_reference (P->c.operands.predicates[i]);

  
  return result;  
}

			/* --------------- */
int 
armoise_predicate_get_nb_operands (const armoise_predicate *P)
{
  ccl_pre (P != NULL);

  if (P->kind == ARMOISE_P_PAREN || P->kind == ARMOISE_P_INDIRECTION)
    return 1;

  return P->c.operands.nb_op;
}

			/* --------------- */

armoise_domain *
armoise_predicate_get_domain (const armoise_predicate *P)
{
  ccl_pre (P != NULL);

  return armoise_domain_add_reference (P->domain);
}

			/* --------------- */

int
armoise_predicate_get_nb_elements (const armoise_predicate *P)
{
  ccl_pre (armoise_predicate_get_kind (P) == ARMOISE_P_ENUM);

  return P->c.elements.card;
}

			/* --------------- */

const armoise_term *
armoise_predicate_get_element (const armoise_predicate *P, int i)
{
  ccl_pre (0 <= i && i < armoise_predicate_get_nb_elements (P));

  return P->c.elements.set[i];
}

			/* --------------- */

const armoise_term *
armoise_predicate_get_set_proto (const armoise_predicate *P,
				 armoise_predicate **resultP)

{
  ccl_pre (armoise_predicate_get_kind (P) == ARMOISE_P_SET);

  if (resultP != NULL)
    *resultP = armoise_predicate_add_reference (P->c.set.P);

  return P->c.set.proto;
}

			/* --------------- */

const armoise_formula *
armoise_predicate_get_set_formula (const armoise_predicate *P)
{
  ccl_pre (armoise_predicate_get_kind (P) == ARMOISE_P_SET);

  return P->c.set.def;
}

			/* --------------- */

extern void
armoise_predicate_get_indirection (const armoise_predicate *P, 
				   ccl_ustring *p_name, int *p_nb_indices, 
				   const int **p_indices)
{
  ccl_pre (armoise_predicate_get_kind (P) ==  ARMOISE_P_INDIRECTION);
  
  *p_name = P->c.indirection.name;
  *p_nb_indices = P->c.indirection.nb_indices;
  *p_indices = P->c.indirection.indices;
}

			/* --------------- */

static armoise_predicate *
s_allocate_predicate (armoise_context *ctx, armoise_predicate_kind kind)
{
  armoise_predicate *result = ccl_new (armoise_predicate);

  ccl_pre (ctx != NULL);

  result->refcount = 1;
  result->kind = kind;
  result->context = armoise_context_add_reference (ctx);
  result->domain = NULL;

  return result;
}

			/* --------------- */

static void
s_delete_predicate (armoise_predicate *P)
{
  int i;

  switch (P->kind)
    {
    case ARMOISE_P_EMPTY: 
    case ARMOISE_P_NAT:
    case ARMOISE_P_INT:
    case ARMOISE_P_POSI:   
    case ARMOISE_P_REAL:
      break;

    case ARMOISE_P_UNION:   
    case ARMOISE_P_INTERSECTION:
    case ARMOISE_P_DIFF:   
    case ARMOISE_P_DELTA:
    case ARMOISE_P_COMPLEMENT:
    case ARMOISE_P_ADD:
    case ARMOISE_P_SUB:
    case ARMOISE_P_MUL:
    case ARMOISE_P_DIV:
    case ARMOISE_P_MOD:
    case ARMOISE_P_NEG:
    case ARMOISE_P_PRODUCT:
      for (i = 0; i < P->c.operands.nb_op; i++)
	armoise_predicate_del_reference (P->c.operands.predicates[i]);
      ccl_delete  (P->c.operands.predicates);
      break;

    case ARMOISE_P_ENUM:
      for (i = 0; i < P->c.elements.card; i++)
	armoise_term_del_reference (P->c.elements.set[i]);
      ccl_delete  (P->c.elements.set);
      break;

    case ARMOISE_P_SET:
      armoise_term_del_reference (P->c.set.proto);
      armoise_formula_del_reference (P->c.set.def);
      armoise_predicate_del_reference (P->c.set.P);
      break;

    case ARMOISE_P_INDIRECTION:
      ccl_zdelete (ccl_delete, P->c.indirection.indices);
      break;
    case ARMOISE_P_PAREN:
      armoise_predicate_del_reference (P->c.P);
      break;
    }

  armoise_context_del_reference (P->context);
  armoise_type_del_reference (P->type);
  ccl_zdelete (armoise_domain_del_reference, P->domain);
  ccl_delete (P);
}

			/* --------------- */

static armoise_predicate *
s_create_nary (armoise_context *ctx, armoise_predicate_kind kind, int nb_op, 
	       armoise_predicate **operands)
{
  int i;
  armoise_predicate *result = s_allocate_predicate (ctx, kind);

  ccl_pre (nb_op > 0);
  ccl_pre (operands != NULL);

  result->c.operands.nb_op = nb_op;
  result->c.operands.predicates = 
    ccl_new_array (armoise_predicate *, nb_op); 
  for (i = 0; i < nb_op; i++)
    result->c.operands.predicates[i] = 
      armoise_predicate_add_reference (operands[i]);

  return result;
}

			/* --------------- */

static armoise_predicate *
s_create_unary (armoise_context *ctx, armoise_predicate_kind kind, 
		armoise_predicate *P)
{
  return s_create_nary (ctx, kind, 1, &P);
}

			/* --------------- */

static armoise_predicate *
s_create_binary (armoise_context *ctx, armoise_predicate_kind kind, 
		 armoise_predicate *P1, armoise_predicate *P2)
{
  armoise_predicate *ops[2];

  ops[0] = P1;
  ops[1] = P2;

  return s_create_nary (ctx, kind, 2, ops);
}
