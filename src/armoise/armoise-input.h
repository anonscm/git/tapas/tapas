/*
 * armoise-input.h -- Syntactical analyzer for the ARMOISE language
 * Copyright (C) 2007 J. Leroux, G. Point, LaBRI, CNRS UMR 5800, 
 * Universite Bordeaux I
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */
/*
 * $Author: point $
 * $Revision: 1.3 $
 * $Date: 2008/01/24 16:20:53 $
 * $Id: armoise-input.h,v 1.3 2008/01/24 16:20:53 point Exp $
 */

/*!
 * \file armoise/armoise-input.h
 * \brief Syntactical analyzer for the ARMOISE language
 * 
 * This module gathers functions that permit to analyze ARMOISE formulas read
 * from a file, a stream or a string. All functions return a pointer to an 
 * \ref armoise_tree or NULL if an error occurs.
 */
#ifndef __ARMOISE_INPUT_H__
# define __ARMOISE_INPUT_H__

# include <stdio.h>
# include <armoise/armoise-tree.h>

BEGIN_C_DECLS

/*!
 * \brief read ARMOISE formulas or definitions from the file specified by 
 * \a filename.
 *
 * \param filename the path to the file to read
 *
 * \pre filename != NULL
 *
 * \return a pointer to the syntactic tree of the file or NULL if the file can
 * not be read.
 */
extern armoise_tree *
armoise_read_file (const char *filename);

/*!
 * \brief read ARMOISE formulas or definitions from \a stream. \a input_name is
 * the name of a location used mainly for messaging purpose (e.g errors).
 *
 * \param stream the input stream
 * \param input_name a location 'name' to indicate where errors occur.
 * 
 * \pre filename != NULL 
 * \pre input_name != NULL
 *
 * \return a pointer to the syntactic tree of the file or NULL if the file can
 * not be read.
 */
extern armoise_tree *
armoise_read_stream (FILE *stream, const char *input_name);

/*!
 * \brief read ARMOISE formulas or definitions from the string \a input. 
 *
 * \param input the string that contains the ARMOISE description.
 * 
 * \pre input != NULL
 *
 * \return a pointer to the syntactic tree of the file or NULL if the file can
 * not be read.
 */
extern armoise_tree *
armoise_read_string (const char *input);

END_C_DECLS


#endif /* ! __ARMOISE_INPUT_H__ */
