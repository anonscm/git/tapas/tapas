/*
 * armoise-type.c -- add a comment about this file
 * 
 * This file is a part of the ARMOISE language library
. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include "armoise.h"

struct armoise_type_st
{
  int refcount;
  int nb_subtypes;
  armoise_type *subtypes[1];
};

			/* --------------- */

static armoise_type *
s_allocate_type (int nb_subtypes, armoise_type **subtypes);

static void
s_delete_type (armoise_type *type);

			/* --------------- */

armoise_type *
armoise_type_create_scalar (void)
{
  return s_allocate_type (0, NULL);
}

			/* --------------- */

armoise_type *
armoise_type_create_product (int nb_subtypes, armoise_type **subtypes)
{
  ccl_pre (nb_subtypes > 0);

  return s_allocate_type (nb_subtypes, subtypes);
}

			/* --------------- */

int
armoise_type_is_unknown (const armoise_type *type)
{
  int result;

  ccl_pre (type != NULL);

  result = type->nb_subtypes < 0;

  if (! result && type->nb_subtypes > 0)
    {
      int i;
      
      for (i = 0; !result && i < type->nb_subtypes; i++)
	result = armoise_type_is_unknown (type->subtypes[i]);
    }

  return result;
}

			/* --------------- */

int
armoise_type_is_scalar (const armoise_type *type)
{
  ccl_pre (type != NULL);

  return type->nb_subtypes == 0;
}

			/* --------------- */

armoise_type *
armoise_type_add_reference (armoise_type *type)
{
  ccl_pre (type != NULL);
  
  type->refcount++;

  return type;
}

			/* --------------- */

void
armoise_type_del_reference (armoise_type *type)
{
  ccl_pre (type != NULL);
  ccl_pre (type->refcount > 0);

  type->refcount--;
  if (type->refcount == 0)
    s_delete_type (type);
}

			/* --------------- */

int
armoise_type_get_width (const armoise_type *type)
{
  ccl_pre (type != NULL);

  return type->nb_subtypes;
}

			/* --------------- */

int
armoise_type_get_depth_width (const armoise_type *type)
{
  int result;

  if (armoise_type_is_scalar (type))
    result = 1;
  else
    {
      int i;
      int w = armoise_type_get_width (type);

      for (result = 0, i = 0; i < w; i++)
	{
	  const armoise_type *st = armoise_type_get_subtype (type, i);
	  result += armoise_type_get_depth_width (st);
	}
    }
  return result;
}

			/* --------------- */

void
armoise_type_log (ccl_log_type log, const armoise_type *type)
{
  if (type->nb_subtypes < 0)
    ccl_log (log, "unknown");
  else if (type->nb_subtypes == 0)
    ccl_log (log, "real");
  else
    {
      int i;

      ccl_log (log, "(");
      for (i = 0; i < type->nb_subtypes - 1; i++)
	{
	  armoise_type_log (log, type->subtypes[i]);
	  ccl_log (log, ", ");
	}
      armoise_type_log (log, type->subtypes[i]);
      ccl_log (log, ")");
    }  
}

			/* --------------- */

int
armoise_type_equals (const armoise_type *type, const armoise_type *other)
{
  int i;
  int result;

  ccl_pre (type != NULL && other != NULL);

  result = type->nb_subtypes == other->nb_subtypes;
  for (i = 0; result && i < type->nb_subtypes; i++)
    result = armoise_type_equals (type->subtypes[i], other->subtypes[i]);

  return result;
}

			/* --------------- */

armoise_type **
armoise_type_get_subtype_ref (armoise_type *type, int i)
{
  ccl_pre (0 <= i && i < armoise_type_get_width (type));

  return &type->subtypes[i];
}

			/* --------------- */


const armoise_type *
armoise_type_get_subtype (const armoise_type *type, int i)
{
  ccl_pre (0 <= i && i < armoise_type_get_width (type));

  return type->subtypes[i];
}

			/* --------------- */

armoise_type *
armoise_type_duplicate (const armoise_type *type)
{
  int i;
  armoise_type *result;
  armoise_type **subtypes = NULL;

  if (type->nb_subtypes > 0)
    {
      subtypes = ccl_new_array (armoise_type *, 
					       type->nb_subtypes);
      for (i = 0; i < type->nb_subtypes; i++)
	subtypes[i] = armoise_type_duplicate (type->subtypes[i]);
    }
  result = s_allocate_type (type->nb_subtypes, subtypes);
  if (type->nb_subtypes > 0)
    {
      for (i = 0; i < type->nb_subtypes; i++)
	armoise_type_del_reference (subtypes[i]);
      ccl_delete (subtypes);
    }

  return result;
}

			/* --------------- */

static armoise_type *
s_allocate_type (int nb_subtypes, armoise_type **subtypes)
{
  int i;
  armoise_type *result;
  size_t size = sizeof (armoise_type);

  if (nb_subtypes > 1)
    size += (nb_subtypes - 1) * sizeof (armoise_type *);
  result = (armoise_type *) ccl_malloc (size);
  result->refcount = 1;
  result->nb_subtypes = nb_subtypes;
  for (i = 0; i < nb_subtypes; i++)
    result->subtypes[i] = armoise_type_add_reference (subtypes[i]);

  return result;
}

			/* --------------- */

static void
s_delete_type (armoise_type *type)
{
  int i;

  ccl_pre (type != NULL);

  for (i = 0; i < type->nb_subtypes; i++)
    armoise_type_del_reference (type->subtypes[i]);

  ccl_delete (type);
}
