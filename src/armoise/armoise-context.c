/*
 * armoise-context.c -- add a comment about this file
 * Copyright (C) 2007 J. Leroux, G. Point, LaBRI, CNRS UMR 5800, 
 * Universite Bordeaux I
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */
/*
 * $Author: point $
 * $Revision: 1.7 $
 * $Date: 2008/01/24 16:20:53 $
 * $Id: armoise-context.c,v 1.7 2008/01/24 16:20:53 point Exp $
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-hash.h>
#include "armoise.h"

struct armoise_context_st 
{
  int refcount;
  armoise_context *parent;  
  ccl_hash *table;
  ccl_list *ids;
  ccl_duplicate_func *dup;
};

			/* --------------- */

static armoise_context *
s_create_context (armoise_context *parent, ccl_delete_proc *del,
		  ccl_duplicate_func *dup);

			/* --------------- */

armoise_context *
armoise_context_create_for_predicates (armoise_context *parent)
{
  return s_create_context (parent, 
			   (ccl_delete_proc *) ccl_tree_del_reference,
			   (ccl_duplicate_func *)ccl_tree_add_reference);
}

			/* --------------- */

armoise_context *
armoise_context_create_for_variables (armoise_context *parent)
{
   return 
     s_create_context (parent, 
		       (ccl_delete_proc *) armoise_variable_del_reference,
		       (ccl_duplicate_func *) armoise_variable_add_reference);
}

			/* --------------- */

armoise_context *
armoise_context_add_reference (armoise_context *ctx)
{
  ccl_pre (ctx != NULL);

  ctx->refcount++;

  return ctx;
}

			/* --------------- */

void
armoise_context_del_reference (armoise_context *ctx)
{
  ccl_pre (ctx != NULL);
  ccl_pre (ctx->refcount > 0);

  ctx->refcount--;
  if (ctx->refcount == 0)
    {
      ccl_hash_delete (ctx->table);
      ccl_list_delete (ctx->ids);
      ccl_delete (ctx);
    }
}

			/* --------------- */

armoise_context *
armoise_context_get_parent (armoise_context *ctx)
{
  armoise_context *result;

  ccl_pre (ctx != NULL);
  
  if (ctx->parent != NULL)
    result = armoise_context_add_reference (ctx->parent);
  else
    result = NULL;

  return result;
}

			/* --------------- */

int
armoise_context_has (armoise_context *ctx, ccl_ustring id)
{
  ccl_pre (ctx != NULL);

  return (ccl_hash_find (ctx->table, id) ||
	  (ctx->parent != NULL && 
	   armoise_context_has (ctx->parent, id)));
}

			/* --------------- */

int
armoise_context_has_locally (armoise_context *ctx, ccl_ustring id)
{
  ccl_pre (ctx != NULL);

  return ccl_hash_find (ctx->table, id);
}

			/* --------------- */

void *
armoise_context_get (armoise_context *ctx, ccl_ustring id)
{
  void *result = NULL;

  ccl_pre (ctx != NULL);

  if (ccl_hash_find (ctx->table, id))
    {
      result = ccl_hash_get (ctx->table);
      result = ctx->dup (result);
    }
  else if (ctx->parent != NULL)
    {
      result = armoise_context_get (ctx->parent, id);
    }

  return result;
}

			/* --------------- */

void
armoise_context_add (armoise_context *ctx, ccl_ustring id, void *object)
{
  ccl_pre (! armoise_context_has_locally (ctx, id));
  ccl_pre (object != NULL);

  ccl_hash_find (ctx->table, id);
  object = ctx->dup (object);
  ccl_hash_insert (ctx->table, object);
  ccl_list_add (ctx->ids, id);
}

			/* --------------- */


ccl_hash *
armoise_context_get_table (armoise_context *ctx)
{
  ccl_pre (ctx != NULL);

  return ctx->table;
}

			/* --------------- */

ccl_list *
armoise_context_get_ids (armoise_context *ctx)
{
  ccl_pre (ctx != NULL);

  return ctx->ids;
}

			/* --------------- */

int
armoise_context_has_predicate_tree (armoise_context *ctx, ccl_ustring id)
{
  return armoise_context_has (ctx, id);
}

			/* --------------- */

ccl_tree *
armoise_context_get_predicate_tree (armoise_context *ctx, ccl_ustring id)
{
  return (ccl_tree *) armoise_context_get (ctx, id);
}

			/* --------------- */

void
armoise_context_add_predicate_tree (armoise_context *ctx, ccl_ustring id,
				    ccl_tree *t)
{
  armoise_context_add (ctx, id, t);
}

			/* --------------- */

static armoise_predicate *
s_get_predicate (armoise_context *ctx, ccl_ustring id, 
		 int nb_indices, const int *indices)
{
  armoise_predicate *result;
  int i = 0;
  ccl_tree *t = armoise_context_get_predicate_tree (ctx, id);

  ccl_pre (t != NULL);

  while (! ccl_tree_is_leaf(t) && i < nb_indices)
    {
      ccl_tree *tmp = ccl_tree_get_child (t, indices[i]);
      ccl_tree_del_reference (t);
      t = tmp;
      i++;
    }      

  ccl_assert (ccl_tree_is_leaf(t) && i == nb_indices);

  result = t->object;
  ccl_tree_del_reference (t);

  return result;
}

			/* --------------- */


armoise_predicate *
armoise_context_get_predicate (armoise_context *ctx, ccl_ustring id, 
			       int nb_indices, const int *indices)
{
  armoise_predicate *result = s_get_predicate (ctx, id, nb_indices, indices);

  return armoise_predicate_add_reference (result);
}

			/* --------------- */

const armoise_predicate *
armoise_context_get_cst_predicate (armoise_context *ctx, ccl_ustring id, 
				   int nb_indices, const int *indices)
{
  return s_get_predicate (ctx, id, nb_indices, indices);
}

			/* --------------- */

int
armoise_context_has_variable (armoise_context *ctx, ccl_ustring id)
{
  return armoise_context_has (ctx, id);
}

			/* --------------- */

void
armoise_context_add_variable (armoise_context *ctx, armoise_variable *var)
{
  ccl_ustring varname = armoise_variable_get_name (var);

  armoise_context_add (ctx, varname, var);
}

			/* --------------- */

armoise_variable *
armoise_context_get_variable (armoise_context *ctx, ccl_ustring id)
{
  return armoise_context_get (ctx, id);
}

			/* --------------- */

static armoise_context *
s_create_context (armoise_context *parent, ccl_delete_proc *del,
		  ccl_duplicate_func *dup)
{
  armoise_context *result = ccl_new (armoise_context);
  
  result->refcount = 1;
  result->parent = parent;
  result->table = ccl_hash_create (NULL, NULL, NULL, del);
  result->ids = ccl_list_create();
  result->dup = dup;

  return result;
}


