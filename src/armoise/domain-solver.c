/*
 * domain-solver.c -- add a comment about this file
 * 
 * This file is a part of the ARMOISE language library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include "armoise-p.h"

#define IN_INTEGERS(a) (((a) & ARMOISE_DOMAIN_IN_INTEGERS) != 0)
#define FINITE(a) (((a) & ARMOISE_DOMAIN_IS_FINITE) != 0)
#define HAS_NEG(a) (((a) & ARMOISE_DOMAIN_HAS_NEGATIVES) != 0)
#define HAS_POS(a) (((a) & ARMOISE_DOMAIN_HAS_POSITIVES) != 0)
#define ONLY_NEG(a) (HAS_NEG (a) && ! HAS_POS (a))
#define ONLY_POS(a) (! HAS_NEG (a) && HAS_POS (a))
#define ZERO(a) (! HAS_NEG (a) && ! HAS_POS (a))

static void
s_compute_domain_for_predicate_tree (ccl_tree *t);

static armoise_domain *
s_compute_domain_for_term (armoise_term *term, ccl_hash *doms);

static armoise_domain *
s_apply (const armoise_domain *D1, const armoise_domain *D2,
	 armoise_domain * (*op) (const armoise_domain *, 
				 const armoise_domain *));

static armoise_domain *
s_apply_mul (const armoise_domain *D1, const armoise_domain *D2,
	     armoise_domain * (*op) (const armoise_domain *, 
				     const armoise_domain *));

static armoise_domain *
s_apply_unary (const armoise_domain *D, 
	       armoise_domain * (*op) (const armoise_domain *));


static armoise_domain *
s_domain_neg (const armoise_domain *D);

static armoise_domain *
s_domain_complement (const armoise_domain *D);

static armoise_domain *
s_domain_union (const armoise_domain *D1, const armoise_domain *D2);

static armoise_domain *
s_domain_intersection (const armoise_domain *D1, const armoise_domain *D2);

static armoise_domain *
s_domain_add (const armoise_domain *D1, const armoise_domain *D2);

static armoise_domain *
s_domain_sub (const armoise_domain *D1, const armoise_domain *D2);

static armoise_domain *
s_domain_mul (const armoise_domain *D1, const armoise_domain *D2);

static armoise_domain *
s_domain_div (const armoise_domain *D1, const armoise_domain *D2);

static armoise_domain *
s_domain_mod (const armoise_domain *D1, const armoise_domain *D2);

static armoise_domain *
s_type_to_domain (const armoise_type *type);

static void
s_solve_domain_for_set (armoise_predicate *P);

			/* --------------- */

void
armoise_predicate_compute_domain (armoise_predicate *P)
{
  ccl_hash *predicates = armoise_context_get_table (P->context);

  if (ccl_hash_get_size (predicates) != 0)
    {
      ccl_pointer_iterator *it = ccl_hash_get_elements (predicates);

      while (ccl_iterator_has_more_elements (it))
	{
	  ccl_tree *t = (ccl_tree *) ccl_iterator_next_element (it);
	  s_compute_domain_for_predicate_tree (t);
	}
      ccl_iterator_delete (it);
    }

  if (P->domain != NULL)
    return;

  if (P->kind == ARMOISE_P_EMPTY)
    {
      P->domain = armoise_domain_create_empty ();
    }
  else if (P->kind == ARMOISE_P_NAT)
    {
      P->domain = 
	armoise_domain_create_with_attr (ARMOISE_DOMAIN_ATTR_FOR_NATURALS);
    }
  else if (P->kind == ARMOISE_P_INT)
    {
      P->domain = 
	armoise_domain_create_with_attr (ARMOISE_DOMAIN_ATTR_FOR_INTEGERS);
    }
  else if (P->kind == ARMOISE_P_POSI)
    {
      P->domain = 
	armoise_domain_create_with_attr (ARMOISE_DOMAIN_ATTR_FOR_POSITIVES);
    }
  else if (P->kind == ARMOISE_P_REAL)
    {
      P->domain = 
	armoise_domain_create_with_attr (ARMOISE_DOMAIN_ATTR_FOR_REALS);
    }
  else if (P->kind == ARMOISE_P_INDIRECTION)
    {
      armoise_predicate *Pi = 
	armoise_context_get_predicate (P->context,
				       P->c.indirection.name,
				       P->c.indirection.nb_indices,
				       P->c.indirection.indices);
      ccl_assert (Pi != NULL && Pi->domain != NULL);
      P->domain = armoise_domain_duplicate (Pi->domain);
      armoise_predicate_del_reference (Pi);
    }
  else if (P->kind == ARMOISE_P_PAREN)
    {
      armoise_predicate_compute_domain (P->c.P);
      P->domain = armoise_domain_duplicate (P->c.P->domain);
    }
  else if (P->kind == ARMOISE_P_ENUM)
    {
      int i;
      int card = P->c.elements.card;
      P->domain = s_compute_domain_for_term (P->c.elements.set[0], NULL);

      for (i = 1; i < card; i++)
	{
	  armoise_domain *D = s_compute_domain_for_term (P->c.elements.set[i],
							 NULL);
	  armoise_domain *aux = s_apply (P->domain, D, s_domain_union);
	  armoise_domain_del_reference (D);
	  armoise_domain_del_reference (P->domain);
	  P->domain = aux;
	}
      ccl_assert (! armoise_domain_is_empty (P->domain));
    }
  else if (P->kind == ARMOISE_P_SET)
    {
      s_solve_domain_for_set (P);
    }
  else if (P->kind == ARMOISE_P_PRODUCT)   
    {
      int i;
      int nb_doms = P->c.operands.nb_op;
      armoise_predicate **operands = P->c.operands.predicates;
      armoise_domain **subdoms = ccl_new_array (armoise_domain *, nb_doms);

      for (i = 0; i < nb_doms; i++)
	{
	  armoise_predicate_compute_domain (operands[i]);
	  if (armoise_domain_is_empty (operands[i]->domain))
	    break;
	  subdoms[i] = operands[i]->domain;
	}
      if (i < nb_doms)
	P->domain = armoise_domain_create_empty ();
      else
	P->domain = armoise_domain_create_compound_domain (nb_doms, subdoms);
      ccl_delete (subdoms);
    }
  else
    {   
      int i;
      int nb_doms = P->c.operands.nb_op;
      armoise_predicate **operands = P->c.operands.predicates;

      for (i = 0; i < nb_doms; i++)
	armoise_predicate_compute_domain (operands[i]);

      if (P->kind == ARMOISE_P_NEG)
	{
	  P->domain = s_apply_unary (operands[0]->domain, s_domain_neg);
	}
      else if (P->kind == ARMOISE_P_COMPLEMENT)
	{
	  if (armoise_domain_is_empty (operands[0]->domain))
	    P->domain = s_type_to_domain (P->type);
	  else
	    P->domain = s_apply_unary (operands[0]->domain, 
				       s_domain_complement);
	}
      else 
	{
	  armoise_domain *
	    (*apply) (const armoise_domain *, const armoise_domain *,
		      armoise_domain * (*) (const armoise_domain *, 
					    const armoise_domain *)) =
	    s_apply;

	  armoise_domain * (*op) (const armoise_domain *, 
				  const armoise_domain *);
	  switch (P->kind)
	    {
	    case ARMOISE_P_UNION: op = s_domain_union; break;
	    case ARMOISE_P_INTERSECTION: op = s_domain_intersection; break;
	    case ARMOISE_P_DIFF: op = s_domain_intersection; break;
	    case ARMOISE_P_DELTA: op = s_domain_union; break;
	    case ARMOISE_P_ADD: op = s_domain_add; break;
	    case ARMOISE_P_SUB: op = s_domain_sub; break; 
	    case ARMOISE_P_MUL: op = s_domain_mul; apply = s_apply_mul; break; 
	    case ARMOISE_P_DIV: op = s_domain_div; apply = s_apply_mul; break; 
	    default:
	      ccl_assert (P->kind == ARMOISE_P_MOD);
	      op = s_domain_mod; 
	      apply = s_apply_mul; 
	      break; 
	    }
	  ccl_assert (op != NULL);	  
	  P->domain = armoise_domain_duplicate (operands[0]->domain);
	  for (i = 1; i < nb_doms; i++)
	    {
	      armoise_domain *d = apply (P->domain, operands[i]->domain, op);
	      armoise_domain_del_reference (P->domain);
	      P->domain = d;
	    }
	}
    }

  ccl_assert (P->domain != NULL);

  if (ccl_debug_is_on)
    {
      armoise_predicate_log (CCL_LOG_DEBUG, P);
      ccl_debug (" --> ");
      armoise_domain_log (CCL_LOG_DEBUG, P->domain);
      ccl_debug ("\n");
    }
}
			/* --------------- */

static void
s_compute_domain_for_predicate_tree (ccl_tree *t)
{
  if (ccl_tree_is_leaf (t))
    armoise_predicate_compute_domain (t->object);
  else
    {
      for (t = t->childs; t; t = t->next)
	s_compute_domain_for_predicate_tree (t);
    }
}

			/* --------------- */

static armoise_domain *
s_apply (const armoise_domain *D1, const armoise_domain *D2,
	 armoise_domain * (*op) (const armoise_domain *, 
				 const armoise_domain *))
{
  armoise_domain *result;
  int w = armoise_domain_get_width (D1);

  if (w == 0 || w == -1 || armoise_domain_get_width (D2) == -1)
    result = op (D1, D2);
  else
    {
      int i;
      armoise_domain **doms = ccl_new_array (armoise_domain *, w);

      for (i = 0; i < w; i++)
	{
	  const armoise_domain *sD1 = armoise_domain_get_component (D1, i);
	  const armoise_domain *sD2 = armoise_domain_get_component (D2, i);
	  
	  doms[i] = s_apply (sD1, sD2, op);
	  if (armoise_domain_is_empty (doms[i]))
	    break;
	}
      if (i < w)
	result = armoise_domain_create_empty ();
      else
	result = armoise_domain_create_compound_domain (w, doms);
      for (i = 0; i < w; i++)
	ccl_zdelete (armoise_domain_del_reference, doms[i]);
      ccl_delete (doms);
    }

  return result;
}

			/* --------------- */


static armoise_domain *
s_apply_mul (const armoise_domain *D1, const armoise_domain *D2,
	     armoise_domain * (*op) (const armoise_domain *, 
				     const armoise_domain *))
{
  armoise_domain *result = NULL;
  int w1 = armoise_domain_get_width (D1);
  int w2 = armoise_domain_get_width (D2);

  if (w1 == -1 || w2 == -1)
    result = armoise_domain_create_empty ();
  else if (w1 == 0 && w2 == 0)
    result = op (D1, D2);
  else if (w1 > 0 && w2 > 0)
    {
      int i;

      ccl_assert (w1 == w2);

      for (i = 0; i < w1; i++)
	{
	  const armoise_domain *sD1 = armoise_domain_get_component (D1, i);
	  const armoise_domain *sD2 = armoise_domain_get_component (D2, i);
	  armoise_domain *tmp = s_apply_mul (sD1, sD2, op);
	  if (result == NULL)
	    result = tmp;
	  else
	    {
	      armoise_domain *aux = s_domain_intersection (tmp, result);
	      armoise_domain_del_reference (tmp);
	      armoise_domain_del_reference (result);
	      result = aux;
	    }
	  if (armoise_domain_is_empty (result))
	    break;
	}
    }
  else
    {
      int i;
      int w = w1 == 0 ? w2 : w1;
      armoise_domain **doms = ccl_new_array (armoise_domain *, w);

      ccl_assert (w1 == 0 || w2 == 0);
      for (i = 0; i < w; i++)
	{
	  const armoise_domain *sD1 = D1;
	  const armoise_domain *sD2 = D2;
	  
	  if (w1 == 0)
	    sD2 = armoise_domain_get_component (D2, i);
	  else 
	    sD1 = armoise_domain_get_component (D1, i);

	  doms[i] = s_apply_mul (sD1, sD2, op);
	  if (armoise_domain_is_empty (doms[i]))
	    break;
	}

      if (i < w)
	result = armoise_domain_create_empty ();
      else
	result = armoise_domain_create_compound_domain (w, doms);
      for (i = 0; i < w; i++)
	ccl_zdelete (armoise_domain_del_reference, doms[i]);
      ccl_delete (doms);
    }

  return result;
}

			/* --------------- */

static armoise_domain *
s_apply_unary (const armoise_domain *D, 
	       armoise_domain * (*op) (const armoise_domain *))
{
  armoise_domain *result;
  int w = armoise_domain_get_width (D);

  if (w == 0 || w == -1)
    result = op (D);
  else
    {
      int i;
      armoise_domain **doms = ccl_new_array (armoise_domain *, w);

      for (i = 0; i < w; i++)
	{
	  const armoise_domain *sD = armoise_domain_get_component (D, i);
	  
	  doms[i] = s_apply_unary (sD, op);
	  if (armoise_domain_is_empty (doms[i]))
	    break;
	}

      if (i < w)
	result = armoise_domain_create_empty ();
      else
	result = armoise_domain_create_compound_domain (w, doms);
      for (i = 0; i < w; i++)
	ccl_zdelete (armoise_domain_del_reference, doms[i]);
      ccl_delete (doms);
    }

  return result;  
}

			/* --------------- */

/* neg (<F, I, min, max>) -> <F, I, -max, -min> */
static armoise_domain *
s_domain_neg (const armoise_domain *D)
{
  armoise_domain *result;

  if (armoise_domain_is_empty (D))
    result = armoise_domain_create_empty ();
  else
    {
      armoise_domain_attr oldattr = armoise_domain_get_attr (D);
      armoise_domain_attr attr = 
	oldattr & ~(ARMOISE_DOMAIN_HAS_POSITIVES|ARMOISE_DOMAIN_HAS_NEGATIVES);

      if ((oldattr & ARMOISE_DOMAIN_HAS_POSITIVES) != 0)
	attr |= ARMOISE_DOMAIN_HAS_NEGATIVES;

      if ((oldattr & ARMOISE_DOMAIN_HAS_NEGATIVES) != 0)
	attr |= ARMOISE_DOMAIN_HAS_POSITIVES;
      result = armoise_domain_create_with_attr (attr);      
    }

  return result;
}

			/* --------------- */

static armoise_domain *
s_domain_complement (const armoise_domain *D)
{
  armoise_domain *result;

  ccl_pre (armoise_domain_get_width (D) <= 0);

  if (armoise_domain_is_empty (D))
    result = armoise_domain_create ();
  else
    {
      armoise_domain_attr Dattr = armoise_domain_get_attr (D);
      armoise_domain_attr nattr = 0;
      
      /* finite and integer flags are lost by complementation */
      
      if (ONLY_NEG (Dattr))
	nattr = ARMOISE_DOMAIN_HAS_POSITIVES;
      else if (ONLY_POS (Dattr))
	nattr = ARMOISE_DOMAIN_HAS_NEGATIVES;
      else 
	nattr = (ARMOISE_DOMAIN_HAS_NEGATIVES | ARMOISE_DOMAIN_HAS_POSITIVES);
      result = armoise_domain_create_with_attr (nattr);
    }

  return result;
}

			/* --------------- */

static armoise_domain *
s_domain_union (const armoise_domain *D1, const armoise_domain *D2)
{
  armoise_domain *result;

  if (armoise_domain_is_empty (D1))
    result = armoise_domain_duplicate (D2);
  else if (armoise_domain_is_empty (D2))
    result = armoise_domain_duplicate (D1);
  else
    {
      armoise_domain_attr attr1 = armoise_domain_get_attr (D1);
      armoise_domain_attr attr2 = armoise_domain_get_attr (D2);     
      armoise_domain_attr attr = 
	((ARMOISE_DOMAIN_IS_FINITE & attr1 & attr2) |
	 (ARMOISE_DOMAIN_IN_INTEGERS & attr1 & attr2) |
	 (ARMOISE_DOMAIN_HAS_POSITIVES & (attr1 | attr2)) |
	 (ARMOISE_DOMAIN_HAS_NEGATIVES & (attr1 | attr2)));
      result = armoise_domain_create_with_attr (attr);
    }

  return result;
}

			/* --------------- */

static armoise_domain *
s_domain_intersection (const armoise_domain *D1, const armoise_domain *D2)
{
  armoise_domain *result;

  if (armoise_domain_is_empty (D1) || armoise_domain_is_empty (D2))
    result = armoise_domain_create_empty ();
  else
    {
      armoise_domain_attr attr1 = armoise_domain_get_attr (D1);
      armoise_domain_attr attr2 = armoise_domain_get_attr (D2);     
      armoise_domain_attr attr = 
	((ARMOISE_DOMAIN_IS_FINITE & (attr1 | attr2)) |
	 (ARMOISE_DOMAIN_IN_INTEGERS & (attr1 | attr2)) |
	 (ARMOISE_DOMAIN_HAS_POSITIVES & (attr1 & attr2)) |
	 (ARMOISE_DOMAIN_HAS_NEGATIVES & (attr1 & attr2)));
      result = armoise_domain_create_with_attr (attr);
    }

  return result;
}

			/* --------------- */

#define ARITH_ADD 0
#define ARITH_SUB 1
#define ARITH_MUL 2
#define ARITH_DIV 3
#define ARITH_MOD 4

static armoise_domain *
s_domain_arithmetic (const armoise_domain *D1, const armoise_domain *D2, int op)
{
  armoise_domain *result;

  if (armoise_domain_is_empty (D1) || armoise_domain_is_empty (D2))
    result = armoise_domain_create_empty ();
  else if (armoise_domain_get_width (D1) > 0 || 
	   armoise_domain_get_width (D2) > 0)
    {
      int i;
      int w1 = armoise_domain_get_width (D1);
      int w2 = armoise_domain_get_width (D2);
      int w = w1 > w2 ? w1 : w2;
      armoise_domain **domains = ccl_new_array (armoise_domain *, w);

      if (w1  > 0 && w2 > 0)
	{
	  ccl_assert (w1 == w2);
	  for (i = 0; i < w1; i++)
	    {
	      const armoise_domain *d1 = armoise_domain_get_component (D1, i);
	      const armoise_domain *d2 = armoise_domain_get_component (D2, i);
	      domains[i] = s_domain_arithmetic (d1, d2, op);
	    }
	}
      else if (w1 > 0)
	{
	  for (i = 0; i < w1; i++)
	    {
	      const armoise_domain *d1 = armoise_domain_get_component (D1, i);
	      domains[i] = s_domain_arithmetic (d1, D2, op);
	    }
	}
      else 
	{
	  for (i = 0; i < w2; i++)
	    {
	      const armoise_domain *d2 = armoise_domain_get_component (D2, i);
	      domains[i] = s_domain_arithmetic (D1, d2, op);
	    }
	}
      result = armoise_domain_create_compound_domain (w, domains);
      for (i = 0; i < w; i++)
	armoise_domain_del_reference (domains[i]);
      ccl_delete (domains);
    }
  else
    {
      armoise_domain_attr attr = 0;
      armoise_domain_attr attr1 = armoise_domain_get_attr (D1);
      armoise_domain_attr attr2 = armoise_domain_get_attr (D2);

      if ( (attr1 & ARMOISE_DOMAIN_IS_FINITE) != 0 &&
	   (attr2 & ARMOISE_DOMAIN_IS_FINITE) != 0)
	attr |= ARMOISE_DOMAIN_IS_FINITE;

      if ( (attr1 & ARMOISE_DOMAIN_IN_INTEGERS) != 0 &&
	   (attr2 & ARMOISE_DOMAIN_IN_INTEGERS) != 0 && 
	   op != ARITH_DIV && op != ARITH_MOD)
	attr |= ARMOISE_DOMAIN_IN_INTEGERS;

      if (op == ARITH_ADD)
	{
	  if ( (attr1 & ARMOISE_DOMAIN_HAS_POSITIVES) != 0 || 
	       (attr2 & ARMOISE_DOMAIN_HAS_POSITIVES) != 0)
	    attr |= ARMOISE_DOMAIN_HAS_POSITIVES;
	  
	  if ( (attr1 & ARMOISE_DOMAIN_HAS_NEGATIVES) != 0 || 
	       (attr2 & ARMOISE_DOMAIN_HAS_NEGATIVES) != 0)
	    attr |= ARMOISE_DOMAIN_HAS_NEGATIVES;
	}
      else if (op == ARITH_SUB)
	{
	  if (ONLY_NEG (attr1) && (ONLY_POS (attr2) || ZERO (attr2)))
	    attr |= ARMOISE_DOMAIN_HAS_NEGATIVES;
	  else if (ONLY_POS (attr1) && (ONLY_NEG (attr2) || ZERO (attr2)))
	    attr |= ARMOISE_DOMAIN_HAS_POSITIVES;
	  else if (ZERO (attr1) && ONLY_POS (attr2))
	    attr |= ARMOISE_DOMAIN_HAS_NEGATIVES;
	  else if (ZERO (attr1) && ONLY_NEG (attr2))
	    attr |= ARMOISE_DOMAIN_HAS_POSITIVES;
	  else
	    attr |= (ARMOISE_DOMAIN_HAS_POSITIVES | 
		     ARMOISE_DOMAIN_HAS_NEGATIVES);
	}
      else
	{
	  if ((ONLY_NEG(attr1) && ONLY_NEG(attr2)) ||
	      (ONLY_POS(attr1) && ONLY_POS(attr2)))
	    attr |= ARMOISE_DOMAIN_HAS_POSITIVES;
	  else if ((ONLY_POS(attr1) && ONLY_NEG(attr2)) ||
		   (ONLY_NEG(attr1) && ONLY_POS(attr2)))
	    attr |= ARMOISE_DOMAIN_HAS_NEGATIVES;
	  else
	    {
	      attr |= (attr1 & 
		       (ARMOISE_DOMAIN_HAS_POSITIVES | 
			ARMOISE_DOMAIN_HAS_NEGATIVES));
	      attr |= (attr2 & 
		       (ARMOISE_DOMAIN_HAS_POSITIVES | 
			ARMOISE_DOMAIN_HAS_NEGATIVES));
	    }
	}

      result = armoise_domain_create_with_attr (attr);
    }
  
  return result;
}
			/* --------------- */

static armoise_domain *
s_domain_add (const armoise_domain *D1, const armoise_domain *D2)
{
  return s_domain_arithmetic (D1, D2, ARITH_ADD);
}

			/* --------------- */

static armoise_domain *
s_domain_sub (const armoise_domain *D1, const armoise_domain *D2)
{
  return s_domain_arithmetic (D1, D2, ARITH_SUB);
}

			/* --------------- */

static armoise_domain *
s_domain_mul (const armoise_domain *D1, const armoise_domain *D2)
{
  return s_domain_arithmetic (D1, D2, ARITH_MUL);
}

			/* --------------- */

static armoise_domain *
s_domain_div (const armoise_domain *D1, const armoise_domain *D2)
{
  return s_domain_arithmetic (D1, D2, ARITH_DIV);
}

			/* --------------- */

static armoise_domain *
s_domain_mod (const armoise_domain *D1, const armoise_domain *D2)
{
  return s_domain_arithmetic (D1, D2, ARITH_MOD);
}

			/* --------------- */

static armoise_domain *
s_type_to_domain (const armoise_type *type)
{  
  armoise_domain *result;
  int dim = armoise_type_get_width (type);

  if (dim == 0)
    result = armoise_domain_create ();
  else
    {
      int i;
      armoise_domain **subdoms = ccl_new_array (armoise_domain *, dim);

      for (i = 0; i < dim; i++)
	{
	  const armoise_type *t = armoise_type_get_subtype (type, i);
	  subdoms[i] = s_type_to_domain (t);
	}      
      
      result = armoise_domain_create_compound_domain (dim, subdoms);

      for (i = 0; i < dim; i++)
	armoise_domain_del_reference (subdoms[i]);
      ccl_delete (subdoms);
    }

  return result;
}

			/* --------------- */

static ccl_hash *
s_domain_assignment_dup (ccl_hash *t)
{
  ccl_hash *result = 
    ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
		     armoise_domain_del_reference);
  ccl_hash_entry_iterator *i = ccl_hash_get_entries (t);

  while (ccl_iterator_has_more_elements (i))   
    {
      ccl_hash_entry e = ccl_iterator_next_element (i);

      ccl_hash_find (result, e.key);
      ccl_hash_insert (result, armoise_domain_duplicate (e.object));
    }
  ccl_iterator_delete (i);

  return result;
}

			/* --------------- */

static ccl_hash *
s_domain_assignment_make_op (ccl_hash *t1, ccl_hash *t2,
			     armoise_domain *(*op)(const armoise_domain *,
						   const armoise_domain *))
{
  ccl_hash *result = s_domain_assignment_dup (t1);
  ccl_hash_entry_iterator *i = ccl_hash_get_entries (t2);

  while (ccl_iterator_has_more_elements (i))   
    {
      ccl_hash_entry e = ccl_iterator_next_element (i);

      if (ccl_hash_find (result, e.key))
	{
	  armoise_domain *d = ccl_hash_get (result);
	  armoise_domain *ud = s_apply (d, e.object, op);

	  ccl_hash_insert (result, ud);
	}
      else
	{
	  ccl_hash_insert (result, armoise_domain_duplicate (e.object));
	}
    }
  ccl_iterator_delete (i);

  return result;
}

			/* --------------- */

static ccl_hash *
s_domain_assignment_union (ccl_hash *t1, ccl_hash *t2)
{
  ccl_hash *result;

  if (t1 == NULL && t2 == NULL)
    result = NULL;
  else if (t1 == NULL || ccl_hash_get_size (t1) == 0)
    result = s_domain_assignment_dup (t2);
  else if (t2 == NULL || ccl_hash_get_size (t2) == 0)
    result = s_domain_assignment_dup (t1);
  else
    result = s_domain_assignment_make_op (t1, t2, s_domain_union);

  return result;
}

			/* --------------- */

static ccl_hash *
s_domain_assignment_intersection (ccl_hash *t1, ccl_hash *t2)
{
  ccl_hash *result = NULL;

  if (t1 != NULL && t2 != NULL)
    result = s_domain_assignment_make_op (t1, t2, s_domain_intersection);

  return result;
}

			/* --------------- */

static void
s_domain_assignment_remove_variables (ccl_hash *H, int nb_vars, 
				      armoise_variable **vars)
{
  int i;

  for (i = 0; i < nb_vars; i++)
    {
      if (ccl_hash_find (H, vars[i]))
	ccl_hash_remove (H);
    }
}

			/* --------------- */

static armoise_domain *
s_compute_domain_for_term (armoise_term *t, ccl_hash *doms)
{
  int i;
  armoise_domain *result;
  armoise_term **operands = t->c.operands.terms;
  armoise_domain **dom_operands;
  int nb_operands = t->c.operands.nb_op;

  switch (t->kind)
    {
    case ARMOISE_T_VARIABLE:
      ccl_assert (doms != NULL);
      if (ccl_hash_find (doms, t->c.variable))
	{
	  result = ccl_hash_get (doms);
	  result = armoise_domain_duplicate (result);
	}
      else
	{
	  result = s_type_to_domain (t->type);
	}
      break;

    case ARMOISE_T_CONSTANT:
      {
	armoise_domain_attr attr = ARMOISE_DOMAIN_IS_FINITE;

	if (t->c.value.den == 1)
	  attr |= ARMOISE_DOMAIN_IN_INTEGERS;

	if (t->c.value.num > 0)
	  attr |= ARMOISE_DOMAIN_HAS_POSITIVES;
	else if (t->c.value.num < 0)
	  attr |= ARMOISE_DOMAIN_HAS_NEGATIVES;
	result = armoise_domain_create_with_attr (attr);
      }
      break;

    case ARMOISE_T_ELEMENT:
      {
	armoise_domain *dom = 
	  s_compute_domain_for_term (t->c.vec_element.variable, doms);
	if (! armoise_domain_is_empty (dom))
	  {
	    const armoise_domain *d = 
	      armoise_domain_get_component (dom, t->c.vec_element.index);
	    result = armoise_domain_duplicate (d);
	  }
	else
	  {
	    result = armoise_domain_create_empty ();
	  }
	armoise_domain_del_reference (dom);
      }
      break;

    case ARMOISE_T_NEG:
    case ARMOISE_T_VECTOR:
    case ARMOISE_T_ADD:
    case ARMOISE_T_SUB:
    case ARMOISE_T_MUL:
    case ARMOISE_T_DIV:
    case ARMOISE_T_MOD:
      dom_operands = ccl_new_array (armoise_domain *, nb_operands);      
      for (i = 0; i < nb_operands; i++)
	dom_operands[i] = s_compute_domain_for_term (operands[i], doms);
      
      if (t->kind == ARMOISE_T_NEG)
	result = s_apply_unary (dom_operands[0], s_domain_neg);
      else if (t->kind == ARMOISE_T_VECTOR)
	result = 
	  armoise_domain_create_compound_domain (nb_operands, dom_operands);
      else 
	{
	  armoise_domain *
	    (*apply) (const armoise_domain *, const armoise_domain *,
		      armoise_domain * (*) (const armoise_domain *, 
					    const armoise_domain *)) =
	    s_apply;

	  armoise_domain * (*op) (const armoise_domain *, 
				  const armoise_domain *);
	  switch (t->kind)
	    {
	    case ARMOISE_T_ADD: op = s_domain_add; break;
	    case ARMOISE_T_SUB: op = s_domain_sub; break; 
	    case ARMOISE_T_MUL: op = s_domain_mul; apply = s_apply_mul; break; 
	    case ARMOISE_T_DIV: op = s_domain_div; apply = s_apply_mul; break; 
	    default:
	      ccl_assert (t->kind == ARMOISE_T_MOD);
	      op = s_domain_mod; 
	      apply = s_apply_mul; 
	      break; 
	    }

	  ccl_assert (op != NULL);	  
	  result = armoise_domain_add_reference (dom_operands[0]);
	  for (i = 1; i < nb_operands; i++)
	    {
	      armoise_domain *d = apply (result, dom_operands[i], op);
	      armoise_domain_del_reference (result);
	      result = d;
	    }
	}
      
      for (i = 0; i < nb_operands; i++)
	armoise_domain_del_reference (dom_operands[i]);
      ccl_delete (dom_operands);
      break;
    }
  
  return result;
}

			/* --------------- */

static void
s_restrict_domains_for_add (armoise_domain **d1, armoise_domain **d2, 
			    const armoise_domain *D);

static void
s_restrict_domains_for_sub (armoise_domain **d1, armoise_domain **d2, 
			    const armoise_domain *D);

static void
s_restrict_domains_for_mul (armoise_domain **d1, armoise_domain **d2, 
			    const armoise_domain *D);

static void
s_restrict_domains_for_div (armoise_domain **d1, armoise_domain **d2, 
			    const armoise_domain *D);

static void
s_restrict_domains_for_mod (armoise_domain **d1, armoise_domain **d2, 
			    const armoise_domain *D);

static void
s_restrict_domain_for_neg (armoise_domain **d, const armoise_domain *D);

			/* --------------- */

static ccl_hash *
s_assign_domain_to_variable_for_term (armoise_term *t, int index, 
				      const armoise_domain *D, 
				      ccl_hash *current);

static ccl_hash *
s_propagate_compound_domain (armoise_term *t, const armoise_domain *D, 
			     ccl_hash *current)
{
  int i;
  ccl_hash *h[2];
  int w = armoise_domain_get_width (D);
  const armoise_domain *d = armoise_domain_get_component (D, 0);
  ccl_hash *result = s_assign_domain_to_variable_for_term (t, 0, d, current);

  for (i = 1; i < w && result != NULL; i++)
    {
      d = armoise_domain_get_component (D, i);
      h[0] = s_assign_domain_to_variable_for_term (t, i, d, current);
      h[1] = s_domain_assignment_intersection (result, h[0]);
      ccl_hash_delete (result);
      ccl_hash_delete (h[0]);
      result = h[1];
    }

  return result;
}

			/* --------------- */

static ccl_hash *
s_assign_domain_to_variable_for_term (armoise_term *t, int index, 
				      const armoise_domain *D, 
				      ccl_hash *current)
{
  armoise_term **operands = t->c.operands.terms;
  int w = armoise_domain_get_width (D);
  ccl_hash *result = NULL;
  
  if (armoise_domain_is_empty (D))
    return NULL;

  switch (t->kind)
    {
    case ARMOISE_T_ELEMENT:
      {
	armoise_term *var = t->c.vec_element.variable;
	int comp = t->c.vec_element.index;

	if (index >= 0)
	  {
	    armoise_domain *Y = s_compute_domain_for_term (var, current);  
	    const armoise_domain *aux = armoise_domain_get_component (Y, comp);
	    armoise_domain *X = armoise_domain_duplicate (aux);
	    armoise_domain *Xj = 
	      s_apply (armoise_domain_get_component (aux, index), D, 
		       s_domain_intersection);
	    armoise_domain_set_component (X, index, Xj);
	    armoise_domain_del_reference (Xj);
	    armoise_domain_del_reference (Y);
	    result =
 	      s_assign_domain_to_variable_for_term (var, comp, X, current);
	    armoise_domain_del_reference (X);
	  }
	else
	  {
	    result = 
	      s_assign_domain_to_variable_for_term (var, comp, D, current);
	  }
      }
      break;

    case ARMOISE_T_VARIABLE:
      {
	armoise_domain *tmp;
	armoise_domain *DV;

	if (ccl_hash_find (current, t->c.variable))
	  tmp = armoise_domain_duplicate (ccl_hash_get (current));
	else
	  tmp = s_type_to_domain (t->type);

	if (armoise_domain_is_empty (tmp))
	  DV = armoise_domain_create_empty ();
	else if (index >= 0)
	  {
	    const armoise_domain *tmp_i = 
	      armoise_domain_get_component (tmp, index);
	    armoise_domain *tmp_D = s_apply (tmp_i, D, s_domain_intersection);

	    armoise_domain_set_component (tmp, index, tmp_D);
	    DV = tmp;
	    armoise_domain_del_reference (tmp_D);
	  }
	else
	  {	  
	    DV = s_apply (tmp, D, s_domain_intersection);
	    armoise_domain_del_reference (tmp);
	  }

	if (armoise_domain_is_empty (DV))
	  armoise_domain_del_reference (DV);	    
	else
	  {
	    result = s_domain_assignment_dup (current);
	    ccl_hash_find (result, t->c.variable);
	    ccl_hash_insert (result, DV);
	  }
      }
      break;

    case ARMOISE_T_CONSTANT:
      ccl_pre (w == 0);
      result = s_domain_assignment_dup (current);
      break;    
      
    case ARMOISE_T_VECTOR:
      if (index >= 0)
	{
	  ccl_assert (index < t->c.operands.nb_op);
	  result = 
	    s_assign_domain_to_variable_for_term (operands[index], -1, D, 
						  current);
	}
      else
	{
	  result = s_propagate_compound_domain (t, D, current);
	}
      break;

    case ARMOISE_T_ADD: case ARMOISE_T_SUB:
      if (w >= 1 && index < 0)
	{
	  result = s_propagate_compound_domain (t, D, current);
	}
      else 
	{
	  ccl_hash *h1;
	  ccl_hash *h2;
	  armoise_domain *d1 = s_compute_domain_for_term (operands[0], current);
	  armoise_domain *d2 = s_compute_domain_for_term (operands[1], current);
	  ccl_assert (armoise_term_get_nb_operands (t) == 2);
	  if (index >= 0)
	    {
	      armoise_domain *d1_i = 
		armoise_domain_duplicate (armoise_domain_get_component (d1, 
									index));
	      armoise_domain *d2_i = 
		armoise_domain_duplicate (armoise_domain_get_component (d2, 
									index));
	      armoise_domain_del_reference (d1);
	      armoise_domain_del_reference (d2);
	      d1 = d1_i;
	      d2 = d2_i;
	    }
	  
	  if (t->kind == ARMOISE_T_ADD)
	    s_restrict_domains_for_add (&d1, &d2, D);
	  else
	    s_restrict_domains_for_sub (&d1, &d2, D);
	  h1 = s_assign_domain_to_variable_for_term (operands[0], index, d1,
						     current);
	  h2 = s_assign_domain_to_variable_for_term (operands[1], index, d2,
						     current);
	  result = s_domain_assignment_intersection (h1, h2);
	  ccl_zdelete (ccl_hash_delete, h1);
	  ccl_zdelete (ccl_hash_delete, h2);
	  armoise_domain_del_reference (d1);
	  armoise_domain_del_reference (d2);
	}      
      break;

    case ARMOISE_T_MUL:
    case ARMOISE_T_DIV:
    case ARMOISE_T_MOD:
      if (w >= 1 && index < 0)
	{
	  result = s_propagate_compound_domain (t, D, current);
	}
      else
	{
	  ccl_hash *h1;
	  ccl_hash *h2;
	  armoise_term *t1;
	  armoise_domain *d1;
	  armoise_term *t2;
	  armoise_domain *d2;
	  void (*restrict_doms) (armoise_domain **, armoise_domain **, 
				 const armoise_domain *);

	  ccl_assert (armoise_term_get_nb_operands (t) == 2);

	  if (t->kind == ARMOISE_T_MUL)
	    restrict_doms = s_restrict_domains_for_mul;
	  else if (t->kind == ARMOISE_T_DIV)
	    restrict_doms = s_restrict_domains_for_div;
	  else
	    restrict_doms = s_restrict_domains_for_mod;

	  if (armoise_type_get_width (operands[1]->type) >= 1)
	    { 
	      t1 = operands[1];
	      t2 = operands[0];
	    }
	  else
	    {
	      t1 = operands[0];
	      t2 = operands[1];
	    }
	  d1 = s_compute_domain_for_term (t1, current);
	  d2 = s_compute_domain_for_term (t2, current);

	  if (index >= 0)
	    {
	      armoise_domain *d1_i = 
		armoise_domain_duplicate (armoise_domain_get_component (d1, 
									index));
	      armoise_domain_del_reference (d1);	    
	      d1 = d1_i;
	    }

	  restrict_doms (&d1, &d2, D);
	  h1 = s_assign_domain_to_variable_for_term (t1, index, d1, current);
	  h2 = s_assign_domain_to_variable_for_term (t2, -1, d2, current);
	  result = s_domain_assignment_intersection (h1, h2);
	  ccl_zdelete (ccl_hash_delete, h1);
	  ccl_zdelete (ccl_hash_delete, h2);
	  armoise_domain_del_reference (d1);
	  armoise_domain_del_reference (d2);
	}
      break;

    default:
      ccl_assert (t->kind == ARMOISE_T_NEG);
      if (w >= 1 && index < 0)
	{
	  result = s_propagate_compound_domain (t, D, current);
	}
      else 
	{
	  armoise_domain *d = s_compute_domain_for_term (operands[0], current);

	  if (index >= 0)
	    {
	      armoise_domain *d_i = 
		armoise_domain_duplicate (armoise_domain_get_component (d, 
									index));
	      armoise_domain_del_reference (d);
	      d = d_i;
	    }

	  s_restrict_domain_for_neg (&d, D);	  
	  result = s_assign_domain_to_variable_for_term (operands[0], index,
							 d, current);
	  armoise_domain_del_reference (d);
	}
      break;
    }
  
  return result;
}

			/* --------------- */

static int
s_same_assignments (ccl_hash *a1, ccl_hash *a2)
{
  int result;
  ccl_hash_entry_iterator *i;

  if (ccl_hash_get_size (a1) != ccl_hash_get_size (a2))
    result = 0;
  else
    {
      result = 1;
      i = ccl_hash_get_entries (a1);
      while (ccl_iterator_has_more_elements (i) && result)
	{
	  ccl_hash_entry e = ccl_iterator_next_element (i);
	  result = ccl_hash_find (a2, e.key);
	  if (result)
	    {
	      armoise_domain *d = ccl_hash_get (a2);
	      result = armoise_domain_equals (e.object, d);
	    }
	}
      ccl_iterator_delete (i);
    }

  return result;
}

			/* --------------- */

static void
s_assign_domain_to_proto (const armoise_term *t, ccl_hash *a)
{
  if (armoise_term_get_kind (t) == ARMOISE_T_VARIABLE)
    {
      armoise_variable *var = armoise_term_get_variable (t);
      const armoise_domain *vardom = armoise_variable_get_domain (var);

      if (! ccl_hash_find (a, var))
	ccl_hash_insert (a, armoise_domain_duplicate (vardom));
    }
  else
    {
      int i;
      int width = armoise_term_get_arity (t);
      ccl_assert (armoise_term_get_kind (t) == ARMOISE_T_VECTOR);
      
      for (i = 0; i < width; i++)
	{
	  const armoise_term *op = armoise_term_get_operand (t, i);
	  s_assign_domain_to_proto (op, a);
	}
    }
}

			/* --------------- */



static ccl_hash *
s_reassign_domain_to_variable_for_formula (armoise_formula *F, int value,
					   ccl_hash *current)
{
  int i;
  ccl_hash *result = NULL;
  armoise_formula **operands = F->c.operands.formulas;
  int nb_operands = F->c.operands.nb_op;
  ccl_hash * (*op) (ccl_hash *, ccl_hash *);

  switch (F->kind)
    {
    case ARMOISE_F_CONSTANT:
      result = s_domain_assignment_dup (current);
      break;

    case ARMOISE_F_NOT:
      result = s_reassign_domain_to_variable_for_formula (operands[0], !value, 
							  current);
      break;

    case ARMOISE_F_AND:
    case ARMOISE_F_OR:
      if (F->kind == ARMOISE_F_AND)
	{
	  if (value) 
	    op = s_domain_assignment_intersection;
	  else 
	    op = s_domain_assignment_union;
	}
      else
	{
	  if (value) 
	    op = s_domain_assignment_union;
	  else
	    op = s_domain_assignment_intersection;
	}

      result = s_reassign_domain_to_variable_for_formula (operands[0], value, 
							  current);
      for (i = 1; i < nb_operands; i++)
	{
	  ccl_hash *t1;
	  ccl_hash *t2;

	  t1 = s_reassign_domain_to_variable_for_formula (operands[i], value, 
							  current);
	  t2 = op (result, t1);
	  ccl_zdelete (ccl_hash_delete, t1);
	  ccl_zdelete (ccl_hash_delete, result);
	  result = t2;
	  if (result == NULL && op == s_domain_assignment_intersection)
	    break;
	}
      break;

    case ARMOISE_F_EXISTS:
    case ARMOISE_F_FORALL:
      {
	int stop = 0;

	result = s_domain_assignment_dup (current);
	s_assign_domain_to_proto (F->c.q.qvars, result);

	while (!stop && result != NULL)
	  {
	    ccl_hash *tmp = 
	      s_reassign_domain_to_variable_for_formula (F->c.q.f, value, 
							 result);
	    if (tmp != NULL)
	      {
		stop = (s_same_assignments (result, tmp));
		ccl_hash_delete (result);
	      }
	    result = tmp;
	  }
      }
      break;

    case ARMOISE_F_PAREN:
      result = s_reassign_domain_to_variable_for_formula (F->c.f, value, 
							  current);
      break;

    case ARMOISE_F_IMPLY:
      {
	ccl_hash *a[2];

	a[0] = s_reassign_domain_to_variable_for_formula (operands[0], !value, 
							  current);
	a[1] = s_reassign_domain_to_variable_for_formula (operands[1], value, 
							  current);
	if (value)
	  result = s_domain_assignment_union (a[0], a[1]);
	else
	  result = s_domain_assignment_intersection (a[0], a[1]);
	ccl_zdelete (ccl_hash_delete, a[0]);
	ccl_zdelete (ccl_hash_delete, a[1]);
      }
      break;

    case ARMOISE_F_EQUIV:
      {
	int i;
	ccl_hash *a[6];

	ccl_assert (F->c.operands.nb_op == 2);
	a[0] = s_reassign_domain_to_variable_for_formula (operands[0], 0, 
							  current);
	a[1] = s_reassign_domain_to_variable_for_formula (operands[1], 0, 
							  current);
	a[2] = s_reassign_domain_to_variable_for_formula (operands[0], 1, 
							  current);
	a[3] = s_reassign_domain_to_variable_for_formula (operands[1], 1, 
							  current);
	if (value)
	  {
	    a[4] = s_domain_assignment_intersection (a[0], a[1]);
	    a[5] = s_domain_assignment_intersection (a[2], a[3]);
	    result = s_domain_assignment_union (a[4], a[5]);
	  }
	else
	  {
	    a[4] = s_domain_assignment_union (a[0], a[1]);
	    a[5] = s_domain_assignment_union (a[2], a[3]);
	    result = s_domain_assignment_intersection (a[4], a[5]);
	  }
	for (i = 0; i < sizeof (a) / sizeof (a[0]); i++)
	  ccl_zdelete (ccl_hash_delete, a[i]);
      }
      break;

    case ARMOISE_F_IN:
      {
	armoise_domain *D;

	armoise_predicate_compute_domain (F->c.in.P);
	if (value)
	  D = armoise_domain_add_reference (F->c.in.P->domain);
	else
	  D = s_apply_unary (F->c.in.P->domain, s_domain_complement);

	result = 
	  s_assign_domain_to_variable_for_term (F->c.in.t, -1, D, current);
	armoise_domain_del_reference (D);
      }
      break;

    case ARMOISE_F_EQ:   
    case ARMOISE_F_NEQ:
      {
	armoise_term *t1 = F->c.cmp.t1;
	armoise_term *t2 = F->c.cmp.t2;
	armoise_domain *d1 = s_compute_domain_for_term (t1, current);
	armoise_domain *d2 = s_compute_domain_for_term (t2, current);

	if ((F->kind == ARMOISE_F_EQ && value) || 
	    (F->kind == ARMOISE_F_NEQ && !value))
	  {
	    armoise_domain *d = s_apply (d1, d2, s_domain_intersection);

	    if (! armoise_domain_is_empty (d))
	      {
		ccl_hash *h1 = 
		  s_assign_domain_to_variable_for_term (t1, -1, d, current);
		ccl_hash *h2 = 
		  s_assign_domain_to_variable_for_term (t2, -1, d, current);
		result = s_domain_assignment_intersection (h1, h2);
		ccl_zdelete (ccl_hash_delete, h1);
		ccl_zdelete (ccl_hash_delete, h2);
	      }
	    armoise_domain_del_reference (d);
	  }
	else
	  {
	    result = s_domain_assignment_dup (current);
	  }
	armoise_domain_del_reference (d1);
	armoise_domain_del_reference (d2);
      }
      break;

    case ARMOISE_F_GEQ:
    case ARMOISE_F_GT:
    case ARMOISE_F_LEQ:   
    case ARMOISE_F_LT:   
      {
	ccl_hash *tmp;
	armoise_term *t1 = F->c.cmp.t1;
	armoise_term *t2 = F->c.cmp.t2;
	armoise_domain *d1 = s_compute_domain_for_term (t1, current);
	armoise_domain_attr attr1 = armoise_domain_get_attr (d1);
	armoise_domain *d2 = s_compute_domain_for_term (t2, current);
	armoise_domain_attr attr2 = armoise_domain_get_attr (d2);

	if (! HAS_NEG (attr1) && 
	    (F->kind == ARMOISE_F_LEQ || F->kind == ARMOISE_F_LT))	  
	  attr2 &= ~ARMOISE_DOMAIN_HAS_NEGATIVES;

	if (! HAS_NEG (attr2) && 
	    (F->kind == ARMOISE_F_GEQ || F->kind == ARMOISE_F_GT))
	  attr1 &= ~ARMOISE_DOMAIN_HAS_NEGATIVES;

	if (! HAS_POS (attr2) && 
	    (F->kind == ARMOISE_F_LEQ || F->kind == ARMOISE_F_LT))
	  attr1 &= ~ARMOISE_DOMAIN_HAS_POSITIVES;

	if (! HAS_POS (attr1) && 
	    (F->kind == ARMOISE_F_GEQ || F->kind == ARMOISE_F_GT))
	  attr2 &= ~ARMOISE_DOMAIN_HAS_POSITIVES;

	armoise_domain_set_attr (d1, attr1);
	armoise_domain_set_attr (d2, attr2);
	tmp = s_assign_domain_to_variable_for_term (t1, -1, d1, current);
	result = s_assign_domain_to_variable_for_term (t2, -1, d2, tmp);
	ccl_hash_delete (tmp);
	armoise_domain_del_reference (d1);
	armoise_domain_del_reference (d2);
      }
      break;
    }

  return result;
}

			/* --------------- */

static void
s_log_domain_assignments (ccl_log_type log, ccl_hash *a)
{
  ccl_hash_entry_iterator *i = ccl_hash_get_entries (a);

  while (ccl_iterator_has_more_elements (i))
    {
      ccl_hash_entry e = ccl_iterator_next_element (i);

      armoise_variable_log (log, e.key);
      ccl_log (log, " --> ");
      armoise_domain_log (log, e.object);
      ccl_log (log, "\n");
    }
  ccl_iterator_delete (i);
}

			/* --------------- */

static ccl_hash *
s_assign_domain_to_variable_for_formula (armoise_predicate *P, int value)
{
  int stop = 0;
  ccl_hash *result = ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
				      armoise_domain_del_reference);
  s_assign_domain_to_proto (P->c.set.proto, result);

  while (!stop && result != NULL)
    {
      ccl_hash *tmp = 
	s_reassign_domain_to_variable_for_formula (P->c.set.def, value, 
						   result);      
      if (tmp != NULL)
	stop = (s_same_assignments (result, tmp));
      ccl_hash_delete (result);
      result = tmp;
    }

  return result;
}

			/* --------------- */

static void
s_solve_domain_for_set (armoise_predicate *P)
{
  ccl_hash *doms = s_assign_domain_to_variable_for_formula (P, 1);

  if (doms == NULL)
    {
      P->domain = armoise_domain_create_empty ();
    }
  else
    {
      ccl_hash_entry_iterator *i = ccl_hash_get_entries (doms);
      P->domain = s_compute_domain_for_term (P->c.set.proto, doms);
      
      while (ccl_iterator_has_more_elements (i))
	{
	  ccl_hash_entry e = ccl_iterator_next_element (i);
	  armoise_variable_set_domain ((armoise_variable *)e.key, 
				       (armoise_domain *)e.object);
	}
      ccl_iterator_delete (i);
      ccl_hash_delete (doms);  
    }
}


			/* --------------- */

static void
s_restrict_domain_for_neg (armoise_domain **d, const armoise_domain *D)
{
  armoise_domain *tmp[2];

  tmp[0] = s_apply_unary (D, s_domain_neg);
  tmp[1] = s_domain_intersection (tmp[0], *d);
  armoise_domain_del_reference (*d);
  armoise_domain_del_reference (tmp[0]);

  *d = tmp[1];
}

			/* --------------- */

static void
s_restrict_domains_for_add (armoise_domain **d1, armoise_domain **d2, 
			    const armoise_domain *D)
{  
  armoise_domain *tmp[2];

  /* d1 <- d1 /\ (D-d2) */
  tmp[0] = s_apply (D, *d2, s_domain_sub);
  tmp[1] = s_apply (*d1, tmp[0], s_domain_intersection);
  armoise_domain_del_reference (tmp[0]);
  armoise_domain_del_reference (*d1);
  *d1 = tmp[1];

  /* d2 <- d2 /\ (D-d1) */
  tmp[0] = s_apply (D, *d1, s_domain_sub);
  tmp[1] = s_apply (*d2, tmp[0], s_domain_intersection);
  armoise_domain_del_reference (tmp[0]);
  armoise_domain_del_reference (*d2);
  *d2 = tmp[1];
}

			/* --------------- */

static void
s_restrict_domains_for_sub (armoise_domain **d1, armoise_domain **d2, 
			    const armoise_domain *D)
{
  armoise_domain *tmp[2];

  /* d1 <- d1 /\ (D+d2) */
  tmp[0] = s_apply (D, *d2, s_domain_add);
  tmp[1] = s_apply (*d1, tmp[0], s_domain_intersection);
  armoise_domain_del_reference (tmp[0]);
  armoise_domain_del_reference (*d1);
  *d1 = tmp[1];

  /* d2 <- d2 /\ (d1-D) */
  tmp[0] = s_apply (*d1, D, s_domain_sub);
  tmp[1] = s_apply (*d2, tmp[0], s_domain_intersection);
  armoise_domain_del_reference (tmp[0]);
  armoise_domain_del_reference (*d2);
  *d2 = tmp[1];
}

			/* --------------- */

static void
s_restrict_domains_for_mul_op (armoise_domain **d1, armoise_domain **d2, 
			       const armoise_domain *D,
			       void (*filter)(armoise_domain **d1, 
					      armoise_domain **d2, 
					      const armoise_domain *D))
			    
{
  int w1 = armoise_domain_get_width (*d1);
  int w2 = armoise_domain_get_width (*d2);

  if (armoise_domain_is_empty (*d1) || armoise_domain_is_empty (*d2))
    return;

  if (w1 == 0 && w2 == 0)
    filter (d1, d2, D);
  else 
    {
      int i;
      armoise_domain **subdoms;

      if (w2 < w1)	
	{
	  int w = w1;
	  armoise_domain **d = d1;
	  w1 = w2;
	  d1 = d2;
	  w2 = w;
	  d2 = d;
	}

      subdoms = ccl_new_array (armoise_domain *, w2);
      for (i = 0; i < w2; i++)
	{
	  const armoise_domain *cd2 = armoise_domain_get_component (*d2, i);
	  const armoise_domain *cD = armoise_domain_get_component (D, i);
	  subdoms[i] = armoise_domain_duplicate (cd2);
	  s_restrict_domains_for_mul_op (d1, &subdoms[i], cD, filter);
	  if (armoise_domain_is_empty (subdoms[i]))
	    break;
	}
      armoise_domain_del_reference (*d2);
      if (i < w2)
	*d2 = armoise_domain_create_empty ();
      else
	*d2 = armoise_domain_create_compound_domain (w2, subdoms);
      for (i = 0; i < w2; i++)
	ccl_zdelete (armoise_domain_del_reference, subdoms[i]);
      ccl_delete (subdoms);
    }
}

			/* --------------- */

static void
s_restrict_mul (armoise_domain **d1, armoise_domain **d2, 
		const armoise_domain *D)
{
  int in_integers = (armoise_domain_is_restricted_to_integers (D) &&
		     (armoise_domain_is_restricted_to_integers (*d1) &&
		      armoise_domain_is_restricted_to_integers (*d2)));
  armoise_domain_attr attrD = armoise_domain_get_attr (D);
  armoise_domain_attr attr1 = armoise_domain_get_attr (*d1);
  armoise_domain_attr attr2 = armoise_domain_get_attr (*d2);

  if (ONLY_POS (attr1) && ONLY_POS (attrD))
    attr2 &= ~ARMOISE_DOMAIN_HAS_NEGATIVES;

  if (ONLY_POS (attr2) && ONLY_POS (attrD))
    attr1 &= ~ARMOISE_DOMAIN_HAS_NEGATIVES;

  if (in_integers)
    {
      attr1 |= ARMOISE_DOMAIN_IN_INTEGERS;
      attr2 |= ARMOISE_DOMAIN_IN_INTEGERS;
    }

  armoise_domain_set_attr (*d1, attr1);
  armoise_domain_set_attr (*d2, attr2);
}

			/* --------------- */

static void
s_restrict_domains_for_mul (armoise_domain **d1, armoise_domain **d2, 
			   const armoise_domain *D)
{
  s_restrict_domains_for_mul_op (d1, d2, D, s_restrict_mul);
}

			/* --------------- */

static void
s_restrict_div (armoise_domain **d1, armoise_domain **d2, 
		const armoise_domain *D)
{
}

			/* --------------- */

static void
s_restrict_domains_for_div (armoise_domain **d1, armoise_domain **d2, 
			    const armoise_domain *D)
{
  s_restrict_domains_for_mul_op (d1, d2, D, s_restrict_div);
}

			/* --------------- */

static void
s_restrict_domains_for_mod (armoise_domain **d1, armoise_domain **d2, 
			    const armoise_domain *D)
{
}

			/* --------------- */

