/*
 * armoise-tree.h -- Data structure encoding the syntactic tree for ARMOISE 
 * Copyright (C) 2007 J. Leroux, G. Point, LaBRI, CNRS UMR 5800, 
 * Universite Bordeaux I
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */
/*
 * $Author: point $
 * $Revision: 1.6 $
 * $Date: 2008/01/24 16:20:53 $
 * $Id: armoise-tree.h,v 1.6 2008/01/24 16:20:53 point Exp $
 */

/*!
 * \file armoise/armoise-tree.h
 * \brief Syntactic tree for the Armoise language
 *
 * This file presents essentially the identifiers used to label the 
 * \ref ccl_parse_tree encoding an ARMOISE syntactic tree.
 */
#ifndef __ARMOISE_TREE_H__
# define __ARMOISE_TREE_H__

# include <ccl/ccl-log.h>
# include <ccl/ccl-parse-tree.h>

BEGIN_C_DECLS

/*!
 * \brief The data-structure encoding the syntactic tree of the ARMOISE 
 * language. 
 *
 * Internal fields are freely accessible to the client code. Have a look to
 * the \ref ccl_parse_tree data structure for a documentation on nodes.
 */
typedef ccl_parse_tree armoise_tree;

enum armoise_label {
   ARMOISE_TREE_DEFINITION,
    ARMOISE_TREE_ID_TUPLE, 
    ARMOISE_TREE_PREDICATE_TUPLE,   
    ARMOISE_TREE_PREDICATE,
     ARMOISE_TREE_PREDICATE_CONTEXT,
      ARMOISE_TREE_UNION,
      ARMOISE_TREE_DIFFERENCE,
      ARMOISE_TREE_XOR,
      ARMOISE_TREE_INTERSECTION,
      ARMOISE_TREE_COMPLEMENT,
      ARMOISE_TREE_PLUS,
      ARMOISE_TREE_MINUS,
      ARMOISE_TREE_MOD,
      ARMOISE_TREE_MUL,
      ARMOISE_TREE_DIV,
      ARMOISE_TREE_NEG,
       ARMOISE_TREE_LIST_ELEMENT,
       ARMOISE_TREE_CARTESIAN_PRODUCT,
       ARMOISE_TREE_POWER,
       ARMOISE_TREE_SET,
       ARMOISE_TREE_ENUMERATED_SET,
        ARMOISE_TREE_VECTOR,
        ARMOISE_TREE_EXISTS,
        ARMOISE_TREE_FORALL,
        ARMOISE_TREE_EQ,
        ARMOISE_TREE_NEQ,
        ARMOISE_TREE_LT,
        ARMOISE_TREE_GT,
        ARMOISE_TREE_LEQ,
        ARMOISE_TREE_GEQ,
        ARMOISE_TREE_CALL,
        ARMOISE_TREE_OR,
        ARMOISE_TREE_AND,
        ARMOISE_TREE_NOT,
        ARMOISE_TREE_EQUIV,
        ARMOISE_TREE_IMPLY,
        ARMOISE_TREE_IN,
        ARMOISE_TREE_TRUE,
        ARMOISE_TREE_FALSE,
  ARMOISE_TREE_NATURALS,
  ARMOISE_TREE_INTEGERS,
  ARMOISE_TREE_POSITIVES,
  ARMOISE_TREE_REALS,
  ARMOISE_TREE_EMPTY,

  ARMOISE_TREE_INTEGER,
  ARMOISE_TREE_IDENTIFIER,

  ARMOISE_TREE_PAREN,
};

extern void
armoise_tree_log (ccl_log_type log, armoise_tree *t);

# define armoise_tree_print(t) armoise_tree_log (CCL_LOG_DISPLAY, t)

END_C_DECLS

#endif /* ! __ARMOISE_TREE_H__ */
