/*
 * armoise-interp-p.h -- add a comment about this file
 * Copyright (C) 2007 J. Leroux, G. Point, LaBRI, CNRS UMR 5800, 
 * Universite Bordeaux I
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */
/*
 * $Author: point $
 * $Revision: 1.2 $
 * $Date: 2007/08/03 14:18:16 $
 * $Id: armoise-interp-p.h,v 1.2 2007/08/03 14:18:16 point Exp $
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __ARMOISE_INTERP_P_H__
# define __ARMOISE_INTERP_P_H__

# include "armoise-interp.h"

extern ccl_ustring
armoise_interp_identifier (armoise_tree *t);

extern int
armoise_count_number_of_nodes (armoise_tree *t);

#endif /* ! __ARMOISE_INTERP_P_H__ */
