/*
 * armoise-p.h -- add a comment about this file
 * 
 * This file is a part of the ARMOISE language library
. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __ARMOISE_P_H__
# define __ARMOISE_P_H__

# include "armoise.h"

typedef union
{
  struct {
    ccl_ustring name;
    int nb_indices;
    int *indices;
  } indirection;
  
  struct {
    int nb_op;
    armoise_predicate **predicates;
  } operands;
  
  struct {
    armoise_term *proto;
    armoise_predicate *P;
    armoise_formula *def;
  } set;

  struct {
    int card;
    armoise_term **set;
  } elements;

  armoise_predicate *P;
} predicate_content;

struct armoise_predicate_st
{
  int refcount;
  armoise_context *context;
  armoise_predicate_kind kind;
  armoise_type *type;
  predicate_content c;
  armoise_domain *domain;
};

struct armoise_variable_st 
{
  int refcount;
  ccl_ustring name;
  armoise_type *type;
  armoise_domain *domain;
};

typedef union
{
  int boolean_value;
  armoise_formula *f;

  struct {
    armoise_term *t;
    armoise_predicate *P;
  } in;

  struct {
    armoise_term *t1;
    armoise_term *t2;
  } cmp;

  struct {
    int nb_op;
    armoise_formula **formulas;
  } operands;

  struct {
    armoise_term *qvars;
    armoise_predicate *domain;
    armoise_formula *f;
  } q;
} formula_content;

struct armoise_formula_st
{
  int refcount;
  armoise_formula_kind kind;
  formula_content c;
};

typedef union 
{
  armoise_variable *variable;
  struct 
  {
    int num;
    int den;
  } value;

  struct 
  {
    int nb_op;
    armoise_term **terms;
  } operands;

  struct 
  {
    int index;
    armoise_term *variable;
  } vec_element;
} term_content;

struct armoise_term_st 
{
  int refcount;
  armoise_term_kind kind;
  term_content c;
  armoise_type *type;
};

			/* --------------- */

#define N_DOM 0
#define Z_DOM 1
#define P_DOM 2
#define R_DOM 3

extern int
armoise_domain_attr_to_gendom (const armoise_domain_attr attr);

extern int
armoise_domain_get_largest_dom (const armoise_domain *dom);

#endif /* ! __ARMOISE_P_H__ */
