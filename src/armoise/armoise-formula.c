/*
 * armoise-formula.c -- add a comment about this file
 * 
 * This file is a part of the ARMOISE language library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "armoise-p.h"

#define BVALUE(_f) ((_f)->c.boolean_value)
#define IN(_f) ((_f)->c.in)
#define CMP(_f) ((_f)->c.cmp)
#define OPERANDS(_f) ((_f)->c.operands)
#define QF(_f) ((_f)->c.q)

			/* --------------- */

static armoise_formula *
s_allocate_formula (armoise_formula_kind kind);

static void
s_delete_formula (armoise_formula *f);

static armoise_formula *
s_create_binary (armoise_formula_kind kind, armoise_formula *f1, 
		 armoise_formula *f2);

static armoise_formula *
s_create_nary (armoise_formula_kind kind, int nb_args, armoise_formula **args);

static armoise_formula *
s_create_cmp (armoise_formula_kind kind, armoise_term *t1, armoise_term *t2);

static armoise_formula *
s_create_quantifier (armoise_formula_kind kind, armoise_term *qvars, 
		     armoise_predicate *domain, armoise_formula *f);

			/* --------------- */

armoise_formula * 
armoise_formula_add_reference (armoise_formula *f)
{
  ccl_pre (f != NULL);

  f->refcount++;

  return f;
}

			/* --------------- */

void 
armoise_formula_del_reference (armoise_formula *f)
{
  ccl_pre (f != NULL);
  ccl_pre (f->refcount > 0);

  f->refcount--;
  if (f->refcount == 0)
    s_delete_formula (f);
}

			/* --------------- */

armoise_formula_kind 
armoise_formula_get_kind (const armoise_formula *f)
{
  ccl_pre (f != NULL);

  return f->kind;
}

			/* --------------- */

armoise_formula *
armoise_formula_create_constant (int value)
{
  armoise_formula *result = s_allocate_formula (ARMOISE_F_CONSTANT);

  BVALUE (result) = (value ? 1 : 0);

  return result;
}

			/* --------------- */

armoise_formula *
armoise_formula_create_in (armoise_term *t, armoise_predicate *P)
{
  armoise_formula *result = s_allocate_formula (ARMOISE_F_IN);

  ccl_pre (t != NULL); 
  ccl_pre (P != NULL);

  IN (result).t = armoise_term_add_reference (t);
  IN (result).P = armoise_predicate_add_reference (P);

  return result;
}

			/* --------------- */

static armoise_formula *
s_gather_operands (armoise_formula_kind kind , armoise_formula *f1, 
		   armoise_formula *f2)
{
  armoise_formula *result;

  if ((f1->kind == kind || f2->kind == kind))
    {
      int i, nb_args;
      armoise_formula **args;
      int k = 0;

      if (f1->kind == kind && f2->kind == kind)
	{
	  nb_args = OPERANDS(f1).nb_op + OPERANDS(f2).nb_op;
	  args = ccl_new_array (armoise_formula *, nb_args);
	  for (i = 0; i < OPERANDS(f1).nb_op; i++, k++)
	    args[k] = OPERANDS(f1).formulas[i];
	  for (i = 0; i < OPERANDS(f2).nb_op; i++, k++)
	    args[k] = OPERANDS(f2).formulas[i];
	}
      else if (f1->kind == kind)
	{
	  nb_args = OPERANDS(f1).nb_op + 1;
	  args = ccl_new_array (armoise_formula *, nb_args);
	  for (i = 0; i < OPERANDS(f1).nb_op; i++, k++)
	    args[k] = OPERANDS(f1).formulas[i];
	  args[k++] = f2;
	}
      else if (f2->kind == kind)
	{
	  nb_args = OPERANDS(f2).nb_op + 1;
	  args = ccl_new_array (armoise_formula *, nb_args);
	  args[k++] = f1;
	  for (i = 0; i < OPERANDS(f2).nb_op; i++, k++)
	    args[k] = OPERANDS(f2).formulas[i];
	}

      result = s_create_nary (kind, nb_args, args);
      ccl_delete (args);
    }  
  else
    {
      result = s_create_binary (kind, f1, f2);
    }

  return result;
}

			/* --------------- */

armoise_formula *
armoise_formula_create_or (armoise_formula *f1, armoise_formula *f2)
{
  return s_gather_operands (ARMOISE_F_OR, f1, f2);
}

			/* --------------- */

armoise_formula *
armoise_formula_create_and (armoise_formula *f1, armoise_formula *f2)
{
  return s_gather_operands (ARMOISE_F_AND, f1, f2);
}

			/* --------------- */

armoise_formula *
armoise_formula_create_equiv (armoise_formula *f1, armoise_formula *f2)
{
  return s_create_binary (ARMOISE_F_EQUIV, f1, f2);
}

			/* --------------- */

armoise_formula *
armoise_formula_create_imply (armoise_formula *f1, armoise_formula *f2)
{
  return s_create_binary (ARMOISE_F_IMPLY, f1, f2);
}

			/* --------------- */

armoise_formula *
armoise_formula_create_not (armoise_formula *f)
{
  armoise_formula *result = s_allocate_formula (ARMOISE_F_NOT);

  ccl_pre (f != NULL);

  OPERANDS (result).nb_op = 1;
  OPERANDS (result).formulas = ccl_new_array (armoise_formula *, 1);
  OPERANDS (result).formulas[0] = armoise_formula_add_reference (f);

  return result;
}

			/* --------------- */

armoise_formula *
armoise_formula_create_eq (armoise_term *t1, armoise_term *t2)
{  
  return s_create_cmp (ARMOISE_F_EQ, t1, t2);
}

			/* --------------- */

armoise_formula *
armoise_formula_create_neq (armoise_term *t1, armoise_term *t2)
{
  return s_create_cmp (ARMOISE_F_NEQ, t1, t2);
}

			/* --------------- */

armoise_formula *
armoise_formula_create_leq (armoise_term *t1, armoise_term *t2)
{
  return s_create_cmp (ARMOISE_F_LEQ, t1, t2);
}

			/* --------------- */

armoise_formula *
armoise_formula_create_geq (armoise_term *t1, armoise_term *t2)
{
  return s_create_cmp (ARMOISE_F_GEQ, t1, t2);
}

			/* --------------- */

armoise_formula *
armoise_formula_create_lt (armoise_term *t1, armoise_term *t2)
{
  return s_create_cmp (ARMOISE_F_LT, t1, t2);
}

			/* --------------- */

armoise_formula *
armoise_formula_create_gt (armoise_term *t1, armoise_term *t2)
{
  return s_create_cmp (ARMOISE_F_GT, t1, t2);
}

			/* --------------- */

armoise_formula *
armoise_formula_create_paren (armoise_formula *f)
{
  armoise_formula *result = s_allocate_formula (ARMOISE_F_PAREN);

  ccl_pre (f != NULL);

  result->c.f = armoise_formula_add_reference (f);

  return result;
}

			/* --------------- */

armoise_formula *
armoise_formula_create_exists (armoise_term *qvars, armoise_predicate *domain, 
			       armoise_formula *f)
{
  return s_create_quantifier (ARMOISE_F_EXISTS, qvars, domain, f);
}

			/* --------------- */

armoise_formula *
armoise_formula_create_forall (armoise_term *qvars, armoise_predicate *domain, 
			       armoise_formula *f)
{
  return s_create_quantifier (ARMOISE_F_FORALL, qvars, domain, f);
}

			/* --------------- */

void
armoise_formula_log (ccl_log_type log, armoise_formula *f)
{
  const char *op;

  ccl_pre (f != NULL);
  
  switch (f->kind)
    {
    case ARMOISE_F_CONSTANT:
      if (BVALUE (f))
	ccl_log (log, "true");
      else
	ccl_log (log, "false");
      break;

    case ARMOISE_F_IN:
      
      armoise_term_log (log, IN (f).t);
      ccl_log (log, " in ");
      armoise_predicate_log (log, IN (f).P);
      break;

    case ARMOISE_F_EQ:
    case ARMOISE_F_NEQ:
    case ARMOISE_F_LEQ:
    case ARMOISE_F_GEQ:
    case ARMOISE_F_LT:
    case ARMOISE_F_GT:
      if (f->kind == ARMOISE_F_EQ) op = "=";
      else if (f->kind == ARMOISE_F_NEQ) op = "!=";
      else if (f->kind == ARMOISE_F_LEQ) op = "<=";
      else if (f->kind == ARMOISE_F_GEQ) op = ">=";
      else if (f->kind == ARMOISE_F_LT) op = "<";
      else 
	{ 
	  ccl_assert (f->kind == ARMOISE_F_GT);
	  op = ">";
	}

      armoise_term_log (log, CMP (f).t1);
      ccl_log (log, " %s ", op);
      armoise_term_log (log, CMP (f).t2);
      break;

    case ARMOISE_F_NOT:
      ccl_log (log, "! ");
      armoise_formula_log (log, OPERANDS (f).formulas[0]);
      break;

    case ARMOISE_F_AND:
    case ARMOISE_F_OR:
    case ARMOISE_F_EQUIV:
    case ARMOISE_F_IMPLY:
      {
	int i;

	if (f->kind == ARMOISE_F_AND) op = "and";
	else if (f->kind == ARMOISE_F_OR) op = "or";
	else if (f->kind == ARMOISE_F_EQUIV) op = "<=>";
	else 
	  { 
	    ccl_assert (f->kind == ARMOISE_F_IMPLY);
	    op = "=>";
	  }
	
	armoise_formula_log (log, OPERANDS (f).formulas[0]);
	for (i = 1; i < OPERANDS (f).nb_op; i++)
	  {
	    ccl_log (log, " %s ", op);
	    armoise_formula_log (log, OPERANDS (f).formulas[i]);
	  }
      }
      break;

    case ARMOISE_F_EXISTS:
    case ARMOISE_F_FORALL:
      if (f->kind == ARMOISE_F_EXISTS)
	ccl_log (log, "exists ");
      else
	ccl_log (log, "forall ");
      armoise_term_log (log, QF (f).qvars);
      ccl_log (log, " in ");
      armoise_predicate_log (log, QF (f).domain);
      ccl_log (log, " ");
      armoise_formula_log (log, QF (f).f);
      break;

    default:
      ccl_assert (f->kind == ARMOISE_F_PAREN);
      ccl_log (log, "(");
      armoise_formula_log (log, f->c.f);
      ccl_log (log, ")");
      break;
    }
}

			/* --------------- */

void
armoise_formula_get_variables (const armoise_formula *F, ccl_list *variables)
{
  int i;

  switch (F->kind)
    {
    case ARMOISE_F_CONSTANT:
      break;

    case ARMOISE_F_IN:
      armoise_term_get_variables (F->c.in.t, variables);
      armoise_predicate_get_variables (F->c.in.P, variables);
      break;

    case ARMOISE_F_EQ:   
    case ARMOISE_F_NEQ:
    case ARMOISE_F_LEQ:   
    case ARMOISE_F_GEQ:
    case ARMOISE_F_LT:   
    case ARMOISE_F_GT:
      armoise_term_get_variables (F->c.cmp.t1, variables);
      armoise_term_get_variables (F->c.cmp.t2, variables);
      break;

    case ARMOISE_F_NOT:
    case ARMOISE_F_AND:
    case ARMOISE_F_OR:
    case ARMOISE_F_EQUIV:
    case ARMOISE_F_IMPLY:
      for (i = 0; i < F->c.operands.nb_op; i++)
	armoise_formula_get_variables (F->c.operands.formulas[i], 
				       variables);
      break;

    case ARMOISE_F_EXISTS:
    case ARMOISE_F_FORALL:
      armoise_term_get_variables (F->c.q.qvars, variables);
      armoise_formula_get_variables (F->c.q.f, variables);
      break;

    case ARMOISE_F_PAREN:
      armoise_formula_get_variables (F->c.f, variables);
      break;
    }
}

			/* --------------- */

int
armoise_formula_get_value (const armoise_formula *F)
{
  ccl_pre (armoise_formula_get_kind (F) == ARMOISE_F_CONSTANT);

  return F->c.boolean_value;
}

			/* --------------- */

const armoise_formula *
armoise_formula_get_operand (const armoise_formula *F, int i)
{
  ccl_pre (F != NULL);

  if (F->kind == ARMOISE_F_PAREN)
    return F->c.f;

  return F->c.operands.formulas[i];
}

			/* --------------- */

int
armoise_formula_get_nb_operands (const armoise_formula *F)
{
  ccl_pre (F != NULL);

  if (F->kind == ARMOISE_F_PAREN)
    return 1;

  return F->c.operands.nb_op;
}

			/* --------------- */

const armoise_formula *
armoise_formula_get_quantified_formula (const armoise_formula *F)
{
  ccl_pre (armoise_formula_get_kind (F) == ARMOISE_F_EXISTS || 
	   armoise_formula_get_kind (F) == ARMOISE_F_FORALL);

  return F->c.q.f;
}

			/* --------------- */

armoise_term *
armoise_formula_get_quantified_variables (const armoise_formula *F, 
					  armoise_predicate **p_domain)
{
  ccl_pre (armoise_formula_get_kind (F) == ARMOISE_F_EXISTS || 
	   armoise_formula_get_kind (F) == ARMOISE_F_FORALL);

  if (p_domain != NULL)
    *p_domain = armoise_predicate_add_reference (F->c.q.domain);

  return armoise_term_add_reference (F->c.q.qvars);
}

			/* --------------- */

armoise_term *
armoise_formula_get_in_term (const armoise_formula *F)
{
  ccl_pre (armoise_formula_get_kind (F) == ARMOISE_F_IN);

  return armoise_term_add_reference (F->c.in.t);
}

			/* --------------- */

armoise_predicate *
armoise_formula_get_in_predicate (const armoise_formula *F)
{
  ccl_pre (armoise_formula_get_kind (F) == ARMOISE_F_IN);

  return armoise_predicate_add_reference (F->c.in.P);
}

			/* --------------- */

armoise_term *
armoise_formula_get_cmp_left_term (const armoise_formula *F)
{
  return armoise_term_add_reference (F->c.cmp.t1);
}

			/* --------------- */

armoise_term *
armoise_formula_get_cmp_right_term (const armoise_formula *F)
{
  return armoise_term_add_reference (F->c.cmp.t2);
}

			/* --------------- */

static armoise_formula *
s_allocate_formula (armoise_formula_kind kind)
{
  armoise_formula *result = ccl_new (armoise_formula);

  result->refcount = 1;
  result->kind = kind;

  return result;
}

			/* --------------- */

static void
s_delete_formula (armoise_formula *f)
{
  int i;

  ccl_pre (f != NULL);
  ccl_pre (f->refcount == 0);

  switch (f->kind)
    {
    case ARMOISE_F_CONSTANT:
      break;

    case ARMOISE_F_IN:
      armoise_term_del_reference (IN (f).t);
      armoise_predicate_del_reference (IN (f).P);
      break;

    case ARMOISE_F_EQ:
    case ARMOISE_F_NEQ:
    case ARMOISE_F_LEQ:
    case ARMOISE_F_GEQ:
    case ARMOISE_F_LT:
    case ARMOISE_F_GT:
      armoise_term_del_reference (CMP (f).t1);
      armoise_term_del_reference (CMP (f).t2);
      break;

    case ARMOISE_F_NOT:
    case ARMOISE_F_AND:
    case ARMOISE_F_OR:
    case ARMOISE_F_EQUIV:
    case ARMOISE_F_IMPLY:
      for (i = 0; i < OPERANDS (f).nb_op; i++)
	armoise_formula_del_reference (OPERANDS (f).formulas[i]);
      ccl_delete (OPERANDS (f).formulas);
      break;

    case ARMOISE_F_EXISTS:
    case ARMOISE_F_FORALL:
      armoise_term_del_reference (QF (f).qvars);
      armoise_predicate_del_reference (QF (f).domain);
      armoise_formula_del_reference (QF (f).f);
      break;

    default:
      ccl_assert (f->kind == ARMOISE_F_PAREN);
      armoise_formula_del_reference (f->c.f);
      break;
    }
  ccl_delete (f);
}

			/* --------------- */

static armoise_formula *
s_create_binary (armoise_formula_kind kind, armoise_formula *f1, 
		 armoise_formula *f2)
{
  armoise_formula *result = s_allocate_formula (kind);

  ccl_pre (f1 != NULL);
  ccl_pre (f2 != NULL);

  OPERANDS (result).nb_op = 2;
  OPERANDS (result).formulas = ccl_new_array (armoise_formula *, 2);
  OPERANDS (result).formulas[0] = armoise_formula_add_reference (f1);
  OPERANDS (result).formulas[1] = armoise_formula_add_reference (f2);

  return result;
}

			/* --------------- */

static armoise_formula *
s_create_nary (armoise_formula_kind kind, int nb_args, armoise_formula **args)
{
  int i;
  armoise_formula *result = s_allocate_formula (kind);

  OPERANDS (result).nb_op = nb_args;
  OPERANDS (result).formulas = ccl_new_array (armoise_formula *, nb_args);
  for (i = 0; i < nb_args; i++)
    OPERANDS (result).formulas[i] = armoise_formula_add_reference (args[i]);

  return result;
}

			/* --------------- */

static armoise_formula *
s_create_cmp (armoise_formula_kind kind, armoise_term *t1, armoise_term *t2)
{
  armoise_formula *result = s_allocate_formula (kind);

  ccl_pre (t1 != NULL);
  ccl_pre (t2 != NULL);

  CMP (result).t1 = armoise_term_add_reference (t1);
  CMP (result).t2 = armoise_term_add_reference (t2);

  return result;
}

			/* --------------- */

static armoise_formula *
s_create_quantifier (armoise_formula_kind kind, armoise_term *qvars, 
		     armoise_predicate *domain, armoise_formula *f)
{
  armoise_formula *result = s_allocate_formula (kind);

  ccl_pre (domain != NULL);
  ccl_pre (qvars != NULL);
  ccl_pre (f != NULL);
    
  QF (result).qvars = armoise_term_add_reference (qvars);
  QF (result).domain = armoise_predicate_add_reference (domain);
  QF (result).f = armoise_formula_add_reference (f);

  return result;
}
