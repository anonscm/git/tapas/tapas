/*
 * alambic-nf.c -- add a comment about this file
 * 
 * This file is a part of the Alambic Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <time.h>
#include <ccl/ccl-assert.h>
#include "alambic.h"

/* TODO:
 * 
 * - Normalize artihmetic over predicates
 * - Rewrite DIV and MOD in terms
 */


			/* --------------- */

static armoise_term *
s_duplicate_term (armoise_context *vctx, const armoise_term *t, int addvars);

static armoise_context *
s_create_predicate_context (armoise_context *parent, armoise_context *ctx,
			    alambic_error *p_err);

static armoise_predicate *
s_normalize_predicate (armoise_context *ctx, const armoise_predicate *P, 
		       alambic_error *p_err);

static armoise_predicate *
s_normalize_set (armoise_context *ctx, const armoise_predicate *P, 
		 alambic_error *p_err);

static armoise_formula *
s_normalize_formula (armoise_context *ctx,  armoise_context *vctx, 
		     const armoise_formula *F, alambic_error *p_err);

static armoise_term *
s_normalize_term (armoise_context *ctx, const armoise_term *t, 
		  alambic_error *p_err);

static armoise_term **
s_collect_term_components (armoise_term *t, int *p_nb_comps);

			/* --------------- */

armoise_normalized_predicate *
alambic_normalize_predicate (armoise_context *parent, 
			     const armoise_predicate *P, alambic_error *p_err)
{
  int t0;
  armoise_predicate *result;
  armoise_context *ctx;
  armoise_context *pctx;

  if (*p_err != ALAMBIC_NO_ERROR)
    return NULL;

  t0 = clock ();
  ccl_debug ("normalizing predicate...\n");
  ccl_debug_push_tab_level ();
  pctx = armoise_predicate_get_context (P);
  ctx = s_create_predicate_context (parent, pctx, p_err);
  armoise_context_del_reference (pctx);

  if (ctx != NULL)
    {
      result = s_normalize_predicate (ctx, P, p_err);
      if (result != NULL)
	armoise_predicate_compute_domain (result);
      armoise_context_del_reference (ctx);
    }
  ccl_debug_pop_tab_level ();
  ccl_debug ("done within %d ms.\n", (1000*(clock () - t0))/CLOCKS_PER_SEC);

  return (armoise_normalized_predicate *)result;
}

			/* --------------- */

ccl_tree *
alambic_normalize_predicate_tree (armoise_context *ctx, const ccl_tree *t,
				  alambic_error *p_err)
{
  ccl_tree *result = NULL;

  if (ccl_tree_is_leaf (t))
    {
      armoise_predicate *P = (armoise_predicate *) t->object;
      armoise_normalized_predicate *nP = 
	alambic_normalize_predicate (ctx, P, p_err);
      if (nP != NULL)
	{
	  result = 
	    ccl_tree_create (nP, 
			     (ccl_delete_proc *) 
			     armoise_predicate_del_reference);
	}
    }
  else
    {
      ccl_tree *c;

      result = ccl_tree_create (NULL, NULL);
      for (c = t->childs; c != NULL && result != NULL; c = c->next)
	{
	  ccl_tree *nc = alambic_normalize_predicate_tree (ctx, c, p_err);
	  if (nc != NULL)
	    {
	      ccl_tree_add_child (result, nc);
	      ccl_tree_del_reference (nc);
	    }
	  else
	    {
	      ccl_tree_del_reference (result);
	      result = NULL;
	    }
	}
    }

  return result;
}

			/* --------------- */

static armoise_context *
s_create_predicate_context (armoise_context *parent, armoise_context *ctx,
			    alambic_error *p_err)
{
  ccl_pair *p;
  armoise_context *result;
  ccl_list *ids = armoise_context_get_ids (ctx);

  if (*p_err != ALAMBIC_NO_ERROR)
    return NULL;

  result = armoise_context_create_for_predicates (parent);
  
  for (p = FIRST (ids); p && result != NULL; p = CDR (p))
    {
      ccl_ustring id = (ccl_ustring) CAR (p);
      ccl_tree *t = armoise_context_get_predicate_tree (ctx, id);
      ccl_tree *nt = alambic_normalize_predicate_tree (result, t, p_err);
      if (nt != NULL)
	{
	  armoise_context_add_predicate_tree (result, id, nt);
	  ccl_tree_del_reference (nt);
	}
      else
	{
	  armoise_context_del_reference (result);
	  result = NULL;
	}
      ccl_tree_del_reference (t);      
    }

  return result;
}

			/* --------------- */

static armoise_predicate *
s_normalize_predicate (armoise_context *ctx, const armoise_predicate *P, 
		       alambic_error *p_err)
{
  armoise_predicate *result = NULL;
  armoise_predicate_kind kind = armoise_predicate_get_kind (P);


  if (*p_err != ALAMBIC_NO_ERROR)
    return NULL;

  switch (kind)
    {
    case ARMOISE_P_NAT:
      result = armoise_predicate_create_naturals (ctx);
      break;
    case ARMOISE_P_INT:
      result = armoise_predicate_create_integers (ctx);
      break;
    case ARMOISE_P_POSI:
      result = armoise_predicate_create_positives (ctx);
      break;
    case ARMOISE_P_REAL:
      result = armoise_predicate_create_reals (ctx);
      break;
    case ARMOISE_P_UNION:
    case ARMOISE_P_INTERSECTION:
    case ARMOISE_P_DIFF:   
    case ARMOISE_P_DELTA:
      {
	int i;
	int nb_operands = armoise_predicate_get_nb_operands (P);
	const armoise_predicate *op = armoise_predicate_get_operand (P, 0);
	armoise_predicate *(*operator) (armoise_context *,
					armoise_predicate *,
					armoise_predicate *);

	if (kind == ARMOISE_P_UNION) 
	  operator = armoise_predicate_create_union;
	else if (kind == ARMOISE_P_INTERSECTION) 
	  operator = armoise_predicate_create_intersection;
	else if (kind == ARMOISE_P_DIFF) 
	  operator = armoise_predicate_create_diff;
	else if (kind == ARMOISE_P_DELTA)
	  operator = armoise_predicate_create_delta;

	result = s_normalize_predicate (ctx, op, p_err);

	for (i = 1; i < nb_operands && result != NULL; i++)
	  {
	    const armoise_predicate *op = armoise_predicate_get_operand (P, i);
	    armoise_predicate *nop = s_normalize_predicate (ctx, op, p_err);
	    if (nop != NULL)
	      {
		armoise_predicate *tmp = operator (ctx, result, nop);
		armoise_predicate_del_reference (nop);
		armoise_predicate_del_reference (result);
		result = tmp;
	      }
	    else
	      {
		armoise_predicate_del_reference (result);
		result = NULL;
	      }
	  }
      }
      break;

    case ARMOISE_P_COMPLEMENT:
      {
	const armoise_predicate *op = armoise_predicate_get_operand (P, 0);
	armoise_predicate *nop = s_normalize_predicate (ctx, op, p_err);
	if (nop != NULL)
	  {
	    result = armoise_predicate_create_complement (ctx, nop);
	    armoise_predicate_del_reference (nop);
	  }
      }
      break;

    case ARMOISE_P_PRODUCT:
      {
	int i;
	int nb_operands = armoise_predicate_get_nb_operands (P);
	armoise_predicate **args = 
	  ccl_new_array (armoise_predicate *, nb_operands);

	for (i = 0; i < nb_operands && *p_err == ALAMBIC_NO_ERROR; i++)
	  {
	    const armoise_predicate *op = armoise_predicate_get_operand (P, i);

	    args[i] = s_normalize_predicate (ctx, op, p_err);
	  }
	if (*p_err == ALAMBIC_NO_ERROR)
	  result = armoise_predicate_create_product (ctx, nb_operands, args);
	for (i = 0; i < nb_operands; i++)
	  ccl_zdelete (armoise_predicate_del_reference, args[i]);
	ccl_delete (args);
      }
      break;

    case ARMOISE_P_INDIRECTION:
      {
	ccl_ustring name;
	int nb_indices;
	const int *indices;
	armoise_predicate_get_indirection (P, &name, &nb_indices, &indices);
	result = armoise_predicate_create_indirection (ctx, name, nb_indices, 
						       indices);
      }
      break;

    case ARMOISE_P_PAREN:
      {
	const armoise_predicate *op = armoise_predicate_get_operand (P, 0);
	armoise_predicate *nop = s_normalize_predicate (ctx, op, p_err);
	if (nop != NULL)
	  {
	    result = armoise_predicate_create_paren (ctx, nop);
	    armoise_predicate_del_reference (nop);
	  }
      }
      break;

    case ARMOISE_P_SET:
      result = s_normalize_set (ctx, P, p_err);
      break;	

    case ARMOISE_P_ENUM:
      {
	int i;
	int nb_elements = armoise_predicate_get_nb_elements (P);
	armoise_term **elements = ccl_new_array (armoise_term *, nb_elements);
  
	for (i = 0; i < nb_elements && *p_err == ALAMBIC_NO_ERROR; i++)
	  {
	    const armoise_term *t = armoise_predicate_get_element (P, i);
	    elements[i] = s_normalize_term (NULL, t, p_err);
	  }
	result = 
	  armoise_predicate_create_finite_set (ctx, nb_elements, elements);
	for (i = 0; i < nb_elements; i++)
	  ccl_zdelete (armoise_term_del_reference, elements[i]);
	ccl_delete (elements);
      }
      break;

    case ARMOISE_P_ADD:
    case ARMOISE_P_SUB:
    case ARMOISE_P_MUL:
    case ARMOISE_P_DIV:
    case ARMOISE_P_MOD:
      {
	int i;
	int nb_operands = armoise_predicate_get_nb_operands (P);
	armoise_predicate *tmp[2];
	const armoise_predicate *op = armoise_predicate_get_operand (P, 0);
	result = s_normalize_predicate (ctx, op, p_err);
	armoise_predicate *(*operator)(armoise_context *, armoise_predicate *, 
				       armoise_predicate *);

	if (kind == ARMOISE_P_ADD)  operator = armoise_predicate_create_add;
	else if (kind == ARMOISE_P_SUB) operator = armoise_predicate_create_sub;
	else if (kind == ARMOISE_P_MUL) operator = armoise_predicate_create_mul;
	else if (kind == ARMOISE_P_DIV) operator = armoise_predicate_create_div;
	else operator = armoise_predicate_create_mod;

	for (i = 1; i  < nb_operands && result != NULL; i++)	  
	  {
	    const armoise_predicate *op = armoise_predicate_get_operand (P, i);
	    tmp[0] = s_normalize_predicate (ctx, op, p_err);
	    if (tmp[0] != NULL)
	      {
		tmp[1] = operator (ctx, result, tmp[0]);
		armoise_predicate_del_reference (result);
		armoise_predicate_del_reference (tmp[0]);
		result = tmp[1];
	      }
	    else
	      {
		armoise_predicate_del_reference (result);
		result = NULL;
	      }
	  }
      }
      break;

    case ARMOISE_P_NEG:
      {
	const armoise_predicate *op = armoise_predicate_get_operand (P, 0);
	armoise_predicate *tmp = s_normalize_predicate (ctx, op, p_err);
	if (tmp != NULL)
	  result = armoise_predicate_create_neg (ctx, tmp);
	armoise_predicate_del_reference (tmp);
      }
      break;

    case ARMOISE_P_EMPTY:
      /*result = armoise_predicate_create_empty (ctx);*/

    default:
      ccl_throw_no_msg (internal_error);
    }

  return result;
}

			/* --------------- */

static armoise_term *
s_duplicate_term (armoise_context *vctx, const armoise_term *t, int addvars)
{  
  int i;
  const armoise_term *op;
  armoise_term *tmp[2];
  armoise_term *result;
  armoise_term_kind kind = armoise_term_get_kind (t);

  switch (kind)
    {
    case ARMOISE_T_VARIABLE:
      {
	armoise_variable *var = armoise_term_get_variable (t);
	ccl_ustring name = armoise_variable_get_name (var);
	armoise_variable *nvar;

	if (addvars)
	  {
	    if (armoise_context_has (vctx, name))
	      nvar = armoise_context_get_variable (vctx, name);
	    else
	      {
		armoise_type *type = armoise_variable_get_type (var);
		nvar = armoise_variable_create (name, type);
		armoise_type_del_reference (type);
		armoise_context_add_variable (vctx, nvar);
	      }
	  }
	else
	  {
	    ccl_assert (armoise_context_has (vctx, name));
	    nvar = armoise_context_get_variable (vctx, name);	    
	  }
	armoise_variable_del_reference (var);
	result = armoise_term_create_variable (nvar);
	armoise_variable_del_reference (nvar);
      }
      break;

    case ARMOISE_T_CONSTANT:
      {
	int num, den;
	armoise_term_get_value (t, &num, &den);
	result = armoise_term_create_constant (num, den);
      }
      break;

    case ARMOISE_T_VECTOR:
      {
	int nb_op = armoise_term_get_nb_operands (t);
	armoise_term **args = ccl_new_array (armoise_term *, nb_op);

	for (i = 0; i < nb_op; i++)
	  {
	    op = armoise_term_get_operand (t, i);
	    args[i] = s_duplicate_term (vctx, op, addvars);
	  }
	result = armoise_term_create_vector (nb_op, args);

	for (i = 0; i < nb_op; i++)
	  armoise_term_del_reference (args[i]);
	ccl_delete (args);
      }
      break;

    case ARMOISE_T_ELEMENT:
      op = armoise_term_get_vec_element_variable (t);
      i = armoise_term_get_vec_element_index (t);
      tmp[0] = s_duplicate_term (vctx, op, addvars);
      result = armoise_term_create_vector_element (tmp[0], i);
      armoise_term_del_reference (tmp[0]);
      break;

    case ARMOISE_T_ADD:
    case ARMOISE_T_SUB:
    case ARMOISE_T_MUL:
    case ARMOISE_T_DIV:
    case ARMOISE_T_MOD:
      {
	int nb_op = armoise_term_get_nb_operands (t);
	armoise_term *(*operator)(armoise_term *, armoise_term *);

	if (kind == ARMOISE_T_ADD) operator = armoise_term_create_add;
	else if (kind == ARMOISE_T_SUB) operator = armoise_term_create_sub;
	else if (kind == ARMOISE_T_MUL) operator = armoise_term_create_mul;
	else if (kind == ARMOISE_T_DIV) operator = armoise_term_create_div;
	else operator = armoise_term_create_mod;

	op = armoise_term_get_operand (t, 0);
	result = s_duplicate_term (vctx, op, addvars);
	for (i = 1; i < nb_op; i++)
	  {
	    op = armoise_term_get_operand (t, i);
	    tmp[0] = s_duplicate_term (vctx, op, addvars);
	    tmp[1] = operator (result, tmp[0]);
	    armoise_term_del_reference (tmp[0]);
	    armoise_term_del_reference (result);
	    result = tmp[1];
	  }
      }
      break;

    case ARMOISE_T_NEG:
      op = armoise_term_get_operand (t, 0);
      tmp[0] = s_duplicate_term (vctx, op, addvars);
      result = armoise_term_create_neg (tmp[0]);
      armoise_term_del_reference (tmp[0]);
      break;
    }
  
  return result;
}

			/* --------------- */

static armoise_formula *
s_rewrite_vectorial_in (armoise_context *ctx,  armoise_term *t,
			armoise_predicate *P);


static armoise_term *
s_normalize_quantified_variables (armoise_context *vctx, const armoise_term *t)
{
  armoise_term *result;

  if (armoise_term_get_kind (t) == ARMOISE_T_VARIABLE)
    {
      armoise_variable *var = armoise_term_get_variable (t);
      ccl_ustring name = armoise_variable_get_name (var);
      const armoise_domain *domain = armoise_variable_get_domain (var);
      armoise_type *type = armoise_variable_get_type (var);
      armoise_variable *nvar = armoise_variable_create (name, type);
      armoise_variable_set_domain (nvar, domain);

      armoise_context_add_variable (vctx, nvar);
      result = armoise_term_create_variable (nvar);

      armoise_type_del_reference (type);
      armoise_variable_del_reference (nvar);
      armoise_variable_del_reference (var);
    }
  else
    {
      int i;
      int nb_operands = armoise_term_get_arity (t);
      armoise_term **operands = ccl_new_array (armoise_term *, nb_operands);

      for (i = 0; i < nb_operands; i++)
	{
	  const armoise_term *arg = armoise_term_get_operand (t, i);
	  operands[i] = s_normalize_quantified_variables (vctx, arg);
	}
      result = armoise_term_create_vector (nb_operands, operands);
      for (i = 0; i < nb_operands; i++)
	armoise_term_del_reference (operands[i]);
      ccl_delete (operands);
    }

  return result;
}

			/* --------------- */

static armoise_predicate *
s_normalize_set (armoise_context *ctx, const armoise_predicate *P, 
		 alambic_error *p_err)
{
  armoise_predicate *result = NULL;
  armoise_predicate *dom;
  const armoise_term *proto = armoise_predicate_get_set_proto (P, &dom);
  const armoise_formula *tmp = armoise_predicate_get_set_formula (P);
  armoise_context *vctx = armoise_context_create_for_variables (NULL);
  armoise_term *nproto = s_normalize_quantified_variables (vctx, proto);
  armoise_predicate *ndom = s_normalize_predicate (ctx, dom, p_err);

  if (ndom != NULL)
    {
      armoise_formula *domC = armoise_formula_create_in (proto, dom);
      armoise_formula *F = armoise_formula_create_and (domC, tmp);
      armoise_formula *nF = s_normalize_formula (ctx, vctx, F, p_err);
      if (nF != NULL)
	{
	  result = armoise_predicate_create_simple_set (ctx, nproto, ndom, nF);
	  armoise_formula_del_reference (nF);
	}
      armoise_predicate_del_reference (ndom);
      armoise_formula_del_reference (domC);
      armoise_formula_del_reference (F);
    }

  armoise_predicate_del_reference (dom);
  armoise_term_del_reference (nproto);
  armoise_context_del_reference (vctx);

  return result;
}

			/* --------------- */


static armoise_formula *
s_normalize_formula (armoise_context *ctx,  armoise_context *vctx, 
		     const armoise_formula *F, alambic_error *p_err)
{
  armoise_formula *result = NULL;
  armoise_formula_kind kind = armoise_formula_get_kind (F);

  if (*p_err != ALAMBIC_NO_ERROR)
    return NULL;

  switch (kind)
    {
    case ARMOISE_F_CONSTANT:
      {
	int value = armoise_formula_get_value (F);
	result = armoise_formula_create_constant (value);
      }
      break;

    case ARMOISE_F_NOT:
      {
	const armoise_formula *op = armoise_formula_get_operand (F, 0);
	armoise_formula *nop = s_normalize_formula (ctx, vctx, op, p_err);
	if (nop != NULL)
	  {
	    result = armoise_formula_create_not (nop);
	    armoise_formula_del_reference (nop);
	  }
      }
      break;

    case ARMOISE_F_AND:
    case ARMOISE_F_OR:
    case ARMOISE_F_EQUIV:
    case ARMOISE_F_IMPLY:
      {
	int i;
	int nb_operands = armoise_formula_get_nb_operands (F);
	const armoise_formula *op = armoise_formula_get_operand (F, 0);
	armoise_formula *tmp[2];
	armoise_formula *(*operator) (armoise_formula *,armoise_formula *);
	if (kind == ARMOISE_F_AND) 
	  operator = armoise_formula_create_and;
	else if (kind == ARMOISE_F_OR) 
	  operator = armoise_formula_create_or;
	else if (kind == ARMOISE_F_EQUIV) 
	  operator = armoise_formula_create_equiv;
	else 
	  operator = armoise_formula_create_imply;

	result = s_normalize_formula (ctx, vctx, op, p_err);
	for (i = 1; i < nb_operands && result != NULL; i++)
	  {
	    op = armoise_formula_get_operand (F, i);
	    tmp[0] = s_normalize_formula (ctx, vctx, op, p_err);
	    if (tmp[0] != NULL)
	      {
		tmp[1] = operator (result, tmp[0]);
		armoise_formula_del_reference (tmp[0]);
		armoise_formula_del_reference (result);
		result = tmp[1];
	      }
	    else
	      {
		armoise_formula_del_reference (result);
		result = NULL;
	      }
	  }
      }
      break;

    case ARMOISE_F_EXISTS:
    case ARMOISE_F_FORALL:
      {
	armoise_predicate *dom;
	armoise_predicate *ndom;
	armoise_term *qvars = 
	  armoise_formula_get_quantified_variables (F, &dom);
	armoise_context *lvctx = armoise_context_create_for_variables (vctx);
	const armoise_formula *op = armoise_formula_get_quantified_formula (F);
	armoise_formula *nqF;

	ndom = s_normalize_predicate (ctx, dom, p_err);
	if (ndom != NULL)
	  {	
	    armoise_term *nqvars = 
	      s_normalize_quantified_variables (lvctx, qvars);
	    armoise_formula *in = armoise_formula_create_in (nqvars, ndom);
	    armoise_formula *qF;

	    if (kind == ARMOISE_F_EXISTS)
	      qF = armoise_formula_create_and (in, op);
	    else
	      qF = armoise_formula_create_imply (in, op);

	    nqF = s_normalize_formula (ctx, lvctx, qF, p_err);
	    if (nqF != NULL)
	      {
		if (kind == ARMOISE_F_EXISTS)
		  result = armoise_formula_create_exists (nqvars, ndom, nqF);
		else
		  result = armoise_formula_create_forall (nqvars, ndom, nqF);
		armoise_formula_del_reference (nqF);
	      }

	    armoise_formula_del_reference (in);
	    armoise_formula_del_reference (qF);
	    armoise_term_del_reference (nqvars);
	    armoise_predicate_del_reference (ndom);
	  }

	armoise_context_del_reference (lvctx);
	armoise_term_del_reference (qvars);
	armoise_predicate_del_reference (dom);
      }
      break;

    case ARMOISE_F_PAREN:
      {
	const armoise_formula *op = armoise_formula_get_operand (F, 0);
	armoise_formula *tmp = s_normalize_formula (ctx, vctx, op, p_err);
	if (tmp != NULL)
	  {
	    result = armoise_formula_create_paren (tmp);
	    armoise_formula_del_reference (tmp);
	  }
      }
      break;

    case ARMOISE_F_LEQ: 
    case ARMOISE_F_GEQ:
    case ARMOISE_F_LT:
    case ARMOISE_F_GT:
      {
	armoise_term *t1 = armoise_formula_get_cmp_left_term (F);
	armoise_term *t2 = armoise_formula_get_cmp_right_term (F);
	armoise_term *t = armoise_term_create_sub (t1, t2);
	armoise_term *nt = s_normalize_term (vctx, t, p_err);
	armoise_term_del_reference (t1);
	armoise_term_del_reference (t2);
	armoise_term_del_reference (t);

	if (nt != NULL)
	  {
	    if (armoise_term_is_constant (nt))
	      {
		int num, den, bval;

		armoise_term_get_value (nt, &num, &den);
		bval = ((num <= 0 && kind == ARMOISE_F_LEQ) ||
			(num >= 0 && kind == ARMOISE_F_GEQ) ||
			(num < 0 && kind == ARMOISE_F_LT) ||
			(num > 0 && kind == ARMOISE_F_GT));
		result = armoise_formula_create_constant (bval);
	      }
	    else
	      {
		armoise_term *zero = armoise_term_create_constant (0, 1);
		armoise_formula *(*operator)(armoise_term *, armoise_term *);
	
		if (kind == ARMOISE_F_LEQ) 
		  operator = armoise_formula_create_leq;
		else if (kind == ARMOISE_F_LT) 
		  operator = armoise_formula_create_lt;
		else if (kind == ARMOISE_F_GEQ) 
		  operator = armoise_formula_create_geq;
		else operator = armoise_formula_create_gt;
		result = operator (nt, zero);
		armoise_term_del_reference (zero);
	      }
	    armoise_term_del_reference (nt);
	  }
      }
      break;

    case ARMOISE_F_IN:
      {
	armoise_term *t = armoise_formula_get_in_term (F);
	armoise_term *nt = s_normalize_term (vctx, t, p_err);
	armoise_predicate *P = armoise_formula_get_in_predicate (F);
	armoise_predicate *nP = (armoise_predicate *)
	  alambic_normalize_predicate (ctx, P, p_err);
	armoise_predicate_del_reference (P);
	armoise_term_del_reference (t);

	result = s_rewrite_vectorial_in (ctx, nt, nP);
	armoise_predicate_del_reference (nP);
	armoise_term_del_reference (nt);
      }
      break;

    case ARMOISE_F_EQ:
    case ARMOISE_F_NEQ:
      {
	armoise_term *t1 = armoise_formula_get_cmp_left_term (F);
	armoise_term *t2 = armoise_formula_get_cmp_right_term (F);
	int dim = armoise_term_get_dim (t1);
	armoise_formula *(*operator)(armoise_term *, armoise_term *);
	armoise_formula *(*operator2)(armoise_formula *, armoise_formula *);
	if (kind == ARMOISE_F_EQ)
	  {
	    operator = armoise_formula_create_eq;
	    operator2 = armoise_formula_create_and;
	  }
	else
	  {
	    operator = armoise_formula_create_neq;
	    operator2 = armoise_formula_create_or;
	  }

	if (dim == 0)
	  {
	    armoise_term *tmp1 = armoise_term_create_sub (t1, t2);
	    armoise_term *nt = s_normalize_term (vctx, tmp1, p_err);
	    if (nt != NULL)
	      {
		if (armoise_term_is_constant (nt))
		  {
		    int num, den, bval;
		    armoise_term_get_value (nt, &num, &den);
		    bval = ((num == 0 && kind == ARMOISE_F_EQ) ||
			    (num == 0 && kind == ARMOISE_F_NEQ));
		    result = armoise_formula_create_constant (bval);
		  }
		else
		  {
		    armoise_term *zero = armoise_term_create_constant (0, 1);
		    if (kind == ARMOISE_F_EQ)
		      result = armoise_formula_create_eq (nt, zero);
		    else
		      result = armoise_formula_create_neq (nt, zero);
		    armoise_term_del_reference (zero);
		  }
		armoise_term_del_reference (nt);
	      }
	    armoise_term_del_reference (tmp1);
	  }
	else
	  {
	    int nb_comps;
	    armoise_term *zero = armoise_term_create_constant (0, 1);
	    armoise_term *t1_minus_t2 = armoise_term_create_sub (t1, t2);
	    armoise_term *n_t1_minus_t2 = 
	      s_normalize_term (vctx, t1_minus_t2, p_err);

	    if (*p_err == ALAMBIC_NO_ERROR)
	      {
		int i;
		armoise_term **comps = 
		  s_collect_term_components (n_t1_minus_t2, &nb_comps);
		/* !!!!!! must check if a term is constant !!! */
		armoise_term_del_reference (n_t1_minus_t2);
		result = operator (comps[0], zero);
		armoise_term_del_reference (comps[0]);

		for (i = 1; i < nb_comps; i++)
		  {
		    armoise_formula *aux = operator (comps[i], zero);
		    armoise_formula *aux2 = operator2 (result, aux);
		    armoise_formula_del_reference (aux);
		    armoise_formula_del_reference (result);
		    result = aux2;
		    armoise_term_del_reference (comps[i]);
		  }
		ccl_delete (comps);

	      }
	    armoise_term_del_reference (zero);
	    armoise_term_del_reference (t1_minus_t2);
	  }
	armoise_term_del_reference (t1);
	armoise_term_del_reference (t2);
      }
      break;
    }

  return result;
}

			/* --------------- */

static armoise_term *
s_expand_variables (armoise_context *vctx, const armoise_term *t)
{
  armoise_term *result;
  int d = armoise_term_get_dim (t);

  if (d == 0)
    result = s_duplicate_term (vctx, t, 0);
  else
    {
      int i;
      armoise_term_kind kind = armoise_term_get_kind (t);

      switch (kind)    
	{
	case ARMOISE_T_CONSTANT:
	  ccl_throw_no_msg (internal_error);
	  break;

	case ARMOISE_T_VARIABLE:
	case ARMOISE_T_ELEMENT:      
	  {
	    armoise_term **subvars = ccl_new_array (armoise_term *, d);
	    armoise_term *var = s_duplicate_term (vctx, t, 0);

	    for (i = 0; i < d; i++)
	      {
		armoise_term *tmp = armoise_term_create_vector_element (var, i);
		subvars[i] = s_expand_variables (vctx, tmp);
		armoise_term_del_reference (tmp);
	      }
	    result = armoise_term_create_vector (d, subvars);
	    armoise_term_del_reference (var);
	    for (i = 0; i < d; i++)
	      armoise_term_del_reference (subvars[i]);
	    ccl_delete (subvars);
	  }
	  break;

	case ARMOISE_T_VECTOR:
	  {
	    armoise_term **tmp = ccl_new_array (armoise_term *, d);
	    for (i = 0; i < d; i++)
	      {
		const armoise_term *op = armoise_term_get_operand (t, i);
		tmp[i] = s_expand_variables (vctx, op);
	      }
	    result = armoise_term_create_vector (d, tmp);

	    for (i = 0; i < d; i++)
	      armoise_term_del_reference (tmp[i]);
	    ccl_delete (tmp);
	  }
	  break;

	case ARMOISE_T_ADD:      
	case ARMOISE_T_SUB:
	case ARMOISE_T_MUL:
	case ARMOISE_T_DIV:
	case ARMOISE_T_MOD:
	  {
	    armoise_term *tmp[2];
	    int nb_operands = armoise_term_get_nb_operands (t);
	    const armoise_term *op = armoise_term_get_operand (t, 0);
	    armoise_term *(*operator)(armoise_term *, armoise_term *);

	    if (kind == ARMOISE_T_ADD) operator = armoise_term_create_add;
	    else if (kind == ARMOISE_T_SUB) operator = armoise_term_create_sub;
	    else if (kind == ARMOISE_T_MUL) operator = armoise_term_create_mul;
	    else if (kind == ARMOISE_T_DIV) operator = armoise_term_create_div;
	    else operator = armoise_term_create_mod;
	    result = s_expand_variables (vctx, op);
	
	    for (i = 1; i < nb_operands; i++)
	      {
		op = armoise_term_get_operand (t, i);
		tmp[0] = s_expand_variables (vctx, op);
		tmp[1] = operator (result, tmp[0]);
		armoise_term_del_reference (tmp[0]);
		armoise_term_del_reference (result);
		result = tmp[1];
	      }
	  }
	  break;

	case ARMOISE_T_NEG:
	  {
	    const armoise_term *op = armoise_term_get_operand (t, 0);
	    armoise_term *tmp = s_expand_variables (vctx, op);
	    result = armoise_term_create_neg (tmp);
	    armoise_term_del_reference (tmp);
	  }
	  break;
	}
    }

  return result;
}

			/* --------------- */

static armoise_term *
s_apply_unary (armoise_term *t, armoise_term *(*operator)(armoise_term *))
{
  armoise_term *result;
  int d = armoise_term_get_dim (t);

  if (d == 0)
    result = operator (t);
  else 
    {
      int i;
      armoise_term **tmp = ccl_new_array (armoise_term *, d);

      ccl_pre (armoise_term_get_kind (t) == ARMOISE_T_VECTOR);

      for (i = 0; i < d; i++)
	{
	  armoise_term *op = armoise_term_get_operand_at (t, i);
	  tmp[i] = s_apply_unary (op, operator);
	  armoise_term_del_reference (op);
	}
      result = armoise_term_create_vector (d, tmp);
      
      for (i = 0; i < d; i++)
	armoise_term_del_reference (tmp[i]);
      ccl_delete (tmp);
    }

  return result;
}

			/* --------------- */

static armoise_term *
s_apply_binary (armoise_term *t1, armoise_term *t2, 
		armoise_term *(*operator)(armoise_term *, armoise_term *))
{
  armoise_term *result;
  int d = armoise_term_get_dim (t1);

  ccl_pre (armoise_term_get_dim (t2) == d);

  if (d == 0)
    result = operator (t1, t2);
  else 
    {
      int i;
      armoise_term **tmp = ccl_new_array (armoise_term *, d);

      ccl_pre (armoise_term_get_kind (t1) == ARMOISE_T_VECTOR);
      ccl_pre (armoise_term_get_kind (t2) == ARMOISE_T_VECTOR);

      for (i = 0; i < d; i++)
	{
	   armoise_term *op1 = armoise_term_get_operand_at (t1, i);
	   armoise_term *op2 = armoise_term_get_operand_at (t2, i);
	   tmp[i] = s_apply_binary (op1, op2, operator);
	   armoise_term_del_reference (op1);
	   armoise_term_del_reference (op2);
	}
      result = armoise_term_create_vector (d, tmp);
      
      for (i = 0; i < d; i++)
	armoise_term_del_reference (tmp[i]);
      ccl_delete (tmp);
    }

  return result;
}

			/* --------------- */

static armoise_term *
s_apply_mul_binary (armoise_term *t1, armoise_term *t2, 
		    armoise_term *(*operator)(armoise_term *, armoise_term *))
{
  armoise_term *result;
  int d = armoise_term_get_dim (t1);

  ccl_pre (armoise_term_get_dim (t2) == 0);

  if (d == 0)
    result = operator (t1, t2);
  else 
    {
      int i;
      armoise_term **tmp = ccl_new_array (armoise_term *, d);

      ccl_pre (armoise_term_get_kind (t1) == ARMOISE_T_VECTOR);

      for (i = 0; i < d; i++)
	{
	  armoise_term *op1 = armoise_term_get_operand_at (t1, i);
	  tmp[i] = s_apply_mul_binary (op1, t2, operator);
	  armoise_term_del_reference (op1);
	}
      result = armoise_term_create_vector (d, tmp);
      
      for (i = 0; i < d; i++)
	armoise_term_del_reference (tmp[i]);
      ccl_delete (tmp);
    }

  return result;
}

			/* --------------- */

static armoise_term *
s_expand_vectorial_terms (armoise_term *t)
{
  armoise_term *result;
  int d = armoise_term_get_dim (t);

  if (d == 0)
    result = armoise_term_add_reference (t);
  else
    {
      int i;
      armoise_term_kind kind = armoise_term_get_kind (t);

      switch (kind)    
	{
	case ARMOISE_T_CONSTANT:
	case ARMOISE_T_VARIABLE:
	case ARMOISE_T_ELEMENT:      
	  ccl_throw_no_msg (internal_error);
	  break;

	case ARMOISE_T_VECTOR:
	  {
	    armoise_term **tmp = ccl_new_array (armoise_term *, d);
	    for (i = 0; i < d; i++)
	      {
		armoise_term *op = armoise_term_get_operand_at (t, i);
		tmp[i] = s_expand_vectorial_terms (op);
		armoise_term_del_reference (op);
	      }
	    result = armoise_term_create_vector (d, tmp);

	    for (i = 0; i < d; i++)
	      armoise_term_del_reference (tmp[i]);
	    ccl_delete (tmp);
	  }
	  break;

	case ARMOISE_T_NEG:
	  {
	    armoise_term *op = armoise_term_get_operand_at (t, 0);
	    armoise_term *tmp = s_expand_vectorial_terms (op);
	    armoise_term_del_reference (op);
	    result = s_apply_unary (tmp, armoise_term_create_neg);
	    armoise_term_del_reference (tmp);
	  }
	  break;

	case ARMOISE_T_ADD:      
	case ARMOISE_T_SUB:
	  {
	    int i;
	    int nb_operands = armoise_term_get_nb_operands (t);
	    armoise_term *op1 = armoise_term_get_operand_at (t, 0);
	    armoise_term *tmp1 = s_expand_vectorial_terms (op1);
	    armoise_term *op2 = armoise_term_get_operand_at (t, 1);
	    armoise_term *tmp2 = s_expand_vectorial_terms (op2);
	    armoise_term *(*operator)(armoise_term *, armoise_term *);
	    armoise_term_del_reference (op1);
	    armoise_term_del_reference (op2);

	    if (kind == ARMOISE_T_ADD) 
	      operator = armoise_term_create_add;
	    else 
	      operator = armoise_term_create_sub;
	    result = s_apply_binary (tmp1, tmp2, operator);
	    armoise_term_del_reference (tmp1);
	    armoise_term_del_reference (tmp2);

	    for (i = 2; i < nb_operands; i++)
	      {
		op1 = armoise_term_get_operand_at (t, i);
		tmp1 = s_expand_vectorial_terms (op1);
		armoise_term_del_reference (op1);
		tmp2 = s_apply_binary (result, tmp1, operator);
		armoise_term_del_reference (result);
		armoise_term_del_reference (tmp1);
		result = tmp2;
	      }
	  }
	  break;

	case ARMOISE_T_MUL:
	case ARMOISE_T_DIV:
	case ARMOISE_T_MOD:
	  {
	    int i;
	    armoise_term *op;
	    int vindex = -1;
	    int nb_operands = armoise_term_get_nb_operands (t);
	    armoise_term *(*operator)(armoise_term *, armoise_term *);
	    
	    if (kind == ARMOISE_T_MUL) 
	      {
		operator = armoise_term_create_mul;
		for (i = 0; i < nb_operands && vindex == -1; i++)
		  {
		    op = armoise_term_get_operand_at (t, i);
		    if (armoise_term_get_dim (op) > 0)
		      vindex = i;
		    armoise_term_del_reference (op);
		  }
		ccl_assert (vindex != -1);
	      }
	    else if (kind == ARMOISE_T_DIV) 
	      operator = armoise_term_create_div;
	    else 
	      operator = armoise_term_create_mod;

	    op = armoise_term_get_operand_at (t, vindex);
	    result = s_expand_vectorial_terms (op);
	    armoise_term_del_reference (op);
	    for (i = 0; i < nb_operands; i++)
	      {
		if (i != vindex)
		  {
		    armoise_term *tmp;
		    op = armoise_term_get_operand_at (t, i);
		    ccl_assert (armoise_term_get_dim (op) == 0);
		    tmp = s_apply_mul_binary (result, op, operator);
		    armoise_term_del_reference (op);
		    armoise_term_del_reference (result);
		    result = tmp;
		  }
	      }
	  }
	  break;
	default:
	  ccl_throw_no_msg (internal_error);
	}

    }

  return result;
}

			/* --------------- */

static void
s_eval_constant_scalar_term (const armoise_term *t, int *p_num, int *p_den)
{
  int i;
  int nb_operands;
  const armoise_term *op;
  armoise_term_kind kind = armoise_term_get_kind (t);

  switch (kind)
    {
    case ARMOISE_T_VECTOR:
    case ARMOISE_T_VARIABLE:      
    case ARMOISE_T_ELEMENT:
      ccl_throw_no_msg (internal_error);
      break;

    case ARMOISE_T_CONSTANT:
      armoise_term_get_value (t, p_num, p_den);
      break;

    case ARMOISE_T_NEG:
      op = armoise_term_get_operand (t, 0);
      s_eval_constant_scalar_term (op, p_num, p_den);
      *p_num = -(*p_num);
      break;

    case ARMOISE_T_ADD:
    case ARMOISE_T_SUB:
    case ARMOISE_T_MUL:
    case ARMOISE_T_DIV:
    case ARMOISE_T_MOD:
      nb_operands = armoise_term_get_nb_operands (t);
      op = armoise_term_get_operand (t, 0);
      s_eval_constant_scalar_term (op, p_num, p_den);
      
      for (i = 1; i < nb_operands; i++)
	{
	  int num, den;
	  op = armoise_term_get_operand (t, i);
	  s_eval_constant_scalar_term (op, &num, &den);
	  if (kind == ARMOISE_T_ADD) 
	    {
	      *p_num = (*p_num) * den + num * (*p_den);
	      *p_den *= den;
	    }
	  else if (kind == ARMOISE_T_SUB) 
	    {
	      *p_num = (*p_num) * den - num * (*p_den);
	      *p_den *= den;
	    }
	  else if (kind == ARMOISE_T_MUL)
	    {
	      *p_num *= num;
	      *p_den *= den;
	    }
	  else if (kind == ARMOISE_T_DIV) 
	    {
	      *p_num *= den;
	      *p_den *= num;
	    }
	  else 
	    {
	      *p_num = *p_num * den % *p_den * num;
	      *p_den = *p_den * den;
	    }
	}
      break;

    default:
      ccl_throw_no_msg (internal_error);
    }
}

			/* --------------- */

/*
 * negate a normlized term t
 */
static armoise_term *
s_negate_scalar_term (armoise_term *t, alambic_error *p_err)
{
  armoise_term *result;
  armoise_term_kind kind = armoise_term_get_kind (t);

  if (*p_err != ALAMBIC_NO_ERROR)
    return NULL;

  switch (kind)    
    {
    case ARMOISE_T_VECTOR:      
      ccl_throw_no_msg (internal_error);
      break;

    case ARMOISE_T_CONSTANT:
      {
	int num, den;
	armoise_term_get_value (t, &num, &den);
	result = armoise_term_create_constant (num, den);
      }
      break;

    case ARMOISE_T_VARIABLE:      
    case ARMOISE_T_ELEMENT:
      result = armoise_term_create_neg (t);
      break;

    case ARMOISE_T_ADD:
      {
	int i;
	armoise_term *tmp[2];
	int nb_operands = armoise_term_get_nb_operands (t);
	tmp[0] = armoise_term_get_operand_at (t, 0);
	result = s_negate_scalar_term (tmp[0], p_err);
	armoise_term_del_reference (tmp[0]);
	if (result != NULL)
	  {
	    for (i = 1; i < nb_operands && result != NULL; i++)
	      {
		tmp[0] = armoise_term_get_operand_at (t, i);
		tmp[1] = armoise_term_create_sub (result, tmp[0]);
		armoise_term_del_reference (tmp[0]);
		armoise_term_del_reference (result);
		result = tmp[1];
	      }
	  }
      }
      break;

    case ARMOISE_T_SUB:
      {
	int i;
	armoise_term *tmp[2];
	int nb_operands = armoise_term_get_nb_operands (t);
	result = armoise_term_get_operand_at (t, 1);
	
	for (i = 2; i < nb_operands; i++)
	  {
	    tmp[0] = armoise_term_get_operand_at (t, i);
	    tmp[1] = armoise_term_create_add (result, tmp[0]);
	    armoise_term_del_reference (tmp[0]);
	    armoise_term_del_reference (result);
	    result = tmp[1];
	  }
	tmp[0] = armoise_term_get_operand_at (t, 0);
	tmp[1] = armoise_term_create_sub (result, tmp[0]);
	armoise_term_del_reference (tmp[0]);
	armoise_term_del_reference (result);
	result = tmp[1];
      }
      break;

    case ARMOISE_T_NEG:
      result = armoise_term_get_operand_at (t, 0);
      break;

    case ARMOISE_T_MUL:
      {
	/* Since the term is normalized it must be of the form a.x. The 
	 * negation is realized by neg(a).x 
	 */
	int num, den;
	armoise_term *op1 = armoise_term_get_operand_at (t, 0);
	armoise_term *op2 = armoise_term_get_operand_at (t, 1);

	ccl_assert (armoise_term_get_nb_operands (t) == 2);
	ccl_assert (armoise_term_get_kind (op1) == ARMOISE_T_CONSTANT);
	ccl_assert (armoise_term_get_kind (op2) == ARMOISE_T_VARIABLE ||
		    armoise_term_get_kind (op2) == ARMOISE_T_ELEMENT);

	armoise_term_get_value (op1, &num, &den);
	num = -num;
	
	if (num == 1 && den == 1)
	  result = armoise_term_add_reference (op2);
	else if (num == -1 && den == 1)
	  result = armoise_term_create_neg (op2);
	else 
	  {
	    armoise_term *tmp = armoise_term_create_constant (num, den);
	    result = armoise_term_create_mul (tmp, op2);
	    armoise_term_del_reference (tmp);
	  }
	armoise_term_del_reference (op1);
	armoise_term_del_reference (op2);
      }
      break;

    case ARMOISE_T_DIV:
    case ARMOISE_T_MOD:	  
      /* Theses cases have been removed by a prior treatment */

    default:
      ccl_throw_no_msg (internal_error);
    }

  return result;
}

			/* --------------- */

static armoise_term *
s_mul_scalar_terms (armoise_term *t1, armoise_term *t2, alambic_error *p_err);

static armoise_term *
s_distribute (armoise_term *t1, armoise_term *t2, alambic_error *p_err)
{
  int i;
  armoise_term *result;
  armoise_term *tmp[2];
  int nb_operands = armoise_term_get_nb_operands (t2);
  armoise_term_kind k2 = armoise_term_get_kind (t2);
  armoise_term *(*operator) (armoise_term *, armoise_term *);

  ccl_assert (k2 == ARMOISE_T_ADD || k2 == ARMOISE_T_SUB);

  if (k2 == ARMOISE_T_ADD) 
    operator = armoise_term_create_add;
  else
    operator = armoise_term_create_sub;

  tmp[0] = armoise_term_get_operand_at (t2, 0);
  result = s_mul_scalar_terms (t1, tmp[0], p_err);
  armoise_term_del_reference (tmp[0]);
  
  for (i = 1; i < nb_operands && result != NULL; i++)
    {
      tmp[0] = armoise_term_get_operand_at (t2, i);
      tmp[1] = s_mul_scalar_terms (t1, tmp[0], p_err);
      armoise_term_del_reference (tmp[0]);
      
      if (tmp[1] != NULL)
	{
	  tmp[0] = operator (result, tmp[1]);
	  armoise_term_del_reference (result);
	  armoise_term_del_reference (tmp[1]);
	  result = tmp[0];
	}
      else
	{
	  armoise_term_del_reference (result);
	  result = NULL;
	}      
    }

  return result;  
}

			/* --------------- */

/*!
 * t1 and t2 are normalized. If neither t1 nor t2 is a constant then
 * the term t1 * t2 is not a linear term. 
 * rewriting rules:
 * 1.1) c * c --> eval (c * c)
 * 1.2) c * var -> c * var 
 * 1.3) c * (t1 + t2) -> mul (c, t1) + mul (c, t2)
 * 1.4) c * (t1 - t2) -> mul (c, t1) - mul (c, t2)
 * 1.5) c * (t1 * t2) -> eval (c, t1) * t2
 * 1.6) c * - t -> eval (-c) * t
 */
static armoise_term *
s_mul_scalar_terms (armoise_term *t1, armoise_term *t2, alambic_error *p_err)
{
  armoise_term *result;
  armoise_term_kind k1 = armoise_term_get_kind (t1);
  armoise_term_kind k2 = armoise_term_get_kind (t2);
  
  if (*p_err != ALAMBIC_NO_ERROR)
    return NULL;

  /* First we check that at least one of the two terms is a constant. In this
   * case we set it as the left operand
   */ 
  if (k1 != ARMOISE_T_CONSTANT)
    {
      if (k2 != ARMOISE_T_CONSTANT)
	*p_err = ALAMBIC_INVALID_MUL_FACTOR;
      else
	{
	  armoise_term_kind k = k2;
	  armoise_term *t = t2;
	  t2 = t1;
	  t1 = t;
	  k2 = k1;
	  k1 = k;
	}
    }

  if (*p_err != ALAMBIC_NO_ERROR)
    return NULL;

  if (k2 == ARMOISE_T_CONSTANT) 
    { /* rule 1.1 */
      int num1, num2, den1, den2;
      armoise_term_get_value (t1, &num1, &den1);
      armoise_term_get_value (t2, &num2, &den2);
      
      result = armoise_term_create_constant (num1 * num2, den1 * den2);
    }
  else if (k2 == ARMOISE_T_VARIABLE || k2 == ARMOISE_T_ELEMENT)
    { /* rule 1.2 */
      result = armoise_term_create_mul (t1, t2);
    }
  else if (k2 == ARMOISE_T_ADD || k2 == ARMOISE_T_SUB)
    { /* rules 1.3 and 1.4 */
      result = s_distribute (t1, t2, p_err);
    }
  else if (k2 == ARMOISE_T_MUL)
    { /* rule 1.5 */
      armoise_term *tmp[2];
      int num[2], den[2];

      armoise_term_get_value (t1, &num[0], &den[0]);
      ccl_assert (armoise_term_get_nb_operands (t2) == 2);
      tmp[0] = armoise_term_get_operand_at (t2, 0);
      ccl_assert (armoise_term_get_kind (tmp[0]) == ARMOISE_T_CONSTANT);
      armoise_term_get_value (tmp[0], &num[1], &den[1]);
      armoise_term_del_reference (tmp[0]);
      tmp[0] = armoise_term_create_constant (num[0] * num[1], den[0] * den[1]);

      tmp[1] = armoise_term_get_operand_at (t2, 1);
      ccl_assert (armoise_term_get_kind (tmp[1]) == ARMOISE_T_VARIABLE ||
		  armoise_term_get_kind (tmp[1]) == ARMOISE_T_ELEMENT);
      result = armoise_term_create_mul (tmp[0], tmp[1]);
      armoise_term_del_reference (tmp[0]);
      armoise_term_del_reference (tmp[1]);
    }
  else 
    { /* rule 1.6 */
      armoise_term *tmp[2];
      int num, den;

      armoise_term_get_value (t1,&num,&den);
      
      ccl_assert (k2 == ARMOISE_T_NEG);
      
      tmp[0] = armoise_term_create_constant (-num, den);
      
      tmp[1] = armoise_term_get_operand_at (t2, 1);
      ccl_assert (armoise_term_get_kind (tmp[1]) == ARMOISE_T_VARIABLE);
      result = armoise_term_create_mul (tmp[0], tmp[1]);
      armoise_term_del_reference (tmp[0]);
      armoise_term_del_reference (tmp[1]);
    }

  return result;
}

			/* --------------- */

static armoise_term *
s_normalize_scalar_terms (armoise_term *t, alambic_error *p_err)
{
  armoise_term *result;
  armoise_term_kind kind = armoise_term_get_kind (t);

  if (*p_err != ALAMBIC_NO_ERROR)
    return NULL;

  if (armoise_term_is_constant (t) && kind != ARMOISE_T_VECTOR)
    {
      int num, den;
      s_eval_constant_scalar_term (t, &num, &den);
      result = armoise_term_create_constant (num, den);
    }
  else
    {
      switch (kind)    
	{
	case ARMOISE_T_VARIABLE:      
	case ARMOISE_T_ELEMENT:
	  result = armoise_term_add_reference (t);
	  break;

	case ARMOISE_T_VECTOR:
	  {
	    int i;
	    int nb_operands = armoise_term_get_nb_operands (t);
	    armoise_term **tmp = ccl_new_array (armoise_term *, nb_operands);
	    for (i = 0; i < nb_operands; i++)
	      {
		armoise_term *op = armoise_term_get_operand_at (t, i);
		tmp[i] = s_normalize_scalar_terms (op, p_err);
		armoise_term_del_reference (op);
	      }
	    result = armoise_term_create_vector (nb_operands, tmp);
	    for (i = 0; i < nb_operands; i++)
	      ccl_zdelete (armoise_term_del_reference, tmp[i]);
	    ccl_delete (tmp);
	  }
	  break;

	case ARMOISE_T_ADD:
	case ARMOISE_T_SUB:
	  {
	    int i;
	    armoise_term *tmp[2];
	    int nb_operands = armoise_term_get_nb_operands (t);
	    tmp[0] = armoise_term_get_operand_at (t, 0);
	    result = s_normalize_scalar_terms (tmp[0], p_err);
	    armoise_term_del_reference (tmp[0]);

	    for (i = 1; i < nb_operands && result != NULL; i++)
	      {
		tmp[0] = armoise_term_get_operand_at (t, i);
		tmp[1] = s_normalize_scalar_terms (tmp[0], p_err);
		armoise_term_del_reference (tmp[0]);
		if (tmp[1] != NULL)
		  {
		    if (kind == ARMOISE_T_ADD)
		      tmp[0] = armoise_term_create_add (result, tmp[1]);
		    else
		      tmp[0] = armoise_term_create_sub (result, tmp[1]);
		    armoise_term_del_reference (tmp[1]);
		    armoise_term_del_reference (result);
		    result = tmp[0];
		  }
	      }
	  }
	  break;

	case ARMOISE_T_NEG:
	  {
	    armoise_term *op = armoise_term_get_operand_at (t, 0);
	    armoise_term *tmp = s_normalize_scalar_terms (op, p_err);
	    armoise_term_del_reference (op);

	    if (tmp != NULL)
	      {
		result = s_negate_scalar_term (tmp, p_err);
		armoise_term_del_reference (tmp);
	      }
	  }
	  break;

	case ARMOISE_T_MUL:
	  {
	    int i;
	    int nb_operands = armoise_term_get_nb_operands (t);
	    armoise_term *tmp[2];

	    tmp[0] = armoise_term_get_operand_at (t, 0);
	    result = s_normalize_scalar_terms (tmp[0], p_err);
	    armoise_term_del_reference (tmp[0]);

	    for (i = 1; i < nb_operands && result != NULL; i++)
	      {
		tmp[0] = armoise_term_get_operand_at (t, i);
		tmp[1] = s_normalize_scalar_terms (tmp[0], p_err);
		armoise_term_del_reference (tmp[0]);
		tmp[0] = s_mul_scalar_terms (result, tmp[1], p_err);
		ccl_zdelete (armoise_term_del_reference, tmp[1]);
		armoise_term_del_reference (result);
		result = tmp[0];
	      }
	  }
	  break;

	case ARMOISE_T_CONSTANT:
	  /* This case is treated at the beginning of the function by checking
	   * that 't' is constant 
	   */

	case ARMOISE_T_DIV:
	case ARMOISE_T_MOD:
	  /* Theses cases have been removed by a prior treatment */

	default:
	  ccl_throw_no_msg (internal_error);
	}
    }

  return result;
}

			/* --------------- */

static armoise_term *
s_normalize_term (armoise_context *vctx, const armoise_term *t, 
		  alambic_error *p_err)
{
  armoise_term *tmp1 = s_expand_variables (vctx, t);
  armoise_term *tmp2 = s_expand_vectorial_terms (tmp1);
  armoise_term *result = s_normalize_scalar_terms (tmp2, p_err);
  armoise_term_del_reference (tmp1);
  armoise_term_del_reference (tmp2);

  return result;
}

/* 
 * This function collects recursively the components of the term 't'. 
 * If 't' is scalar then the result is 't' itself.
 * If 't' has a vectorial value then the expression is projected on each
 * dimension.
 */
static void
s_collect_term_components_rec (armoise_term *t, armoise_term **comps, 
			       int *p_last_index)
{
  int d = armoise_term_get_dim (t);

  if (d == 0) /* the trivial case where 't' is a scalar */
    {
      comps[*p_last_index] = armoise_term_add_reference (t);
      (*p_last_index)++;
    }
  else
    {
      int i;

      ccl_assert (armoise_term_get_kind (t) == ARMOISE_T_VECTOR);

      for (i = 0; i < d; i++)
	{
	  armoise_term *op = armoise_term_get_operand_at (t, i);
	  s_collect_term_components_rec (op, comps, p_last_index);
	  armoise_term_del_reference (op);
	}
    }  
}

/*
 * This function expands the normalized term 't' in its components. 
 */
static armoise_term **
s_collect_term_components (armoise_term *t, int *p_nb_comps)
{  
  armoise_type *type = armoise_term_get_type (t);
  int w = armoise_type_get_depth_width (type);
  armoise_term **result = ccl_new_array (armoise_term *, w);
  *p_nb_comps = 0;
  s_collect_term_components_rec (t, result, p_nb_comps);
  armoise_type_del_reference (type);

  ccl_post (*p_nb_comps == w);

  return result;
}

			/* --------------- */

static armoise_formula *
s_rewrite_vectorial_in (armoise_context *ctx,  armoise_term *t,
			armoise_predicate *P)
{
  armoise_formula *result;

  if (armoise_predicate_get_kind (P) == ARMOISE_P_PRODUCT)
    {
      int i;
      armoise_formula *tmp[2];
      armoise_term *op_t;
      armoise_predicate *op_P;
      int nb_operands = armoise_predicate_get_nb_operands (P);

      ccl_assert (armoise_term_get_kind (t) == ARMOISE_T_VECTOR);

      op_t = armoise_term_get_operand_at (t, 0);
      op_P = armoise_predicate_get_operand_at (P, 0);
      result = armoise_formula_create_in (op_t, op_P);
      armoise_term_del_reference (op_t);
      armoise_predicate_del_reference (op_P);

      for (i = 1; i < nb_operands; i++)
	{
	  op_t = armoise_term_get_operand_at (t, i);
	  op_P = armoise_predicate_get_operand_at (P, i);
	  tmp[0] = s_rewrite_vectorial_in (ctx, op_t, op_P);
	  armoise_term_del_reference (op_t);
	  armoise_predicate_del_reference (op_P);

	  tmp[1] = armoise_formula_create_and (result, tmp[0]);
	  armoise_formula_del_reference (tmp[0]);
	  armoise_formula_del_reference (result);
	  result = tmp[1];
	}
    }
  else if (armoise_predicate_get_kind (P) == ARMOISE_P_PAREN || 
	   armoise_predicate_get_kind (P) == ARMOISE_P_INDIRECTION)
    {
      armoise_predicate *Paux = armoise_predicate_get_operand_at (P, 0);

      result = s_rewrite_vectorial_in (ctx, t, Paux);
      armoise_predicate_del_reference (Paux);
    }
  else   
    {
      result = armoise_formula_create_in (t, P);
    }

  return result;
}
