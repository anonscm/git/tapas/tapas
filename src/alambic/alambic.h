/*
 * alambic.h -- A tool to translate ARMOISE formula into GENEPI calls
 * 
 * This file is a part of the Alambic Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \page alambic The Alambic Library 
 * \brief A tool to translate ARMOISE formula into GENEPI calls   
 */

/*!
 * \file alambic/alambic.h
 * \brief Alambic API 
 * 
 */
#ifndef __ALAMBIC_H__
# define __ALAMBIC_H__

# include <genepi/genepi.h>
# include <armoise/armoise.h>

BEGIN_C_DECLS

/*!
 * \brief Error code returned by Alambic functions.
 */
typedef enum {
  ALAMBIC_NO_ERROR = 0, /**< The operation terminates without error. */

  ALAMBIC_REAL_NOT_SUPPORTED, /**< The Genepi solver does not support variables
			       * taken their value in real numbers.
			       */

  ALAMBIC_POSI_NOT_SUPPORTED, /**< The Genepi solver does not support variables
			       * taken their value in positive real numbers.
			       */

  ALAMBIC_INT_NOT_SUPPORTED, /**< The Genepi solver does not support variables
			       * taken their value in integers.
			       */

  ALAMBIC_INVALID_MUL_DIMENSIONS, /**< The formula specifies a multiplication 
				   * of two relations with arities > 1. 
				   */ 

  ALAMBIC_INVALID_MUL_CARDINALITY, /**< The formula specifies a multiplication 
				    * of two infinite relations. 
				    */

  ALAMBIC_INVALID_MUL_FACTOR, /**< A non-linear factor is found. */ 

  ALAMBIC_INVALID_DIV_DIMENSIONS, /**< The formula specifies a division 
				   * of two relations with arities > 1. 
				   */ 

  ALAMBIC_INVALID_DIV_CARDINALITY,  /**< The formula specifies a division
				     * of two infinite relations.
				     */

  ALAMBIC_INVALID_MOD_DIMENSIONS, /**< The formula specifies a modulo between 
				   * two relations with arities > 1. 
				   */ 

  ALAMBIC_INVALID_MOD_CARDINALITY /**< The formula specifies a division
				     * of two infinite relations.
				     */
  
} alambic_error;

/*!
 * \brief Abstract type of a normalized ARMOISE predicate.
 * \see alambic_normalize_predicate
 */
typedef struct armoise_normalized_predicate_st armoise_normalized_predicate;

/*!
 * \brief Normalize the armoise predicate \a P.
 *
 * This rewriting procedure is required before computations with Genepi.
 * The rewriting rules are mainly applied to first order formulas:
 * \li vectorial variables are expanded into vectors
 * \li vectorial expressions are exploded
 * \li term comparisons are rewrited to obtain linear expressions  
 * \param parent the parent of the context used to create the normalized 
 *        version of \a P.
 * \param P the predicate to normalize
 * \param p_err address where an error status should be stored
 * \return a normalized version of \a P
 */
extern armoise_normalized_predicate *
alambic_normalize_predicate (armoise_context *parent, 
			     const armoise_predicate *P, alambic_error *p_err);

/*!
 * \brief Normalize the predicates stored in the tree \a t.
 * \param parent the parent of the context used to create the normalized 
 *        version of \a P.
 * \param t the tree containing the predicates to normalize
 * \param p_err address where an error status should be stored
 * \return a new tree containing the normalized version of predicates in \a t
 */
extern ccl_tree *
alambic_normalize_predicate_tree (armoise_context *parent, 
				  const ccl_tree *t, alambic_error *p_err);

/*!
 * \brief Computes the solutions of the normalized predicate \a P.
 * 
 * \param solver the Genepi solver 
 * \param P a normalized predicate
 * \param p_error address where an error status should be stored
 *
 * \return
 *  \li the \ref genepi_set encoding the elements of \a P 
 *  \li NULL if an error occurs.
 */
extern genepi_set *
alambic_compute_predicate (genepi_solver *solver, 
			   const armoise_normalized_predicate *P, 
			   alambic_error *p_error);

END_C_DECLS

#endif /* ! __ALAMBIC_H__ */
