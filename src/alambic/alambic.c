/*
 * alambic.c -- add a comment about this file
 * 
 * This file is a part of the Alambic Library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include <genepi/genepi.h>
#include <genepi/genepi-util.h>
#include <armoise/armoise.h>
#include <armoise/armoise-interp.h>
#include "alambic.h"

#define TODO() abort()

#define VAR_IMAX(_i32) ((_i32)>>16)
#define VAR_IMIN(_i32) ((_i32)&0x0000FFFF)
#define VAR_IRANGE(_min, _max) (((uint32_t)(_min))| (((uint32_t)(_max))<<16))

typedef struct
{
  genepi_solver *solver;
  ccl_hash *cache;
} compiler;

			/* --------------- */

static genepi_set *
s_compute_predicate (compiler *C, const armoise_predicate *P, 
		     alambic_error *p_err);

static genepi_set *
s_compute_set_for_term (compiler *C, const armoise_term *T, ccl_hash *ordering,
			int width, alambic_error *p_err);

static int
s_domain_is_supported (genepi_solver_type stype, const armoise_predicate *P, 
		       alambic_error *p_error);

			/* --------------- */

genepi_set *
alambic_compute_predicate (genepi_solver *solver, 
			   const armoise_normalized_predicate *nP, 
			   alambic_error *p_error)
{
  genepi_set *result = NULL;
  const armoise_predicate *P = (const armoise_predicate *)nP;
  genepi_solver_type stype = genepi_solver_get_solver_type (solver);

  if (s_domain_is_supported (stype, P, p_error))
    {
      compiler comp;
      ccl_pointer_iterator *sets;

      comp.solver = solver;
      comp.cache = ccl_hash_create (NULL, NULL, NULL, NULL);
      result = s_compute_predicate (&comp, P, p_error);      
	  
      sets = ccl_hash_get_elements (comp.cache);
      while (ccl_iterator_has_more_elements (sets))
	{
	  genepi_set *S = ccl_iterator_next_element (sets);
	  genepi_set_del_reference (solver, S);
	}
      ccl_iterator_delete (sets);
      ccl_hash_delete (comp.cache);
    }

  return result;
}

			/* --------------- */

static int
s_remove_one_element (genepi_solver *solver, genepi_set **pS, int *element,
		      int width)
{
  int den = genepi_set_get_one_solution (solver, *pS, element, width);
  genepi_set *el = genepi_set_singleton (solver, width, element, den);
  genepi_set *result = genepi_set_diff (solver, *pS, el);
  genepi_set_del_reference (solver, el);
  genepi_set_del_reference (solver, *pS);
  *pS = result;

  return den;
}

			/* --------------- */

static genepi_set *
s_genepi_set_mul (genepi_solver *solver, armoise_domain *d1, genepi_set *S1, 
		  armoise_domain *d2, genepi_set *S2, alambic_error *p_err)
{
  genepi_set *result = NULL;
  int w1 = genepi_set_get_width (solver, S1);
  int w2 = genepi_set_get_width (solver, S2);

  if (w1 > 1 && w2 > 1)
    *p_err = ALAMBIC_INVALID_MUL_DIMENSIONS;
  else if (!armoise_domain_is_finite (d1) && !armoise_domain_is_finite (d2))
    *p_err = ALAMBIC_INVALID_MUL_CARDINALITY;
  else
    {
      /* w is the width of the resulting relation */
      int w = w1 < w2 ? w2 : w1;

      if (! armoise_domain_is_finite (d1))
	{
	  armoise_domain *tmpd = d1;
	  genepi_set *tmpS = S1;
	  int tmpw = w1;
	  d1 = d2;
	  S1 = S2;
	  w1 = w2;
	  d2 = tmpd;
	  S2 = tmpS;
	  w2 = tmpw;
	}

      if (!genepi_set_is_empty (solver, S1) && 
	  !genepi_set_is_empty (solver, S2))
	{
	  int den1;
	  int *elS1 = ccl_new_array (int, w1);
	  genepi_set *tmpS1 = genepi_set_add_reference (solver, S1);

	  if (armoise_domain_is_finite (d2))
	    {
	      int den2;
	      int denr;
	      int *r = ccl_new_array (int, w);
	      int *elS2 = ccl_new_array (int, w2);
	      int *x = w1 == 1 ? elS2 : elS1;
	      int *c = w1 == 1 ? elS1 : elS2;

	      result = genepi_set_bot (solver, w);
	      while (!genepi_set_is_empty (solver, tmpS1))
		{
		  genepi_set *tmpS2 = genepi_set_add_reference (solver, S2);

		  den1 = s_remove_one_element (solver, &tmpS1, elS1, w1);

		  while (!genepi_set_is_empty (solver, tmpS2))
		    {
		      int i;
		      genepi_set *X;
		      genepi_set *tmp;
		      den2 = s_remove_one_element (solver, &tmpS2, elS2, w2);

		      for (i = 0; i < w; i++)
			r[i] = *c * x[i];
		      denr = den1 * den2;
		      X = genepi_set_singleton (solver, w, r, denr);
		      tmp = genepi_set_union (solver, result, X);
		      genepi_set_del_reference (solver, result);
		      genepi_set_del_reference (solver, X);
		      result = tmp;
		    }
		  genepi_set_del_reference (solver, tmpS2);
		}
	      ccl_delete (elS2);
	      ccl_delete (r);
	    }
	  else
	    {
	      int i;
	      int *sel = ccl_new_array (int, w + w2);
	      genepi_set *aux;

	      if (w1 == 1)
		{
		  for (i = 0; i < w + w2; i++)
		    {
		      if ((i % 2) == 0)
			sel[i] = 1;
		    }
		}
	      else
		{
		  for (i = 1; i < w + w2; i++)
		    sel[i] = 1;
		}

	      result = genepi_set_bot (solver, w + w2);
	      aux = genepi_set_invproject (solver, S2, sel, w + w2);
	      while (!genepi_set_is_empty (solver, tmpS1))
		{
		  genepi_set *tmpres;
		  genepi_set *X = genepi_set_add_reference (solver, aux);

		  den1 = s_remove_one_element (solver, &tmpS1, elS1, w1);
		  ccl_memzero (sel, sizeof (int) * (w + w2));

		  for (i = 0; i < w; i++)
		    {
		      genepi_set *tmp1;
		      genepi_set *tmp2;

		      if (w1 == 1)
			{
			  sel[2 * i] = den1;
			  sel[2 * i + 1] = -elS1[0];
			}
		      else
			{
			  sel[1 + i] = den1;
			  sel[0] = -elS1[i];
			}
		      tmp1 = 
			genepi_set_linear_equality (solver, sel, w + w2, 0);
		      tmp2 = genepi_set_intersection (solver, X, tmp1);
		      genepi_set_del_reference (solver, X);
		      genepi_set_del_reference (solver, tmp1);
		      X = tmp2;

		      if (w1 == 1)
			{
			  sel[2 * i] = 0;
			  sel[2 * i + 1] = 0;
			}
		      else
			{
			  sel[1 + i] = 0;
			  sel[0] = 0;
			}
		    }
		  tmpres = genepi_set_union (solver, result, X);
		  genepi_set_del_reference (solver, result);
		  genepi_set_del_reference (solver, X);
		  result = tmpres;
		}	      
	      genepi_set_del_reference (solver, aux);
	      if (w1 == 1)
		{
		  for (i = 0; i < w + w2; i++)
		    sel[i] = ((i % 2) == 1) ? 1 : 0;
		}
	      else
		{
		  sel[0] = 1;
		}
	      aux = genepi_set_project (solver, result, sel, w + w2);
	      genepi_set_del_reference (solver, result);
	      result = aux;
	      ccl_delete (sel);
	    }
	  ccl_delete (elS1);
	  genepi_set_del_reference (solver, tmpS1);
	}
      else
	{
	  result = genepi_set_bot (solver, w);
	}
    }

  return result;
}

			/* --------------- */

static genepi_set *
s_genepi_set_div (genepi_solver *solver, armoise_domain *d1, genepi_set *S1, 
		  armoise_domain *d2, genepi_set *S2, alambic_error *p_err)
{
  genepi_set *result = NULL;
  int w1 = genepi_set_get_width (solver, S1);
  int w2 = genepi_set_get_width (solver, S2);

  if (w2 > 1)
    *p_err = ALAMBIC_INVALID_DIV_DIMENSIONS;
  else if (!armoise_domain_is_finite (d2))
    *p_err = ALAMBIC_INVALID_DIV_CARDINALITY;
  else if (genepi_set_is_empty (solver, S1) || 
	   genepi_set_is_empty (solver, S2))
    result = genepi_set_bot (solver, w1);
  else
    {
      int i;
      int den2;
      int elS2;
      genepi_set *tmp[3];
      genepi_set *tmpS2 = genepi_set_add_reference (solver, S2);      
      int *sel = ccl_new_array (int, w1 * 2);
      genepi_set *extS1;

      for (i = 0; i < w1; i++)
	{
	  sel[2 * i] = 1;
	  sel[2 * i + 1] = 0;
	}
      extS1 = genepi_set_invproject (solver, S1, sel, 2 * w1);
      for (i = 0; i < w1; i++)
	{
	  sel[2 * i] = 0;
	  sel[2 * i + 1] = 0;
	}
      result = genepi_set_bot (solver, w1);

      while (!genepi_set_is_empty (solver, tmpS2))
	{
	  den2 = s_remove_one_element (solver, &tmpS2, &elS2, 1);
	  if (elS2 == 0)
	    continue;

	  tmp[2] = NULL;
	  for (i = 0; i < w1; i++)
	    {
	      sel[2 * i] = elS2;
	      sel[2 * i + 1] = -den2;
	      tmp[0] = genepi_set_linear_equality (solver, sel, 2 * w1, 0);
	      sel[2 * i] = 0;
	      sel[2 * i + 1] = 0;

	      if (tmp[2] == NULL)
		tmp[2] = tmp[0];
	      else
		{
		  tmp[1] = genepi_set_intersection (solver, tmp[2], tmp[0]);
		  genepi_set_del_reference (solver, tmp[2]);
		  genepi_set_del_reference (solver, tmp[0]);
		  tmp[2] = tmp[1];
		}
	    }

	  tmp[1] = genepi_set_intersection (solver, extS1, tmp[2]);
	  genepi_set_del_reference (solver, tmp[2]);

	  for (i = 0; i < w1; i++)
	    {
	      sel[2 * i] = 0;
	      sel[2 * i + 1] = 1;
	    }
	  tmp[0] = genepi_set_project (solver, tmp[1], sel, 2 * w1);
	  genepi_set_del_reference (solver, tmp[1]);
	  tmp[1] = genepi_set_union (solver, tmp[0], result);
	  genepi_set_del_reference (solver, tmp[0]);
	  genepi_set_del_reference (solver, result);
	  result = tmp[1];
	}
      ccl_delete (sel);
      genepi_set_del_reference (solver, extS1);
      genepi_set_del_reference (solver, tmpS2);
    }

  return result;
}

			/* --------------- */

static genepi_set *
s_genepi_set_mod (genepi_solver *solver, armoise_domain *d1, genepi_set *S1, 
		  armoise_domain *d2, genepi_set *S2, alambic_error *p_err)
{
  genepi_set *result = NULL;
  int w1 = genepi_set_get_width (solver, S1);
  int w2 = genepi_set_get_width (solver, S2);

  if (w2 > 1)
    *p_err = ALAMBIC_INVALID_DIV_DIMENSIONS;
  else if (!armoise_domain_is_finite (d2))
    *p_err = ALAMBIC_INVALID_DIV_CARDINALITY;
  else if (genepi_set_is_empty (solver, S1) || 
	   genepi_set_is_empty (solver, S2))
    result = genepi_set_bot (solver, w1);
  else
    {
      int i;
      int den2;
      int elS2;
      genepi_set *tmp[3];
      genepi_set *tmpS2 = genepi_set_add_reference (solver, S2);      
      int *sel = ccl_new_array (int, w1 * 3);
      genepi_set *extS1;

      /* x % y/d = z -> exists q d.x - q.y -d.z = 0 /\ 0 <= z < |y| */
      for (i = 0; i < w1; i++)
	{
	  sel[3 * i] = 0;     /* x */
	  sel[3 * i + 1] = 1; /* q */
	  sel[3 * i + 2] = 1; /* z */
	}
      extS1 = genepi_set_invproject (solver, S1, sel, 3 * w1);
      result = genepi_set_bot (solver, w1);

      while (!genepi_set_is_empty (solver, tmpS2))
	{
	  den2 = s_remove_one_element (solver, &tmpS2, &elS2, 1);
	  if (elS2 == 0)
	    continue;

	  for (i = 0; i < w1; i++)
	    {
	      sel[3 * i] = den2;
	      sel[3 * i + 1] = -elS2;
	      sel[3 * i + 2] = -den2;
	    }
	  tmp[0] = genepi_set_linear_equality (solver, sel, 3 * w1, 0);
	  for (i = 0; i < w1; i++)
	    {
	      sel[3 * i] = 0;
	      sel[3 * i + 1] = 0;
	      sel[3 * i + 2] = 1;
	    }
	  tmp[1] = 
	    genepi_set_linear_operation (solver, sel, 3 * w1, GENEPI_GEQ, 0);
	  tmp[2] = genepi_set_intersection (solver, tmp[0], tmp[1]);
	  genepi_set_del_reference (solver, tmp[0]);
	  genepi_set_del_reference (solver, tmp[1]);
	  for (i = 0; i < w1; i++)
	    {
	      sel[3 * i] = 0;
	      sel[3 * i + 1] = 0;
	      sel[3 * i + 2] = den2;
	    }
	  if (elS2 > 0)
	    tmp[0] = genepi_set_linear_operation (solver, sel, 3 * w1, 
						  GENEPI_LT, elS2);
	  else
	    tmp[0] = genepi_set_linear_operation (solver, sel, 3 * w1, 
						  GENEPI_LT, -elS2);
	  tmp[1] = genepi_set_intersection (solver, tmp[0], tmp[2]);
	  genepi_set_del_reference (solver, tmp[0]);
	  genepi_set_del_reference (solver, tmp[2]);
	  tmp[0] = genepi_set_intersection (solver, tmp[1], extS1);
	  genepi_set_del_reference (solver, tmp[1]);

	  for (i = 0; i < w1; i++)
	    {
	      sel[3 * i] = 1;
	      sel[3 * i + 1] = 1;
	      sel[3 * i + 2] = 0;
	    }
	  tmp[1] = genepi_set_project (solver, tmp[0], sel, 3 * w1);
	  genepi_set_del_reference (solver, tmp[0]);
	  tmp[0] = genepi_set_union (solver, tmp[1], result);
	  genepi_set_del_reference (solver, tmp[1]);
	  genepi_set_del_reference (solver, result);
	  result = tmp[0];
	}
      genepi_set_del_reference (solver, extS1);
      genepi_set_del_reference (solver, tmpS2);
    }

  return result;
}

			/* --------------- */

static genepi_set *
s_compute_set (compiler *C, const armoise_predicate *P, alambic_error *p_err);

static genepi_set *
s_set_add_cst (genepi_solver *solver, genepi_set *X, genepi_set *cst);

static genepi_set *
s_compute_predicate (compiler *C, const armoise_predicate *P, 
		     alambic_error *p_err)
{
  int width;
  armoise_type *type;
  armoise_domain *domain;
  genepi_set *result = NULL;
  armoise_predicate_kind kind = armoise_predicate_get_kind (P);


  if (*p_err != ALAMBIC_NO_ERROR)
    return NULL;

  if (ccl_hash_find (C->cache, P))
    {
      result = ccl_hash_get (C->cache);
      result = genepi_set_add_reference (C->solver, result);
    }
  else
    {
      static int k = 0;
      type = armoise_predicate_get_type (P);
      width = armoise_type_get_width (type);
      domain = armoise_predicate_get_domain (P);

      ccl_debug_push_tab_level ();
      ccl_debug ("[%d] compute predicate for ", k);
      armoise_predicate_log (CCL_LOG_DEBUG, P);
      ccl_debug ("\n");
      k++;

      switch (kind)
	{
	case ARMOISE_P_EMPTY:
	  if (width == 0)
	    width++;
	  result = genepi_set_bot (C->solver, width);
	  break;

	case ARMOISE_P_NAT:
	  result = genepi_set_top_N (C->solver, 1);
	  break;

	case ARMOISE_P_INT:
	  if (genepi_solver_get_solver_type (C->solver) >= GENEPI_Z_SOLVER)
	    result = genepi_set_top_Z (C->solver, 1);
	  else
	    *p_err = ALAMBIC_INT_NOT_SUPPORTED;
	  break;

	case ARMOISE_P_POSI:  
	  if (genepi_solver_get_solver_type (C->solver) >= GENEPI_P_SOLVER)
	    result = genepi_set_top_P (C->solver, 1);
	  else
	    *p_err = ALAMBIC_POSI_NOT_SUPPORTED;

	  break;
	case ARMOISE_P_REAL:
	  if (genepi_solver_get_solver_type (C->solver) >= GENEPI_R_SOLVER)
	    result = genepi_set_top_R (C->solver, 1);
	  else
	    *p_err = ALAMBIC_REAL_NOT_SUPPORTED;
	  break;
      
	case ARMOISE_P_UNION:  
	case ARMOISE_P_INTERSECTION:
	case ARMOISE_P_DIFF:
	case ARMOISE_P_DELTA:
	  {	
	    int i;
	    int nb_operands = armoise_predicate_get_nb_operands (P);
	    const armoise_predicate *op = armoise_predicate_get_operand (P, 0);
	    result = s_compute_predicate (C, op, p_err);
	    for (i = 1; i < nb_operands && result != NULL; i++)
	      {
		genepi_set *tmp;

		op = armoise_predicate_get_operand (P, i);
		tmp = s_compute_predicate (C, op, p_err);
		if (tmp == NULL)
		  {
		    genepi_set_del_reference (C->solver, result);
		    result = NULL;
		  }
		else
		  {
		    genepi_set *tmp2; 
		    genepi_set *(*operator)(genepi_solver *, genepi_set *, 
					    genepi_set *);

		    if (kind == ARMOISE_P_UNION)
		      operator = genepi_set_union;
		    else if (kind == ARMOISE_P_INTERSECTION)
		      operator = genepi_set_intersection;
		    else if (kind == ARMOISE_P_DIFF)
		      operator = genepi_set_diff;
		    else 
		      operator = genepi_set_delta;
		    tmp2 = operator (C->solver, result, tmp);
		    genepi_set_del_reference (C->solver, tmp);
		    genepi_set_del_reference (C->solver, result);
		    result = tmp2;
		  }
	      }	
	  }
	  break;

	case ARMOISE_P_COMPLEMENT:
	  {
	    const armoise_predicate *op = armoise_predicate_get_operand (P, 0);
	    genepi_set *opS = s_compute_predicate (C, op, p_err);
	    if (opS != NULL)
	      {
		result = genepi_set_complement (C->solver, opS);
		genepi_set_del_reference (C->solver, opS);
	      }
	  }
	  break;

	case ARMOISE_P_PRODUCT:
	case ARMOISE_P_ADD:
	case ARMOISE_P_SUB:
	  {
	    int i;
	    int nb_operands = armoise_predicate_get_nb_operands (P);
	    const armoise_predicate *op = armoise_predicate_get_operand (P, 0);
	    armoise_domain *dop = armoise_predicate_get_domain (op);
	    int res_is_finite = armoise_domain_is_finite (dop);
	    result = s_compute_predicate (C, op, p_err);
	    armoise_domain_del_reference (dop);

	    for (i = 1; i < nb_operands && result != NULL; i++)
	      {
		genepi_set *tmp;

		op = armoise_predicate_get_operand (P, i);
		tmp = s_compute_predicate (C, op, p_err);
		if (tmp == NULL)
		  {
		    genepi_set_del_reference (C->solver, result);
		    result = NULL;
		  }
		else
		  {
		    genepi_set *tmp2;
		    if (kind == ARMOISE_P_PRODUCT)
		      tmp2 = 
			genepi_set_cartesian_product (C->solver, result, tmp);
		    else if (kind == ARMOISE_P_SUB)
		      tmp2 = genepi_set_sub (C->solver, result, tmp);
		    else 
		      {
			armoise_domain *dop = armoise_predicate_get_domain (op);
			/*			
			if (res_is_finite)
			  {
			    tmp2 = s_set_add_cst (C->solver, tmp, result);
			    res_is_finite = armoise_domain_is_finite (dop);
			  }
			else if (armoise_domain_is_finite (dop))
			  tmp2 = s_set_add_cst (C->solver, result, tmp);
			  else*/
			  tmp2 = genepi_set_add (C->solver, result, tmp);
			armoise_domain_del_reference (dop);
		      }
		    genepi_set_del_reference (C->solver, tmp);
		    genepi_set_del_reference (C->solver, result);
		    result = tmp2;
		  }
	      }	
	  }
	  break;

	case ARMOISE_P_NEG:
	  {
	    const armoise_predicate *op = armoise_predicate_get_operand (P, 0);
	    genepi_set *tmp = result = s_compute_predicate (C, op, p_err);
	    if (tmp != NULL)
	      {
		result = genepi_set_neg (C->solver, tmp);
		genepi_set_del_reference (C->solver, tmp);
	      }
	  }
	  break;

	case ARMOISE_P_INDIRECTION:
	case ARMOISE_P_PAREN:
	  {
	    const armoise_predicate *op = armoise_predicate_get_operand (P, 0);
	    result = s_compute_predicate (C, op, p_err);
	  }
	  break;

	case ARMOISE_P_MUL: 
	case ARMOISE_P_DIV: 
	case ARMOISE_P_MOD: 
	  {
	    const armoise_predicate *op1 = armoise_predicate_get_operand (P, 0);
	    genepi_set *op1S = s_compute_predicate (C, op1, p_err);
	    armoise_domain *d1 = armoise_predicate_get_domain (op1);
	    const armoise_predicate *op2 = armoise_predicate_get_operand (P, 1);
	    genepi_set *op2S = s_compute_predicate (C, op2, p_err);
	    armoise_domain *d2 = armoise_predicate_get_domain (op2);
	    genepi_set * (*compute)(genepi_solver *, armoise_domain *, 
				    genepi_set *, armoise_domain *, 
				    genepi_set *, alambic_error *);

	    if (kind == ARMOISE_P_MUL)
	      compute = s_genepi_set_mul;
	    else if (kind == ARMOISE_P_DIV)
	      compute = s_genepi_set_div;
	    else 
	      compute = s_genepi_set_mod;

	    if (op1S != NULL && op2S != NULL)
	      result = compute (C->solver, d1, op1S, d2, op2S, p_err);

	    if (op1S != NULL)
	      genepi_set_del_reference (C->solver, op1S);
	    if (op2S != NULL)
	      genepi_set_del_reference (C->solver, op2S);
	    armoise_domain_del_reference (d1);
	    armoise_domain_del_reference (d2);
	  }
	  break;

	case ARMOISE_P_SET:      
	  result = s_compute_set (C, P, p_err);
	  break;

	case ARMOISE_P_ENUM:
	  {
	    int i;
	    genepi_set *tmp[2];
	    int nb_elements = armoise_predicate_get_nb_elements (P);
	    const armoise_term *e = armoise_predicate_get_element (P, 0);
	    result = s_compute_set_for_term (C, e, NULL, 0, p_err);

	    for (i = 1; i < nb_elements && result != NULL; i++)
	      {
		e = armoise_predicate_get_element (P, i);
		tmp[0] = s_compute_set_for_term (C, e, NULL, 0, p_err);
		if (tmp[0] != NULL)
		  {
		    tmp[1] = genepi_set_union (C->solver, result, tmp[0]);
		    genepi_set_del_reference (C->solver, result);
		    genepi_set_del_reference (C->solver, tmp[0]);
		    result = tmp[1];
		  }
		else
		  {
		    genepi_set_del_reference (C->solver, result);
		    result = NULL;
		  }
	      }
	  }
	  break;
	default:
	  ccl_throw (internal_error, "invalid predicate error");
	  break;
	}
  
      armoise_type_del_reference (type);
      armoise_domain_del_reference (domain);

      if (result != NULL)
	{
	  ccl_assert (!ccl_hash_find (C->cache, P));
	  ccl_hash_find (C->cache, P);
	  result = genepi_set_add_reference (C->solver, result);
	  ccl_hash_insert (C->cache, result);
	}
      ccl_debug ("[%d] done\n", --k);
      ccl_debug_pop_tab_level ();
    }

  return result;
}

			/* --------------- */

static int
s_get_type_width (const armoise_type *t)
{  
  int result;
  if(armoise_type_is_scalar (t))
    result = 1;
  else
    {
      int i;
      int w = armoise_type_get_width (t);

      for (result = 0, i = 0; i < w; i++)
	{
	  const armoise_type *st = armoise_type_get_subtype (t, i);
	  result += s_get_type_width (st);
	}
    }
  return result;
}

			/* --------------- */

static int
s_get_term_width (const armoise_term *t)
{  
  armoise_type *type = armoise_term_get_type (t);
  int result = s_get_type_width (type);
  armoise_type_del_reference (type);

  return result;
}

			/* --------------- */

static int
s_get_variable_width (armoise_variable *v)
{
  armoise_type *t = armoise_variable_get_type (v);
  int result = s_get_type_width (t);
  armoise_type_del_reference (t);

  return result;
}

			/* --------------- */

static genepi_set *
s_compute_set_for_term (compiler *C, const armoise_term *T, ccl_hash *ordering,
			int width, alambic_error *p_err);

static void
s_collect_linear_term_coefficients (const armoise_term *T, int width, 
				    ccl_hash *ordering, int *nums, int *cst);

static genepi_set *
s_project_set (genepi_solver *solver, genepi_set *P, int last_index, 
	       int offset, int w)
{
  int i;
  int Pw = genepi_set_get_width (solver, P);
  int total_width = last_index + w;
  int max_width = total_width < Pw ? Pw : total_width;
  int *sel = ccl_new_array (int, max_width);
  genepi_set *tmp;
  genepi_set *result;

  for (i = 0; i < Pw; i++)
    sel[i] = (offset <= i && i < offset + w) ? 1 : 0;
  tmp = genepi_set_project (solver, P, sel, Pw);
  for (i = 0; i < total_width; i++)
    sel[i] = (i < last_index) ? 1 : 0;
  result = genepi_set_invproject (solver, tmp, sel, total_width);
  genepi_set_del_reference (solver, tmp);

  return result;
}

			/* --------------- */

static void
s_get_var_range (const armoise_term *T, ccl_hash *ordering, 
		 int *pmin, int *pmax)
{
  if (armoise_term_get_kind (T) == ARMOISE_T_VARIABLE)
    {
      int range;
      armoise_variable *var = armoise_term_get_variable (T);

      ccl_assert (ccl_hash_find (ordering, var));
      ccl_hash_find (ordering, var);
      range = (intptr_t) ccl_hash_get (ordering);
      *pmin = VAR_IMIN (range);
      *pmax = VAR_IMAX (range);
      armoise_variable_del_reference (var);
    }
  else
    {
      int i;
      const armoise_term *v = armoise_term_get_vec_element_variable (T);      
      armoise_type *type = armoise_term_get_type (v);      
      int index = armoise_term_get_vec_element_index (T);
      s_get_var_range (v, ordering, pmin, pmax);

      for (i = 0; i < index; i++)
	{
	  const armoise_type *st = armoise_type_get_subtype (type, i);
	  *pmin += s_get_type_width (st);
	}
      armoise_type_del_reference (type);
      *pmax = *pmin + s_get_term_width (T) - 1;
    }
}


			/* --------------- */

static int
s_get_slot_index (const armoise_term *T, ccl_hash *ordering)
{
  int min, max;

  s_get_var_range (T, ordering, &min, &max);
  ccl_assert (min == max);

  return min;
}

			/* --------------- */


static int *
s_is_vector_of_variables (compiler *C, const armoise_term *t, 
			  ccl_hash *ordering)
{ 
  int i;
  const int nb_operands = armoise_term_get_nb_operands (t);
  int *result = ccl_new_array (int, nb_operands);

  for (i = 0; i < nb_operands; i++)
    {
      const armoise_term *op = armoise_term_get_operand (t, i);
      armoise_term_kind kind = armoise_term_get_kind (op);

      if (kind == ARMOISE_T_ELEMENT || kind == ARMOISE_T_VARIABLE)
	{
	  ccl_assert (s_get_term_width (op) == 1);
	  result[i] = s_get_slot_index (op, ordering);
	  ccl_assert (result[i] >= 0);
	}
      else
	{
	  ccl_delete (result);
	  return NULL;
	}
    }
  return result;
}

			/* --------------- */

static genepi_set *
s_vector_of_variables_in_P (compiler *C, int *vars, int width, genepi_set *P,
			    ccl_hash *ordering, int last_index, 
			    alambic_error *p_err)
{
  int i;
  int total_width;
  genepi_set *tmp[2];
  genepi_set *result = NULL;
  int *sel;

  ccl_assert (genepi_set_get_width (C->solver, P) == width);
  ccl_assert (width <= last_index);

  if (width == last_index)
    {
      for (i = 0; i < width; i++)
	if (vars[i] != i)
	  break;
      if (i == width)
	return genepi_set_add_reference (C->solver, P);
    }
  total_width = last_index + width;
  sel = ccl_new_array (int, total_width);

  for (i = 0; i < 2 * width; i += 2)
    {
      sel[i] = 0;
      sel[i + 1] = 1;
    }

  for (; i < total_width; i ++)
    sel[i] = 1;
  result = genepi_set_invproject (C->solver, P, sel, total_width);

  for (i = 0; i < total_width; i++)
    sel[i] = 0;

  for (i = 0; i < width; i++)
    {
      int j = vars[i];
      int Pi = 2 * i;
      int Ri = j < width ? 2 * j + 1 : width + j;

      sel[Pi] = 1;
      sel[Ri] = -1;
      tmp[0] = genepi_set_linear_equality (C->solver, sel, total_width, 0);
      tmp[1] = genepi_set_intersection (C->solver, result, tmp[0]);
      genepi_set_del_reference (C->solver, tmp[0]);
      genepi_set_del_reference (C->solver, result);
      result = tmp[1];
      sel[Pi] = 0;
      sel[Ri] = 0;
    }

  for (i = 0; i < 2 * width; i += 2)
    sel[i] = 1;
  tmp[0] = genepi_set_project (C->solver, result, sel, total_width);
  genepi_set_del_reference (C->solver, result);
  result = tmp[0];
  ccl_delete (sel);

  ccl_assert (genepi_set_get_width (C->solver, result) == last_index);

  return result;
}

			/* --------------- */

static genepi_set *
s_compute_vector_in_P (compiler *C, const armoise_term *t, genepi_set *P,
		       ccl_hash *ordering, int last_index, alambic_error *p_err)
{
  int i;
  genepi_set *tmp[3];
  genepi_set *result;
  const int Pwidth = genepi_set_get_width (C->solver, P);
  const int pref_len = last_index + Pwidth;
  const int nb_operands = armoise_term_get_nb_operands (t);
  int *sel;
  int tindex = last_index;
  int sel_len = 0;

  if (*p_err != ALAMBIC_NO_ERROR)
    return NULL;

  sel = s_is_vector_of_variables (C, t, ordering);
  if (sel != NULL)
    {
      result = s_vector_of_variables_in_P (C, sel, nb_operands, P, ordering, 
					   last_index, p_err);
      ccl_delete (sel);
      return result;
    }

  for (i = 0; i < nb_operands; i++)
    {
      const armoise_term *op = armoise_term_get_operand (t, i);
      const int op_w = s_get_term_width (op);
      if (sel_len < op_w)
	sel_len = op_w;
    }


  sel_len += pref_len;
  sel = ccl_new_array (int, sel_len);
  tmp[0] = genepi_set_top (C->solver, last_index);
  result = genepi_set_cartesian_product (C->solver, tmp[0], P);
  genepi_set_del_reference (C->solver, tmp[0]);
  
  for (i = 0; i < nb_operands; i++)
    {
      int j;      
      const armoise_term *op = armoise_term_get_operand (t, i);
      const int op_w = s_get_term_width (op);
      const int r_w = pref_len + op_w;

      /* put the value of op at the end of the current result */
      tmp[0] = s_compute_set_for_term (C, op, ordering, pref_len, p_err);
      tmp[1] = genepi_set_top (C->solver, op_w);
      tmp[2] = genepi_set_cartesian_product (C->solver, result, tmp[1]);

      genepi_set_del_reference (C->solver, result);
      genepi_set_del_reference (C->solver, tmp[1]);
      result = genepi_set_intersection (C->solver, tmp[0], tmp[2]);
      genepi_set_del_reference (C->solver, tmp[0]);
      genepi_set_del_reference (C->solver, tmp[2]);

      /* now ensure that the value of op is in P */
      for (j = 0; j < op_w; j++)
	{
	  sel[tindex + j] = 1;
	  sel[pref_len + j] = -1;
	  tmp[0] = genepi_set_linear_equality (C->solver, sel, r_w, 0);
	  tmp[1] = genepi_set_intersection (C->solver, result, tmp[0]);
	  genepi_set_del_reference (C->solver, tmp[0]);
	  genepi_set_del_reference (C->solver, result);
	  result = tmp[1];
	  sel[tindex + j] = 0;
	  sel[pref_len + j] = 0;
	}
      tindex += op_w;

      /* finally, remove op columns */
      for (j = 0; j < op_w; j++)
	sel[pref_len + j] = 1;

      tmp[0] = genepi_set_project (C->solver, result, sel, r_w);
      genepi_set_del_reference (C->solver, result);
      result = tmp[0];
      for (j = 0; j < op_w; j++)
	sel[pref_len + j] = 0;
    }

  for (i = last_index; i < last_index + Pwidth; i++)
    sel[i] = 1;
  
  tmp[0] = genepi_set_project (C->solver, result, sel, pref_len);
  genepi_set_del_reference (C->solver, result);
  result = tmp[0];

  ccl_delete (sel);

  return result;
}

			/* --------------- */

static genepi_set *
s_compute_set_for_formula (compiler *C, const armoise_formula *F, 
			   ccl_hash *ordering, int last_index, 
			   alambic_error *p_err)
{
  genepi_set *result = NULL;
  armoise_formula_kind kind = armoise_formula_get_kind (F);

  if (*p_err != ALAMBIC_NO_ERROR)
    return NULL;

  switch (kind)   
    {
    case ARMOISE_F_CONSTANT:
      if (armoise_formula_get_value (F))
	result = genepi_set_top (C->solver, last_index);
      else
	result = genepi_set_bot (C->solver, last_index);
      break;

    case ARMOISE_F_AND:
    case ARMOISE_F_OR:
    case ARMOISE_F_EQUIV:
    case ARMOISE_F_IMPLY:
    case ARMOISE_F_NOT:
      {
	int nb_operands = armoise_formula_get_nb_operands (F);
	const armoise_formula *op = armoise_formula_get_operand (F, 0);
	result = s_compute_set_for_formula (C, op, ordering, last_index, p_err);

	if (kind == ARMOISE_F_NOT)
	  {
	    genepi_set *tmp = genepi_set_complement (C->solver, result);
	    genepi_set_del_reference (C->solver, result);
	    result = tmp;
	  }
	else
	  {
	    int i;
	    genepi_set *(*operator)(genepi_solver *, genepi_set*, 
				    genepi_set *);
	    
	    if (kind == ARMOISE_F_AND)
	      operator = genepi_set_intersection;
	    else if (kind == ARMOISE_F_OR)
	      operator = genepi_set_union;
	    else if (kind == ARMOISE_F_EQUIV)
	      operator = genepi_set_equiv;
	    else 
	      operator = genepi_set_imply;

	    for (i = 1; i < nb_operands && result != NULL; i++)
	      {
		genepi_set *tmp;
		op = armoise_formula_get_operand (F, i);
		tmp = s_compute_set_for_formula (C, op, ordering, last_index,
						 p_err);
		if (tmp == NULL)
		  {
		    genepi_set_del_reference (C->solver, result);
		    result = NULL;
		  }
		else 
		  {
		    genepi_set *tmp2 = operator (C->solver, result, tmp);
		    genepi_set_del_reference (C->solver, result);
		    genepi_set_del_reference (C->solver, tmp);
		    result = tmp2;
		  }
	      }
	  }
      }
      break;

    case ARMOISE_F_EXISTS: 
    case ARMOISE_F_FORALL:
      {
	int i;
	int start_index = last_index;
	ccl_pair *p;
	ccl_list *qvars_list = ccl_list_create ();
	armoise_term *qvars = 
	  armoise_formula_get_quantified_variables (F, NULL);
	const armoise_formula *qF = armoise_formula_get_quantified_formula (F);
	genepi_set *qFS = NULL;

	armoise_term_get_variables (qvars, qvars_list);
	for (p = FIRST (qvars_list); p; p = CDR (p))
	  {
	    armoise_variable *var = CAR (p);
	    int min = last_index;
	    int max = min + s_get_variable_width (var) - 1;

	    ccl_hash_find (ordering, var);
	    ccl_hash_insert (ordering, (void *)
			     (intptr_t) VAR_IRANGE (min, max));
	    last_index = max + 1;
	  }
	
	qFS = s_compute_set_for_formula (C, qF, ordering, last_index, p_err);	
	if (qFS != NULL)
	  {	    
	    int w = last_index;
	    int *sel = ccl_new_array (int,  w);

	    for (i = start_index; i < last_index; i++)
	      sel[i] = 1;

	    if (kind == ARMOISE_F_FORALL)
	      {
		genepi_set *tmp = genepi_set_complement (C->solver, qFS);
		genepi_set_del_reference (C->solver, qFS);
		qFS = tmp;
	      }

	    result = genepi_set_project (C->solver, qFS, sel, w);
	    genepi_set_del_reference (C->solver, qFS);
	    if (kind == ARMOISE_F_FORALL)
	      {
		qFS = genepi_set_complement (C->solver, result);
		genepi_set_del_reference (C->solver, result);
		result = qFS;
	      }
	    ccl_delete (sel);
	  }

	while (!ccl_list_is_empty (qvars_list))
	  {
	    armoise_variable *var = ccl_list_take_first (qvars_list);
	    ccl_hash_find (ordering, var);
	    ccl_hash_remove (ordering);
	  }
	ccl_list_delete (qvars_list);
      }
      break;

    case ARMOISE_F_PAREN:
      {
	const armoise_formula *op = armoise_formula_get_operand (F, 0);
	result = s_compute_set_for_formula (C, op, ordering, last_index, p_err);
      }
      break;

    case ARMOISE_F_IN:
      {
	armoise_predicate *P = armoise_formula_get_in_predicate (F);	
	armoise_term *t = armoise_formula_get_in_term (F);
	genepi_set *Pset = s_compute_predicate (C, P, p_err);

	if (armoise_term_get_kind (t) == ARMOISE_T_VECTOR)
	  result = 
	    s_compute_vector_in_P (C, t, Pset, ordering, last_index, p_err);
	else 
	  {
	    genepi_set *tset = 
	      s_compute_set_for_term (C, t, ordering, last_index, p_err);
	    if (*p_err == ALAMBIC_NO_ERROR)		  
	      {
		int i;
		int max = genepi_set_get_width (C->solver, Pset);
		int *sel = ccl_new_array (int, last_index + max);
		genepi_set *tmp1 = genepi_set_top(C->solver, last_index);
		genepi_set *tmp2 = 
		  genepi_set_cartesian_product(C->solver, tmp1, Pset);
		genepi_set_del_reference (C->solver, tmp1);
		tmp1 = genepi_set_intersection (C->solver, tset, tmp2);
		genepi_set_del_reference (C->solver, tmp2);
		
		for (i = 0; i < max; i++)
		  sel[last_index + i] = 1;
		result = genepi_set_project (C->solver, tmp1, sel, 
					     last_index + max);
		genepi_set_del_reference (C->solver, tmp1);
		ccl_delete (sel);
		genepi_set_del_reference (C->solver, tset);
	      }
	  }

	if (Pset != NULL)
	  genepi_set_del_reference (C->solver, Pset);
	armoise_predicate_del_reference (P);
	armoise_term_del_reference (t);
      }
      break;

    case ARMOISE_F_EQ:      
    case ARMOISE_F_NEQ:
    case ARMOISE_F_LEQ: 
    case ARMOISE_F_GEQ:
    case ARMOISE_F_LT:
    case ARMOISE_F_GT:
      {
	genepi_comparator cmp;
	int cst;
	int *nums = ccl_new_array (int, last_index);
	armoise_term *l = armoise_formula_get_cmp_left_term (F);
	s_collect_linear_term_coefficients (l, last_index, ordering, nums, 
					    &cst);

	if (kind == ARMOISE_F_EQ || kind == ARMOISE_F_NEQ) cmp = GENEPI_EQ;
	else if (kind == ARMOISE_F_LEQ) cmp = GENEPI_LEQ;
	else if (kind == ARMOISE_F_GEQ) cmp = GENEPI_GEQ;
	else if (kind == ARMOISE_F_LT) cmp = GENEPI_LT;
	else cmp = GENEPI_GT;

	result = 
	  genepi_set_linear_operation (C->solver, nums, last_index, cmp, -cst);

	if (kind == ARMOISE_F_NEQ)
	  {
	    genepi_set *tmp = genepi_set_complement (C->solver, result);
	    genepi_set_del_reference (C->solver, result);
	    result = tmp;
	  }
	ccl_delete (nums);
	armoise_term_del_reference (l);
      }
      break;
    }

  return result;
}

			/* --------------- */

static genepi_set *
s_concat_keep_prefix (genepi_solver *solver, const int width, genepi_set *S1, 
		      genepi_set *S2, int *sel)
{
  int i;
  genepi_set *tmp;
  genepi_set *result;
  const int w1 = genepi_set_get_width (solver, S1);
  const int w2 = genepi_set_get_width (solver, S2);

  ccl_assert (w2 > w1);
  /* add w2 columns at the end of S1 */
  for (i = 0; i < w1; i++)
    sel[i] = 0;
  for (; i < w2; i++)
    sel[i] = 1;
  tmp = genepi_set_invproject (solver, S1, sel, w2);
  result = genepi_set_intersection (solver, tmp, S2);
  genepi_set_del_reference (solver, tmp);
  
  return result;
}

			/* --------------- */

static genepi_set *
s_compute_set_for_term (compiler *C, const armoise_term *T, ccl_hash *ordering,
			int width, alambic_error *p_err)
{
  genepi_set *result = NULL;
  armoise_term_kind kind = armoise_term_get_kind (T);
  
  if (*p_err != ALAMBIC_NO_ERROR)
    return NULL;

  if (kind == ARMOISE_T_CONSTANT)
      {
	int num, den;
	int *sel = ccl_new_array (int, width + 1);
	armoise_term_get_value (T, &num, &den);
	sel[width] = den;
	result = genepi_set_linear_equality (C->solver, sel, width + 1, num);
	ccl_delete (sel);
      }
  else if (kind == ARMOISE_T_ELEMENT || kind == ARMOISE_T_VARIABLE)
    {
      int *sel = ccl_new_array (int, width + 1);
      int index = s_get_slot_index (T, ordering);

      sel[index] = 1;
      sel[width] = -1;
      result = genepi_set_linear_equality (C->solver, sel, width + 1, 0);
      ccl_delete (sel);
    }
  else if (kind == ARMOISE_T_MUL)
    {
      /* the term is normalized so it is a factor a*x */
      int num, den, *sel = ccl_new_array (int, width + 1);
      const armoise_term *cst = armoise_term_get_operand (T, 0);
      const armoise_term *var = armoise_term_get_operand (T, 1);
      int index = s_get_slot_index (var, ordering);
      armoise_term_get_value (cst, &num, &den);

      sel[index] = den;
      sel[width] = num;

      result = genepi_set_linear_equality (C->solver, sel, width + 1, 0);
      ccl_delete (sel);
    }
  else if (kind == ARMOISE_T_DIV || kind == ARMOISE_T_MOD)
    {
      ccl_throw_no_msg (internal_error);
    }
  else
    {
      int i;
      const int nb_operands = armoise_term_get_nb_operands (T);
      genepi_set **operands = ccl_new_array (genepi_set *, nb_operands);
      const int Twidth = s_get_term_width (T);
      int total_width = width + Twidth;
      
      if (kind == ARMOISE_T_VECTOR)
	{
	  int index = width;
	  for (i = 0; i < nb_operands; i++)
	    {
	      const armoise_term *op = armoise_term_get_operand (T, i);
	      operands[i] = s_compute_set_for_term (C, op, ordering, index, 
						    p_err);
	      index += s_get_term_width (op);
	    }
	}
      else
	{
	  for (i = 0; i < nb_operands; i++)
	    {
	      const armoise_term *op = armoise_term_get_operand (T, i);
	      operands[i] = s_compute_set_for_term (C, op, ordering, width, 
						    p_err);
	      total_width += s_get_term_width (op);
	    }
	}

      if (*p_err == ALAMBIC_NO_ERROR)
	{
	  genepi_set *tmp[2];
	  int *selection = ccl_new_array (int, total_width);
	  int *sel = selection + width;

	  if (kind == ARMOISE_T_ADD || kind == ARMOISE_T_SUB ||
	      kind == ARMOISE_T_NEG)
	    {		
	      int j;

	      ccl_assert (Twidth == 1);

	      sel[0] = 1;

	      for (j = 0; j < nb_operands; j++)
		{
		  int c;
		  
		  if (kind == ARMOISE_T_ADD) c = -1;
		  else if (kind == ARMOISE_T_NEG) c = +1;
		  else if (j == 0) c = -1;
		  else c = +1;
		  
		  sel[j + 1] = c;
		}

	      result = genepi_set_linear_equality (C->solver, selection, 
						   total_width, 0);
		
	      for (i = 0; i < nb_operands + 1; i++)
		sel[i] = 1;
		
	      for (i = 0; i < nb_operands; i++)
		{
		  sel[i + 1] = 0;
		  tmp[0] = genepi_set_invproject (C->solver, operands[i], 
						  selection, total_width);
		  genepi_set_del_reference (C->solver, operands[i]);
		  operands[i] = NULL;
		  tmp[1] = 
		    genepi_set_intersection (C->solver, tmp[0], result);
		  genepi_set_del_reference (C->solver, tmp[0]);
		  genepi_set_del_reference (C->solver, result);
		  result = tmp[1];
		  sel[i + 1] = 1;
		}		

	      sel[0] = 0;
	      tmp[0] = genepi_set_project (C->solver, result, selection, 
					   total_width);
	      genepi_set_del_reference (C->solver, result);
	      result = tmp[0];
	    }
	  else 
	    {
	      ccl_assert (kind == ARMOISE_T_VECTOR);

	      result = operands[0];
	      operands[0] = NULL;
	      for (i = 1; i < nb_operands; i++)
		{
		  tmp[0] = s_concat_keep_prefix (C->solver, width, result, 
						 operands[i], selection);
		  genepi_set_del_reference (C->solver, result);
		  genepi_set_del_reference (C->solver, operands[i]);
		  operands[i] = NULL;
		  result = tmp[0];
		}
	      
	      
	      /* finally we project on actual variables */
	      /*
	      for (i = 0; i < Twidth; i++)
		sel[i] = 0;
	      for (; i < 2 * Twidth; i++)
		sel[i] = 1;
	      tmp[0] = genepi_set_project (C->solver, result, selection, 
					   total_width);
	      genepi_set_del_reference (C->solver, result);
	      result = tmp[0];
	      */
	    }
	  ccl_delete (selection);
	}

	
      for (i = 0; i < nb_operands; i++)
	if (operands[i] != NULL)
	  genepi_set_del_reference (C->solver, operands[i]);
      ccl_delete (operands);
    }

  return result;
}

			/* --------------- */

static int
s_build_index_ranges_for_variables (int offset, const armoise_term *proto, 
				    ccl_hash *indices)
{
  int last_index;

  if (armoise_term_get_kind (proto) == ARMOISE_T_VARIABLE)
    {
      int range[2];
      armoise_variable *var = armoise_term_get_variable (proto);
      ccl_assert (! ccl_hash_find (indices, var));
      ccl_hash_find (indices, var);
      range[0] = offset;
      range[1] = offset + s_get_term_width (proto) - 1;

      ccl_hash_insert (indices, (void *)
		       (intptr_t) VAR_IRANGE (range[0], range[1]));
      last_index = range[1] + 1;
      armoise_variable_del_reference (var);
    }
  else
    {
      int i;

      const int nb_operands = armoise_term_get_nb_operands (proto);

      ccl_assert (armoise_term_get_kind (proto) == ARMOISE_T_VECTOR);

      for (i = 0; i < nb_operands; i++)
	{
	  const armoise_term *op = armoise_term_get_operand (proto, i);
	  offset = s_build_index_ranges_for_variables (offset, op, indices);
	}
      last_index = offset;
    }

  return last_index;
}

			/* --------------- */

static genepi_set *
s_compute_set (compiler *C, const armoise_predicate *P, alambic_error *p_err)
{
  genepi_set *result;
  armoise_predicate *dom;
  ccl_hash *indices = ccl_hash_create (NULL, NULL, NULL, NULL);
  const armoise_term *proto = armoise_predicate_get_set_proto (P, &dom);
  const armoise_formula *F = armoise_predicate_get_set_formula (P);
  int maxindex = s_build_index_ranges_for_variables (0, proto, indices);
  result = s_compute_set_for_formula (C, F, indices, maxindex, p_err);
  armoise_predicate_del_reference (dom);

  ccl_assert (ccl_imply (result != NULL, 
			 genepi_set_get_width (C->solver, result) == maxindex));
  ccl_hash_delete (indices);

  return result;
}


			/* --------------- */

static void
s_collect_linear_term_coefficients_rec (int sign, const armoise_term *T, 
					ccl_hash *ordering,
					int *nums, int *dens, int *cst, 
					int *cst_den)
{
  armoise_term_kind kind = armoise_term_get_kind (T);

  switch (kind)    
    {
    case ARMOISE_T_CONSTANT:
      {
	int n, d;
	armoise_term_get_value (T, &n, &d);
	n = sign * n;
	*cst = *cst * d + n * *cst_den;
	*cst_den = *cst_den * d;
      }
      break;

    case ARMOISE_T_VARIABLE:
    case ARMOISE_T_ELEMENT:
      {
	int index = s_get_slot_index (T, ordering);
	nums[index] = nums[index] + sign * dens[index];
      }
      break;

    case ARMOISE_T_ADD:
    case ARMOISE_T_SUB:
      {
	int i;
	int nb_operands = armoise_term_get_nb_operands (T);
	for (i = 0; i < nb_operands; i++)
	  {
	    int lsign = sign;
	    const armoise_term *op = armoise_term_get_operand (T, i);

	    if (kind == ARMOISE_T_SUB && i > 0)
	      lsign = -sign;
	    s_collect_linear_term_coefficients_rec (lsign, op, ordering, nums,
						    dens, cst, cst_den);
	  }	
      }
      break;

    case ARMOISE_T_MUL:
      {	
	const armoise_term *coef = armoise_term_get_operand (T, 0);
	const armoise_term *var = armoise_term_get_operand (T, 1);
	int index = s_get_slot_index (var, ordering);
	int num, den;

	ccl_assert (2 == armoise_term_get_nb_operands (T));
	armoise_term_get_value (coef, &num, &den);
	num *= sign;
	nums[index] = nums[index] * den + num * dens[index];
	dens[index] = den * dens[index];
      }
      break;

    case ARMOISE_T_NEG:
      {
	const armoise_term *var = armoise_term_get_operand (T, 0);
	int index = s_get_slot_index (var, ordering);
	nums[index] = nums[index] - sign * dens[index];
      }
      break;

    case ARMOISE_T_DIV:
    case ARMOISE_T_MOD:
      ccl_throw_no_msg (internal_error);

    case ARMOISE_T_VECTOR:
      ccl_throw_no_msg (internal_error);

    default:
      ccl_throw_no_msg (internal_error);
    }
}

			/* --------------- */

static int
s_gcd (int a, int b)
{
  if (a < 0) 
    a = -a;

  while (b != 0)
    {
      if (a > b)
	a = a -b;
      else
	b = b - a;
    }

  return a;
}

			/* --------------- */

static int
s_lcm (int a, int b)
{
  return a * b / s_gcd (a, b);
}

			/* --------------- */

static void
s_collect_linear_term_coefficients (const armoise_term *T, int width, 
				    ccl_hash *ordering, int *nums, int *cst)
{
  int i;
  int cst_den = 1;
  int *dens = ccl_new_array (int, width);
  int lcm;

  for (i = 0; i < width; i++)
    {
      nums[i] = 0;
      dens[i] = 1;
    }
  *cst = 0;
  s_collect_linear_term_coefficients_rec (1, T, ordering, nums, dens, cst,
					  &cst_den);

  lcm = s_lcm (cst_den, dens[0]);
  for (i = 1; i < width; i++)
    lcm = s_lcm (lcm, dens[i]);

  for (i = 0; i < width; i++)
    nums[i] = nums[i] * (lcm / dens[i]);
  *cst = *cst * (lcm / cst_den);

  ccl_delete (dens);
}

			/* --------------- */

static int
s_domain_is_supported (genepi_solver_type stype, const armoise_predicate *P, 
		       alambic_error *p_error)
{
  int result;
  armoise_domain_attr attr = armoise_predicate_get_largest_domain (P);


  if (stype == GENEPI_N_SOLVER)
    {
      result = (attr == ARMOISE_DOMAIN_ATTR_FOR_NATURALS);
      if (!result)
	{
	  if (!(attr & ARMOISE_DOMAIN_IN_INTEGERS))
	    *p_error = ALAMBIC_REAL_NOT_SUPPORTED;
	  else
	    *p_error = ALAMBIC_INT_NOT_SUPPORTED;
	}
    }
  else if (stype == GENEPI_Z_SOLVER)
    {
      result = (attr & ARMOISE_DOMAIN_IN_INTEGERS);
      if (!result)
	*p_error = ALAMBIC_REAL_NOT_SUPPORTED;
    }
  else if (stype == GENEPI_P_SOLVER)
    {
      result = (attr & ARMOISE_DOMAIN_HAS_NEGATIVES) == 0;
      if (!result)
	*p_error = ALAMBIC_REAL_NOT_SUPPORTED;
    }
  else
    {
      /* stype == GENEPI_R_SOLVER */
      result = 1;
    }

  return result;
}

			/* --------------- */

static genepi_set *
s_set_add_cst (genepi_solver *solver, genepi_set *X, genepi_set *cst)
{
  int cden;
  int w = genepi_set_get_width (solver, X);
  int *c = calloc (sizeof (int), w);
  genepi_set *tmp[2];  
  genepi_set *result = genepi_set_bot (solver, w);

  cst = genepi_set_add_reference (solver, cst);
  while (! genepi_set_is_empty (solver, cst))
    {
      cden = s_remove_one_element (solver, &cst, c, w);
      tmp[0] = genepi_set_add_constant (solver, 1, X, c, cden);
      tmp[1] = genepi_set_union (solver, result, tmp[0]);
      genepi_set_del_reference (solver, tmp[0]);
      genepi_set_del_reference (solver, result);
      result = tmp[1];
    }
  genepi_set_del_reference (solver, cst);
  free (c);

  return result;
}

			/* --------------- */

