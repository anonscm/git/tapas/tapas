/*
 * batof-bfsfp.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include "batof-sfp.h"
#include "prestaf-batof-p.h"

#define SRC 0
#define TGT 1

			/* --------------- */

typedef struct backtracking_stack_st backtracking_stack;
struct backtracking_stack_st
{
  backtracking_stack *next;
  void **addr;
  void *value;
};

			/* --------------- */

#define DLINK(_type) struct { _type *prev; _type *next; }
#define PREV(_obj, _l) ((_obj)->links[(_l)].prev)
#define NEXT(_obj, _l) ((_obj)->links[(_l)].next)
#define FIRST_ELEMENT(_sentinel, _l) NEXT (_sentinel, _l)
#define LAST_ELEMENT(_sentinel, _l) PREV (_sentinel, _l)

#define MAKE_EMPTY(_sentinel, _l) \
  do { NEXT (_sentinel, _l) = PREV (_sentinel, _l) = (_sentinel); } while (0)

#define INIT_DLISTS(_sentinel) \
  do { MAKE_EMPTY (_sentinel, SRC); MAKE_EMPTY (_sentinel, TGT); } while (0)

#define IS_EMPTY_LIST(_sentinel, _l) (NEXT (_sentinel, _l) == (_sentinel))

#define INSERT_IN_LIST(_sentinel, _obj, _l)		\
  do							\
    {							\
      NEXT (_obj, _l) = (_sentinel);			\
      PREV (_obj, _l) = PREV (_sentinel, _l);		\
      PREV (_sentinel, _l) = (_obj);			\
      NEXT (PREV (_obj, _l), _l) = (_obj);		\
    }							\
  while (0)

#define REMOVE_FROM_LIST(_obj, _l)			\
  do							\
    {							\
      PREV (NEXT (_obj, _l), _l) = PREV (_obj, _l);	\
      NEXT (PREV (_obj, _l), _l) = NEXT (_obj, _l);	\
      PREV (_obj, _l) = NULL;				\
      NEXT (_obj, _l) = NULL;				\
    }							\
  while (0)

typedef struct vertex_st vertex; 
typedef struct edge_st edge;

			/* --------------- */

struct edge_st 
{  
  int label; 
  vertex *src;
  vertex *tgt;
  DLINK(edge) links[2];
};

			/* --------------- */

struct vertex_st 
{
  int card; 
  int rank;
  vertex *parent;
  DLINK(vertex) links[2];
  edge edgelists;
};

			/* --------------- */

typedef struct 
{
  int q0;
  sataf_ea *ea;
  int nb_states;
  vertex unfolded;
  vertex *vertices;
  edge *edges;
  backtracking_stack *bs;
  int *state_to_class;
  int **class_to_states;
} bfsfp_data;

			/* --------------- */

#define REMEMBER(_data, _addr) s_remember (&((_data)->bs), (void **) (_addr))

static void
s_remember (backtracking_stack **stack, void **addr);

static void
s_remember_value (backtracking_stack **stack, void **addr, void *value);

static backtracking_stack *
s_insert_choice_point (backtracking_stack **stack);

static void
s_restore_values (backtracking_stack **stack, backtracking_stack *choice_point);

static void
s_init_computation_data (bfsfp_data *data);

static void
s_clean_computation_data (bfsfp_data *data);

static int
s_get_same_edges (edge *sentinel, int incidence, edge **e);

static void
s_check_folded_status (bfsfp_data *data, vertex *u, int incidence);

static int
s_fold_edges (bfsfp_data *data, edge **e);

			/* --------------- */

int 
btf_compute_half_spaces_automata (int q0, sataf_ea *ea, 
				  sataf_ea ***p_hspa, int *p_nb_hspa)
{
  bfsfp_data data;
  void *top_cp;

  CCL_DEBUG_START_TIMED_BLOCK (("compute 1/2 space automata"));

  *p_hspa = NULL;
  *p_nb_hspa = 0;
  data.q0 = q0;
  data.ea = ea;
  data.nb_states = sataf_ea_get_nb_states (ea);
  data.bs = NULL;

  s_init_computation_data (&data);

  top_cp = s_insert_choice_point (&data.bs);  
  while (! IS_EMPTY_LIST (&data.unfolded, TGT))
    {
      edge *ex[2] = { NULL, NULL };
      vertex *x = FIRST_ELEMENT (&data.unfolded, TGT);

      s_insert_choice_point (&data.bs);
      s_get_same_edges (&x->edgelists, TGT, ex);
      s_fold_edges (&data, ex);

      while (! IS_EMPTY_LIST (&data.unfolded, SRC))
	{
	  edge *ey[2] = { NULL, NULL };
	  vertex *y = FIRST_ELEMENT (&data.unfolded, SRC);

	  s_get_same_edges (&y->edgelists, SRC, ey);
	  s_fold_edges (&data, ey);
	}

      if (IS_EMPTY_LIST (&data.unfolded, TGT))
	{
	  /* mod automaton -> go back one step 
	  s_restore_last_choice_point (&data.bs);
	  ccl_assert (! IS_EMPTY_LIST (&data.unfolded, TGT));
	  */
	}
    }

  s_clean_computation_data (&data);

  CCL_DEBUG_END_TIMED_BLOCK ();
  
  return 1;
}

			/* --------------- */

static void
s_remember (backtracking_stack **stack, void **addr)
{
  s_remember_value (stack, addr, *addr);
}

			/* --------------- */

static void
s_remember_value (backtracking_stack **stack, void **addr, void *value)
{
  backtracking_stack *newcell = ccl_new (backtracking_stack);

  newcell->next = *stack;
  newcell->addr = addr;
  newcell->value = value;

  *stack = newcell;
}

			/* --------------- */

static backtracking_stack *
s_insert_choice_point (backtracking_stack **stack)
{
  s_remember_value (stack, NULL, NULL);

  return *stack;
}

			/* --------------- */
#if 0
static void
s_restore_last_choice_point (backtracking_stack **stack)
{
  int stop = 0;

  do
    {
      backtracking_stack *cell = *stack;
      
      *stack = cell->next;

      ccl_assert (cell != NULL);

      if (cell->addr != NULL)
	*cell->addr = cell->value;
      else
	stop = 1;
      ccl_delete (cell);
    }
  while (! stop);
}
#endif
			/* --------------- */

static void
s_restore_values (backtracking_stack **stack, backtracking_stack *choice_point)
{
  backtracking_stack *s = *stack;

  while (s != choice_point)
    {
      backtracking_stack *cell = s;
      s = cell->next;
      if (cell->addr != NULL) /* skip forgotten choice-points */
	*cell->addr = cell->value;
      ccl_delete (cell);
    }
}

			/* --------------- */

static void
s_init_computation_data (bfsfp_data *data)
{
  int i;
  sataf_ea *modea = 
    btf_ea_compute_mod_classes (data->ea, &data->state_to_class,
				&data->class_to_states);

  data->unfolded.links[SRC].next = data->unfolded.links[SRC].prev = 
    &data->unfolded;
  data->unfolded.links[TGT].next = data->unfolded.links[TGT].prev = 
    &data->unfolded;

  data->vertices = ccl_new_array (vertex, data->nb_states);
  data->edges = ccl_new_array (edge, 2 * data->nb_states);

  for (i = 0; i < data->nb_states; i++)
    {
      vertex *v = data->vertices + i;

      v->rank = 0;
      v->card = 1;
      v->parent = v;
      INIT_DLISTS (&v->edgelists);
    }

  for (i = 0; i < data->nb_states; i++)
    {
      int b;
      vertex *src = &data->vertices[i];

      for (b = 0; b < 2; b++)
	{	  
	  int succ = sataf_ea_get_successor (data->ea, i, b);

	  if (sataf_ea_is_local_state (succ))
	    {
	      vertex *tgt = &data->vertices[sataf_ea_decode_succ_state (succ)];
	      edge *e = &data->edges[2 * i + b];
	      e->label = b;
	      e->src = src;
	      e->tgt = tgt;

	      INSERT_IN_LIST (&src->edgelists, e, SRC);
	      INSERT_IN_LIST (&tgt->edgelists, e, TGT);
	    }
	}
    }
      
  for (i = 0; i < data->nb_states; i++)
    {
      s_check_folded_status (data, data->vertices + i, SRC);
      s_check_folded_status (data, data->vertices + i, TGT);
    }
  sataf_ea_del_reference (modea);
}

			/* --------------- */

static void
s_clean_computation_data (bfsfp_data *data)
{
  s_restore_values (&data->bs, NULL);
  ccl_delete (data->state_to_class);
  ccl_delete (data->class_to_states);
  ccl_delete (data->vertices);
  ccl_delete (data->edges);
}

			/* --------------- */

static int
s_get_same_edges (edge *sentinel, int incidence, edge **e)
{
  edge *aux;

  e[0] = NEXT (sentinel, incidence);
  if (e[0] == sentinel) /* empty list */
    return 0;
  e[1] = NEXT (e[0], incidence);
  if (e[1] == sentinel) /* singleton list */
    return 0;

  if (e[0]->label == e[1]->label)
    return 1;

  aux = NEXT (e[1], incidence);
  if (aux == sentinel) /* couple case with different labels*/
    return 0;

  /* here there exists more than 3 edges and thus there exists 2 edges with the 
   * same label */
  if (e[0]->label == aux->label)
    e[1] = aux;
  else 
    e[0] = aux;

  ccl_post (e[0]->label == e[1]->label);

  return 1;

}

			/* --------------- */

static int
s_is_unfolded (vertex *v, int incidence)
{
  edge *e[2];

  return s_get_same_edges (&v->edgelists, incidence, e);
}

			/* --------------- */

static void
s_check_folded_status (bfsfp_data *data, vertex *u, int incidence)
{  
  if (s_is_unfolded (u, incidence))
    {
      if (PREV (u, incidence) == NULL && NEXT (u, incidence) == NULL)
	{
	  REMEMBER (data, &NEXT (u, incidence));
	  NEXT (u, incidence) = (&data->unfolded);

	  REMEMBER (data, &PREV (u, incidence));
	  PREV (u, incidence) = PREV (&data->unfolded, incidence);
	  
	  REMEMBER (data, &PREV (&data->unfolded, incidence));
	  PREV (&data->unfolded, incidence) = (u);

	  REMEMBER (data, &NEXT (PREV (u, incidence), incidence));
	  NEXT (PREV (u, incidence), incidence) = (u);
	}
    }
  else if (PREV (u, incidence) != NULL && NEXT (u, incidence) != NULL)
    {
      REMEMBER (data, &PREV (NEXT (u, incidence), incidence));
      PREV (NEXT (u, incidence), incidence) = PREV (u, incidence);

      REMEMBER (data, &NEXT (PREV (u, incidence), incidence));
      NEXT (PREV (u, incidence), incidence) = NEXT (u, incidence);

      REMEMBER (data, &PREV (u, incidence));
      PREV (u, incidence) = NULL;

      REMEMBER (data, &NEXT (u, incidence));
      NEXT (u, incidence) = NULL;
    }
}

			/* --------------- */

static vertex *
s_get_representative (bfsfp_data *data, vertex *v)
{
  if (v->parent != v)
    {
      REMEMBER (data, &v->parent);
      v->parent = s_get_representative (data, v->parent);
    }

  return v->parent;
}

			/* --------------- */

static void
s_concat_edge_lists_for_incidence (bfsfp_data *data, vertex *u, vertex *v, 
				   int incidence)
{
  if (IS_EMPTY_LIST (&u->edgelists, incidence))
    {
      if (! IS_EMPTY_LIST (&v->edgelists, incidence))
	{
	  REMEMBER (data, &PREV (&u->edgelists, incidence));	 
	  PREV (&u->edgelists, incidence) = PREV (&v->edgelists, incidence);

	  REMEMBER (data, &NEXT (&u->edgelists, incidence));
	  NEXT (&u->edgelists, incidence) = NEXT (&v->edgelists, incidence);

	  REMEMBER (data, &NEXT (PREV (&u->edgelists, incidence), 
					incidence));
	  NEXT (PREV (&u->edgelists, incidence), incidence) = &u->edgelists;

	  REMEMBER (data, &PREV (NEXT (&u->edgelists, incidence), 
				 incidence));
	  PREV (NEXT (&u->edgelists, incidence), incidence) = &u->edgelists;

	  REMEMBER (data, &NEXT (&v->edgelists, incidence));
	  REMEMBER (data, &PREV (&v->edgelists, incidence));
	  MAKE_EMPTY (&v->edgelists, incidence);
	}
    }
  else if (! IS_EMPTY_LIST (&v->edgelists, incidence))
    {
      edge *first_v = FIRST_ELEMENT (&v->edgelists, incidence);
      edge *last_v = LAST_ELEMENT (&v->edgelists, incidence);
      edge *last_u = LAST_ELEMENT (&u->edgelists, incidence);

      REMEMBER (data, &NEXT (last_u, incidence));
      NEXT (last_u, incidence) = first_v;

      REMEMBER (data, &PREV (first_v, incidence));
      PREV (first_v, incidence) = last_u;

      REMEMBER (data, &NEXT (last_v, incidence));
      NEXT (last_v, incidence) = &u->edgelists;

      REMEMBER (data, &PREV (&u->edgelists, incidence));
      PREV (&u->edgelists, incidence) = last_v;

      REMEMBER (data, &NEXT (&v->edgelists, incidence));
      REMEMBER (data, &PREV (&v->edgelists, incidence));
      MAKE_EMPTY (&v->edgelists, incidence);
    }
}

			/* --------------- */
static void
s_concat_edge_lists (bfsfp_data *data, vertex *u, vertex *v)
{
  s_concat_edge_lists_for_incidence (data, u, v, SRC);
  s_concat_edge_lists_for_incidence (data, u, v, TGT);
}

			/* --------------- */

static vertex *
s_merge_classes (bfsfp_data *data, vertex *r1, vertex *r2)
{
  vertex *r = r1;
  vertex *nr = r2;

  ccl_assert (r1->parent == r1);
  ccl_assert (r2->parent == r2);

  ccl_assert (r1 != r2);

  REMEMBER (data, &r1->card);
  REMEMBER (data, &r2->card);

  r1->card += r2->card;
  r2->card = r1->card;

  if (r1->rank > r2->rank) 
    { 
      REMEMBER (data, &r2->parent);
      r2->parent = r1; 
    }

  if (r2->rank > r1->rank) 
    { 
      REMEMBER (data, &r1->parent);
      r1->parent = r2; 
      r = r2; 
      nr = r1; 
    }
  else 
    {
      REMEMBER (data, &r2->parent);
      REMEMBER (data, &r1->rank);
      r2->parent = r1;
      r1->rank++;
    }

  s_concat_edge_lists (data, r, nr);
  if (NEXT (nr, SRC) != NULL)
    {
      REMEMBER (data, &(PREV (nr, SRC)));
      REMEMBER (data, &(NEXT (nr, SRC)));     
      REMEMBER (data, &(NEXT (PREV (nr, SRC), SRC)));
      REMEMBER (data, &(PREV (NEXT (nr, SRC), SRC)));
      REMOVE_FROM_LIST (nr, SRC);
    }

  if (NEXT (nr, TGT) != NULL)
    {
      REMEMBER (data, &(PREV (nr, TGT)));
      REMEMBER (data, &(NEXT (nr, TGT)));     
      REMEMBER (data, &(NEXT (PREV (nr, TGT), TGT)));
      REMEMBER (data, &(PREV (NEXT (nr, TGT), TGT)));
      REMOVE_FROM_LIST (nr, TGT);
    }

  return r;
}

			/* --------------- */

static void 
s_remove_edge_from_lists (bfsfp_data *data, edge *e)
{
  REMEMBER (data, &(PREV (e, SRC)));
  REMEMBER (data, &(NEXT (e, SRC)));
  REMEMBER (data, &(NEXT (PREV (e, SRC), SRC)));
  REMEMBER (data, &(PREV (NEXT (e, SRC), SRC)));
  REMOVE_FROM_LIST (e, SRC);

  REMEMBER (data, &(PREV (e, TGT)));
  REMEMBER (data, &(NEXT (e, TGT)));
  REMEMBER (data, &(NEXT (PREV (e, TGT), TGT)));
  REMEMBER (data, &(PREV (NEXT (e, TGT), TGT)));
  REMOVE_FROM_LIST (e, TGT);
}

			/* --------------- */

static void
s_case_two_self_loops (bfsfp_data *data, edge **e)
{
  s_remove_edge_from_lists (data, e[1]);
  s_check_folded_status (data, e[0]->src, SRC);
  s_check_folded_status (data, e[0]->src, TGT);
}

			/* --------------- */

static void
s_case_one_loop (bfsfp_data *data, edge *not_loop_edge)
{
  vertex *u = not_loop_edge->src;
  vertex *v = not_loop_edge->tgt;

  u = s_merge_classes (data, u, v);
  s_remove_edge_from_lists (data, not_loop_edge);
  s_check_folded_status (data, u, SRC);
  s_check_folded_status (data, u, TGT);
}

			/* --------------- */

static void
s_case_V (bfsfp_data *data, vertex *v, vertex *u, vertex *w, edge *to_remove)
{
  u = s_merge_classes (data, u, w);
  s_remove_edge_from_lists (data, to_remove);
  s_check_folded_status (data, u, SRC);
  s_check_folded_status (data, v, SRC);
  s_check_folded_status (data, u, TGT);
  s_check_folded_status (data, v, TGT);
}

			/* --------------- */

static void
s_case_parallel (bfsfp_data *data, vertex *v, vertex *u, edge *to_remove)
{
  s_remove_edge_from_lists (data, to_remove);
  s_check_folded_status (data, u, SRC);
  s_check_folded_status (data, v, SRC);
  s_check_folded_status (data, u, TGT);
  s_check_folded_status (data, v, TGT);
}

			/* --------------- */

static int
s_fold_edges (bfsfp_data *data, edge **e)
{
  int i;
  int one_merge = 1;
  vertex *u[2];
  vertex *v[2];

  ccl_assert (e[0] != NULL && e[1] != NULL);
  ccl_assert (e[0] != e[1]);

  for (i = 0; i < 2; i++)
    {
      REMEMBER (data, &e[i]->src);
      REMEMBER (data, &e[i]->tgt);

      e[i]->src = u[i] = s_get_representative (data, e[i]->src);
      e[i]->tgt = v[i] = s_get_representative (data, e[i]->tgt);
    }

  if (u[0] == v[0])
    {
      if (u[1] == v[1])
	{
	  ccl_assert (u[0] == u[1]);
	  s_case_two_self_loops (data, e);
	  one_merge = 0;
	}
      else 
	{
	  ccl_assert (u[1] == u[0] || v[1] == u[0]);
	  s_case_one_loop (data, e[1]);
	}
    }
  else if (u[1] == v[1])
    {
      ccl_assert (u[1] == u[0] || u[1] == v[0]);
      s_case_one_loop (data, e[0]);
    }
  else if (u[0] == u[1])
    {
      if (v[0] == v[1])
	{
	  s_case_parallel (data, u[0], v[0], e[1]);
	  one_merge = 0;
	}
      else
	{
	  s_case_V (data, u[0], v[0], v[1], e[1]);
	}
    }
  else
    {
      ccl_assert (v[0] == v[1]);
      s_case_V (data, v[0], u[0], u[1], e[1]);
    }

  return one_merge;
}

			/* --------------- */

