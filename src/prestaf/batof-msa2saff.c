/*
 * batof-msa2saff.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <sataf/sataf.h>
#include "prestaf-batof-p.h"

#define BOUNDS_ALGO 3

struct component
{
  sataf_sa *sa;
# define T_LAMBDAS 0
# define T_INIT 1
# define T_BAD 2
# define T_GOOD 3
  ccl_bittable *tables[4];
  struct component *childs[1];
};

struct compdata
{
  ccl_list result;
  int m;
  struct component *C;
};

			/* --------------- */

static btf_bound *
s_looks_for_vsp (btf_bound **pbounds, int *pbounds_size, tla_vsp_t V);

#if BOUNDS_ALGO == 1
static int
s_are_lambdas_equal (int m, ccl_bittable *lambdas, int q1, int q2);
#endif

#if (BOUNDS_ALGO == 1 || BOUNDS_ALGO == 2)
static unsigned int
s_hash_state (const void *ptr);

static int 
s_cmp_state (const void *ptr1, const void *ptr2);
#endif

			/* --------------- */

void
btf_msa_to_saff (tla_saff_t saff, sataf_msa *msa)
{
  if (ccl_debug_is_on)
    {
      ccl_debug ("compute saff for MSA %p=<%p,%d>\n", msa, msa->A->automaton,
		 msa->initial);
      sataf_msa_log_info (CCL_LOG_DEBUG, msa);
      ccl_debug("\n");
      ccl_debug_push_tab_level ();
    }

  {
    ccl_pair *p;
    ccl_list *components = btf_compute_components (saff->m, msa, NULL);
    for (p = FIRST (components); p; p = CDR (p))
      {
	btf_saffcomp *comp = (btf_saffcomp *) CAR (p);

	tla_saff_add_vsp (saff, saff, comp->V);
	btf_saffcomp_delete (comp);
      }
    ccl_list_delete (components);
  }

  if (ccl_debug_is_on)
    {
      ccl_debug_pop_tab_level ();
      ccl_debug ("done\n");
    }
}

			/* --------------- */

#if BOUNDS_ALGO == 1
static ccl_bittable *
s_build_bad_good (int m, int q0, btf_sa_tables *C);

static int
s_update_bad_good (int m, int q1, int q2, btf_sa_tables *C, ccl_bittable *set,
		   ccl_bittable *unset);

			/* --------------- */

void
btf_msa_to_bounds(int m, sataf_msa *X, ccl_list *Tcomps, ccl_hash *sa_tables, 
		  btf_bound **pbounds, int *pbounds_size)
{
  ccl_pair *p;

  *pbounds = NULL;
  *pbounds_size = 0;

  ccl_assert (! sataf_msa_is_zero (X));

  if (ccl_debug_is_on)
    {
      ccl_debug ("compute bounds for MSA %p=<%p,%d>\n", X, X->A->automaton,
		 X->initial);
      sataf_msa_log_info (CCL_LOG_DEBUG, X);
      ccl_debug("\n");
      ccl_debug_push_tab_level ();
    }

  *pbounds = ccl_new_array (btf_bound, ccl_list_get_size (Tcomps));
  if (ccl_debug_is_on)
    ccl_debug ("compute bounds for each %d component(s).\n",
	       ccl_list_get_size (Tcomps));

  for (p = FIRST (Tcomps); p; p = CDR (p))
    {
      int qp;
      btf_sa_tables *sa_tab;
      btf_saffcomp *comp = CAR (p);
      sataf_msa *Xq = sataf_msa_find_or_add (comp->C, comp->q);
      sataf_ea *ea = comp->C->automaton;
      int nb_states = sataf_ea_get_nb_states (ea);
      btf_bound *bounds = s_looks_for_vsp (pbounds, pbounds_size, comp->V);
      ccl_bittable *on_0_loop;

      comp->mi = ccl_new (modulo_info);
      comp->mi->m = comp->m;
      comp->mi->refcount = 1;
      comp->mi->modea = 
	btf_ea_compute_mod_classes (ea, &comp->mi->states_to_classes, 
				    &comp->mi->classes_to_states);

      ccl_assert (*pbounds_size <= ccl_list_get_size (Tcomps));

      ccl_hash_find (sa_tables, comp->C);
      sa_tab = ccl_hash_get (sa_tables);
      if (ccl_debug_is_on)
	{
	  ccl_debug ("nb_classes=%d\n",comp->mi->nb_classes);
	  ccl_debug ("build BAD/GOOD sets\n");
	}

      on_0_loop = s_build_bad_good (m, comp->q, sa_tab);

      if (ccl_debug_is_on)
	{

	  ccl_debug ("|ON_0_LOOP|=%d\n", ccl_bittable_get_nb_one (on_0_loop));
	  ccl_debug ("|BAD|=%d/%d\n",
		     ccl_bittable_get_nb_one (sa_tab->tables[T_BAD]), 
		     nb_states);
	}

      for (qp = ccl_bittable_get_first (on_0_loop);
	   qp >= 0; qp = ccl_bittable_get_next (on_0_loop, qp))
	{
	  if (!(batof_flags&BATOF_FLAG_TRACE))
	    ccl_debug ("%d/%d\n", qp, nb_states);

	  if ((!ccl_bittable_has (sa_tab->tables[T_BAD], qp) &&
	       !ccl_bittable_has (sa_tab->tables[T_GOOD], qp)))
	    {
	      int i, u;
	      int has_it = 0;
	      tla_saff_t aux;
	      sataf_msa *Xqp = sataf_msa_find_or_add (comp->C, qp);
	      sataf_msa *Xdiff;

	      if (ccl_debug_is_on)
		ccl_debug ("compute XOR(%p,%p)...", Xq, Xqp);
	      Xdiff = sataf_msa_xor (Xq, Xqp);
	      if (ccl_debug_is_on)
		ccl_debug ("done\n");

	      tla_saff_init (aux, m);
	      btf_msa_to_saff (aux, Xdiff);
	      sataf_msa_del_reference (Xdiff);

	      for (i = 0; i < aux->size && !has_it; i++)
		{
		  has_it = tla_vsp_equals (aux->V[i], comp->V);
		}

	      if (!has_it)
		{
		  /* ccl_assert (s2c[qp] == s2c[comp->q]); */
		  
		  ccl_bittable_set (sa_tab->tables[T_GOOD], qp);
		  u = s_update_bad_good (m, comp->q, qp, sa_tab, 
					 sa_tab->tables[T_GOOD],
					 sa_tab->tables[T_BAD]);
		  tla_saff_assign_union (bounds->B, aux);
		  if (ccl_debug_is_on)
		    ccl_debug ("move %d states into GOOD\n", u);
		}
	      else
		{
		  ccl_bittable_set (sa_tab->tables[T_BAD], qp);
		  u = s_update_bad_good (m, comp->q, qp, sa_tab, 
					 sa_tab->tables[T_BAD],
					 sa_tab->tables[T_GOOD]);
		  if (ccl_debug_is_on)
		    ccl_debug ("move %d states into BAD\n", u);
		}
	      tla_saff_clear (aux);
	      sataf_msa_del_reference (Xqp);
	    }
	}

      if (ccl_debug_is_on)
	{
	  int bad = ccl_bittable_get_nb_one (sa_tab->tables[T_BAD]);
	  int good = ccl_bittable_get_nb_one (sa_tab->tables[T_GOOD]);
	  ccl_debug ("|GOOD|=%d/%d\n", good, nb_states);
	  ccl_debug ("|BAD|=%d/%d\n", bad, nb_states);
	  ccl_debug ("total=%d/%d\n", bad + good, nb_states);

	  ccl_debug ("nb_classes=%d n0=%d n=%d\n",comp->mi->nb_classes,
		     comp->mi->n0,comp->mi->n);
	}

      ccl_bittable_delete (on_0_loop);
      sataf_msa_del_reference (Xq);
    }

  if (ccl_debug_is_on)
    {
      ccl_debug_pop_tab_level ();
      ccl_debug ("done.\n");
    }
}

			/* --------------- */

static ccl_bittable *
s_build_bad_good (int m, int q0, btf_sa_tables *C)
{
  int q;
  int s;
  int nb_states = C->sa->automaton->nb_local_states;
  sataf_ea *ea = C->sa->automaton;
  ccl_bittable *done = ccl_bittable_create (nb_states);
  ccl_bittable *on_stack = ccl_bittable_create (nb_states);
  ccl_bittable *on_0_loop = ccl_bittable_create (nb_states);
  int t = 0;

  if (t)
    {
      int z = q0;
      
      do
	{
	  z = sataf_ea_get_successor (ea, z, 0);
	  ccl_assert (sataf_ea_is_local_state (z));
	  z = sataf_ea_decode_succ_state (z);
	}
      while (z != q0);
    }

  C->tables[T_GOOD] = ccl_bittable_create (nb_states);
  C->tables[T_BAD] = ccl_bittable_create (nb_states);

  for (s = 0; s < nb_states; s++)
    {
      int dsp, sp, i;
      int stop;

      if (ccl_bittable_has (done, s))
	continue;

      ccl_bittable_clear (on_stack);
      ccl_bittable_set (on_stack, s);
      dsp = s;
      stop = 0;

      do
	{
	  sp = sataf_ea_get_successor (ea, (unsigned int) dsp, 0);
	  dsp = sataf_ea_decode_succ_state (sp);

	  if (sataf_ea_is_exit_state (sp) ||
	      ccl_bittable_has (C->tables[T_BAD], dsp))
	    stop = 1;
	  else if (ccl_bittable_has (on_stack, dsp))
	    stop = 2;
	  else
	    ccl_bittable_set (on_stack, dsp);

	  if (stop)
	    {
	      if (stop == 2)
		{
		  int saux = dsp;

		  do
		    {
		      saux =
			sataf_ea_get_successor (ea, (unsigned int) saux, 0);
		      ccl_assert (!sataf_ea_is_exit_state (saux));
		      saux = sataf_ea_decode_succ_state (saux);
		      ccl_bittable_set (on_0_loop, saux);
		    }
		  while (saux != dsp);
		}

	      for (i = ccl_bittable_get_first (on_stack);
		   i >= 0; i = ccl_bittable_get_next (on_stack, i))
		{
		  if (stop == 1)
		    ccl_bittable_set (C->tables[T_BAD], i);
		  ccl_bittable_set (done, i);
		}
	    }
	}
      while (!stop);
    }

  ccl_bittable_delete (done);
  ccl_bittable_delete (on_stack);

  ccl_assert (ccl_bittable_has (on_0_loop, q0));
  ccl_bittable_set (C->tables[T_GOOD], q0);

  for (q = 0; q < nb_states; q++)
    {
      if (ccl_bittable_has (C->tables[T_BAD], q))
	continue;

      if (!s_are_lambdas_equal (m, C->tables[T_LAMBDAS], q, q0))
	{
	  ccl_bittable_set (C->tables[T_BAD], q);
	  continue;
	}
    }

  return on_0_loop;
}

			/* --------------- */

static int
s_update_bad_good (int m, int q1, int q2, btf_sa_tables *C, ccl_bittable *set, 
		   ccl_bittable *unset)
{
  int result = 0;
  ccl_hash *done = ccl_hash_create (s_hash_state, s_cmp_state, NULL, ccl_free);
  ccl_list *todo = ccl_list_create ();
  sataf_ea *ea = C->sa->automaton;
  int aux[3];
  int *state = ccl_new_array (int, 3);

  state[0] = q1;
  state[1] = q2;
  state[2] = 0;
  ccl_list_add (todo, state);

  while (!ccl_list_is_empty (todo))
    {
      unsigned int b;
      int *s = ccl_list_take_first (todo);


      for (b = 0; b < 2; b++)
	{
	  aux[0] = sataf_ea_get_successor (ea, (unsigned int) s[0], b);
	  if (sataf_ea_is_exit_state (aux[0]))
	    continue;
	  aux[1] = sataf_ea_get_successor (ea, (unsigned int) s[1], b);
	  if (sataf_ea_is_exit_state (aux[1]))
	    continue;
	  aux[2] = (s[2] + 1) % m;

	  aux[0] = sataf_ea_decode_succ_state (aux[0]);
	  aux[1] = sataf_ea_decode_succ_state (aux[1]);

	  if (ccl_hash_find (done, aux))
	    continue;
	  s = ccl_new_array (int, 3);
	  ccl_memcpy (s, aux, sizeof (int) * 3);
	  ccl_hash_find (done, s);
	  ccl_hash_insert (done, s);
	  ccl_list_add (todo, s);
	  if (aux[0] == q1 && aux[2] == 0)
	    {
	      ccl_assert (!ccl_bittable_has (unset, aux[1]));

	      if (!ccl_bittable_has (set, aux[1]))
		{
		  result++;
		  ccl_bittable_set (set, aux[1]);
		}
	    }
	}
    }

  ccl_delete (state);
  ccl_list_delete (todo);
  ccl_hash_delete (done);

  return result;
}
#endif

			/* --------------- */

#if BOUNDS_ALGO == 2
static void
s_update_0_loop (int m, int q1, int q2, btf_sa_tables *C, ccl_bittable *loop);


void
btf_msa_to_bounds(int m, sataf_msa *X, ccl_list *Tcomps, ccl_hash *sa_tables, 
		  btf_bound **pbounds, int *pbounds_size)
{
  ccl_pair *p;

  *pbounds = NULL;
  *pbounds_size = 0;

  ccl_assert (! sataf_msa_is_zero (X));

  CCL_DEBUG_START_TIMED_BLOCK (("compute bounds for MSA %p=<%p,%d>", X, 
				X->A->automaton, X->initial));
  if (ccl_debug_is_on)
    {
      sataf_msa_log_info (CCL_LOG_DEBUG, X);
      ccl_debug("\n");
    }


  *pbounds = ccl_new_array (btf_bound, ccl_list_get_size (Tcomps));
  if (ccl_debug_is_on)
    ccl_debug ("compute bounds for each %d component(s).\n",
	       ccl_list_get_size (Tcomps));

  for (p = FIRST (Tcomps); p; p = CDR (p))
    {
      int qp;
      int *cq;
      ccl_bittable *on_0_loop;
      btf_sa_tables *sa_tab;
      btf_saffcomp *comp = CAR (p);
      sataf_msa *Xq = sataf_msa_find_or_add (comp->C, comp->q);
      sataf_ea *ea = comp->C->automaton;
      btf_bound *bounds = s_looks_for_vsp (pbounds, pbounds_size, comp->V);

      comp->mi = ccl_new (modulo_info);
      comp->mi->m = comp->m;
      comp->mi->refcount = 1;
      comp->mi->modea = 
	btf_ea_compute_mod_classes (ea, &comp->mi->states_to_classes, 
				    &comp->mi->classes_to_states);
      cq = comp->mi->classes_to_states[comp->mi->states_to_classes[comp->q]];
      ccl_hash_find (sa_tables, comp->C);
      sa_tab = ccl_hash_get (sa_tables);

      on_0_loop = 
	ccl_bittable_create (comp->C->automaton->nb_local_states);
      cq = comp->mi->classes_to_states[comp->mi->states_to_classes[comp->q]];
      for(; *cq >= 0; cq++)
	ccl_bittable_set (on_0_loop, *cq);

      cq = comp->mi->classes_to_states[comp->mi->states_to_classes[comp->q]];
      while ((qp = ccl_bittable_get_first (on_0_loop)) >= 0)
	{
	  int i;
	  int has_it = 0;
	  tla_saff_t aux;
	  sataf_msa *Xdiff;
	  sataf_msa *Xqp;

	  ccl_bittable_unset (on_0_loop, qp);

	  if (qp == comp->q)
	    continue;

	  Xqp = sataf_msa_find_or_add (comp->C, qp);
	  CCL_DEBUG_START_TIMED_BLOCK (("compute XOR(%p,%p)...", Xq, Xqp));
	  Xdiff = sataf_msa_xor (Xq, Xqp);
	  CCL_DEBUG_END_TIMED_BLOCK ();
	  
	  tla_saff_init (aux, m);
	  btf_msa_to_saff (aux, Xdiff);
	  sataf_msa_del_reference (Xdiff);
	  
	  for (i = 0; i < aux->size && !has_it; i++)
	    has_it = tla_vsp_equals (aux->V[i], comp->V);
	  
	  if (!has_it)
	    {
	      tla_saff_assign_union (bounds->B, aux);
	    }
	  s_update_0_loop (m, comp->q, qp, sa_tab, on_0_loop);

	  tla_saff_clear (aux);
	  sataf_msa_del_reference (Xqp);
	}

      ccl_bittable_delete (on_0_loop);
      sataf_msa_del_reference (Xq);
    }
  CCL_DEBUG_END_TIMED_BLOCK ();
}

			/* --------------- */

static void
s_update_0_loop (int m, int q1, int q2, btf_sa_tables *C, ccl_bittable *loop)
{
  ccl_hash *done = ccl_hash_create (s_hash_state, s_cmp_state, NULL, ccl_free);
  ccl_list *todo = ccl_list_create ();
  sataf_ea *ea = C->sa->automaton;
  int aux[3];
  int *state = ccl_new_array (int, 3);

  state[0] = q1;
  state[1] = q2;
  state[2] = 0;
  ccl_list_add (todo, state);

  while (!ccl_list_is_empty (todo))
    {
      unsigned int b;
      int *s = ccl_list_take_first (todo);


      for (b = 0; b < 2; b++)
	{
	  aux[0] = sataf_ea_get_successor (ea, (unsigned int) s[0], b);
	  if (sataf_ea_is_exit_state (aux[0]))
	    continue;
	  aux[1] = sataf_ea_get_successor (ea, (unsigned int) s[1], b);
	  if (sataf_ea_is_exit_state (aux[1]))
	    continue;
	  aux[2] = (s[2] + 1) % m;

	  aux[0] = sataf_ea_decode_succ_state (aux[0]);
	  aux[1] = sataf_ea_decode_succ_state (aux[1]);

	  if (ccl_hash_find (done, aux))
	    continue;
	  s = ccl_new_array (int, 3);
	  ccl_memcpy (s, aux, sizeof (int) * 3);
	  ccl_hash_find (done, s);
	  ccl_hash_insert (done, s);
	  ccl_list_add (todo, s);
	  if (aux[0] == q1 && aux[2] == 0)
	    ccl_bittable_unset (loop, aux[1]);
	}
    }

  ccl_delete (state);
  ccl_list_delete (todo);
  ccl_hash_delete (done);
}
#endif

#if BOUNDS_ALGO == 3

static int
s_get_lambda (int m, int s, ccl_bittable *lambdas)
{
  int lambda;

  if (s == 0)
    lambda = ccl_bittable_get_first (lambdas);
  else 
    lambda = ccl_bittable_get_next (lambdas, m * s - 1);
  ccl_assert (ccl_bittable_get_next (lambdas, lambda) == -1 ||
	      ccl_bittable_get_next (lambdas, lambda) >= m * (s+1));

  lambda -= m * s;

  return lambda; 
}

			/* --------------- */

void
btf_msa_to_bounds(int m, sataf_msa *X, ccl_list *Tcomps, ccl_hash *sa_tables, 
		  btf_bound **pbounds, int *pbounds_size)
{
  ccl_pair *p;

  *pbounds = NULL;
  *pbounds_size = 0;

  ccl_assert (! sataf_msa_is_zero (X));

  CCL_DEBUG_START_TIMED_BLOCK (("compute bounds for MSA %p=<%p,%d>", X, 
				X->A->automaton, X->initial));
  if (ccl_debug_is_on)
    {
      sataf_msa_log_info (CCL_LOG_DEBUG, X);
      ccl_debug("\n");
    }


  *pbounds = ccl_new_array (btf_bound, ccl_list_get_size (Tcomps));
  if (ccl_debug_is_on)
    ccl_debug ("compute bounds for each %d component(s).\n",
	       ccl_list_get_size (Tcomps));

  for (p = FIRST (Tcomps); p; p = CDR (p))
    {
      int i;
      int nb_xors;
      btf_sa_tables *sa_tab;
      int *q0_xors;
      sataf_msa **xors;
      btf_saffcomp *comp = CAR (p);
      sataf_ea *ea = comp->C->automaton;
      btf_bound *bounds = s_looks_for_vsp (pbounds, pbounds_size, comp->V);
      ccl_bittable *lambdas;
      int lambda_q;

      if (0) {
	int i;
	int nb_hspa;
	sataf_ea **hspa;
	
	btf_compute_half_spaces_automata (comp->q, ea, &hspa, &nb_hspa);
	for (i = 0; i < nb_hspa; i++)
	  sataf_ea_del_reference (hspa[i]);
	ccl_zdelete (ccl_delete, hspa);	
      }
      ccl_assert (*pbounds_size <= ccl_list_get_size (Tcomps));

      ccl_hash_find (sa_tables, comp->C);
      sa_tab = ccl_hash_get (sa_tables);
      lambdas = sa_tab->tables[T_LAMBDAS];
      lambda_q = s_get_lambda (m, comp->q, lambdas);
      xors = btf_backward_xor (m, ea, &q0_xors, &nb_xors);

      for (i = 0; i < nb_xors; i++)
	{
	  if (btf_is_untransient (xors[i]->A))
	    {
	      tla_vsp_t V;
	      int lambda_q0;

	      lambda_q0 = s_get_lambda (m, q0_xors[i], lambdas);
	      while (lambda_q0 != lambda_q)
		{
		  sataf_msa *succ = sataf_msa_succ (xors[i], 0);
		  
		  if (sataf_msa_is_zero (succ))
		    {
		      sataf_msa_del_reference (succ);
		      succ = sataf_msa_succ (xors[i], 1);
		      ccl_assert (! sataf_msa_is_zero (succ));
		    }
		  sataf_msa_del_reference (xors[i]);
		  xors[i] = succ;
		  lambda_q0 = (lambda_q0 + 1) % m;
		}

	      tla_vsp_init (V, m);
	      btf_ea_to_vsp(V, xors[i]->A->automaton, xors[i]->initial);
	      tla_saff_add_vsp(bounds->B, bounds->B, V);
	      tla_vsp_clear (V);
	    }
	  sataf_msa_del_reference (xors[i]);
	}
      ccl_delete (xors);
      ccl_delete (q0_xors);
    }
  CCL_DEBUG_END_TIMED_BLOCK ();
}
#endif

#if 0
static void
s_log_lambdas (ccl_log_type log, int nb_states, int m, ccl_bittable *bt)
{
  int i;

  for (i = 0; i < nb_states; i++)
    {
      int j;

      ccl_log (log, "%d : ", i);
      for (j = 0; j < m; j++)
	if (ccl_bittable_has (bt, i * m + j))
	  ccl_log (log, "%d ", j);
      ccl_log (log, "\n");
    }
}

			/* --------------- */

static void
s_log_lambdas_for_msa (ccl_log_type log, sataf_msa *msa, int m,
		       ccl_bittable *bt)
{
  sataf_ea_display_as_dot (msa->A->automaton, log, NULL);
  ccl_log (log, "\n");
  s_log_lambdas (log, msa->A->automaton->nb_local_states, m, bt);
}

static void
s_log_msa_as_saff (ccl_log_type log, sataf_msa *msa, int m)
{
  tla_saff_t aux;
  tla_saff_init (aux, m);
  btf_msa_to_saff (aux, msa);
  ccl_log (log, "saff(%p) = ", msa);
  tla_saff_log_equations (log, aux);
  tla_saff_clear (aux);
}
#endif

void
btf_modulo_info_del_reference (modulo_info *mi)
{
  ccl_pre (mi != NULL);
  ccl_pre (mi->refcount > 0);

  mi->refcount--;
  if (mi->refcount != 0)
    return;

  tla_group_clear (mi->invV);
  
  if (mi->modea != NULL)
    {
      sataf_ea_del_reference (mi->modea);
      ccl_delete (mi->states_to_classes);
      ccl_delete (mi->classes_to_states);
    }

  ccl_zdelete (ccl_delete, mi);
}

			/* --------------- */

void
btf_saffcomp_delete (btf_saffcomp *c)
{
  ccl_zdelete (btf_modulo_info_del_reference, c->mi);

  if (c->patterns != NULL)
    {
      int i;
      int max = c->nb_classes * c->m;
      for (i = 0; i < max; i++)
	ccl_zdelete (ccl_delete, c->patterns[i]);
      ccl_delete (c->patterns);
    }
  tla_matrix_clear (c->X);
  ccl_zdelete (ccl_delete, c->ci);

  tla_vsp_clear (c->V);
  sataf_sa_del_reference (c->C);
  ccl_delete (c);
}

			/* --------------- */

static btf_bound *
s_looks_for_vsp (btf_bound **pbounds, int *pbounds_size, tla_vsp_t V)
{
  int i;
  btf_bound *bounds;

  for (i = 0, bounds = *pbounds; i < *pbounds_size; i++, bounds++)
    {
      if (tla_vsp_are_equal (bounds->V, V))
	break;
    }

  if (i == *pbounds_size)
    {
      int m = tla_vsp_get_size (V);

      *pbounds_size = i + 1;
      tla_vsp_init (bounds->V, m);
      tla_vsp_set (bounds->V, V);
      tla_saff_init (bounds->B, m);
    }

  return bounds;
}

			/* --------------- */

#if 0
static ccl_bittable *
s_compute_0_loop (int m, int q0, btf_sa_tables *C, int *s2c, int *c)
{
  int nb_states = C->sa->automaton->nb_local_states;
  sataf_ea *ea = C->sa->automaton;
  ccl_bittable *done = ccl_bittable_create (nb_states);
  ccl_bittable *on_stack = ccl_bittable_create (nb_states);
  ccl_bittable *on_0_loop = ccl_bittable_create (nb_states);  

  for (; *c >= 0; c++)
    {
      int dsp = *c;
      int stop = 0;
      
      if (ccl_bittable_has (done, dsp))
	continue;

      ccl_bittable_set (on_stack, dsp);

      while (!stop)
	{
	  int sp = sataf_ea_get_successor (ea, (unsigned int) dsp, 0);
	  dsp = sataf_ea_decode_succ_state (sp);

	  if (! ccl_bittable_has (on_stack, dsp))
	    ccl_bittable_set (on_stack, dsp);
	  else
	    {
	      int i;
	      int saux = dsp;
	  
	      do
		{
		  saux = sataf_ea_get_successor (ea, (unsigned int) saux, 0);
		  ccl_assert (!sataf_ea_is_exit_state (saux));
		  saux = sataf_ea_decode_succ_state (saux);
		  if (s_are_lambdas_equal (m, C->tables[T_LAMBDAS], saux, q0) 
		      && s2c[q0] == s2c[saux])
		    ccl_bittable_set (on_0_loop, saux);
		}
	      while (saux != dsp);


	      for (i = ccl_bittable_get_first (on_stack);
		   i >= 0; i = ccl_bittable_get_next (on_stack, i))
		ccl_bittable_set (done, i);

	      stop = 1;
	    }
	}
      ccl_bittable_clear (on_stack);
    }

  ccl_bittable_delete (on_stack);
  ccl_bittable_delete (done);

  ccl_assert (ccl_bittable_has (on_0_loop, q0));

  return on_0_loop;
}
#endif
			/* --------------- */

#if (BOUNDS_ALGO == 1 || BOUNDS_ALGO == 2)
static unsigned int
s_hash_state (const void *ptr)
{
  return 13 * ((uint32_t *) ptr)[0] + 19 * ((uint32_t *) ptr)[1] +
    141 * ((uint32_t *) ptr)[2];
}

			/* --------------- */

static int
s_cmp_state (const void *ptr1, const void *ptr2)
{
  return ccl_memcmp (ptr1, ptr2, sizeof (int) * 3);
}
#endif


#if BOUNDS_ALGO == 1
static int
s_are_lambdas_equal (int m, ccl_bittable *lambdas, int q1, int q2)
{
  int i, hq1, hq2;

  if (q1 == q2)
    return 1;

  for (i = 0, q1 *= m, q2 *= m; i < m; i++, q1++, q2++)
    {
      hq1 = ccl_bittable_has (lambdas, q1) ? 1 : 0;
      hq2 = ccl_bittable_has (lambdas, q2) ? 1 : 0;
      if (hq1 != hq2)
	return 0;
    }
  return 1;
}
#endif


#if BOUNDS_ALGO == 4
void
btf_msa_to_bounds(int m, sataf_msa *X, ccl_list *Tcomps, ccl_hash *sa_tables, 
		  btf_bound **pbounds, int *pbounds_size)
{
  ccl_pair *p;

  *pbounds = NULL;
  *pbounds_size = 0;

  ccl_assert (! sataf_msa_is_zero (X));

  CCL_DEBUG_START_TIMED_BLOCK (("compute bounds for MSA %p=<%p,%d>", X, 
				X->A->automaton, X->initial));
  if (ccl_debug_is_on)
    {
      sataf_msa_log_info (CCL_LOG_DEBUG, X);
      ccl_debug("\n");
    }


  *pbounds = ccl_new_array (btf_bound, ccl_list_get_size (Tcomps));
  if (ccl_debug_is_on)
    ccl_debug ("compute bounds for each %d component(s).\n",
	       ccl_list_get_size (Tcomps));

  for (p = FIRST (Tcomps); p; p = CDR (p))
    {
      int *state_to_class = NULL;
      int **class_to_states = NULL;
      btf_saffcomp *comp = CAR (p);
      sataf_ea *ea = comp->C->automaton;
      btf_bound *bounds = s_looks_for_vsp (pbounds, pbounds_size, comp->V);
      sataf_ea *modea = 
	btf_ea_compute_mod_classes (ea, &p_state_to_class, &p_class_to_state);

      ccl_assert (*pbounds_size <= ccl_list_get_size (Tcomps));

      sataf_ea_del_reference (modea);
      ccl_delete (state_to_class);
      ccl_delete (class_to_states);
    }
  CCL_DEBUG_END_TIMED_BLOCK ();
}
#endif
