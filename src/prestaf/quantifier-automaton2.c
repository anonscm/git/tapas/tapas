/*
 * quantifier-automaton.c -- Quantifier V2 for a MSA-encoded relation
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include "prestaf-formula-p.h"
#include "prestaf-automata.h"

typedef struct qaut_st
{
  sataf_mao super;
  uint32_t i;
  uint32_t n;
  sataf_msa *fset;
} qaut;

typedef struct qaut_methods_st
{
  sataf_mao_methods super;
  sataf_msa *(*quantifier) (sataf_msa *a);
} qaut_methods;

			/* --------------- */

static size_t
s_qaut_size (sataf_mao *self);

static void
s_qaut_destroy (sataf_mao *self);

#define s_qaut_no_cache NULL

#define s_qaut_is_root_of_scc NULL

#define s_qaut_simplify NULL

static uint32_t
s_qaut_get_alphabet_size (sataf_mao *self);

static sataf_mao *
s_qaut_succ (sataf_mao *self, uint32_t letter);

static int
s_qaut_is_final (sataf_mao *self);

static int
s_qaut_is_equal_to (sataf_mao *self, sataf_mao *other);

static uint32_t
s_qaut_hashcode (sataf_mao *self);

#define s_qaut_to_dot NULL

			/* --------------- */

static const qaut_methods QAUT_FORALL_METHODS = 
  {
    {
      "SA-QAUT-FA2",
      s_qaut_size,
      s_qaut_destroy,
      s_qaut_no_cache,
      s_qaut_is_root_of_scc,
      s_qaut_simplify,
      s_qaut_get_alphabet_size,
      NULL,
      s_qaut_succ,
      s_qaut_is_final,
      s_qaut_is_equal_to,
      s_qaut_hashcode,
      s_qaut_to_dot
    },
    sataf_msa_forall
  };

			/* --------------- */

static const qaut_methods QAUT_EXISTS_METHODS = 
  {
    {
      "SA-QAUT-EX2",
      s_qaut_size,
      s_qaut_destroy,
      s_qaut_no_cache,
      s_qaut_is_root_of_scc,
      s_qaut_simplify,
      s_qaut_get_alphabet_size,
      NULL,
      s_qaut_succ,
      s_qaut_is_final,
      s_qaut_is_equal_to,
      s_qaut_hashcode,
      s_qaut_to_dot
    },
    sataf_msa_exists
  };

			/* --------------- */

static qaut *
s_create_qaut (uint32_t i, uint32_t n, sataf_msa *fset,
	       const sataf_mao_methods *methods);

			/* --------------- */

static sataf_mao *
s_create_quantifier (uint32_t i, uint32_t n, sataf_msa *fset,
		     const qaut_methods *methods)
{
  sataf_mao *R;

  ccl_pre (n > 1);

  if (sataf_msa_is_zero (fset))
    R = sataf_mao_create_zero (sataf_msa_get_alphabet_size (fset));
  else if (sataf_msa_is_one (fset))
    R = sataf_mao_create_one (sataf_msa_get_alphabet_size (fset));
  else if (i == 0)
    {
      sataf_msa *tmp = methods->quantifier (fset);
      R = s_create_quantifier (n - 1, n, tmp, methods);
      sataf_msa_del_reference (tmp);
    }
  else
    {
      R = (sataf_mao *)
	s_create_qaut (i, n, fset, (const sataf_mao_methods *) methods);
    }

  return R;
}

			/* --------------- */


sataf_mao *
prestaf_crt_quantifier_automaton2 (int forall, uint32_t i, uint32_t n, 
				   sataf_msa *fset)
{
  const qaut_methods *methods;
  sataf_mao *R;

  if (sataf_msa_is_zero (fset))
    R = sataf_mao_create_zero (sataf_msa_get_alphabet_size (fset));
  else if (sataf_msa_is_one (fset))
    R = sataf_mao_create_one (sataf_msa_get_alphabet_size (fset));
  else if (n == 1)
    {
      if (forall)
	R = sataf_mao_create_zero (sataf_msa_get_alphabet_size (fset));
      else
	R = sataf_mao_create_one (sataf_msa_get_alphabet_size (fset));
    }
  else
    {
      if (forall)
	methods = &QAUT_FORALL_METHODS;
      else
	methods = &QAUT_EXISTS_METHODS;

      R = s_create_quantifier (i, n, fset, methods);
    }

  return R;
}

			/* --------------- */

static size_t
s_qaut_size (sataf_mao *self)
{
  return sizeof (qaut);
}

			/* --------------- */

static void
s_qaut_destroy (sataf_mao *self)
{
  qaut *a = (qaut *) self;

  sataf_msa_del_reference (a->fset);
}

			/* --------------- */

static uint32_t
s_qaut_get_alphabet_size (sataf_mao *self)
{
  qaut *a = (qaut *) self;

  return sataf_msa_get_alphabet_size (a->fset);
}

			/* --------------- */

static sataf_mao *
s_qaut_succ (sataf_mao *self, uint32_t letter)
{
  qaut *a = (qaut *) self;
  sataf_msa *succ = sataf_msa_succ (a->fset, letter);
  sataf_mao *R = (sataf_mao *)
    s_create_quantifier (a->i - 1, a->n, succ,
			 (const qaut_methods *) a->super.methods);

  ccl_assert (a->i > 0);

  sataf_msa_del_reference (succ);

  return R;
}

			/* --------------- */

static int
s_qaut_is_final (sataf_mao *self)
{
  qaut *a = (qaut *) self;

  return sataf_msa_is_final (a->fset);
}

			/* --------------- */

static int
s_qaut_is_equal_to (sataf_mao *self, sataf_mao *other)
{
  qaut *a1 = (qaut *) self;
  qaut *a2 = (qaut *) other;

  return a1->i == a2->i && a1->n == a2->n && a1->fset == a2->fset;
}

			/* --------------- */

static uint32_t
s_qaut_hashcode (sataf_mao *self)
{
  qaut *a = (qaut *) self;

  return 13 * a->i + 17 * a->n + 19 * (uintptr_t) a->fset;
}

			/* --------------- */

static qaut *
s_create_qaut (uint32_t i, uint32_t n, sataf_msa *fset,
	       const sataf_mao_methods *methods)
{
  qaut *R = (qaut *) sataf_mao_create (sizeof (qaut), methods);

  R->i = i;
  R->n = n;
  R->fset = sataf_msa_add_reference (fset);

  return R;
}
