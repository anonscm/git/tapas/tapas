/*
 * batof-formulas.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-string.h>
#include "prestaf-automata.h"
#include "batof-formulas.h"

typedef struct half_space_st
{
  tla_vec_t alpha;
  int op;
} half_space;

typedef struct pattern_st
{
  ccl_list *B;
  ccl_ustring G;
} pattern;

#define ZN ((armoise_tree *) NULL)

#define IS_LABELLED_WITH(t,id) ((t)->node_type == ARMOISE_TREE_ ## id)
#define NEW_NODE(_type,_vtype,_child,_next)		\
  (ccl_parse_tree_create (_type, #_type, _vtype,	\
			  ((_child) == ZN				\
			   ? __LINE__					\
			   : (_child)->line),				\
			  ((_child) == ZN				\
			   ? __FILE__					\
			   : (_child)->filename),			\
			  _child, _next, NULL))

#define NN(_type,_vtype,_child,_next) \
  NEW_NODE(ARMOISE_TREE_ ## _type,_vtype,_child,_next)

#define NE(_type,_child,_next) NN (_type, CCL_PARSE_TREE_EMPTY, _child, _next)
#define U_NE(_type,_child) NE (_type, _child, ZN)
#define B_NE(_result, _type, _child1, _child2)	\
  do { \
    armoise_tree *c1 = _child1; \
    armoise_tree *c2 = _child2; \
    (_result) = U_NE (_type, c1); \
    c1->next = c2; \
  } while (0)

#define NID()  NN (IDENTIFIER, CCL_PARSE_TREE_IDENT, ZN, ZN)
#define NINT() NN (INTEGER, CCL_PARSE_TREE_INT, ZN, ZN)
#define NSTR() NN (STRING, CCL_PARSE_TREE_STRING, ZN, ZN)
#define REVERSE(_t) ccl_parse_tree_reverse_siblings (_t)


			/* --------------- */

static unsigned int
s_tla_vec_hash (const tla_vec_t v);
static int
s_tla_vec_cmp (const tla_vec_t v1, const tla_vec_t v2);
static void
s_tla_vec_delete (tla_vec_t v);

static unsigned int
s_tla_vsp_hash (const tla_vsp_t V);
static int
s_tla_vsp_cmp (const tla_vsp_t V1, const tla_vsp_t V2);
static void
s_tla_vsp_delete (tla_vsp_t V);

static ccl_ustring *
s_string_to_unique_strings (int m, const char * const *names);

static int
s_tla_group_cmp (const tla_group_t G1, const tla_group_t G2);
static void
s_tla_group_delete (tla_group_t G);

static int
s_H_cmp (const half_space *H1, const half_space *H2);
static unsigned int
s_H_hash (const half_space *H);
static void
s_H_delete (half_space *H);

static int
s_pattern_cmp (const pattern *P1, const pattern *P2);
static unsigned int
s_pattern_hash (const pattern *P);
static void
s_pattern_delete (pattern *P);

static void
s_compute_rho_plus_xhi (tla_vsp_t V, const tla_vec_t alpha, tla_vec_t rho,
			tla_vec_t xhi, tla_vec_t result);

			/* --------------- */

void
btf_init_compilation_data (struct msa2form_compilation_data *cd, int m,
			   const char * const *varnames)
{
  cd->m = m;
  cd->varnames = s_string_to_unique_strings (m, varnames);
  cd->nat_power_m_id = NULL;
  cd->Z_power_m_id = NULL;
  cd->definitions = ccl_list_create ();
  cd->mi = 
    ccl_hash_create (NULL, NULL, (ccl_delete_proc *)sataf_sa_del_reference,
		     (ccl_delete_proc *) btf_modulo_info_del_reference);
  cd->def_ids = 
    ccl_hash_create (NULL, NULL, (ccl_delete_proc *) sataf_msa_del_reference,
		     NULL);
  cd->V_defs = ccl_hash_create ((ccl_hash_func *) s_tla_vsp_hash, 
				(ccl_compare_func *) s_tla_vsp_cmp, 
				(ccl_delete_proc *) s_tla_vsp_delete,
				NULL);
  cd->G_defs = ccl_hash_create ((ccl_hash_func *) tla_group_hash, 
				(ccl_compare_func *) s_tla_group_cmp, 
				(ccl_delete_proc *) s_tla_group_delete,
				NULL);
  cd->vectors_defs = ccl_hash_create ((ccl_hash_func *) s_tla_vec_hash, 
				      (ccl_compare_func *) s_tla_vec_cmp, 
				      (ccl_delete_proc *) s_tla_vec_delete,
				      NULL);

  cd->H_defs = ccl_hash_create ((ccl_hash_func *) s_H_hash, 
				(ccl_compare_func *) s_H_cmp, 
				(ccl_delete_proc *) s_H_delete,
				NULL);

  cd->patterns_defs = ccl_hash_create ((ccl_hash_func *) s_pattern_hash, 
				       (ccl_compare_func *) s_pattern_cmp, 
				       (ccl_delete_proc *) s_pattern_delete,
				       NULL);
}

			/* --------------- */

void
btf_clear_compilation_data (struct msa2form_compilation_data *cd)
{
  ccl_delete (cd->varnames);
  ccl_list_delete (cd->definitions);
  ccl_hash_delete (cd->mi);
  ccl_hash_delete (cd->def_ids);
  ccl_hash_delete (cd->V_defs);
  ccl_hash_delete (cd->G_defs);
  ccl_hash_delete (cd->vectors_defs);
  ccl_hash_delete (cd->H_defs);
  ccl_hash_delete (cd->patterns_defs);
}

			/* --------------- */

armoise_tree *
btf_add_definition (struct msa2form_compilation_data *data, const char *s,
		    armoise_tree *F)
{
  armoise_tree *def = btf_form_crt_definition (s, F);
  
  ccl_list_add (data->definitions, def);

  return def;
}

/*!
 * let 
 *   id := P;
 * in
 *  { (X[0], ..., X[m-1]) | (X[0], ..., X[m-1]) in (nat, ..., nat) && id };
 */
static armoise_tree *
s_crt_synform_context (struct msa2form_compilation_data *cd)
{
  armoise_tree *result = U_NE (PREDICATE_CONTEXT, ZN);
  armoise_tree **pdefs = &(result->child);

  while (!ccl_list_is_empty (cd->definitions))
    {
      armoise_tree *def = ccl_list_take_first (cd->definitions);
      *pdefs = def;
      pdefs = &((*pdefs)->next);
    }

  return result;
}

			/* --------------- */

armoise_tree *
btf_form_crt_synform (struct msa2form_compilation_data *cd, armoise_tree *P)
{
  armoise_tree *result;
  armoise_tree *proto = btf_form_crt_vector_of_ids (cd->m, cd->varnames);
  armoise_tree *var = btf_form_crt_vector_of_ids (cd->m, cd->varnames);
  armoise_tree *natpm = btf_form_crt_nat_power_m (cd);
  armoise_tree *synform = btf_add_definition (cd, "R", P);
  ccl_ustring synformid = btf_form_get_def_id (synform);
  armoise_tree *ctx = s_crt_synform_context (cd);
  armoise_tree *F = NID ();

  B_NE (proto, IN, proto, natpm); 
  F->value.id_value = synformid;
  B_NE (F, IN, var, F); 
  B_NE (F, SET, proto, F);
  B_NE (result, PREDICATE, ctx, F);

  return result;
}

			/* --------------- */

armoise_tree *
btf_form_crt_definition (const char *id, armoise_tree *P)
{
  armoise_tree *result;
  armoise_tree *t_id = NID();

  t_id->value.id_value = ccl_string_make_unique (id);
  if (P->node_type != ARMOISE_TREE_PREDICATE)
    P = U_NE (PREDICATE, P);
  B_NE (result, DEFINITION, t_id, P);

  return result;
}

			/* --------------- */

static armoise_tree *
s_crt_factor (mpz_t c, int varindex, ccl_ustring *X)
{
  armoise_tree *var;
  armoise_tree *result;

  if (mpz_cmp_ui (c, 0) == 0 )
    return NULL;

  var = NID ();
  var->value.id_value = X[varindex];

  if (mpz_cmp_si (c, 1) == 0)
    result = var;
  else if (mpz_cmp_si (c, -1) == 0)
    result = U_NE (NEG, var);
  else
    {
      int value = MPZ2SI (c);
      armoise_tree *val = NINT ();
      if (value < 0)
	{
	  val->value.int_value = -value;
	  val = U_NE (NEG, val);
	}
      else
	{
	  val->value.int_value = value;
	}
      B_NE (result, MUL, val, var);
    }

  return result;
}

			/* --------------- */

armoise_tree *
btf_form_crt_V_definition (struct msa2form_compilation_data *cd, 
			   tla_vsp_t V)
{
  armoise_tree *result = NULL;

  if (ccl_hash_find (cd->V_defs, V))
    return ccl_hash_get (cd->V_defs);

  if (V->dim == 0)
    result = btf_form_trivial_predicate (cd, 1);
  else if (V->dim == V->size)
    result = btf_form_crt_Z_power_m (cd);
  else
    {
      int j;
      mpz_t lcm, c;
      int m = V->size;
      armoise_tree *proto;
      armoise_tree *var = btf_form_crt_vector_of_ids (cd->m, cd->varnames);
      armoise_tree *Zm = btf_form_crt_Z_power_m (cd);
      ccl_bittable *I = ccl_bittable_create (m);

      B_NE (proto, IN, var, Zm);

      mpz_init (c);
      mpz_init_set (lcm, V->gen[0].v->den);
      ccl_bittable_set (I, V->gen[0].i);

      for (j = 1; j < V->dim; j++)
	{
	  mpz_lcm (lcm, lcm, V->gen[j].v->den);
	  ccl_bittable_set (I, V->gen[j].i);
	}

      for (j = 0; j < V->size; j++)
	{
	  int k;
	  armoise_tree *eq;
	  armoise_tree *linear_term;
	  armoise_tree *zero;

	  if (ccl_bittable_has (I, j))
	    continue;

	  linear_term = s_crt_factor (lcm, j, cd->varnames);
	  ccl_assert (linear_term != NULL);

	  for (k = 0; k < V->dim; k++)
	    {
	      armoise_tree *f;

	      mpz_mul(c, lcm, V->gen[k].v->num[j]);
	      mpz_cdiv_q (c, c, V->gen[k].v->den);
	      mpz_neg (c, c);
	      
	      f = s_crt_factor (c, V->gen[k].i, cd->varnames);
	      if (f != NULL)
		B_NE (linear_term, PLUS, linear_term, f);
	    }
	  zero = NINT ();
	  zero->value.int_value = 0;

	  B_NE (eq, EQ, linear_term, zero);
	  if (result == NULL)
	    result = eq;
	  else
	    B_NE (result, AND, result, eq);
	}

      B_NE (result, SET, proto, result);
      
      mpz_clear (lcm);
      mpz_clear (c);

      ccl_bittable_delete (I);
    }

  {
    struct tla_vsp_st *nV = ccl_new (struct tla_vsp_st);
    char *id = ccl_string_format_new ("V%d", ccl_hash_get_size (cd->V_defs));

    result = btf_add_definition (cd, id, result);
    ccl_delete (id);
  
    tla_vsp_init (nV, V->size);
    tla_vsp_set (nV, V);
    ccl_assert (!ccl_hash_find (cd->V_defs, nV));
    ccl_hash_find (cd->V_defs, nV);
    ccl_hash_insert (cd->V_defs, result);
  }

  return result;
}

			/* --------------- */

static armoise_tree *
s_crt_VH (struct msa2form_compilation_data *cd, tla_vec_t rho, tla_vec_t xhi, 
	  tla_vsp_t V, tla_vec_t alpha, int op)
{
  armoise_tree *Hdef;
  armoise_tree *tmp;
  armoise_tree *Vdef;
  armoise_tree *rho_plus_xhi = NULL;
  tla_vec_t integral_rho_plus_xhi;
  int m = tla_vsp_get_size (V);
  armoise_tree *result;
  tla_vec_t a;

  tla_vec_init (integral_rho_plus_xhi, m);

  if (op == LCA_GEQ)
    tla_vec_init_neg (a, alpha);
  else
    tla_vec_init_set (a, alpha);

  Vdef = btf_form_crt_V_definition (cd, V);

  s_compute_rho_plus_xhi (V, a, rho, xhi, integral_rho_plus_xhi);
  if (! tla_vec_is_zero (integral_rho_plus_xhi))
    rho_plus_xhi = btf_form_vec_to_armoise_predicate (integral_rho_plus_xhi);

  Hdef = btf_form_crt_H_definition (cd, btf_form_get_def_id (Vdef), a, LCA_LEQ);
  tmp = NID ();
  tmp->value.id_value = btf_form_get_def_id (Hdef);
  Hdef = tmp;

  result = Hdef;

  if (rho_plus_xhi != NULL)
    B_NE (result, PLUS, rho_plus_xhi, result);

  tla_vec_clear (a);
  tla_vec_clear (integral_rho_plus_xhi);  

  return result;
}

			/* --------------- */

armoise_tree *
btf_form_crt_VH_definition (struct msa2form_compilation_data *cd, 
			    const char *id, tla_vec_t rho, tla_vec_t xhi, 
			    tla_vsp_t V, tla_vec_t alpha, int op)
{  
  armoise_tree *result = s_crt_VH (cd, rho, xhi, V, alpha, op);
  result = btf_form_crt_definition (id, result);

  return result;
}


			/* --------------- */

static armoise_tree *
s_find_H (struct msa2form_compilation_data *cd, tla_vec_t alpha, 
	  int op)
{
  half_space tmp;
  armoise_tree *result = NULL;

  tla_vec_init_set (tmp.alpha, alpha);
  tmp.op = op;
  
  if (ccl_hash_find (cd->H_defs, &tmp))
    result = (armoise_tree *) ccl_hash_get (cd->H_defs);

  tla_vec_clear (tmp.alpha);

  return result;
}

			/* --------------- */

static armoise_tree *
s_crt_x_in_V (struct msa2form_compilation_data *cd, ccl_ustring V)
{
  armoise_tree *proto = btf_form_crt_vector_of_ids (cd->m, cd->varnames);
  armoise_tree *Vid = NID ();
  armoise_tree *result;

  Vid->value.id_value = V;

  B_NE(result, IN, proto, Vid);

  return result;
}

/* 
 * compute the formula encoding <alpha_H, x> # <alpha_H, xhi(sigma_0)> 
 * with # to be replaced with <= or >= 
 */
armoise_tree *
btf_form_crt_H_definition (struct msa2form_compilation_data *cd, 
			   ccl_ustring Vid, tla_vec_t alpha, 
			   int op)
{
  int i, c = 0;
  armoise_tree *tmp[2];
  armoise_tree *F;
  armoise_tree *t = NULL; 
  armoise_tree *X_in_nat_power_m;
  armoise_tree *result = s_find_H (cd, alpha, op);

  if (result != NULL)
    return result;

  ccl_pre (mpz_cmp_si (alpha->den, 1) == 0);

  X_in_nat_power_m = s_crt_x_in_V (cd, Vid);
  for (i = 0; i < alpha->size; i++)
    {
      int coef =  MPZ2SI (alpha->num[i]);

      if (coef != 0)
	{
	  int sign;

	  armoise_tree *v = NID ();

	  if (coef > 0)
	    sign = 1;
	  else
	    {
	      coef = -coef;
	      sign = -1;
	    }
	  	  
	  v->value.id_value = cd->varnames[i];

	  if (coef == 1)
	    tmp[0] = v;
	  else
	    {
	      armoise_tree *a = NINT ();
	      a->value.int_value = coef;
	      B_NE (tmp[0], MUL, a, v);
	    }

	  if (t == NULL)
	    {
	      t = tmp[0];
	      if (sign < 0)
		{
		  if (coef == 1)
		    t = U_NE (NEG, t);
		  else
		    {
		      tmp[0] = U_NE (NEG, t->child);
		      tmp[0]->next = t->child->next;
		      t->child->next = NULL;
		      t->child = tmp[0];
		    }
		}
	    }
	  else if (sign == 1)
	    B_NE (t, PLUS, t, tmp[0]);
	  else
	    B_NE (t, MINUS, t, tmp[0]);
	}
    }

  tmp[0] = NINT();
  tmp[0]->value.int_value = c;

  if (op == LCA_LEQ)
    B_NE (F, LEQ, t, tmp[0]);
  else 
    B_NE (F, GEQ, t, tmp[0]);

  B_NE (result, SET, X_in_nat_power_m, F);

  {
    half_space *H = ccl_new (half_space);
    char *id = ccl_string_format_new ("H%d", ccl_hash_get_size (cd->H_defs));

    result = btf_add_definition (cd, id, result);
    ccl_delete (id);

    tla_vec_init_set (H->alpha, alpha);
    H->op = op;

    ccl_assert (!ccl_hash_find (cd->H_defs, H));
    ccl_hash_find (cd->H_defs, H);
    ccl_hash_insert (cd->H_defs, result);
  }

  return result;  
}


			/* --------------- */

armoise_tree *
btf_form_crt_PH_definition (struct msa2form_compilation_data *cd, 
			    const char *id, ccl_ustring P, 
			    tla_vec_t rho, tla_vec_t xhi, 
			    tla_vsp_t V, tla_vec_t alpha, int op)
{
  armoise_tree *Pt = NID ();
  armoise_tree *Ht = s_crt_VH (cd, rho, xhi, V, alpha, op);
  armoise_tree *result = U_NE (PAREN, Ht);

  Pt->value.id_value = P;

  B_NE (result, INTERSECTION, Pt, result);

  result = btf_form_crt_definition (id, result);

  return result;
}

			/* --------------- */

/*
 * compute the formula encoding x \in \rho(w)+B+G:
 * [
 *  \exists z_1^+, z_1^-,..., z_k^+, z_k^- 
 *   \bigvee_{b\in B} 
 *    \bigwedge_{i = 0}^m 
 *      (x-\rho(w)-b)[i] = (z_1^+-z_1^-)h_1[i]+...+(z_k^+-z_k^-)h_k[i]
 * ] 
 * where k is the dimension of G and h_j's are Hermite vectors of G.
 */
armoise_tree *
btf_form_crt_P_definition (struct msa2form_compilation_data *cd, 
			   const char *id, tla_vec_t rho_w, ccl_list *B, 
			   tla_group_t G)
{
  armoise_tree *pattern = btf_form_crt_pattern_definition (cd, B, G);
  armoise_tree *result = NID ();

  result->value.id_value = btf_form_get_def_id (pattern);

  if (rho_w != NULL && ! tla_vec_is_zero (rho_w))
    {
      armoise_tree *tmp = btf_form_vec_to_armoise_predicate (rho_w);
      
      B_NE (result, PLUS, tmp, result);
    }

  result = btf_form_crt_definition (id, result);

  return result;
}

			/* --------------- */

static armoise_tree *
s_find_pattern (struct msa2form_compilation_data *cd, ccl_list *B, 
		ccl_ustring G)
{
  pattern P; 

  P.B = B;
  P.G = G;

  if (ccl_hash_find (cd->patterns_defs, &P))
    return ccl_hash_get (cd->patterns_defs);
  return NULL;
}

			/* --------------- */

armoise_tree *
btf_form_crt_pattern_definition (struct msa2form_compilation_data *cd, 
				 ccl_list *B, tla_group_t G)
{
  int i;
  ccl_pair *p;
  int is_zero;
  pattern *P;
  char *id;
  armoise_tree *tmp;
  armoise_tree *values;
  armoise_tree *Gdef = btf_form_crt_G_definition (cd, G);
  armoise_tree *result = s_find_pattern (cd, B, btf_form_get_def_id (Gdef));

  if (result != NULL)
    return result;

  P = ccl_new (pattern);
  P->B = ccl_list_create ();
  P->G = btf_form_get_def_id (Gdef);

  is_zero = 1;
  values = NULL;

  for (i = 0, p = FIRST (B); p; p = CDR(p), i++)
    {
      tla_vec_t *pv = (tla_vec_t *) CAR (p);
      tla_vec_t *npv = ccl_new (tla_vec_t);      

      tmp = btf_form_vec_to_armoise_term (*pv);
      tla_vec_init_set (*npv, *pv);
      ccl_list_add (P->B, npv);

      tmp->next = values;
      values = tmp;

      is_zero = is_zero && tla_vec_is_zero (*pv);
      ccl_assert ( mpz_cmp_ui ((*pv)->den, 1) == 0);
    }
  ccl_assert (ccl_imply (is_zero, ccl_list_get_size (B) == 1));

  if (!is_zero)
    {
      result = REVERSE (values);
      result = U_NE (ENUMERATED_SET, result);
    }
  else
    {
      ccl_parse_tree_delete_tree (values);
    }

  /* create G sub-predicate and (rho_w+B)+G */
  tmp = NID ();   
  tmp->value.id_value = P->G;
  if (result == NULL)
    result = tmp;
  else
    B_NE (result, PLUS, result, tmp);
  ccl_assert (result != NULL);

  id = ccl_string_format_new ("PAT%d", ccl_hash_get_size (cd->patterns_defs));

  result = btf_add_definition (cd, id, result);
  ccl_delete (id);

  ccl_assert (!ccl_hash_find (cd->patterns_defs, P));

  ccl_hash_find (cd->patterns_defs, P);
  ccl_hash_insert (cd->patterns_defs, result);

  return result;  
}

			/* --------------- */

armoise_tree *
btf_form_crt_G_definition (struct msa2form_compilation_data *cd, tla_group_t G)
{
  int i;
  tla_vec_t c;
  int m = tla_group_get_size (G);
  armoise_tree *result = NULL;
    
  if (ccl_hash_find (cd->G_defs, G))
    return ccl_hash_get (cd->G_defs);

  tla_vec_init (c, m);
  
  for (i = 0; i < G->dim; i++)
    {
      armoise_tree *tmp;
      armoise_tree *Z = U_NE (INTEGERS, ZN);
      
      tla_matrix_get_column (G->Hermite, i + 1, c);
      
      tmp = btf_form_vec_to_armoise_predicate (c);
      B_NE (tmp, MUL, Z, tmp);
      
      if (result == NULL)
	result = tmp;
      else
	B_NE (result, PLUS, result, tmp);
    }
  tla_vec_clear (c);

  {
    struct tla_group_st *nG = ccl_new (struct tla_group_st);
    char *id = ccl_string_format_new ("G%d", ccl_hash_get_size (cd->G_defs));

    result = btf_add_definition (cd, id, result);
    ccl_delete (id);
  
    tla_group_init (nG, tla_group_get_size (G));
    tla_group_set (nG, G);
    ccl_assert (!ccl_hash_find (cd->G_defs, nG));
    ccl_hash_find (cd->G_defs, nG);
    ccl_hash_insert (cd->G_defs, result);
  }

  return result;
}

			/* --------------- */

armoise_tree *
btf_form_crt_vec_definition (struct msa2form_compilation_data *cd, 
			     tla_vec_t v)
{
  armoise_tree *result = NULL;

  if (ccl_hash_find (cd->vectors_defs, v))
    return ccl_hash_get (cd->vectors_defs);

  result = btf_form_vec_to_armoise_predicate (v);
  
  {
    struct tla_vec_st *nv = ccl_new (struct tla_vec_st);
    char *id = ccl_string_format_new ("rho%d", 
				      ccl_hash_get_size (cd->vectors_defs));
    result = btf_add_definition (cd, id, result);
    ccl_delete (id);
  
    tla_vec_init (nv, v->size);
    tla_vec_set (nv, v);
    ccl_assert (!ccl_hash_find (cd->vectors_defs, nv));
    ccl_hash_find (cd->vectors_defs, nv);
    ccl_hash_insert (cd->vectors_defs, result);
  }

  return result;
}

			/* --------------- */

armoise_tree *
btf_form_crt_rho_w_definition (struct msa2form_compilation_data *cd, 
			       tla_vec_t rho_w)
{
  if (tla_vec_is_zero (rho_w))
    return NULL;
  
  return btf_form_crt_vec_definition (cd, rho_w);
}

			/* --------------- */

armoise_tree *
btf_form_trivial_predicate (struct msa2form_compilation_data *cd, int is_empty)
{
  armoise_tree *result;
  armoise_tree *natpm = btf_form_crt_x_in_nat_power_m (cd);
  armoise_tree *t = is_empty ? U_NE (FALSE, ZN) : U_NE (TRUE, ZN);

  B_NE (result, SET, natpm, t);

  return result;
}

			/* --------------- */

armoise_tree *
btf_form_crt_vector_of_ids (int m, ccl_ustring *X)
{
  int i;
  armoise_tree *proto;
  armoise_tree *vars = NULL;

  for (i = m - 1; i >= 0; i--)
    {      
      armoise_tree *V = NID ();
      
      V->value.id_value = X[i];
      V->next = vars;
      vars = V;
    }

  if (m > 1)
    proto = U_NE (VECTOR, vars);
  else
    proto = vars;

  return proto;
}

			/* --------------- */

armoise_tree *
btf_form_zero_vec_predicate (int m)
{
  tla_vec_t zero;
  armoise_tree *result;

  tla_vec_init (zero, m);
  result = btf_form_vec_to_armoise_predicate (zero);
  tla_vec_clear (zero);

  return result;
}

			/* --------------- */

armoise_tree *
btf_form_crt_x_in_nat_power_m (struct msa2form_compilation_data *cd)
{
  armoise_tree *proto = btf_form_crt_vector_of_ids (cd->m, cd->varnames);
  armoise_tree *nat_power_m = btf_form_crt_nat_power_m (cd);
  armoise_tree *result;

  B_NE(result, IN, proto, nat_power_m);

  return result;
}

			/* --------------- */

static armoise_tree *
s_crt_D_power_m (struct msa2form_compilation_data *cd,
		 ccl_ustring *pid, const char *fmt, enum armoise_label label)
{
  armoise_tree *result = NULL;

  if (cd->m == 1)
    return NEW_NODE (label, CCL_PARSE_TREE_EMPTY, ZN, ZN);

  if (*pid == NULL)
    {
      int i;
      armoise_tree *t;
      armoise_tree *D_power_m = 
	NEW_NODE (label, CCL_PARSE_TREE_EMPTY, ZN, ZN);
	  
      for (t = D_power_m, i = 1; i < cd->m; i++, t = t->next)
	t->next = NEW_NODE (label, CCL_PARSE_TREE_EMPTY, ZN, ZN);
      D_power_m = U_NE (CARTESIAN_PRODUCT, D_power_m);

      if (cd != NULL)
	{
	  char *id = ccl_string_format_new (fmt, cd->m);
	  armoise_tree *def = btf_add_definition (cd, id, D_power_m);	  
	  *pid = btf_form_get_def_id (def);
	  ccl_delete (id);
	}
    }

  result = NID ();
  result->value.id_value = *pid;

  return result;
}

			/* --------------- */


armoise_tree *
btf_form_crt_nat_power_m (struct msa2form_compilation_data *cd)
{
  return s_crt_D_power_m (cd, &(cd->nat_power_m_id), "nat%d", 
			  ARMOISE_TREE_NATURALS);
}

			/* --------------- */

armoise_tree *
btf_form_crt_Z_power_m (struct msa2form_compilation_data *cd)
{
  return s_crt_D_power_m (cd, &(cd->Z_power_m_id), "int%d", 
			  ARMOISE_TREE_INTEGERS);
}

			/* --------------- */

armoise_tree *
btf_form_crt_gamma (int m, ccl_ustring *dummyX, armoise_tree *phi, int b, 
		    int index)
{
  armoise_tree *result = phi;

  if (index + 1 == m)
    {
      armoise_tree *two = NINT ();
      two->value.int_value = 2;
      B_NE (result, MUL, two, result);
    }

  if (b)
    {
      armoise_tree *one = btf_form_crt_e_m_i_predicate (m, index);

      B_NE (result, PLUS, result, one);
    }

  return result;
}

			/* --------------- */

armoise_tree *
btf_form_crt_e_m_i_predicate (int m, int i)
{
  int j;
  armoise_tree *result;
  armoise_tree *comp = NINT ();

  comp->value.int_value = (i == 0) ? 1 : 0;

  if (m > 1)
    {
      result = U_NE (CARTESIAN_PRODUCT, comp);
      
      for (j = 1; j < m; j++, comp = comp->next)
	{
	  comp->next = NINT ();
	  comp->next->value.int_value = (i == j) ? 1 : 0;      
	}
    }
  else
    {
      result = comp;
    }

  return result;
}

			/* --------------- */

armoise_tree *
btf_form_vec_to_armoise_predicate (tla_vec_t v)
{
  armoise_tree *t = btf_form_vec_to_armoise_term (v);
  armoise_tree *result = U_NE (ENUMERATED_SET, t);
  return result;
}

			/* --------------- */

armoise_tree *
btf_form_vec_to_armoise_term (tla_vec_t v)
{
  int i;
  int m = v->size;
  armoise_tree *operands = NULL;
  armoise_tree *result;

  for (i = m - 1; i >= 0; i--)
    {
      int value = MPZ2SI (v->num[i]);
      armoise_tree *cst = NINT();
      cst->value.int_value = value;
      cst->next = operands;
      operands = cst;
    }

  result = U_NE (VECTOR, operands);

  return result;
}

			/* --------------- */

static int
s_find_P1_in_formula_F (ccl_ustring P1, armoise_tree *F);

static int
s_find_P1_in_predicate_P2_without_context (ccl_ustring P1, armoise_tree *P2)
{
  int result = 0;

  switch (P2->node_type)
    {
    case ARMOISE_TREE_UNION:
    case ARMOISE_TREE_DIFFERENCE:
    case ARMOISE_TREE_XOR:
    case ARMOISE_TREE_INTERSECTION:
    case ARMOISE_TREE_PLUS:
    case ARMOISE_TREE_MINUS:
    case ARMOISE_TREE_MUL:
    case ARMOISE_TREE_DIV:
      result = s_find_P1_in_predicate_P2_without_context (P1, P2->child->next);

    case ARMOISE_TREE_LIST_ELEMENT:
    case ARMOISE_TREE_COMPLEMENT: 
    case ARMOISE_TREE_NEG:
    case ARMOISE_TREE_PAREN:
      result = (result || 
		s_find_P1_in_predicate_P2_without_context (P1, P2->child));
      break;

    case ARMOISE_TREE_NATURALS:
    case ARMOISE_TREE_INTEGERS:
    case ARMOISE_TREE_POSITIVES:
    case ARMOISE_TREE_REALS:
    case ARMOISE_TREE_INTEGER:
    case ARMOISE_TREE_ENUMERATED_SET:
      break;

    case ARMOISE_TREE_IDENTIFIER:
      result = (P2->value.id_value == P1);
      break;

    case ARMOISE_TREE_CARTESIAN_PRODUCT:
      for (P2 = P2->child; P2 && !result; P2 = P2->next)
	result = s_find_P1_in_predicate_P2_without_context (P1, P2);
      break;

    default:
      ccl_assert (IS_LABELLED_WITH (P2, SET));
      if (IS_LABELLED_WITH (P2->child, IN))
	result = s_find_P1_in_formula_F (P1, P2->child);
      result = result || s_find_P1_in_formula_F (P1, P2->child->next);
    };


  return result;
}

			/* --------------- */

static int
s_find_P1_in_predicate_P2 (ccl_ustring P1, armoise_tree *P2)
{
  int result = 0;

  if (IS_LABELLED_WITH (P2, PREDICATE))
    {
      if (IS_LABELLED_WITH (P2->child, PREDICATE_CONTEXT))
	{
	  armoise_tree *defs;

	  for (defs = P2->child->child; defs && !result; defs = defs->next)
	    result = s_find_P1_in_predicate_P2 (P1, defs->child->next);
	  P2 = P2->child->next;
	}
      else
	{
	  P2 = P2->child;
	}
    }

  if (!result)
    result = s_find_P1_in_predicate_P2_without_context (P1, P2);

  return result;
}

			/* --------------- */

static int
s_find_P1_in_formula_F (ccl_ustring P1, armoise_tree *F)
{
  int result = 0;

  switch (F->node_type)
    {
    case ARMOISE_TREE_IN:
      result = s_find_P1_in_predicate_P2_without_context (P1, F->child->next);
      break;

    case ARMOISE_TREE_OR:
    case ARMOISE_TREE_AND:
    case ARMOISE_TREE_EQUIV:
    case ARMOISE_TREE_IMPLY:
      result = s_find_P1_in_formula_F (P1, F->child->next);

    case ARMOISE_TREE_EXISTS:
    case ARMOISE_TREE_FORALL:
    case ARMOISE_TREE_NOT:
    case ARMOISE_TREE_PAREN:
      result = result || s_find_P1_in_formula_F (P1, F->child);
      break;

    case ARMOISE_TREE_EQ:
    case ARMOISE_TREE_NEQ:
    case ARMOISE_TREE_GEQ:
    case ARMOISE_TREE_LEQ:
    case ARMOISE_TREE_GT:
    case ARMOISE_TREE_LT:
    case ARMOISE_TREE_TRUE:
    case ARMOISE_TREE_FALSE:
      break;

    default :
      ccl_assert (IS_LABELLED_WITH (F,CALL));
      ccl_assert (IS_LABELLED_WITH (F->child, IDENTIFIER));
      result = (P1 == F->child->value.id_value);
      break;
    }
  return result;
}

			/* --------------- */

static void
s_delete_tree (armoise_tree *t)
{
  t->next = NULL;
  ccl_parse_tree_delete_tree (t);  
}


static void
s_subtitute_def_in_F (armoise_tree *def, armoise_tree **pF);

static void
s_subtitute_def_in_P_without_ctx (armoise_tree *def, armoise_tree **pP)
{
  switch ((*pP)->node_type)
    {
    case ARMOISE_TREE_UNION:
    case ARMOISE_TREE_DIFFERENCE:
    case ARMOISE_TREE_XOR:
    case ARMOISE_TREE_INTERSECTION:
    case ARMOISE_TREE_PLUS:
    case ARMOISE_TREE_MINUS:
    case ARMOISE_TREE_MUL:
    case ARMOISE_TREE_DIV:
      s_subtitute_def_in_P_without_ctx (def, &((*pP)->child->next));

    case ARMOISE_TREE_LIST_ELEMENT:
    case ARMOISE_TREE_COMPLEMENT: 
    case ARMOISE_TREE_NEG:
    case ARMOISE_TREE_PAREN:
      s_subtitute_def_in_P_without_ctx (def, &((*pP)->child));
      break;

    case ARMOISE_TREE_NATURALS:
    case ARMOISE_TREE_INTEGERS:
    case ARMOISE_TREE_POSITIVES:
    case ARMOISE_TREE_REALS:
    case ARMOISE_TREE_INTEGER:
    case ARMOISE_TREE_ENUMERATED_SET:
      break;

    case ARMOISE_TREE_IDENTIFIER:
      if ((*pP)->value.id_value == def->child->value.id_value)
	{
	  armoise_tree *todel = *pP;
	  armoise_tree *subst = def->child->next->child;

	  ccl_assert (!IS_LABELLED_WITH (subst, PREDICATE_CONTEXT));
	  *pP = ccl_parse_tree_duplicate (subst, NULL);
	  (*pP)->next = todel->next;
	  s_delete_tree (todel);
	}
      break;

    case ARMOISE_TREE_CARTESIAN_PRODUCT:      
      for (pP = &((*pP)->child); *pP; pP = &((*pP)->next))
	s_subtitute_def_in_P_without_ctx (def, pP);
      break;

    default:
      ccl_assert (IS_LABELLED_WITH (*pP, SET));
      if (IS_LABELLED_WITH((*pP)->child, IN))
	s_subtitute_def_in_F (def, &((*pP)->child));
      s_subtitute_def_in_F (def, &((*pP)->child->next));
      break;
    }    
}

			/* --------------- */

static void
s_subtitute_def_in_F (armoise_tree *def, armoise_tree **pF)
{
  switch ((*pF)->node_type)
    {
    case ARMOISE_TREE_IN:
      s_subtitute_def_in_P_without_ctx (def, &((*pF)->child->next));
      break;

    case ARMOISE_TREE_OR:
    case ARMOISE_TREE_AND:
    case ARMOISE_TREE_EQUIV:
    case ARMOISE_TREE_IMPLY:
      s_subtitute_def_in_F (def, &((*pF)->child->next));

    case ARMOISE_TREE_EXISTS:
    case ARMOISE_TREE_FORALL:
    case ARMOISE_TREE_NOT:
    case ARMOISE_TREE_PAREN:
      s_subtitute_def_in_F (def, &((*pF)->child));
      break;

    case ARMOISE_TREE_EQ:
    case ARMOISE_TREE_NEQ:
    case ARMOISE_TREE_GEQ:
    case ARMOISE_TREE_LEQ:
    case ARMOISE_TREE_GT:
    case ARMOISE_TREE_LT:
    case ARMOISE_TREE_TRUE:
    case ARMOISE_TREE_FALSE:
      break;

    default :
      ccl_assert (IS_LABELLED_WITH (*pF,CALL));
      ccl_assert (IS_LABELLED_WITH ((*pF)->child, IDENTIFIER));
      if (def->child->value.id_value == (*pF)->child->value.id_value)
	{
	  armoise_tree *nF;
	  armoise_tree *subst = def->child->next->child;
	  armoise_tree *vec = (*pF)->child;
	  armoise_tree *P = ccl_parse_tree_duplicate (subst, NULL);

	  ccl_assert (!IS_LABELLED_WITH (subst, PREDICATE_CONTEXT));

	  B_NE (nF, IN, vec, P);
	  s_delete_tree ((*pF)->child);
	  nF->next = (*pF)->next;
	  s_delete_tree (*pF);
	  *pF = nF;
	}
      break;
    }    
}

			/* --------------- */

static void
s_subtitute_def_in_P (armoise_tree *def, armoise_tree *P)
{
  armoise_tree **pP;

  ccl_pre (IS_LABELLED_WITH (P, PREDICATE));

  if (IS_LABELLED_WITH (P->child, PREDICATE_CONTEXT))
    {
      armoise_tree *defs;

      for (defs = P->child->child; defs; defs = defs->next)
	s_subtitute_def_in_P (def, defs->child->next);
      pP = &(P->child->next);
    }
  else
    {
      pP = &(P->child);
    }
  s_subtitute_def_in_P_without_ctx (def, pP);
}

			/* --------------- */

static int
s_is_trivial_predicate (armoise_tree *P)
{
  ccl_pre (! IS_LABELLED_WITH (P, PREDICATE));
  ccl_pre (! IS_LABELLED_WITH (P, PREDICATE_CONTEXT));

  switch (P->node_type)    
    {
    case ARMOISE_TREE_PAREN:
      return s_is_trivial_predicate (P->child);

    case ARMOISE_TREE_NATURALS:
    case ARMOISE_TREE_INTEGERS:
    case ARMOISE_TREE_POSITIVES:
    case ARMOISE_TREE_REALS:
    case ARMOISE_TREE_INTEGER:
    case ARMOISE_TREE_IDENTIFIER:
      return 1;
    default:
      return 0;
    }
}

			/* --------------- */

static int
s_is_trivial_definition (armoise_tree *def)
{
  armoise_tree *P = def->child->next;

  if (IS_LABELLED_WITH (P->child, PREDICATE_CONTEXT))
    return 0;
  return s_is_trivial_predicate (P->child);
}

			/* --------------- */

static armoise_tree *
s_remove_useless_definitions_rec (armoise_tree *def, armoise_tree *P, int *pchg)
{
  int is_useless;
  armoise_tree *result = def;
  armoise_tree **ppredicate = &(P->child->next);

  if (def->next != NULL)
    def->next = s_remove_useless_definitions_rec (def->next, P, pchg);

  if (s_is_trivial_definition (def))
    {
      armoise_tree *p;
      
      s_subtitute_def_in_P_without_ctx (def, ppredicate);	      
      for (p = def->next; p; p = p->next)
	s_subtitute_def_in_P (def, p->child->next);      
      is_useless = 1;
    }
  else
    {
      ccl_ustring id = def->child->value.id_value;
      
      is_useless = ! s_find_P1_in_predicate_P2 (id, *ppredicate);
      if (is_useless)
	{
	  armoise_tree *p;
	  
	  for (p = def->next; p && is_useless; p = p->next)
	    is_useless = ! s_find_P1_in_predicate_P2 (id, p->child->next);
	}
    }	  

  if (is_useless)
    {
      *pchg = 1;
      result = def->next;
      s_delete_tree (def);
    }

  return result;
}

			/* --------------- */

static int
s_remove_useless_definitions (armoise_tree *P)
{
  int result = 0;
  armoise_tree *defs;

  ccl_pre (IS_LABELLED_WITH (P, PREDICATE));
  ccl_pre (IS_LABELLED_WITH (P->child, PREDICATE_CONTEXT));

  defs = s_remove_useless_definitions_rec (P->child->child, P, &result);
  if (defs == NULL)
    {
      /* there no more definition in the context; we remove it. */
      armoise_tree *ctx = P->child;
      P->child = ctx->next;
      ccl_parse_tree_delete_node (ctx);
      ccl_assert (result);
    }
  else
    {
      P->child->child = defs;
    }

  return result;
}

			/* --------------- */

static int
s_useless_definitions (armoise_tree *P)
{
  int result = 0;
  armoise_tree *defs;

  ccl_pre (IS_LABELLED_WITH (P, PREDICATE));
  ccl_pre (IS_LABELLED_WITH (P->child, PREDICATE_CONTEXT));

  while (IS_LABELLED_WITH (P->child->next, IDENTIFIER))
    {
      for(defs = P->child->child; defs; defs = defs->next)
	{
	  if (defs->child->value.id_value == P->child->next->value.id_value)
	    break;
	}

      if (defs != NULL)
	s_subtitute_def_in_P_without_ctx (defs, &(P->child->next));
      else
	break;
    }

  for(defs = P->child->child; defs; defs = defs->next)
    {
      armoise_tree *subP = defs->child->next;
      
      if (! IS_LABELLED_WITH (subP->child, PREDICATE_CONTEXT))
	continue;
      if (s_useless_definitions (subP))
	result = 1;
    }
  
  if (s_remove_useless_definitions (P))
    result = 1;

  return result;
}

			/* --------------- */

armoise_tree *
btf_form_simplify_predicate (armoise_tree *P)
{
  ccl_pre (IS_LABELLED_WITH (P, PREDICATE));

  while (s_useless_definitions (P));
  
  return P;
}

			/* --------------- */

static unsigned int
s_tla_vec_hash (const tla_vec_t v)
{
  int i;
  unsigned int result = (13 * v->size + 19 * mpz_get_ui (v->den));

  for (i = 0; i < v->size; i++)
    result = ((result << 3) + 11 * mpz_get_ui (v->num[i]));

  return result;
}

			/* --------------- */

static int
s_tla_vec_cmp (const tla_vec_t v1, const tla_vec_t v2)
{
  return tla_vec_are_equal (v1, v2) ? 0 : 1;
}

			/* --------------- */

static void
s_tla_vec_delete (tla_vec_t v)
{
  tla_vec_clear (v);
  ccl_delete (v);
}

			/* --------------- */

static unsigned int
s_tla_vsp_hash (const tla_vsp_t V)
{
  int i;
  unsigned int result = 13 * V->size + 19 * V->dim;

  for (i = 0; i < V->dim; i++)
    {
      result = ((result << 3) + 
		11 * V->gen[i].i + 
		17 * s_tla_vec_hash (V->gen[i].v));
    }

  return result;
}

			/* --------------- */

static int
s_tla_vsp_cmp (const tla_vsp_t V1, const tla_vsp_t V2)
{
  return tla_vsp_are_equal (V1, V2) ? 0 : 1;
}

			/* --------------- */

static void
s_tla_vsp_delete (tla_vsp_t V)
{
  tla_vsp_clear (V);
  ccl_delete (V);
}

			/* --------------- */

static ccl_ustring *
s_string_to_unique_strings (int m, const char * const *names)
{
  int i;
  ccl_ustring *result = ccl_new_array (ccl_ustring, m);

  if (names != NULL)
    {
      for (i = 0; i < m; i++)
	result[i] = ccl_string_make_unique (names[i]);
    }
  else
    {
      char tmp[100];

      for (i = 0; i < m; i++)
	{
	  sprintf (tmp, "x%d", i);
	  result[i] = ccl_string_make_unique (tmp);
	}
    }

  return result;
}

			/* --------------- */

static int
s_tla_group_cmp (const tla_group_t G1, const tla_group_t G2)
{
  return tla_group_are_equal (G1, G2) ? 0 : 1;
}

			/* --------------- */

static void
s_tla_group_delete (tla_group_t G)
{
  tla_group_clear (G);
  ccl_delete (G);
}

			/* --------------- */

static int
s_H_cmp (const half_space *H1, const half_space *H2)
{
  if (tla_vec_are_equal (H1->alpha, H2->alpha) && 
      H1->op == H2->op)
    return 0;
  return 1;
}

			/* --------------- */

static unsigned int
s_H_hash (const half_space *H)
{
  return (19 * s_tla_vec_hash (H->alpha) + 11 * H->op);
}

			/* --------------- */

static void
s_H_delete (half_space *H)
{
  tla_vec_clear (H->alpha);
  ccl_delete (H);
}

			/* --------------- */

// Return an integral vector result such that:
// (rho + V) \cap (xhi + H) = result + (V\cap H) 

static void
s_compute_rho_plus_xhi (tla_vsp_t V, const tla_vec_t vec_alpha, tla_vec_t rho,
			tla_vec_t xhi, tla_vec_t result)
{
  tla_vec_t tmp;
  int m = tla_vsp_get_size (V);

  CCL_DEBUG_START_TIMED_BLOCK (("compute rho + xhi"));

  if (ccl_debug_is_on)
    {
      ccl_debug ("V := ");
      tla_display_vsp (CCL_LOG_DEBUG, V);
      ccl_debug ("\n");
      ccl_debug ("alpha := ");
      tla_display_vec (CCL_LOG_DEBUG, vec_alpha);
      ccl_debug ("\n");
      ccl_debug ("rho := ");
      tla_display_vec (CCL_LOG_DEBUG, rho);
      ccl_debug ("\n");
      ccl_debug ("xhi := ");
      tla_display_vec (CCL_LOG_DEBUG, xhi);
      ccl_debug ("\n");
    }
  tla_vec_init (tmp, m);
  tla_vec_sub (tmp, xhi, rho);

    
  /* Compute an integral vector y in $tmp+V$ */
  //tla_vec_t y;
  //tla_vec_init(y,m);
  //ccl_assert( tla_vsp_get_integral(V, tmp , y) );
  //tla_vsp_get_integral(V, tmp , y);
 
  /* Compute coef = <alpha, tmp> */
  mpq_t c;
  mpq_init(c);
  mpz_t coef;
  mpz_t num, den;
  mpz_init(coef);
  mpz_init(num);
  mpz_init(den);
  tla_vec_scalar_product( c , vec_alpha , tmp );

  mpq_get_num(num,c);
  mpq_get_den(den,c);
  mpz_fdiv_q(coef,num,den);

  /*
  ccl_debug("num(c)=%d\n", mpz_get_si (coef));

  ccl_debug("den(c)=%d\n", mpz_get_si (coef));
  mpz_neg(coef,coef);*/

  mpq_clear(c);
  mpz_clear(num);
  mpz_clear(den);
  
  
  /* Compute $G=V \cap Z^n$ */
  tla_group_t G;
  tla_group_init(G,m);
  tla_group_Z_cap_vsp(G,V);
  
  /*
   * Compute  $x \in G$ is a vector such that $<\alpha,x> \leq coef and 
   * $<\alpha,x>$ is maximal 
   */
  tla_vec_t x;
  tla_vec_init(x,m);
  ccl_assert( tla_group_alpha_min(G, vec_alpha, coef, x) );
  tla_group_alpha_min(G, vec_alpha, coef, x);
 
  
  /* result = x + rho */
  tla_vec_add (result , x , rho);

  if (ccl_debug_is_on)
    {
      char *s;
      mpq_t c;
      mpq_init(c);

      ccl_debug ("tmp := ");
      tla_display_vec (CCL_LOG_DEBUG, tmp);
      ccl_debug ("\n");

      tla_vec_scalar_product( c , vec_alpha , tmp );
      s = mpq_get_str (NULL, 10, c);
      ccl_debug ("c := %s\n", s);
      free (s);

      tla_vec_scalar_product( c , vec_alpha , x );
      s = mpq_get_str (NULL, 10, c);
      ccl_debug ("c := %s\n", s);
      free (s);
      mpq_clear (c);

      ccl_debug ("Result := ");
      tla_display_vec (CCL_LOG_DEBUG, result);
      ccl_debug ("\n");
    }

  tla_vec_clear(x);
  tla_group_clear(G);
  mpz_clear(coef);
  //tla_vec_clear(y);
  tla_vec_clear (tmp);

  CCL_DEBUG_END_TIMED_BLOCK ();
}

			/* --------------- */

static int
s_pattern_cmp (const pattern *P1, const pattern *P2)
{
  ccl_pair *p1;
  ccl_pair *p2;

  if (P1 == P2)
    return 0;

  if (ccl_list_get_size (P1->B) != ccl_list_get_size (P2->B) || P1->G != P2->G)
    return 1;

  for (p1 = FIRST (P1->B), p2 = FIRST (P2->B); p1 && p2; 
       p1 = CDR (p1), p2 = CDR (p2))
    {
      tla_vec_t *pv1 = CAR (p1);
      tla_vec_t *pv2 = CAR (p2);

      if (! tla_vec_are_equal (*pv1, *pv2))
	return 1;
    }

  return 0;
}

			/* --------------- */

static unsigned int
s_pattern_hash (const pattern *P)
{
  ccl_pair *p;
  unsigned int result = (uintptr_t) P->G;

  for (p = FIRST (P->B); p; p = CDR (p))
    {
      tla_vec_t *pv = CAR (p);

      result = (result << 3) + tla_vec_hash (*pv);
    }

  return result;
}

			/* --------------- */

static void
s_pattern_delete (pattern *P)
{
  ccl_pair *p;
  
  for (p = FIRST (P->B); p; p = CDR (p))
    {
      tla_vec_t *pv = CAR (p);
      tla_vec_clear (*pv);
      ccl_delete (pv);
    }
  ccl_list_delete (P->B);
  ccl_delete (P);
}
