/*
 * batof-sfp.h -- Stallings' folding process
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * Implementation of the algorithm proposed by Nicholas W. M. Touikan in
 * the article:
 *
 * <em>A fast algorithm for Stallings' folding process.</em>, in the 
 * International Journal of Algebra and Computation, Vol. 16, No. 6 (2006),
 * pages 1031-1045, World Scientific Publishing Company.
 *
 */
#ifndef __BATOF_SFP_H__
# define __BATOF_SFP_H__

#define BTF_SFP_FORWARD  0x01
#define BTF_SFP_BACKWARD 0x02
#define BTF_SFP_BOTHWARD (BTF_SFP_BACKWARD|BTF_SFP_FORWARD)

/*!
 *
 */
extern void
btf_stalling_folding_process (int sfp_mode,
			      int nb_nodes, int nb_edges, int **edges,
			      int *p_nb_classes,
			      int **p_node_to_class_map,
			      int ***p_class_to_nodes_map);

#endif /* ! __BATOF_SFP_H__ */
