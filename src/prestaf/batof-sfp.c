/*
 * batof-sfp.c -- Stallings' folding process
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include "batof-sfp.h"

#define ENABLE_STAT_FILE 1

#if ENABLE_STAT_FILE 
# include <time.h>
#endif 

#define SRC 0
#define TGT 1

typedef struct vertex_st vertex; 
typedef struct edge_st edge;

			/* --------------- */

struct edge_st 
{  
  int label; /* label of the edge */
  vertex *src; /* initial node */
  vertex *tgt; /* terminal node */
  edge *prev[2]; /* 'previous' link in initial and terminal lists */
  edge *next[2]; /* 'next' link in initial and terminal lists */
};

			/* --------------- */

struct vertex_st 
{
  int card; /* Cardinality of the class. Used at the end of the algorithm to
	     * build the class_to_state map */
  int rank; /* The upper bound of the depth of the tree rooted at this 
	     * node. This field is also used to assign to each root its index 
	     * in the set of classes.
	     */
  vertex *parent; /* parent link */
  vertex *prev; /* 'previous' link in the unfolded list */
  vertex *next; /* 'next' link in the unfolded list */

  edge edgelists; /* Incidence lists. This dummy edge is used as the sentinel 
		   * of circular double-linked lists of edges. We use two 
		   * incidence lists one for incoming edges and one for 
		   * outgoing edges.
		   */
};

			/* --------------- */

static void
s_init_folding_data_struct (int sfp_mode, int nb_nodes, int nb_edges, 
			    int **edges, vertex **p_V, edge **p_E, 
			    vertex *unfolded_list);

static int
s_make_folding (vertex *unfolded_list);

static void
s_compute_indexes (vertex *nodes, int nb_nodes, int nb_classes, 
		   int **p_node_to_class, int ***p_class_to_nodes);

static void
s_check_folded_status (vertex *u, vertex *unfolded_list);

			/* --------------- */

void
btf_stalling_folding_process (int sfp_mode, int nb_nodes, int nb_edges, 
			      int **edges,
			      int *p_nb_classes,
			      int **p_node_to_class_map,
			      int ***p_class_to_nodes_map)
{
  vertex unfolded_list;
  vertex *V = NULL;
  edge *E = NULL;

  ccl_pre (sfp_mode != 0);

  s_init_folding_data_struct (sfp_mode, nb_nodes, nb_edges, edges, &V, &E, 
			      &unfolded_list);
  *p_nb_classes = nb_nodes - s_make_folding (&unfolded_list);
  ccl_delete (E);
  
  s_compute_indexes (V, nb_nodes, *p_nb_classes, p_node_to_class_map,
		     p_class_to_nodes_map);
  ccl_delete (V);
}

			/* --------------- */


static void
s_init_folding_data_struct (int sfp_mode, int nb_nodes, int nb_edges, 
			    int **edges, vertex **p_V, edge **p_E, 
			    vertex *unfolded_list)
{
  int i;  
  vertex *v = *p_V = ccl_new_array (vertex, nb_nodes);
  edge *e = *p_E = ccl_new_array (edge, nb_edges);

  unfolded_list->prev = unfolded_list->next = unfolded_list;

  for (i = 0; i < nb_nodes; i++)
    {
      v->rank = 0;
      v->card = 1;
      v->parent = v;
      v->edgelists.prev[SRC] = v->edgelists.next[SRC] = &v->edgelists;
      v->edgelists.prev[TGT] = v->edgelists.next[TGT] = &v->edgelists;
      v++;
    }

  for (i = 0; i < nb_edges; i++)
    {
      vertex *src = *p_V + edges[i][0];
      int label = edges[i][1];
      vertex *tgt = *p_V + edges[i][2];

      e->label = label;
      e->src = src;
      e->tgt = tgt;

      if ((sfp_mode & BTF_SFP_FORWARD) != 0)
	{
	  edge *sentinel = &src->edgelists;
	  e->next[SRC] = sentinel;
	  e->prev[SRC] = sentinel->prev[SRC];
	  sentinel->prev[SRC] = e;
	  e->prev[SRC]->next[SRC] = e;
	}
      else
	{
	  e->next[SRC] = e;
	  e->prev[SRC] = e;
	}

      if ((sfp_mode & BTF_SFP_BACKWARD) != 0)
	{
	  edge *sentinel = &tgt->edgelists;
	  e->next[TGT] = sentinel;
	  e->prev[TGT] = sentinel->prev[TGT];
	  sentinel->prev[TGT] = e;
	  e->prev[TGT]->next[TGT] = e;
	}
      else
	{
	  e->next[TGT] = e;
	  e->prev[TGT] = e;
	}

      e++;
    }

  for (i = 0; i < nb_nodes; i++)
    s_check_folded_status (*p_V + i, unfolded_list);
}


			/* --------------- */

static int
s_get_same_edges_with_incidence (edge *sentinel, int incidence, edge **e)
{
  edge *aux;

  e[0] = sentinel->next[incidence]; 
  if (e[0] == sentinel) /* empty list */
    return 0;
  e[1] = e[0]->next[incidence]; 
  if (e[1] == sentinel) /* singleton list */
    return 0;

  if (e[0]->label == e[1]->label)
    return 1;

  aux = e[1]->next[incidence]; 
  if (aux == sentinel) /* couple case with different labels*/
    return 0;

  /* here there exists more than 3 edges and thus there exists 2 edges with the 
   * same label */
  if (e[0]->label == aux->label)
    e[1] = aux;
  else 
    e[0] = aux;

  ccl_post (e[0]->label == e[1]->label);

  return 1;

}

			/* --------------- */

static int
s_get_same_edges (edge *sentinel, edge **e)
{
  int result = s_get_same_edges_with_incidence (sentinel, SRC, e);
  
  if (! result)
    result = s_get_same_edges_with_incidence (sentinel, TGT, e);

  if (!result)
    e[0] = e[1] = NULL;

  return result;
}

			/* --------------- */

static int
s_is_unfolded (vertex *v)
{
  edge *e[2];

  return s_get_same_edges (&v->edgelists, e);
}

			/* --------------- */

static vertex *
s_get_representative (vertex *v)
{
  if (v->parent != v)
    v->parent = s_get_representative (v->parent);

  return v->parent;
}

			/* --------------- */

static void
s_concat_edge_lists_for_incidence (vertex *u, vertex *v, int incidence)
{
  if (u->edgelists.next[incidence] == &u->edgelists)
    {
      if (v->edgelists.next[incidence] != &v->edgelists)
	{
	  u->edgelists.prev[incidence] = v->edgelists.prev[incidence];
	  u->edgelists.next[incidence] = v->edgelists.next[incidence];
	  u->edgelists.prev[incidence]->next[incidence] = &u->edgelists;
	  u->edgelists.next[incidence]->prev[incidence] = &u->edgelists;
	  v->edgelists.prev[incidence] = &v->edgelists;
	  v->edgelists.next[incidence] = &v->edgelists;
	}
    }
  else if (v->edgelists.next[incidence] != &v->edgelists)
    {
      edge *first_v = v->edgelists.next[incidence];
      edge *last_v = v->edgelists.prev[incidence];
      edge *last_u = u->edgelists.prev[incidence];

      last_u->next[incidence] = first_v;
      first_v->prev[incidence] = last_u;

      last_v->next[incidence] = &u->edgelists;
      u->edgelists.prev[incidence] = last_v;

      v->edgelists.prev[incidence] = &v->edgelists;
      v->edgelists.next[incidence] = &v->edgelists;
    }
}

			/* --------------- */
static void
s_concat_edge_lists (vertex *u, vertex *v)
{
  s_concat_edge_lists_for_incidence (u, v, SRC);
  s_concat_edge_lists_for_incidence (u, v, TGT);
}

			/* --------------- */

static vertex *
s_merge_classes (vertex *r1, vertex *r2)
{
  vertex *r = r1;
  vertex *nr = r2;

  ccl_assert (r1->parent == r1);
  ccl_assert (r2->parent == r2);

  ccl_assert (r1 != r2);

  r1->card += r2->card;
  r2->card = r1->card;
  if (r1->rank > r2->rank) { r2->parent = r1; }
  if (r2->rank > r1->rank) { r1->parent = r2; r = r2; nr = r1; }
  else 
    {
      r2->parent = r1;
      r1->rank++;
    }

  s_concat_edge_lists (r, nr);
  if (nr->next != NULL)
    {
      nr->next->prev = nr->prev;
      nr->prev->next = nr->next;
      nr->prev = NULL;
      nr->next = NULL;
    }

  return r;
}

			/* --------------- */


static void 
s_remove_edge_from_lists (edge *e)
{
  e->next[SRC]->prev[SRC] = e->prev[SRC];
  e->prev[SRC]->next[SRC] = e->next[SRC];
  e->next[TGT]->prev[TGT] = e->prev[TGT];
  e->prev[TGT]->next[TGT] = e->next[TGT];
}

			/* --------------- */

static void
s_case_two_self_loops (edge **e, vertex *unfolded_list)
{
  s_remove_edge_from_lists (e[1]);
  s_check_folded_status (e[0]->src, unfolded_list);
}

			/* --------------- */

static void
s_case_one_loop (edge *not_loop_edge, vertex *unfolded_list)
{
  vertex *u = not_loop_edge->src;
  vertex *v = not_loop_edge->tgt;

  u = s_merge_classes (u, v);
  s_remove_edge_from_lists (not_loop_edge);
  s_check_folded_status (u, unfolded_list);
}

			/* --------------- */

static void
s_case_V (vertex *v, vertex *u, vertex *w, edge *to_remove, 
	  vertex *unfolded_list)
{
  u = s_merge_classes (u, w);
  s_remove_edge_from_lists (to_remove);
  s_check_folded_status (u, unfolded_list);
  s_check_folded_status (v, unfolded_list);
}

			/* --------------- */

static void
s_case_parallel (vertex *v, vertex *u, edge *to_remove, vertex *unfolded_list)
{
  s_remove_edge_from_lists (to_remove);
  s_check_folded_status (u, unfolded_list);
  s_check_folded_status (v, unfolded_list);
}

			/* --------------- */

static int
s_make_folding (vertex *unfolded_list)
{
  int nb_merges = 0;

  while (unfolded_list->next != unfolded_list)
    {
      int i;
      vertex *x = unfolded_list->next;
      vertex *u[2];
      vertex *v[2];
      edge *e[2] = { NULL, NULL };

      s_get_same_edges (&x->edgelists, e);

      ccl_assert (e[0] != NULL && e[1] != NULL);
      ccl_assert (e[0] != e[1]);

      for (i = 0; i < 2; i++)
	{
	  e[i]->src = u[i] = s_get_representative (e[i]->src);
	  e[i]->tgt = v[i] = s_get_representative (e[i]->tgt);
	}

      ccl_assert (x == u[0] || x == u[1] || x == v[0] || x == v[1]);

      if (u[0] == v[0])
	{
	  if (u[1] == v[1])
	    {
	      ccl_assert (u[0] == u[1]);
	      s_case_two_self_loops (e, unfolded_list);
	    }
	  else 
	    {
	      ccl_assert (u[1] == u[0] || v[1] == u[0]);
	      s_case_one_loop (e[1], unfolded_list);
	      nb_merges++;
	    }
	}
      else if (u[1] == v[1])
	{
	  ccl_assert (u[1] == u[0] || u[1] == v[0]);
	  s_case_one_loop (e[0], unfolded_list);
	  nb_merges++;
	}
      else if (u[0] == u[1])
	{
	  if (v[0] == v[1])
	    s_case_parallel (u[0], v[0], e[1], unfolded_list);
	  else
	    {
	      s_case_V (u[0], v[0], v[1], e[1], unfolded_list);
	      nb_merges++;
	    }
	}
      else
	{
	  ccl_assert (v[0] == v[1]);
	  s_case_V (v[0], u[0], u[1], e[1], unfolded_list);
	  nb_merges++;
	}
    }

  return nb_merges;
}

			/* --------------- */

static void
s_compute_indexes (vertex *nodes, int nb_nodes, int nb_classes, 
		   int **p_node_to_class, int ***p_class_to_nodes)
{
  int i;
  vertex *v = nodes;
  int C;
  int **p;
  int *array_for_classes;

  for (i = 0, C = 0, v = nodes; i < nb_nodes; i++, v++)
    {
      if (v->parent == v)
	v->rank = C++;
    }
  ccl_assert (C == nb_classes);

  *p_node_to_class = ccl_new_array (int, 2 * nb_nodes + nb_classes);
  p = ccl_new_array (int *, nb_classes);
  array_for_classes = (*p_node_to_class) + nb_nodes;
  *p_class_to_nodes = ccl_new_array (int *, nb_classes);

  for (i = 0, v = nodes; i < nb_nodes; i++, v++)
    {
      vertex *r = s_get_representative (v);
      int cindex = r->rank;

      (*p_node_to_class)[i] = cindex;

      if (p[cindex] == NULL)
	{
	  (*p_class_to_nodes)[cindex] = p[cindex] = array_for_classes;
	  p[cindex][r->card] = -1;
	  array_for_classes += r->card + 1;
	}

      *(p[cindex]) = i;
      p[cindex]++;
    }
  ccl_delete (p);
}

			/* --------------- */

static void
s_check_folded_status (vertex *u, vertex *unfolded_list)
{
  if (s_is_unfolded (u))
    {
      if (u->prev == NULL && u->next == NULL)
	{	  
	  u->next = unfolded_list;
	  u->prev = unfolded_list->prev;
	  unfolded_list->prev = u;
	  u->prev->next = u;      
	}
    }
  else if (u->prev != NULL && u->next != NULL)
    {
      u->next->prev = u->prev;
      u->prev->next = u->next;
      u->prev = NULL;
      u->next = NULL;
    }
}

			/* --------------- */

