/*
 * batof-invgroup.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <time.h>
#include <ccl/ccl-memory.h>
#include "prestaf-batof-p.h"

# define STATE(mn,c,k) ((c)*(mn)+(k))
# define STATE_K(mn,s) ((s)%(mn))
# define STATE_C(mn,s) ((s)/(mn))

static void
s_make_bfs (sataf_ea *ea, int m, int mn, int q, int *pred, ccl_list *queue,
	    ccl_bittable *visited, ccl_bittable *letters);

static sataf_ea *
s_reverse_transitions (sataf_ea *ea);

static int
s_compute_rhos (int q0, const int *pred, tla_vec_t *rho_loop, 
		tla_vec_t *rho_q0_q, ccl_bittable *u2set, tla_vec_t *tmp, 
		int m, int mn, 
		int nb_classes, sataf_ea *ea, sataf_ea *iea, ccl_list *queue, 
		int nb_principals, tla_group_t G, ccl_bittable *letters);

static int
s_nb_principal_states_with_0_eyes (int q0, sataf_ea *ea, int m);

static int
s_is_completed (tla_group_t G, int nb_principals);

static int
s_add_generator_for_state (int s, int b, int q0, sataf_ea *ea, const int *pred, 
			   tla_group_t G, int mn, int nb_principals, 
			   ccl_bittable *u2set, tla_vec_t *rho_half, 
			   tla_vec_t *rho_full, tla_vec_t *tmp);

			/* --------------- */

void
btf_compute_invariant_group (tla_group_t G, tla_vsp_t VSP, sataf_ea *ea, 
			     int q0, int m, int n0, int n)
{
  int s;
  int mn;
  int *pred;
  int stop;
  ccl_list *queue;
  ccl_bittable *visited;
  ccl_bittable *letters;
  tla_vec_t tmp[2];
  tla_vec_t *rho_full_u2;
  tla_vec_t *rho_half_u2;
  ccl_bittable *u2set;
  int nb_classes = sataf_ea_get_nb_states (ea);
  int Q0;
  sataf_ea *iea;
  int nb_principals;

  CCL_DEBUG_START_TIMED_BLOCK (("compute invariant group"));
  
  if (sataf_ea_is_one (ea))
    {
      int i;
      
      if (ccl_debug_is_on)
	ccl_debug ("trivial ONE component. return basis vectors.\n");
      tla_vec_init (tmp[0], m);
      for (i = 0; i < m; i++)
	{
	  tla_vec_set_unity (tmp[0], i);
	  tla_group_add_generator (G, G, tmp[0]);
	}
      tla_vec_clear (tmp[0]);
      goto end;
    }

  mn = m * n;
  Q0 = STATE (mn, q0, 0);

  tla_vec_init (tmp[0], m);
  tla_vec_init (tmp[1], m);
  pred = ccl_new_array (int, mn * (nb_classes + 1));
  queue = ccl_list_create ();
  visited = ccl_bittable_create (mn * nb_classes);
  letters = ccl_bittable_create (2 * mn * nb_classes);

  rho_full_u2 = ccl_new_array (tla_vec_t, mn*nb_classes);
  rho_half_u2 = ccl_new_array (tla_vec_t, mn*nb_classes);
  u2set = ccl_bittable_create (2*mn*nb_classes);

  /* initialize the result */
  tla_group_Z_cap_vsp (G, VSP);
  tla_group_z_mul_ui (G, G, n0);

  CCL_DEBUG_START_TIMED_BLOCK (("make first BFS"));
  s_make_bfs (ea, m, mn, Q0, pred, queue, visited, letters);
  CCL_DEBUG_END_TIMED_BLOCK ();

  nb_principals = s_nb_principal_states_with_0_eyes (q0, ea, m);
  if (ccl_debug_is_on)
    ccl_debug ("nb principal states = %d\n", nb_principals);

  stop = s_is_completed (G, nb_principals);

  if (!stop)
    {
      iea = s_reverse_transitions (ea);
      CCL_DEBUG_START_TIMED_BLOCK (("compute rho(s->q0) and rho(q0->s->q0)"));
      stop = s_compute_rhos (Q0, pred, rho_full_u2, rho_half_u2, u2set, tmp, m,
			     mn, nb_classes, ea, iea, queue, nb_principals, G,
			     letters);
      CCL_DEBUG_END_TIMED_BLOCK ();
      sataf_ea_del_reference (iea);
    }

  ccl_list_delete (queue);
  if (! stop)
    {
      int i;

      CCL_DEBUG_START_TIMED_BLOCK (("actually compute group"));
      i = ccl_bittable_get_first (letters);
      for (; i >= 0 && !stop; i = ccl_bittable_get_next (letters, i))
	{
	  int b = i & 0x1;
	  int s = i >> 1;
	  stop = s_add_generator_for_state (s, b, Q0, ea, pred, G, mn, 
					    nb_principals, u2set, 
					    rho_half_u2, rho_full_u2, tmp);
	}
      CCL_DEBUG_END_TIMED_BLOCK ();
    }

  /* clean up step */
  tla_vec_clear (tmp[0]);
  tla_vec_clear (tmp[1]);
  ccl_delete (pred);
  ccl_bittable_delete (visited);
  ccl_bittable_delete (letters);

  for (s = 0; s < mn * nb_classes; s++)
    {
      if (ccl_bittable_has (u2set, 2 * s))
	tla_vec_clear (rho_half_u2[s]);
      if (ccl_bittable_has (u2set, 2 * s + 1))
	tla_vec_clear (rho_full_u2[s]);
    }
  ccl_bittable_delete (u2set);
  ccl_delete (rho_full_u2);
  ccl_delete (rho_half_u2);

 end:
  if (ccl_debug_is_on)
    {
      if (m < 5)
	{
	  ccl_debug ("invariant group is:\n");
	  tla_display_group(CCL_LOG_DEBUG, G);
	  ccl_debug ("\n");
	}
    }
  CCL_DEBUG_END_TIMED_BLOCK ();
}

/*!
 * Computes shortest paths from state q to any other accessible state.
 *
 * \param ea the automaton where paths are computed.
 * \param mn the upper bound of the counter used to unfold the automaton
 * \param q the starting state
 * \param pred an array encoding the predecessor relation allowing to go back 
 *        from any state s to q
 * \param queue a re-usable FIFO for BFS algorithm
 * \param visited a re-usable vector of bits indicating whether a state has 
 *        already been visited.
 */
static void
s_make_bfs (sataf_ea *ea, int m, int mn, int q, int *pred, ccl_list *queue,
	    ccl_bittable *visited, ccl_bittable *letters)
{
  int i;
  int nb_classes = sataf_ea_get_nb_states (ea);

  ccl_list_add (queue, (void *) (intptr_t) q);
  ccl_bittable_set (visited, q);
  ccl_bittable_set (letters, 2 * q);
  ccl_bittable_set (letters, 2 * q + 1);

  for (i = 0; i <mn*nb_classes; i++)
    pred[i] = -1;

  while (!ccl_list_is_empty (queue))
    {
      int b;
      int s = (intptr_t) ccl_list_take_first (queue);
      int c = STATE_C(mn,s);
      int k = STATE_K(mn,s);
		
      ccl_assert (s == q || pred[s] >= 0);

      for (b = 0; b < 2; b++)
	{
	  int ksucc;
	  int ssucc;
	  int csucc = sataf_ea_get_successor (ea, c, b);

	  if (sataf_ea_is_exit_state (csucc))
	    continue;

	  csucc = sataf_ea_decode_succ_state (csucc);
	  ksucc = (k + 1) % mn;
	  ssucc = STATE (mn, csucc, ksucc);
	  	  
	  if (ssucc == q && pred[ssucc] == -1)
	    pred[ssucc] = (s << 1) + b;
	  else if (!ccl_bittable_has (visited, ssucc))
	    {
	      ccl_assert (pred[ssucc] == -1);
	      pred[ssucc] = (s << 1) + b;
	      ccl_bittable_set (visited, ssucc);
	      ccl_list_add (queue, (void *) (intptr_t) ssucc);
	      ccl_bittable_set (letters, 2 * ssucc);
	      ccl_bittable_set (letters, 2 * ssucc + 1);
	    }
	}
    }	
}

			/* --------------- */

static int
s_compute_rhos (int q0, const int *pred, tla_vec_t *rho_loop, 
		tla_vec_t *rho_q0_q, 
		ccl_bittable *u2set, tla_vec_t *tmp, int m, int mn, 
		int nb_classes, sataf_ea *ea, sataf_ea *iea, ccl_list *queue,
		int nb_principals, tla_group_t G, ccl_bittable *letters)
{
  ccl_bittable *visited = ccl_bittable_create (mn * nb_classes);
  ccl_list *states = ccl_list_create ();  
  int h = sataf_ea_get_nb_states (iea) - 1;
  int G_is_done = 0;

  ccl_list_add (queue, (void *) (intptr_t) q0);
  ccl_bittable_set (visited, q0);
  tla_vec_init (rho_q0_q[q0], m);
  ccl_bittable_set (u2set, 2 * q0);
  
  while (! ccl_list_is_empty (queue) && ! G_is_done)
    {
      int b;
      int q = (intptr_t) ccl_list_take_first (queue);
      int c = STATE_C(mn,q);
      int k = STATE_K(mn,q);
      
      ccl_assert (ccl_bittable_has (u2set, 2 * q));
      if (! ccl_bittable_has (u2set, 2 * q + 1))
	{
	  int qp = q;
	  
	  tla_vec_set (tmp[0], rho_q0_q[q]);	  
	  ccl_list_add (states, (void *) (intptr_t) qp);
	  do
	    {
	      b = (pred[qp] & 0x1);
	      qp = (pred[qp] >> 1);
	      
	      tla_vec_gamma (tmp[1], tmp[0], b);
	      tla_vec_set (tmp[0], tmp[1]);
	      if (! ccl_bittable_has (u2set, 2 * qp))
		{
		  tla_vec_init_set (rho_q0_q[qp], tmp[0]);
		  ccl_bittable_set (u2set, 2 * qp);
		}

	      if (! ccl_bittable_has (u2set, 2 * qp + 1))
		ccl_list_add (states, (void *) (intptr_t) qp);
	    }
	  while (qp != q0);

	  while (! ccl_list_is_empty (states))	    
	    {
	      int s = (intptr_t) ccl_list_take_first (states);
	      if  (! ccl_bittable_has (u2set, 2 * s + 1))
		{
		  ccl_bittable_set (u2set, 2 * s + 1);
		  tla_vec_init_set (rho_loop[s], tmp[0]);
		}
	    }
	}

      for (b = 0; b < 2 && ! G_is_done; b++)
	{
	  int ssucc;
	  int csucc = sataf_ea_get_successor (iea, c, b);
	  int ksucc = (mn + k - 1) % mn;
	  
	  ccl_assert (! sataf_ea_is_exit_state (csucc));
	  csucc = sataf_ea_decode_succ_state (csucc);
	  ssucc = STATE (mn, csucc, ksucc);

	  if (ccl_bittable_has (visited, ssucc) || csucc == h)
	    continue;

	  ccl_bittable_set (visited, ssucc);
	  ccl_list_add (queue, (void *) (intptr_t) ssucc);

	  if (! ccl_bittable_has (u2set, 2 * ssucc))
	    {
	      ccl_bittable_set (u2set, 2 * ssucc);
	      tla_vec_init (rho_q0_q[ssucc], m);
	      tla_vec_gamma (rho_q0_q[ssucc], rho_q0_q[q], b);
	    }
	  else if (ccl_bittable_has (u2set, 2 * ssucc + 1))
	    {
	      G_is_done = 
		s_add_generator_for_state (ssucc, b, q0, ea, pred, G, mn, 
					   nb_principals, u2set, rho_q0_q, 
					   rho_loop, tmp);
	      ccl_bittable_unset (letters, 2 * ssucc + b);
	    }
	}
    }

  ccl_list_delete (states);
  ccl_bittable_delete (visited);

  return G_is_done;
}

			/* --------------- */

static sataf_ea *
s_reverse_transitions (sataf_ea *ea)
{
  int s;
  sataf_ea *result;
  int nb_states = sataf_ea_get_nb_states (ea);
  uint32_t *trans = ccl_new_array (uint32_t, 2 * (nb_states + 1));
  uint8_t *is_final = ccl_new_array (uint8_t, nb_states+1);

  for (s = 0; s < 2 * nb_states; s++)
    trans[s] = 0xFFFFFFFF;

  for (s = 0; s < nb_states; s++)
    {
      int b;
      int es = sataf_ea_encode_succ_as_local_state (s);

      is_final[s] = sataf_ea_is_final (ea, s);
      for (b = 0; b < 2; b++)
	{
	  int esucc = sataf_ea_get_successor (ea, s, b);

	  if (sataf_ea_is_local_state (esucc))
	    {
	      int succ = sataf_ea_decode_succ_state (esucc);
	      ccl_assert (trans[2 * succ + b] == 0xFFFFFFFF);
	      trans[2 * succ + b] = es;
	    }
	}
    }

  trans[2 * nb_states] = trans[2 * nb_states + 1] = 
    sataf_ea_encode_succ_as_local_state (nb_states);
  for (s = 0; s < 2 * nb_states; s++)
    {
      if (trans[s] == 0xFFFFFFFF)
	trans[s] = trans[2 * nb_states];
    }

  result = sataf_ea_create_with_arrays (nb_states + 1, 0, 2, is_final, trans);
  ccl_delete (trans);
  ccl_delete (is_final);

  return result;
}

			/* --------------- */

static ccl_bittable *
s_on_0_loop (sataf_ea *ea)
{
  int i;
  int nb_states = sataf_ea_get_nb_states (ea);
  ccl_bittable *done = ccl_bittable_create (nb_states);
  ccl_bittable *on_stack = ccl_bittable_create (nb_states);
  ccl_bittable *on_0_loop = ccl_bittable_create (nb_states);  

  for (i = 0; i < nb_states; i++)
    {
      int s = i;
      int stop = 0;

      if (ccl_bittable_has (done, s))
	  continue;
      ccl_bittable_clear (on_stack);
      ccl_bittable_set (on_stack, s);

      while (! stop)
	{
	  s = sataf_ea_get_successor (ea, s, 0);
	  if (sataf_ea_is_exit_state (s))
	    {
	      int j;
	      for (j = ccl_bittable_get_first (on_stack);
		   j >= 0;
		   j = ccl_bittable_get_next (on_stack, j))
		ccl_bittable_set (done, j);
	      stop = 1;
	    }
	  else
	    {
	      s = sataf_ea_decode_succ_state (s);	      
	      
	      if (!ccl_bittable_has (on_stack, s))
		ccl_bittable_set (on_stack, s);
	      else
		{
		  int j;
		  int saux = s;

		  do
		    {
		      saux = sataf_ea_get_successor (ea, saux, 0);
		      ccl_assert (!sataf_ea_is_exit_state (saux));
		      saux = sataf_ea_decode_succ_state (saux);
		      ccl_bittable_set (on_0_loop, saux);
		    }
		  while (saux != s);
		  for (j = ccl_bittable_get_first (on_stack);
		       j >= 0;
		       j = ccl_bittable_get_next (on_stack, j))
		    ccl_bittable_set (done, j);
		  stop = 1;
		}
	    }
	}
    }
  ccl_bittable_delete (done);
  ccl_bittable_delete (on_stack);

  return on_0_loop;
}

			/* --------------- */

static int
s_nb_principal_states_with_0_eyes (int q0, sataf_ea *ea, int m)
{
  int i;
  ccl_bittable *on_0_loop;
  int result = 0;
  int nb_states = sataf_ea_get_nb_states (ea);
  int *dist = ccl_new_array (int, nb_states);
  ccl_list *Q = ccl_list_create ();

  for (i = 0; i < nb_states; i++)
    dist[i] = -1;
  
  dist[q0] = 0;
  ccl_list_add (Q, (void *) (intptr_t) q0);
  while (!ccl_list_is_empty (Q))
    {
      int b;
      int s = (intptr_t) ccl_list_take_first (Q);

      for (b = 0; b < 2; b++)	
	{
	  int succ = sataf_ea_get_successor (ea, s, b);
	  if (sataf_ea_is_exit_state (succ))
	    continue;
	  succ = sataf_ea_decode_succ_state (succ);
	  if (dist[succ] == -1)
	    {
	      dist[succ] = dist[s] + 1;
	      ccl_list_add (Q, (void *) (intptr_t) succ);
	    }
	}
    }
  ccl_list_delete (Q);
  on_0_loop = s_on_0_loop (ea);
  for (i = 0; i < nb_states; i++)
    {
      if ((dist[i] % m) == 0 && ccl_bittable_has (on_0_loop, i))
	result++;
    }
  ccl_bittable_delete (on_0_loop);
  ccl_delete (dist);

  return result;
}

			/* --------------- */

static int
s_is_completed (tla_group_t G, int nb_principals)
{
  int k;
  int result;
  mpz_t p;
  int size = tla_group_get_size (G);
  int m = size;
  int n = m+1;
  tla_matrix_t S;

  tla_matrix_init (S, m, n);
  tla_matrix_compute_snf (G->Hermite, S, NULL, NULL, NULL, NULL, NULL);

  mpz_init_set_ui (p, 1);
  for (k = 1; k <= tla_group_get_dim (G); k++)
    mpz_mul (p, p, MM (S, k, k));
  result = mpz_cmp_si (p, nb_principals) <= 0;
  /*
   * seems to false e.g. { x | exists y(x-5y = 0 or x -5y = 1)};
   ccl_assert (ccl_imply (result, mpz_cmp_si (p, nb_principals) == 0));
   */
  mpz_clear (p);
  tla_matrix_clear (S);

  return result;
}

			/* --------------- */

static int
s_add_generator_for_state (int s, int b, int q0, sataf_ea *ea, const int *pred, 
			   tla_group_t G, int mn, int nb_principals, 
			   ccl_bittable *u2set, tla_vec_t *rho_half, 
			   tla_vec_t *rho_full, tla_vec_t *tmp)
{  
  int c = STATE_C (mn, s);
  int k = STATE_K (mn, s);
  int completed = 0;
  int baux;
  int saux;
  int ksucc;
  int ssucc;
  int csucc = sataf_ea_get_successor (ea, c, b);

  if (sataf_ea_is_exit_state (csucc))
    return 0;
  
  csucc = sataf_ea_decode_succ_state (csucc);
  ksucc = (k + 1) % mn;
  ssucc = STATE (mn, csucc, ksucc);
  
  ccl_assert (ccl_bittable_has (u2set, 2*ssucc));
  ccl_assert (ccl_bittable_has (u2set, 2*ssucc+1));
  
  saux = s;
  tla_vec_gamma (tmp[0], rho_half[ssucc], b); /* u1 |- b -> u2 */
	  
  while (saux != q0)	  
    {	
      baux = pred[saux]&0x1;
      saux = pred[saux] >> 1;
      tla_vec_gamma (tmp[1], tmp[0], baux);
      tla_vec_set (tmp[0], tmp[1]);
    }

  tla_vec_sub (tmp[1], tmp[0], rho_full[ssucc]);
  if (! tla_group_has (G, tmp[1], NULL))
    {
      tla_group_add_generator (G, G, tmp[1]);
      if (s_is_completed (G, nb_principals))
	{	  
	  if (ccl_debug_is_on)
	    ccl_debug ("invariant group is completed\n");
	  completed = 1;
	}
    }

  return completed;
}
