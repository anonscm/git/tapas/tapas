/*
 * inv-project-automaton.c -- Add column to a MSA encoded relation
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include "prestaf-automata.h"

typedef struct ipa_st
{
  sataf_mao super;
  sataf_msa *msa;
  int width;
  int *selection;
  int selsize;
  int pos;
} ipa;

			/* --------------- */

static sataf_mao *
s_crt_add_variable_set (sataf_msa *msa, int n, int pos, int *sel, int selsize);


# define s_ipa_to_dot NULL

			/* --------------- */

static size_t
s_ipa_size (sataf_mao *self)
{
  return sizeof (ipa);
}

			/* --------------- */

static void
s_ipa_destroy (sataf_mao *self)
{
  ipa *a = (ipa *) self;
  if (--a->selection[a->selsize] == 0)
    ccl_delete (a->selection);
  sataf_msa_del_reference (a->msa);
}

			/* --------------- */

#define s_ipa_no_cache NULL

#define s_ipa_is_root_of_scc NULL

#define s_ipa_simplify NULL

static uint32_t
s_ipa_get_alphabet_size (sataf_mao *self)
{
  return 2;
}

			/* --------------- */

# define s_ipa_to_string NULL

static sataf_mao * 
s_ipa_succ (sataf_mao *self, uint32_t letter)
{
  ipa *a = (ipa *) self;
  sataf_msa *succ;
  sataf_mao *R;
  int newpos = (a->pos + 1) % a->selsize;

  if (a->selection[a->pos])
    succ = sataf_msa_add_reference (a->msa);
  else
    succ = sataf_msa_succ (a->msa, letter);
  R =
    s_crt_add_variable_set (succ, a->width, newpos, a->selection, a->selsize);
  sataf_msa_del_reference (succ);

  return R;
}

			/* --------------- */

static int
s_ipa_is_final (sataf_mao *self)
{
  ipa *a = (ipa *) self;

  return sataf_msa_is_final (a->msa);
}

			/* --------------- */

static int
s_ipa_is_equal_to (sataf_mao *self, sataf_mao *other)
{
  ipa *a1 = (ipa *) self;
  ipa *a2 = (ipa *) other;

  return a1->msa == a2->msa
    && a1->width == a2->width
    && a1->selsize == a2->selsize && a1->pos == a2->pos
    && a1->selection == a2->selection;
}

			/* --------------- */


static uint32_t
s_ipa_hashcode (sataf_mao *self)
{
  ipa *a = (ipa *) self;

  return 13 * (uintptr_t) a->selection
    + 71 * (uint32_t) a->selsize + 97 * a->pos + (uintptr_t) a->msa;
}

			/* --------------- */

static const sataf_mao_methods IPA_METHODS = 
  {
  "IPA",
  s_ipa_size,
  s_ipa_destroy,
  s_ipa_no_cache,
  s_ipa_is_root_of_scc,
  s_ipa_simplify,
  s_ipa_get_alphabet_size,
  s_ipa_to_string,
  s_ipa_succ,
  s_ipa_is_final,
  s_ipa_is_equal_to,
  s_ipa_hashcode,
  s_ipa_to_dot
};

			/* --------------- */

static sataf_mao * 
s_crt_add_variable_set (sataf_msa *msa, int n, int pos, int *sel, int selsize)
{
  sataf_mao *R;

  if (sataf_msa_is_one (msa))
    R = sataf_mao_create_one (2);
  else if (sataf_msa_is_zero (msa))
    R = sataf_mao_create_zero (2);
  else
    {
      ipa *A = (ipa *) sataf_mao_create (sizeof (ipa), &IPA_METHODS);

      A->msa = sataf_msa_add_reference (msa);
      A->width = n;
      A->selection = sel;
      A->selection[selsize]++;
      A->selsize = selsize;
      A->pos = pos;

      R = (sataf_mao *) A;
    }

  return R;
}

			/* --------------- */

sataf_mao * 
prestaf_crt_inv_project_automaton (sataf_msa *msa, int width, 
				   const int *selection, int selsize)
{
  sataf_mao *result;
  int *sel = ccl_new_array (int, 1 + selsize);
  ccl_memcpy (sel, selection, sizeof (int) * (selsize));
  sel[selsize] = 1;
  result = s_crt_add_variable_set (msa, width, 0, sel, selsize);
  if (--sel[selsize] == 0)
    ccl_delete (sel);

  return result;
}
