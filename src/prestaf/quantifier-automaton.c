/*
 * quantifier-automaton.c -- Quantifier for a MSA-encoded relation
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-list.h>
#include <ccl/ccl-memory.h>
#include <sataf/sataf.h>
#include "prestaf-automata.h"

			/* --------------- */

typedef struct qaut_st
{
  sataf_mao super;
  uint16_t i;
  uint16_t n;
  uint16_t nb_autos;
  sataf_msa **autos;
} qaut;

			/* --------------- */

static size_t
s_qaut_size (sataf_mao *self);

static void
s_qaut_destroy (sataf_mao *self);

static int
s_qaut_no_cache (sataf_mao *self);

#define s_qaut_is_root_of_scc NULL

#define s_qaut_simplify NULL

static uint32_t
s_qaut_get_alphabet_size (sataf_mao *self);

#define s_qaut_to_string NULL

static sataf_mao * 
s_qaut_succ_exists (sataf_mao *self, uint32_t letter);

static sataf_mao * 
s_qaut_succ_forall (sataf_mao *self, uint32_t letter);

static int
s_qaut_is_final_forall (sataf_mao *self);

static int
s_qaut_is_final_exists (sataf_mao *self);

static int
s_qaut_is_equal_to (sataf_mao *self, sataf_mao *other);

static unsigned int
s_qaut_actual_hashcode (qaut *a);

static unsigned int
s_qaut_hashcode (sataf_mao *self);

#define s_qaut_to_dot NULL

			/* --------------- */

static const int PRIME_TABLE[] = {
  1, 3, 7, 17, 37, 79, 163, 331, 673, 1361,
  2729, 5471, 10949, 21911, 43853, 87719, 175447, 350899, 701819, 1403641,
  2807303, 5614657, 11229331
};

static const size_t PRIME_TABLE_SIZE =
  (sizeof (PRIME_TABLE) / sizeof (PRIME_TABLE[0]));

			/* --------------- */

static const sataf_mao_methods QAUT_FORALL_METHODS = 
  {
    "SA-QAUT-FA",
    s_qaut_size,
    s_qaut_destroy,
    s_qaut_no_cache,
    s_qaut_is_root_of_scc,
    s_qaut_simplify,
    s_qaut_get_alphabet_size,
    s_qaut_to_string,
    s_qaut_succ_forall,
    s_qaut_is_final_forall,
    s_qaut_is_equal_to,
    s_qaut_hashcode,
    s_qaut_to_dot
  };

			/* --------------- */

static const sataf_mao_methods QAUT_EXISTS_METHODS = 
  {
    "SA-QAUT-EX",
    s_qaut_size,
    s_qaut_destroy,
    s_qaut_no_cache,
    s_qaut_is_root_of_scc,
    s_qaut_simplify,
    s_qaut_get_alphabet_size,
    s_qaut_to_string,
    s_qaut_succ_exists,
    s_qaut_is_final_exists,
    s_qaut_is_equal_to,
    s_qaut_hashcode,
    s_qaut_to_dot
  };

			/* --------------- */

static qaut *
s_create_qaut (uint32_t i, uint32_t n, sataf_msa **autos, int nb_autos,
	       const sataf_mao_methods *methods);

static sataf_mao * 
s_create_quantifier (uint32_t i, uint32_t n, int nb_autos, sataf_msa **autos,
		     const sataf_mao_methods *methods);

			/* --------------- */

sataf_mao * 
prestaf_crt_quantifier_automaton (int forall, uint32_t i, uint32_t n,
				  sataf_msa *fset)
{
  sataf_mao *R;


  if (sataf_msa_is_zero (fset))
    R = sataf_mao_create_zero (2);
  else if (sataf_msa_is_one (fset))
    R = sataf_mao_create_one (2);
  else if (n < 2)
    {
      if (forall)
	R = sataf_mao_create_zero (2);
      else
	R = sataf_mao_create_one (2);
    }
  else
    {
      sataf_msa **autos = ccl_new_array (sataf_msa *, 1);
      const sataf_mao_methods *methods = forall
	? &QAUT_FORALL_METHODS : &QAUT_EXISTS_METHODS;
      *autos = sataf_msa_add_reference (fset);
      R = s_create_quantifier (i, n, 1, autos, methods);
    }

  return R;
}

			/* --------------- */

static sataf_mao * 
s_create_quantifier (uint32_t i, uint32_t n, int nb_autos, sataf_msa **autos,
		     const sataf_mao_methods *methods)
{
  sataf_mao *R;
  int forall = (methods == &QAUT_FORALL_METHODS);
  ccl_pre (n > 1);

  if (nb_autos == 0)
    {
      ccl_zdelete (ccl_delete, autos);
      if (forall)
	R = sataf_mao_create_zero (2);
      else
	R = sataf_mao_create_one (2);
    }
  else if (i == 0)
    {
      int a, j, k, l = 0;
      int len = nb_autos << 1;
      sataf_msa *tmp;
      sataf_msa *msa;
      sataf_msa **new_array = ccl_new_array (sataf_msa *, len);

      for (j = 0; j < nb_autos; j++)
	{
	  for (a = 0; a < 2; a++)
	    {
	      msa = sataf_msa_succ (autos[j], a);

	      if ((sataf_msa_is_zero (msa) && forall) || 
		  (sataf_msa_is_one (msa) && !forall))
		{
		  sataf_msa_del_reference (msa);
		  for (; j < nb_autos; j++)
		    sataf_msa_del_reference (autos[j]);
		  ccl_delete (autos);
		  while (l--)
		    sataf_msa_del_reference (new_array[l]);
		  ccl_delete (new_array);
		  if (forall)
		    return sataf_mao_create_zero (2);
		  else
		    return sataf_mao_create_one (2);
		}

	      for (k = 0; k < l; k++)
		{
		  if (msa < new_array[k])
		    {
		      tmp = new_array[k];
		      new_array[k] = msa;
		      msa = tmp;
		    }
		  else if (msa == new_array[k])
		    {
		      sataf_msa_del_reference (msa);
		      break;
		    }
		}

	      if (k == l)
		new_array[l++] = msa;
	      else
		ccl_assert (msa == new_array[k]);
	    }
	  sataf_msa_del_reference (autos[j]);
	}

      ccl_delete (autos);


      ccl_assert (0 < l && l <= len);
      R = s_create_quantifier (n - 1, n, l, new_array, methods);
    }
  else
    {
      ccl_assert (nb_autos > 0);
      R = (sataf_mao *) s_create_qaut (i, n, autos, nb_autos, methods);
    }

  return R;
}

			/* --------------- */


static size_t
s_qaut_size (sataf_mao *self)
{
  return sizeof (qaut);
}

			/* --------------- */

static void
s_qaut_destroy (sataf_mao *self)
{
  qaut *a = (qaut *) self;
  sataf_msa **m = a->autos;
  int i = a->nb_autos;

  ccl_pre (i > 0);

  while (i--)
    {
      sataf_msa_del_reference (*m);
      m++;
    }

  ccl_delete (a->autos);
}

			/* --------------- */

static int
s_qaut_no_cache (sataf_mao *self)
{
  return 0;
}

			/* --------------- */

static uint32_t
s_qaut_get_alphabet_size (sataf_mao *self)
{
  return 2;
}

			/* --------------- */

static sataf_mao * 
s_qaut_succ_exists (sataf_mao *self, uint32_t letter)
{
  int i, k, l = 0;
  qaut *a = (qaut *) self;
  int len = a->nb_autos;
  sataf_msa **succs = ccl_new_array (sataf_msa *, len);
  sataf_mao *R;
  sataf_msa *msa;
  sataf_msa *tmp;

  ccl_assert (len > 0);
  ccl_assert (a->i > 0);

  for (i = 0; i < len; i++)
    {
      ccl_assert (a->nb_autos > 0);
      msa = sataf_msa_succ (a->autos[i], letter);

      if (sataf_msa_is_one (msa))
	{
	  sataf_msa_del_reference (msa);
	  while (l--)
	    sataf_msa_del_reference (succs[l]);
	  ccl_delete (succs);
	  return sataf_mao_create_one (2);
	}

      for (k = 0; k < l; k++)
	{
	  if (msa < succs[k])
	    {
	      tmp = succs[k];
	      succs[k] = msa;
	      msa = tmp;
	    }
	  else if (msa == succs[k])
	    {
	      sataf_msa_del_reference (msa);
	      break;
	    }
	}

      if (k == l)
	succs[l++] = msa;
    }

  ccl_assert (0 < l && l <= len);

  R = (sataf_mao *)
    s_create_quantifier (a->i - 1, a->n, l, succs, a->super.methods);

  return R;
}

			/* --------------- */

static sataf_mao * 
s_qaut_succ_forall (sataf_mao *self, uint32_t letter)
{
  int i, k, l = 0;
  qaut *a = (qaut *) self;
  int len = a->nb_autos;
  sataf_msa **succs = ccl_new_array (sataf_msa *, len);
  sataf_mao *R;
  sataf_msa *msa;
  sataf_msa *tmp;

  ccl_assert (a->i > 0);

  for (i = 0; i < len; i++)
    {
      msa = sataf_msa_succ (a->autos[i], letter);

      if (sataf_msa_is_zero (msa))
	{
	  sataf_msa_del_reference (msa);
	  while (l--)
	    sataf_msa_del_reference (succs[l]);
	  ccl_delete (succs);
	  return sataf_mao_create_zero (2);
	}

      for (k = 0; k < l; k++)
	{
	  if (msa < succs[k])
	    {
	      tmp = succs[k];
	      succs[k] = msa;
	      msa = tmp;
	    }
	  else if (msa < succs[k])
	    {
	      sataf_msa_del_reference (msa);
	      break;
	    }
	}

      if (k == l)
	succs[l++] = msa;
    }

  ccl_assert (0 < l && l <= len);

  R = (sataf_mao *)
    s_create_quantifier (a->i - 1, a->n, l, succs, a->super.methods);

  return R;
}

			/* --------------- */

static int
s_qaut_is_final_forall (sataf_mao *self)
{
  int i;
  qaut *a = (qaut *) self;
  int max = a->nb_autos;

  for (i = 0; i < max; i++)
    if (!sataf_msa_is_final (a->autos[i]))
      return 0;
  return 1;
}

			/* --------------- */

static int
s_qaut_is_final_exists (sataf_mao *self)
{
  int i;
  qaut *a = (qaut *) self;
  int max = a->nb_autos;

  for (i = 0; i < max; i++)
    if (sataf_msa_is_final (a->autos[i]))
      return 1;
  return 0;
}

			/* --------------- */

static int
s_qaut_is_equal_to (sataf_mao *self, sataf_mao *other)
{
  qaut *a1 = (qaut *) self;
  qaut *a2 = (qaut *) other;

  if (!(a1->i == a2->i && a1->n == a2->n && a1->nb_autos == a2->nb_autos))
    return 0;
  return 
    ccl_memcmp (a1->autos, a2->autos, sizeof (sataf_msa *) * a1->nb_autos) == 0;
}

			/* --------------- */

static unsigned int
s_qaut_actual_hashcode (qaut *a)
{
  int i;
  int max = a->nb_autos;
  uint32_t result = 175447 * a->i + 43853 * a->n + 350899 * max;

  for (i = 0; i < max; i++)
    result = 175447 * result +
      PRIME_TABLE[i % PRIME_TABLE_SIZE] * (uintptr_t) a->autos[i];

  return result;
}

			/* --------------- */

static unsigned int
s_qaut_hashcode (sataf_mao *self)
{
  qaut *a = (qaut *) self;

  return s_qaut_actual_hashcode (a);
}

			/* --------------- */

static qaut *
s_create_qaut (uint32_t i, uint32_t n, sataf_msa **autos, int nb_autos,
	       const sataf_mao_methods *methods)
{
  qaut *R = (qaut *) sataf_mao_create (sizeof (qaut), methods);

  ccl_assert (nb_autos > 0);

  R->i = i;
  R->n = n;
  R->autos = autos;
  R->nb_autos = nb_autos;

  return R;
}
