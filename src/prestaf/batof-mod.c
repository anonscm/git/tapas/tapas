/*
 * batof-mod.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <ccl/ccl-memory.h>
#include "batof-sfp.h"
#include "prestaf-batof-p.h"


static int
s_compute_n (int n0);

			/* --------------- */

sataf_ea *
btf_ea_compute_mod_classes (sataf_ea *ea, int **p_state_to_class,
			    int ***p_class_to_states)
{
  int i;
  sataf_ea *result;
  int nb_classes;
  int nb_exits = sataf_ea_get_nb_exits (ea) ? 1 : 0;
  int nb_states = sataf_ea_get_nb_states (ea);
  int nb_edges = 0;
  int **edges = ccl_new_array (int *, 2 * nb_states);
  int *E = ccl_new_array (int, 3 * 2 * nb_states);
  int *e = E;

  for (i = 0; i < nb_states; i++)
    {
      int b;

      for (b = 0; b < 2; b++)
	{
	  int tgt = sataf_ea_get_successor (ea, i, b);
	  if (sataf_ea_is_local_state (tgt))
	    {
	      tgt = sataf_ea_decode_succ_state (tgt);

	      e[0] = i;
	      e[1] = b;
	      e[2] = tgt;
	      edges[nb_edges] = e;

	      e += 3;
	      nb_edges++;
	    }
	}
    }

  btf_stalling_folding_process (BTF_SFP_BOTHWARD, nb_states, nb_edges, edges, 
				&nb_classes, p_state_to_class, 
				p_class_to_states);
  ccl_delete (edges);
  ccl_delete (E);

  result = sataf_ea_create (nb_classes, nb_exits, 2);

  for (i = 0; i < nb_classes; i++)
    {
      int b;
      int *states = (*p_class_to_states)[i];

      for (b = 0; b < 2; b++)
	{
	  int etgt = sataf_ea_get_successor (ea, *states, b);

	  if (sataf_ea_is_exit_state (etgt))
	    {
	      sataf_ea_set_successor (result, i, b, 0, 1);
	    }
	  else
	    {
	      int tgt = sataf_ea_decode_succ_state (etgt);
	      int ctgt = (*p_state_to_class)[tgt];
	      sataf_ea_set_successor (result, i, b, ctgt, 0);
	    }
	}

      for (; *states >= 0; states++)
	if (sataf_ea_is_final (ea, *states))
	  {
	    sataf_ea_set_final (result, i, 1);
	    break;
	  }
    }

  return result;
}

			/* --------------- */

void
btf_compute_n0 (sataf_ea *ea, int *p_n0, int *p_n)
{
  int result = 0;
  int c;
  int nb_states = sataf_ea_get_nb_states (ea);
  ccl_bittable *good = ccl_bittable_create (nb_states);
  ccl_bittable *todo = ccl_bittable_create (nb_states);
  ccl_bittable *stack = ccl_bittable_create (nb_states);

  for (c = 0; c < nb_states; c++)
    ccl_bittable_set (todo , c);

  while ((c = ccl_bittable_get_first (todo)) >= 0)
    { 
      int loop_or_out = 0;

      ccl_bittable_clear (stack);

      while (! loop_or_out) 
	{
	  int succ = sataf_ea_get_successor (ea, c, 0);

	  ccl_assert (ccl_bittable_has (todo, c));
	  ccl_assert (! ccl_bittable_has (stack, c));

	  ccl_bittable_set (stack, c);
	  ccl_bittable_unset (todo, c);

	  if (sataf_ea_is_exit_state (succ))
	    loop_or_out = 1;
	  else 
	    {
	      int cp = sataf_ea_decode_succ_state (succ);

	      if (ccl_bittable_has (stack, cp) || ccl_bittable_has (good, cp))
		{
		  int i;

		  for (i = ccl_bittable_get_first (stack);
		       i >= 0;
		       i = ccl_bittable_get_next (stack, i))
		    {
		      result++;
		      ccl_bittable_set (good,i);
		    }
		  loop_or_out = 1;
		}
	      else if (ccl_bittable_has (todo, cp))
		{
		  c = cp;
		}
	      else 
		{
		  ccl_assert (! ccl_bittable_has (todo, cp)
			      && ! ccl_bittable_has (good, cp) );
		  loop_or_out = 1;
		}
	    }
	}
    }

  ccl_bittable_delete (good);
  ccl_bittable_delete (todo);
  ccl_bittable_delete (stack);

  while ((result%2) == 0)
    result >>= 1;

  *p_n0 = result;
  *p_n = s_compute_n(result);
}

			/* --------------- */

static int
s_compute_n(int n0)
{
  int n = 1;
  int r = 2 % n0;

  if (n0 == 1)
    return 1;

  while (r != 1)
    {
      n++;
      r = (r * 2) % n0;
    }

  return n;
}

#if 0

			/* --------------- */

typedef struct equivalence_class_st eq_class;
typedef struct class_element_st class_element;
typedef struct edge_st edge;

			/* --------------- */

struct equivalence_class_st
{
  int size;
  class_element *first;
  class_element **last;
  edge *pred[2];
  edge **pred_last[2];
  eq_class *next;
  eq_class *prev;
};

			/* --------------- */

struct edge_st
{
  class_element *el;
  edge *next;
};

			/* --------------- */

struct class_element_st
{
  eq_class *C;
  class_element *next;
};

			/* --------------- */

# define ELINDEX(base,el) ((unsigned int)((el)-(base)))
# define CLINDEX(base,cl) ((unsigned int)((cl)-(base)))

			/* --------------- */

static void
s_init (sataf_ea *ea, unsigned int nb_states, eq_class *S,
	class_element *elements, eq_class * classes, edge *preds,
	ccl_bittable *todo);

static void
s_build_classes (int nb_states, int nb_classes, class_element *elements, 
		 eq_class *classes, int **p_state_to_class, 
		 int ***p_class_to_state, eq_class *S);

#if CCL_ENABLE_ASSERTIONS
static int
s_display_classes (eq_class *C, eq_class *basecl, class_element *baseel);

static void
s_classes_to_dot (ccl_log_type log, sataf_ea *ea, int nb_classes,
		  int *s2c, int **c2s);

static int
s_is_backward_deterministic (sataf_ea *ea, eq_class *first_class,
			     class_element *baseel);

static int
s_is_deterministic (sataf_ea *ea, eq_class * first_class,
		    class_element * baseel);
static int
s_is_actual_class(eq_class *c, eq_class *S);

static int
s_check_classes (int nb_states, int nb_classes, class_element *elements, 
		 eq_class *classes, eq_class *S);
#endif /* ! CCL_ENABLE_ASSERTIONS */

			/* --------------- */

static int
s_get_biggest (ccl_bittable *todo, eq_class *classes)
{
  int index;
  int biggest;
  int i = ccl_bittable_get_first (todo);

  if (i < 0)
    return -1;

  biggest = i;
  index = i >> 1;

  while ((i = ccl_bittable_get_next (todo, i)) >= 0)
    {
      if (classes[i >> 1].size > classes[index].size)
	index = (biggest = i) >> 1;
    }

  return biggest;
}

			/* --------------- */


sataf_ea *
btf_ea_compute_mod_classes (sataf_ea *ea, int **p_state_to_class,
			    int ***p_class_to_state)
{
  int i;
  int nb_classes;
  unsigned int nb_states = ea->nb_local_states;
  edge *preds = ccl_new_array (edge, 2 * nb_states);
  class_element *elements = ccl_new_array (class_element, nb_states);
  eq_class *classes = ccl_new_array (eq_class, nb_states);
  eq_class S[2];
  ccl_bittable *todo = ccl_bittable_create (2 * nb_states);
  ccl_bittable *to_merge = ccl_bittable_create (nb_states);

  CCL_DEBUG_START_TIMED_BLOCK (("compute mod automaton"));

  s_init (ea, nb_states, S, elements, classes, preds, todo);
  nb_classes = nb_states;
  while ((i = s_get_biggest (todo, classes)) >= 0)
    {
      int k;
      eq_class *Cm;
      edge *e;
      unsigned int q = (i >> 1);
      unsigned int b = (i & 0x1);
      eq_class *C = classes + q;

      ccl_assert (s_is_actual_class(C, S));

      ccl_bittable_unset (todo, i);

      ccl_assert (C->pred[b]->next != NULL);

      ccl_assert (s_check_classes (nb_states, nb_classes, elements, 
				   classes, S));

      Cm = C->pred[b]->el->C;
      ccl_assert (s_is_actual_class(Cm, S));

      ccl_bittable_unset (todo, 2 * CLINDEX (classes, Cm));
      ccl_bittable_unset (todo, 2 * CLINDEX (classes, Cm) + 1);
      ccl_bittable_clear (to_merge);

      for (e = C->pred[b]->next; e; e = e->next)
	{
	  class_element *el;
	  eq_class *Cm2 = e->el->C;
	  int index = CLINDEX (classes, Cm2);

	  if (Cm2 == Cm || ccl_bittable_has (to_merge, index))
	    continue;

	  ccl_bittable_set (to_merge, index);
	  if (ccl_bittable_has (todo, 2*index))
	    ccl_bittable_unset (todo, 2*index);
	  if (ccl_bittable_has (todo, 2*index + 1))
	    ccl_bittable_unset (todo, 2*index + 1);
	  *(Cm->last) = Cm2->first;
	  Cm->last = Cm2->last;
	  for (el = Cm2->first; el; el = el->next)
	    el->C = Cm;
	  Cm->size += Cm2->size;
	}

      for (k = ccl_bittable_get_first (to_merge);
	   k >= 0; k = ccl_bittable_get_next (to_merge, k))
	{
	  eq_class *Cm2 = classes + k;

	  ccl_assert (Cm2 != Cm);

	  if (Cm2->pred[0])
	    {
	      *(Cm->pred_last[0]) = Cm2->pred[0];
	      Cm->pred_last[0] = Cm2->pred_last[0];
	    }

	  if (Cm2->pred[1])
	    {
	      *(Cm->pred_last[1]) = Cm2->pred[1];
	      Cm->pred_last[1] = Cm2->pred_last[1];
	    }

	  nb_classes--;
	  Cm2->next->prev = Cm2->prev;
	  Cm2->prev->next = Cm2->next;
	  Cm2->next = NULL;
	  Cm2->prev = NULL;
	}

      if (Cm->pred[0] != NULL && Cm->pred[0]->next != NULL)
	ccl_bittable_set (todo, 2 * (CLINDEX (classes, Cm)));
      if (Cm->pred[1] != NULL && Cm->pred[1]->next != NULL)
	ccl_bittable_set (todo, 1 + 2 * (CLINDEX (classes, Cm)));

      C->pred[b]->next = NULL;
      C->pred[b]->el = Cm->first;
      C->pred_last[b] = &(C->pred[b]->next);

      ccl_assert (s_check_classes (nb_states, nb_classes, elements, 
				   classes, S));
    }

  ccl_assert (1 || s_is_backward_deterministic (ea, S[0].next, elements));
  ccl_assert (s_is_deterministic (ea, S[0].next, elements));

  s_build_classes (nb_states, nb_classes, elements, classes, p_state_to_class, 
		   p_class_to_state, S);

  ccl_bittable_delete (todo);

  ccl_bittable_delete (to_merge);
  ccl_delete (elements);
  ccl_delete (preds);
  ccl_delete (classes);

  {
    unsigned int s;
    int nb_exits = sataf_ea_get_nb_exits (ea)?1:0;
    sataf_ea *result = sataf_ea_create (nb_classes, nb_exits, 2);

    for (s = 0; s < nb_states; s++)
      {
	int b;
	int cs = (*p_state_to_class)[s];

	if (sataf_ea_is_final (ea, s))	  
	  sataf_ea_set_final (result, cs, 1);

	for (b = 0; b < 2; b++)
	  {
	    int succ = sataf_ea_get_successor (ea, s, b);
	    int is_exit = sataf_ea_is_exit_state (succ);
	    int csucc;

	    if (is_exit)
	      csucc = 0;
	    else
	      {
		succ = sataf_ea_decode_succ_state (succ);
		csucc = (*p_state_to_class)[succ];
	      }
	    sataf_ea_set_successor (result, cs, b, csucc, is_exit);
	  }
      }

    if (ccl_debug_is_on)
      {
	ccl_debug ("# classes = %d\n", result->nb_local_states);
      }
    CCL_DEBUG_END_TIMED_BLOCK ();

    return result;
  }
}

			/* --------------- */


#if 0
static void
s_comp_vec_rec(int s, int v, btf_vec_t *V, int *pred)
{
  if (s != v)
    {
      int b = pred[2 * s] >= 0 ? 0 : 1;
      int p = pred[2 * s + b];
      s_comp_vec_rec (p, v, V, pred);
      btf_vec_inv_gamma (V[2], V[0], b);
      btf_vec_set (V[0],V[2]);
      btf_vec_inv_gamma (V[2], V[1], b);
      btf_vec_set (V[1],V[2]);
    }  
}
#endif 

			/* --------------- */

static int
s_succ_class(sataf_ea *ea, int c, int b, int *state_to_class, 
	     int **class_to_state)
{
  int s = *class_to_state[c];
  int q = sataf_ea_get_successor (ea, s ,b);
  
  if (sataf_ea_is_exit_state (q))
    return -1;

  q = sataf_ea_decode_succ_state (q);

  return state_to_class[q];
}

			/* --------------- */


static void
s_display_classes_as_dot (ccl_log_type log, sataf_ea *ea, int q0,
			  int nb_classes, int *s2c, int **c2s)
{
  int s;

  ccl_log(log,"digraph classes {\n");
  ccl_log (log, "\t%d [style=filled, fillcolor=\"grey\"];\n", q0);
  for (s = 0; s < nb_classes; s++)
    {
      int b;

      for (b = 0; b < 2; b++)
	{
	  int succ = s_succ_class(ea, s, b, s2c, c2s);
	  
	  if (succ >= 0)
	    ccl_log (log, "\t%d -> %d [label=\"%d\"];\n", s, succ, b);
	}
    }
  ccl_log(log,"}\n");
}

			/* --------------- */

static void
s_init (sataf_ea *ea, unsigned int nb_states, eq_class *S,
	class_element *elements, eq_class *classes, edge *preds,
	ccl_bittable *todo)
{
  unsigned int q;

  S[0].next = &S[1];
  S[0].prev = NULL;
  S[1].next = NULL;
  S[1].prev = &S[0];

  for (q = 0; q < nb_states; q++)
    {
      eq_class *C = classes + q;

      elements[q].C = C;
      elements[q].next = NULL;

      C->size = 1;
      C->first = &elements[q];
      C->last = &(C->first->next);
      C->next = S[0].next;
      C->prev = &(S[0]);
      C->pred[0] = C->pred[1] = NULL;
      C->pred_last[0] = &(C->pred[0]);
      C->pred_last[1] = &(C->pred[1]);
      S[0].next->prev = C;
      S[0].next = C;
    }

  for (q = 0; q < nb_states; q++)
    {
      unsigned int b;

      for (b = 0; b < 2; b++)
	{
	  edge *e;
	  int qp = sataf_ea_get_successor (ea, q, b);

	  if (sataf_ea_is_local_state (qp))
	    {
	      eq_class *Cqp;

	      qp = sataf_ea_decode_succ_state (qp);
	      Cqp = elements[qp].C;

	      if (Cqp->pred[b] != NULL
		  && !ccl_bittable_has (todo, 2 * qp + b))
		ccl_bittable_set (todo, 2 * qp + b);

	      e = preds++;
	      e->el = &elements[q];
	      e->next = NULL;
	      *(Cqp->pred_last[b]) = e;
	      Cqp->pred_last[b] = &(e->next);
	    }
	}
    }
}

			/* --------------- */

static void
s_build_classes (int nb_states, int nb_classes, class_element *elements, 
		 eq_class *classes, int **p_state_to_class, 
		 int ***p_class_to_state, eq_class *S)
{
  eq_class *c;
  int cindex;
  int *array_for_classes;
    
  *p_state_to_class = ccl_new_array (int, 2 * nb_states + nb_classes);
  for (cindex = 0; cindex < nb_states; cindex++)
    (*p_state_to_class)[cindex] = -1;

  array_for_classes = (*p_state_to_class) + nb_states;
  *p_class_to_state = ccl_new_array (int *, nb_classes);

  for (cindex = 0, c = S[0].next; c->next; c = c->next, cindex++)
    {
      class_element *el;

      (*p_class_to_state)[cindex] = array_for_classes;

      for (el = c->first; el; el = el->next)
	{
	  int index = ELINDEX (elements, el);
	  ccl_assert ((*p_state_to_class)[index] == -1);
	  (*p_state_to_class)[index] = cindex;
	  *(array_for_classes++) = index;
	}
      *(array_for_classes++) = -1;
    }    
}

			/* --------------- */


#if CCL_ENABLE_ASSERTIONS
static int
s_display_classes (eq_class *C, eq_class *basecl, class_element *baseel)
{
  int count = 0;
  int scount = 0;

  ccl_debug ("===============\n");
  for (; C->next; C = C->next)
    {
      class_element *el;

      ccl_debug ("[%d] = {", CLINDEX (basecl, C));
      ccl_assert (C->last != NULL);

      for (el = C->first; el; el = el->next)
	{
	  scount++;
	  ccl_debug ("%d", ELINDEX (baseel, el));
	  if (el->next != NULL)
	    ccl_debug (", ");
	}
      ccl_debug ("}\n");
      count++;
    }

  ccl_debug ("#classes = %d\n", count);
  return scount;
}

			/* --------------- */

static int
s_get_pred(sataf_ea *ea, int s, int b)
{
  int x;

  for (x = 0; x < ea->nb_local_states; x++)
    {
      int succ = sataf_ea_get_successor (ea, x, b);

      if (sataf_ea_is_exit_state (succ))
	continue;
      succ = sataf_ea_decode_succ_state (succ);
      if (succ == s)
	return x;
    }

  return -1;
}

			/* --------------- */

static int
s_check_pred(sataf_ea *ea, int s, eq_class **pred, class_element *baseel)
{
  int x;

  for (x = 0; x < ea->nb_local_states; x++)
    {
      int b;
      
      for (b = 0; b < 2; b++)
	{
	  int succ = sataf_ea_get_successor (ea, x, b);

	  if (sataf_ea_is_exit_state (succ))
	    continue;
	  succ = sataf_ea_decode_succ_state (succ);
	  if (succ == s)
	    {
	      if (pred[b] == NULL)
		return 0;
	      else if(baseel[x].C != pred[b])
		return 0;
	    }
	}
    }

  return 1;
}

			/* --------------- */

static int
s_class_size (int *c)
{
  int result = 0;

  while (*c >= 0)
    {
      c++;
      result++;
    }
  return result;
}

			/* --------------- */

static void
s_classes_to_dot (ccl_log_type log, sataf_ea *ea, int nb_classes,
		  int *s2c, int **c2s)
{
  ccl_list *todo = ccl_list_create ();
  ccl_bittable *done = ccl_bittable_create (nb_classes);

  ccl_log (log, "digraph mod_classes_%p {\n", ea);

  ccl_list_add (todo, (void *) s2c[0]);
  ccl_bittable_set (done, s2c[0]);

  while (!ccl_list_is_empty (todo))
    {
      int c = (int) ccl_list_take_first (todo);
      int q = c2s[c][0];
      unsigned int b;

      ccl_assert (q >= 0);

      ccl_log (log, "%d[label=\"%d/%d\"];\n", c, c, s_class_size (c2s[c]));

      for (b = 0; b < 2; b++)
	{
	  int qp = sataf_ea_get_successor (ea, (unsigned int) q, b);

	  ccl_log (log, "%d -> ", c);

	  if (sataf_ea_is_exit_state (qp))
	    ccl_log (log, "NULL");
	  else
	    {
	      qp = sataf_ea_decode_succ_state (qp);
	      ccl_log (log, "%d", s2c[qp]);
	      if (!ccl_bittable_has (done, s2c[qp]))
		{
		  ccl_bittable_set (done, s2c[qp]);
		  ccl_list_add (todo, (void *) s2c[qp]);
		}
	    }
	  ccl_log (log, "[label=\"%d\"];\n", b);
	}
    }

  ccl_log (log, "};\n");

  ccl_list_delete (todo);
  ccl_bittable_delete (done);
}

			/* --------------- */

static int
s_is_backward_deterministic (sataf_ea *ea, eq_class *first_class,
			     class_element *baseel)
{
  int is_deterministic = 1;
  eq_class *c;

  for (c = first_class; c->next && is_deterministic; c = c->next)
    {
      unsigned int q;
      int p;
      class_element *el = c->first;
      eq_class *pred[2] = { NULL, NULL };

      for (el = c->first; el ; el = el->next)
	{
	  q = ELINDEX (baseel, el);
	  if ((p = s_get_pred (ea, q, 0)) >= 0)
	    pred[0] = baseel[p].C;
	  else
	    pred[0] = NULL;
	  if (pred[0] == NULL)
	    continue;

	  if ((p = s_get_pred (ea, q, 1)) >= 0)
	    pred[1] = baseel[p].C;
	  else
	    pred[1] = NULL;

	  if (pred[1] != NULL)
	    break;
	}

      for (el = c->first; el && is_deterministic; el = el->next)
	is_deterministic = s_check_pred (ea, ELINDEX (baseel, el), pred,
					 baseel);
    }

  return is_deterministic;
}

			/* --------------- */

static int
s_is_deterministic (sataf_ea *ea, eq_class *first_class,
		    class_element *baseel)
{
  int is_deterministic = 1;
  eq_class *c;

  for (c = first_class; c->next && is_deterministic; c = c->next)
    {
      unsigned int q;
      unsigned int b;
      class_element *el = c->first;
      eq_class *succ[2] = { NULL, NULL };

      q = ELINDEX (baseel, el);

      for (b = 0; b < 2; b++)
	{
	  int qp = sataf_ea_get_successor (ea, q, b);

	  if (sataf_ea_is_exit_state (qp))
	    continue;
	  qp = sataf_ea_decode_succ_state (qp);
	  succ[b] = baseel[qp].C;
	}

      for (el = el->next; el && is_deterministic; el = el->next)
	{
	  q = ELINDEX (baseel, el);
	  for (b = 0; b < 2 && is_deterministic; b++)
	    {
	      int qp = sataf_ea_get_successor (ea, q, b);

	      if (sataf_ea_is_exit_state (qp))
		continue;
	      qp = sataf_ea_decode_succ_state (qp);
	      is_deterministic = (succ[b] == baseel[qp].C);
	    }
	}
    }

  return is_deterministic;
}

			/* --------------- */


static int
s_is_actual_class(eq_class *cl, eq_class *S)
{
  eq_class *c;

  for (c = S[0].next; c->next; c = c->next)
    if (c == cl)
      return 1;

  return 0;
}

			/* --------------- */

static int
s_check_classes (int nb_states, int nb_classes, class_element *elements, 
		 eq_class *classes, eq_class *S)
{
  int nb_cl = 0;
  int nb_elements = 0;
  eq_class *c;
  int *s2c, **c2s;

  for (c = S[0].next; c->next; c = c->next)
    {
      nb_elements += c->size;
      nb_cl++;
    }

  ccl_assert (nb_cl == nb_classes);
  ccl_assert (nb_elements == nb_states);
  
  s_build_classes (nb_states, nb_classes, elements, classes, &s2c, &c2s, S);
  ccl_delete (s2c);
  ccl_delete (c2s);

  return 1;
}
#endif /* CCL_ENABLE_ASSERTIONS */

			/* --------------- */


#endif
