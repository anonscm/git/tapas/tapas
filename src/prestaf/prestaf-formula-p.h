/*
 * prestaf-formula-p.h -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __PRESTAF_FORMULA_P_H__
# define __PRESTAF_FORMULA_P_H__

# include "prestaf-formula.h"
# include "prestaf-predicate.h"

typedef enum prestaf_formula_type_enum {
  PF_AND, PF_OR, PF_NOT, PF_EQUIV, PF_IMPLY, PF_XOR,
  PF_EQ, PF_GT, PF_GEQ,
  PF_FORALL, PF_EXISTS, PF_CST, PF_FILE
} prestaf_formula_type;


typedef struct prestaf_boolean_formula_st prestaf_boolean_formula;
typedef struct prestaf_file_formula_st prestaf_file_formula;
typedef struct prestaf_boolean_constant_st prestaf_boolean_constant;
typedef struct prestaf_factor_st prestaf_factor;
typedef struct prestaf_linear_constraint_st prestaf_linear_constraint;
typedef struct prestaf_quantifier_st prestaf_quantifier;


struct prestaf_formula_st
{
  uint32_t refcount;
  prestaf_formula_type type;
  prestaf_predicate *P;
};

struct prestaf_file_formula_st
{
  prestaf_formula super;
  char *filename;
};

struct prestaf_boolean_constant_st
{
  prestaf_formula super;
  int value;
};

struct prestaf_boolean_formula_st
{
  prestaf_formula super;
  ccl_list *operands;
};

struct prestaf_linear_constraint_st
{
  prestaf_formula super;
  prestaf_linear_term *axb;
};

struct prestaf_quantifier_st
{
  prestaf_formula super;
  ccl_ustring qV;
  prestaf_formula *F;
};

struct prestaf_factor_st
{
  prestaf_factor *next;
  int a;
  ccl_ustring x;
};

struct prestaf_linear_term_st
{
  uint32_t refcount;
  prestaf_factor *factors;
  int b;
  uint32_t nb_factors;
};

#endif /* ! __PRESTAF_FORMULA_P_H__ */
