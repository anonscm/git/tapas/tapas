/*
 * batof-backxor.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include "prestaf-batof-p.h"

#define DEPTH_FOR_SUCC_CHECKING 3

typedef struct xor_state_st
{
  int q1;
  int q2;
  int turn;
} xor_state;

struct comp_data
{
  sataf_ea *ea;
  struct int_list **couples;
  ccl_hash *result;
  int done;
  int turn;
  int nb_states;  
  int state_table_size_index;
  int state_table_size;  
  xor_state *state_table;
};

struct int_list
{
  int i;
  struct int_list *next;
};


			/* --------------- */

typedef struct backward_xor_st
{
  sataf_msa super;
  struct comp_data *data;
  int q1;
  int q2;
} backward_xor;

			/* --------------- */

struct pred
{
  int s;
  int b;
  struct pred *next;
};

			/* --------------- */

static sataf_mao *
s_create_backward_xor (struct comp_data *data, int q1, int q2);

static struct int_list **
s_compute_couples (sataf_ea *ea);

static void
s_init_state_space (struct comp_data *data);

static void
s_terminate_state_space (struct comp_data *data);

static int
s_state_turn (struct comp_data *data, int q1, int q2);

static void
s_add_state (struct comp_data *data, int q1, int q2);

static int 
s_actually_need_to_compute_xor_for (struct comp_data *data, int q1, int q2, 
				    int depth);
				    

			/* --------------- */

sataf_msa **
btf_backward_xor (int m, sataf_ea *ea, int **p_q, int *p_nb_msa)
{
  int i;
  int nb_states;
  struct comp_data data;
  sataf_msa **result = NULL;
  ccl_hash_entry_iterator *ri;
  int nb_built_xor = 0;

  CCL_DEBUG_START_TIMED_BLOCK (("compute backward XOR of %p", ea));

  nb_states = sataf_ea_get_nb_states (ea);
  data.ea = ea;
  data.turn = 0;
  data.couples = s_compute_couples (ea);
  data.result = ccl_hash_create (NULL, NULL, NULL, NULL);
  s_init_state_space (&data);

  for (i = 0; i < nb_states; i++)
    {
      while (data.couples[i])
	{
	  sataf_mao *mao;
	  sataf_msa *msa;
	  struct int_list *c = data.couples[i];
	  int s = c->i;

	  data.couples[i] = c->next;
	  ccl_delete (c);

	  if (! s_actually_need_to_compute_xor_for (&data, i, s, 
						    DEPTH_FOR_SUCC_CHECKING))
	    continue;
	  nb_built_xor++;
	  data.turn++;
	  data.done = 0;
	  mao = s_create_backward_xor (&data, i, s);
	  msa = sataf_msa_compute (mao);
	  sataf_mao_del_reference (mao);
	  sataf_msa_del_reference (msa);
	}
    }

  ccl_delete (data.couples);

  *p_nb_msa = ccl_hash_get_size (data.result);
  result = ccl_new_array (sataf_msa *, *p_nb_msa);
  *p_q = ccl_new_array(int, *p_nb_msa);
  ri = ccl_hash_get_entries (data.result);
  i = 0;

  while (ccl_iterator_has_more_elements (ri))
    {
      ccl_hash_entry e = ccl_iterator_next_element (ri);
      result[i] = e.key;
      (*p_q)[i] = (intptr_t) e.object;
      i++;
    }
  ccl_iterator_delete (ri);
  ccl_hash_delete (data.result);
  s_terminate_state_space (&data);

  if (ccl_debug_is_on)
    ccl_debug ("%d xor built\n", nb_built_xor);
  CCL_DEBUG_END_TIMED_BLOCK ();

  return result;
}

			/* --------------- */

static size_t
s_bx_size (sataf_mao *self)
{
  return sizeof (backward_xor);
}

			/* --------------- */

static void
s_bx_destroy (sataf_mao *self)
{
}

			/* --------------- */

static int
s_bx_no_cache (sataf_mao *self)
{
  return 1;
}

			/* --------------- */

static void
s_bx_is_root_of_scc (sataf_mao *self, sataf_sa *sa, int q0)
{
  backward_xor *bx = (backward_xor *) self;

  if (btf_is_untransient (sa) && ! bx->data->done )
    {
      sataf_msa *msa = sataf_msa_find_or_add (sa, q0);

      if (! ccl_hash_find (bx->data->result, msa))
	{
	  int q = (bx->q1 < 0) ? bx->q2 : bx->q1;
	  ccl_hash_insert (bx->data->result, (void *) (intptr_t) q);
	  ccl_debug ("VSP to compute for (%d,%d)\n", bx->q1, bx->q2);
	  bx->data->done = 1;
	}
      else
	sataf_msa_del_reference (msa);
    }
}

			/* --------------- */

static sataf_msa *
s_bx_simplify (sataf_mao *self)
{
  sataf_msa *R = NULL;
  backward_xor *bx = (backward_xor *) self;

  ccl_assert (bx->q1 <= bx->q2);

  if (bx->q1 == bx->q2 || bx->data->done)
    R = sataf_msa_zero (2);
  else 
    {
      struct comp_data *data = bx->data;
      int turn = s_state_turn (data, bx->q1, bx->q2);
      
      if (0 <= turn && turn < data->turn)
	{
	  ccl_assert (0 < turn);
	  R = sataf_msa_zero (2);
	  bx->data->done = 1;
	}
      else
	{
	  if (bx->q1 == -1)
	    {
	      uint32_t bind_init[] = { 0 };
	      sataf_sa *zero = sataf_sa_create_zero (2);
	      sataf_sa *sa = 
		sataf_sa_find_or_add (data->ea, &zero, bind_init);
	      R = sataf_msa_find_or_add (sa, bx->q2);
	      sataf_sa_del_reference (sa);
	      sataf_sa_del_reference (zero);
	    }
	  else if(data->couples[bx->q1])
	    {
	      struct int_list **pl;
	      
	      for (pl = &data->couples[bx->q1]; *pl; pl = &((*pl)->next))
		{
		  if ((*pl)->i == bx->q2)
		    {
		      struct int_list *c = *pl;
		      *pl = c->next;
		      ccl_delete (c);
		      break;
		    }
		}
	    }
	  if (turn == -1)
	    s_add_state (data, bx->q1, bx->q2);
	}
    }

  return R;
}

			/* --------------- */

static uint32_t
s_bx_get_alphabet_size (sataf_mao *self)
{
  return 2;
}

			/* --------------- */

#define s_bx_to_string NULL

			/* --------------- */

static void
s_compute_succ (struct comp_data *data, int q1, int q2, int b, int *p_succ1,
		int *p_succ2)
{
  int sq1 = q1 >= 0 ? sataf_ea_get_successor (data->ea, q1, b) : -1;
  int sq2 = q2 >= 0 ? sataf_ea_get_successor (data->ea, q2, b) : -1;
  
  if (sataf_ea_is_exit_state (sq1))
    sq1 = -1;
  else
    sq1 = sataf_ea_decode_succ_state (sq1);

  if (sataf_ea_is_exit_state (sq2))
    sq2 = -1;
  else
    sq2 = sataf_ea_decode_succ_state (sq2);  

  if (sq1 < sq2)
    {
      *p_succ1 = sq1;
      *p_succ2 = sq2;
    }
  else
    {
      *p_succ1 = sq2;
      *p_succ2 = sq1;
    }
}

			/* --------------- */

static sataf_mao * 
s_bx_succ (sataf_mao *self, uint32_t letter)
{
  sataf_mao *result;
  int sq1, sq2;
  backward_xor *bx = (backward_xor *) self;

  s_compute_succ (bx->data, bx->q1, bx->q2, letter, &sq1, &sq2);

  result = s_create_backward_xor (bx->data, sq1, sq2);

  return result;
}

			/* --------------- */

static int
s_bx_is_final (sataf_mao *self)
{
  backward_xor *bx = (backward_xor *) self;
  int q1 = bx->q1 >= 0 ? sataf_ea_is_final (bx->data->ea, bx->q1) : 0;
  int q2 = bx->q2 >= 0 ? sataf_ea_is_final (bx->data->ea, bx->q2) : 0;

  return (q1 && !q2) || (q2 && !q1);
}

			/* --------------- */

static int
s_bx_is_equal_to (sataf_mao *self, sataf_mao *other)
{
  backward_xor *bx1 = (backward_xor *) self;
  backward_xor *bx2 = (backward_xor *) other;

  return (bx1->data->ea == bx2->data->ea && bx1->q1 == bx2->q1 &&
	  bx1->q2 == bx2->q2);
}
			/* --------------- */

static unsigned int
s_bx_hashcode (sataf_mao *self)
{
  backward_xor *bx = (backward_xor *) self;

  return (7 * (intptr_t) bx->data->ea + 13 * bx->q1 + 19 * bx->q2);
}


			/* --------------- */

#define s_bx_to_dot NULL

			/* --------------- */

static sataf_mao *
s_create_backward_xor (struct comp_data *data, int q1, int q2)
{  
  static const sataf_mao_methods BACKWARD_XOR_METHODS = 
    {
      "SA-BACKWARD-XOR",
      s_bx_size,
      s_bx_destroy,
      s_bx_no_cache,
      s_bx_is_root_of_scc,
      s_bx_simplify,
      s_bx_get_alphabet_size,
      s_bx_to_string,
      s_bx_succ,
      s_bx_is_final,
      s_bx_is_equal_to,
      s_bx_hashcode,
      s_bx_to_dot
    };
  backward_xor *result = (backward_xor *) 
    sataf_mao_create (sizeof (backward_xor), &BACKWARD_XOR_METHODS);

  result->data = data;
  if (q1 < q2)
    {
      result->q1 = q1;
      result->q2 = q2;
    }
  else
    {
      result->q1 = q2;
      result->q2 = q1;
    }

  return (sataf_mao *) result;
}

			/* --------------- */

static struct pred **
s_compute_pred (sataf_ea *ea)
{
  int i;
  struct pred *p;
  int nb_states = sataf_ea_get_nb_states (ea);
  int prefix_size = 
    ((nb_states * sizeof (struct pred *))/sizeof (struct pred)) + 1;
  struct pred *free_preds = 
    ccl_calloc (sizeof (struct pred), 2 * nb_states + prefix_size);
  struct pred **result = (struct pred **) free_preds;
    
  free_preds += prefix_size;

  CCL_DEBUG_START_TIMED_BLOCK (("computing preds"));
  for (i = 0; i < nb_states; i++)
    {
      int b;

      for (b = 0; b < 2; b++)
	{
	  struct pred **pp;
	  int succ = sataf_ea_get_successor (ea, i, b);
	  if (sataf_ea_is_exit_state (succ))
	    continue;
	  succ = sataf_ea_decode_succ_state (succ);

	  for (pp = &result[succ]; *pp && (*pp)->b < b; pp = &((*pp)->next))
	    CCL_NOP ();

	  p = free_preds++;
	  p->s = i;
	  p->b = b;
	  p->next = *pp;
	  *pp = p;
	}
    }
  CCL_DEBUG_END_TIMED_BLOCK ();

  return result;
}

			/* --------------- */


static struct int_list **
s_compute_couples (sataf_ea *ea)
{
  int s;
  struct int_list **result;
  int nb_states; 
  struct pred **pred_rel;

  CCL_DEBUG_START_TIMED_BLOCK (("compute couples"));
  nb_states = sataf_ea_get_nb_states (ea);
  pred_rel = s_compute_pred (ea);
  result = ccl_new_array (struct int_list *, nb_states);
  for (s = 0; s < nb_states; s++)
    {
      struct pred *p1;
      
      for (p1 = pred_rel[s]; p1; p1 = p1->next)
	{
	  struct pred *p2;

	  for (p2 = p1->next; p2 && p2->b == p1->b; p2 = p2->next)
	    {
	      int q1, q2;
	      struct int_list *c;
	      if (p1->s < p2->s) { q1 = p1->s; q2 = p2->s; }
	      else { q1 = p2->s; q2 = p1->s; }

	      ccl_assert (q1 != q2);
	      c = ccl_new (struct int_list);
	      c->i = q2;
	      c->next = result[q1];
	      result[q1] = c;
	    }
	}
    }
  ccl_delete (pred_rel);
  CCL_DEBUG_END_TIMED_BLOCK ();

  return result;
}

			/* --------------- */

static const int PRIME_TABLE[] = {
  87719, 175447, 350899, 701819, 1403641, 2807303, 5614657, 11229331
};

static const size_t PRIME_TABLE_SIZE = 
  (sizeof (PRIME_TABLE) / sizeof (PRIME_TABLE[0]));

static const xor_state NULL_STATE = { -1, -1, -1 };

			/* --------------- */
static xor_state *
s_new_table (int size)
{
  int i;
  xor_state *s;
  xor_state *result = ccl_new_array (xor_state, size);

  for (s = result, i = 0; i < size; i++, s++)
    *s = NULL_STATE;

  return result;
}

			/* --------------- */

static void
s_init_state_space (struct comp_data *data)
{
  int index;
  data->turn = 0;
  data->nb_states = 0;
  
  for (index = 0; index < PRIME_TABLE_SIZE - 1; index++)
    if (PRIME_TABLE[index] > 2 * data->ea->nb_local_states)
      break;
  
  data->state_table_size_index = index;
  data->state_table_size = PRIME_TABLE[data->state_table_size_index];
  data->state_table = s_new_table (data->state_table_size);
}

			/* --------------- */

static void
s_terminate_state_space (struct comp_data *data)
{
  ccl_delete (data->state_table);
}

			/* --------------- */

static unsigned int
s_state_hash (int q1, int q2)
{
  return (673 * q1 + 1361 * q2) ^ 11229331;
}

			/* --------------- */

static int
s_is_null_state (xor_state *s)
{
  return s->q1 == -1 && s->q2 == -1 && s->turn == -1;
}

			/* --------------- */
static int
s_state_turn (struct comp_data *data, int q1, int q2)
{
  int i;
  int index = s_state_hash (q1, q2) % data->state_table_size;

  for (i = 0; i < data->state_table_size; i++)
    {
      xor_state *s = data->state_table + index;

      if (s_is_null_state (s))
	return -1;
      
      if (s->q1 == q1 && s->q2 == q2)
	{
	  ccl_assert (s->turn >= 0);
	  return s->turn;
	}

      index++;
      if (index == data->state_table_size)
	index = 0;
    }

  return -1;
}

			/* --------------- */

static void
s_insert_in_table (xor_state *table, int table_size, int q1, int q2, int turn)
{
  int i;
  int index = s_state_hash (q1, q2) % table_size;
  
  for (i = 0; i < table_size; i++)
    {
      xor_state *s = table + index;
	  
      if (s_is_null_state (s))
	{
	  s->q1 = q1;
	  s->q2 = q2;
	  s->turn = turn;	 
	  break;
	}
	  
      index++;
      if (index == table_size)
	index = 0;
    }
}

			/* --------------- */

static void
s_increase_state_table (struct comp_data *data)
{  
  int i;
  int new_size;
  xor_state *new_table;
  xor_state *s;
  
  if (data->state_table_size_index == PRIME_TABLE_SIZE - 1)
    new_size = 2 * data->state_table_size + 1;
  else
    {
      data->state_table_size_index++;
      new_size = PRIME_TABLE[data->state_table_size_index];
    }

  CCL_DEBUG_START_TIMED_BLOCK (("resize xor state space to %d", new_size));
  new_table = s_new_table (new_size);
  
  for (s = data->state_table, i = 0; i < data->state_table_size; i++, s++)
    {
      if (!s_is_null_state (s))
	s_insert_in_table (new_table, new_size, s->q1, s->q2, s->turn);
    }

  ccl_delete (data->state_table);
  data->state_table = new_table;
  data->state_table_size = new_size;
  CCL_DEBUG_END_TIMED_BLOCK ();
}

			/* --------------- */

static void
s_add_state (struct comp_data *data, int q1, int q2)
{
  if (data->nb_states == data->state_table_size)
    s_increase_state_table (data);
  s_insert_in_table (data->state_table, data->state_table_size, q1, q2, 
		     data->turn);
  data->nb_states++;
}


			/* --------------- */

static int 
s_actually_need_to_compute_xor_for (struct comp_data *data, int q1, int q2,
				    int depth)
{
  int b;
  int result = 1;
  int sq[2][2];

  if (depth == 0)
    return 1;

  for (b = 0; b < 2 && result; b++)
    {
      s_compute_succ (data, q1, q2, b, &sq[b][0], &sq[b][1]);
      if (s_state_turn (data, sq[b][0], sq[b][1]) != -1)
	result = 0;
    }

  if (result && depth > 1)
    {
      for (b = 0; b < 2 && result; b++)
	result = 
	  s_actually_need_to_compute_xor_for (data, sq[b][0], sq[b][1], 
					      depth - 1);
    }

  return result;
}
