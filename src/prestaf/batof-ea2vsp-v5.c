/*
 * batof-ea2vsp-v5.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-bittable.h>
#include "prestaf-batof-p.h"

static int
s_look_for_terminal_loop_start_point (sataf_ea *ea, int m, int from);

static void
s_compute_shortest_paths (int m, sataf_ea *ea, int q1,
			  ccl_bittable *lambdas, tla_vsp_t res);

static int
s_post (tla_vec_t res, int b, int i, tla_vec_t x);

static int *
s_compute_in_degrees (int m, sataf_ea *ea);

			/* --------------- */

#if 0
static void
s_display_lambdas (ccl_log_type log, int m, int nb_states,
		   ccl_bittable *lambdas);
#endif

static void
s_display_q0_loop (ccl_log_type log, sataf_ea *ea, int q0);

#if 0
static void
s_display_invgammas (ccl_log_type log, int m, int nb_states,
		     tla_vec_t ** invgammas);
#endif

			/* --------------- */

ccl_bittable *
btf_ea_to_vsp_v5 (tla_vsp_t res, sataf_ea *ea, int *pq0)
{
  int m = tla_vsp_get_size (res);
  ccl_bittable *lambdas =
    ccl_bittable_create (m * (ea->nb_local_states + ea->nb_exit_states));
  int q0 = s_look_for_terminal_loop_start_point (ea, m, *pq0);

  if (ccl_debug_is_on)
    s_display_q0_loop (CCL_LOG_DEBUG, ea, q0);

  s_compute_shortest_paths (m, ea, q0, lambdas, res);


  tla_vsp_canonicalize (res);


#if 0
  if (1)
    {
      int q = *pq0;
      tla_vsp_t aux;
      ccl_bittable *l;

      t0 = clock ();
      tla_vsp_init (aux, tla_vsp_get_size (res));
      l = btf_ea_to_vsp_v3 (aux, ea, &q);

      if (!tla_vsp_equals (aux, res))
	{
	  ccl_debug ("NOK=\n");
	  tla_vsp_log_reduced (CCL_LOG_DEBUG, res);
	  ccl_debug ("\nOK=\n");
	  tla_vsp_log_reduced (CCL_LOG_DEBUG, aux);
	  ccl_debug ("\n");
	  if (0)
	    {
	      int x;
	      tla_vsp_t v;

	      tla_vsp_init (v, m);

	      for (x = 0; x < m && !tla_vsp_equals (aux, res); x++)
		{
		  tla_vsp_inv_gamma0_v1 (v, res);
		  tla_vsp_set (res, v);
		  ccl_debug ("x=%d\n", x);
		  tla_vsp_log_reduced (CCL_LOG_DEBUG, res);
		  ccl_debug ("\n");
		}
	      tla_vsp_clear (v);
	      abort ();
	    }
	  ccl_assert (tla_vsp_equals (aux, res));
	}

      ccl_bittable_delete (l);
      tla_vsp_clear (aux);
    }
#endif

  *pq0 = q0;

  return lambdas;
}

			/* --------------- */

struct invgamma_minpath
{
  struct invgamma_minpath *next;
  int refcount;
  int lambda;
  tla_vec_t invgamma;
};

			/* --------------- */

static struct invgamma_minpath **
s_get_invgamma (int q, int lambda, struct invgamma_minpath **vecs)
{
  struct invgamma_minpath **pim = vecs + q;

  for (; *pim && (*pim)->lambda < lambda; pim = &((*pim)->next))
    /* empty */ ;

  return pim;
}

			/* --------------- */

static struct invgamma_minpath *
s_allocate_invgamma (int q, int lambda, int m, int *deg,
		     struct invgamma_minpath *next)
{
  struct invgamma_minpath *im = ccl_new (struct invgamma_minpath);

  im->next = next;
  im->refcount = deg[q];
  im->lambda = lambda;
  tla_vec_init (im->invgamma, m);

  return im;
}

			/* --------------- */

static struct invgamma_minpath **
s_add_invgamma (int q, int lambda, int m, struct invgamma_minpath **vecs,
		int *degrees)
{
  struct invgamma_minpath **pim = s_get_invgamma (q, lambda, vecs);
  struct invgamma_minpath *im =
    s_allocate_invgamma (q, lambda, m, degrees, *pim);

  ccl_assert (*pim == NULL || (*pim)->lambda > lambda);

  *pim = im;

  return pim;
}

			/* --------------- */

static void
s_unref_invgamma (struct invgamma_minpath **pim)
{
  struct invgamma_minpath *im = *pim;

  ccl_assert (im->refcount > 0);

  if (--im->refcount > 0)
    return;

  tla_vec_clear (im->invgamma);
  *pim = im->next;

  ccl_delete (im);
}

			/* --------------- */

static void
s_clean_invgammas (int nb_states, int *degrees, struct invgamma_minpath **IMS)
{
  int i;

  for (i = 0; i < nb_states; i++)
    {
      struct invgamma_minpath *im;
      struct invgamma_minpath *next;

      for (im = IMS[i]; im; im = next)
	{
	  next = im->next;
	  tla_vec_clear (im->invgamma);
	  ccl_delete (im);
	}
    }
  ccl_delete (IMS);
  ccl_delete (degrees);
}


			/* --------------- */


static void
s_compute_shortest_paths (int m, sataf_ea *ea, int q0, ccl_bittable *lambdas, 
			  tla_vsp_t res)
{
  int D = 0;
  ccl_list *Q = ccl_list_create ();
  tla_vec_t v1, v2;
  int nb_states = ea->nb_local_states;
  int *depths = ccl_new_array (int, nb_states);
  int *degrees = s_compute_in_degrees (m, ea);
  struct invgamma_minpath **IMS =
    ccl_new_array (struct invgamma_minpath *, nb_states);

  tla_vec_init (v1, m);
  tla_vec_init (v2, m);

  ccl_bittable_set (lambdas, q0 * m);
  (void) s_add_invgamma (q0, 0, m, IMS, degrees);
  ccl_list_add (Q, (void *) (intptr_t) (q0 * m));

  {
    int i;

    for (i = 0; i < nb_states; i++)
      depths[i] = -1;
    depths[q0] = 0;
  }

  while (!ccl_list_is_empty (Q))
    {
      unsigned int b;
      int index_q = (intptr_t) ccl_list_take_first (Q);
      int q = index_q / m;
      int lambda_q = index_q % m;
      struct invgamma_minpath **pim_q =
	IMS ? s_get_invgamma (q, lambda_q, IMS) : NULL;

      for (b = 0; b < 2; b++)
	{
	  int index_qp;
	  int lambda_qp = (lambda_q + 1) % m;
	  int qp = sataf_ea_get_successor (ea, (unsigned int) q, b);
	  struct invgamma_minpath **pim_qp;

	  if (sataf_ea_is_exit_state (qp))
	    continue;

	  qp = sataf_ea_decode_succ_state (qp);
	  if (depths[qp] == -1)
	    depths[qp] = depths[q] + 1;
	  index_qp = m * qp + lambda_qp;
	  pim_qp = IMS ? s_get_invgamma (qp, lambda_qp, IMS) : NULL;

	  ccl_assert (D == m || (*pim_qp) == NULL || (*pim_qp)->refcount > 0);

	  if (ccl_bittable_has (lambdas, index_qp))
	    {
	      if (pim_qp)
		{
		  ccl_assert (*pim_qp != NULL);

		  s_post (v1, b, lambda_q, (*pim_q)->invgamma);
		  tla_vec_sub (v2, v1, (*pim_qp)->invgamma);
		  D += tla_vsp_add_generator (res, res, v2);

		  if (D == m)
		    {
		      s_clean_invgammas (nb_states, degrees, IMS);
		      IMS = NULL;
		      degrees = NULL;
		    }
		}
	      else
		{
		  ccl_assert (IMS == NULL);
		}
	    }
	  else
	    {
	      ccl_assert (!ccl_bittable_has (lambdas, index_qp));
	      /*ccl_assert( ! ccl_list_has(Q,(void *)index_qp) ); */

	      if (pim_qp)
		{
		  *pim_qp =
		    s_allocate_invgamma (qp, lambda_qp, m, degrees, *pim_qp);
		  s_post ((*pim_qp)->invgamma, b, lambda_q,
			  (*pim_q)->invgamma);
		}

	      ccl_bittable_set (lambdas, index_qp);
	      ccl_list_add (Q, (void *) (intptr_t) index_qp);
	    }

	  if (IMS)
	    {
	      int reget_q = ((*pim_qp)->next == *pim_q);
	      s_unref_invgamma (pim_qp);
	      if (reget_q)
		pim_q = s_get_invgamma (q, lambda_q, IMS);
	    }
	}

      if (IMS)
	s_unref_invgamma (pim_q);
    }

  {
    int i;
    int max_depth = 0;
    int mean_depth = 0;

    for (i = 0; i < nb_states; i++)
      {
	if (depths[i] > max_depth)
	  max_depth = depths[i];
	mean_depth += depths[i];
      }
    ccl_debug ("|S|=%d, max_depth = %d mean_depth = %f\n", nb_states,
	       max_depth, (float) mean_depth / (float) nb_states);
    ccl_delete (depths);
  }
  tla_vec_clear (v1);
  tla_vec_clear (v2);
  ccl_list_delete (Q);

  if (IMS != NULL)
    s_clean_invgammas (nb_states, degrees, IMS);
}

			/* --------------- */

static int
s_look_for_terminal_loop_start_point (sataf_ea *ea, int m, int from)
{
  int result = -1;
  ccl_bittable *on_path = ccl_bittable_create (ea->nb_local_states);
  int dist_to_from = 0;
  unsigned int q = from;

  if (!ea->is_final[from])
    {
      ccl_list *Q = ccl_list_create ();
      int *depths = ccl_new_array (int, ea->nb_local_states);

      for (q = 0; q < ea->nb_local_states; q++)
	depths[q] = -1;
      depths[from] = 0;
      dist_to_from = -1;

      ccl_list_add (Q, (void *) (intptr_t) from);
      while (!ccl_list_is_empty (Q) && dist_to_from < 0)
	{
	  unsigned int b;

	  q = (intptr_t) ccl_list_take_first (Q);

	  for (b = 0; b < 2; b++)
	    {
	      int qp = sataf_ea_get_successor (ea, q, b);

	      if (sataf_ea_is_exit_state (qp))
		continue;

	      qp = sataf_ea_decode_succ_state (qp);
	      if (depths[qp] < 0)
		{
		  depths[qp] = (depths[q] + 1) % m;
		  if (ea->is_final[qp])
		    {
		      q = qp;
		      dist_to_from = depths[qp];
		      break;
		    }
		  else
		    ccl_list_add (Q, (void *) (intptr_t) qp);
		}
	    }
	}
      ccl_list_delete (Q);
      ccl_assert (ea->is_final[q]);
      ccl_delete (depths);
    }

  ccl_bittable_set (on_path, q);

  while (result < 0)
    {
      unsigned int qp = sataf_ea_get_successor (ea, q, 0);

      ccl_assert (sataf_ea_is_local_state (qp));
      q = sataf_ea_decode_succ_state (qp);
      dist_to_from = (dist_to_from + 1) % m;
      if (ccl_bittable_has (on_path, q))
	result = q;
      else
	ccl_bittable_set (on_path, q);
    }

  ccl_bittable_delete (on_path);

  if (dist_to_from != 0)
    {
      while (dist_to_from != 0)
	{
	  unsigned int qp = sataf_ea_get_successor (ea, q, 0);

	  ccl_assert (sataf_ea_is_local_state (qp));
	  q = sataf_ea_decode_succ_state (qp);
	  dist_to_from = (dist_to_from + 1) % m;
	}
      result = q;
    }

  return result;
}

			/* --------------- */

static int
s_post (tla_vec_t res, int b, int i, tla_vec_t x)
{
  int m = tla_vec_get_size (x);

  if (i < m - 1)
    {
      if (b == 0)
	tla_vec_set (res, x);
      else
	tla_vec_sub_ith_den (res, x, i);
    }
  else
    {
      if (b == 0)
	tla_vec_div_2 (res, x);
      else
	tla_vec_sub_last_den_div_2 (res, x);
    }

  return (i + 1) % m;
}

			/* --------------- */

#if 0
static void
s_display_lambdas (ccl_log_type log, int m, int nb_states,
		   ccl_bittable *lambdas)
{
  int q;

  ccl_log (log, "LAMBDAS=\n");
  for (q = 0; q < nb_states; q++)
    {
      int l;

      ccl_log (log, "%d : ", q);

      for (l = 0; l < m; l++)
	{
	  if (ccl_bittable_has (lambdas, q * m + l))
	    ccl_log (log, "%d ", l);
	}
      ccl_log (log, "\n");
    }
}
#endif
			/* --------------- */

static void
s_display_q0_loop (ccl_log_type log, sataf_ea *ea, int q0)
{
  unsigned int q = q0;

  ccl_log (log, "LOOP ON q0=%d :\n", q0);

  do
    {
      ccl_log (log, "%d -> ", q);
      q = sataf_ea_get_successor (ea, q, 0);
      q = sataf_ea_decode_succ_state (q);
    }
  while ((int) q != q0);
  ccl_log (log, "%d\n", q);
}


			/* --------------- */
#if 0
static void
s_display_invgammas (ccl_log_type log, int m, int nb_states,
		     tla_vec_t ** invgammas)
{
  int q;

  ccl_log (log, "INVGAMMA=\n");
  for (q = 0; q < nb_states; q++)
    {
      int l;

      for (l = 0; l < m; l++)
	{
	  if (invgammas[q * m + l] != NULL)
	    {
	      ccl_log (log, "(q=%d,i=%d)=", q, l);
	      tla_display_vec_reduced (log, *(invgammas[q * m + l]));
	      ccl_log (log, "\n");
	    }
	}
    }
}
#endif
			/* --------------- */

static int *
s_compute_in_degrees (int m, sataf_ea *ea)
{
  unsigned int i, nb_states = ea->nb_local_states;
  int *in_degrees = ccl_new_array (int, nb_states);

  for (i = 0; i < nb_states; i++)
    {
      unsigned int b;

      in_degrees[i]++;

      for (b = 0; b < 2; b++)
	{
	  int succ = sataf_ea_get_successor (ea, i, b);

	  if (sataf_ea_is_exit_state (succ))
	    continue;
	  succ = sataf_ea_decode_succ_state (succ);
	  in_degrees[succ]++;
	}
    }

  return in_degrees;
}
