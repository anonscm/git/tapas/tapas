/*
 * batof-detection.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include "prestaf-batof-p.h"

struct YV {  
  tla_vsp_t vsp;

  int m, n;
  tla_matrix_t alphas; /* m x n */
  mpz_t *C; /* n cells */
  tla_matrix_t Y; /* m x m cells */
  tla_matrix_t V; /* n x n cells */
};

typedef struct detection_info {
  tla_vec_t a;
  int vsp_index;   /* = -1 if the associated set is empty */
  sataf_msa *pred; /* the letter b is encoded by the first bit of pred i.e. 
		      if pred is even then b = 0 else b = 1 */  
} detection_info;

			/* --------------- */

static struct YV **
s_compute_YVs (tla_vsp_t V);

static struct YV *
s_new_YV (tla_vsp_t V);

static void
s_delete_YV (struct YV *yv);

static int
s_compute_one_element (tla_vec_t a, struct YV *yv, tla_vec_t x0);

			/* --------------- */

int
btf_is_detectable (sataf_msa *X, tla_vec_t a, tla_vsp_t V, sataf_msa *final,
		   tla_vec_t rho_0, tla_vec_t rho_1, int *p_l0, int *p_l1)
{
  int i;
  int detectable = 1;
  int m = V->size;
  int last_info = 0;
  detection_info *dinfos;
  detection_info *inf;
  sataf_msa_info Xinfo;
  struct YV **yv = s_compute_YVs (V);
  ccl_hash *states = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_list *queue = ccl_list_create ();
  tla_vec_t tmp;
  tla_group_t G;

  tla_vec_init (tmp, m);
  tla_group_init (G, m);

  sataf_msa_get_info (X, &Xinfo);
  dinfos = ccl_new_array (detection_info, Xinfo.nb_states);
  tla_vec_init (dinfos[last_info].a, m);

  if (! s_compute_one_element (dinfos[last_info].a, yv[0], a))
    {
      tla_vec_clear (dinfos[last_info].a);
      dinfos[last_info].vsp_index = -1;
    }
  else
    {
      dinfos[last_info].vsp_index = 0;
      dinfos[last_info].pred = NULL;

      X = sataf_msa_add_reference (X);
      ccl_hash_find (states, X);
      ccl_hash_insert (states, dinfos+last_info);
      ccl_list_add (queue, X);
      last_info++;

      while (! ccl_list_is_empty (queue) && detectable)
	{
	  int b;
	  sataf_msa *msa = (sataf_msa *) ccl_list_take_first (queue);

	  ccl_hash_find (states, msa);
	  inf = (detection_info *) ccl_hash_get (states);
	  ccl_assert (inf != NULL);

	  for (b = 0; b < 2 && detectable; b++)
	    {
	      detection_info inf_succ;
	      sataf_msa *succ = sataf_msa_succ (msa, b);

	      inf_succ.pred = b ? CCL_BITPTR(sataf_msa *, msa) : msa;
	      if (inf->vsp_index == -1)
		inf_succ.vsp_index = -1;
	      else
		{		  
		  tla_vec_init (inf_succ.a, m);
		  inf_succ.vsp_index = (inf->vsp_index + 1) % m;

		  tla_vec_inv_gamma (tmp, inf->a, b);
		  
		  if (! s_compute_one_element (inf_succ.a,
					       yv[inf_succ.vsp_index], tmp))
		    {
		      tla_vec_clear (inf_succ.a);
		      inf_succ.vsp_index = -1;
		    }
		}

	      if (ccl_hash_find (states, succ))
		{
		  detection_info *inf_succ2 = ccl_hash_get (states);

		  if (inf_succ.vsp_index >= 0 && inf_succ2->vsp_index >= 0)
		    {

		      ccl_assert (inf_succ.vsp_index == inf_succ2->vsp_index);

		      tla_vec_sub (tmp, inf_succ.a, inf_succ2->a);
		      tla_group_Z_cap_vsp (G, yv[inf_succ2->vsp_index]->vsp);
		      detectable = tla_group_has (G, tmp, NULL);
		    }
		  else 
		    {
		      detectable = (inf_succ.vsp_index == -1 &&  
				    inf_succ2->vsp_index == -1);
		    }

		  if (! detectable)
		    {
		      int l = b;
		      detection_info *i;
		      sataf_msa *q = msa;

		      *p_l0 = 0;
		      btf_misc_compute_rho (succ, final, rho_0, p_l0);
		      tla_vec_set (rho_1, rho_0);
		      *p_l1 = *p_l0;
		      (*p_l0)++;

		      do 
			{
			  tla_vec_inv_gamma (tmp, rho_0, l);
			  tla_vec_set (rho_0, tmp);
			  (*p_l0)++;
			  ccl_hash_find (states, q);
			  i = (detection_info *) ccl_hash_get (states);
			  l = CCL_PTRHASBIT(i->pred)?1:0;
			  q = CCL_BITPTR2PTR(sataf_msa *, i->pred);
			}
		      while (q != X);

		      if (inf_succ2->pred != NULL)
			{
			  b = CCL_PTRHASBIT(inf_succ2->pred);
			  q = CCL_BITPTR2PTR(sataf_msa *, inf_succ2->pred);
			  (*p_l1)++;

			  do 
			    {
			      tla_vec_inv_gamma (tmp, rho_1, l);
			      tla_vec_set (rho_1, tmp);
			      (*p_l1)++;
			      ccl_hash_find (states, q);
			      i = (detection_info *) ccl_hash_get (states);
			      l = CCL_PTRHASBIT(i->pred)?1:0;
			      q = CCL_BITPTR2PTR(sataf_msa *, i->pred);
			    }
			  while (q != X);
			}
		    }
		}
	      else
		{
		  dinfos[last_info].vsp_index = inf_succ.vsp_index;
		  if (dinfos[last_info].vsp_index >= 0)
		    tla_vec_init_set (dinfos[last_info].a, inf_succ.a);
		  dinfos[last_info].pred = inf_succ.pred;
		  ccl_hash_insert (states, &dinfos[last_info]);
		  succ = sataf_msa_add_reference (succ);
		  ccl_list_add (queue, succ);
		  last_info++;
		}
	  
	      if (inf_succ.vsp_index >= 0)
		tla_vec_clear (inf_succ.a);

	      sataf_msa_del_reference (succ);
	    }
	}
    }

  tla_vec_clear (tmp);
  tla_group_clear (G);
  ccl_list_delete (queue);
  {
    ccl_pointer_iterator *it = ccl_hash_get_keys (states);

    while (ccl_iterator_has_more_elements (it))
      {
	sataf_msa *msa = ccl_iterator_next_element (it);
	sataf_msa_del_reference (msa);
      }
    ccl_iterator_delete (it);
  }
  ccl_hash_delete (states);
  for (i = 0; i < m; i++)
    s_delete_YV (yv[i]);
  ccl_delete (yv);

  for (i = 0; i < last_info; i++)
    {
      if (dinfos[i].vsp_index != -1)
	tla_vec_clear (dinfos[i].a);      
    }
  ccl_delete (dinfos);

  return detectable;
}

			/* --------------- */

static struct YV **
s_compute_YVs(tla_vsp_t V)
{
  int k;
  tla_vsp_t V0;
  int m = V->size;
  struct YV **result = ccl_new_array (struct YV *, m);

  tla_vsp_init (V0, V->size);
  tla_vsp_set (V0, V);

  for (k = 0; k < m; k++)
    {
      result[k] = s_new_YV (V0);
      tla_vsp_inv_gamma0(V0, result[k]->vsp);
    }

  tla_vsp_clear (V0);

  return result;
}

			/* --------------- */

static struct YV *
s_new_YV (tla_vsp_t V)
{
  int i;
  tla_matrix_t A;
  struct YV *yv = ccl_new (struct YV);

  tla_vsp_init (yv->vsp, V->size);
  tla_vsp_set (yv->vsp, V);

  tla_vsp_to_alphas (V, yv->alphas);
  yv->m = yv->alphas->m;
  yv->n = yv->alphas->n;

  yv->C = ccl_new_array (mpz_t, yv->m);

  tla_matrix_init (yv->Y, yv->m, yv->m);
  tla_matrix_init (yv->V, yv->n, yv->n);
  
  tla_matrix_init_set (A, yv->alphas);
  
  tla_matrix_compute_snf (A, A, NULL, NULL, yv->Y, NULL, yv->V);

  for (i = 1; i <= yv->m; i++)
    mpz_init_set (yv->C[i-1], MM (A, i, i));
  tla_matrix_clear (A);

  return yv;
}

			/* --------------- */

static void
s_delete_YV(struct YV *yv)
{
  int i;

  tla_vsp_clear (yv->vsp);
  
  for (i = 0; i < yv->m; i++)
    mpz_clear (yv->C[i]);
  ccl_delete (yv->C);

  tla_matrix_clear (yv->alphas);
  tla_matrix_clear (yv->Y);
  tla_matrix_clear (yv->V);
  ccl_delete (yv);
}

			/* --------------- */


static int
s_compute_one_element (tla_vec_t a, struct YV *yv, tla_vec_t x0)
{
  int result;
  int i;
  tla_vec_t c;
  tla_vec_t z;

  if (yv->n == yv->m)
    tla_vec_set_zero (a);
  else
    {
      ccl_pre (x0->size == yv->n);
      ccl_assert (yv->n > yv->m);

      tla_vec_init (c, yv->m);
      tla_vec_init (z, yv->n); /* put more cells in order to get 0s */

      tla_matrix_mul_vector (c, yv->alphas, x0);
      tla_matrix_mul_vector (z, yv->Y, c);
      tla_vec_canonicalize (z);

      result = mpz_cmp_ui (z->den, 1) == 0;
      for (i = 0; i < yv->m && result; i++)
	{
	  result = mpz_divisible_p (z->num[i], yv->C[i]);
	  if (result)
	    mpz_cdiv_q (z->num[i], z->num[i], yv->C[i]);
	}

      if (result)
	{
	  ccl_assert (a->size == yv->n);
	  tla_matrix_mul_vector (a, yv->V, z);      
	}
      tla_vec_clear (z);
      tla_vec_clear (c);

      ccl_assert (! result || mpz_cmp_ui (a->den, 1) == 0);
    }

  return result;
}

