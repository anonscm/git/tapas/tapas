/*
 * batof-formulas.h -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __BATOF_FORMULAS_H__
# define __BATOF_FORMULAS_H__

# include "prestaf-batof-p.h"

struct global_definition
{
  ccl_ustring id;
  armoise_tree *formula;
};

struct msa2form_compilation_data
{
  int m;
  ccl_ustring *varnames;
  ccl_ustring nat_power_m_id;
  ccl_ustring Z_power_m_id;
  ccl_list *definitions;
  ccl_hash *V_defs;
  ccl_hash *G_defs;
  ccl_hash *vectors_defs;
  ccl_hash *H_defs;
  ccl_hash *patterns_defs;

  ccl_hash *mi;
  ccl_hash *def_ids;
};

			/* --------------- */

extern void
btf_init_compilation_data (struct msa2form_compilation_data *data, int m,
			   const char * const *varnames);

extern void
btf_clear_compilation_data (struct msa2form_compilation_data *data);

extern armoise_tree *
btf_add_definition (struct msa2form_compilation_data *data, const char *s,
		    armoise_tree *F);

/*!
 * let 
 *   SynForm := R;
 * in
 *  { (X[0], ..., X[m-1]) | (X[0], ..., X[m-1]) in (nat, ..., nat) && SynForm };
 */
extern armoise_tree *
btf_form_crt_synform (struct msa2form_compilation_data *cd, armoise_tree *P);

extern armoise_tree *
btf_form_crt_definition (const char *id, armoise_tree *P);

extern armoise_tree *
btf_form_crt_V_definition (struct msa2form_compilation_data *cd, 
			   tla_vsp_t V);

extern armoise_tree *
btf_form_crt_Zm_definition (const char *id, int m);

/*!
 * $Z^m \cap (x_0 + (V_G \cap H))$
 */
extern armoise_tree *
btf_form_crt_VH_definition (struct msa2form_compilation_data *cd, 
			    const char *id, tla_vec_t rho, tla_vec_t xhi, 
			    tla_vsp_t V, tla_vec_t alpha, int op);

# define btf_form_get_def_id(def) ((def)->child->value.id_value)

extern armoise_tree *
btf_form_crt_H_definition (struct msa2form_compilation_data *cd, 
			   ccl_ustring Vid, tla_vec_t alpha, int op);

extern armoise_tree *
btf_form_crt_PH_definition (struct msa2form_compilation_data *cd, 
			    const char *id, ccl_ustring P, 
			    tla_vec_t rho, tla_vec_t xhi, 
			    tla_vsp_t V, tla_vec_t alpha, int op);

extern armoise_tree *
btf_form_crt_P_definition (struct msa2form_compilation_data *cd, 
			   const char *id, tla_vec_t rho_w, ccl_list *B, 
			   tla_group_t G);

extern armoise_tree *
btf_form_crt_pattern_definition (struct msa2form_compilation_data *cd, 
				 ccl_list *B, tla_group_t G);

extern armoise_tree *
btf_form_crt_G_definition (struct msa2form_compilation_data *cd, tla_group_t G);

extern armoise_tree *
btf_form_crt_vec_definition (struct msa2form_compilation_data *cd, 
			     tla_vec_t v);

extern armoise_tree *
btf_form_trivial_predicate (struct msa2form_compilation_data *cd, int is_empty);

extern armoise_tree *
btf_form_crt_vector_of_ids (int m, ccl_ustring *X);

extern armoise_tree *
btf_form_zero_vec_predicate (int m);

extern armoise_tree *
btf_form_crt_x_in_nat_power_m (struct msa2form_compilation_data *cd);

extern armoise_tree *
btf_form_crt_nat_power_m (struct msa2form_compilation_data *cd);

extern armoise_tree *
btf_form_crt_Z_power_m (struct msa2form_compilation_data *cd);

extern armoise_tree *
btf_form_crt_gamma (int m, ccl_ustring *dummyX, armoise_tree *phi, int b, 
		    int index);

extern armoise_tree *
btf_form_crt_e_m_i_predicate (int m, int i);

extern armoise_tree *
btf_form_vec_to_armoise_predicate (tla_vec_t v);

extern armoise_tree *
btf_form_vec_to_armoise_term (tla_vec_t v);

extern armoise_tree *
btf_form_simplify_predicate (armoise_tree *P);

#endif /* ! __BATOF_FORMULAS_H__ */
