/*
 * prestaf-automata.h -- MAO constructors for Presburger formulas
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __PRESTAF_AUTOMATA_H__
# define __PRESTAF_AUTOMATA_H__

# include <ccl/ccl-hash.h>
# include <sataf/sataf.h>

BEGIN_C_DECLS

# define LCA_EQ   0
# define LCA_GT   1
# define LCA_GEQ  2
# define LCA_LT   3
# define LCA_LEQ  5

/**
 * Encode a.x op b with op in { =, >, >=, <, <= }
 */
extern sataf_mao * 
prestaf_crt_linear_constraint_automaton(int type, const int *alpha, 
					int alpha_size, int b);

/**
 *
 */
extern sataf_mao * 
prestaf_crt_add_variable_automaton(sataf_msa *msa, uint32_t i, uint32_t n);

/**
 *
 */
extern sataf_mao * 
prestaf_crt_inv_project_automaton(sataf_msa *msa, int width, 
				  const int *selection, int selsize);

/**
 *
 */
extern sataf_mao * 
prestaf_crt_apply_automaton(sataf_msa *R, sataf_msa *A);

/**
 *
 */
extern sataf_mao * 
prestaf_crt_invapply_automaton(sataf_msa *R, sataf_msa *A);

/**
 *
 */
extern sataf_mao * 
prestaf_crt_quantifier_automaton(int forall, uint32_t i, uint32_t n, 
				 sataf_msa *fset);

extern sataf_mao * 
prestaf_crt_quantifier_automaton2(int forall, uint32_t i, uint32_t n, 
				  sataf_msa *fset);

extern sataf_mao * 
prestaf_crt_unfolding_automaton(int m,  sataf_msa *fset);

extern void
prestaf_unfold_to_dot (ccl_log_type log, sataf_mao *self, const char **sigma,
		       const char *graph_name, const char *graph_type);

extern sataf_mao *
prestaf_crt_intersect_new_cond (sataf_msa *X, sataf_msa *Y, ccl_hash *Ycond);

extern sataf_mao *
prestaf_crt_new_cond (sataf_msa *X, ccl_hash *Xcond);

extern sataf_mao *
prestaf_crt_from_exit_automaton (int s0, sataf_ea *ea, const uint8_t *final);

END_C_DECLS

#endif /* ! __PRESTAF_AUTOMATA_H__ */
