/*
 * inter-new-cond.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-bittable.h>
#include "prestaf-automata.h"

struct hash 
{
  int refcount;
  ccl_hash *table;
};

typedef struct intersect_condition_1_st
{
  sataf_mao super;
  sataf_msa *msa;
  struct hash *acond;
} intersect_condition_1;

typedef struct intersect_condition_2_st
{
  sataf_mao super;
  sataf_msa *X;
  sataf_msa *Y;
  struct hash *acond;
} intersect_condition_2;

			/* --------------- */

#define s_new_cond_size s_intersect_condition_1_size 

#define s_new_cond_destroy s_intersect_condition_1_destroy 

#define s_new_cond_no_cache s_intersect_condition_1_no_cache 

#define s_new_cond_is_root_of_scc s_intersect_condition_1_is_root_of_scc

#define s_new_cond_simplify s_intersect_condition_1_simplify 

#define s_new_cond_get_alphabet_size s_intersect_condition_1_get_alphabet_size 

#define s_new_cond_to_string s_intersect_condition_1_to_string 

static sataf_mao * 
s_new_cond_succ (sataf_mao *self, uint32_t letter);

static int
s_new_cond_is_final (sataf_mao *self);

#define s_new_cond_is_equal_to s_intersect_condition_1_is_equal_to 

#define s_new_cond_hashcode s_intersect_condition_1_hashcode 

#define s_new_cond_to_dot s_intersect_condition_1_to_dot 

			/* --------------- */

static size_t
s_intersect_condition_1_size (sataf_mao *self);

static void
s_intersect_condition_1_destroy (sataf_mao *self);

#define s_intersect_condition_1_no_cache NULL

#define s_intersect_condition_1_is_root_of_scc NULL

#define s_intersect_condition_1_simplify NULL

static uint32_t
s_intersect_condition_1_get_alphabet_size (sataf_mao *self);

#define s_intersect_condition_1_to_string NULL

static sataf_mao * 
s_intersect_condition_1_succ (sataf_mao *self, uint32_t letter);

static int
s_intersect_condition_1_is_final (sataf_mao *self);

static int
s_intersect_condition_1_is_equal_to (sataf_mao *self, sataf_mao *other);

static unsigned int
s_intersect_condition_1_hashcode (sataf_mao *self);

#define s_intersect_condition_1_to_dot NULL

			/* --------------- */

static size_t
s_intersect_condition_2_size (sataf_mao *self);

static void
s_intersect_condition_2_destroy (sataf_mao *self);

#define s_intersect_condition_2_no_cache NULL

#define s_intersect_condition_2_is_root_of_scc NULL

#define s_intersect_condition_2_simplify NULL

static uint32_t
s_intersect_condition_2_get_alphabet_size (sataf_mao *self);

#define s_intersect_condition_2_to_string NULL

static sataf_mao * 
s_intersect_condition_2_succ (sataf_mao *self, uint32_t letter);

static int
s_intersect_condition_2_is_final (sataf_mao *self);

static int
s_intersect_condition_2_is_equal_to (sataf_mao *self, sataf_mao *other);

static unsigned int
s_intersect_condition_2_hashcode (sataf_mao *self);

#define s_intersect_condition_2_to_dot NULL

			/* --------------- */

static const sataf_mao_methods NEW_COND_METHODS = 
  {
    "SA-NEW_COND",
    s_new_cond_size,
    s_new_cond_destroy,
    s_new_cond_no_cache,
    s_new_cond_is_root_of_scc,
    s_new_cond_simplify,
    s_new_cond_get_alphabet_size,
    s_new_cond_to_string,
    s_new_cond_succ,
    s_new_cond_is_final,
    s_new_cond_is_equal_to,
    s_new_cond_hashcode,
    s_new_cond_to_dot
  };

			/* --------------- */

static const sataf_mao_methods INTERSECT_CONDITION_1_METHODS = 
  {
    "SA-INTERSECT_CONDITION_1",
    s_intersect_condition_1_size,
    s_intersect_condition_1_destroy,
    s_intersect_condition_1_no_cache,
    s_intersect_condition_1_is_root_of_scc,
    s_intersect_condition_1_simplify,
    s_intersect_condition_1_get_alphabet_size,
    s_intersect_condition_1_to_string,
    s_intersect_condition_1_succ,
    s_intersect_condition_1_is_final,
    s_intersect_condition_1_is_equal_to,
    s_intersect_condition_1_hashcode,
    s_intersect_condition_1_to_dot
  };

			/* --------------- */

static const sataf_mao_methods INTERSECT_CONDITION_2_METHODS = 
  {
    "SA-INTERSECT_CONDITION_2",
    s_intersect_condition_2_size,
    s_intersect_condition_2_destroy,
    s_intersect_condition_2_no_cache,
    s_intersect_condition_2_is_root_of_scc,
    s_intersect_condition_2_simplify,
    s_intersect_condition_2_get_alphabet_size,
    s_intersect_condition_2_to_string,
    s_intersect_condition_2_succ,
    s_intersect_condition_2_is_final,
    s_intersect_condition_2_is_equal_to,
    s_intersect_condition_2_hashcode,
    s_intersect_condition_2_to_dot
  };

			/* --------------- */

static sataf_mao *
s_crt_new_cond (sataf_msa *X, struct hash *acond);

static sataf_mao *
s_crt_intersect_1 (sataf_msa *X, struct hash *acond);

static sataf_mao *
s_crt_intersect_2 (sataf_msa *X, sataf_msa *Y, struct hash *acond);

			/* --------------- */

sataf_mao *
prestaf_crt_intersect_new_cond (sataf_msa *X, sataf_msa *Y, ccl_hash *Ycond)
{
  sataf_mao *result; 
  struct hash *h = ccl_new (struct hash);

  h->refcount = 0;
  h->table = Ycond;

  if (X == Y)
    result = s_crt_intersect_1 (X, h);
  else
    result = s_crt_intersect_2 (X, Y, h);

  return result;
}

			/* --------------- */

sataf_mao *
prestaf_crt_new_cond (sataf_msa *X, ccl_hash *Xcond)
{
  sataf_mao *result; 
  struct hash *h = ccl_new (struct hash);

  h->refcount = 0;
  h->table = Xcond;
  result = s_crt_new_cond (X, h);

  return result;
}

			/* --------------- */

static sataf_mao *
s_crt_new_cond (sataf_msa *X, struct hash *acond)
{
  intersect_condition_1 *ic = (intersect_condition_1 *)
    sataf_mao_create (sizeof (intersect_condition_1), 
		      &NEW_COND_METHODS);

  ic->msa = sataf_msa_add_reference (X);
  ic->acond = acond;
  acond->refcount++;

  return (sataf_mao *) ic;
}

			/* --------------- */

static sataf_mao *
s_crt_intersect_1 (sataf_msa *X, struct hash *acond)
{
  intersect_condition_1 *ic = (intersect_condition_1 *)
    sataf_mao_create (sizeof (intersect_condition_1), 
		      &INTERSECT_CONDITION_1_METHODS);

  ic->msa = sataf_msa_add_reference (X);
  ic->acond = acond;
  acond->refcount++;

  return (sataf_mao *) ic;
}

			/* --------------- */

static sataf_mao *
s_crt_intersect_2 (sataf_msa *X, sataf_msa *Y, struct hash *acond)
{
  intersect_condition_2 *ic = (intersect_condition_2 *)
    sataf_mao_create (sizeof (intersect_condition_2), 
		      &INTERSECT_CONDITION_2_METHODS);

  ic->X = sataf_msa_add_reference (X);
  ic->Y = sataf_msa_add_reference (Y);
  ic->acond = acond;
  acond->refcount++;

  return (sataf_mao *) ic;
}

			/* --------------- */

static sataf_mao * 
s_new_cond_succ (sataf_mao *self, uint32_t letter)
{
  intersect_condition_1 *ic = (intersect_condition_1 *) self;
  sataf_msa *succ = sataf_msa_succ (ic->msa, letter);
  sataf_mao *result = s_crt_new_cond (succ, ic->acond);
  sataf_msa_del_reference (succ);

  return result;
}

			/* --------------- */

static int
s_is_final_in_table (ccl_hash *table, sataf_msa *msa)
{
  int result = 0;

  if (ccl_hash_find (table, msa->A))
    {
      ccl_bittable *final = ccl_hash_get (table);
      result = ccl_bittable_has (final, msa->initial);
    }

  return result;
}

			/* --------------- */

static int
s_new_cond_is_final (sataf_mao *self)
{
  intersect_condition_1 *ic = (intersect_condition_1 *) self;

  return s_is_final_in_table (ic->acond->table, ic->msa);
}

			/* --------------- */

static size_t
s_intersect_condition_1_size (sataf_mao *self)
{
  return sizeof (intersect_condition_1);
}

			/* --------------- */

static void
s_intersect_condition_1_destroy (sataf_mao *self)
{
  intersect_condition_1 *ic = (intersect_condition_1 *) self;

  ccl_assert (ic->acond->refcount > 0);
  ic->acond->refcount--;
  if (ic->acond->refcount == 0)
    {
      ccl_hash_delete (ic->acond->table);
      ccl_delete (ic->acond);
    }
  sataf_msa_del_reference (ic->msa);
}
			/* --------------- */

static uint32_t
s_intersect_condition_1_get_alphabet_size (sataf_mao *self)
{
  return 2;
}

			/* --------------- */

static sataf_mao * 
s_intersect_condition_1_succ (sataf_mao *self, uint32_t letter)
{
  intersect_condition_1 *ic = (intersect_condition_1 *) self;
  sataf_msa *succ = sataf_msa_succ (ic->msa, letter);
  sataf_mao *result = s_crt_intersect_1 (succ, ic->acond);
  sataf_msa_del_reference (succ);

  return result;
}

			/* --------------- */

static int
s_intersect_condition_1_is_final (sataf_mao *self)
{
  intersect_condition_1 *ic = (intersect_condition_1 *) self;

  return (sataf_msa_is_final(ic->msa) && 
	  s_is_final_in_table(ic->acond->table, ic->msa));
}

			/* --------------- */

static int
s_intersect_condition_1_is_equal_to (sataf_mao *self, sataf_mao *other)
{
  intersect_condition_1 *ic1 = (intersect_condition_1 *) self;
  intersect_condition_1 *ic2 = (intersect_condition_1 *) other;

  return (ic1->msa == ic2->msa && ic1->acond == ic2->acond);
}

			/* --------------- */

static unsigned int
s_intersect_condition_1_hashcode (sataf_mao *self)
{
  intersect_condition_1 *ic = (intersect_condition_1 *) self;

  return ((uintptr_t)(ic->msa))+19*ccl_hash_get_size (ic->acond->table);
}


			/* --------------- */

static size_t
s_intersect_condition_2_size (sataf_mao *self)
{
  return sizeof (intersect_condition_1);
}

static void
s_intersect_condition_2_destroy (sataf_mao *self)
{
  intersect_condition_2 *ic = (intersect_condition_2 *) self;

  ccl_assert (ic->acond->refcount > 0);
  ic->acond->refcount--;
  if (ic->acond->refcount == 0)
    {
      ccl_hash_delete (ic->acond->table);
      ccl_delete (ic->acond);
    }
  sataf_msa_del_reference (ic->X);
  sataf_msa_del_reference (ic->Y);
}

			/* --------------- */

static uint32_t
s_intersect_condition_2_get_alphabet_size (sataf_mao *self)
{
  return 2;
}

			/* --------------- */

static sataf_mao * 
s_intersect_condition_2_succ (sataf_mao *self, uint32_t letter)
{
  intersect_condition_2 *ic = (intersect_condition_2 *) self;
  sataf_msa *Xsucc = sataf_msa_succ (ic->X, letter);
  sataf_msa *Ysucc = sataf_msa_succ (ic->Y, letter);
  sataf_mao *result = s_crt_intersect_2 (Xsucc, Ysucc, ic->acond);
  sataf_msa_del_reference (Xsucc);
  sataf_msa_del_reference (Ysucc);

  return result;
}

			/* --------------- */

static int
s_intersect_condition_2_is_final (sataf_mao *self)
{
  intersect_condition_2 *ic = (intersect_condition_2 *) self;

  return (sataf_msa_is_final(ic->X) && 
	  s_is_final_in_table (ic->acond->table, ic->Y));
}

			/* --------------- */

static int
s_intersect_condition_2_is_equal_to (sataf_mao *self, sataf_mao *other)
{
  intersect_condition_2 *ic1 = (intersect_condition_2 *) self;
  intersect_condition_2 *ic2 = (intersect_condition_2 *) other;

  return (ic1->X == ic2->X && ic1->Y == ic2->Y && 
	  ic1->acond == ic2->acond);
}

			/* --------------- */

static unsigned int
s_intersect_condition_2_hashcode (sataf_mao *self)
{
  intersect_condition_2 *ic = (intersect_condition_2 *) self;

  return (13 * ((uintptr_t) (ic->X)) + 
	  19 * ccl_hash_get_size (ic->acond->table) +
	  17 * ((uintptr_t) (ic->Y)));
}

