/*
 * prestaf-batof.h -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __PRESTAF_BATOF_H__
# define __PRESTAF_BATOF_H__

# include <gmp.h>
# include <ccl/ccl-common.h>
# include <ccl/ccl-log.h>
# include <ccl/ccl-string.h>
# include <sataf/sataf.h>
# include <armoise/armoise-tree.h>
# include <talalib/talalib.h>

# define BTF_MAX_NUMBER_OF_VARIABLES 256

BEGIN_C_DECLS

typedef struct btf_bound_st {
  tla_vsp_t V;
  tla_saff_t B;
} btf_bound;

/*
 * EXIT AUTOMATON TO ASP
 * @pre ea->nb_local_states > 1 || exit_automaton_is_zero_or_one(ea) 
 @ @pre ea->alphabet_size == 2
 */
extern int
btf_ea_to_vsp (tla_vsp_t res, sataf_ea *ea, int s);

extern void
btf_msa_to_saff (tla_saff_t saff, sataf_msa *msa);

extern armoise_tree *
btf_msa_to_formula (int m, sataf_msa *msa, const char * const *varnames,
		    int with_prefix);

extern armoise_tree *
btf_msa_to_convex_hull (int m, sataf_msa *msa, const char * const *varnames);

END_C_DECLS

#endif /* ! __PRESTAF_BATOF_H__ */
