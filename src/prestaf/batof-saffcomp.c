/*
 * batof-saffcomp.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <time.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-list.h>
#include <sataf/sataf.h>
#include "prestaf-batof-p.h"

			/* --------------- */

static void
s_compute_sa_tables (int m, sataf_msa *msa, ccl_hash *tables, ccl_list *Tcomp);

static void
s_compute_component_on_loop (int m, int q0, ccl_list *result, 
			      btf_sa_tables *sa_tab, int **pbackward, 
			     int *pbacksize);

static void
s_delete_sa_tables (btf_sa_tables *sa_tab);

static ccl_list *
s_get_nodes (sataf_msa *msa, ccl_hash *tables, int m);

static void
s_compute_components (int q0, int bq0, int q1, int *backward, ccl_list *result,
		      int m, btf_sa_tables *sa_tab);

static ccl_bittable *
s_alloc_lambdas (sataf_ea *ea, int m);

static int 
s_are_lambdas_equals (int m, ccl_bittable *lambdas, int q1, int q2);

			/* --------------- */

ccl_list *
btf_compute_components (int m, sataf_msa *msa, ccl_hash **p_sa_tables)
{
  int is_presburger = 1;
  ccl_pair *p;
  ccl_list *result = ccl_list_create ();
  ccl_list *components = ccl_list_create ();
  ccl_hash *sa_tables = 
    ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)s_delete_sa_tables);
  int *backward = ccl_new_array (int, 1);
  int backsize = 1;
  
  CCL_DEBUG_START_TIMED_BLOCK (("compute saff components for MSA %p=<%p,%d>",
				msa, msa->A->automaton, msa->initial));
			       
  if (ccl_debug_is_on)
    {
      sataf_msa_log_info (CCL_LOG_DEBUG, msa);
      ccl_debug("\n");      
    }

  s_compute_sa_tables (m, msa, sa_tables, components);

  CCL_DEBUG_START_TIMED_BLOCK (("compute sub-components for %d "
				"main component(s)",
				ccl_list_get_size (components)));

  for (p = FIRST (components); p; p = CDR (p))
    {
      unsigned int q;
      btf_sa_tables *sa_tab = (btf_sa_tables *) CAR (p);
      sataf_ea *ea = sa_tab->sa->automaton;
      int nb_states = sataf_ea_get_nb_states (ea);

      for (q = 0; q < nb_states; q++)
	{
	  if (sataf_ea_is_final (ea, q))
	    break;
	}

      ccl_assert (q != nb_states);

      if (q == nb_states)
	q = 0;

      s_compute_component_on_loop (m, q, result, sa_tab, &backward, &backsize);
    }

  CCL_DEBUG_END_TIMED_BLOCK ();

  if (! is_presburger)
    {
      ccl_list_clear_and_delete (result,
				 (ccl_delete_proc *)btf_saffcomp_delete);
      result = NULL;
    }
  else
    {
      if (p_sa_tables == NULL)
	ccl_hash_delete (sa_tables);
      else
	*p_sa_tables = sa_tables;
    }

  ccl_list_delete (components);
  ccl_delete (backward);

  CCL_DEBUG_END_TIMED_BLOCK ();

  return result;
}

			/* --------------- */

static int
s_is_zero (sataf_sa *sa)
{
  int s, maxs = sataf_ea_get_nb_states (sa->automaton);

  if (sataf_ea_get_nb_exits (sa->automaton))
    return 0;

  for (s = 0; s < maxs; s++)
    if (sataf_ea_is_final (sa->automaton, s))
      return 0;
  return 1;
}

			/* --------------- */

int
btf_is_untransient (sataf_sa *sa)
{
  if (s_is_zero (sa))
    return 0;
  
  if (sa->automaton->nb_exit_states == 0)
    return 1;

  if (sa->automaton->nb_exit_states > 1)
    return 0;

  return s_is_zero (sa->bind[0]->A);
}

			/* --------------- */

static void
s_compute_sa_tables (int m, sataf_msa *msa, ccl_hash *tables, ccl_list *Tcomp)
{
  btf_sa_tables *DC;
  ccl_pair *p;
  ccl_list *Q;

  CCL_DEBUG_START_TIMED_BLOCK (("compute lambdas for MSA %p=<%p,%d>", msa,
				msa->A->automaton, msa->initial));
  if (ccl_debug_is_on)
    {
      sataf_msa_log_info (CCL_LOG_DEBUG, msa);
      ccl_debug("\n");
    }

  Q = s_get_nodes (msa, tables, m);

  p = FIRST (Q);
  DC = CAR (p);
  ccl_assert (DC->sa == msa->A);

  ccl_bittable_set (DC->tables[T_LAMBDAS], m * msa->initial);

  if (DC->tables[T_INIT] != NULL)
    ccl_bittable_set (DC->tables[T_INIT], msa->initial);
  for (; p; p = CDR (p))
    {
      int i, eindex, nb_exits, nb_states;

      DC = (btf_sa_tables *) CAR (p);
      nb_exits = DC->sa->automaton->nb_exit_states;
      nb_states = DC->sa->automaton->nb_local_states;

      if (Tcomp != NULL && btf_is_untransient (DC->sa))
	ccl_list_add (Tcomp, DC);

      btf_compute_lambdas (m, DC->sa->automaton, DC->tables[T_INIT],
			   DC->tables[T_LAMBDAS]);
      ccl_zdelete (ccl_bittable_delete, DC->tables[T_INIT]);
      DC->tables[T_INIT] = NULL;

      /* propagate lambdas on child SCC */
      for (i = 0, eindex = m * nb_states; i < nb_exits; i++, eindex += m)
	{
	  sataf_msa *succ = DC->sa->bind[i];
	  int succ_init_index = succ->initial * m;

	  ccl_bittable_window_union (DC->childs[i]->tables[T_LAMBDAS],
				     succ_init_index,
				     DC->tables[T_LAMBDAS], eindex, m);
	}
    }  
  ccl_list_delete (Q);

  CCL_DEBUG_END_TIMED_BLOCK ();
}

			/* --------------- */

static void
s_compute_component_on_loop (int m, int q0, ccl_list *result, 
			     btf_sa_tables *sa_tab, int **pbackward, 
			     int *pbacksize)
{
  int q1, bq0, loop_found = 0;
  ccl_list *todo = ccl_list_create ();
  sataf_ea *ea = sa_tab->sa->automaton;
  int nb_states = sataf_ea_get_nb_states (ea);
  int *backward;

  if (*pbacksize < (int) ea->nb_local_states)
    {
      *pbackward = (int *) 
	ccl_realloc (*pbackward, sizeof (int) * ea->nb_local_states);
      *pbacksize = nb_states;
    }
  backward = *pbackward;

  for (q1 = 0; q1 < nb_states; q1++)
    backward[q1] = -1;

  q1 = sataf_ea_get_successor (ea, q0, 0);
  if (sataf_ea_is_exit_state (q1))
    {
      bq0 = 1;
      q1 = sataf_ea_get_successor (ea, q0, 1);
    }
  else
    {
      bq0 = 0;
    }
  q1 = sataf_ea_decode_succ_state (q1);

  ccl_list_add (todo, (void *) (intptr_t) q1);

  while (!ccl_list_is_empty (todo) && !loop_found)
    {
      int q = (intptr_t) ccl_list_take_first (todo);

      if (q == q0)
	loop_found = 1;
      else
	{
	  int b;

	  for (b = 0; b < 2; b++)
	    {
	      int qp = sataf_ea_get_successor (ea, q, b);

	      if (sataf_ea_is_exit_state (qp))
		continue;
	      qp = sataf_ea_decode_succ_state (qp);

	      if (backward[qp] >= 0)
		continue;
	      backward[qp] = (q << 1) | b;
	      ccl_list_add (todo, (void *) (intptr_t) qp);
	    }
	}
    }
  ccl_list_delete (todo);

  ccl_assert (loop_found);

  s_compute_components (q0, bq0, q1, backward, result, m, sa_tab);
}

			/* --------------- */

static void
s_delete_sa_tables (btf_sa_tables *sa_tab)
{
  ccl_bittable_delete (sa_tab->tables[T_LAMBDAS]);
  ccl_zdelete (ccl_bittable_delete, sa_tab->tables[T_INIT]);
  /*
  ccl_zdelete (ccl_bittable_delete, sa_tab->tables[T_BAD]);
  ccl_zdelete (ccl_bittable_delete, sa_tab->tables[T_GOOD]);
  */
  ccl_delete (sa_tab);
}

			/* --------------- */

static btf_sa_tables *
s_get_nodes_rec (sataf_sa *sa, ccl_list *result, ccl_hash *tables, int m)
{
  btf_sa_tables *comp;

  if (ccl_hash_find (tables, sa))
    comp = (btf_sa_tables *) ccl_hash_get (tables);
  else
    {
      int i, nb_exits = sa->automaton->nb_exit_states;

      comp = ccl_calloc (sizeof (btf_sa_tables) +
			 sizeof (btf_sa_tables *) * (nb_exits - 1), 1);
      comp->sa = sa;
      comp->tables[T_LAMBDAS] = s_alloc_lambdas (sa->automaton, m);
      if (sa->automaton->nb_local_states > 1)
	comp->tables[T_INIT] =
	  ccl_bittable_create (sa->automaton->nb_local_states);

      ccl_hash_insert (tables, comp);

      for (i = 0; i < nb_exits; i++)
	{
	  comp->childs[i] =
	    s_get_nodes_rec (sa->bind[i]->A, result, tables, m);
	  if (comp->childs[i]->tables[T_INIT])
	    {
	      ccl_bittable_set (comp->childs[i]->tables[T_INIT],
				sa->bind[i]->initial);
	    }
	}

      ccl_list_put_first (result, comp);
    }

  return comp;
}

			/* --------------- */

static ccl_list *
s_get_nodes (sataf_msa *msa, ccl_hash *lambdas, int m)
{
  ccl_list *result = ccl_list_create ();
  s_get_nodes_rec (msa->A, result, lambdas, m);

  return result;
}

			/* --------------- */

static int
s_has_sa (ccl_list *l, sataf_sa *sa)
{
  ccl_pair *p;

  for (p = FIRST (l); p; p = CDR (p))
    {
      btf_saffcomp *C = (btf_saffcomp *) CAR (p);
      if (C->C == sa)
	return 1;
    }
  return 0;
}

			/* --------------- */

static int
s_is_on_0_loop (sataf_ea *ea, int q0)
{
  int s = q0;
  int result = 1;
  int nb_states = sataf_ea_get_nb_states (ea);
  ccl_bittable *path = ccl_bittable_create (nb_states);

  ccl_bittable_set (path, q0);
  while (1)
    {
      s = sataf_ea_get_successor (ea, s, 0);
      if (sataf_ea_is_exit_state (s))
	{
	  result = 0;
	  break;
	}
      else
	{
	  s = sataf_ea_decode_succ_state (s);
	  if (ccl_bittable_has (path, s))
	    {
	      result = (s == q0);
	      break;
	    }
	  else
	    {
	      ccl_bittable_set (path, s);
	    }
	}
    }

  ccl_bittable_delete (path);

  return result;
}

			/* --------------- */

static void
s_compute_components (int q0, int bq0, int q1, int *backward, ccl_list *result,
		      int m, btf_sa_tables *sa_tab)
{
  int s, b;
  ccl_bittable *lambdas = sa_tab->tables[T_LAMBDAS];
  sataf_sa *C = sa_tab->sa;

  CCL_DEBUG_START_TIMED_BLOCK (("compute components on the loop"));

  s = q0;
  b = backward[q0] & 0x1;
  do
    {
      if (ccl_bittable_has (lambdas, s * m) && ! s_has_sa (result, C))
	{
	  btf_saffcomp *comp = ccl_new (btf_saffcomp);

	  comp->C = sataf_sa_add_reference (C);
	  tla_vsp_init (comp->V, m);
	  comp->q = btf_ea_to_vsp (comp->V, C->automaton, s);
	  ccl_assert (s_is_on_0_loop (C->automaton, comp->q));
	  ccl_list_add (result, comp);
	  ccl_assert (ccl_bittable_has (sa_tab->tables[T_LAMBDAS], m*comp->q));
	  comp->m = m;
	  comp->mi = NULL;
	  tla_matrix_init (comp->X, m, m);
	}

      if (s == q1)
	{
	  s = q0;
	  b = bq0;
	}
      else
	{
	  s = backward[s] >> 1;
	  b = backward[s] & 0x1;
	}
    }
  while (!s_are_lambdas_equals (m, lambdas, q0, s));

  if (ccl_debug_is_on)
    ccl_debug ("%d components computed", ccl_list_get_size (result));
  CCL_DEBUG_END_TIMED_BLOCK ();
}

			/* --------------- */

static ccl_bittable *
s_alloc_lambdas (sataf_ea *ea, int m)
{
  return ccl_bittable_create (m * (ea->nb_local_states + ea->nb_exit_states));
}

			/* --------------- */

static int
s_are_lambdas_equals (int m, ccl_bittable *lambdas, int q1, int q2)
{
  int i, hq1, hq2;

  if (q1 == q2)
    return 1;

  for (i = 0, q1 *= m, q2 *= m; i < m; i++, q1++, q2++)
    {
      hq1 = ccl_bittable_has (lambdas, q1) ? 1 : 0;
      hq2 = ccl_bittable_has (lambdas, q2) ? 1 : 0;
      if (hq1 != hq2)
	return 0;
    }
  return 1;
}

			/* --------------- */

