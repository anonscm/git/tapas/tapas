/*
 * batof-bcomb.c -- Computation of Boolean combinations of patterns
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include "prestaf-batof-p.h"

typedef struct class_element_st class_element;

struct class_element_st 
{
  int index;
  int class_index;
  class_element *next;
};

typedef struct partition_class_st
{
  int inX;
  boolean_formula *F;
  class_element *elements;
} partition_class;

			/* --------------- */

static boolean_formula *
s_create_top (ccl_bittable **C, int nb_C);

static boolean_formula *
s_create_binary (boolean_formula_type type, boolean_formula *op1, 
		 boolean_formula *op2);

static boolean_formula *
s_create_cst (int i);

static class_element *
s_compute_partition (ccl_bittable **C, int nb_C, ccl_bittable *E, 
		     ccl_bittable *X, partition_class **p_classes,  
		     int *p_nb_classes);

/*!
 * We assume that :
 * $\forall i = 0\dots nb\_C-1, X \subseteq \cap_{i=1}^{nb\_C-1} C[i]$
 * The function computes a Boolean combination of C[i]s such that it equals X.
 * Allowed operations are the union, the intersection and the difference.
 *
 */
boolean_formula *
btf_boolean_combination (ccl_bittable **C, int nb_C, ccl_bittable *X)
{
  int i;
  int ok = 1;
  ccl_bittable *Xaux;
  boolean_formula *result = NULL;
  partition_class *classes = NULL;
  int nb_classes = 0;
  ccl_bittable *E;
  class_element *elements;
  ccl_list *subformulas;

  /* check the trivial case where a C[i] is exactly X. */
  for (i = 0; i < nb_C; i++)
    {
      if (ccl_bittable_equals (C[i], X))
	return s_create_cst (i);
    }

  /* if the hypothesis does not hold then no combination is possible. */
  E = ccl_bittable_nary_union (C, nb_C);
  if (! ccl_bittable_is_included_in (X, E))
    {
      ccl_bittable_delete (E);
      return NULL;      
    }

  elements = s_compute_partition (C, nb_C, E, X, &classes, &nb_classes);

  ccl_debug ("compute useful classes\n");
  Xaux = ccl_bittable_dup (X);
  ccl_assert (ccl_bittable_get_first (Xaux) >= 0);
  subformulas = ccl_list_create();

  while (ok && (i = ccl_bittable_get_first (Xaux)) >= 0)
    {
      class_element *el = &elements[i];
      partition_class *C = &classes[el->class_index];

      ccl_assert (el->class_index >= 0);
      ok = C->inX;

      if (ok)
	{
	  for (el = C->elements; el && ok; el = el->next)
	    ccl_bittable_unset (Xaux, el->index);
	  ccl_list_add (subformulas, C->F);
	}
    }
  ccl_bittable_delete (Xaux);
  ccl_debug ("compute useful classes done\n");

  if (! ccl_list_is_empty (subformulas))
    {
      boolean_formula **pf;
      int nb_operands = ccl_list_get_size (subformulas);

      ccl_assert (nb_operands > 0);

      result = ccl_malloc (sizeof (boolean_formula)+
			   sizeof (boolean_formula *) * (nb_operands - 1));
      result->refcount = 1;
      result->type = BF_UNION;
      result->arity_or_cst = nb_operands;
      for (pf = result->operands; ! ccl_list_is_empty (subformulas); pf++)
	{
	  *pf = (boolean_formula *) ccl_list_take_first (subformulas);
	  (*pf)->refcount++;
	}
    }

  ccl_list_delete (subformulas);
  ccl_delete (elements);
  for (i = 0; i < nb_classes; i++)
    {
      ccl_assert (classes[i].F->refcount == 1 || classes[i].F->refcount == 2);
      btf_boolean_formula_delete (classes[i].F);
    }
  ccl_delete (classes);
  ccl_bittable_delete (E);

  if (result)
    {
      ccl_bittable *aux = btf_boolean_formula_eval (result, C);
      ccl_assert (ccl_bittable_equals (aux, X));
      ccl_bittable_delete (aux);
    }
  return result;
}

			/* --------------- */

void
btf_boolean_formula_delete (boolean_formula *F)
{
  ccl_assert (F->refcount > 0);
  
  F->refcount--;
  if (F->refcount > 0)
    return;

  if (F->type != BF_CST)
    {
      int i;

      for (i = 0; i < F->arity_or_cst; i++)
	btf_boolean_formula_delete (F->operands[i]);
    }
  ccl_delete (F);
}

			/* --------------- */

void
btf_boolean_formula_display (ccl_log_type log, boolean_formula *f)
{
  if (f->type == BF_CST)
    ccl_log(log,"C%d", f->arity_or_cst);
  else
    {
      int i;
      char op;

      ccl_log (log, "(");
      if (f->type == BF_INTER) 
	op = '&';
      else if (f->type == BF_UNION) 
	op = '|';
      else 
	op = '-';
      btf_boolean_formula_display (log, f->operands[0]);
      for (i = 1; i < f->arity_or_cst; i++)
	{
	  ccl_log(log," %c ", op);
	  btf_boolean_formula_display (log, f->operands[i]);
	}
      ccl_log (log, ")");
    }
}


			/* --------------- */

static ccl_bittable *
s_eval_rec(boolean_formula *f, ccl_bittable **C, ccl_hash **cache)
{
  ccl_bittable *result;

  if (ccl_hash_find (*cache, f))
    result = ccl_hash_get (*cache);
  else
    {
      if (f->type == BF_CST)
	{
	  result = ccl_bittable_dup (C[f->arity_or_cst]);
	}
      else
	{
	  int i;
	  ccl_bittable *op[2];
	  ccl_bittable *(*opfunc)(const ccl_bittable *, const ccl_bittable *);
	  
	  if (f->type == BF_UNION)
	    opfunc = ccl_bittable_union;
	  else if (f->type == BF_INTER)
	    opfunc = ccl_bittable_intersection;
	  else
	    opfunc = ccl_bittable_sub;
	  
	  result = s_eval_rec (f->operands[0], C, cache);
	  for (i = 1; i < f->arity_or_cst; i++)
	    {
	      op[0] = s_eval_rec (f->operands[i], C, cache);
	      op[1] = opfunc (result, op[0]);
	      ccl_bittable_delete (op[0]);
	      ccl_bittable_delete (result);
	      result = op[1];
	    }
	}

      ccl_hash_find (*cache, f);
      ccl_hash_insert (*cache, result);
    }
  result = ccl_bittable_dup (result);

  if (ccl_hash_get_size (*cache) > 100)
    {
      ccl_hash_delete (*cache);
      *cache = ccl_hash_create (NULL, NULL, NULL, 
			       (ccl_delete_proc *) ccl_bittable_delete);
    }

  return result;
}

			/* --------------- */

ccl_bittable *
btf_boolean_formula_eval (boolean_formula *f, ccl_bittable **C)
{
  ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, 
				     (ccl_delete_proc *) ccl_bittable_delete);
  ccl_bittable *result = s_eval_rec(f, C, &cache);
  ccl_hash_delete (cache);

  return result;
}

			/* --------------- */

static boolean_formula *
s_create_top (ccl_bittable **C, int nb_C)
{
  int i;
  boolean_formula *F = (boolean_formula *)
    ccl_malloc (sizeof (boolean_formula) +
		sizeof (boolean_formula *) * (nb_C - 1));

  F->refcount = 1;
  F->type = BF_UNION;

  F->arity_or_cst = nb_C;
  for (i = 0; i < nb_C; i++)
    F->operands[i] = s_create_cst (i);

  return F;
}

			/* --------------- */

static boolean_formula *
s_create_just_a_binary_op (boolean_formula_type type, 
			   boolean_formula *op1, boolean_formula *op2)
{
  boolean_formula *F = (boolean_formula *)
    ccl_malloc (sizeof (boolean_formula) + sizeof (boolean_formula *));
  
  F->refcount = 1;
  F->type = type;
  F->arity_or_cst = 2;
  F->operands[0] = op1;
  F->operands[1] = op2;
  op1->refcount++;
  op2->refcount++;

  return F;
}

			/* --------------- */

static boolean_formula *
s_create_inter (boolean_formula *op1, boolean_formula *op2)
{
  boolean_formula *result;

  result = s_create_just_a_binary_op (BF_INTER, op1, op2);
  
  return result;
}

			/* --------------- */

static boolean_formula *
s_create_diff (boolean_formula *op1, boolean_formula *op2)
{
  boolean_formula *result;

  if (op1->type == BF_INTER)
    {
      boolean_formula *aux = s_create_diff (op1->operands[0], op2);
      result = s_create_inter (aux, op1->operands[1]);
      btf_boolean_formula_delete (aux);
    }
  else if (op1->type == BF_DIFF)
    {
      boolean_formula *aux = s_create_diff (op1->operands[0], op2);
      result = s_create_just_a_binary_op (BF_DIFF, aux, op1->operands[1]);
      btf_boolean_formula_delete (aux);
    }
  else if (op1->type == BF_UNION && op2->type == BF_CST)
    {
      int i;

      for (i = 0; i < op1->arity_or_cst; i++)
	{
	  if (op1->operands[i]->type == BF_CST && 
	      op1->operands[i]->arity_or_cst == op2->arity_or_cst)
	    break;
	}

      if (i < op1->arity_or_cst)
	{
	  boolean_formula *d;

	  ccl_assert (op1->arity_or_cst > 1);

	  if (op1->arity_or_cst == 2)
	    {
	      d = op1->operands[(i + 1) % 2];
	    }
	  else
	    {
	      int k;
	      int new_arity = op1->arity_or_cst - 1;
	      int to_remove = i;
	      
	      d = (boolean_formula *)
		ccl_malloc (sizeof (boolean_formula) + 
			    sizeof (boolean_formula *) * new_arity);

	      d->type = BF_UNION;
	      d->refcount = 0;
	      d->arity_or_cst = new_arity;
	      for (k = 0, i = 0; i < op1->arity_or_cst; i++)
		{
		  if (i == to_remove)
		    continue;

		  d->operands[k] = op1->operands[i];
		  d->operands[k]->refcount++;
		  k++;
		}
	    }
	  result = s_create_just_a_binary_op (BF_DIFF, d, op2);	  
	}
      else
	{
	  result = s_create_just_a_binary_op (BF_DIFF, op1, op2);
	}
    }
  else
    {
      result = s_create_just_a_binary_op (BF_DIFF, op1, op2);
    }

  return result;
}

			/* --------------- */

static boolean_formula *
s_create_binary (boolean_formula_type type, boolean_formula *op1, 
		 boolean_formula *op2)
{
  boolean_formula *result;

  switch (type)
    {
    case BF_INTER: result = s_create_inter (op1, op2); break;
    case BF_DIFF: result = s_create_diff (op1, op2); break;
    default: abort();
    }
  return result;
}

			/* --------------- */

static boolean_formula *
s_create_cst (int i)
{
  boolean_formula *F = ccl_new (boolean_formula);
  F->type = BF_CST;
  F->refcount = 1;
  F->arity_or_cst = i;

  return F;
}

			/* --------------- */

static int
s_cmp_bittables (const void *pbt1, const void *pbt2, const void *data)
{
  int i1 = (intptr_t) pbt1;
  int i2 = (intptr_t) pbt2;
  const ccl_bittable * const *C = data;
  return (ccl_bittable_get_nb_one (C[i2]) - ccl_bittable_get_nb_one (C[i1]));
}

			/* --------------- */

/* E is the union of C[i]s for i = 0 \dots nb\_C-1 */
static class_element *
s_compute_partition (ccl_bittable **C, int nb_C, 
		     ccl_bittable *E, 
		     ccl_bittable *X,
		     partition_class **p_classes,  int *p_nb_classes)
{
  int i;
  ccl_list *indexes;
  int card = ccl_bittable_get_size (E);
  ccl_bittable *inC = ccl_bittable_create (card);
  ccl_bittable *notinC = ccl_bittable_create (card);
  int nb_classes = 1;
  partition_class *classes = ccl_new_array (partition_class, 1);
  class_element *elements = ccl_new_array (class_element, card);
  boolean_formula **fC = ccl_new_array (boolean_formula *, nb_C);

  ccl_debug ("compute partition\n");
  for (i = 0; i < nb_C; i++)
    fC[i] = s_create_cst (i);

  classes[0].F = s_create_top (C, nb_C);
  classes[0].inX = 1;
  for (i = 0; i < card; i++)
    {
      elements[i].index = i;
      if (ccl_bittable_has (E, i))
	{
	  elements[i].class_index = 0;
	  elements[i].next = classes[0].elements;
	  classes[0].elements = &(elements[i]);
	  classes[0].inX = (classes[0].inX && ccl_bittable_has (X, i));
	}
      else
	{
	  elements[i].class_index = -1;
	  elements[i].next = NULL;
	}
    }

  if (classes[0].inX)
    goto end;

  indexes = ccl_list_create ();
  for (i = 0; i < nb_C; i++)
    ccl_list_add (indexes, (void *) (intptr_t) i);
  ccl_list_sort_with_data (indexes,
			   (ccl_compare_with_data_func *) s_cmp_bittables, C);

  while (! ccl_list_is_empty (indexes))
    {
      int s;
      int smax;
      int nb_inX = 0;

      i = (intptr_t) ccl_list_take_first (indexes);
      for (s = 0; s < card; s++)
	{
	  int index = elements[s].class_index;

	  if (index < 0 || classes[index].inX)
	    continue;

	  if (ccl_bittable_has (C[i], s))
	    ccl_bittable_set (inC, index);
	  else
	    ccl_bittable_set (notinC, index);
	}

      smax = nb_classes;
      for (s = 0; s < smax; s++)
	{
	  class_element **pe;
	  boolean_formula *f0;
	  boolean_formula *f1;

	  if (classes[s].inX)
	    {
	      nb_inX++;
	      continue;
	    }

	  if (! (ccl_bittable_has (inC, s) && ccl_bittable_has (notinC, s)))
	    continue;

	  classes = (partition_class *)
	    ccl_realloc (classes, 
			 (nb_classes + 1)* sizeof (partition_class));

	  f0 = s_create_binary (BF_INTER, classes[s].F, fC[i]);
	  f1 = s_create_binary (BF_DIFF, classes[s].F, fC[i]);
	  btf_boolean_formula_delete (classes[s].F);
	      
	  classes[s].F = f0;
	  classes[nb_classes].F = f1;
	  classes[s].inX = 1;
	  classes[nb_classes].inX = 1;
	  classes[nb_classes].elements = NULL;
	  nb_classes++;

	  pe = &classes[s].elements;
	  while (*pe)
	    {
	      class_element *e = *pe;

	      if (! ccl_bittable_has (C[i], (*pe)->index))
		{
		  *pe = e->next;
		  e->class_index = nb_classes-1;
		  e->next = classes[nb_classes-1].elements;
		  classes[nb_classes-1].elements = e;
		  classes[nb_classes-1].elements = e;
		  classes[nb_classes-1].inX = (classes[nb_classes-1].inX &&
					       ccl_bittable_has (X, e->index));
		}
	      else
		{
		  classes[s].inX = (classes[s].inX && 
				    ccl_bittable_has (X, e->index));
		  pe = &(e->next);
		}
	    }
	}
      ccl_bittable_clear (inC);
      ccl_bittable_clear (notinC);
    }

  ccl_list_delete (indexes);

 end:
  for (i = 0; i < nb_C; i++)
    btf_boolean_formula_delete (fC[i]);
  ccl_delete (fC);

  ccl_bittable_delete (inC);
  ccl_bittable_delete (notinC);

  *p_classes = classes;
  *p_nb_classes = nb_classes;

  ccl_debug ("compute partition done\n");

  return elements;
}

			/* --------------- */

static float 
s_proba(void) 
{
  return (float) rand () / (float) RAND_MAX;
}

			/* --------------- */

#include <time.h>

void
btf_boolean_combination_test (void)
{
  int N = 50;
  int W = 200;
  ccl_bittable **C = ccl_new_array (ccl_bittable *, N);
  boolean_formula *F = NULL;
  ccl_bittable *Fvalue;

  if(0)
    {
      int s = time (NULL);
      ccl_debug ("seed = %d\n", s);
      srand (s);
    }
  else
    srand (1179323908);
  { /* random generation of Ci */
    int i; 
    ccl_debug ("generating Ci\n");
    for (i = 0; i < N; i++)
      {
	int j; 

	C[i] = ccl_bittable_create (W);
	while (ccl_bittable_get_nb_one (C[i]) == 0)
	  {
	    for (j = 0; j < W; j++)
	      {
		if (s_proba() > 0.5)
		  ccl_bittable_set (C[i], j);
	      }
	  }
      }
    ccl_debug ("generating Ci done\n");
  }

  ccl_debug ("generating random formulas\n");
  {
    int i;
    boolean_formula **fC = ccl_new_array (boolean_formula *, N);
    
    for (i = 0; i < N; i++)
      {
	fC[i] = ccl_new (boolean_formula);
	fC[i]->refcount = 1;
	fC[i]->type = BF_CST;
	fC[i]->arity_or_cst = i;
      }
    
    while(1)
      {    
	/* random formula generation */
	
	for (i = 0; i < N; i++)
	  {
	    boolean_formula *f;
	  
	    if (F == NULL)
	      {
		f = fC[i];
		f->refcount++;
	      }
	    else
	      {
		f = (boolean_formula *)ccl_malloc (sizeof (boolean_formula) +
						   sizeof (boolean_formula *));
	      
		f->refcount = 1;
		f->arity_or_cst = 2;
		f->operands[0] = F;
		f->operands[1] = fC[i];
		fC[i]->refcount++;
		F->refcount++;
	      
		switch (rand()%3) 
		  {
		  case 0 :
		    f->type = BF_UNION;
		    break;
		  case 1 :
		    f->type = BF_INTER;
		    break;
		  case 2 :
		    f->type = BF_DIFF;
		    break;
		  }
	      }
	  
	    if (F != NULL)
	      btf_boolean_formula_delete (F);
	    F = f;
	  }

	Fvalue = btf_boolean_formula_eval (F, C);
	if (ccl_bittable_get_nb_one(Fvalue) == 0)
	  {
	    ccl_bittable_delete (Fvalue);
	    btf_boolean_formula_delete (F);
	    F = NULL;
	  }
	else
	  break;
      }
  
    for (i = 0; i < N; i++)
      btf_boolean_formula_delete (fC[i]);
    ccl_delete (fC);
  }
  ccl_debug ("generating random formulas done\n");
    

  ccl_display ("F = ");
  btf_boolean_formula_display (CCL_LOG_DISPLAY, F);
  ccl_display ("\n");
  if(0){
    int j;
    
    ccl_display ("Fvalue = { ");
    for (j = 0; j < W; j++)
      {
	if (ccl_bittable_has (Fvalue, j))
	  ccl_display ("%d ", j);
      }
    ccl_display ("}\n");
  }

  if(0) {
    int i;
    for (i = 0; i < N; i++)
      {
	int j;
	
	ccl_display ("C%d = { ", i);
	for (j = 0; j < W; j++)
	  {
	    if (ccl_bittable_has (C[i], j))
	      ccl_display ("%d ", j);
	  }
	ccl_display ("}\n");
      }
  }
  {
    boolean_formula *Faux = btf_boolean_combination (C, N, Fvalue);

    ccl_display ("Faux = ");
    btf_boolean_formula_display (CCL_LOG_DISPLAY, Faux);
    ccl_display ("\n");

    if(0)
      {
	ccl_bittable *Fauxvalue = btf_boolean_formula_eval (Faux, C);
	ccl_assert (ccl_bittable_equals (Fauxvalue, Fvalue));
	ccl_bittable_delete (Fauxvalue);
      }
    btf_boolean_formula_delete (Faux);
  }

  ccl_bittable_delete (Fvalue);
  btf_boolean_formula_delete (F);
  { /* clean up */
    int i;

    for (i = 0; i < N; i++)
      ccl_bittable_delete (C[i]);
  }
  ccl_delete (C);
}

			/* --------------- */

