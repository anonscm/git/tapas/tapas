/*
 * prestaf-formula.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include "prestaf-automata.h"
#include "prestaf-formula-p.h"
#include "prestaf-formula.h"

static prestaf_formula *
s_create_formula (size_t size, prestaf_formula_type type);

static void
s_delete_formula (prestaf_formula *F);

static void
s_get_variables (prestaf_formula *f, ccl_list * R);

static void
s_display_factor (ccl_log_type log, prestaf_factor *ft);

static void
s_compute_quantifier_solutions (prestaf_quantifier *qf, int depth);

static void
s_compute_linear_constraint_solutions (prestaf_linear_constraint *f);

static void
s_compute_solutions (prestaf_formula *f, int depth);

static prestaf_formula *
s_boolean_formula (prestaf_formula_type type, ccl_list *operands);

static prestaf_formula *
s_create_boolean_formula (prestaf_formula_type type, prestaf_formula *f1,
			  prestaf_formula *f2);

static prestaf_formula *
s_create_quantifier (prestaf_formula_type type, ccl_ustring qV, 
		     prestaf_formula *F);

static prestaf_formula *
s_create_quantifier_formula_rec (prestaf_formula_type type, ccl_pair *p,
				 prestaf_formula *F);

static prestaf_formula *
s_create_linear_constraint (prestaf_formula_type type, prestaf_linear_term *t1,
			    prestaf_linear_term *t2);

static prestaf_factor *
s_new_factor (int a, ccl_ustring x);

static prestaf_factor *
s_merge_factors (prestaf_factor *f1, prestaf_factor *f2,
		 int (*operator) (int, int));

static int 
s_plus (int a1, int a2);

static uint32_t
s_count_factors (prestaf_linear_term *t);

static int
s_minus (int a1, int a2);

static void
s_reduce_linear_constraint (prestaf_linear_term *t);

			/* --------------- */

ccl_list *
prestaf_formula_get_variables (prestaf_formula *f)
{
  ccl_list *R;

  ccl_pre (f != NULL);

  if (f->P != NULL)
    R = ccl_list_dup (prestaf_predicate_get_variables (f->P));
  else
    {
      R = ccl_list_create ();
      s_get_variables (f, R);
    }

  return R;
}

			/* --------------- */

prestaf_formula *
prestaf_formula_add_reference (prestaf_formula *F)
{
  ccl_pre (F != NULL);

  F->refcount++;

  return F;
}

			/* --------------- */

void
prestaf_formula_del_reference (prestaf_formula *F)
{
  ccl_pre (F != NULL && F->refcount > 0);

  if (--F->refcount == 0)
    s_delete_formula (F);
}

			/* --------------- */

void
prestaf_formula_display (ccl_log_type log, prestaf_formula *f)
{
  ccl_pair *p;
  const char *fmt;
  prestaf_factor *pf;
  prestaf_linear_term *t;

  ccl_pre (f != NULL);

  switch (f->type)
    {
    case PF_FILE:
      ccl_log (log, "load(\"%s\")", ((prestaf_file_formula *) f)->filename);
      break;
      
    case PF_CST:
      ccl_log (log, "%s", 
	       ((prestaf_boolean_constant *) f)->value?"true":"false");
      break;

    case PF_AND:
    case PF_XOR:
    case PF_OR:
    case PF_EQUIV:
    case PF_IMPLY:
      if (f->type == PF_AND)
	fmt = " && ";
      else if (f->type == PF_OR)
	fmt = " || ";
      else if (f->type == PF_IMPLY)
	fmt = " => ";
      else if (f->type == PF_XOR)
	fmt = " ^^ ";
      else
	fmt = " <=> ";

      for (p = FIRST (((prestaf_boolean_formula *) f)->operands); p;
	   p = CDR (p))
	{
	  ccl_log (log, "(");
	  prestaf_formula_display (log, CAR (p));
	  ccl_log (log, ")");
	  if (CDR (p) != NULL)
	    ccl_log (log, fmt);
	}
      break;

    case PF_NOT:
      {
	prestaf_formula *op = 
	  CAR (FIRST (((prestaf_boolean_formula *) f)->operands));
	ccl_log (log, "(! (");
	prestaf_formula_display (log, op);
	ccl_log (log, "))");
      }
      break;

    case PF_FORALL:
    case PF_EXISTS:
      {
	prestaf_quantifier *fp;

	if (f->type == PF_FORALL)
	  ccl_log (log, "(forall ");
	else
	  ccl_log (log, "(exists ");	
	ccl_log (log, "%s", ((prestaf_quantifier *) f)->qV);
		
	fp = (prestaf_quantifier *) ((prestaf_quantifier *) f)->F;
	while (fp->super.type == f->type)
	  {
	    ccl_log (log, ", %s", fp->qV);
	    fp = (prestaf_quantifier *) (fp->F);
	  }
	ccl_log (log, " (");
	prestaf_formula_display (log, (prestaf_formula *) fp);
	ccl_log (log, "))");
	break;
      }

    case PF_EQ:
    case PF_GT:
    case PF_GEQ:
      ccl_log (log, "(");
      t = ((prestaf_linear_constraint *) f)->axb;

      for (pf = t->factors; pf; pf = pf->next)
	{
	  if (pf == t->factors || pf->a < 0)
	    s_display_factor (log, pf);
	  else
	    {
	      ccl_log (log, "+");
	      s_display_factor (log, pf);
	    }
	}

      if (f->type == PF_EQ)
	fmt = " == %d";
      else if (f->type == PF_GEQ)
	fmt = " >= %d";
      else
	fmt = " > %d";
      ccl_log (log, fmt, -t->b);
      ccl_log (log, ")");
      break;
    };
}


			/* --------------- */


prestaf_predicate *
prestaf_formula_solutions (prestaf_formula *f)
{
  ccl_pre (f != NULL);

  s_compute_solutions (f, 0);

  return prestaf_predicate_add_reference (f->P);
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_from_file (const char *filename)
{
  prestaf_file_formula *R = (prestaf_file_formula *)
    s_create_formula (sizeof (prestaf_file_formula), PF_FILE);
  R->filename = ccl_string_dup (filename);
  
  return (prestaf_formula *) R;
}

			/* --------------- */
prestaf_formula *
prestaf_formula_create_boolean_cst (int value)
{
  prestaf_boolean_constant *R = (prestaf_boolean_constant *)
    s_create_formula (sizeof (prestaf_boolean_constant), PF_CST);
  R->value = value;
  
  return (prestaf_formula *) R;
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_or (prestaf_formula *f1, prestaf_formula *f2)
{
  ccl_pair *p;
  prestaf_boolean_formula *R = (prestaf_boolean_formula *)
    s_create_formula (sizeof (prestaf_boolean_formula), PF_OR);

  ccl_pre (f1 != NULL && f2 != NULL);

  R->operands = ccl_list_create ();

  if (f1->type == PF_OR)
    {
      for (p = FIRST (((prestaf_boolean_formula *) f1)->operands); p;
	   p = CDR (p))
	prestaf_formula_add_reference (CAR (p));
      ccl_list_append (R->operands, 
		       ((prestaf_boolean_formula *) f1)->operands);
    }
  else
    {
      ccl_list_add (R->operands, prestaf_formula_add_reference (f1));
    }

  if (f2->type == PF_OR)
    {
      for (p = FIRST (((prestaf_boolean_formula *) f2)->operands); p;
	   p = CDR (p))
	prestaf_formula_add_reference (CAR (p));
      ccl_list_append (R->operands, 
		       ((prestaf_boolean_formula *) f2)->operands);
    }
  else
    {
      ccl_list_add (R->operands, prestaf_formula_add_reference (f2));
    }

  return (prestaf_formula *) R;
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_and (prestaf_formula *f1, prestaf_formula *f2)
{
  ccl_pair *p;
  prestaf_boolean_formula *R = (prestaf_boolean_formula *)
    s_create_formula (sizeof (prestaf_boolean_formula), PF_AND);

  ccl_pre (f1 != NULL && f2 != NULL);

  R->operands = ccl_list_create ();

  if (f1->type == PF_AND)
    {
      for (p = FIRST (((prestaf_boolean_formula *) f1)->operands); p;
	   p = CDR (p))
	prestaf_formula_add_reference (CAR (p));
      ccl_list_append (R->operands, 
		       ((prestaf_boolean_formula *) f1)->operands);
    }
  else
    {
      ccl_list_add (R->operands, prestaf_formula_add_reference (f1));
    }

  if (f2->type == PF_AND)
    {
      for (p = FIRST (((prestaf_boolean_formula *) f2)->operands); p;
	   p = CDR (p))
	prestaf_formula_add_reference (CAR (p));
      ccl_list_append (R->operands, 
		       ((prestaf_boolean_formula *) f2)->operands);
    }
  else
    {
      ccl_list_add (R->operands, prestaf_formula_add_reference (f2));
    }

  return (prestaf_formula *) R;
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_xor (prestaf_formula *f1, prestaf_formula *f2)
{
  return s_create_boolean_formula (PF_XOR, f1, f2);
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_equiv (prestaf_formula *f1, prestaf_formula *f2)
{
  return s_create_boolean_formula (PF_EQUIV, f1, f2);
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_imply (prestaf_formula *f1, prestaf_formula *f2)
{
  return s_create_boolean_formula (PF_IMPLY, f1, f2);
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_not (prestaf_formula *f)
{
  return s_create_boolean_formula (PF_NOT, f, NULL);
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_forall (ccl_list *variables, prestaf_formula *f)
{
  ccl_pre (variables != NULL && f != NULL);
  ccl_pre (ccl_list_get_size (variables) > 0);

  return s_create_quantifier_formula_rec (PF_FORALL, FIRST (variables), f);
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_exists (ccl_list *variables, prestaf_formula *f)
{
  ccl_pre (ccl_list_get_size (variables) > 0);

  return s_create_quantifier_formula_rec (PF_EXISTS, FIRST (variables), f);
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_single_exists (ccl_ustring v, prestaf_formula *f)
{
  return s_create_quantifier (PF_EXISTS, v, f);
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_eq (prestaf_linear_term *t1, prestaf_linear_term *t2)
{
  return s_create_linear_constraint (PF_EQ, t1, t2);
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_neq (prestaf_linear_term *t1, prestaf_linear_term *t2)
{
  prestaf_formula *f = prestaf_formula_create_eq (t1, t2);
  prestaf_formula *nf = prestaf_formula_create_not (f);
  prestaf_formula_del_reference (f);

  return nf;
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_lt (prestaf_linear_term *t1, prestaf_linear_term *t2)
{
  prestaf_formula *f = prestaf_formula_create_geq (t1, t2);
  prestaf_formula *nf = prestaf_formula_create_not (f);
  prestaf_formula_del_reference (f);

  return nf;
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_leq (prestaf_linear_term *t1, prestaf_linear_term *t2)
{
  prestaf_formula *f = prestaf_formula_create_gt (t1, t2);
  prestaf_formula *nf = prestaf_formula_create_not (f);
  prestaf_formula_del_reference (f);

  return nf;
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_gt (prestaf_linear_term *t1, prestaf_linear_term *t2)
{
  return s_create_linear_constraint (PF_GT, t1, t2);
}

			/* --------------- */

prestaf_formula *
prestaf_formula_create_geq (prestaf_linear_term *t1, prestaf_linear_term *t2)
{
  return s_create_linear_constraint (PF_GEQ, t1, t2);
}

			/* --------------- */

prestaf_linear_term *
prestaf_linear_term_create (uint32_t n, int *a, ccl_ustring *x, int b)
{
  uint32_t i;
  prestaf_factor **pf;
  prestaf_linear_term *R = ccl_new (prestaf_linear_term);

  ccl_pre (ccl_imply (n >= 1, a != NULL && x != NULL));

  R->refcount = 1;
  R->factors = NULL;
  R->b = b;
  R->nb_factors = n;

  for (i = 0, pf = &R->factors; i < n; i++, pf = &((*pf)->next))
    *pf = s_new_factor (a[i], x[i]);

  return R;
}

			/* --------------- */


prestaf_linear_term *
prestaf_linear_term_plus (prestaf_linear_term *t1, prestaf_linear_term *t2)
{
  prestaf_linear_term *R = ccl_new (prestaf_linear_term);

  ccl_pre (t1 != NULL && t2 != NULL);

  R->refcount = 1;
  R->factors = s_merge_factors (t1->factors, t2->factors, s_plus);
  R->b = t1->b + t2->b;
  R->nb_factors = s_count_factors (R);

  return R;
}

			/* --------------- */

prestaf_linear_term *
prestaf_linear_term_minus (prestaf_linear_term *t1, prestaf_linear_term *t2)
{
  prestaf_linear_term *R = ccl_new (prestaf_linear_term);

  ccl_pre (t1 != NULL && t2 != NULL);

  R->refcount = 1;
  R->factors = s_merge_factors (t1->factors, t2->factors, s_minus);
  R->b = t1->b - t2->b;
  R->nb_factors = s_count_factors (R);

  return R;
}

			/* --------------- */

prestaf_linear_term *
prestaf_linear_term_neg (prestaf_linear_term *t)
{
  prestaf_factor *f;
  prestaf_factor **pf;
  prestaf_linear_term *result = ccl_new (prestaf_linear_term);

  ccl_pre (t != NULL);

  result->refcount = 1;
  result->b = -t->b;
  result->nb_factors = t->nb_factors;
  pf = &result->factors;
  for (f = t->factors; f; f = f->next, pf = &((*pf)->next))
    {
      prestaf_factor *nf = ccl_new (prestaf_factor);

      nf->a = -f->a;
      nf->x = f->x;
      nf->next = NULL;
      *pf = nf;
    }

  return result;
}

			/* --------------- */

prestaf_linear_term *
prestaf_linear_term_add_reference (prestaf_linear_term *t)
{
  ccl_pre (t != NULL);

  t->refcount++;

  return t;
}

			/* --------------- */

void
prestaf_linear_term_del_reference (prestaf_linear_term *t)
{
  ccl_pre (t != NULL && t->refcount > 0);

  if (--t->refcount == 0)
    {
      prestaf_factor *next;
      prestaf_factor *f;

      for (f = t->factors; f; f = next)
	{
	  next = f->next;
	  ccl_delete (f);
	}

      ccl_delete (t);
    }
}

			/* --------------- */

int
prestaf_linear_term_get_coef (prestaf_linear_term *t, ccl_ustring x)
{
  prestaf_factor *f;

  ccl_pre (t != NULL && x != NULL);

  for (f = t->factors; f; f = f->next)
    {
      if (f->x == x)
	return f->a;
    }

  return 0;
}

			/* --------------- */

int
prestaf_linear_term_get_ith_coef (prestaf_linear_term *t, uint32_t i)
{
  prestaf_factor *f = t->factors;

  ccl_pre (i < t->nb_factors);

  while (i-- && f != NULL)
    f = f->next;

  if (f != NULL)
    return f->a;
  return 0;
}

			/* --------------- */

void
prestaf_formula_statistics (prestaf_formula *F, ccl_log_type log)
{
  sataf_msa *rel;
  sataf_msa_info info;

  ccl_pre (F != NULL);

  rel = prestaf_predicate_get_relation (F->P);
  sataf_msa_get_info (rel, &info);
  sataf_msa_del_reference (rel);

  ccl_log (log,
	   "# states                = %d\n"
	   "# shared automaton (SA) = %d\n"
	   "# ea                    = %d\n"
	   "# depth                 = %d\n"
	   "# refcount              = %d\n",
	   info.nb_states,
	   info.nb_shared_automata,
	   info.nb_exit_automata, info.depth, info.refcount);
}

			/* --------------- */

static prestaf_formula *
s_linear_transform (prestaf_formula *F, ccl_hash *boundvars, ccl_hash *c, 
		    int a)
{
  prestaf_formula *result;

  switch (F->type) 
    {
    case PF_AND:
    case PF_XOR:
    case PF_OR:
    case PF_EQUIV:
    case PF_IMPLY:
    case PF_NOT:
      {
	ccl_pair *p;
	prestaf_boolean_formula *bf = (prestaf_boolean_formula *) F;
	ccl_list *operands = ccl_list_create ();

	for (p = FIRST (bf->operands); p; p = CDR (p))
	  {
	    prestaf_formula *f = (prestaf_formula *) CAR (p);
	    prestaf_formula *ltf = s_linear_transform (f, boundvars, c, a);
	    ccl_list_add (operands, ltf);
	  }
	result = s_boolean_formula (F->type, operands);
      }
      break;

    case PF_FORALL:
    case PF_EXISTS:
      {
	int refcount;
	prestaf_formula *aux;
	prestaf_quantifier *qF = (prestaf_quantifier *) F;

	if (! ccl_hash_find (boundvars, qF->qV))
	  ccl_hash_insert (boundvars, (void *) 1);
	else
	  {
	    refcount = (intptr_t) ccl_hash_get (boundvars);
	    refcount++;
	    ccl_hash_insert (boundvars, (void *) (intptr_t) refcount);
	  }

	aux = s_linear_transform (qF->F, boundvars, c, a);
	result = s_create_quantifier (F->type, qF->qV, aux);
	prestaf_formula_del_reference (aux);

	ccl_assert (ccl_hash_find (boundvars, qF->qV));
	ccl_hash_find (boundvars, qF->qV);
	refcount = (intptr_t) ccl_hash_get (boundvars);
	if (--refcount == 0)
	  ccl_hash_remove (boundvars);
	break;
      }

    case PF_EQ: 
    case PF_GT:
    case PF_GEQ:
      {
	prestaf_factor *pf;
	prestaf_linear_constraint *lc = (prestaf_linear_constraint *) F;

	for (pf = lc->axb->factors; pf; pf = pf->next)
	  {
	    if (! ccl_hash_find (boundvars, pf->x) && ccl_hash_find (c, pf->x))
	      break;
	  }

	if (pf == NULL)
	  result = prestaf_formula_add_reference (F);
	else
	  {
	    int cst;
	    prestaf_factor **ppf;
	    prestaf_linear_constraint *ltlc = (prestaf_linear_constraint *) 
	      s_create_formula (sizeof (prestaf_linear_constraint), F->type);

	    ltlc->axb = ccl_new (prestaf_linear_term);
	    ltlc->axb->refcount = 1;
	    ltlc->axb->factors = NULL;
	    ltlc->axb->b = a * lc->axb->b;
	    ltlc->axb->nb_factors = lc->axb->nb_factors;
	    ppf = &ltlc->axb->factors;

	    for (pf = lc->axb->factors; pf; pf = pf->next)
	      {
		if (ccl_hash_find (boundvars, pf->x) || 
		    ! ccl_hash_find (c, pf->x))
		  *ppf = s_new_factor (pf->a * a, pf->x);
		else
		  {		    
		    *ppf = s_new_factor (pf->a, pf->x);

		    ccl_assert (ccl_hash_find (c, pf->x));
		    ccl_hash_find (c, pf->x);
		    cst = (intptr_t) ccl_hash_get (c);
		    ltlc->axb->b += pf->a * cst;
		  }
		ppf = &((*ppf)->next);
	      }
	    s_reduce_linear_constraint (ltlc->axb);
	    result = (prestaf_formula *) ltlc;
	  }
      }
      break;

    default:
      ccl_assert (F->type == PF_CST);
      result = prestaf_formula_add_reference (F);
      break;
    }

  return result;
}

			/* --------------- */

int
prestaf_formula_get_size (prestaf_formula *F)
{
  int result = 0;

  switch (F->type)
    {
    case PF_AND:
    case PF_XOR:
    case PF_OR:
    case PF_EQUIV:
    case PF_IMPLY:
    case PF_NOT:
      {
	ccl_pair *p;
	prestaf_boolean_formula *bf = (prestaf_boolean_formula *) F;

	for (p = FIRST (bf->operands); p; p = CDR (p))
	  {
	    prestaf_formula *f = (prestaf_formula *) CAR (p);
	    result += prestaf_formula_get_size (f);
	  }
	result++;
      }
      break;

    case PF_FORALL:
    case PF_EXISTS:
      {
	prestaf_quantifier *qF = (prestaf_quantifier *) F;

	result = 1 + prestaf_formula_get_size (qF->F);
      }
      break;

    case PF_EQ: 
    case PF_GT:
    case PF_GEQ:
      {
	prestaf_linear_constraint *lc = (prestaf_linear_constraint *) F;
	result = 1 + lc->axb->nb_factors;
      }
      break;

    default:
      ccl_assert (F->type == PF_CST);
      result = 1;
      break;
    }

  return result;
}

			/* --------------- */

prestaf_formula *
prestaf_formula_linear_transform (prestaf_formula *F, ccl_hash *c, int a)
{
  ccl_hash *boundvars = ccl_hash_create (NULL, NULL, NULL, NULL);
  prestaf_formula *result = s_linear_transform (F, boundvars, c, a);
  ccl_assert (ccl_hash_get_size (boundvars) == 0);
  ccl_hash_delete (boundvars);

  return result;
}

			/* --------------- */

void
prestaf_formula_display_automaton (prestaf_formula *F, ccl_log_type log)
{
  ccl_pre (F != NULL);

  s_compute_solutions (F, 0);
  prestaf_predicate_display_automaton (log, F->P);
}

static prestaf_formula *
s_create_formula (size_t size, prestaf_formula_type type)
{
  prestaf_formula *R = ccl_calloc (size, 1);

  R->refcount = 1;
  R->type = type;
  R->P = NULL;

  return R;
}

			/* --------------- */

static void
s_delete_formula (prestaf_formula *F)
{
  ccl_pair *p;

  switch (F->type)
    {
    case PF_CST:
      break;
    case PF_FILE:
      ccl_delete (((prestaf_file_formula *)F)->filename);
      break;
    case PF_AND:
    case PF_XOR:
    case PF_OR:
    case PF_NOT:
    case PF_EQUIV:
    case PF_IMPLY:
      for (p = FIRST (((prestaf_boolean_formula *) F)->operands); p;
	   p = CDR (p))
	prestaf_formula_del_reference (CAR (p));
      ccl_list_delete (((prestaf_boolean_formula *) F)->operands);
      break;

    case PF_FORALL:
    case PF_EXISTS:
      prestaf_formula_del_reference (((prestaf_quantifier *) F)->F);
      break;

    default:
      prestaf_linear_term_del_reference (((prestaf_linear_constraint *) F)->
					 axb);
    };

  if (F->P != NULL)
    prestaf_predicate_del_reference (F->P);

  ccl_delete (F);
}

			/* --------------- */

static void
s_get_variables (prestaf_formula *f, ccl_list *R)
{
  ccl_pair *p;
  prestaf_factor *pf;
  ccl_list *tmp;

  switch (f->type)
    {
    case PF_CST:
      break;
    case PF_NOT:
    case PF_AND:
    case PF_XOR:
    case PF_OR:
    case PF_EQUIV:
    case PF_IMPLY:
      for (p = FIRST (((prestaf_boolean_formula *) f)->operands); p;
	   p = CDR (p))
	s_get_variables (CAR (p), R);
      break;

    case PF_FORALL:
    case PF_EXISTS:
      tmp = ccl_list_create ();
      s_get_variables (((prestaf_quantifier *) f)->F, tmp);

      for (p = FIRST (tmp); p; p = CDR (p))
	{
	  if (CAR (p) == ((prestaf_quantifier *) f)->qV)
	    continue;
	  if (!ccl_list_has (R, CAR (p)))
	    ccl_list_insert (R, CAR (p), NULL);
	}
      ccl_list_delete (tmp);
      break;

    default:
      for (pf = ((prestaf_linear_constraint *) f)->axb->factors; pf;
	   pf = pf->next)
	{
	  if (!ccl_list_has (R, pf->x))
	    ccl_list_insert (R, pf->x, NULL);
	}
    };
}

			/* --------------- */

static void
s_display_factor (ccl_log_type log, prestaf_factor *ft)
{
  if (ft->a == 1)
    ccl_log (log, "%s", ft->x);
  else if (ft->a == -1)
    ccl_log (log, "-%s", ft->x);
  else
    ccl_log (log, "%d*%s", ft->a, ft->x);

}

			/* --------------- */

static void
s_compute_quantifier_solutions (prestaf_quantifier *qf, int depth)
{
  s_compute_solutions (qf->F, depth);
  if (qf->super.type == PF_FORALL)
    qf->super.P = prestaf_predicate_forall (qf->qV, qf->F->P);
  else
    qf->super.P = prestaf_predicate_exists (qf->qV, qf->F->P);
}

			/* --------------- */

static void
s_compute_linear_constraint_solutions (prestaf_linear_constraint *f)
{
  int type;
  sataf_mao *saut;
  sataf_msa *rel;
  ccl_list *vars = prestaf_formula_get_variables ((prestaf_formula *) f);

  ccl_assert (f->super.P == NULL);

  switch (f->super.type)
    {
    case PF_EQ:
      type = LCA_EQ;
      break;
    case PF_GT:
      type = LCA_GT;
      break;
    default:
      ccl_assert (f->super.type == PF_GEQ);
      type = LCA_GEQ;
      break;
    }

  {
    int *alpha = ccl_new_array (int, f->axb->nb_factors);
    prestaf_factor *pf = f->axb->factors;
    uint32_t i;

    for (i = 0; i < f->axb->nb_factors; i++, pf = pf->next)
      alpha[i] = pf->a;
    saut =
      prestaf_crt_linear_constraint_automaton (type, alpha, f->axb->nb_factors,
					       -f->axb->b);
    ccl_free (alpha);
  }

  rel = sataf_msa_compute (saut);

  f->super.P = prestaf_predicate_create (vars, rel);

  sataf_msa_del_reference (rel);
  ccl_list_delete (vars);
  sataf_mao_del_reference (saut);
}

			/* --------------- */

static ccl_list *
s_split_string (char *s, char sep)
{
  char *c;
  ccl_list *result = ccl_list_create ();

  while ((c = strchr(s, sep)) != NULL)
    {
      *c = 0;
      ccl_list_add (result, ccl_string_dup (s));
      s = c + 1;
    }
  if (*s != '\0')
    ccl_list_add (result, ccl_string_dup (s));
  return result;
}


static ccl_list *s_read_line (FILE *input)
{
  int c;
  ccl_list *lres;
  char *result = NULL;
  const int bufsize = 4;
  char buf[bufsize];
  int i = 0;

  while ((c = fgetc(input)) != EOF)
    {
      if (c == '\n')
	break;
      buf[i++] = (char) c;
      if (i == bufsize -1)
	{
	  buf[i] = 0;
	  ccl_string_format_append (&result, "%s", buf);
	  i = 0;
	}
    }

  buf[i] = 0;
  ccl_string_format_append (&result, "%s", buf);
  lres = s_split_string (result, ' ');
  ccl_delete (result);
  
  return lres;
}

static int *
s_strings_to_ints (ccl_list *l)
{
  int i = 0;
  int nb_ints = ccl_list_get_length (l);
  int *result = ccl_new_array (int, nb_ints + 1);

  result[i++] = nb_ints;
  while(! ccl_list_is_empty (l))
    {
      char *str = ccl_list_take_first(l);
      result[i++] = ccl_string_parse_int (str);
      ccl_delete (str);
    }
  ccl_list_delete (l);

  return result;
}

static FILE *
s_open_file (const char *filename)
{
  FILE *file = fopen (filename, "r");

  if (file == NULL && getenv("PRESTAF_INCLUDE") != NULL)
    {
      char *prestaf_inc = ccl_string_dup (getenv("PRESTAF_INCLUDE"));
      ccl_list *searchpaths = s_split_string (prestaf_inc, ':');

      while (! ccl_list_is_empty (searchpaths) && file == NULL)
	{
	  char *path = ccl_list_take_first (searchpaths);
	  char *tmp = ccl_string_format_new ("%s/%s", path, filename);

	  file = fopen (tmp, "r");
	  
	  ccl_delete (path);
	  ccl_delete (tmp);
	}
      ccl_list_clear_and_delete (searchpaths, ccl_string_delete);
      ccl_delete (prestaf_inc);
    }
  return file;
}

static prestaf_predicate *
s_load_file (const char *filename)
{
  prestaf_predicate *R = NULL;
  FILE *file = s_open_file (filename);
  enum { READ_VARS, READ_SIZES, READ_EDGES } state = READ_VARS;
  ccl_list *variables = NULL;
  uint32_t nb_states = 0;
  uint32_t initial = 0;
  uint32_t *succ = NULL;
  uint8_t *final = NULL;

  if (file == NULL)
    {
      ccl_error ("can't open file '%s'\n", filename);
      return NULL;
    }

  ccl_try(exception)
    {
      int line = 0;
      while (! feof (file))
	{
	  ccl_list *tokens = s_read_line (file);
	  line++;
	  if (state == READ_VARS)
	    {
	      ccl_pair *p;
	      variables = tokens;
	      for(p = FIRST (variables); p; p = CDR (p))
		{
		  char *s = CAR (p);
		  CAR(p) = ccl_string_make_unique (s);
		  ccl_delete (s);
		}
	      ccl_list_sort (variables, NULL);
	      state = READ_SIZES;
	    }
	  else if (state == READ_SIZES)
	    {
	      uint32_t i;
	      int *ints = s_strings_to_ints (tokens);

	      if (*ints < 2)
		{
		  ccl_delete (ints);
		  ccl_error ("%s:%d: invalid state line\n", filename, line);
		  ccl_throw_no_msg (exception);
		}
	      nb_states = ints[1];
	      initial = ints[2];	      
	      if (initial >= nb_states)
		{
		  ccl_delete (ints);
		  ccl_error ("%s:%d: invalid initial state index %d\n",
			     filename, line,
			     initial);
		  ccl_throw_no_msg (exception);
		}
	      succ = ccl_new_array (uint32_t, nb_states * 2);
	      final = ccl_new_array (uint8_t, nb_states);

	      for(i = 3; i < ints[0] + 1; i++)
		{
		  if (ints[i] >= nb_states)
		    {
		      ccl_delete (ints);
		      ccl_error ("%s:%d: invalid state index %d\n", filename,
				 line, ints[i]);
		      ccl_throw_no_msg (exception);
		    }
		  final[ints[i]] = 1;
		}
	      state = READ_EDGES;
	    }
	  else if (! ccl_list_is_empty (tokens))
	    {
	      int *edge;
	      int src, tgt, label;

	      ccl_assert (succ != NULL);

	      edge = s_strings_to_ints (tokens);
	      if (*edge != 3)
		{
		  ccl_delete (edge);
		  ccl_error ("%s:%d: invalid edge line\n", filename, line);
		  ccl_throw_no_msg (exception);
		}
	      src = edge[1];
	      label = edge[2];
	      tgt = edge[3];

	      ccl_delete (edge);
	      if (src >= nb_states)
		{
		  ccl_error ("%s:%d: invalid src state (%d) index in edge\n",
			     filename, line, src);
		  ccl_throw_no_msg (exception);
		}
	      if (tgt >= nb_states)
		{
		  ccl_error ("%s:%d: invalid tgt state (%d) index in edge\n",
			     filename, line, tgt);
		  ccl_throw_no_msg (exception);
		}
	      if (label != 0 && label != 1)
		{
		  ccl_error ("%s:%d: invalid label (%d) index in edge\n",
			     filename, line, label);
		  ccl_throw_no_msg (exception);
		}	      
	      succ[2 * src + label] = tgt;
	    }
	  else
	    {
	      ccl_list_delete (tokens);
	    }
	}
    }
  ccl_catch
    {
      ccl_zdelete (ccl_list_delete, variables);
      ccl_zdelete (ccl_delete, succ);
      ccl_zdelete (ccl_delete, final);
      fclose (file);

      return NULL;
    }
  ccl_end_try;

  {
    sataf_mao *mao = sataf_mao_create_from_arrays (nb_states, 2, initial,
						   final, succ);
    sataf_msa *msa = sataf_msa_compute (mao);
    sataf_mao_del_reference (mao);
    ccl_delete (succ);
    ccl_delete (final);
    R = prestaf_predicate_create(variables, msa);
    ccl_list_delete (variables);
    sataf_msa_del_reference (msa);
  }
  fclose (file);

  return R;
}

static void
s_compute_solutions (prestaf_formula *f, int depth)
{
  ccl_pair *p;
  prestaf_boolean_formula *bf = (prestaf_boolean_formula *) f;

  if (f->P != NULL)
    return;

  /*
     ccl_log(log,"[%d] compute ",depth);
     prestaf_formula_display(f);
     ccl_log(log,"\n");
   */

  switch (f->type)
    {
    case PF_FILE:
      f->P = s_load_file (((prestaf_file_formula *)f)->filename);
      if (f->P == NULL)
	ccl_error ("error while loading file\n");
      break;
    case PF_CST:
      if (((prestaf_boolean_constant *) f)->value)
	f->P = prestaf_predicate_true ();
      else
	f->P = prestaf_predicate_false ();
      break;
    case PF_NOT:
    case PF_AND:
    case PF_XOR:
    case PF_OR:
    case PF_EQUIV:
    case PF_IMPLY:
      for (p = FIRST (bf->operands); p; p = CDR (p))
	s_compute_solutions (CAR (p), depth + 1);
      p = FIRST (bf->operands);

      if (f->type == PF_AND || f->type == PF_OR)
	{
	  ccl_list *args = ccl_list_create ();
	  for (; p; p = CDR (p))
	    ccl_list_add (args, ((prestaf_formula *) CAR (p))->P);
	  if (f->type == PF_AND)
	    f->P = prestaf_predicate_multi_and (args);
	  else
	    f->P = prestaf_predicate_multi_or (args);
	  ccl_list_delete (args);
	}
      else if (f->type == PF_NOT)
	f->P = prestaf_predicate_not (((prestaf_formula *) CAR (p))->P);
      else if (f->type == PF_XOR)
	f->P = prestaf_predicate_xor (((prestaf_formula *) CAR (p))->P,
				      ((prestaf_formula *) CADR (p))->P);
      else if (f->type == PF_IMPLY)
	f->P = prestaf_predicate_imply (((prestaf_formula *) CAR (p))->P,
					((prestaf_formula *) CADR (p))->P);
      else
	f->P = prestaf_predicate_equiv (((prestaf_formula *) CAR (p))->P,
					((prestaf_formula *) CADR (p))->P);
      break;

    case PF_FORALL:
    case PF_EXISTS:
      s_compute_quantifier_solutions ((prestaf_quantifier *) f, depth + 1);
      break;

    default: /* linear constraint */
      s_compute_linear_constraint_solutions ((prestaf_linear_constraint *) f);
      break;
    };

  /*
   *  ccl_log(log,"[%d] done\n",depth);
   */
  
  ccl_post (f->P != NULL);
}

			/* --------------- */

static prestaf_formula *
s_boolean_formula (prestaf_formula_type type, ccl_list *operands)
{
  prestaf_boolean_formula *R = (prestaf_boolean_formula *)
    s_create_formula (sizeof (prestaf_boolean_formula), type);

  R->operands = operands;

  return (prestaf_formula *) R;
}

			/* --------------- */

static prestaf_formula *
s_create_boolean_formula (prestaf_formula_type type, prestaf_formula *f1, 
			  prestaf_formula *f2)
{
  prestaf_boolean_formula *R = (prestaf_boolean_formula *)
    s_create_formula (sizeof (prestaf_boolean_formula), type);

  R->operands = ccl_list_create ();
  ccl_list_add (R->operands, prestaf_formula_add_reference (f1));
  if (f2 != NULL)
    ccl_list_add (R->operands, prestaf_formula_add_reference (f2));

  return (prestaf_formula *) R;
}

			/* --------------- */

static prestaf_formula *
s_create_quantifier (prestaf_formula_type type, ccl_ustring qV, 
		     prestaf_formula *F)
{
  prestaf_quantifier *qF = (prestaf_quantifier *)
    s_create_formula (sizeof (prestaf_quantifier), type);

  qF->qV = qV;
  qF->F = prestaf_formula_add_reference (F);

  return (prestaf_formula *) qF;
}

			/* --------------- */

static prestaf_formula *
s_create_quantifier_formula_rec (prestaf_formula_type type, ccl_pair *p, 
				 prestaf_formula *F)
{
  prestaf_formula *R;

  if (CDR (p) == NULL)
    R = s_create_quantifier (type, CAR (p), F);
  else
    {
      prestaf_formula *tmp = 
	s_create_quantifier_formula_rec (type, CDR (p), F);
      R = s_create_quantifier (type, CAR (p), tmp);
      prestaf_formula_del_reference (tmp);
    }

  return R;
}

			/* --------------- */

static prestaf_formula *
s_create_linear_constraint (prestaf_formula_type type, prestaf_linear_term *t1,
			    prestaf_linear_term *t2)
{
  prestaf_linear_constraint *R = (prestaf_linear_constraint *)
    s_create_formula (sizeof (prestaf_linear_constraint), type);

  ccl_pre (t1 != NULL && t2 != NULL);

  R->axb = prestaf_linear_term_minus (t1, t2);

  return (prestaf_formula *) R;
}

			/* --------------- */

static prestaf_factor *
s_new_factor (int a, ccl_ustring x)
{
  prestaf_factor *R = ccl_new (prestaf_factor);

  R->next = NULL;
  R->a = a;
  R->x = x;

  return R;
}

			/* --------------- */

static prestaf_factor *
s_merge_factors (prestaf_factor *f1, prestaf_factor *f2,
		 int (*operator) (int, int))
{
  int a, a1, a2;
  prestaf_factor *R;
  prestaf_factor *nf1;
  prestaf_factor *nf2;
  ccl_ustring x;

  a = a1 = a2 = 0;

  if (f1 == NULL && f2 == NULL)
    return NULL;

  if (f1 == NULL)
    {
      nf1 = f1;
      nf2 = f2->next;
      a1 = 0;
      a2 = f2->a;
      x = f2->x;
    }
  else if (f2 == NULL)
    {
      nf1 = f1->next;
      nf2 = f2;
      a1 = f1->a;
      a2 = 0;
      x = f1->x;
    }
  else if (f1->x < f2->x)
    {
      nf1 = f1->next;
      nf2 = f2;
      a1 = f1->a;
      a2 = 0;
      x = f1->x;
    }
  else if (f1->x > f2->x)
    {
      nf1 = f1;
      nf2 = f2->next;
      a1 = 0;
      a2 = f2->a;
      x = f2->x;
    }
  else
    {
      nf1 = f1->next;
      nf2 = f2->next;
      a1 = f1->a;
      a2 = f2->a;
      x = f1->x;
    }

  if ((a = operator (a1, a2)) == 0)
    R = s_merge_factors (nf1, nf2, operator);
  else
    {
      R = s_new_factor (a, x);
      R->next = s_merge_factors (nf1, nf2, operator);
    }

  return R;
}

			/* --------------- */

static int
s_plus (int a1, int a2)
{
  return a1 + a2;
}

			/* --------------- */

static uint32_t
s_count_factors (prestaf_linear_term *t)
{
  uint32_t r = 0;
  prestaf_factor *pf;

  for (pf = t->factors; pf; pf = pf->next)
    r++;

  return r;
}

			/* --------------- */

static int
s_minus (int a1, int a2)
{
  return a1 - a2;
}

			/* --------------- */

static int
s_gcd (int a, int b)
{
  while (b != 0)
    {
      int c = b;
      b = a % b;
      a = c;
    }

  return a;
}

			/* --------------- */

static void
s_reduce_linear_constraint (prestaf_linear_term *t)
{
  int gcd = t->b;
  prestaf_factor *pf;
  
  if (t->nb_factors == 0)
    return;

  if (gcd < 0)
    gcd = -gcd;

  for (pf = t->factors; pf && gcd != 1; pf = pf->next)
    {
      int a = pf->a;
      if (a < 0)
	a = -a;
      gcd = s_gcd (gcd, a);
    }

  if (gcd != 1)
    {
      t->b /= gcd;
      for (pf = t->factors; pf && gcd != 1; pf = pf->next)
	pf->a /= gcd;
    }
}
