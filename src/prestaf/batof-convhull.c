/*
 * batof-convhull.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-list.h>
#include <sataf/sataf.h>
#include <ppl_c.h>
#include "prestaf-automata.h"
#include "prestaf-batof-p.h"

struct alpha_array
{
  tla_vec_t *alphas;
  int nb_alphas;
};


#define ZN ((armoise_tree *) NULL)

#define NN(_type,_vtype,_child,_next)					\
  (ccl_parse_tree_create (ARMOISE_TREE_ ## _type, #_type, _vtype,	\
			  ((_child) == ZN				\
			   ? __LINE__					\
			   : (_child)->line),				\
			  ((_child) == ZN				\
			   ? __FILE__					\
			   : (_child)->filename),			\
			  _child, _next, NULL))

#define NE(_type,_child,_next) NN (_type, CCL_PARSE_TREE_EMPTY, _child, _next)
#define U_NE(_type,_child) NE (_type, _child, ZN)
#define B_NE(_result, _type, _child1, _child2)	\
  do { \
    armoise_tree *c1 = _child1; \
    armoise_tree *c2 = _child2; \
    (_result) = U_NE (_type, c1); \
    c1->next = c2; \
  } while (0)

#define NID()  NN (IDENTIFIER, CCL_PARSE_TREE_IDENT, ZN, ZN)
#define NINT() NN (INTEGER, CCL_PARSE_TREE_INT, ZN, ZN)
#define NSTR() NN (STRING, CCL_PARSE_TREE_STRING, ZN, ZN)
#define REVERSE(_t) ccl_parse_tree_reverse_siblings (_t)

			/* --------------- */

static int
s_mpz_to_si (mpz_t z);

static sataf_msa *
s_unfold_automaton (int m, sataf_msa *msa);

static armoise_tree *
s_compute_formula_2 (int m, sataf_msa *msa, ccl_ustring *X);

static int
s_is_zero_vec (sataf_sa *sa);

static ppl_Polyhedron_t *
s_compute_polyhull_without_prefix (int m, sataf_msa *msa);

static struct alpha_array *
s_gather_alphas (int m, sataf_msa *msa, tla_vec_t xhi, int *p_nb_array);

static tla_vec_t *
s_compute_alphas_for_comp (int m, sataf_msa *msa, btf_saffcomp *comp, 
			   int nb_bounds, btf_bound *bounds, int *nb_alphas);

static void
s_compute_xhi_sigma0 (int m, sataf_msa *msa, tla_vec_t xhi);

static int
s_compute_alpha (tla_vsp_t V, tla_vsp_t B, tla_vec_t alpha);

static sataf_msa *
s_compute_bound_automaton (tla_vec_t alpha, int op);

static ccl_ustring *
s_string_to_unique_strings (int m, const char * const *names);

static armoise_tree *
s_crt_vector_of_ids (int m, ccl_ustring *X);

static armoise_tree *
s_trivial_predicate (int is_empty, int m, ccl_ustring *X);

static armoise_tree *
s_crt_definition (ccl_ustring id, armoise_tree *P);

static void
s_clear_alpha_array (struct alpha_array *a);

static void
s_build_linear_constraint (int m, tla_vec_t alpha, tla_vec_t xhi,
			   ppl_Constraint_t *result);

static void
s_build_polyhedron (int m, int nb_alphas, tla_vec_t *alphas,  tla_vec_t xhi, 
		    ppl_Polyhedron_t *result);

static void
s_build_polyhedral_hull (int m, int nb_arrays, struct alpha_array *arrays,
			 tla_vec_t xhi, ppl_Polyhedron_t *result);

static void
s_polyhedron_assign_gamma (int m, ppl_Polyhedron_t *R, 
			   ppl_const_Polyhedron_t P,  int b);

static armoise_tree *
s_polyhedron_to_armoise_tree (int m, ppl_const_Polyhedron_t P, ccl_ustring *X);

static void
s_delete_polyhedron (ppl_Polyhedron_t *P);

static void
s_display_polyhedron (ccl_log_type log, int m, ppl_const_Polyhedron_t P, 
		      ccl_ustring *X);

			/* --------------- */

armoise_tree *
btf_msa_to_convex_hull (int m, sataf_msa *msa, const char * const *varnames)
{
  armoise_tree *result;
  ccl_ustring *us = s_string_to_unique_strings (m, varnames);

  if (sataf_msa_is_zero (msa) || sataf_msa_is_one (msa))
    {
      result = s_trivial_predicate (sataf_msa_is_zero (msa), m, us);
      result = U_NE (PREDICATE, result);
    }
  else
    {
      sataf_msa *umsa = s_unfold_automaton (m, msa);      
      result = s_compute_formula_2 (m, umsa, us);
      sataf_msa_del_reference (umsa);  
    }

  ccl_delete (us);

  return result;
}

			/* --------------- */

static sataf_msa *
s_unfold_automaton (int m, sataf_msa *msa)
{
  sataf_mao *unfold;
  sataf_msa *X;

  if (ccl_debug_is_on)
    {
      ccl_debug ("Unfold MSA     %p: ", msa);
      sataf_msa_log_info(CCL_LOG_DEBUG, msa);
      ccl_debug ("\n");
    }

  unfold = prestaf_crt_unfolding_automaton (m, msa);
  X = sataf_msa_compute (unfold);
  sataf_mao_del_reference (unfold);

  if (ccl_debug_is_on)
    {
      ccl_debug ("Unfolding done %p: ", X);
      sataf_msa_log_info(CCL_LOG_DEBUG, X);
      ccl_debug ("\n");
    }

  return X;
}

			/* --------------- */

struct prefix {
  int b;
  struct prefix *prev;
};

			/* --------------- */

static int
s_mpz_to_si (mpz_t z)
{
  ccl_assert (mpz_fits_sint_p (z));
  return mpz_get_si (z);
}

			/* --------------- */

static ccl_ustring *VARS;

static ppl_Polyhedron_t *
s_compute_poly_hull_rec (int m, int varindex, sataf_msa *msa, ccl_hash *cache) 
{  
  ppl_Polyhedron_t **cacheline;
  ppl_Polyhedron_t *result = NULL;

  if (ccl_hash_find (cache, msa))
    cacheline = (ppl_Polyhedron_t **) ccl_hash_get (cache);
  else
    {
      msa = sataf_msa_add_reference (msa);
      cacheline = ccl_new_array (ppl_Polyhedron_t *, m);
      ccl_hash_insert (cache, cacheline);
    }

  if (cacheline[varindex] != NULL)
    result = cacheline[varindex];
  else 
    {
      if (sataf_msa_is_zero (msa))
	{
	  result = ccl_new (ppl_Polyhedron_t);
	  ppl_new_C_Polyhedron_from_space_dimension (result, m, 1);
	}
      else if (varindex != 0 || 
	       !(btf_is_untransient (msa->A) || 
		 sataf_ea_get_nb_states (msa->A->automaton) > 1))
	{
	  int b;
	  ppl_Polyhedron_t *C[2];
	  sataf_msa *succ;
	  
	  for (b = 0; b < 2; b++)
	    {
	      succ = sataf_msa_succ (msa, b);
	      C[b] = s_compute_poly_hull_rec (m, (varindex + 1) % m, succ,
					      cache);
	      sataf_msa_del_reference (succ);
	    }

	  if (C[0] != NULL && C[1] != NULL)
	    {
	      result = ccl_new (ppl_Polyhedron_t);
	      s_polyhedron_assign_gamma (m, result, *C[0], 0);
	      s_polyhedron_assign_gamma (m, result, *C[1], 1);
	    }
	}
      else
	{
	  result = s_compute_polyhull_without_prefix (m, msa);
	}

      if (result != NULL)
	cacheline[varindex] = result;
    }
  if(1) s_display_polyhedron (CCL_LOG_ERROR, m, *result, VARS);

  return result;
}

			/* --------------- */

static armoise_tree *
s_compute_formula_2 (int m, sataf_msa *msa, ccl_ustring *X)
{
  armoise_tree *R = NULL;
  ppl_Polyhedron_t *h;
  VARS = X;

  if (sataf_ea_get_nb_states (msa->A->automaton) > 1)
    h = s_compute_polyhull_without_prefix (m, msa);
  else
    {
      ccl_pointer_iterator *i;
      ccl_hash *cache = ccl_hash_create (NULL, NULL, 
					 (ccl_delete_proc *) 
					 sataf_msa_del_reference,
					 NULL);
      h = s_compute_poly_hull_rec (m, 0, msa, cache);
      if (h != NULL)
	{
	  ppl_Polyhedron_t *tmp = ccl_new (ppl_Polyhedron_t);

	  ppl_new_C_Polyhedron_from_C_Polyhedron (tmp, *h);
	  h = tmp;
	}

      i = ccl_hash_get_elements (cache);
      while (ccl_iterator_has_more_elements (i))
	{
	  int j;

	  ppl_Polyhedron_t **cl = 
	    (ppl_Polyhedron_t **) ccl_iterator_next_element (i);
	  for (j = 0; j < m; j++)
	    {
	      ppl_Polyhedron_t *P = cl[j];
	      
	      ccl_zdelete (s_delete_polyhedron, P);
	    }
	  ccl_delete (cl);
	}
      ccl_iterator_delete (i);
      ccl_hash_delete (cache);
    }

  if (h != NULL)
    {
      R = s_polyhedron_to_armoise_tree (m, *h, X);
      ppl_delete_Polyhedron (*h);
      ccl_delete (h);
    }

  return R;
}

			/* --------------- */

static int
s_is_zero_vec (sataf_sa *sa)
{
  int one_final = 0;
  sataf_ea *ea = sa->automaton;
  int s, maxs = sataf_ea_get_nb_states (ea);

  for (s = 0; s < maxs; s++)
    {
      int succ = sataf_ea_get_successor (ea, s, 0);
      if (sataf_ea_is_exit_state (succ))
	return 0;
      one_final = one_final || sataf_ea_is_final (ea, s);
      succ = sataf_ea_get_successor (ea, s, 1);      
      if (sataf_ea_is_local_state (succ))
	return 0;
      succ = sataf_ea_decode_succ_state (succ);
      if (! sataf_msa_is_zero (sa->bind[succ]))
	return 0;
    }

  return one_final;
}

			/* --------------- */

static armoise_tree *
s_crt_vector_of_ids (int m, ccl_ustring *X)
{
  int i;
  armoise_tree *proto;
  armoise_tree *vars = NULL;

  for (i = m - 1; i >= 0; i--)
    {      
      armoise_tree *V = NID ();
      
      V->value.id_value = X[i];
      V->next = vars;
      vars = V;
    }
      
  proto = U_NE (VECTOR, vars);

  return proto;
}

			/* --------------- */

static armoise_tree *
s_trivial_predicate (int is_empty, int m, ccl_ustring *X)
{
  int i;
  armoise_tree *proto = s_crt_vector_of_ids (m, X);
  armoise_tree *R_n = NULL;
  armoise_tree *result;

  for (i = 0; i < m; i++)
    {
      armoise_tree *tmp = is_empty ? U_NE (REALS, ZN): U_NE (NATURALS, ZN);
      tmp->next = R_n;
      R_n = tmp;
    }

  R_n = U_NE (CARTESIAN_PRODUCT, R_n);
  B_NE (proto, SET, proto, R_n);
  result = is_empty ? U_NE (FALSE, ZN) : U_NE (TRUE, ZN);
  B_NE (result, SET, proto, result);

  return result;
}

			/* --------------- */

static ppl_Polyhedron_t *
s_compute_polyhull_without_prefix (int m, sataf_msa *msa)
{
  ppl_Polyhedron_t *result = NULL;

  if (sataf_msa_is_zero (msa))
    {
      result = ccl_new (ppl_Polyhedron_t);
      ppl_new_C_Polyhedron_from_space_dimension (result, m, 1);
    }
  else if (sataf_msa_is_one (msa))
    {
      result = ccl_new (ppl_Polyhedron_t);
      ppl_new_C_Polyhedron_from_space_dimension (result, m, 0);
    }
  else
    {
      tla_vec_t xhi;
      int nb_arrays;
      struct alpha_array *arrays;

      tla_vec_init (xhi, m);
      arrays = s_gather_alphas (m, msa, xhi, &nb_arrays);

      if (arrays != NULL)
	{
	  int i;

	  result = ccl_new (ppl_Polyhedron_t);
	  s_build_polyhedral_hull (m, nb_arrays, arrays, xhi, result);

	  for (i = 0; i < nb_arrays; i++)
	    {
	      int j;

	      for (j = 0; j < arrays[i].nb_alphas; j++)
		tla_vec_clear (arrays[i].alphas[j]);
	      ccl_delete (arrays[i].alphas);
	    }
	  ccl_delete (arrays);
	}

      tla_vec_clear (xhi);
    }

  return result;
}

			/* --------------- */

static struct alpha_array *
s_gather_alphas (int m, sataf_msa *msa, tla_vec_t xhi, int *p_nb_array)
{
  ccl_hash *sa_tables = NULL;    
  btf_bound *bounds = NULL;
  int nb_bounds = 0;
  ccl_list *Tcomps = btf_compute_components (m, msa, &sa_tables);  
  int is_presburger = (Tcomps != NULL);
  struct alpha_array *result;
  int nb_array = 0;

  if (! is_presburger)
    return NULL;

  s_compute_xhi_sigma0 (m, msa, xhi);
  btf_msa_to_bounds (m, msa, Tcomps, sa_tables, &bounds, &nb_bounds);
  ccl_hash_delete (sa_tables);
  result = ccl_new_array(struct alpha_array, ccl_list_get_size (Tcomps));

  while (! ccl_list_is_empty (Tcomps) && is_presburger)
    {
      btf_saffcomp *comp = (btf_saffcomp *) ccl_list_take_first (Tcomps);

      result[nb_array].alphas = 
	s_compute_alphas_for_comp (m, msa, comp, nb_bounds, bounds, 
				   &result[nb_array].nb_alphas);
      is_presburger = (result[nb_array].alphas != NULL);
      if (is_presburger)
	nb_array++;
      
      btf_saffcomp_delete (comp);
    }

  ccl_list_clear_and_delete (Tcomps, (ccl_delete_proc *) btf_saffcomp_delete);
  while (nb_bounds--)
    {
      tla_vsp_clear (bounds[nb_bounds].V);
      tla_saff_clear (bounds[nb_bounds].B);
    }
  ccl_delete (bounds);

  if (is_presburger)
    *p_nb_array = nb_array;
  else
    {
      int i;

      for (i = 0; i < nb_array; i++)
	s_clear_alpha_array (result+i);
      ccl_delete (result);
      result = NULL;
    }

  return result;
}

			/* --------------- */

static tla_vec_t *
s_compute_alphas_for_comp (int m, sataf_msa *msa, btf_saffcomp *comp, 
			   int nb_bounds, btf_bound *bounds,  int *p_nb_alphas)
{
  int i;
  int is_presburger;
  btf_bound *comp_bound;
  tla_vec_t alpha;
  tla_vec_t *result;
  int nb_alphas = 0;
  sataf_msa *compmsa;
  int op[] = { LCA_GEQ, LCA_LEQ };
  int nb_op = sizeof (op) / sizeof (op[0]);

  if (s_is_zero_vec (comp->C))
    {
      result = ccl_new_array (tla_vec_t, 1);
      tla_vec_init (*result, m);
      *p_nb_alphas = 1;

      return result;
    }

  for (i = 0; i < nb_bounds && ! tla_vsp_are_equal (bounds[i].V, comp->V); i++)
    /* do nothing */ ;
  ccl_assert (i < nb_bounds);
  comp_bound = bounds + i;

  tla_vec_init (alpha, m);
  result = ccl_new_array (tla_vec_t, comp_bound->B->size);
  compmsa = sataf_msa_find_or_add (comp->C, comp->q);

  for (i = 0; i < comp_bound->B->size; i++)
    {
      int o;
      int stop = 0;
      
      is_presburger = s_compute_alpha (comp->V, comp_bound->B->V[i], alpha);
      
      if (!is_presburger)
	break;

      for (o = 0; o < nb_op && !stop; o++)
	{
	  sataf_msa *A_h_op = s_compute_bound_automaton (alpha, op[o]);
	  if (sataf_msa_is_included_in (compmsa, A_h_op))
	    {
	      tla_vec_init (result[nb_alphas], m);
	      if (op[o] == LCA_GEQ)
		tla_vec_z_mul_si(result[nb_alphas], -1, alpha);
	      else
		tla_vec_set(result[nb_alphas], alpha);
	      nb_alphas++;
	      stop = 1;
	    }
	  sataf_msa_del_reference (A_h_op);
	}
    }

  sataf_msa_del_reference (compmsa);
  tla_vec_clear (alpha);
  if (is_presburger)
    *p_nb_alphas = nb_alphas;
  else
    {
      for (i = 0; i < nb_alphas; i++)
	tla_vec_clear (result[i]);
      ccl_delete (result);
      result = NULL;
    }

  return result;
}

/*!
 * Compute \f$\xhi(\sigma_0)\f$ where \f$\sigma_0\f$ is a loop starting in
 * the state \a msa. 
 */
static void
s_compute_xhi_sigma0 (int m, sataf_msa *msa, tla_vec_t xhi)
{
  int len;
  int s;
  int initial =  msa->initial;
  int S0 = m * initial;
  sataf_ea *ea = msa->A->automaton;
  int nb_states = sataf_ea_get_nb_states (ea);
  ccl_bittable *visited = ccl_bittable_create (m * nb_states);
  ccl_list *Q = ccl_list_create ();
  int *pred = ccl_new_array (int, m * nb_states);
  tla_vec_t tmp;

  tla_vec_init (tmp, m);

  for (s = 0; s < m * nb_states; s++)
    pred[s] = -1;

  ccl_bittable_set (visited, S0);
  ccl_list_add (Q, (void *) (intptr_t) S0);

  while (!ccl_list_is_empty (Q))
    {
      int b, q, i;

      s = (intptr_t) ccl_list_take_first (Q);
      q = s / m;
      i = s % m;

      for (b = 0; b < 2; b++)
	{
	  int t;
	  int qp = sataf_ea_get_successor (ea, q, b);

	  if (sataf_ea_is_exit_state (qp))
	    continue;
	  qp = sataf_ea_decode_succ_state (qp);
	  t = m * qp + (i+1) % m;
	  
	  if (pred[t] < 0)
	    pred[t] = (s << 1)+b;

	  if (! ccl_bittable_has (visited, t))
	    {
	      ccl_bittable_set (visited, t);
	      ccl_list_add (Q, (void *) (intptr_t) t);
	    }
	}
    }

  s = S0;
  tla_vec_set_zero (xhi);
  len = 0;
  ccl_assert (pred [s] >= 0);

  do 
    {   
      int b = pred[s] & 0x1;
      s = pred[s] >> 1;
      len++;
      tla_vec_gamma (tmp, xhi, b);
      tla_vec_set (xhi, tmp);
    }
  while (s != S0);

  ccl_assert (len % m == 0);
  len /= m;

  {
    mpz_t d;

    mpz_init_set_si (d, -1);
    mpz_mul_2exp (d, d, len);
    mpz_add_ui(d, d, 1);
    tla_vec_z_div (tmp, xhi, d);
    tla_vec_set (xhi, tmp);
    tla_vec_canonicalize(xhi);
    mpz_clear (d);
  }

  tla_vec_clear (tmp);
  ccl_bittable_delete (visited);
  ccl_list_delete (Q);
  ccl_delete (pred);
}

/*!
 * compute the precedence function defined by a breadth first search from the
 * state 'from' until the state 'to' is reached. Using the *p_preds relation
 * one gets a shortest path linking 'from' and 'to'. Elements *p_preds encode
 * the predecessor toward 'from' and the letter (on the first bit) used to 
 * reach the state in the following way: if b = (*p_preds[i] & 0x1) and t = 
 * (*p_preds[i] >> 1) then there is the transition  t |- b -> i belongs to
 * the shortest path from 'from' to 'i'.
 */
static void
s_compute_shortest_path (sataf_ea *ea, int from, int to, int **p_preds, 
			 int *p_preds_size)
{
  int s;
  int stop = 0;
  int nb_states = sataf_ea_get_nb_states (ea);
  int *preds = *p_preds;
  int preds_size = *p_preds_size;
  ccl_bittable *visited = ccl_bittable_create (nb_states);
  ccl_list *Q = ccl_list_create ();

  if (preds_size < nb_states)
    {
      *p_preds = preds = (int *) ccl_realloc (preds, sizeof (int) * nb_states);
      *p_preds_size = preds_size = nb_states;
    }

  for (s = 0; s < nb_states; s++)
    preds[s] = -1;

  ccl_list_add (Q, (void *) (intptr_t) from);
  ccl_bittable_set (visited, from);

  while (! ccl_list_is_empty (Q) && ! stop)
    {
      int b;

      s = (intptr_t) ccl_list_take_first (Q);

      for (b = 0; b < 2 && ! stop; b++)
	{
	  int t = sataf_ea_get_successor (ea, s, b);

	  if (sataf_ea_is_exit_state (t))
	    continue;
	  t = sataf_ea_decode_succ_state (t);

	  if (! ccl_bittable_has (visited, t))
	    {
	      ccl_bittable_set (visited, t);
	      ccl_list_add (Q, (void *) (intptr_t) t);
	      preds[t] = (s << 1) + b;
	    }

	  stop = (t == to);
	}
    }
  ccl_list_delete (Q);
  ccl_bittable_delete (visited);
}

			/* --------------- */

static int
s_compute_rho_rec (int m, sataf_msa *msa, tla_vec_t rho, ccl_list *Tcomps,
		   int **p_preds, int *p_preds_size, btf_saffcomp **p_comp)
{
  int s;
  int result = 0;
  sataf_ea *ea = msa->A->automaton;

  if (sataf_msa_is_zero (msa))
    return -1;

  if (btf_is_untransient (msa->A))
    {      

      ccl_pair *p;
      btf_saffcomp *comp = NULL;

      /* look for a saff component (msa->A, q) for some adhoc q in 
	 msa->A->automaton */
      for (p = FIRST(Tcomps); p != NULL; p = CDR(p))
	{
	  comp = (btf_saffcomp *) CAR(p);

	  if (comp->C == msa->A)
	    break;
	}

      if (p == NULL)
	return -1;

      tla_vec_set_zero (rho);      
      s = comp->q;
      *p_comp = comp;
    }
  else
    { 
      int b;
      int out;
      int e = 0;
      int nb_states = sataf_ea_get_nb_states (ea);
      int nb_exits = sataf_ea_get_nb_exits (ea);

      ccl_assert (nb_exits == 0 || ! sataf_msa_is_zero (msa->A->bind[0]));

      do 
	{
	  out = sataf_ea_encode_succ_as_exit_state (e);
	  result = s_compute_rho_rec (m, msa->A->bind[e], rho, Tcomps, p_preds,
				      p_preds_size, p_comp);
	  e++;
	}
      while (result == -1 && e < nb_exits);

      if (result == -1)
	return -1;

      for (s = 0; s < nb_states; s++)
	{	  
	  b = 0;
	  if (sataf_ea_get_successor (ea, s, b) == out)
	    break;
	  b = 1;
	  if (sataf_ea_get_successor (ea, s, b) == out)
	    break;
	}

      ccl_assert (s < nb_states);

      tla_vec_gamma (rho, rho, b);
      result++;
    }

  if (s != msa->initial)
    {
      s_compute_shortest_path (ea, msa->initial, s, p_preds, p_preds_size);
  
      while (s != msa->initial)
	{
	  int b = (*p_preds)[s] & 0x1;
	  s = (*p_preds)[s] >> 1;
	  tla_vec_gamma (rho, rho, b);
	  result++;
	}
    }

  ccl_assert (btf_is_untransient (msa->A) || result > 0);

  return result;
}

			/* --------------- */

static int
s_compute_alpha (tla_vsp_t V, tla_vsp_t B, tla_vec_t alpha)
{
  int i, m, n;
  tla_matrix_t alphas;

  if (B->dim != V->dim-1)
    return 0;

  if (! tla_vsp_is_included_in (B, V))
    return 0;

  tla_vec_set_zero (alpha);
  tla_vsp_to_alphas (B, alphas);
  m = tla_matrix_get_nb_lines(alphas);
  n = tla_matrix_get_nb_columns(alphas);

  for (i = 0; i < V->dim; i++)
    {
      tla_matrix_mul_vector (alpha, alphas, V->gen[i].v);
      
      if (! tla_vec_is_zero (alpha))
	break;
    }

  ccl_assert (i < V->dim);
  for (i = 0; i < m; i++)
    {
      if (mpz_sgn (alpha->num[i]) != 0)
	{
	  int k;
	  for (k = 0; k < n; k++)
	    mpz_set (alpha->num[k], MM (alphas, i+1, k+1));
	  mpz_set_ui (alpha->den, 1);
	  break;
	}
    }
  tla_matrix_clear (alphas);

  return 1;
}

			/* --------------- */

static sataf_msa *
s_compute_bound_automaton (tla_vec_t alpha, int op)
{
  int i;
  int *args;
  sataf_mao *mao;
  sataf_msa *result;

  ccl_pre (mpz_cmp_si (alpha->den, 1) == 0);

  args = ccl_new_array (int, alpha->size);
  for (i = 0; i < alpha->size; i++)
    args[i] = s_mpz_to_si (alpha->num[i]);
  mao = prestaf_crt_linear_constraint_automaton(op, args, alpha->size, 0);
  result = sataf_msa_compute (mao);
  sataf_mao_del_reference (mao);
  ccl_delete (args);

  return result;
}

			/* --------------- */

static ccl_ustring *
s_string_to_unique_strings (int m, const char * const *names)
{
  int i;
  ccl_ustring *result = ccl_new_array (ccl_ustring, m);

  for (i = 0; i < m; i++)
    result[i] = ccl_string_make_unique (names[i]);

  return result;
}

			/* --------------- */

static armoise_tree *
s_crt_definition (ccl_ustring id, armoise_tree *P)
{
  armoise_tree *result;
  armoise_tree *t_id = NID();

  t_id->value.id_value = ccl_string_make_unique (id);
  if (P->node_type != ARMOISE_TREE_PREDICATE)
    P = U_NE (PREDICATE, P);
  B_NE (result, DEFINITION, t_id, P);

  return result;
}

			/* --------------- */

static void
s_clear_alpha_array (struct alpha_array *a)
{
  int i;

  for (i = 0; i < a->nb_alphas; i++)
    tla_vec_clear (a->alphas[i]);
  ccl_delete (a->alphas);
}

			/* --------------- */

static void
s_build_linear_constraint (int m, tla_vec_t alpha, tla_vec_t xhi,
			   ppl_Constraint_t *result)
{
  int i;
  mpq_t alpha_xhi;
  tla_vec_t z_alpha;
  ppl_Linear_Expression_t le;
  ppl_Coefficient_t C;

  mpq_init (alpha_xhi);  
  tla_vec_init (z_alpha, alpha->size);
  tla_vec_z_mul (z_alpha, xhi->den, alpha);  
  tla_vec_scalar_product(alpha_xhi, alpha, xhi);

  ppl_new_Coefficient (&C);
  ppl_new_Linear_Expression_with_dimension (&le, m);

  for (i = 0; i < m; i++)
    {
      ppl_assign_Coefficient_from_mpz_t (C, z_alpha->num[i]);
      ppl_Linear_Expression_add_to_coefficient (le, i, C);
    }

  mpq_neg (alpha_xhi, alpha_xhi);
  ppl_assign_Coefficient_from_mpz_t (C, mpq_numref (alpha_xhi));
  ppl_Linear_Expression_add_to_inhomogeneous (le, C);
  mpq_clear (alpha_xhi);
  tla_vec_clear (z_alpha);

  ccl_assert (ppl_Linear_Expression_OK (le) >= 0);

  ppl_new_Constraint (result, le, PPL_CONSTRAINT_TYPE_LESS_OR_EQUAL);

  ppl_delete_Linear_Expression (le);  
  ppl_delete_Coefficient (C);

  ccl_assert (ppl_Constraint_OK (*result) >= 0);
}

			/* --------------- */

static void
s_build_xi_positive_constraint (int m, int i, ppl_Constraint_t *result)
{
  ppl_Linear_Expression_t le;
  ppl_Coefficient_t C;
  mpz_t one;

  mpz_init_set_si (one, 1);
  ppl_new_Coefficient (&C);
  ppl_assign_Coefficient_from_mpz_t (C, one);
  ppl_new_Linear_Expression_with_dimension (&le, m);
  ppl_Linear_Expression_add_to_coefficient (le, i, C);
  ccl_assert (ppl_Linear_Expression_OK (le) >= 0);

  ppl_new_Constraint (result, le, PPL_CONSTRAINT_TYPE_GREATER_OR_EQUAL);

  ppl_delete_Linear_Expression (le);  
  ppl_delete_Coefficient (C);
  mpz_clear (one);

  ccl_assert (ppl_Constraint_OK (*result) >= 0);
}

			/* --------------- */

static void
s_build_polyhedron (int m, int nb_alphas, tla_vec_t *alphas, tla_vec_t xhi, 
		    ppl_Polyhedron_t *result)
{
  int i;

  if (nb_alphas == 1 && tla_vec_is_zero (alphas[0]))
    {
      ppl_Generator_t g;
      ppl_Linear_Expression_t le;
      ppl_Coefficient_t c;
      mpz_t one;

      mpz_init_set_si (one, 1);

      ppl_new_C_Polyhedron_from_space_dimension (result, m, 1);
      ppl_new_Linear_Expression_with_dimension (&le, m);
      ppl_new_Coefficient_from_mpz_t (&c, one);
      
      ppl_new_Generator (&g, le, PPL_GENERATOR_TYPE_POINT, c);

      ppl_Polyhedron_add_generator (*result, g);

      ppl_delete_Linear_Expression (le);
      ppl_delete_Coefficient (c);
      mpz_clear (one);
    }
  else
    {
      ppl_new_C_Polyhedron_from_space_dimension (result, m, 0);
      for (i = 0; i < nb_alphas; i++)
	{
	  ppl_Constraint_t C;

	  s_build_linear_constraint (m, alphas[i], xhi, &C);
	  ppl_Polyhedron_add_constraint (*result, C);
	  ppl_delete_Constraint (C);
	}

      /* add x_i >= 0 constraints */

      for (i = 0; i < m; i++)
	{
	  ppl_Constraint_t C;

	  s_build_xi_positive_constraint (m, i, &C);
	  ppl_Polyhedron_add_constraint (*result, C);
	  ppl_delete_Constraint (C);
	}
    }
  ccl_assert (ppl_Polyhedron_OK (*result) >= 0);
}


			/* --------------- */


static void
s_build_polyhedral_hull (int m, int nb_arrays, struct alpha_array *arrays,
			 tla_vec_t xhi, ppl_Polyhedron_t *result)
{
  int i;

  ppl_new_C_Polyhedron_from_space_dimension (result, m, 1);

  for (i = 0; i < nb_arrays; i++)
    {
      ppl_Polyhedron_t p;

      s_build_polyhedron (m, arrays[i].nb_alphas, arrays[i].alphas, xhi, &p);

      ppl_Polyhedron_poly_hull_assign (*result, p);
      ppl_delete_Polyhedron (p);
    }
  ccl_assert (ppl_Polyhedron_OK (*result) >= 0);
}

			/* --------------- */

static void
s_apply_gamma_to_constraint (int m, ppl_Constraint_t *R, 
			     ppl_const_Constraint_t C, int b)
{
  int i;
  ppl_Linear_Expression_t le;
  ppl_Coefficient_t a;
  ppl_Coefficient_t am;

  ppl_new_Linear_Expression (&le);
  ppl_new_Coefficient (&a);
  ppl_new_Coefficient (&am);

  ppl_Constraint_coefficient (C, m - 1, am);

  for (i = m - 1; i >= 1; i--)
    {
      ppl_Constraint_coefficient (C, i - 1, a);
      ppl_Linear_Expression_add_to_coefficient (le, i, a);
      ppl_Linear_Expression_add_to_coefficient (le, i, a);
    }

  ppl_Linear_Expression_add_to_coefficient (le, 0, am);

  ppl_Constraint_inhomogeneous_term (C, a);
  ppl_Linear_Expression_add_to_inhomogeneous (le, a);
  ppl_Linear_Expression_add_to_inhomogeneous (le, a);

  if (b == 1)
    {
      mpz_t aux;

      mpz_init (aux);
      ppl_Coefficient_to_mpz_t (am, aux);
      mpz_neg (aux, aux);
      ppl_assign_Coefficient_from_mpz_t (am, aux);
      ppl_Linear_Expression_add_to_inhomogeneous (le, am);
    }

  ppl_delete_Coefficient (a);

  ppl_new_Constraint (R, le, ppl_Constraint_type (C));
  ppl_delete_Linear_Expression (le);
}

			/* --------------- */

static void
s_polyhedron_assign_gamma (int m, ppl_Polyhedron_t *R, 
			   ppl_const_Polyhedron_t P, int b)
{
  ppl_Polyhedron_t A;
  ppl_const_Constraint_System_t cs;
  ppl_Constraint_System_const_iterator_t ci;
  ppl_Constraint_System_const_iterator_t ci_end;

  ppl_new_C_Polyhedron_from_space_dimension (&A, m, 0);
  ppl_Polyhedron_get_constraints (P, &cs);

  ppl_new_Constraint_System_const_iterator (&ci);
  ppl_new_Constraint_System_const_iterator (&ci_end);
  ppl_Constraint_System_begin (cs, ci);
  ppl_Constraint_System_end (cs, ci_end);

  while (! ppl_Constraint_System_const_iterator_equal_test (ci, ci_end))
    {
      ppl_Constraint_t gammaC;
      ppl_const_Constraint_t C;

      ppl_Constraint_System_const_iterator_dereference (ci, &C);
      s_apply_gamma_to_constraint (m, &gammaC, C, b);
      ppl_Polyhedron_add_constraint (A, gammaC);

      ppl_delete_Constraint (gammaC);
      ppl_Constraint_System_const_iterator_increment (ci);
    }
  ppl_delete_Constraint_System_const_iterator (ci);
  ppl_delete_Constraint_System_const_iterator (ci_end);

  if (*R == NULL)
    *R = A;
  else
    {
      ppl_Polyhedron_poly_hull_assign (*R, A);
      ppl_delete_Polyhedron (A);
    }

  s_display_polyhedron (2, 2, *R, VARS);
}

			/* --------------- */

static armoise_tree *
s_crt_x_in_nat_power_m (int m, ccl_ustring *X)
{
  int i;
  armoise_tree *t;
  armoise_tree *proto = s_crt_vector_of_ids (m, X);
  armoise_tree *nat_power_m = U_NE (NATURALS, ZN);
  armoise_tree *result;

  for (t = nat_power_m, i = 1; i < m; i++, t = t->next)
    t->next = U_NE (NATURALS, ZN);
  nat_power_m = U_NE (CARTESIAN_PRODUCT, nat_power_m);

  B_NE(result, IN, proto, nat_power_m);

  return result;
}

			/* --------------- */

static armoise_tree *
s_constraint_to_armoise_set (int m, ppl_const_Constraint_t C, ccl_ustring *X)
{
  int i;
  armoise_tree *result;
  armoise_tree *proto = s_crt_vector_of_ids (m, X);
  armoise_tree *dom = s_crt_x_in_nat_power_m (m, X);
  armoise_tree *le = NULL;
  armoise_tree *c;
  ppl_Coefficient_t a;
  mpz_t aux;
  
  B_NE (proto, IN, proto, dom);

  ppl_new_Coefficient (&a);
  mpz_init (aux);

  for (i = 0; i < m; i++)
    {
      armoise_tree *coef = NINT ();
      armoise_tree *factor = NID ();

      ppl_Constraint_coefficient (C, i, a);
      ppl_Coefficient_to_mpz_t (a, aux);

      coef->value.int_value = s_mpz_to_si (aux);
      if (coef->value.int_value == 0)
	continue;

      factor->value.id_value = X[i];
      B_NE (factor, MUL, coef, factor);
      if (le == NULL)
	le = factor;
      else 
	B_NE (le, PLUS, le, factor);
    }

  ppl_Constraint_inhomogeneous_term (C, a);
  ppl_Coefficient_to_mpz_t (a, aux);
  c = NINT ();
  c->value.int_value = -s_mpz_to_si (aux);
  
  switch (ppl_Constraint_type (C))
    {
    case PPL_CONSTRAINT_TYPE_LESS_THAN:
      B_NE(le, LT, le, c);
      break;
    case PPL_CONSTRAINT_TYPE_LESS_OR_EQUAL:
      B_NE(le, LEQ, le, c);
      break;
    case PPL_CONSTRAINT_TYPE_EQUAL:
      B_NE(le, EQ, le, c);
      break;
    case PPL_CONSTRAINT_TYPE_GREATER_OR_EQUAL:
      B_NE(le, GEQ, le, c);
      break;
    case PPL_CONSTRAINT_TYPE_GREATER_THAN:
      B_NE(le, GT, le, c);
      break;
    }
  mpz_clear (aux);
  ppl_delete_Coefficient (a);

  B_NE (result, SET, proto, le);

  return result;
}

			/* --------------- */

static armoise_tree *
s_polyhedron_to_armoise_tree (int m, ppl_const_Polyhedron_t P, ccl_ustring *X)
{
  int c = 0;
  armoise_tree *context = NULL;
  armoise_tree *set = NULL;
  armoise_tree *result;

  if (ppl_Polyhedron_is_universe (P))
    c = 1;

  if (ppl_Polyhedron_is_empty (P))
    c = 2;

  if (c)
    {
      int i;
      armoise_tree *proto = s_crt_vector_of_ids (m, X);
      armoise_tree *R_n = NULL;

      for (i = 0; i < m; i++)
	{
	  armoise_tree *tmp = U_NE (REALS, ZN);
	  tmp->next = R_n;
	  R_n = tmp;
	}
      R_n = U_NE (CARTESIAN_PRODUCT, R_n);
      B_NE (proto, IN, proto, R_n);

      set = (c == 2) ? U_NE (FALSE, ZN) : U_NE (TRUE, ZN);
      B_NE (set, SET, proto, set);
    }
  else
    {
      char tmp[100];
      int cindex = 0;
      armoise_tree **pctx = &context;      
      ppl_const_Constraint_System_t cs;
      ppl_Constraint_System_const_iterator_t ci;
      ppl_Constraint_System_const_iterator_t ci_end;

      ppl_Polyhedron_get_constraints (P, &cs);
      
      ppl_new_Constraint_System_const_iterator (&ci);
      ppl_new_Constraint_System_const_iterator (&ci_end);
      ppl_Constraint_System_begin (cs, ci);
      ppl_Constraint_System_end (cs, ci_end);

      while (! ppl_Constraint_System_const_iterator_equal_test (ci, ci_end))
	{
	  ppl_const_Constraint_t C;
	  armoise_tree *auxt;
	  ccl_ustring cid;

	  ppl_Constraint_System_const_iterator_dereference (ci, &C);
	  auxt = s_constraint_to_armoise_set (m, C, X);
	  sprintf (tmp, "C%d", cindex++);
	  cid = ccl_string_make_unique (tmp);
	  *pctx = s_crt_definition (cid, auxt);
	  pctx = &((*pctx)->next);
      
	  auxt = NID ();
	  auxt->value.id_value = cid;
      
	  if (set == NULL)
	    set = auxt;
	  else 
	    {
	      B_NE (set, INTERSECTION, set, auxt);
	    }

	  ppl_Constraint_System_const_iterator_increment (ci);
	}
      ppl_delete_Constraint_System_const_iterator (ci);
      ppl_delete_Constraint_System_const_iterator (ci_end);

      context = U_NE (PREDICATE_CONTEXT, context);
    }

  if (context == NULL)
    result = U_NE (PREDICATE, set);
  else
    B_NE (result, PREDICATE, context, set);

  return result;
}

			/* --------------- */

static void
s_delete_polyhedron (ppl_Polyhedron_t *P)
{
  ppl_delete_Polyhedron (*P);
  ccl_delete (P);
}

			/* --------------- */

static void
s_display_constraint (ccl_log_type log, int m, ppl_const_Constraint_t C, 
		      ccl_ustring *X)
{
  int first = 1;
  int i;
  ppl_Coefficient_t a;
  mpz_t aux;

  ppl_new_Coefficient (&a);
  mpz_init (aux);

  for (i = 0; i < m; i++)
    {
      int c;
      ppl_Constraint_coefficient (C, i, a);
      ppl_Coefficient_to_mpz_t (a, aux);
      c = s_mpz_to_si (aux);
      
      if (c == 0)
	continue;
      
      if (first)
	{
	  ccl_log (log, "%d * %s", c, X[i]);
	  first = 0;
	}
      else if (c < 0)
	ccl_log (log, "%d * %s", c, X[i]);
      else 
	ccl_log (log, "+%d * %s", c, X[i]);
    }

  switch (ppl_Constraint_type (C))
    {
    case PPL_CONSTRAINT_TYPE_LESS_THAN: ccl_log (log, " < "); break;
    case PPL_CONSTRAINT_TYPE_LESS_OR_EQUAL: ccl_log (log, " <= "); break;
    case PPL_CONSTRAINT_TYPE_EQUAL: ccl_log (log, " = "); break;
    case PPL_CONSTRAINT_TYPE_GREATER_OR_EQUAL: 
      ccl_log (log, " >= "); 
      break;
    case PPL_CONSTRAINT_TYPE_GREATER_THAN: ccl_log (log, " > "); break;
    }

  ppl_Constraint_inhomogeneous_term (C, a);
  ppl_Coefficient_to_mpz_t (a, aux);
  ccl_log (log, "%d", - s_mpz_to_si (aux));

  mpz_clear (aux);
  ppl_delete_Coefficient (a);
}

			/* --------------- */

static void
s_display_polyhedron (ccl_log_type log, int m, ppl_const_Polyhedron_t P, 
		      ccl_ustring *X)
{
  if (ppl_Polyhedron_is_universe (P))
    ccl_log (log, "top\n");
  else if (ppl_Polyhedron_is_empty (P))
    ccl_log (log, "bottom\n");
  else 
    {
      ppl_const_Constraint_System_t cs;
      ppl_Constraint_System_const_iterator_t ci;
      ppl_Constraint_System_const_iterator_t ci_end;

      ppl_Polyhedron_get_constraints (P, &cs);
      
      ppl_new_Constraint_System_const_iterator (&ci);
      ppl_new_Constraint_System_const_iterator (&ci_end);
      ppl_Constraint_System_begin (cs, ci);
      ppl_Constraint_System_end (cs, ci_end);
      ccl_log (log, "{ \n");
      while (! ppl_Constraint_System_const_iterator_equal_test (ci, ci_end))
	{
	  ppl_const_Constraint_t C;

	  ppl_Constraint_System_const_iterator_dereference (ci, &C);
	  ccl_log (log, "  ");
	  s_display_constraint (log, m, C, X);
	  ccl_log (log, "\n");
	  ppl_Constraint_System_const_iterator_increment (ci);
	}
      ccl_log (log, " }\n");
      ppl_delete_Constraint_System_const_iterator (ci);
      ppl_delete_Constraint_System_const_iterator (ci_end);
    }
}
