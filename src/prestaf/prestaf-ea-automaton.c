/*
 * prestaf-ea-automaton.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include "prestaf-automata.h"


struct final_cond 
{
  int refcount;
  uint8_t final[1];
};

typedef struct ea_auto_st
{
  sataf_mao super;
  int s0;
  sataf_ea *ea;
  struct final_cond *final;
} ea_auto;


			/* --------------- */

static size_t
s_ea_auto_size (sataf_mao *self);

static void
s_ea_auto_destroy (sataf_mao *self);

#define s_ea_auto_no_cache NULL

#define s_ea_auto_is_root_of_scc NULL

#define s_ea_auto_simplify NULL

static uint32_t
s_ea_auto_get_alphabet_size (sataf_mao *self);

#define s_ea_auto_to_string NULL

static sataf_mao * 
s_ea_auto_succ (sataf_mao *self, uint32_t letter);

static int
s_ea_auto_is_final (sataf_mao *self);

static int
s_ea_auto_is_equal_to (sataf_mao *self, sataf_mao *other);

static unsigned int
s_ea_auto_hashcode (sataf_mao *self);

#define s_ea_auto_to_dot NULL

			/* --------------- */


static const sataf_mao_methods EA_AUTO_METHODS = 
  {
    "SA-EA-AUTO",
    s_ea_auto_size,
    s_ea_auto_destroy,
    s_ea_auto_no_cache,
    s_ea_auto_is_root_of_scc,
    s_ea_auto_simplify,
    s_ea_auto_get_alphabet_size,
    s_ea_auto_to_string,
    s_ea_auto_succ,
    s_ea_auto_is_final,
    s_ea_auto_is_equal_to,
    s_ea_auto_hashcode,
    s_ea_auto_to_dot
  };

			/* --------------- */

static sataf_mao *
s_crt_ea_auto (int s0, sataf_ea *ea, struct final_cond *final);

			/* --------------- */

sataf_mao *
prestaf_crt_from_exit_automaton (int s0, sataf_ea *ea, const uint8_t *final)
{
  sataf_mao *result; 
  int nb_exits = sataf_ea_get_nb_exits (ea);
  struct final_cond *f = (struct final_cond *)
    ccl_malloc (sizeof (struct final_cond) + 
		(sizeof(uint8_t) * (nb_exits - 1)));

  
  f->refcount = 0;
  ccl_memcpy (f->final, final, sizeof(uint8_t) * nb_exits);

  result = s_crt_ea_auto (s0, ea, f);

  return result;
}

			/* --------------- */


static sataf_mao *
s_crt_ea_auto (int s0, sataf_ea *ea, struct final_cond *final)
{
  ea_auto *eaa = (ea_auto *) 
    sataf_mao_create (sizeof (ea_auto), &EA_AUTO_METHODS);

  eaa->s0 = s0;
  eaa->ea = sataf_ea_add_reference (ea);
  eaa->final = final;
  if (final != NULL)
    final->refcount++;

  return (sataf_mao *) eaa;
}

			/* --------------- */

static sataf_mao * 
s_ea_auto_succ (sataf_mao *self, uint32_t letter)
{
  ea_auto *eaa = (ea_auto *) self;
  sataf_mao *result;

  if (eaa->s0 < 0)
    result = sataf_mao_add_reference (self);
  else 
    {      
      int succ = sataf_ea_get_successor (eaa->ea, eaa->s0, letter);
      int local = sataf_ea_is_local_state (succ);
      succ = sataf_ea_decode_succ_state (succ);      
      if (local)
	result = s_crt_ea_auto (succ, eaa->ea, eaa->final);
      else
	result = s_crt_ea_auto (-succ, eaa->ea, eaa->final);
    }

  return result;
}

			/* --------------- */

static int
s_ea_auto_is_final (sataf_mao *self)
{
  ea_auto *eaa = (ea_auto *) self;
  int result;

  if (eaa->s0 < 0)
    {
      if (eaa->final != NULL)
	result = eaa->final->final[eaa->s0];
      else
	result = 0;
    }
  else 
    {
      result = sataf_ea_is_final (eaa->ea, eaa->s0);
    }

  return result;
}

			/* --------------- */

static size_t
s_ea_auto_size (sataf_mao *self)
{
  return sizeof (ea_auto);
}

			/* --------------- */

static void
s_ea_auto_destroy (sataf_mao *self)
{
  ea_auto *eaa = (ea_auto *) self;

  ccl_pre (ccl_imply (eaa->final != NULL, eaa->final->refcount  > 0));

  if (eaa->final != NULL)
    {
      eaa->final->refcount--;
      if (eaa->final->refcount == 0)
	ccl_delete (eaa->final);
    }
  sataf_ea_del_reference (eaa->ea);
}
			/* --------------- */

static uint32_t
s_ea_auto_get_alphabet_size (sataf_mao *self)
{
  return 2;
}

			/* --------------- */

static int
s_ea_auto_is_equal_to (sataf_mao *self, sataf_mao *other)
{
  ea_auto *eaa1 = (ea_auto *) self;
  ea_auto *eaa2 = (ea_auto *) other;

  if (!(eaa1->ea == eaa2->ea && eaa1->s0 == eaa2->s0))
    return 0;
  if (eaa1->final == eaa2->final)
    return 1;

  if (eaa1->final == NULL || eaa2->final == NULL)
    return 0;

  return ccl_memcmp (eaa1->final, eaa2->final, 
		     sizeof (uint8_t) * sataf_ea_get_nb_exits (eaa1->ea)) == 0;
}

			/* --------------- */

static unsigned int
s_ea_auto_hashcode (sataf_mao *self)
{
  ea_auto *eaa = (ea_auto *) self;

  return (uintptr_t)eaa->ea + 19 * eaa->s0;
}
