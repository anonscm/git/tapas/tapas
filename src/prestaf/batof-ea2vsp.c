/*
 * batof-ea2vsp.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <stdlib.h>
#include <time.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-bittable.h>
#include "prestaf-batof-p.h"

			/* --------------- */

typedef struct loop_search_data_st
{
  sataf_ea *ea;
  ccl_bittable *on_stack;
  ccl_bittable *visited;
  btf_loop_element *from;
  void (*apply) (btf_loop_element * e, void *clientdata);
  void *clientdata;
} loop_search_data;

struct ksi_data
{
  int m;
  tla_vec_t *ksi;
  ccl_bittable *is_set;

  tla_vec_t *ksi2;
  ccl_bittable *is_set2;
};

			/* --------------- */

static void
s_eval_ksi (btf_loop_element * loop, void *data)
{
  tla_vec_t ksi;
  tla_vec_t aux;
  struct ksi_data *I = data;
  btf_loop_element *q;
  btf_loop_element *c;

  tla_vec_init (ksi, I->m);
  tla_vec_init (aux, I->m);

#if 0
  ccl_display ("loop(%d", loop->s);
  for (q = loop; q->next; q = q->next)
    ccl_display (" -%d-> %d", q->next->l, q->next->s);
  ccl_display (")\n");
#endif

  ccl_assert (!ccl_bittable_has (I->is_set, loop->s));

  {
    int k = 0;
    mpz_t x, one, exp2;

    tla_vec_set_zero (ksi);
    do
      {
	for (c = loop; c->next; c = c->next, k++)
	  {
	    tla_vec_gamma (aux, ksi, c->next->l);
	    tla_vec_set (ksi, aux);
	  }
      }
    while ((k % I->m) != 0);

    mpz_init_set_ui (one, 1);
    mpz_init (exp2);
    mpz_init (x);
    mpz_mul_2exp (exp2, one, k / I->m);
    mpz_sub (x, one, exp2);
    tla_vec_z_div (aux, ksi, x);
    mpz_clear (one);
    mpz_clear (exp2);
    mpz_clear (x);

    tla_vec_init (I->ksi[loop->s], I->m);
    tla_vec_set (I->ksi[loop->s], aux);
    ccl_bittable_set (I->is_set, loop->s);
  }

  tla_vec_set (ksi, I->ksi[loop->s]);
  for (q = loop->next; q->next; q = q->next)
    {
      tla_vec_gamma (aux, ksi, q->l);
      tla_vec_set (ksi, aux);

      if (ccl_bittable_has (I->is_set, q->s))
	continue;

      tla_vec_init (I->ksi[q->s], I->m);
      tla_vec_set (I->ksi[q->s], ksi);
      ccl_bittable_set (I->is_set, q->s);
    }

  tla_vec_clear (ksi);
  tla_vec_clear (aux);
}

			/* --------------- */

#if 0
static void
s_eval_ksi2 (btf_loop_element * loop, void *data)
{
  tla_vec_t ksi;
  tla_vec_t aux;
  btf_loop_element *q;
  btf_loop_element *c;
  struct ksi_data *I = data;

  tla_vec_init (ksi, I->m);
  tla_vec_init (aux, I->m);

#if 0
  ccl_display ("loop(%d", loop->s);
  for (q = loop; q->next; q = q->next)
    ccl_display (" -%d-> %d", q->next->l, q->next->s);
  ccl_display (")\n");
#endif

  for (q = loop; q->next; q = q->next)
    {
      int k = 0;

      if (ccl_bittable_has (I->is_set2, q->s))
	continue;

      tla_vec_set_zero (ksi);
      do
	{
	  for (c = q; c->next; c = c->next, k++)
	    {
	      tla_vec_gamma (aux, ksi, c->next->l);
	      tla_vec_set (ksi, aux);
	    }

	  if (q != loop)
	    {
	      for (c = loop; c != q; c = c->next, k++)
		{
		  tla_vec_gamma (aux, ksi, c->next->l);
		  tla_vec_set (ksi, aux);
		}
	    }
	}
      while ((k % I->m) != 0);

      {
	mpz_t x, one, exp2;

	mpz_init_set_ui (one, 1);
	mpz_init (exp2);
	mpz_init (x);
	mpz_mul_2exp (exp2, one, k / I->m);
	mpz_sub (x, one, exp2);
	tla_vec_z_div (aux, ksi, x);
	mpz_clear (one);
	mpz_clear (exp2);
	mpz_clear (x);
      }

      tla_vec_init (I->ksi2[q->s], I->m);
      tla_vec_set (I->ksi2[q->s], aux);
      ccl_bittable_set (I->is_set2, q->s);
    }

  tla_vec_clear (ksi);
  tla_vec_clear (aux);

  if (1)
    s_eval_ksi (loop, I);
}
#endif
			/* --------------- */

static int
s_loop_length (btf_loop_element * loop)
{
  int result = 0;

  while (loop->next != NULL)
    {
      result++;
      loop = loop->next;
    }
  return result;
}

			/* --------------- */

static int
s_compute_loop (unsigned int q, btf_loop_element * loop,
		loop_search_data * data)
{
  int found_loop = (data->from != NULL && data->from->s == q);

  if (found_loop)
    {
      btf_loop_element cell;
      cell.s = q;
      cell.next = loop;

      if (ccl_debug_is_on)
	ccl_debug ("find a loop of length %d\n", s_loop_length (loop));
      data->apply (&cell, data->clientdata);
    }
  else
    {
      int i;
      int succ;
      btf_loop_element cell;
      unsigned int b[2];

      if (ccl_bittable_has (data->on_stack, q) ||
	  ccl_bittable_has (data->visited, q))
	return 0;

      ccl_bittable_set (data->on_stack, q);

      if (data->from == NULL)
	data->from = &cell;

      cell.s = q;
      cell.next = loop;
      b[0] = random () % 2;
      b[1] = (b[0] + 1) % 2;

      for (i = 0; i < 2 && !found_loop; i++)
	{
	  cell.l = b[i];
	  succ = sataf_ea_get_successor (data->ea, q, cell.l);

	  if (sataf_ea_is_exit_state (succ))
	    continue;


	  if (i == 0 && succ != (int) data->from->s)
	    {
	      int succ2 = sataf_ea_get_successor (data->ea, q, b[1]);

	      if (sataf_ea_is_local_state (succ2) &&
		  (int) (data->from->s) ==
		  sataf_ea_decode_succ_state (succ2))
		{
		  cell.l = b[1];
		  succ = succ2;
		}
	    }

	  succ = sataf_ea_decode_succ_state (succ);
	  found_loop = s_compute_loop (succ, &cell, data);
	}
      ccl_bittable_unset (data->on_stack, q);
      ccl_bittable_set (data->visited, q);
    }

  return found_loop;
}

			/* --------------- */

static int
s_apply_on_loop (sataf_ea *ea, int q0,
		 void (*apply) (btf_loop_element * e, void *data),
		 void *clientdata,
		 ccl_bittable *on_stack, ccl_bittable *visited)
{
  loop_search_data data;

  data.ea = ea;
  data.on_stack = on_stack;
  data.visited = visited;
  data.from = NULL;
  data.apply = apply;
  data.clientdata = clientdata;

  return s_compute_loop (q0, NULL, &data);
}

			/* --------------- */

int
btf_apply_on_loop (sataf_ea *ea, int q0,
		   void (*apply) (btf_loop_element * e, void *data),
		   void *clientdata)
{
  int result;

  if (ccl_debug_is_on)
    {
      ccl_debug ("build a loop in a graph of size %d : ",
		 ea->nb_local_states);
      ccl_debug_push_tab_level ();
    }

  {
    ccl_bittable *on_stack = ccl_bittable_create (ea->nb_local_states);
    ccl_bittable *visited = ccl_bittable_create (ea->nb_local_states);
    result = s_apply_on_loop (ea, q0, apply, clientdata, on_stack, visited);
    ccl_bittable_delete (on_stack);
    ccl_bittable_delete (visited);
  }

  if (ccl_debug_is_on)
    {
      ccl_debug_pop_tab_level ();
      if (result == 0)
	ccl_debug ("no loop found\n");
    }

  return result;
}

			/* --------------- */

static void
s_compute_ksi (int m, sataf_ea *ea, tla_vec_t ** pksi)
{
  int i;
  ccl_bittable *on_stack;
  ccl_bittable *visited;

  struct ksi_data I;

  if (ccl_debug_is_on)
    {
      ccl_debug ("compute ksis\n");
      ccl_debug_push_tab_level ();
    }

  on_stack = ccl_bittable_create (ea->nb_local_states);
  visited = ccl_bittable_create (ea->nb_local_states);

  I.m = m;
  I.ksi = ccl_new_array (tla_vec_t, ea->nb_local_states);
  I.ksi2 = ccl_new_array (tla_vec_t, ea->nb_local_states);
  I.is_set = ccl_bittable_create (ea->nb_local_states);
  I.is_set2 = ccl_bittable_create (ea->nb_local_states);

  for (i = 0; i < (int) ea->nb_local_states; i++)
    {
      int found_loop;

      if (ccl_bittable_has (I.is_set, i))
	continue;

      ccl_bittable_clear (visited);
      found_loop = s_apply_on_loop (ea, i, s_eval_ksi, &I, on_stack, visited);
      ccl_assert (found_loop);
    }

  ccl_bittable_delete (I.is_set);
  ccl_bittable_delete (I.is_set2);

  for (i = 0; 0 && i < (int) ea->nb_local_states; i++)
    {
      ccl_assert (tla_vec_are_equal (I.ksi[i], I.ksi2[i]));
      tla_vec_clear (I.ksi2[i]);
    }
  ccl_delete (I.ksi2);

  *pksi = I.ksi;

  ccl_bittable_delete (on_stack);
  ccl_bittable_delete (visited);

  if (ccl_debug_is_on)
    {
      ccl_debug_pop_tab_level ();
      ccl_debug ("done\n");
    }
}

			/* --------------- */

int
btf_lambda_inc (int m, ccl_bittable *lambdas, int s, int t)
{
  int l;
  int result = 0;

  if (s == t)
    {
      for (l = 0; l < m; l++)
	{
	  if (ccl_bittable_has (lambdas, s + l))
	    continue;
	  ccl_bittable_set (lambdas, s + l);
	  result = 1;
	}
    }
  else
    {
      if (1)
	{
	  result =
	    ccl_bittable_window_union_with_roll (lambdas, t, lambdas, s, m);
	}
      else
	{
#if 1
	  {
	    int r2 = 0;
	    ccl_bittable *aux = ccl_bittable_dup (lambdas);

	    result =
	      ccl_bittable_window_union_with_roll (lambdas, t, lambdas, s, m);

	    if (s == 0)
	      l = ccl_bittable_get_first (aux);
	    else
	      l = ccl_bittable_get_next (aux, s - 1);

	    while (l >= 0 && l < s + m)
	      {
		int i = t + ((l == s + m - 1) ? 0 : l - s + 1);

		if (!ccl_bittable_has (aux, i))
		  {
		    ccl_bittable_set (aux, i);
		    r2 = 1;
		  }
		l = ccl_bittable_get_next (aux, l);
	      }
	    ccl_assert (ccl_bittable_equals (aux, lambdas));
	    ccl_assert (result == r2);
	    ccl_bittable_delete (aux);
	  }

#else
	  for (l = 0; l < m - 1; l++)
	    {
	      if (ccl_bittable_has (lambdas, s + l) &&
		  !ccl_bittable_has (lambdas, t + l + 1))
		{
		  ccl_bittable_set (lambdas, t + l + 1);
		  result = 1;
		}
	    }

	  if (ccl_bittable_has (lambdas, s + l)
	      && !ccl_bittable_has (lambdas, t))
	    {
	      ccl_bittable_set (lambdas, t);
	      result = 1;
	    }
#endif
	}
    }

  return result;
}

			/* --------------- */

#if 0
static void
s_log_lambdas (ccl_log_type log, sataf_ea *ea, int q0, int m,
	       ccl_bittable *bt)
{
  int i, max = ea->nb_local_states;

  sataf_ea_display_as_dot (ea, log, NULL);
  ccl_log (log, "\n/*");

  ccl_log (log, "start from %d\n", q0);
  for (i = 0; i < max; i++)
    {
      int j;

      ccl_log (log, "%d : ", i);
      for (j = 0; j < m; j++)
	if (ccl_bittable_has (bt, i * m + j))
	  ccl_log (log, "%d ", j);
      ccl_log (log, "\n");
    }
  ccl_log (log, "*/\n");
}
#endif
			/* --------------- */

void
btf_compute_lambdas (int m, sataf_ea *ea, ccl_bittable *todo,
		     ccl_bittable *lambdas)
{
  if (ea->nb_local_states == 1)
    {
      unsigned int b;

      for (b = 0; b < 2; b++)
	{
	  int qprime = sataf_ea_get_successor (ea, 0, b);
	  int qp = sataf_ea_decode_succ_state (qprime);
	  int to_index = m * qp;

	  if (sataf_ea_is_exit_state (qprime))
	    {
	      to_index += m * ea->nb_local_states;
	      btf_lambda_inc (m, lambdas, 0, to_index);
	    }
	  else
	    btf_lambda_inc (m, lambdas, 0, to_index);
	}
    }
  else
    {
#if 1
      int i;
      int t0;
      ccl_list *Q = ccl_list_create ();

      if (ccl_debug_is_on)
	{
	  t0 = clock ();
	  ccl_debug ("compute big lambda table for EA %p\n", ea);
	  ccl_debug_push_tab_level ();
	}

      ccl_assert (todo != NULL);

      for (i = ccl_bittable_get_first (todo);
	   i >= 0; i = ccl_bittable_get_next (todo, i))
	{
	  int j, max = m * (i + 1);

	  if (i == 0)
	    j = ccl_bittable_get_first (lambdas);
	  else
	    j = ccl_bittable_get_next (lambdas, m * i - 1);

	  while (j >= 0 && j < max)
	    {
	      ccl_list_add (Q, (void *) (intptr_t) j);
	      j = ccl_bittable_get_next (lambdas, j);
	    }
	}

      while (!ccl_list_is_empty (Q))
	{
	  unsigned int b;
	  unsigned int q = (uintptr_t) ccl_list_take_first (Q);
	  int l = (int) (q % m);
	  q /= m;

	  for (b = 0; b < 2; b++)
	    {
	      int qprime = sataf_ea_get_successor (ea, q, b);
	      int qp = sataf_ea_decode_succ_state (qprime);
	      int index = m * qp + (l + 1) % m;


	      if (sataf_ea_is_exit_state (qprime))
		{
		  index += m * ea->nb_local_states;

		  if (!ccl_bittable_has (lambdas, index))
		    ccl_bittable_set (lambdas, index);
		}
	      else if (!ccl_bittable_has (lambdas, index))
		{
		  ccl_bittable_set (lambdas, index);
		  ccl_list_add (Q, (void *) (intptr_t) index);
		}
	    }
	}
      ccl_list_delete (Q);

      if (ccl_debug_is_on)
	{
	  ccl_debug_pop_tab_level ();
	  ccl_debug ("done within %d ms\n",
		     (1000 * (clock () - t0))/CLOCKS_PER_SEC);
	}
#else
      int i;

      while ((i = ccl_bittable_get_first (todo)) >= 0)
	{
	  unsigned int b;
	  unsigned int q = i;
	  int from_index = m * q;

	  ccl_bittable_unset (todo, i);
	  for (b = 0; b < 2; b++)
	    {
	      int qprime = sataf_ea_get_successor (ea, q, b);
	      int qp = sataf_ea_decode_succ_state (qprime);
	      int to_index = m * qp;

	      if (sataf_ea_is_exit_state (qprime))
		{
		  to_index += m * ea->nb_local_states;
		  btf_lambda_inc (m, lambdas, from_index, to_index);
		}
	      else if (btf_lambda_inc (m, lambdas, from_index, to_index))
		{
		  ccl_bittable_set (todo, qp);
		}
	    }
	}
#endif
    }
}

			/* --------------- */

#if CCL_ENABLE_ASSERTIONS
static int
s_is_non_trivial (sataf_ea *ea)
{
  ccl_pre (ea->alphabet_size == 2);

  return ea->nb_local_states > 1
    || sataf_ea_is_zero_or_one (ea)
    || sataf_ea_is_local_state (sataf_ea_get_successor (ea, 0, 0))
    || sataf_ea_is_local_state (sataf_ea_get_successor (ea, 0, 1));
}
#endif

			/* --------------- */

static int
s_max_lambda (int m, ccl_bittable *lambdas, int from)
{
  int i;
  int result = -1;

  if (from == 0)
    i = ccl_bittable_get_first (lambdas);
  else
    i = ccl_bittable_get_next (lambdas, from - 1);

  while (i >= 0 && i < from + m)
    {
      result = i;
      i = ccl_bittable_get_next (lambdas, i);
    }

  ccl_assert (result >= 0);

  return result - from + 1;
}

			/* --------------- */

ccl_bittable *
btf_ea_to_vsp_v2 (tla_vsp_t res, sataf_ea *ea, int *ps)
{
  int s = *ps;
  unsigned int q;
  int m = tla_vsp_get_size (res);
  ccl_bittable *lambdas;
  tla_vec_t aux1, aux2;
  tla_vec_t *ksi;
  tla_vsp_t V;

  tla_vec_init (aux1, m);
  tla_vec_init (aux2, m);
  tla_vsp_init (V, m);

  lambdas =
    ccl_bittable_create (m * (ea->nb_local_states + ea->nb_exit_states));

  ccl_bittable_set (lambdas, m * s);
  {
    ccl_bittable *todo = ccl_bittable_create (m * (ea->nb_local_states +
						  ea->nb_exit_states));
    ccl_bittable_set (todo, s);
    btf_compute_lambdas (m, ea, todo, lambdas);
    ccl_bittable_delete (todo);
  }

  s_compute_ksi (m, ea, &ksi);

  if (ccl_debug_is_on)
    ccl_debug ("now actually compute VSP...");

  for (q = 0; q < ea->nb_local_states; q++)
    {
      unsigned int b;
      int lindex = q * m;
      int max_lambda = s_max_lambda (m, lambdas, lindex);

#if 0
      ccl_display ("ksi[%d] = ", q);
      tla_vec_log (CCL_LOG_DISPLAY, ksi[q]);
      ccl_display ("\n");
#endif
      for (b = 0; b < 2; b++)
	{
	  int i;

	  int qp = sataf_ea_get_successor (ea, q, b);

	  if (sataf_ea_is_exit_state (qp))
	    continue;

	  qp = sataf_ea_decode_succ_state (qp);
	  tla_vec_inv_gamma (aux1, ksi[q], b);
	  tla_vec_sub (aux2, aux1, ksi[qp]);

	  for (i = 0; i < max_lambda; i++)
	    {
	      tla_vec_gamma (aux1, aux2, 0);
	      tla_vec_set (aux2, aux1);

	      if (ccl_bittable_has (lambdas, lindex + i))
		tla_vsp_add_generator (V, V, aux2);
	    }
	}
    }

  if (ccl_debug_is_on)
    ccl_debug ("done\n");

  tla_vsp_set (res, V);

  for (q = 0; q < ea->nb_local_states; q++)
    tla_vec_clear (ksi[q]);
  ccl_delete (ksi);

  tla_vec_clear (aux1);
  tla_vec_clear (aux2);
  tla_vsp_clear (V);

  return lambdas;
}

			/* --------------- */

typedef struct vsp_cache_line_st
{
  ccl_bittable *lambdas;
  int s;
  tla_vsp_t *vsps[1];
} vsp_cache_line;

			/* --------------- */

#if 0
static void
s_compute_vsp_from_cache_line (tla_vsp_t res, sataf_ea *ea,
			       const int s, vsp_cache_line *line)
{
  if (line->vsps[s] != NULL)
    {
      if ((ccl_debug_is_on))
	ccl_debug ("found it in cache\n");
    }
  else
    {
      int i;
      int m = tla_vsp_get_size (res);
      int osindex = line->s * m;
      int sindex = s * m;
      tla_vsp_t aux;

      line->vsps[s] = ccl_new (tla_vsp_t);
      tla_vsp_init (*(line->vsps[s]), m);
      tla_vsp_init (aux, m);
      tla_vsp_set (*(line->vsps[s]), *(line->vsps[line->s]));

      if ((ccl_debug_is_on))
	ccl_debug ("recompute it from previous one\n");

      for (i = 0; i < m; i++)
	{
	  int k;

	  for (k = 0; k < m; k++)
	    {
	      if (ccl_bittable_has (line->lambdas, osindex + k) &&
		  !ccl_bittable_has (line->lambdas, sindex + (k + i) % m))
		break;
	    }

	  if (k == m)
	    break;

	  tla_vsp_inv_gamma0 (aux, *(line->vsps[s]));
	  tla_vsp_set (*(line->vsps[s]), aux);
	}

      ccl_assert (i < m);

      tla_vsp_clear (aux);
    }
  tla_vsp_set (res, *(line->vsps[s]));

  if (0)
    {
      int q = s;
      tla_vsp_t aux;
      ccl_bittable *l;
      tla_vsp_init (aux, tla_vsp_get_size (res));
      l = btf_ea_to_vsp_v2 (aux, ea, &q);
      ccl_delete (l);
      ccl_assert (tla_vsp_equals (aux, res));
    }
}
#endif
			/* --------------- */

int
btf_ea_to_vsp (tla_vsp_t res, sataf_ea *ea, int s)
{
  static ccl_hash *VSP_CACHE = NULL;
  int m = tla_vsp_get_size (res);
  vsp_cache_line *cache_line = NULL;

  ccl_bittable *(*ea2vsp) (tla_vsp_t res, sataf_ea *ea, int *s) =
    btf_ea_to_vsp_v5;

  ccl_pre (s_is_non_trivial (ea));

  if (ccl_debug_is_on)
    {
      ccl_debug ("compute VSP for EA=(%p,%d)\n", ea, s);
      ccl_debug_push_tab_level ();
    }

  if (VSP_CACHE == NULL)
    VSP_CACHE = ccl_hash_create (NULL, NULL, NULL, NULL);

  if (ccl_hash_find (VSP_CACHE, ea))
    {
      cache_line = ccl_hash_get (VSP_CACHE);
      s = cache_line->s;
      tla_vsp_set (res, *cache_line->vsps[cache_line->s]);
      /*
      s_compute_vsp_from_cache_line (res, ea, s, cache_line);
      */
      if (ccl_debug_is_on)
	ccl_debug_pop_tab_level ();
      return s;
    }

  cache_line = (vsp_cache_line *)
    ccl_calloc (sizeof (vsp_cache_line) +
		sizeof (tla_vsp_t *) * (ea->nb_local_states - 1), 1);
  ccl_hash_insert (VSP_CACHE, cache_line);


  cache_line->lambdas = ea2vsp (res, ea, &s);
  cache_line->s = s;
  cache_line->vsps[s] = ccl_new (tla_vsp_t);

  tla_vsp_init (*(cache_line->vsps[s]), m);
  tla_vsp_set (*(cache_line->vsps[s]), res);

  if ((ccl_debug_is_on))
    {
      ccl_debug_pop_tab_level ();
      ccl_debug ("done\n");
    }

  return s;
}
