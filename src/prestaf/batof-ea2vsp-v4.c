/*
 * batof-ea2vsp-v4.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-bittable.h>
#include "prestaf-batof-p.h"

			/* --------------- */

static int
s_look_for_terminal_loop_start_point (sataf_ea *a, int m, int from);

static void
s_compute_shortest_paths (int m, sataf_ea *a, int q1,
			  ccl_bittable *lambdas,
			  tla_vec_t ** invgamma_minpaths);

static int
s_post (tla_vec_t res, int b, int i, tla_vec_t x);

			/* --------------- */

static void
s_display_lambdas (ccl_log_type log, int m, int nb_states,
		   ccl_bittable *lambdas);

static void
s_display_q0_loop (ccl_log_type log, sataf_ea *a, int q0);

static void
s_display_invgammas (ccl_log_type log, int m, int nb_states,
		     tla_vec_t ** invgammas);


			/* --------------- */

ccl_bittable *
btf_ea_to_vsp_v4 (tla_vsp_t res, sataf_ea *ea, int *pq0)
{
  int p;
  tla_vec_t v1, v2;
  int m = tla_vsp_get_size (res);
  ccl_bittable *lambdas =
    ccl_bittable_create (m * (ea->nb_local_states + ea->nb_exit_states));
  tla_vec_t **invgamma_minpaths =
    ccl_new_array (tla_vec_t *, ea->nb_local_states * m);
  int q0 = s_look_for_terminal_loop_start_point (ea, m, *pq0);

  s_compute_shortest_paths (m, ea, q0, lambdas, invgamma_minpaths);

  if (ccl_debug_is_on)
    {
      s_display_q0_loop (CCL_LOG_DEBUG, ea, q0);
      if (0)
	s_display_lambdas (CCL_LOG_DEBUG, m, ea->nb_local_states, lambdas);
      if (0)
	s_display_invgammas (CCL_LOG_DEBUG, m, ea->nb_local_states,
			     invgamma_minpaths);
    }

  tla_vec_init (v1, m);
  tla_vec_init (v2, m);

  if (ccl_debug_is_on)
    ccl_debug ("adding generators...");

  for (p = ccl_bittable_get_first (lambdas); p >= 0;
       p = ccl_bittable_get_next (lambdas, p))
    {
      int q = p / m;
      int lambda_q = p % m;
      unsigned int b;

      for (b = 0; b < 2; b++)
	{
	  int qp = sataf_ea_get_successor (ea, (unsigned) q, b);
	  int lambda_qp = (lambda_q + 1) % m;

	  if (sataf_ea_is_exit_state (qp))
	    continue;

	  qp = sataf_ea_decode_succ_state (qp);
	  s_post (v1, b, lambda_q, *(invgamma_minpaths[q * m + lambda_q]));
	  tla_vec_sub (v2, v1, *(invgamma_minpaths[qp * m + lambda_qp]));
	  tla_vsp_add_generator (res, res, v2);
	}
    }

  if (ccl_debug_is_on)
    ccl_debug ("done\n");

  tla_vec_clear (v1);
  tla_vec_clear (v2);
  for (p = ea->nb_local_states * m - 1; p >= 0; p--)
    ccl_zdelete (tla_vec_clear, *(invgamma_minpaths[p]));
  ccl_delete (invgamma_minpaths);

#if 0
  if (1)
    {
      int q = *pq0;
      tla_vsp_t aux;
      ccl_bittable *l;

      tla_vsp_init (aux, tla_vsp_get_size (res));
      l = btf_ea_to_vsp_v3 (aux, ea, &q);

      if (!tla_vsp_equals (aux, res))
	{
	  ccl_debug ("NOK=\n");
	  tla_vsp_log_reduced (CCL_LOG_DEBUG, res);
	  ccl_debug ("\nOK=\n");
	  tla_vsp_log_reduced (CCL_LOG_DEBUG, aux);
	  ccl_debug ("\n");
	  if (0)
	    {
	      int x;
	      tla_vsp_t v;

	      tla_vsp_init (v, m);

	      for (x = 0; x < m && !tla_vsp_equals (aux, res); x++)
		{
		  tla_vsp_inv_gamma0_v1 (v, res);
		  tla_vsp_set (res, v);
		  ccl_debug ("x=%d\n", x);
		  tla_vsp_log_reduced (CCL_LOG_DEBUG, res);
		  ccl_debug ("\n");
		}
	      tla_vsp_clear (v);
	      abort ();
	    }
	  ccl_assert (tla_vsp_equals (aux, res));
	}

      ccl_bittable_delete (l);
      tla_vsp_clear (aux);
    }
#endif

  *pq0 = q0;

  return lambdas;
}

			/* --------------- */

static void
s_compute_shortest_paths (int m, sataf_ea *ea, int q0,
			  ccl_bittable *lambdas,
			  tla_vec_t ** invgamma_minpaths)
{
  unsigned int b;
  ccl_list *Q = ccl_list_create ();
  tla_vec_t zero;
  tla_vec_t **invgamma;

  tla_vec_init (zero, m);


  /*
     ccl_bittable_set(lambdas,q0*m);
     tla_vec_set(ksis[q0*m],zero);
   */

  for (b = 0; b < 2; b++)
    {
      int index;
      int qp = sataf_ea_get_successor (ea, (unsigned) q0, b);

      if (sataf_ea_is_exit_state (qp))
	continue;

      qp = sataf_ea_decode_succ_state (qp);
      index = qp * m + 1 % m;
      invgamma = invgamma_minpaths + index;
      if (*invgamma != NULL)
	continue;

      ccl_assert (!ccl_bittable_has (lambdas, index));

      ccl_bittable_set (lambdas, index);
      ccl_list_add (Q, (void *) (intptr_t) index);

      *invgamma = ccl_new (tla_vec_t);
      tla_vec_init (**invgamma, m);
      s_post (**invgamma, b, 0, zero);
    }
  tla_vec_clear (zero);

  while (!ccl_list_is_empty (Q))
    {
      unsigned int b;
      int q = (intptr_t) ccl_list_take_first (Q);
      int lambda_q = q % m;
      q /= m;

      for (b = 0; b < 2; b++)
	{
	  int qp = sataf_ea_get_successor (ea, (unsigned) q, b);


	  if (!sataf_ea_is_exit_state (qp))
	    {
	      int lambda_qp = (lambda_q + 1) % m;

	      qp = sataf_ea_decode_succ_state (qp);
	      invgamma = invgamma_minpaths + (m * qp + lambda_qp);

	      if (*invgamma != NULL)
		continue;

	      ccl_assert (!ccl_bittable_has (lambdas, m * qp + lambda_qp));
	      ccl_assert (!ccl_list_has (Q, ((void *) (intptr_t) 
					     (qp * m + lambda_qp))));

	      *invgamma = ccl_new (tla_vec_t);
	      tla_vec_init (**invgamma, m);
	      s_post (**invgamma, b, lambda_q,
		      *(invgamma_minpaths[q * m + lambda_q]));
	      ccl_bittable_set (lambdas, m * qp + lambda_qp);
	      ccl_list_add (Q, (void *) (intptr_t) (m * qp + lambda_qp));
	    }
	}
    }
  ccl_list_delete (Q);
}

			/* --------------- */

static int
s_look_for_terminal_loop_start_point (sataf_ea *ea, int m, int from)
{
  int result = -1;
  ccl_bittable *on_path = ccl_bittable_create (ea->nb_local_states);
  int dist_to_from = 0;
  unsigned int q = from;

  if (!ea->is_final[from])
    {
      ccl_list *Q = ccl_list_create ();
      int *depths = ccl_new_array (int, ea->nb_local_states);

      for (q = 0; q < ea->nb_local_states; q++)
	depths[q] = -1;
      depths[from] = 0;
      dist_to_from = -1;

      ccl_list_add (Q, (void *) (intptr_t) from);
      while (!ccl_list_is_empty (Q) && dist_to_from < 0)
	{
	  unsigned int b;

	  q = (intptr_t) ccl_list_take_first (Q);

	  for (b = 0; b < 2; b++)
	    {
	      int qp = sataf_ea_get_successor (ea, q, b);

	      if (sataf_ea_is_exit_state (qp))
		continue;

	      qp = sataf_ea_decode_succ_state (qp);
	      if (depths[qp] < 0)
		{
		  depths[qp] = (depths[q] + 1) % m;
		  if (ea->is_final[qp])
		    {
		      q = qp;
		      dist_to_from = depths[qp];
		      break;
		    }
		  else
		    ccl_list_add (Q, (void *) (intptr_t) qp);
		}
	    }
	}
      ccl_list_delete (Q);
      ccl_assert (ea->is_final[q]);
    }

  ccl_bittable_set (on_path, q);

  while (result < 0)
    {
      unsigned int qp = sataf_ea_get_successor (ea, q, 0);

      ccl_assert (sataf_ea_is_local_state (qp));
      q = sataf_ea_decode_succ_state (qp);
      dist_to_from = (dist_to_from + 1) % m;
      if (ccl_bittable_has (on_path, q))
	result = q;
      else
	ccl_bittable_set (on_path, q);
    }

  ccl_bittable_delete (on_path);

  if (dist_to_from != 0)
    {
      while (dist_to_from != 0)
	{
	  unsigned int qp = sataf_ea_get_successor (ea, q, 0);

	  ccl_assert (sataf_ea_is_local_state (qp));
	  q = sataf_ea_decode_succ_state (qp);
	  dist_to_from = (dist_to_from + 1) % m;
	}
      result = q;
    }

  return result;
}

			/* --------------- */

static int
s_post (tla_vec_t res, int b, int i, tla_vec_t x)
{
  int m = tla_vec_get_size (x);

  if (i < m - 1)
    {
      if (b == 0)
	tla_vec_set (res, x);
      else
	tla_vec_sub_ith_den (res, x, i);
    }
  else
    {
      if (b == 0)
	tla_vec_div_2 (res, x);
      else
	tla_vec_sub_last_den_div_2 (res, x);
    }

  return (i + 1) % m;
}

			/* --------------- */

static void
s_display_lambdas (ccl_log_type log, int m, int nb_states,
		   ccl_bittable *lambdas)
{
  int q;

  ccl_log (log, "LAMBDAS=\n");
  for (q = 0; q < nb_states; q++)
    {
      int l;

      ccl_log (log, "%d : ", q);

      for (l = 0; l < m; l++)
	{
	  if (ccl_bittable_has (lambdas, q * m + l))
	    ccl_log (log, "%d ", l);
	}
      ccl_log (log, "\n");
    }
}

			/* --------------- */

static void
s_display_q0_loop (ccl_log_type log, sataf_ea *ea, int q0)
{
  unsigned int q = q0;

  ccl_log (log, "LOOP ON q0=%d :\n", q0);

  do
    {
      ccl_log (log, "%d -> ", q);
      q = sataf_ea_get_successor (ea, q, 0);
      q = sataf_ea_decode_succ_state (q);
    }
  while ((int) q != q0);
  ccl_log (log, "%d\n", q);
}


			/* --------------- */

static void
s_display_invgammas (ccl_log_type log, int m, int nb_states,
		     tla_vec_t ** invgammas)
{
  int q;

  ccl_log (log, "INVGAMMA=\n");
  for (q = 0; q < nb_states; q++)
    {
      int l;

      for (l = 0; l < m; l++)
	{
	  if (invgammas[q * m + l] != NULL)
	    {
	      ccl_log (log, "(q=%d,i=%d)=", q, l);
	      tla_display_vec_reduced (log, *(invgammas[q * m + l]));
	      ccl_log (log, "\n");
	    }
	}
    }
}
