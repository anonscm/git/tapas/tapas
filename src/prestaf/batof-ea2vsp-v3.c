/*
 * batof-ea2vsp-v3.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-bittable.h>
#include "prestaf-batof-p.h"

static void
s_gamma0_i (tla_vec_t res, tla_vec_t x, int i);

static void
s_compute_ksi (tla_vec_t ksi, int q0, int q1, int bq0, int m, int *backward,
	       tla_vec_t aux);

static void
s_compute_inv_gamma (tla_vec_t res, int q, int q1, int l, tla_vec_t ksi,
		     int m, int *backward, tla_vec_t aux, int bq0,
		     tla_vec_t **ksis);

static int *
s_compute_shortest_paths (int m, sataf_ea *ea, int q1, 
			  ccl_bittable *lambdas);

static void
s_display_paths (ccl_log_type log, int m, sataf_ea *ea, int q1,
		 int *backward);

			/* --------------- */

ccl_bittable *
btf_ea_to_vsp_v3 (tla_vsp_t res, sataf_ea *ea, int *pq0)
{
  int q0 = *pq0;
  int m = tla_vsp_get_size (res);
  int bq0 = 0;
  int q1 = sataf_ea_get_successor (ea, (unsigned) q0, 0);
  int *backward;
  ccl_bittable *lambdas =
    ccl_bittable_create (m * (ea->nb_local_states + ea->nb_exit_states));
  tla_vec_t ksi, aux;
  tla_vec_t **ksis;

  if (sataf_ea_is_exit_state (q1))
    {
      bq0 = 1;
      q1 = sataf_ea_get_successor (ea, (unsigned) q0, 1);
      ccl_assert (!sataf_ea_is_exit_state (q1));
    }

  q1 = sataf_ea_decode_succ_state (q1);

  backward = s_compute_shortest_paths (m, ea, q1, lambdas);

  if (0)
    {
      int q;

      ccl_debug ("LAMBDAS(q0=%d, q1=%d):\n", q0, q1);
      for (q = 0; q < (int) ea->nb_local_states; q++)
	{
	  int i;
	  ccl_debug ("%d : ", q);

	  for (i = 0; i < m; i++)
	    {
	      if (backward[q * m + i] < 0)
		{
		  ccl_assert (!ccl_bittable_has (lambdas, q * m + i));
		  continue;
		}
	      else
		{
		  ccl_assert (ccl_bittable_has (lambdas, q * m + i));
		  ccl_debug ("%d ", i);
		}
	    }
	  ccl_debug ("\n");
	}

      if (0)
	s_display_paths (CCL_LOG_DEBUG, m, ea, q1, backward);
    }

  tla_vec_init (ksi, m);
  tla_vec_init (aux, m);

  s_compute_ksi (ksi, q0, q1, bq0, m, backward, aux);

  if (ccl_debug_is_on)
    {
      ccl_debug ("KSI=");
      tla_display_vec (CCL_LOG_DEBUG, ksi);
      ccl_debug ("\n");
    }

  /* compute VG */
  {
    int p;
    tla_vec_t ksi_q, ksi_qp, ksi_q_aux;

    tla_vec_init (ksi_q, m);
    tla_vec_init (ksi_q_aux, m);
    tla_vec_init (ksi_qp, m);
    tla_vec_init (aux, m);

    ksis = ccl_new_array (tla_vec_t *, ea->nb_local_states * m);

    for (p = ccl_bittable_get_first (lambdas); p >= 0;
	 p = ccl_bittable_get_next (lambdas, p))
      {
	int q = p / m;
	int i = p % m;
	unsigned int b;

	ccl_assert (backward[m * q + i] >= 0);

	s_compute_inv_gamma (ksi_q_aux, q, q1, i, ksi, m, backward, aux, bq0,
			     ksis);

	for (b = 0; b < 2; b++)
	  {
	    int qp = sataf_ea_get_successor (ea, (unsigned) q, b);

	    if (sataf_ea_is_exit_state (qp))
	      continue;

	    qp = sataf_ea_decode_succ_state (qp);

	    tla_vec_inv_gamma (aux, ksi_q_aux, b);
	    tla_vec_set (ksi_q, aux);

	    tla_vec_set_zero (ksi_qp);
	    s_compute_inv_gamma (ksi_qp, qp, q1, (i + 1) % m, ksi, m,
				 backward, aux, bq0, ksis);

	    tla_vec_sub (aux, ksi_q, ksi_qp);

	    s_gamma0_i (ksi_q, aux, i + 2);

	    tla_vsp_add_generator (res, res, ksi_q);
	  }
      }
    for (p = ea->nb_local_states * m - 1; p >= 0; p--)
      if (ksis[p] != NULL)
	tla_vec_clear (*(ksis[p]));
    ccl_delete (ksis);
    tla_vec_clear (ksi_q);
    tla_vec_clear (ksi_q_aux);
    tla_vec_clear (ksi_qp);
  }

  tla_vec_clear (aux);
  tla_vec_clear (ksi);
  ccl_delete (backward);


  {
    ccl_bittable *todo =
      ccl_bittable_create (m * (ea->nb_local_states + ea->nb_exit_states));
    ccl_bittable_clear (lambdas);
    ccl_bittable_set (lambdas, m * q0);
    ccl_bittable_set (todo, q0);
    btf_compute_lambdas (m, ea, todo, lambdas);
    ccl_bittable_delete (todo);

    if (0)
      {
	tla_vsp_t aux;
	ccl_bittable *l;

	tla_vsp_init (aux, tla_vsp_get_size (res));
	l = btf_ea_to_vsp_v2 (aux, ea, &q0);


	ccl_assert (ccl_bittable_equals (l, lambdas));

	if (!tla_vsp_are_equal (aux, res))
	  {
	    ccl_debug ("NOK=\n");
	    tla_display_vsp_reduced (CCL_LOG_DEBUG, res);
	    ccl_debug ("\nOK=\n");
	    tla_display_vsp_reduced (CCL_LOG_DEBUG, aux);
	    ccl_debug ("\n");
	    {
	      int x;
	      tla_vsp_t v;

	      tla_vsp_init (v, m);

	      for (x = 0; x < m && !tla_vsp_are_equal (aux, res); x++)
		{
		  tla_vsp_inv_gamma0 (v, res);
		  tla_vsp_set (res, v);
		  ccl_debug ("x=%d\n", x);
		  tla_display_vsp_reduced (CCL_LOG_DEBUG, res);
		  ccl_debug ("\n");
		}
	      tla_vsp_clear (v);
	      abort ();
	    }
	    ccl_assert (tla_vsp_are_equal (aux, res));
	  }

	ccl_delete (l);
      }

    return lambdas;
  }
}

			/* --------------- */

/* compute ksi(q0->q1->...->q0) */
static void
s_compute_ksi (tla_vec_t ksi, int q0, int q1, int bq0, int m, int *backward,
	       tla_vec_t aux)
{
  int k = 0;
  int len = 0;


  while (len < m && backward[m * q0 + len] < 0)
    len++;
  ccl_assert (len != m);

  do
    {
      int l = len;
      int q = q0;

      while (q != q1)
	{
	  int qp = backward[m * q + l];
	  int b = qp & 0x1;

	  ccl_assert (qp >= 0);

	  q = qp >> 1;
	  tla_vec_gamma (aux, ksi, b);
	  tla_vec_set (ksi, aux);

	  if (l == 0)
	    l = m - 1;
	  else
	    l--;
	  k++;
	}
      k++;
      tla_vec_gamma (aux, ksi, bq0);
      tla_vec_set (ksi, aux);
    }
  while (k % m != 0);

  {
    mpz_t x, one, exp2;

    mpz_init_set_ui (one, 1);
    mpz_init (exp2);
    mpz_init (x);
    mpz_mul_2exp (exp2, one, k / m);
    mpz_sub (x, one, exp2);
    tla_vec_z_div (aux, ksi, x);
    mpz_clear (one);
    mpz_clear (exp2);
    mpz_clear (x);
    tla_vec_set (ksi, aux);
  }
}

			/* --------------- */

static void
s_display_paths (ccl_log_type log, int m, sataf_ea *ea, int q1,
		 int *backward)
{
  int s;

  ccl_log (log, "paths back to %d (max len<%d)\n", q1, m);

  for (s = 0; s < (int) ea->nb_local_states; s++)
    {
      int i;
      int q = s;

      for (i = 0; i < m; i++)
	{
	  if (backward[m * q + i] >= 0)
	    break;
	}
      ccl_log (log, "|%d=>%d| = %d : ", q, q1, i);

      do
	{
	  ccl_log (log, "%d -%d-> ", q, backward[m * q + i] & 0x1);
	  q = backward[m * q + i] >> 1;
	  if (i == 0)
	    i = m - 1;
	  else
	    i--;
	}
      while (q != q1);
      ccl_log (log, "%d\n", q1);
    }
}

			/* --------------- */

static int *
s_compute_shortest_paths (int m, sataf_ea *ea, int q1,
			  ccl_bittable *lambdas)
{
  unsigned int b;
  int q;
  int *backward = ccl_new_array (int, m * ea->nb_local_states);
  ccl_list *Q = ccl_list_create ();

  for (q = ea->nb_local_states * m - 1; q >= 0; q--)
    backward[q] = -1;

  for (b = 0; b < 2; b++)
    {
      int qp = sataf_ea_get_successor (ea, (unsigned) q1, b);

      if (sataf_ea_is_exit_state (qp))
	continue;

      qp = sataf_ea_decode_succ_state (qp);

      backward[m * qp + 1 % m] = (q1 << 1) | b;
      ccl_bittable_set (lambdas, m * qp + 1 % m);
      if (! ccl_list_has (Q, (void *) (intptr_t) qp))
	ccl_list_add (Q, (void *) (intptr_t) qp);
    }

  while (!ccl_list_is_empty (Q))
    {
      unsigned int b;
      int q = (intptr_t) ccl_list_take_first (Q);

      for (b = 0; b < 2; b++)
	{
	  int i;
	  int qp = sataf_ea_get_successor (ea, (unsigned) q, b);

	  if (sataf_ea_is_exit_state (qp))
	    continue;

	  qp = sataf_ea_decode_succ_state (qp);

	  for (i = 0; i < m; i++)
	    {
	      if (backward[m * q + i] < 0
		  || backward[m * qp + (i + 1) % m] >= 0)
		continue;

	      ccl_bittable_set (lambdas, m * qp + (i + 1) % m);
	      backward[m * qp + (i + 1) % m] = (q << 1) | b;
	      if (!ccl_list_has (Q, (void *) (intptr_t) qp))
		ccl_list_add (Q, (void *) (intptr_t) qp);
	    }
	}
    }

  ccl_list_delete (Q);


  if (0)
    s_display_paths (CCL_LOG_DEBUG, m, ea, q1, backward);

  return backward;
}

			/* --------------- */

#if 0
static void
s_compute_inv_gamma (tla_vec_t res, int q, int q1, int l, tla_vec_t ksi,
		     int m, int *backward, tla_vec_t aux, int bq0)
{
  while (q != q1)
    {
      int b = backward[m * q + l] & 0x1;
      int qp = backward[m * q + l];

      ccl_assert (backward[m * q + l] >= 0);

      q = qp >> 1;
      tla_vec_inv_gamma (aux, res, b);
      tla_vec_set (res, aux);

      if (l == 0)
	l = m - 1;
      else
	l--;

      ccl_assert (q != q1 || l == 0);
    }

  tla_vec_inv_gamma (aux, res, bq0);
  tla_vec_set (res, aux);
}
#else
static void
s_compute_inv_gamma (tla_vec_t res, int q, int q1, int l, tla_vec_t ksi,
		     int m, int *backward, tla_vec_t aux, int bq0,
		     tla_vec_t ** ksis)
{
  tla_vec_t **cache = ksis + (q * m + l);

  if (*cache != NULL)
    {
      tla_vec_set (res, **cache);
    }
  else
    {
      if (q == q1 && l == 0)
	{
	  tla_vec_inv_gamma (res, ksi, bq0);
	}
      else
	{
	  int b = backward[m * q + l] & 0x1;
	  int qp = backward[m * q + l];

	  ccl_assert (backward[m * q + l] >= 0);

	  q = qp >> 1;

	  if (l == 0)
	    l = m - 1;
	  else
	    l--;

	  s_compute_inv_gamma (res, q, q1, l, ksi, m, backward, aux, bq0,
			       ksis);

	  tla_vec_inv_gamma (aux, res, b);
	  tla_vec_set (res, aux);
	}

      *cache = ccl_new (tla_vec_t);
      tla_vec_init (**cache, m);
      tla_vec_set (**cache, res);
    }
}
#endif
			/* --------------- */

static void
s_gamma0_i (tla_vec_t res, tla_vec_t x, int n)
{
  int i;
  tla_vec_t aux;

  tla_vec_init (aux, tla_vec_get_size (res));
  tla_vec_set (res, x);
  for (i = 1; i <= n; i++)
    {
      tla_vec_gamma (aux, res, 0);
      tla_vec_set (res, aux);
    }
  tla_vec_clear (aux);
}
