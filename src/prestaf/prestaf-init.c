/*
 * prestaf-init.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include "prestaf-init.h"
#if HAVE_PPL
# include <ppl_c.h>

static int PPL_IS_INIT = 0;

static void
s_error_handler (enum ppl_enum_error_code code, const char *description)
{
  ccl_error ("PPL Error (%d): %s\n", description);
}
#endif



			/* --------------- */

 	
void
prestaf_init (void)
{
#if HAVE_PPL
  if (ppl_initialize () != PPL_ERROR_INVALID_ARGUMENT)
    {
      ppl_restore_pre_PPL_rounding ();
      PPL_IS_INIT = 1;
      ppl_set_error_handler (s_error_handler);
    }
#endif
}

			/* --------------- */

void
prestaf_terminate (void)
{
#if HAVE_PPL
  if (PPL_IS_INIT)
    ppl_finalize ();
#endif
}
