/*
 * prestaf-batof-p.h -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __PRESTAF_BATOF_P_H__
# define __PRESTAF_BATOF_P_H__

# include <ccl/ccl-bittable.h>
# include <ccl/ccl-hash.h>
# include "prestaf-batof.h"


#define M(A,m,n,i,j) (ccl_assert(((n)*((i)-1)+((j)-1))<((m)*(n))), \
		      ((A)[(n)*((i)-1)+((j)-1)]))

#define MM(_m,_i,_j) M((_m)->coefs->num, (_m)->m, (_m)->n, _i, _j)

#define MPZ2SI(z) (ccl_assert (mpz_fits_sint_p ((z))), mpz_get_si ((z)))

			/* --------------- */

typedef struct modulo_info_st modulo_info;
struct modulo_info_st
{
  int refcount;
  int m;
  int n;
  int n0;

  /* modula automaton */
  sataf_ea *modea;
  int *states_to_classes;
  int **classes_to_states;
  tla_group_t invV;
};

			/* --------------- */

typedef struct btf_saff_component_st
{
  sataf_sa *C;
  int q;
  tla_vsp_t V;
  int m;
  modulo_info *mi;

  /* patterns */
  int nb_classes;
  int dim;
  int *ci;
  tla_matrix_t X;
  int qpattern;
  int **patterns;
  int nb_patterns;  
  int pattern_width;
} btf_saffcomp;

typedef struct btf_sa_tables_st
{
  sataf_sa *sa;
# define T_LAMBDAS 0
# define T_INIT 1
  /*
# define T_BAD 2
# define T_GOOD 3
  */
  ccl_bittable *tables[4];
  struct btf_sa_tables_st *childs[1];
} btf_sa_tables;

typedef struct btf_loop_element_st
{
  struct btf_loop_element_st *next;
  unsigned int s;
  unsigned int l;
} btf_loop_element;


typedef enum { BF_CST, BF_UNION, BF_INTER, BF_DIFF } boolean_formula_type;
typedef struct boolean_formula_st boolean_formula;
struct boolean_formula_st
{
  boolean_formula_type type;
  int refcount;
  int arity_or_cst;
  boolean_formula *operands[1];
};

			/* --------------- */

extern void
btf_compute_lambdas (int m, sataf_ea *ea, ccl_bittable *todo,
		     ccl_bittable *lambdas);

extern void
btf_compute_msa_lambdas (int m, sataf_msa *msa, ccl_hash *lambdas, 
			 ccl_list *components);

extern int
btf_lambda_inc (int m, ccl_bittable *lambdas, int from, int to);

extern int
btf_apply_on_loop (sataf_ea *ea, int q0,
		   void (*apply) (btf_loop_element * e, void *data),
		   void *clientdata);

extern int
btf_ea_to_vsp (tla_vsp_t res, sataf_ea *ea, int s);

extern ccl_bittable *
btf_ea_to_vsp_v2 (tla_vsp_t res, sataf_ea *ea, int *ps);

extern ccl_bittable *
btf_ea_to_vsp_v3 (tla_vsp_t res, sataf_ea *ea, int *ps);

extern ccl_bittable *
btf_ea_to_vsp_v4 (tla_vsp_t res, sataf_ea *ea, int *ps);

extern ccl_bittable *
btf_ea_to_vsp_v5 (tla_vsp_t res, sataf_ea *ea, int *ps);

extern sataf_ea *
btf_ea_compute_mod_classes (sataf_ea *ea, int **p_state_to_class,
			    int ***p_class_to_state);

extern void
btf_compute_invariant_group (tla_group_t G, tla_vsp_t V, sataf_ea *ea, 
			     int q0, int m, int n0, int n);

extern int
btf_compute_patterns(tla_group_t G, sataf_ea *ea, int q0, int *C,
		     int ***p_patterns, int *p_patwidth, int *p_nb_patterns,
		     tla_matrix_t *pX);

extern void
btf_decompose_int_point (int *p, int intpoint, int *C, int k);

extern ccl_list *
btf_compute_components (int m, sataf_msa *msa, ccl_hash **p_sa_tables);

extern void
btf_msa_to_bounds(int m, sataf_msa *X, ccl_list *Tcomps, ccl_hash *sa_tables, 
		  btf_bound **pbounds, int *pbounds_size);

extern void
btf_saffcomp_delete (btf_saffcomp *c);

extern int
btf_is_untransient (sataf_sa *sa);

			/* --------------- */

extern boolean_formula *
btf_boolean_combination (ccl_bittable **C, int nb_C, ccl_bittable *X);

extern void
btf_boolean_formula_delete (boolean_formula *F);

extern void
btf_boolean_formula_display (ccl_log_type log, boolean_formula *f);

extern ccl_bittable *
btf_boolean_formula_eval (boolean_formula *f, ccl_bittable **C);

extern void
btf_boolean_combination_test (void);

			/* --------------- */

extern int
btf_is_detectable (sataf_msa *X, tla_vec_t a, tla_vsp_t V, sataf_msa *final,
		   tla_vec_t rho_0, tla_vec_t rho_1, int *p_l0, int *p_l1);

extern void
btf_detect_test (int m, sataf_msa *msa);


extern void
btf_compute_n0 (sataf_ea *ea, int *p_n0, int *p_n);

			/* --------------- */

extern void
btf_misc_compute_rho (sataf_msa *from, sataf_msa *to, tla_vec_t rho, int *p_n);

extern void
btf_misc_compute_rho_ea (sataf_ea *ea, int from, int to, tla_vec_t rho, 
			 int *p_n);

extern ccl_hash *
btf_misc_compute_acceptance_condition (int m, sataf_msa *X,
				       int (*is_accepting)(sataf_sa *C,
							   int s,
							   tla_vec_t rho,
							   void *data),
				       void *clientdata,
				       ccl_list **p_final_states);

extern void
btf_misc_visit_all_states (int m, sataf_msa *X,
			   void (*visit)(sataf_sa *C, int s, tla_vec_t rho,
					 void *data),
			   void *clientdata);


/*!
 * Compute \f$\xhi(\sigma_0)\f$ where \f$\sigma_0\f$ is a loop starting in
 * the state \a msa. 
 */
extern void
btf_misc_compute_xhi_sigma0 (int m, sataf_msa *msa, tla_vec_t xhi);

			/* --------------- */

extern sataf_msa **
btf_backward_xor (int m, sataf_ea *ea, int **p_q, int *p_nb_msa);


			/* --------------- */


extern void
btf_modulo_info_del_reference (modulo_info *mi);

			/* --------------- */

extern int 
btf_compute_half_spaces_automata (int q0, sataf_ea *ea, 
				  sataf_ea ***p_hspa, int *p_nb_hspa);

#endif /* ! __PRESTAF_BATOF_P_H__ */
