/*
 * batof-misc.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include "prestaf-batof-p.h"

typedef struct prefix_word_st prefix_word;
struct prefix_word_st {
  int b;
  prefix_word *prefix;
};

			/* --------------- */

static void
s_delete_msa (void *p)
{
  sataf_msa_del_reference (CCL_BITPTR2PTR(sataf_msa *,p));
}

			/* --------------- */

static ccl_hash *
s_compute_shortest_sa_path (sataf_msa *from, sataf_msa *to)
{
  int stop = 0;
  ccl_hash *sa_done = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_hash *result = 
    ccl_hash_create (NULL, NULL, 
		     (ccl_delete_proc *) sataf_msa_del_reference,
		     (ccl_delete_proc *) s_delete_msa);
  ccl_list *Q = ccl_list_create();

  ccl_list_add (Q, from->A);
  ccl_hash_find (sa_done, from->A);
  ccl_hash_insert (sa_done, from->A);

  while (!ccl_list_is_empty (Q) && !stop)
    {
      sataf_sa *sa = (sataf_sa *) ccl_list_take_first (Q);
      
      if (sataf_ea_get_nb_exits (sa->automaton) > 0)
	{
	  int s;
	  int max_s = sataf_ea_get_nb_states (sa->automaton);
	  
	  for (s = 0; s < max_s; s++)
	    {
	      int b;
	      
	      for (b = 0; b < 2; b++)
		{
		  int succ = sataf_ea_get_successor (sa->automaton, s, b);
		  
		  if (sataf_ea_is_local_state (succ))
		    continue;

		  succ = sataf_ea_decode_succ_state (succ);
		  
		  if (! ccl_hash_find (sa_done, sa->bind[succ]->A))
		    {
		      sataf_msa *s_succ = 
			sataf_msa_add_reference (sa->bind[succ]);
		      sataf_msa *pred = sataf_msa_find_or_add (sa, s);
		      ccl_hash_insert (sa_done, sa->bind[succ]->A);
		      ccl_list_add (Q, sa->bind[succ]->A);

		      ccl_assert (! ccl_hash_find (result, s_succ));
		      ccl_hash_find (result, s_succ);
		      
		      if (b) 
			pred = CCL_BITPTR (sataf_msa *, pred);

		      ccl_hash_insert (result, pred);
		    }
		}
	    }
	}
      stop = (sa == to->A);
    }

  ccl_list_delete (Q);
  ccl_hash_delete (sa_done);

  return result;
}

			/* --------------- */

void
btf_misc_compute_rho (sataf_msa *from, sataf_msa *to, tla_vec_t rho, int *p_n)
{
  ccl_hash *preds = s_compute_shortest_sa_path (from, to);

  while (to->A != from->A)
    {
      sataf_msa *succ;
      sataf_msa *p;
      int b;

      ccl_assert (ccl_hash_find (preds, to));
      ccl_hash_find (preds, to);
      p = (sataf_msa *) ccl_hash_get (preds);
      b = CCL_PTRHASBIT (p);
      p = CCL_BITPTR2PTR (sataf_msa *, p);
      succ = sataf_msa_succ (p, b);
	  
      ccl_assert (succ->A == to->A);

      btf_misc_compute_rho_ea (succ->A->automaton, succ->initial, to->initial, 
			       rho, p_n);
      (*p_n)++;
      tla_vec_gamma (rho, rho, b);
      sataf_msa_del_reference (succ);
      to = p;
    }

  btf_misc_compute_rho_ea (from->A->automaton, from->initial, to->initial, rho,
			   p_n);


  ccl_hash_delete (preds);
}

			/* --------------- */

static int *
s_compute_shortest_path (sataf_ea *ea, int from, int to)
{
  int s;
  int stop = 0;
  int nb_states = sataf_ea_get_nb_states (ea);
  int *preds = ccl_new_array (int, nb_states);
  ccl_bittable *visited = ccl_bittable_create (nb_states);
  ccl_list *Q = ccl_list_create ();

  for (s = 0; s < nb_states; s++)
    preds[s] = -1;

  ccl_list_add (Q, (void *) (intptr_t) from);
  ccl_bittable_set (visited, from);

  while (! ccl_list_is_empty (Q) && ! stop)
    {
      int b;

      s = (intptr_t) ccl_list_take_first (Q);

      for (b = 0; b < 2 && ! stop; b++)
	{
	  int t = sataf_ea_get_successor (ea, s, b);

	  if (sataf_ea_is_exit_state (t))
	    continue;
	  t = sataf_ea_decode_succ_state (t);

	  if (! ccl_bittable_has (visited, t))
	    {
	      ccl_bittable_set (visited, t);
	      ccl_list_add (Q, (void *) (intptr_t) t);
	      preds[t] = (s << 1) + b;
	    }

	  stop = (t == to);
	}
    }
  ccl_list_delete (Q);
  ccl_bittable_delete (visited);

  return preds;
}

			/* --------------- */

void
btf_misc_compute_rho_ea (sataf_ea *ea, int from, int to, tla_vec_t rho, 
			 int *p_n)
{
  int *pred;
  int len = 0;
  if (from == to)
    return;

  pred = s_compute_shortest_path (ea, from, to);
  while (to != from)
    {
      int b = pred[to] & 0x1;
      to = pred[to] >> 1;
      tla_vec_gamma (rho, rho, b);
      len++;
    }
  ccl_delete (pred);
  *p_n = len;
}

			/* --------------- */

static int
s_has_exit_successor (sataf_msa *msa, int s, ccl_hash *done)
{
  int b;

  for (b = 0; b < 2; b++)
    {
      int succ = sataf_ea_get_successor (msa->A->automaton, s, b);
      if (sataf_ea_is_exit_state (succ))
	{
	  succ = sataf_ea_decode_succ_state (succ);
	  if (! ccl_hash_find (done, msa->A->bind[succ]->A))
	    return 1;
	}
    } 

  return 0;
}

			/* --------------- */

static int *
s_compute_all_shortest_paths (sataf_ea *ea, int from)
{
  return s_compute_shortest_path (ea, from, -1);
}

			/* --------------- */

struct visit_for_acc_data 
{
  int (*accept) (sataf_sa *sa, int s, tla_vec_t rho, void *data);
  void *accept_data;
  ccl_list *final_states;
  ccl_hash *acond;
};

			/* --------------- */

static void
s_visit_for_acceptance_condition (sataf_sa *C, int s, tla_vec_t rho, void *data)
{
  ccl_bittable *final;
  struct visit_for_acc_data *vdata = data;

  if (! vdata->accept (C, s, rho, vdata->accept_data))
    return;

  if (ccl_hash_find (vdata->acond, C))
    final = ccl_hash_get (vdata->acond);
  else
    {
      int nb_states = sataf_ea_get_nb_states (C->automaton);
      
      final = ccl_bittable_create (nb_states);
      ccl_hash_insert (vdata->acond, final);
      sataf_sa_add_reference (C);
    }

  ccl_bittable_set (final, s);
  if (vdata->final_states != NULL)
    {
      sataf_msa *msa = sataf_msa_find_or_add (C, s);
      ccl_list_add (vdata->final_states, msa);
    }
}

			/* --------------- */

ccl_hash *
btf_misc_compute_acceptance_condition (int m, sataf_msa *X,
				       int (*is_accepting)(sataf_sa *C,
							   int s,
							   tla_vec_t rho,
							   void *data),
				       void *clientdata,
				       ccl_list **p_final_states)
{
  struct visit_for_acc_data vdata;

  vdata.accept = is_accepting;
  vdata.accept_data = clientdata;
  vdata.acond = ccl_hash_create (NULL, NULL, 
				 (ccl_delete_proc *) sataf_sa_del_reference,
				 (ccl_delete_proc *) ccl_bittable_delete); 
  if (p_final_states != NULL)
    vdata.final_states = *p_final_states = ccl_list_create ();
  else
    vdata.final_states = NULL;

  btf_misc_visit_all_states (m, X, s_visit_for_acceptance_condition, &vdata);

  return vdata.acond;
}



			/* --------------- */

typedef struct visit_data_st
{
  int m;
  tla_vec_t rho;
  ccl_hash *done;
  void (*visit)(sataf_sa *C, int s, tla_vec_t rho, void *data);
  void *clientdata;  
} visit_data;

			/* --------------- */

static void
s_visit_rec (visit_data *data, sataf_msa *X, prefix_word *prefix, 
	     int prefix_len)
{
  int s;
  sataf_ea *ea = X->A->automaton;  
  int maxs = sataf_ea_get_nb_states (ea);
  int initial = X->initial;
  int *preds = s_compute_all_shortest_paths (ea, initial);

  for (s = 0; s < maxs; s++)
    {
      prefix_word *p;
      int local_prefix_len = prefix_len;
      prefix_word *local_prefix = NULL;
      prefix_word **p_local_prefix = &local_prefix;
      int has_exit_successor = s_has_exit_successor (X, s, data->done);
      int q = s;

      tla_vec_set_zero (data->rho);

      while (q != initial)
	{
	  int b = preds[q] & 0x1;
	  q = preds[q] >> 1;
	  local_prefix_len++;
	  if (has_exit_successor)
	    {
	      *p_local_prefix = ccl_new (prefix_word);
	      (*p_local_prefix)->b = b;
	      (*p_local_prefix)->prefix = NULL;
	      p_local_prefix = &((*p_local_prefix)->prefix);
	    }
	  tla_vec_gamma (data->rho, data->rho, b);
	}

      if (has_exit_successor)
	*p_local_prefix = prefix;

      if (local_prefix_len % data->m == 0)
	{
	  for (p = prefix; p; p = p->prefix)
	    tla_vec_gamma (data->rho, data->rho, p->b);

	  data->visit (X->A, s, data->rho, data->clientdata);
	}

      if (has_exit_successor)
	{
	  int b;
	  prefix_word *aux = ccl_new (prefix_word);
	  int len = local_prefix_len + 1;

	  aux->prefix = local_prefix;
	  for (b = 0; b < 2; b++)
	    {
	      int esucc = sataf_ea_get_successor (ea, s, b);	      
	      int succ = sataf_ea_decode_succ_state (esucc);
	  
	      if (sataf_ea_is_local_state (esucc))
		continue;

	      if (ccl_hash_find (data->done, X->A->bind[succ]->A))
		continue;

	      aux->b = b;	      
	      s_visit_rec (data, X->A->bind[succ], aux, len);
	    }
	  ccl_delete (aux);

	  while (local_prefix != prefix)
	    {
	      aux = local_prefix->prefix;
	      ccl_delete (local_prefix);
	      local_prefix = aux;
	    }
	}
    }
  ccl_free (preds);

  ccl_assert (! ccl_hash_find (data->done, X->A));
  ccl_hash_find (data->done, X->A);
  ccl_hash_insert (data->done, X->A);
}

			/* --------------- */

void
btf_misc_visit_all_states (int m, sataf_msa *X,
			   void (*visit)(sataf_sa *C, int s, tla_vec_t rho,
					void *data),
			   void *clientdata)
{
  visit_data data;

  data.m = m;
  data.done = ccl_hash_create (NULL, NULL, NULL, NULL);
  tla_vec_init (data.rho, m);
  data.visit = visit;
  data.clientdata = clientdata;

  s_visit_rec (&data, X, NULL, 0);

  tla_vec_clear (data.rho);
  ccl_hash_delete (data.done);
}


			/* --------------- */

void
btf_misc_compute_xhi_sigma0 (int m, sataf_msa *msa, tla_vec_t xhi)
{
  int stop;
  int len;
  int s;
  mpz_t d;
  int initial =  msa->initial;
  int S0 = m * initial;
  sataf_ea *ea = msa->A->automaton;
  int nb_states = sataf_ea_get_nb_states (ea);
  ccl_bittable *visited = ccl_bittable_create (m * nb_states);
  ccl_list *Q = ccl_list_create ();
  int *pred = ccl_new_array (int, m * nb_states);
  tla_vec_t tmp;

  tla_vec_init (tmp, m);

  for (s = 0; s < m * nb_states; s++)
    pred[s] = -1;

  ccl_bittable_set (visited, S0);
  ccl_list_add (Q, (void *) (intptr_t) S0);
  stop = 0;

  while (!ccl_list_is_empty (Q) && ! stop)
    {
      int b, q, i;

      s = (intptr_t) ccl_list_take_first (Q);
      q = s / m;
      i = s % m;

      for (b = 0; b < 2 && !stop; b++)
	{
	  int t;
	  int qp = sataf_ea_get_successor (ea, q, b);

	  if (sataf_ea_is_exit_state (qp))
	    continue;
	  qp = sataf_ea_decode_succ_state (qp);
	  t = m * qp + (i+1) % m;
	  
	  if (pred[t] < 0)
	    pred[t] = (s << 1)+b;

	  stop = (t == S0);
	  if (! ccl_bittable_has (visited, t))
	    {
	      ccl_bittable_set (visited, t);
	      ccl_list_add (Q, (void *) (intptr_t) t);
	    }
	}
    }

  s = S0;
  tla_vec_set_zero (xhi);
  len = 0;
  ccl_assert (pred [s] >= 0);

  do 
    {   
      int b = pred[s] & 0x1;
      ccl_assert (pred [s] >= 0);
      s = pred[s] >> 1;
      len++;
      tla_vec_gamma (tmp, xhi, b);
      tla_vec_set (xhi, tmp);
    }
  while (s != S0);
  ccl_assert (len % m == 0);
  len /= m;

  mpz_init_set_si (d, -1);
  mpz_mul_2exp (d, d, len);
  mpz_add_ui(d, d, 1);
  tla_vec_z_div (tmp, xhi, d);
  tla_vec_set (xhi, tmp);
  tla_vec_canonicalize(xhi);
  mpz_clear (d);

  tla_vec_clear (tmp);
  ccl_bittable_delete (visited);
  ccl_list_delete (Q);
  ccl_delete (pred);
}
