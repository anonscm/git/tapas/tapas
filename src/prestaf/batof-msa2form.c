/*
 * batof-msa2form.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-list.h>
#include <sataf/sataf.h>
#include "prestaf-automata.h"
#include "batof-formulas.h"
#include "prestaf-batof-p.h"

typedef struct P_formula_record_st P_formula_record;

struct P_formula_record_st 
{
  struct P_formula_record_st *next;
  ccl_list *final_states;
  int in_comb;
  armoise_tree *def;
  armoise_tree *deps[4];
};

typedef struct acond_st acond;
struct acond_st 
{
  ccl_list *final_states;
  ccl_hash *cond;
};

typedef struct P_data_st P_data;
struct P_data_st 
{
  ccl_list *pattern_vectors;
  acond p_cond;
};

			/* --------------- */

#define ZN ((armoise_tree *) NULL)

#define NN(_type,_vtype,_child,_next)					\
  (ccl_parse_tree_create (ARMOISE_TREE_ ## _type, #_type, _vtype,	\
			  ((_child) == ZN				\
			   ? __LINE__					\
			   : (_child)->line),				\
			  ((_child) == ZN				\
			   ? __FILE__					\
			   : (_child)->filename),			\
			  _child, _next, NULL))

#define NE(_type,_child,_next) NN (_type, CCL_PARSE_TREE_EMPTY, _child, _next)
#define U_NE(_type,_child) NE (_type, _child, ZN)
#define B_NE(_result, _type, _child1, _child2)	\
  do { \
    armoise_tree *c1 = _child1; \
    armoise_tree *c2 = _child2; \
    (_result) = U_NE (_type, c1); \
    c1->next = c2; \
  } while (0)

#define NID()  NN (IDENTIFIER, CCL_PARSE_TREE_IDENT, ZN, ZN)
#define NINT() NN (INTEGER, CCL_PARSE_TREE_INT, ZN, ZN)
#define NSTR() NN (STRING, CCL_PARSE_TREE_STRING, ZN, ZN)
#define REVERSE(_t) ccl_parse_tree_reverse_siblings (_t)

			/* --------------- */

static sataf_msa *
s_unfold_automaton (int m, sataf_msa *msa);

static armoise_tree *
s_compute_formula_2 (struct msa2form_compilation_data *cd,
		     sataf_msa *msa, int prefix);

static int
s_is_zero_vec (sataf_sa *sa);

static armoise_tree *
s_compute_formula_without_prefix (struct msa2form_compilation_data *cd,
				  sataf_msa *msa);

static sataf_msa *
s_reduce_msa (struct msa2form_compilation_data *cd, sataf_msa *msa, 
	      armoise_tree **pF);

static int
s_compute_formulas_for_comp (struct msa2form_compilation_data *cd,
			     sataf_msa *msa, btf_saffcomp *comp, 
			     ccl_list *formulas, int offset, int nb_bounds, 
			     btf_bound *comp_bound, tla_vec_t rho_w, 
			     tla_vec_t xhi, ccl_hash *acond);

static int
s_compute_patterns (struct msa2form_compilation_data *cd,
		    btf_saffcomp *comp, int offset, tla_group_t invV);

static int
s_compute_rho_w (int m, sataf_msa *msa, tla_vec_t rho, ccl_list *Tcomps,
		 btf_saffcomp **p_comp);

static ccl_list *
s_compute_pattern_vectors (btf_saffcomp *comp, int P);

static void
s_delete_vec (tla_vec_t *v);

static sataf_msa *
s_compute_bound_automaton (tla_vec_t alpha, tla_vec_t xhi, int op);

static int
s_compute_boolean_combination (ccl_list *deflist,
			       P_formula_record *formulas, sataf_msa *msa, 
			       btf_saffcomp *comp, ccl_list *result, 
			       ccl_hash *acond);

static void
s_cleanup_formula_records (P_formula_record *frec);

static sataf_msa *
s_compute_with_new_cond (sataf_msa *msa, ccl_hash *newcond);

static P_data *
s_compute_acceptance_conditions (int m, sataf_msa *msa, btf_saffcomp *comp, 
				 tla_group_t invV, tla_vec_t rho_w,
				 acond *unioncond);

static void
s_init_acond (acond *acc);

static void
s_clear_acond (acond *acc);

static void
s_add_accepting_state (acond *acc, sataf_sa *sa, int s);

static P_formula_record *
s_new_formula_record (armoise_tree *def, ccl_list *final_states, 
		      armoise_tree *dep0, armoise_tree *dep1, 
		      armoise_tree *dep2, armoise_tree *dep3, 
		      P_formula_record *next);

static tla_vec_t *
s_compute_alphas (btf_saffcomp *comp, int nb_bounds,  btf_bound *bounds, 
		  int *p_nb_alphas);

static ccl_list *
s_build_definitions (struct msa2form_compilation_data *cd,
		     armoise_tree **p_def_G, 
		     armoise_tree **p_def_V, 
		     armoise_tree ***p_def_P, 
		     armoise_tree ***p_def_VH, 		     
		     int *op, char **str_op, int nb_op,
		     tla_vsp_t V,
		     tla_group_t invV, tla_vec_t rho_w,
		     P_data *pdata, int nb_patterns,
		     tla_vec_t *alphas, int nb_alphas, tla_vec_t xhi);

			/* --------------- */

armoise_tree *
btf_msa_to_formula (int m, sataf_msa *msa, const char * const *varnames,
		    int with_prefix)
{
  armoise_tree *P;
  armoise_tree *result = NULL;
  struct msa2form_compilation_data cd;

  btf_init_compilation_data (&cd, m, varnames);
  
  if (sataf_msa_is_zero (msa) || sataf_msa_is_one (msa))
    {
      P = btf_form_trivial_predicate (&cd, sataf_msa_is_zero (msa));
      P = U_NE (PREDICATE, P);
    }
  else
    {
      sataf_msa *umsa = s_unfold_automaton (m, msa);      
      P = s_compute_formula_2 (&cd, umsa, with_prefix);
      sataf_msa_del_reference (umsa);  
    }
  if (P != NULL)
    {
      result = btf_form_crt_synform (&cd, P);
      result = btf_form_simplify_predicate (result);
    }
  btf_clear_compilation_data (&cd);

  return result;
}

			/* --------------- */

static sataf_msa *
s_unfold_automaton (int m, sataf_msa *msa)
{
  sataf_mao *unfold;
  sataf_msa *X;

  CCL_DEBUG_START_TIMED_BLOCK (("Unfold MSA     %p: ", msa));
  if (ccl_debug_is_on)
    {
      sataf_msa_log_info(CCL_LOG_DEBUG, msa);
      ccl_debug ("\n");
    }

  unfold = prestaf_crt_unfolding_automaton (m, msa);
  X = sataf_msa_compute (unfold);
  sataf_mao_del_reference (unfold);

  if (ccl_debug_is_on)
    {
      ccl_debug ("Unfolded automaton %p: ", X);
      sataf_msa_log_info(CCL_LOG_DEBUG, X);
      ccl_debug ("\n");
    }
  CCL_DEBUG_END_TIMED_BLOCK ();

  return X;
}

			/* --------------- */

struct prefix {
  int b;
  struct prefix *prev;
};

			/* --------------- */

static int
s_is_zero (sataf_msa *msa)
{
  int s, maxs = sataf_ea_get_nb_states (msa->A->automaton);

  if (sataf_ea_get_nb_exits (msa->A->automaton))
    return 0;

  for (s = 0; s < maxs; s++)
    if (sataf_ea_is_final (msa->A->automaton, s))
      return 0;
  return 1;
}

			/* --------------- */
#if 0
static void
s_check_vector (tla_vec_t v)
{
  int m = v->size;

  while (m--)
    ccl_assert (mpz_fits_sint_p (v->num[m]));
}
#endif
			/* --------------- */

static int
s_is_trivial_component (const sataf_msa *msa)
{
  if (btf_is_untransient (msa->A) || 
      sataf_ea_get_nb_states (msa->A->automaton) > 1)
    return 0;

  
  if (sataf_ea_get_nb_states (msa->A->automaton) == 1
      && sataf_ea_get_nb_exits (msa->A->automaton) == 1)
    {
      int s0 = sataf_ea_encode_succ_as_local_state (0);
      if (sataf_ea_get_successor(msa->A->automaton, 0, 0) == s0 || 
	  sataf_ea_get_successor(msa->A->automaton, 0, 1) == s0)
	return 0;
    }

  return 1;
}

			/* --------------- */

static int
s_compute_formula_with_prefix_rec (struct msa2form_compilation_data *cd, 
				   sataf_msa *msa, int i, 
				   struct prefix *prefix, tla_vec_t rho, 
				   mpz_t explen, ccl_list *result, 
				   ccl_list *defs)
{
  int is_presburger;

  if (s_is_zero (msa))
    return 1;

  if ((i % cd->m) != 0 || s_is_trivial_component (msa))
    {
      struct prefix p;
      
      is_presburger = 1;
      p.prev = prefix;
      for (p.b = 0; p.b < 2 && is_presburger; p.b++)
	{
	  sataf_msa *succ = sataf_msa_succ (msa, p.b);
	  is_presburger = 
	    s_compute_formula_with_prefix_rec (cd, succ, i + 1, &p, rho, 
					       explen, result, defs);
	  sataf_msa_del_reference (succ);
	}
    }
  else 
    {
      ccl_ustring F = NULL;
      int len = 0;

      tla_vec_set_zero (rho);
      while (prefix != NULL)
	{
	  tla_vec_gamma (rho, rho, prefix->b);
	  len++;
	  prefix = prefix->prev;
	}
      ccl_assert ((len % cd->m) == 0);
      len /= cd->m;
      mpz_ui_pow_ui (explen, 2, len);
      ccl_assert (mpz_fits_sint_p (explen));
      len = mpz_get_si (explen);

      if (ccl_hash_find (cd->def_ids, msa))
	F = (ccl_ustring) ccl_hash_get (cd->def_ids);
      else
	{
	  int index = ccl_hash_get_size (cd->def_ids);
	  char tmp[100];
	  armoise_tree *pf = s_compute_formula_without_prefix (cd, msa);

	  if (pf != NULL)
	    {
	      sprintf (tmp, "PCF%d", index);
	      pf = btf_form_crt_definition (tmp, pf);
	      ccl_list_add (defs, pf);
	      F = btf_form_get_def_id(pf);
	      msa = sataf_msa_add_reference (msa);
	      ccl_hash_insert (cd->def_ids, F);
	    }
	}

      is_presburger = (F != NULL);
      
      if (is_presburger)
	{
	  armoise_tree *P;
	  armoise_tree *p_len = NINT ();
	  armoise_tree *p_F = NID ();
	  armoise_tree *p_rho = btf_form_vec_to_armoise_predicate (rho);

	  p_len->value.int_value = len;
	  p_F->value.id_value = F;
	  B_NE (P, MUL, p_len, p_F);
	  B_NE (P, PLUS, P, p_rho);

	  ccl_list_add (result, P);
	}
    }

  return is_presburger;
}

			/* --------------- */

static armoise_tree *
s_compute_local_formulas_rec (struct msa2form_compilation_data *cd, 
			      int varindex, sataf_msa *msa, 
			      ccl_ustring *dummyX, ccl_list *defs, int *err)
{  
  ccl_ustring resultID;
  armoise_tree *result;

  if (*err || s_is_zero (msa))
    resultID = NULL;
  else 
    {
      ccl_ustring *cacheline = NULL;

      if (ccl_hash_find (cd->def_ids, msa))
	cacheline = (ccl_ustring *) ccl_hash_get (cd->def_ids);
      else
	{
	  msa = sataf_msa_add_reference (msa);
	  cacheline = ccl_new_array (ccl_ustring, cd->m);
	  ccl_hash_insert (cd->def_ids, cacheline);
	}

      if (cacheline[varindex] != NULL)
	resultID = cacheline[varindex];
      else 
	{
	  const char *idfmt;
	  char tmp[100];
	  armoise_tree *F = NULL;
	  int index = ccl_hash_get_size (cd->def_ids);

	  if (varindex != 0 || s_is_trivial_component (msa))
	    {
	      int i;
	      armoise_tree *phi[2];
	      sataf_msa *succ;

	      idfmt = "I_F%d_%d";

	      for (i = 0; i < 2; i++)
		{
		  succ = sataf_msa_succ (msa, i);
		  phi[i] = 
		    s_compute_local_formulas_rec (cd, (varindex + 1) % cd->m, 
						  succ, dummyX, defs, err);
		  if (phi[i] != NULL)	
		    phi[i] = btf_form_crt_gamma (cd->m, dummyX, phi[i], i, 
						 varindex);
		  sataf_msa_del_reference (succ);
		}

	      if (!*err)
		{
		  armoise_tree *formula = phi[0];

		  if (formula == NULL)
		    formula = phi[1];
		  else if (phi[1] != NULL)
		    B_NE (formula, UNION, formula, phi[1]);
		  ccl_assert (formula != NULL);
		  F = formula;
		}
	      else	    
		{
		  ccl_zdelete (ccl_parse_tree_delete_tree, phi[0]);
		  ccl_zdelete (ccl_parse_tree_delete_tree, phi[1]);
		}
	    }
	  else
	    {
	      F = s_compute_formula_without_prefix (cd, msa);
	      idfmt = "PCF%d_%d";
	    }

	  if (F != NULL)
	    {
	      sprintf (tmp, idfmt, index, varindex); 

	      F = btf_form_crt_definition (tmp, F);
	      ccl_list_add (defs, F);
	      ccl_assert (cacheline[varindex] == NULL);
	      resultID = cacheline[varindex] = btf_form_get_def_id (F);
	    }
	  else
	    {
	      resultID = NULL;
	    }
	}
    }

  if (resultID != NULL)
    {
      result = NID ();
      result->value.id_value = resultID;
    }
  else
    {
      result = NULL;
    }

  return result;
}

			/* --------------- */

static armoise_tree *
s_compute_formula_2 (struct msa2form_compilation_data *cd, sataf_msa *msa, 
		     int prefix)
{
  armoise_tree *R = NULL;

  if (sataf_ea_get_nb_states (msa->A->automaton) > 1)
    R = s_compute_formula_without_prefix (cd, msa);
  else
    {
      ccl_list *definitions;
      ccl_delete_proc *del = NULL;
      
      definitions = ccl_list_create ();
      
      if (prefix)
	{
	  mpz_t explen;
	  tla_vec_t rho;
	  ccl_list *formulas = ccl_list_create ();

	  tla_vec_init (rho, cd->m);
	  mpz_init (explen);

	  if (s_compute_formula_with_prefix_rec (cd, msa, 0, NULL, rho, 
						 explen, formulas, definitions))
	    {
	      armoise_tree *ctx = NULL;
	      
	      ccl_assert (ccl_list_get_size (formulas) > 0);
	      ccl_assert (ccl_list_get_size (definitions) > 0);
	      
	      while (!ccl_list_is_empty (definitions))
		{
		  armoise_tree *def = 
		    (armoise_tree *) ccl_list_take_first (definitions);

		  def->next = ctx;
		  ctx = def;
		}
	      ctx = REVERSE (ctx);
	      ctx = U_NE (PREDICATE_CONTEXT, ctx);
	      
	      R = (armoise_tree *) ccl_list_take_first (formulas);
	      if (! ccl_list_is_empty (formulas))
		R = U_NE (PAREN, R);

	      while (!ccl_list_is_empty (formulas))
		{
		  armoise_tree *P = ccl_list_take_first (formulas);
		  P = U_NE (PAREN, P);
		  B_NE (R, UNION, R, P);
		}	  
	      B_NE (R, PREDICATE, ctx, R);
	    }
	  else
	    {
	      del = (ccl_delete_proc *) ccl_parse_tree_delete_tree;
	    }

	  tla_vec_clear (rho);
	  mpz_clear (explen);
	  ccl_list_clear_and_delete (formulas, del);
	}
      else
	{
	  int i;
	  int error = 0;
	  armoise_tree *F;
	  char tmp[100];
	  ccl_ustring *dummyX = ccl_new_array (ccl_ustring , cd->m + 1);
	  
	  for (i = 0; i < cd->m; i++)
	    {
	      sprintf (tmp, "x%d", i);
	      dummyX[i] = ccl_string_make_unique (tmp);
	    }
	  sprintf (tmp, "xp%d", 0);
	  dummyX[i] = ccl_string_make_unique (tmp);
	  
	  F = s_compute_local_formulas_rec (cd, 0, msa, dummyX, definitions, 
					    &error);
	  ccl_delete (dummyX);

	  if (error)
	    del = (ccl_delete_proc *) ccl_parse_tree_delete_tree;
	  else
	    {
	      armoise_tree *ctx = NULL;
	      armoise_tree **pctx = &ctx;

	      ccl_assert (ccl_list_get_size (definitions) > 0);
	      
	      while (!ccl_list_is_empty (definitions))
		{
		  armoise_tree *def = 
		    (armoise_tree *) ccl_list_take_first (definitions);

		  *pctx = def;
		  pctx = &(def->next);
		}
	      ctx = U_NE (PREDICATE_CONTEXT, ctx);
	      
	      B_NE (R, PREDICATE, ctx, F);
	    }
	  
	  {
	    ccl_pointer_iterator *i = ccl_hash_get_elements (cd->def_ids);
	    while (ccl_iterator_has_more_elements (i))
	      {
		ccl_ustring *cl = (ccl_ustring *) ccl_iterator_next_element (i);

		ccl_delete (cl);
	      }
	    ccl_iterator_delete (i);
	  }
	}

      ccl_list_clear_and_delete (definitions, del);
    }

  return R;
}

			/* --------------- */

static int
s_is_zero_vec (sataf_sa *sa)
{
  int one_final = 0;
  sataf_ea *ea = sa->automaton;
  int s, maxs = sataf_ea_get_nb_states (ea);

  for (s = 0; s < maxs; s++)
    {
      int succ = sataf_ea_get_successor (ea, s, 0);
      if (sataf_ea_is_exit_state (succ))
	return 0;
      one_final = one_final || sataf_ea_is_final (ea, s);
      succ = sataf_ea_get_successor (ea, s, 1);      
      if (sataf_ea_is_local_state (succ))
	return 0;
      succ = sataf_ea_decode_succ_state (succ);
      if (! sataf_msa_is_zero (sa->bind[succ]))
	return 0;
    }

  return one_final;
}

			/* --------------- */

static armoise_tree *
s_compute_formula_without_prefix (struct msa2form_compilation_data *cd,
				  sataf_msa *msa)
{
  armoise_tree *result = NULL;

  if (sataf_msa_is_zero (msa) || sataf_msa_is_one (msa))
    {
      result = btf_form_trivial_predicate (cd, sataf_msa_is_zero (msa));
      result = U_NE (PREDICATE, result);
    }
  else
    {
      ccl_delete_proc *del = NULL;
      ccl_list *l = ccl_list_create ();
      sataf_msa *x = sataf_msa_add_reference (msa);

      while (x != NULL && ! sataf_msa_is_zero (x))
	{
	  armoise_tree *f = NULL;

	  if (s_is_zero_vec (x->A))
	    {
	      f = btf_form_zero_vec_predicate (cd->m);
	      sataf_msa_del_reference (x);
	      x = sataf_msa_zero (2);
	    }
	  else
	    {
	      sataf_msa *aux = s_reduce_msa (cd, x, &f);
	      sataf_msa_del_reference (x);
	      x = aux;
	    }

	  if (x != NULL)
	    {
	      ccl_assert (f != NULL);
	      ccl_list_add (l, f);
	    }
	}

      if (x != NULL)
	{
	  int i;
	  ccl_pair *p;
	  armoise_tree *defs = NULL;
	  armoise_tree *P = NULL;

	  if (ccl_list_get_size (l) == 1)
	    {
	      defs = (armoise_tree *) ccl_list_take_first (l);
	      result = ccl_parse_tree_duplicate (defs, NULL);
	      ccl_parse_tree_delete_tree (defs);
	    }
	  else
	    {
	      for (i = 0, p = FIRST (l); p; p = CDR (p), i++)
		{
		  char tmp[100];
		  armoise_tree *aux;

		  sprintf (tmp, "CF%d", i);
		  aux = btf_form_crt_definition (tmp, CAR (p));
		  aux->next = defs;
		  defs = aux;

		  aux = NID ();
		  aux->value.id_value = btf_form_get_def_id (defs);

		  if (P == NULL)
		    P = aux;
		  else
		    B_NE (P, XOR, P, aux);
		}
	  
	      if (defs != NULL)
		{
		  defs = REVERSE (defs);
		  defs = U_NE (PREDICATE_CONTEXT, defs);
		  B_NE (result, PREDICATE, defs, P);
		}
	      else
		{
		  result = U_NE (PREDICATE, P);
		}
	    }
	  sataf_msa_del_reference (x);
	}
      else
	{
	  del = (ccl_delete_proc *) ccl_parse_tree_delete_tree;
	}

      ccl_list_clear_and_delete (l, del);
    }

  return result;
}

			/* --------------- */

static sataf_msa *
s_reduce_msa (struct msa2form_compilation_data *cd, sataf_msa *msa,
	      armoise_tree **pF)
	      
{
  ccl_delete_proc *del = NULL;
  ccl_hash *sa_tables = NULL;    
  tla_vec_t xhi;
  tla_vec_t rho_w;
  btf_saffcomp *comp;
  btf_bound *bounds = NULL;
  int nb_bounds = 0;
  ccl_list *formulas;
  sataf_msa *result = NULL;
  ccl_list *Tcomps = btf_compute_components (cd->m, msa, &sa_tables);  
  int is_presburger = (Tcomps != NULL);
  ccl_hash *acond;

  if (! is_presburger)
    return NULL;

  formulas = ccl_list_create ();
  tla_vec_init (xhi, cd->m);
  tla_vec_init (rho_w, cd->m);

  btf_misc_compute_xhi_sigma0 (cd->m, msa, xhi);
  btf_msa_to_bounds (cd->m, msa, Tcomps, sa_tables, &bounds, &nb_bounds);
  acond = ccl_hash_create (NULL, NULL, 
			   (ccl_delete_proc *) sataf_sa_del_reference,
			   (ccl_delete_proc *) ccl_bittable_delete);

  while (! ccl_list_is_empty (Tcomps) && is_presburger)
    {
      int offset = s_compute_rho_w (cd->m, msa, rho_w, Tcomps, &comp);

      CCL_DEBUG_START_TIMED_BLOCK (("compute formulas for comp %p", comp->C));
      ccl_list_remove (Tcomps, comp);
      is_presburger = 
	s_compute_formulas_for_comp (cd, msa, comp, formulas, offset, nb_bounds,
				     bounds, rho_w, xhi, acond);
      btf_saffcomp_delete (comp);
      CCL_DEBUG_END_TIMED_BLOCK ();
    }

  ccl_list_clear_and_delete (Tcomps, (ccl_delete_proc *) btf_saffcomp_delete);
  while (nb_bounds--)
    {
      tla_vsp_clear (bounds[nb_bounds].V);
      tla_saff_clear (bounds[nb_bounds].B);
    }
  ccl_delete (bounds);

  tla_vec_clear (xhi);
  tla_vec_clear (rho_w);
  ccl_hash_delete (sa_tables);

  if (is_presburger)
    {
      int i;
      char tmp[100];
      ccl_pair *p;
      armoise_tree *defs = NULL;
      armoise_tree *P = NULL;
      sataf_msa *x = s_compute_with_new_cond (msa, acond);
      result = sataf_msa_xor (x, msa);
      sataf_msa_del_reference (x);

      if (ccl_list_get_size (formulas) == 1)
	{
	  defs = (armoise_tree *) ccl_list_take_first (formulas);
	  *pF = ccl_parse_tree_duplicate (defs, NULL);
	  ccl_parse_tree_delete_tree (defs);
	}
      else
	{
	  for (i = 0, p = FIRST (formulas); p; p = CDR(p), i++)
	    {
	      armoise_tree *aux;

	      sprintf (tmp, "F%d", i);
	      aux = btf_form_crt_definition (tmp, CAR (p));
	      aux->next = defs;
	      defs = aux;

	      aux = NID ();
	      aux->value.id_value = btf_form_get_def_id (defs);

	      if (P == NULL)
		P = aux;
	      else
		B_NE (P, UNION, P, aux);
	    }

	  if (defs != NULL)
	    {
	      defs = REVERSE (defs);
	      defs = U_NE (PREDICATE_CONTEXT, defs);
	      B_NE (*pF, PREDICATE, defs, P);
	    }
	  else
	    {
	      *pF = U_NE (PREDICATE, P);
	    }
	}
    }
  else
    {
      del = (ccl_delete_proc *) ccl_parse_tree_delete_tree;
    }
  ccl_hash_delete (acond);
  ccl_list_clear_and_delete (formulas, del);

  return result;
}

			/* --------------- */

static int
s_compute_formulas_for_comp (struct msa2form_compilation_data *cd, 
			     sataf_msa *msa, btf_saffcomp *comp, 
			     ccl_list *formulas, int offset, int nb_bounds, 
			     btf_bound *bounds, tla_vec_t rho_w, tla_vec_t xhi,
			     ccl_hash *acond)
{
  int i;
  int p;
  int is_presburger;
  P_formula_record *frec = NULL;
  tla_group_t invV;
  armoise_tree *def_G;
  armoise_tree *def_V;
  armoise_tree **def_P;
  armoise_tree **def_VH;
  ccl_list *definitions;
  sataf_msa ***A_h;
  int nb_h_removed = 0;
  int nb_alphas;
  tla_vec_t *alphas;
  int op[] = { LCA_GEQ, LCA_LEQ };
  char *str_op[] = { "geq", "leq" };
  int nb_op = sizeof (op) / sizeof (op[0]);
  P_data *pdata = NULL;
  struct acond_st unioncond;
  sataf_msa *Punion;

  if (s_is_zero_vec (comp->C))
    {
      armoise_tree *F = btf_form_zero_vec_predicate (cd->m);
      ccl_list_add (formulas, F);

      return 1;
    }

  tla_group_init (invV, cd->m);
  is_presburger = s_compute_patterns (cd, comp, offset, invV);
  ccl_assert (comp->patterns[cd->m * comp->qpattern] != NULL);

  if (! is_presburger) 
    {
      tla_group_clear (invV);

      return 0;
    }
      
  alphas = s_compute_alphas (comp, nb_bounds, bounds, &nb_alphas);
  if (alphas == NULL)
    {
      tla_group_clear (invV);
      return 0;
    }

  CCL_DEBUG_START_TIMED_BLOCK (("compute P conds for %d patterns",
				comp->nb_patterns));
  s_init_acond (&unioncond);
  pdata = s_compute_acceptance_conditions (cd->m, msa, comp, invV, rho_w,
					   &unioncond);
  CCL_DEBUG_END_TIMED_BLOCK ();
  CCL_DEBUG_START_TIMED_BLOCK (("compute union of Ps"));
  Punion = s_compute_with_new_cond (msa, unioncond.cond);
  s_clear_acond (&unioncond);

  CCL_DEBUG_END_TIMED_BLOCK ();

  definitions = s_build_definitions (cd, &def_G, &def_V, 
				     &def_P, &def_VH,
				     op, str_op, nb_op, comp->V,
				     invV, rho_w, pdata, comp->nb_patterns,
				     alphas, nb_alphas, xhi);
  
  CCL_DEBUG_START_TIMED_BLOCK (("detection for %d patterns and %d spaces", 
				comp->nb_patterns,
				nb_alphas));

  CCL_DEBUG_START_TIMED_BLOCK (("detect H"));
  A_h = ccl_new_array (sataf_msa **, nb_alphas);
  for (i = 0; i < nb_alphas; i++)
    {
      int o;

      A_h[i] = ccl_new_array (sataf_msa *, nb_op);
      for (o = 0; o < nb_op && def_VH[i] == NULL; o++)
	{
	  sataf_msa *UP_cap_H;
	  ccl_list *Fstates = ccl_list_create ();

	  A_h[i][o] = s_compute_bound_automaton (alphas[i], xhi, op[o]);
	  UP_cap_H = sataf_msa_and (Punion, A_h[i][o]);

	  if (sataf_msa_is_detectable (UP_cap_H, msa, Fstates))
	    {
	      char tmp[100];
	      
	      sprintf (tmp, "VH%d", i);
	      def_VH[i] = 
		btf_form_crt_VH_definition (cd, tmp, rho_w, xhi, comp->V, 
					    alphas[i], op[o]);
	      ccl_list_add (definitions, def_VH[i]);

	      frec = 
		s_new_formula_record (def_VH[i], Fstates, NULL,
				      NULL, NULL, NULL, frec);
	    }
	  else
	    {
	      ccl_list_clear_and_delete (Fstates, (ccl_delete_proc *)
					 sataf_msa_del_reference);
	    }
	  sataf_msa_del_reference (UP_cap_H);
	}

      if (def_VH[i] != NULL)
	{
	  nb_h_removed++;
	  for (o = 0; o < nb_op; o++)
	    {
	      ccl_zdelete (sataf_msa_del_reference, A_h[i][o]);
	      A_h[i][o] = NULL;
	    }
	  ccl_delete (A_h[i]);
	  A_h[i] = NULL;
	}
    }
  sataf_msa_del_reference (Punion);

  CCL_DEBUG_END_TIMED_BLOCK ();
  CCL_DEBUG_START_TIMED_BLOCK (("detect P & H for %d/%d half-spaces", 
				nb_alphas-nb_h_removed, nb_alphas));
  for (p = 0; p < comp->nb_patterns; p++)
    {
      int j;
      sataf_msa *A_p = s_compute_with_new_cond (msa, pdata[p].p_cond.cond);
      ccl_ustring Pid = btf_form_get_def_id (def_P[p]);

      frec = 
	s_new_formula_record (def_P[p], pdata[p].p_cond.final_states, NULL,
			      NULL, NULL, NULL, frec);
      pdata[p].p_cond.final_states = NULL;
      s_clear_acond (&pdata[p].p_cond);
      
      for (j = 0; j < nb_alphas; j++)
	{
	  int o;
	  int stop = 0;

	  if (A_h[j] == NULL)
	    continue;

	  for (o = 0; o < nb_op && !stop; o++)
	    {
	      ccl_list *Fstates = ccl_list_create ();
	      sataf_msa *A_P_cap_h = sataf_msa_and (A_p, A_h[j][o]);

	      if (sataf_msa_is_detectable (A_P_cap_h, msa, Fstates))
		{
		  char tmp[100];
		  armoise_tree *def_ph;

		  sprintf (tmp, "P%dH%d", p, j);
		  def_ph = 
		    btf_form_crt_PH_definition (cd, tmp, Pid, rho_w, xhi,
						comp->V, alphas[j], op[o]);
		  ccl_list_add (definitions, def_ph);
		  frec = 
		    s_new_formula_record (def_ph, Fstates, def_P[p], NULL,
					  NULL, NULL, frec);
		  stop = 1;		  
		}
	      else
		{
		  ccl_list_clear_and_delete (Fstates, (ccl_delete_proc *)
					     sataf_msa_del_reference);
		}
	      sataf_msa_del_reference (A_P_cap_h);
	    }
	}

      sataf_msa_del_reference (A_p);
      ccl_list_clear_and_delete (pdata[p].pattern_vectors, 
				 (ccl_delete_proc *) s_delete_vec);
    }

  ccl_delete (pdata);
  CCL_DEBUG_END_TIMED_BLOCK ();

  for (i = 0; i < nb_alphas; i++)
    {
      int o;
      
      tla_vec_clear (alphas[i]);
      if (A_h[i] == NULL)
	continue;
      for (o = 0; o < nb_op; o++)
	ccl_zdelete (sataf_msa_del_reference, A_h[i][o]);
      ccl_delete (A_h[i]);
    }
  ccl_delete (alphas);
  ccl_delete (A_h);
  tla_group_clear (invV);

  ccl_zdelete (ccl_delete, def_P);
  ccl_zdelete (ccl_delete, def_VH);

  CCL_DEBUG_END_TIMED_BLOCK ();

  is_presburger = 
    s_compute_boolean_combination (definitions, frec, msa, comp, 
				   formulas, acond);
  ccl_list_clear_and_delete (definitions, (ccl_delete_proc *)
			     ccl_parse_tree_delete_tree);
  s_cleanup_formula_records (frec);

  return is_presburger;
}


			/* --------------- */

static int
s_compute_pattern_start_state (btf_saffcomp *comp, int offset)
{
  sataf_ea *ea = comp->C->automaton;
  int mn = comp->m * comp->mi->n;
  int q = comp->q;

  while ((offset % mn) != 0)
    {
      q = sataf_ea_get_successor (ea, q, 0);
      ccl_assert (sataf_ea_is_local_state (q));
      q = sataf_ea_decode_succ_state (q);      
      offset++;
      ccl_assert (offset % comp->m != 0 || sataf_ea_is_final (ea, q));
    }

  return q;
}

			/* --------------- */

static int
s_compute_modulo_info (btf_saffcomp *comp, modulo_info *mi, int offset)
{
  int qpattern;
  btf_compute_n0 (mi->modea, &mi->n0, &mi->n);
  qpattern = s_compute_pattern_start_state (comp, offset);
  tla_group_init (mi->invV, mi->m);
  btf_compute_invariant_group (mi->invV, comp->V, mi->modea, 
			       mi->states_to_classes[qpattern], 
			       mi->m, mi->n0, mi->n);
  return qpattern;
}

			/* --------------- */

static int
s_compute_patterns (struct msa2form_compilation_data *cd,
		    btf_saffcomp *comp, int offset, 
		    tla_group_t invV)
{
  int qpattern;
  int is_presburger;
  sataf_ea *modea;
  int *s2c;

  CCL_DEBUG_START_TIMED_BLOCK (("compute pattern for comp %p", comp->C));

  if (comp->mi == NULL)
    {
      if (ccl_hash_find (cd->mi, comp->C))
	{
	  if (ccl_debug_is_on)
	    ccl_debug ("find modulo info in cache\n");
	  comp->mi = ccl_hash_get (cd->mi);
	  comp->mi->refcount++;
	  qpattern = s_compute_pattern_start_state (comp, offset);
	}
      else
	{
	  comp->mi = ccl_new (modulo_info);
	  ccl_hash_insert (cd->mi, comp->mi);
	  sataf_sa_add_reference (comp->C);

	  comp->mi->refcount = 2;
	  comp->mi->m = comp->m;
	  comp->mi->modea = 
	    btf_ea_compute_mod_classes (comp->C->automaton, 
					&comp->mi->states_to_classes, 
					&comp->mi->classes_to_states);	  
	  qpattern = s_compute_modulo_info (comp, comp->mi, offset);
	}
    }
  else
    {
      qpattern = s_compute_modulo_info (comp, comp->mi, offset);
    }

  modea = comp->mi->modea;
  s2c = comp->mi->states_to_classes;
  comp->nb_classes = sataf_ea_get_nb_states (modea);
  tla_group_set (invV, comp->mi->invV);

  comp->dim = invV->dim;
  comp->ci = ccl_new_array (int, comp->dim);
  is_presburger = 
    btf_compute_patterns (comp->mi->invV, modea, s2c[qpattern], comp->ci, 
			  &comp->patterns, &comp->pattern_width, 
			  &comp->nb_patterns, 
			  &comp->X);
  if (is_presburger)
    comp->qpattern = s2c[qpattern];
  else
    comp->qpattern = -1;

  CCL_DEBUG_END_TIMED_BLOCK ();

  return is_presburger;
}

			/* --------------- */

static void
s_delete_marked_msa (void *p)
{
  sataf_msa *msa = CCL_BITPTR2PTR(sataf_msa *, p);
  sataf_msa_del_reference (msa);
}

			/* --------------- */

static int
s_compute_rho_w (int m, sataf_msa *msa, tla_vec_t rho, ccl_list *Tcomps,
		 btf_saffcomp **p_comp)
{
  int result = 0;
  int stop = 0;
  btf_saffcomp *comp = CAR (FIRST (Tcomps));
  sataf_msa *goal = sataf_msa_find_or_add (comp->C, comp->q);
  ccl_hash *pred = 
    ccl_hash_create (NULL, NULL, (ccl_delete_proc *)sataf_msa_del_reference,
		     s_delete_marked_msa);
  ccl_list *todo = ccl_list_create ();
  tla_vec_set_zero (rho);
  if (msa != goal)
    {      
      sataf_msa *aux;
      ccl_list_add (todo, sataf_msa_add_reference (msa));
      ccl_hash_find (pred, sataf_msa_add_reference (msa));
      ccl_hash_insert (pred, sataf_msa_add_reference (msa));
      
      while (!ccl_list_is_empty (todo) && !stop)
	{
	  int b;
	  sataf_msa *s = ccl_list_take_first (todo);
	  	  
	  for (b = 0; b < 2 && !stop; b++)
	    {
	      sataf_msa *t = sataf_msa_succ (s, b);
	      if (!ccl_hash_find (pred, t))
		{
		  s = sataf_msa_add_reference (s);
		  t = sataf_msa_add_reference (t);
		  if (b == 0)		
		    ccl_hash_insert (pred, s);
		  else
		    ccl_hash_insert (pred, CCL_BITPTR (void *, s));
		  ccl_list_add (todo, sataf_msa_add_reference (t));
		  stop = (t == goal);
		}
	      sataf_msa_del_reference (t);
	    }
	  sataf_msa_del_reference (s);
	}

      result = 0;
      aux = goal;
      while (aux != msa)
	{
	  int b;
	  sataf_msa *p;
	  result++;
	  ccl_hash_find (pred, aux);
	  p = ccl_hash_get (pred);
	  b = CCL_PTRHASBIT (p) ? 1 : 0;
	  p = CCL_BITPTR2PTR (sataf_msa *, p);
	  aux = p;
	  tla_vec_gamma (rho, rho, b);  	  
	}
    }

  ccl_list_clear_and_delete (todo, (ccl_delete_proc *)sataf_msa_del_reference);
  ccl_hash_delete (pred);
  sataf_msa_del_reference (goal);
  *p_comp = comp;

  ccl_assert (result >= 0);

  return result;
}

			/* --------------- */

static ccl_list *
s_compute_pattern_vectors (btf_saffcomp *comp, int P)
{
  int p;
  ccl_list *result = ccl_list_create ();
  int m = comp->m;
  int *pattern = comp->patterns[m * comp->qpattern];
  int *ivec = ccl_new_array (int, comp->dim);

  for (p = 0; p < comp->pattern_width; p++)
    {
      if (pattern[p] == P)
	{
	  int i;
	  tla_vec_t *v = ccl_new (tla_vec_t);

	  tla_vec_init (*v, m);
	  btf_decompose_int_point (ivec, p, comp->ci, comp->dim);
	  for (i = 0; i < comp->dim; i++)
	    {
	      int j;
	      
	      for (j = 0; j < m; j++)
		mpz_addmul_ui ((*v)->num[j], MM (comp->X, j + 1, i + 1), 
			       ivec[i]);
	    }
	  ccl_list_add (result, v);
	}
    }
  ccl_delete (ivec);

  return result;
}

			/* --------------- */

static void
s_delete_vec (tla_vec_t *v)
{
  tla_vec_clear (*v);
  ccl_delete (v);
}

			/* --------------- */

static sataf_msa *
s_compute_bound_automaton (tla_vec_t alpha, tla_vec_t xhi, int op)
{
  int i;
  int *args;
  int c;
  tla_vec_t z_alpha;
  mpq_t alpha_xhi;
  sataf_mao *mao;
  sataf_msa *result;

  CCL_DEBUG_START_TIMED_BLOCK (("compute bound automaton"));
  mpq_init (alpha_xhi);  
  tla_vec_init (z_alpha, alpha->size);
  tla_vec_z_mul (z_alpha, xhi->den, alpha);  
  tla_vec_scalar_product(alpha_xhi, alpha, xhi);
  args = ccl_new_array (int, alpha->size);

  for (i = 0; i < alpha->size; i++)
    {
      ccl_assert (mpz_fits_sint_p (z_alpha->num[i]));
      args[i] = mpz_get_si (z_alpha->num[i]);
    }

  ccl_assert (mpz_fits_sint_p (mpq_numref (alpha_xhi)));
  c = mpz_get_si (mpq_numref (alpha_xhi));
  mpq_clear (alpha_xhi);  
  mao = prestaf_crt_linear_constraint_automaton(op, args, alpha->size, c);
  result = sataf_msa_compute (mao);
  sataf_mao_del_reference (mao);
  tla_vec_clear (z_alpha);
  ccl_delete (args);

  if (ccl_debug_is_on)
    {
      sataf_msa_log_info (CCL_LOG_DEBUG, result);
      ccl_debug ("\n");
    }
  CCL_DEBUG_END_TIMED_BLOCK ();

  return result;
}

			/* --------------- */

struct subformula
{
  ccl_ustring id;
  int *p_in_comb;
};

static armoise_tree *
s_boolean_combination_to_paren_predicate (boolean_formula *bf, 
					  struct subformula *subf,
					  boolean_formula *parent)
{
  int i;
  armoise_tree *tmp1;
  armoise_tree *tmp2;
  armoise_tree *result;

  switch (bf->type)
    {
    case BF_CST:
      result = NID ();
      result->value.id_value = subf[bf->arity_or_cst].id;
      *(subf[bf->arity_or_cst].p_in_comb) = 1;
      break;

    case BF_UNION: case BF_INTER:
      result = s_boolean_combination_to_paren_predicate (bf->operands[0], subf,
							 bf);
      for (i = 1; i < bf->arity_or_cst; i++)
	{
	  tmp1 = s_boolean_combination_to_paren_predicate (bf->operands[i], 
							   subf, bf);

	  if (bf->type == BF_UNION)
	    B_NE (result, UNION, result, tmp1);
	  else
	    B_NE (result, INTERSECTION, result, tmp1);
	}      
      break;

    case BF_DIFF: 
      ccl_assert (bf->arity_or_cst == 2);

      tmp1 = 
	s_boolean_combination_to_paren_predicate (bf->operands[0], subf, bf);
      tmp2 = 
	s_boolean_combination_to_paren_predicate (bf->operands[1], subf, bf);
      B_NE (result, DIFFERENCE, tmp1, tmp2);
      break;      
    default:
      result = NULL;
      ccl_assert ("invalid Boolean formula type" && 0);
    }
  
  if (parent != NULL && (bf->type == BF_UNION || bf->type == BF_DIFF) &&
      parent->type != bf->type)
    result = U_NE (PAREN, result);

  return result;
}

			/* --------------- */

static armoise_tree *
s_boolean_combination_to_predicate (boolean_formula *bf, 
				    struct subformula *subf)
{
  return s_boolean_combination_to_paren_predicate (bf, subf, NULL);
}

			/* --------------- */

static ccl_list *
s_get_final_states (sataf_sa *sa)
{
  int s;  
  int maxs = sataf_ea_get_nb_states (sa->automaton);
  ccl_list *result = ccl_list_create ();

  for (s = 0; s < maxs; s++)
    {
      if (sataf_ea_is_final (sa->automaton, s))
	{
	  sataf_msa *a = sataf_msa_find_or_add (sa, s);
	  ccl_list_add (result, a);
	}
    }

  return result;
}

			/* --------------- */

static void 
s_add_final_states (ccl_list *states, ccl_hash *msa2idx)
{
  ccl_pair *p;

  for (p = FIRST (states); p; p = CDR(p))
    {
      int index;
      sataf_msa *a = (sataf_msa *) CAR (p);
      
      if (ccl_hash_find (msa2idx, a))
	continue;

      index = ccl_hash_get_size (msa2idx);
      sataf_msa_add_reference (a);
      ccl_hash_insert (msa2idx, (void *) (intptr_t) index);
    }
}

			/* --------------- */

static int
s_add_formula_to_context (ccl_list *ctx, P_formula_record *formulas,
			  P_formula_record *frec,
			  ccl_hash *done, ccl_list *deflist)
{
  int i;
  int result = 0;
  const int nb_deps = sizeof (frec->deps)/sizeof (frec->deps[0]);
  
  if (ccl_hash_find (done,frec->def))
    return 0;

  for (i = 0; i < nb_deps; i++)
    {
      P_formula_record *f;

      if (frec->deps[i] == NULL || ccl_hash_find (done, frec->deps[i]))
	continue;

      for (f = formulas; f && f->def != frec->deps[i]; f = f->next)
	CCL_NOP ();
	  
      if (f == NULL)
	{
	  ccl_hash_find (done, frec->deps[i]);
	  ccl_hash_insert (done, frec->deps[i]);
	  ccl_list_add(ctx, frec->deps[i]);
	  ccl_list_remove (deflist, frec->deps[i]);
	  result++;
	}
      else
	{
	  result += s_add_formula_to_context (ctx, formulas, f, done, deflist);
	}
    }
  ccl_list_add(ctx, frec->def);
  ccl_list_remove (deflist, frec->def);
  ccl_hash_find (done, frec->def);
  ccl_hash_insert (done, frec->def);
  result++;

  return result;
}

			/* --------------- */
			  
static int
s_compute_boolean_combination (ccl_list *deflist,
			       P_formula_record *formulas, sataf_msa *msa, 
			       btf_saffcomp *comp, ccl_list *fresult,
			       ccl_hash *acond)
{
  int i;
  ccl_pair *p;
  int nb_states;
  int is_presburger;
  P_formula_record *frec;
  int nb_formulas;
  struct subformula *sub_formulas;
  ccl_bittable **C; 
  ccl_bittable **C_all;
  ccl_bittable *X;
  ccl_list *Xelements;
  ccl_hash *msa2idx;
  boolean_formula *bf;
  sataf_msa **final_states;
  int nb_definitions;
  armoise_tree **definitions;

  CCL_DEBUG_START_TIMED_BLOCK (("compute boolean combination"));
  definitions = (armoise_tree **) ccl_list_to_array (deflist, &nb_definitions);
  msa2idx = 
    ccl_hash_create (NULL, NULL, (ccl_delete_proc *) sataf_msa_del_reference,
		     NULL);

  for (nb_formulas = 0, frec = formulas; frec; frec = frec->next)
    {
      nb_formulas++;
      s_add_final_states (frec->final_states, msa2idx);
    }
  

  Xelements = s_get_final_states (comp->C);
  s_add_final_states (Xelements, msa2idx); 
  
  nb_states = ccl_hash_get_size (msa2idx);

  final_states = ccl_new_array (sataf_msa *, nb_states);

  X = ccl_bittable_create (nb_states);
  for (p = FIRST (Xelements); p; p = CDR (p))
    {
      int index;
      sataf_msa *a = (sataf_msa *) CAR (p);
      
      ccl_assert (ccl_hash_find (msa2idx, a));
      
      ccl_hash_find (msa2idx, a);
      index = (intptr_t) ccl_hash_get (msa2idx);	  
      ccl_bittable_set (X, index);
      final_states[index] = a;
  }
  ccl_list_clear_and_delete (Xelements, (ccl_delete_proc *)
			     sataf_msa_del_reference );

  sub_formulas = ccl_new_array (struct subformula, nb_formulas);
  C = ccl_new_array (ccl_bittable *, nb_formulas);
  C_all = ccl_new_array (ccl_bittable *, nb_formulas);
  for (i = 0, frec = formulas; frec; frec = frec->next)
    {
      ccl_pair *p;

      sub_formulas[i].id = btf_form_get_def_id (frec->def);
      sub_formulas[i].p_in_comb = &frec->in_comb;

      C[i] = ccl_bittable_create (nb_states);
      C_all[i] = ccl_bittable_create (nb_states);
      for (p = FIRST (frec->final_states); p; p = CDR(p))
	{
	  int index;
	  sataf_msa *a = (sataf_msa *) CAR (p);

	  ccl_assert (ccl_hash_find (msa2idx, a));

	  ccl_hash_find (msa2idx, a);
	  index = (intptr_t) ccl_hash_get (msa2idx);	  
	  ccl_bittable_set (C_all[i], index);
	  if (a->A == comp->C)
	    ccl_bittable_set (C[i], index);
	  final_states[index] = a;
	}
      i++;
    }

  bf = btf_boolean_combination (C, nb_formulas, X);

  is_presburger = (bf != NULL);

  if (bf != NULL)
    {
      int nb_actually_used = 1;
      ccl_bittable *bit_acond = btf_boolean_formula_eval (bf, C_all);
      armoise_tree *F = s_boolean_combination_to_predicate (bf, sub_formulas);
      armoise_tree *ctx = NE (PREDICATE_CONTEXT, ZN, F);
      armoise_tree **p_def = &ctx->child;
      ccl_hash *done = ccl_hash_create (NULL, NULL, NULL, NULL);
      ccl_list *forms_in_ctx = ccl_list_create ();

      ccl_assert (ccl_bittable_is_included_in (X, bit_acond));

      for (frec = formulas; frec; frec = frec->next)
	if (frec->in_comb)
	  {
	    nb_actually_used += 
	      s_add_formula_to_context (forms_in_ctx, formulas, frec, done, 
					deflist);	    
	  }

      while (!ccl_list_is_empty (forms_in_ctx))
	{
	  *p_def = ccl_list_take_first (forms_in_ctx);
	  p_def = &((*p_def)->next);
	}
      ccl_list_delete (forms_in_ctx);
      ccl_hash_delete (done);
      btf_boolean_formula_delete (bf);

      if (nb_actually_used == 1)
	{
	  F = ccl_parse_tree_duplicate (ctx->child->child->next, NULL);
	  ccl_parse_tree_delete_tree (ctx);
	}
      else
	{
	  F = NE (PREDICATE, ctx, ZN);
	}
      
      ccl_list_add (fresult, F);

      for (i = ccl_bittable_get_first (bit_acond);
	   i >= 0;
	   i = ccl_bittable_get_next (bit_acond, i))
	{
	  ccl_bittable *final;
	  sataf_sa *sa = final_states[i]->A;
	  uint32_t init = final_states[i]->initial;

	  if (ccl_hash_find (acond, sa))
	    final = ccl_hash_get (acond);
	  else
	    {
	      int nb_states = sataf_ea_get_nb_states (sa->automaton);
	      final = ccl_bittable_create (nb_states);
	      sataf_sa_add_reference (sa);	      
	      ccl_hash_insert (acond, final);
	    }
	  ccl_bittable_set (final, init);
	}
	     
      ccl_bittable_delete (bit_acond);
    }

  ccl_bittable_delete (X);
  for (i = 0; i < nb_formulas; i++)
    ccl_bittable_delete (C[i]);
  ccl_delete (C);
  for (i = 0; i < nb_formulas; i++)
    ccl_bittable_delete (C_all[i]);
  ccl_delete (C_all);
  ccl_hash_delete (msa2idx);
  ccl_delete (final_states);
  ccl_delete (sub_formulas);
  ccl_delete (definitions);

  CCL_DEBUG_END_TIMED_BLOCK ();

  return is_presburger;
}

			/* --------------- */

static void
s_cleanup_formula_records (P_formula_record *frec)
{
  while (frec != NULL)
    {
      P_formula_record *frec_next = frec->next;
      
      ccl_list_clear_and_delete (frec->final_states, (ccl_delete_proc *)
				 sataf_msa_del_reference);
      ccl_delete (frec);
      frec = frec_next;
    }
}

			/* --------------- */


static sataf_msa *
s_compute_with_new_cond (sataf_msa *msa, ccl_hash *newcond)
{
  sataf_msa *result;

  if (0)
    {
      sataf_mao *mao = prestaf_crt_new_cond (msa, newcond);
      result = sataf_msa_compute (mao);
      sataf_mao_del_reference (mao);
    }
  else
    {
      /*ccl_hash *nc = s_convert_condition (newcond);*/
      result = sataf_msa_change_acceptance (msa, newcond);
      /*ccl_hash_delete (nc);*/

      if(0) 
	{
	  sataf_mao *mao = prestaf_crt_new_cond (msa, newcond);
	  sataf_msa *aux = sataf_msa_compute (mao);
	  ccl_assert (aux == result);
	  sataf_msa_del_reference (aux);
	  sataf_mao_del_reference (mao);
	}
    }

  return result;
}


			/* --------------- */

struct visit_for_acc_data
{
  int m;
  sataf_msa *msa;
  btf_saffcomp *comp;
  tla_group_t invV;
  tla_vec_t rho_w;
  P_data *result;
  acond *unioncond;
  tla_vec_t tmp[2];
};

			/* --------------- */

static void
s_visit_for_acc (sataf_sa *C, int s, tla_vec_t rho, void *data)
{
  int P;
  struct visit_for_acc_data *D = data;

  for (P = 0; P < D->comp->nb_patterns; P++)
    {
      int accepting = 0;
      ccl_pair *p; 
      P_data *pdata = &D->result[P];
      
      for (p = FIRST (pdata->pattern_vectors); p && ! accepting; 
	   p = CDR (p))
	{
	  tla_vec_t *v = (tla_vec_t *) CAR (p);
	  tla_vec_sub (D->tmp[0], rho, *v);
	  if (tla_group_has (D->invV, D->tmp[0], NULL))
	    accepting = 1;
	}
      
      if (accepting)
	{
	  s_add_accepting_state (&pdata->p_cond, C, s);
	  s_add_accepting_state (D->unioncond, C, s);
	}
    }
}

			/* --------------- */

static P_data *
s_compute_acceptance_conditions (int m, sataf_msa *msa, btf_saffcomp *comp, 
				 tla_group_t invV, tla_vec_t rho_w, 
				 acond *unioncond)
{
  int p;
  struct visit_for_acc_data data;

  data.m = m;
  data.msa = msa;
  data.comp = comp;
  *data.invV = *invV;
  *data.rho_w = *rho_w;
  data.unioncond = unioncond;
  data.result = ccl_new_array (P_data, comp->nb_patterns);
  tla_vec_init (data.tmp[0], m);
  tla_vec_init (data.tmp[1], m);
  for (p = 0; p < comp->nb_patterns; p++)
    {
      ccl_pair *ppv;
      P_data *pdata = &data.result[p];

      pdata->pattern_vectors = s_compute_pattern_vectors (comp, p);
      for (ppv = FIRST (pdata->pattern_vectors); ppv; ppv = CDR (ppv))
	{
	  tla_vec_t *v = (tla_vec_t *) CAR (ppv);
	  tla_vec_add (*v, *v, rho_w);
	}

      s_init_acond (&pdata->p_cond);
    }

  btf_misc_visit_all_states (m, msa, s_visit_for_acc, &data);

  for (p = 0; p < comp->nb_patterns; p++)
    {
      ccl_pair *ppv;
      P_data *pdata = &data.result[p];

      for (ppv = FIRST (pdata->pattern_vectors); ppv; ppv = CDR (ppv))
	{
	  tla_vec_t *v = (tla_vec_t *) CAR (ppv);
	  tla_vec_sub (*v, *v, rho_w);
	}
    }

  tla_vec_clear (data.tmp[0]);
  tla_vec_clear (data.tmp[1]);

  return data.result;
}

			/* --------------- */

static void
s_init_acond (acond *acc)
{
  acc->final_states = ccl_list_create ();
  acc->cond = 
    ccl_hash_create (NULL, NULL, (ccl_delete_proc *) sataf_sa_del_reference,
		     (ccl_delete_proc *) ccl_bittable_delete); 
}

			/* --------------- */

static void
s_clear_acond (acond *acc)
{
  if (acc->final_states != NULL)
    ccl_list_clear_and_delete (acc->final_states, (ccl_delete_proc *)
			       sataf_msa_del_reference);
  ccl_zdelete (ccl_hash_delete, acc->cond);
}


			/* --------------- */

static void
s_add_accepting_state (acond *acc, sataf_sa *sa, int s)
{
  ccl_bittable *final;

  if (ccl_hash_find (acc->cond, sa))
    final = ccl_hash_get (acc->cond);
  else
    {
      int nb_states = sataf_ea_get_nb_states (sa->automaton);
      
      final = ccl_bittable_create (nb_states);
      ccl_hash_insert (acc->cond, final);
      sataf_sa_add_reference (sa);
    }

  ccl_bittable_set (final, s);
  if (acc->final_states != NULL)
    {
      sataf_msa *msa = sataf_msa_find_or_add (sa, s);
      ccl_list_add (acc->final_states, msa);
    }
}

			/* --------------- */

static P_formula_record *
s_new_formula_record (armoise_tree *def, ccl_list *final_states, 
		      armoise_tree *dep0, armoise_tree *dep1, 
		      armoise_tree *dep2, armoise_tree *dep3, 
		      P_formula_record *next)
{
  P_formula_record *result = ccl_new (P_formula_record);
	      
  result->final_states = final_states;
  result->in_comb = 0;
  result->def = def;
  result->deps[0] = dep0;
  result->deps[1] = dep1;
  result->deps[2] = dep2; 
  result->deps[3] = dep3; 
  result->next = next;

  return result;
}

			/* --------------- */

static int
s_compute_alpha (tla_vsp_t V, tla_vsp_t B, tla_vec_t alpha)
{
  int i, m, n;
  tla_matrix_t alphas;

  if (B->dim != V->dim-1)
    return 0;

  if (! tla_vsp_is_included_in (B, V))
    return 0;

  tla_vec_set_zero (alpha);
  tla_vsp_to_alphas (B, alphas);
  m = tla_matrix_get_nb_lines (alphas);
  n = tla_matrix_get_nb_columns (alphas);

  for (i = 0; i < V->dim; i++)
    {
      tla_matrix_mul_vector (alpha, alphas, V->gen[i].v);
      
      if (! tla_vec_is_zero (alpha))
	break;
    }

  ccl_assert (i < V->dim);
  for (i = 0; i < m; i++)
    {
      if (mpz_sgn (alpha->num[i]) != 0)
	{
	  int k;
	  for (k = 0; k < n; k++)
	    mpz_set (alpha->num[k], MM (alphas, i+1, k+1));
	  mpz_set_ui (alpha->den, 1);
	  break;
	}
    }
  tla_matrix_clear (alphas);

  return 1;
}

			/* --------------- */

static tla_vec_t *
s_compute_alphas (btf_saffcomp *comp, int nb_bounds,  btf_bound *bounds, 
		  int *p_nb_alphas)
{
  int i;
  int nb_alphas;
  btf_bound *comp_bound;
  tla_vec_t *result;
  int is_presburger = 1;

  for (i = 0; i < nb_bounds && ! tla_vsp_are_equal (bounds[i].V, comp->V); i++)
    CCL_NOP ();
  ccl_assert (i < nb_bounds);

  comp_bound = bounds + i;
  nb_alphas = comp_bound->B->size;  
  result = ccl_new_array (tla_vec_t, nb_alphas);

  for (i = 0; i < nb_alphas && is_presburger; i++)
    {
      tla_vec_init (result[i], comp->m);
      if (! s_compute_alpha (comp->V, comp_bound->B->V[i], result[i]))
	break;
    }

  if (i < nb_alphas)
    {
      do
	{
	  tla_vec_clear (result[i]);
	}
      while (--i);
      ccl_delete (result);
      result = NULL;
    }
  else
    {
      *p_nb_alphas = nb_alphas;
    }
  return result;
}

			/* --------------- */

static ccl_list *
s_build_definitions (struct msa2form_compilation_data *cd,
		     armoise_tree **p_def_G, 
		     armoise_tree **p_def_V, 
		     armoise_tree ***p_def_P,
		     armoise_tree ***p_def_VH, 
		     int *op, char **str_op, int nb_op,
		     tla_vsp_t V, tla_group_t invV, tla_vec_t rho_w,
		     P_data *pdata, int nb_patterns,
		     tla_vec_t *alphas, int nb_alphas, tla_vec_t xhi)
{
  int i;
  char tmp[100];
  ccl_ustring Gid;
  ccl_list *definitions = ccl_list_create ();

  *p_def_G = btf_form_crt_G_definition (cd, invV);
  Gid = btf_form_get_def_id (*p_def_G);

  *p_def_V = btf_form_crt_V_definition (cd, V);

  *p_def_P = ccl_new_array (armoise_tree *, nb_patterns);
  for (i = 0; i < nb_patterns; i++)
    {      
      sprintf (tmp, "P%d", i);
      (*p_def_P)[i] = btf_form_crt_P_definition (cd, tmp, rho_w,
						 pdata[i].pattern_vectors, invV);
      ccl_list_add (definitions, (*p_def_P)[i]);
    }

  *p_def_VH = ccl_new_array (armoise_tree *, nb_alphas);

  if (ccl_debug_is_on)
    {
      armoise_tree_log (CCL_LOG_DEBUG, *p_def_G);
      armoise_tree_log (CCL_LOG_DEBUG, *p_def_V);
    }
	  
  return definitions;
}

			/* --------------- */

