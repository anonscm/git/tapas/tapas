/*
 * linear-constraint-automaton.c -- MAO encoding a linear constraint
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-string.h>
#include "prestaf-automata.h"

typedef struct lca_st
{
  sataf_mao super;
  int width;
  int op;
  int *alpha;
  int factor_index;
  int value;
} lca;

			/* --------------- */

static size_t
s_lca_size (sataf_mao *self);

static void
s_lca_destroy (sataf_mao *self);

#define s_lca_no_cache NULL

#define s_lca_is_root_of_scc NULL

#define s_lca_simplify NULL

static uint32_t
s_lca_get_alphabet_size (sataf_mao *self);

static char *
s_lca_to_string (sataf_mao *self);

static sataf_mao * 
s_lca_succ (sataf_mao *self, uint32_t letter);

static int
s_lca_is_final (sataf_mao *self);

static int
s_lca_is_equal_to (sataf_mao *self, sataf_mao *other);

static unsigned int
s_lca_hashcode (sataf_mao *self);

#define s_lca_to_dot NULL

			/* --------------- */

static sataf_mao * 
s_crt_linear_operation (int *a, int asize, int factor_index, int op, 
			int value);

			/* --------------- */

static const int PRIME_TABLE[] = {
  1, 3, 7, 17, 37, 79, 163, 331, 673, 1361,
  2729, 5471, 10949, 21911, 43853, 87719, 175447, 350899, 701819, 1403641,
  2807303, 5614657, 11229331
};

static const size_t PRIME_TABLE_SIZE =
  (sizeof (PRIME_TABLE) / sizeof (PRIME_TABLE[0]));

			/* --------------- */

static const sataf_mao_methods LCA_METHODS = 
  {
    "LCA",
    s_lca_size,
    s_lca_destroy,
    s_lca_no_cache,
    s_lca_is_root_of_scc,
    s_lca_simplify,
    s_lca_get_alphabet_size,
    s_lca_to_string,
    s_lca_succ,
    s_lca_is_final,
    s_lca_is_equal_to,
    s_lca_hashcode,
    s_lca_to_dot
  };

			/* --------------- */

sataf_mao * 
prestaf_crt_linear_constraint_automaton (int type, const int *alpha, 
					 int alpha_size, int b)
{
  int *tab = ccl_new_array (int, alpha_size + 1);
  *tab = 0;
  ccl_memcpy (tab + 1, alpha, sizeof (int) * alpha_size);

  return s_crt_linear_operation (tab + 1, alpha_size, 0, type, -b);
}

			/* --------------- */

static size_t
s_lca_size (sataf_mao *self)
{
  return sizeof (lca);
}

			/* --------------- */

static void
s_lca_destroy (sataf_mao *self)
{
  lca *a = (lca *) self;

  if (--a->alpha[-1] == 0)
    ccl_delete (&(a->alpha[-1]));
}

			/* --------------- */

static uint32_t
s_lca_get_alphabet_size (sataf_mao *self)
{
  return 2;
}

			/* --------------- */

static char *
s_lca_to_string (sataf_mao *self)
{
  int i;
  lca *a = (lca *) self;
  char *result = NULL;
  char **s = &result;
  int *coeff = a->alpha;

  for (i = 0; i < a->width; i++)
    {
      if (coeff[i] > 0)
	{
	  char *sign;
	  if (i == 0)
	    sign = "";
	  else
	    sign = "+";

	  if (coeff[i] == 1)
	    ccl_string_format_append (s, "%sx%d", sign, i);
	  else
	    ccl_string_format_append (s, "%s%d*x%d", sign, coeff[i], i);
	}
      else if (coeff[i] < 0)
	{
	  if (coeff[i] == -1)
	    ccl_string_format_append (s, "-x%d", i);
	  else
	    ccl_string_format_append (s, "%d*x%d", coeff[i], i);
	}
    }
  {
    const char *op = "NULL";

    if (a->op == LCA_EQ)
      op = "=";
    else if (a->op == LCA_LT)
      op = "<";
    else if (a->op == LCA_LEQ)
      op = "<=";
    else if (a->op == LCA_GT)
      op = ">";
    else if (a->op == LCA_GEQ)
      op = ">=";

    ccl_string_format_append (s, " %s %d", op, a->value);
  }

  return result;
}

			/* --------------- */

static sataf_mao * 
s_lca_succ (sataf_mao *self, uint32_t letter)
{
  lca *a = (lca *) self;
  int32_t value = a->value;
  int factor_index = a->factor_index;
  sataf_mao *R;

  if (letter == 1)
    value += a->alpha[factor_index];

  if (factor_index + 1 < a->width)
    R = s_crt_linear_operation (a->alpha, a->width, factor_index + 1, a->op,
				value);
  else if ((value % 2) == 0)
    R = s_crt_linear_operation (a->alpha, a->width, 0, a->op, value / 2);
  else if (a->op == LCA_EQ)
    R = sataf_mao_create_zero (2);
  else if (a->op == LCA_GT || a->op == LCA_GEQ)
    R = s_crt_linear_operation (a->alpha, a->width, 0, LCA_GEQ, 
				(value - 1) / 2);
  else
    R = s_crt_linear_operation (a->alpha, a->width, 0, LCA_LEQ, 
				(value + 1) / 2);

  return R;
}

			/* --------------- */

static int
s_lca_is_final (sataf_mao *self)
{
  lca *a = (lca *) self;

  switch (a->op)
    {
    case LCA_EQ:
      return (a->value == 0);
    case LCA_LT:
      return (a->value < 0);
    case LCA_LEQ:
      return (a->value <= 0);
    case LCA_GT:
      return (a->value > 0);
    default:
      ccl_assert (a->op == LCA_GEQ);
      return (a->value >= 0);
    }
}

			/* --------------- */

static int
s_lca_is_equal_to (sataf_mao *self, sataf_mao *other)
{
  lca *a1 = (lca *) self;
  lca *a2 = (lca *) other;

  return a1->width == a2->width
    && a1->value == a2->value
    && a1->op == a2->op
    && a1->factor_index == a2->factor_index
    && memcmp (a1->alpha, a2->alpha, sizeof (int) * a1->width) == 0;
}

			/* --------------- */


static unsigned int
s_lca_hashcode (sataf_mao *self)
{
  lca *a = (lca *) self;
  uint32_t i = a->width;
  unsigned int result = 1361 * a->width + 350899 * (uint32_t) a->factor_index
    + 5614657 * a->value + 33 * a->op;

  while (i--)
    result = 21911 * result + a->alpha[i] * PRIME_TABLE[i % PRIME_TABLE_SIZE];

  return result;
}

			/* --------------- */

static sataf_mao * 
s_crt_linear_operation (int *a, int asize, int factor_index, int op,
			int value)
{
  lca *R = (lca *) sataf_mao_create (sizeof (lca), &LCA_METHODS);

  R->width = asize;
  R->alpha = a;
  R->op = op;
  a[-1]++;
  R->factor_index = factor_index;
  R->value = value;

  return (sataf_mao *) R;
}

			/* --------------- */

