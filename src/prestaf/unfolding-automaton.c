/*
 * unfolding-automaton.c -- add a comment about this file
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-string.h>
#include "prestaf-automata.h"

typedef struct unfold_st
{
  sataf_mao super;
  sataf_msa *msa;
  uint16_t i;
  uint16_t m;
} unfold;

			/* --------------- */

static size_t
s_unfold_size (sataf_mao *self);

static void
s_unfold_destroy (sataf_mao *self);

#define s_unfold_no_cache NULL

#define s_unfold_is_root_of_scc NULL

#define s_unfold_simplify NULL

static uint32_t
s_unfold_get_alphabet_size (sataf_mao *self);

#define s_unfold_to_string NULL

static sataf_mao * 
s_unfold_succ (sataf_mao *self, uint32_t letter);

static int
s_unfold_is_final (sataf_mao *self);

static int
s_unfold_is_equal_to (sataf_mao *self, sataf_mao *other);

static unsigned int
s_unfold_hashcode (sataf_mao *self);

static void
s_unfold_to_dot (ccl_log_type log, sataf_mao *self, const char **sigma,
		 const char *graph_name, const char *graph_type);

			/* --------------- */

static sataf_mao * 
s_new_unfold (uint16_t i, uint16_t m, sataf_msa *msa);

			/* --------------- */

static const sataf_mao_methods UNFOLD_METHODS = 
  {
    "SA-UNFOLD",
    s_unfold_size,
    s_unfold_destroy,
    s_unfold_no_cache,
    s_unfold_is_root_of_scc,
    s_unfold_simplify,
    s_unfold_get_alphabet_size,
    s_unfold_to_string,
    s_unfold_succ,
    s_unfold_is_final,
    s_unfold_is_equal_to,
    s_unfold_hashcode,
    s_unfold_to_dot
  };

			/* --------------- */

sataf_mao * 
prestaf_crt_unfolding_automaton(int m,  sataf_msa *msa)
{
  sataf_mao *R = s_new_unfold (0, m, msa);

  return R;
}

			/* --------------- */

static size_t
s_unfold_size (sataf_mao *self)
{
  return sizeof (unfold);
}

			/* --------------- */

static void
s_unfold_destroy (sataf_mao *self)
{
  unfold *a = (unfold *) self;

  sataf_msa_del_reference (a->msa);
}

			/* --------------- */

static uint32_t
s_unfold_get_alphabet_size (sataf_mao *self)
{
  unfold *a = (unfold *) self;

  return sataf_msa_get_alphabet_size (a->msa);
}

			/* --------------- */

static sataf_mao * 
s_unfold_succ (sataf_mao *self, uint32_t letter)
{
  unfold *a = (unfold *) self;
  sataf_msa *succ = sataf_msa_succ (a->msa, letter);
  sataf_mao *R = (sataf_mao *) s_new_unfold ((a->i + 1) % a->m, a->m, succ);
  sataf_msa_del_reference (succ);

  return R;
}

			/* --------------- */

static int
s_unfold_is_final (sataf_mao *self)
{
  unfold *a = (unfold *) self;

  return sataf_msa_is_final (a->msa) && a->i == 0;
}

			/* --------------- */

static int
s_unfold_is_equal_to (sataf_mao *self, sataf_mao *other)
{
  unfold *a1 = (unfold *) self;
  unfold *a2 = (unfold *) other;

  return a1->i == a2->i && a1->m == a2->m && a1->msa == a2->msa;
}

			/* --------------- */

static unsigned int
s_unfold_hashcode (sataf_mao *self)
{
  unfold *a = (unfold *) self;

  return 13 * a->i + 37 * a->m + 91 * ((uintptr_t) a->msa);
}


			/* --------------- */

static sataf_mao * 
s_new_unfold (uint16_t i, uint16_t m, sataf_msa *msa)
{
  unfold *R = (unfold *) sataf_mao_create (sizeof (unfold), &UNFOLD_METHODS);

  R->msa = sataf_msa_add_reference (msa);
  R->i = i;
  R->m = m;

  return (sataf_mao *) R;
}

			/* --------------- */

typedef struct memo_st MEMO;
struct memo_st
{
  sataf_mao_memo super;
  int visited_and_id;
  ccl_list *letters;
};


static void
s_init_memo (sataf_mao_memo * m, void *data)
{
  MEMO *mptr = (MEMO *) m;
  unfold *U = (unfold *) mptr->super.A;
  uint32_t *idptr = (uint32_t *) data;

  if (U->i == 0)
    {
      mptr->visited_and_id = *idptr << 1;
      (*idptr)++;
    }
  else
    mptr->visited_and_id = 0;

  mptr->letters = ccl_list_create ();

}

			/* --------------- */


static void
s_delete_memo (sataf_mao_memo * m)
{
  MEMO *mptr = (MEMO *) m;
  ccl_list_delete (mptr->letters);
}

			/* --------------- */

static void
s_unfold_to_dot (ccl_log_type log, sataf_mao *self, const char **sigma,
		       const char *graph_name, const char *graph_type)
{
  MEMO *m;
  uint32_t state_id;
  sataf_mao_memorizer *M;
  ccl_list *todo = ccl_list_create();

  if (graph_name == NULL)
    ccl_log (log, "%s sataf_mao_%p {\n", graph_type, self);
  else
    ccl_log (log, "%s %s {\n", graph_type, graph_name);

  state_id = 0;
  M = sataf_mao_memorizer_create (sizeof (MEMO), s_init_memo, &state_id, NULL);
  m = (MEMO *) sataf_mao_memorizer_remind (M, self);
  m->visited_and_id |= 1;
  ccl_list_add (todo, m);

  while (! ccl_list_is_empty (todo))
    {
      int b;
      uint32_t label;
      unfold *U;
      MEMO *succ[2];

      m = (MEMO *) ccl_list_take_first (todo);
      U = (unfold *) m->super.A;

      label = m->visited_and_id >> 1;

      if (U->i == 0)
	{
	  if (self->methods->to_string != NULL)
	    {
	      char *s = sataf_mao_to_string (m->super.A);
	      ccl_log (log, "N%p[shape=circle,label=\"%s\"", m, s);
	      ccl_delete (s);
	    }
	  else
	    {
	      ccl_log (log, "N%p[shape=circle,label=\"%d\"", m,
		       label);
	    }
	  if (m->super.A == self)
	    ccl_log (log, ",style=filled,fillcolor=grey");
	  if (sataf_mao_is_final (m->super.A))
	    ccl_log (log, ",peripheries=2");
	}
      else
	{
	  ccl_log (log, "N%p[shape=box,label=\"\",width=0.1,height=0.1,"
		   "fixedsize=true", m);
	}

      ccl_log (log, "]\n");


      for (b = 0; b < 2; b++)
	succ[b] = (MEMO *) sataf_mao_memorizer_succ (M, m->super.A, b);

      if (succ[0] == succ[1])
	{
	  ccl_log (log, "N%p->N%p[label=\"0,1\"]\n", m, succ[0]);
	  if ((succ[0]->visited_and_id & 0x1) == 0)
	    {
	      /* not visited and not in the queue */
	      succ[0]->visited_and_id |= 1;
	      ccl_list_add (todo, succ[0]);
	    }
	}
      else
	{
	  for (b = 0; b < 2; b++)
	    {
	      ccl_log (log, "N%p->N%p[label=\"%d\"]\n", m, succ[b], b);
	      if ((succ[b]->visited_and_id & 0x1) == 0)
		{
		  /* not visited and not in the queue */
		  succ[b]->visited_and_id |= 1;
		  ccl_list_add (todo, succ[b]);
		}
	    }
	}
    }
  ccl_log (log, "}\n");

  sataf_mao_memorizer_delete (M);
  ccl_list_delete (todo);
}

			/* --------------- */

static void
s_make_dfs (MEMO *s, sataf_mao_memorizer *M, ccl_list *todo, char *letter,
	    ccl_list *successors)
{
  MEMO *succ[2];
  unfold *Us = (unfold *) s->super.A;

  succ[0] = (MEMO *) sataf_mao_memorizer_succ (M, s->super.A, 0);
  succ[1] = (MEMO *) sataf_mao_memorizer_succ (M, s->super.A, 1);
  
  if (succ[0] == succ[1])
    {
      unfold *Usucc = (unfold *) succ[0]->super.A;

      letter[Us->i] = '*';
      if (Usucc->i == 0)
	{
	  if ((succ[0]->visited_and_id & 0x1) == 0)
	    {
	      succ[0]->visited_and_id |= 1;
	      ccl_list_add (todo, succ[0]);
	    }
	  if (! ccl_list_has (successors, succ[0]))
	    ccl_list_add (successors, succ[0]);
	  ccl_list_add (succ[0]->letters, ccl_string_dup (letter));
	}
      else 
	{
	  s_make_dfs (succ[0], M, todo, letter, successors);
	}
    }
  else
    {
      int b;

      for (b = 0; b < 2; b++)
	{
	  unfold *Usucc = (unfold *) succ[b]->super.A;

	  letter[Us->i] = b?'1':'0';
	  if (Usucc->i == 0)
	    {
	      if ((succ[b]->visited_and_id & 0x1) == 0)
		{
		  succ[b]->visited_and_id |= 1;
		  ccl_list_add (todo, succ[b]);
		}
	      if (! ccl_list_has (successors, succ[b]))
		ccl_list_add (successors, succ[b]);
	      ccl_list_add (succ[b]->letters, ccl_string_dup (letter));
	    }
	  else
	    {
	      s_make_dfs (succ[b], M, todo, letter, successors);
	    }
	}
    }
}

			/* --------------- */

void
prestaf_unfold_to_dot (ccl_log_type log, sataf_mao *self, const char **sigma,
		       const char *graph_name, const char *graph_type)
{
  uint32_t state_id = 0;
  sataf_mao_memorizer *M = 
    sataf_mao_memorizer_create (sizeof (MEMO), s_init_memo, &state_id, 
				s_delete_memo);
  ccl_list *todo = ccl_list_create ();
  MEMO *q = (MEMO *) sataf_mao_memorizer_remind (M, self);
  unfold *U = (unfold *) self;
  int m = U->m;
  char *letter = ccl_new_array (char, U->m+1);
  ccl_list *successors = ccl_list_create ();

  q->visited_and_id |= 1;
  ccl_list_add (todo, q);

  if (graph_name == NULL)
    ccl_log (log, "%s sataf_mao_%p {\n", graph_type, self);
  else
    ccl_log (log, "%s %s {\n", graph_type, graph_name);

  while (! ccl_list_is_empty (todo))
    {
      q = (MEMO *)ccl_list_take_first (todo);

      ccl_assert (q->visited_and_id >= 0);

      ccl_log (log, "N%p[label=\"%d\"", q->super.A, q->visited_and_id >> 1);

      if (q->super.A == self)
	ccl_log (log, ",style=filled,fillcolor=grey");
      if (sataf_mao_is_final (q->super.A))
	ccl_log (log, ",peripheries=2");
      ccl_log (log, "]\n");

      s_make_dfs (q, M, todo, letter, successors);
      while (! ccl_list_is_empty (successors))
	{
	  int i;
	  MEMO *qp = (MEMO *) ccl_list_take_first (successors);

	  ccl_log (log, "N%p -> N%p[label=\"", q->super.A, qp->super.A);

	  for (i = 0; i < m; i++)
	    {
	      ccl_pair *p;

	      for (p = FIRST(qp->letters); p; p = CDR(p))
		{
		  char *l = (char *) CAR(p);

		  ccl_log (log, "%c", l[i]);
		  if (CDR(p) == NULL) 
		    ccl_log (log, "\\n");
		  else
		    ccl_log (log, " ");
		}
	    }
	  ccl_list_clear (qp->letters, ccl_string_delete);
	  ccl_log (log, "\"]\n");
	}
    }
  ccl_log (log, "}\n");

  ccl_list_delete (successors);
  ccl_list_delete (todo);
  sataf_mao_memorizer_delete (M);
  ccl_delete (letter);
}
