/*
 * add-variable-automaton.c -- Add one column to a MSA-encoded relation
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include "prestaf-automata.h"

typedef struct addvar_st
{
  sataf_mao super;
  uint32_t i;
  uint32_t n;
  sataf_msa *msa;
} addvar;

			/* --------------- */

static size_t
s_addvar_size (sataf_mao *self);

static void
s_addvar_destroy (sataf_mao *self);

#define s_addvar_no_cache NULL

#define s_addvar_is_root_of_scc NULL

#define s_addvar_simplify NULL

static uint32_t
s_addvar_get_alphabet_size (sataf_mao *self);

#define s_addvar_to_string NULL

static sataf_mao * 
s_addvar_succ (sataf_mao *self, uint32_t letter);

static int
s_addvar_is_final (sataf_mao *self);

static int
s_addvar_is_equal_to (sataf_mao *self, sataf_mao *other);

static unsigned int
s_addvar_hashcode (sataf_mao *self);

#define s_addvar_to_dot NULL

			/* --------------- */

static sataf_mao * 
s_new_addvar (sataf_msa *msa, uint32_t i, uint32_t n);

			/* --------------- */

static const sataf_mao_methods ADDVAR_METHODS = 
  {
  "SA-ADDVAR",
  s_addvar_size,
  s_addvar_destroy,
  s_addvar_no_cache,
  s_addvar_is_root_of_scc,
  s_addvar_simplify,
  s_addvar_get_alphabet_size,
  s_addvar_to_string,
  s_addvar_succ,
  s_addvar_is_final,
  s_addvar_is_equal_to,
  s_addvar_hashcode,
  s_addvar_to_dot
};

			/* --------------- */

sataf_mao * 
prestaf_crt_add_variable_automaton (sataf_msa *msa, uint32_t i, uint32_t n)
{
  sataf_mao *R;

  if (sataf_msa_is_zero (msa) || sataf_msa_is_one (msa))
    R = sataf_msa_to_mao (msa);
  else
    R = s_new_addvar (msa, i, n);

  return R;
}

			/* --------------- */

static size_t
s_addvar_size (sataf_mao *self)
{
  return sizeof (addvar);
}

			/* --------------- */

static void
s_addvar_destroy (sataf_mao *self)
{
  addvar *a = (addvar *) self;

  sataf_msa_del_reference (a->msa);
}

			/* --------------- */

static uint32_t
s_addvar_get_alphabet_size (sataf_mao *self)
{
  addvar *a = (addvar *) self;

  return sataf_msa_get_alphabet_size (a->msa);
}

			/* --------------- */

static sataf_mao * 
s_addvar_succ (sataf_mao *self, uint32_t letter)
{
  sataf_mao *R;
  addvar *a = (addvar *) self;

  if (a->i == 0)
    R = s_new_addvar (a->msa, a->n, a->n);
  else
    {
      sataf_msa *succ = sataf_msa_succ (a->msa, letter);
      R = (sataf_mao *) s_new_addvar (succ, a->i - 1, a->n);
      sataf_msa_del_reference (succ);
    }

  return R;
}

			/* --------------- */

static int
s_addvar_is_final (sataf_mao *self)
{
  addvar *a = (addvar *) self;

  return sataf_msa_is_final (a->msa);
}

			/* --------------- */

static int
s_addvar_is_equal_to (sataf_mao *self, sataf_mao *other)
{
  addvar *a1 = (addvar *) self;
  addvar *a2 = (addvar *) other;

  return a1->i == a2->i && a1->n == a2->n && a1->msa == a2->msa;
}

			/* --------------- */

static unsigned int
s_addvar_hashcode (sataf_mao *self)
{
  addvar *a = (addvar *) self;

  return 13 * a->i + 37 * a->n + 91 * ((uintptr_t) a->msa);
}


			/* --------------- */

static sataf_mao * 
s_new_addvar (sataf_msa *msa, uint32_t i, uint32_t n)
{
  addvar *R = (addvar *) sataf_mao_create (sizeof (addvar), &ADDVAR_METHODS);

  R->msa = sataf_msa_add_reference (msa);
  R->i = i;
  R->n = n;

  return (sataf_mao *) R;
}

			/* --------------- */
