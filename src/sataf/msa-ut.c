/*
 * msa-ut.c -- Unicity Table for MSAs
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-pool.h>
#include "sataf-p.h"

#define LAZY_DEL_REF 1

#define MSA_POOL_PAGE_SIZE 1000

typedef struct msa_manager_st msa_manager;
struct msa_manager_st
{
  uint32_t init_table_size;
  uint32_t fill_degree;
  uint32_t table_size;
  sataf_msa **table;
  uint32_t nb_elements;
  ccl_pool *pool;
  sataf_msa *current;
};

			/* --------------- */


#define HVALUE MSA_HVALUE

#define MUST_INCREASE_TABLE(m) \
  ((m)->nb_elements > (m)->table_size * (m)->fill_degree)

#define MUST_DECREASE_TABLE(m) \
  ((m)->nb_elements < (m)->table_size * (m)->fill_degree && \
   (m)->table_size > (m)->init_table_size)

			/* --------------- */

static void
s_msa_manager_resize_table (uint32_t new_size);

			/* --------------- */

static msa_manager *MSA_MANAGER = NULL;

			/* --------------- */


int
sataf_msa_unique_table_init (int ut_table_size, int ut_max_fill_degree)
{
  MSA_MANAGER = ccl_new (msa_manager);

  MSA_MANAGER->init_table_size = ut_table_size;
  MSA_MANAGER->fill_degree = ut_max_fill_degree;
  MSA_MANAGER->table_size = ut_table_size;
  MSA_MANAGER->table = ccl_new_array (sataf_msa *, MSA_MANAGER->table_size);
  MSA_MANAGER->nb_elements = 0;
  MSA_MANAGER->pool =
    ccl_pool_create ("MSA Pool", sizeof (sataf_msa), MSA_POOL_PAGE_SIZE);
  MSA_MANAGER->current = NULL;

  return 1;
}

			/* --------------- */

void
sataf_msa_unique_table_terminate (void)
{
  if (LAZY_DEL_REF)
    {
      int count = MSA_MANAGER->nb_elements;

      do
	{
	  int i = MSA_MANAGER->table_size;

	  while (i--)
	    {
	      sataf_msa *msa;
	      sataf_msa *next;

	      for (msa = MSA_MANAGER->table[i]; msa; msa = next)
		{
		  next = msa->next;
		  if (msa->A->refcount)
		    sataf_sa_del_reference (msa->A);
		}
	    }

	  if (MSA_MANAGER->nb_elements < count)
	    count = MSA_MANAGER->nb_elements;
	  else if (count > 0)
	    {
	      ccl_error("memory leak in MSA uniqueness table");
	      abort();
	    }
	}
      while (MSA_MANAGER->nb_elements != 0);
    }
  else
    {
      ccl_pre (MSA_MANAGER->nb_elements == 0);
    }

  ccl_delete (MSA_MANAGER->table);
  ccl_pool_delete (MSA_MANAGER->pool);
  ccl_delete (MSA_MANAGER);
}

			/* --------------- */

void
sataf_msa_destroy (sataf_msa *msa)
{
  if (msa == MSA_MANAGER->current)
    MSA_MANAGER->current = NULL;

  if (!LAZY_DEL_REF)
    {
      sataf_msa **pmsa;
      uint32_t index = HVALUE (msa->A, msa->initial) % MSA_MANAGER->table_size;

      for (pmsa = MSA_MANAGER->table + index; *pmsa && *pmsa != msa;
	   pmsa = &((*pmsa)->next))
	/* do nothing */ ;

      ccl_assert (*pmsa != NULL);
      *pmsa = msa->next;
      sataf_sa_del_reference (msa->A);

      ccl_pool_release (MSA_MANAGER->pool, msa);
    }

  MSA_MANAGER->nb_elements--;

  if (0 && MUST_DECREASE_TABLE (MSA_MANAGER))
    s_msa_manager_resize_table ((MSA_MANAGER->table_size - 13) >> 1);
}

			/* --------------- */

sataf_msa * 
sataf_msa_find_or_add (sataf_sa *sa, uint32_t initial)
{
  sataf_msa *msa;
  uint32_t index;
  sataf_msa **pmsa;

  if (MSA_MANAGER->current != NULL && MSA_MANAGER->current->A == sa &&
      MSA_MANAGER->current->initial == initial)
    {
      ccl_assert (MSA_MANAGER->current->refcount > 0);
      return sataf_msa_add_reference (MSA_MANAGER->current);
    }

  index = HVALUE (sa, initial) % MSA_MANAGER->table_size;
  pmsa = MSA_MANAGER->table + index;

  while (*pmsa)
    {
      msa = *pmsa;

      if (LAZY_DEL_REF && msa->refcount == 0)
	{
	  sataf_sa_del_reference (msa->A);
	  *pmsa = msa->next;
	  ccl_pool_release (MSA_MANAGER->pool, msa);
	}
      else
	{
	  ccl_assert (msa->refcount > 0);

	  if (msa->A == sa && msa->initial == initial)
	    {
	      return sataf_msa_add_reference (msa);
	    }
	  else
	    {
	      pmsa = &(msa->next);
	    }
	}
    }

  msa = (sataf_msa *) ccl_pool_alloc (MSA_MANAGER->pool);
  msa->next = NULL;
  msa->refcount = 1;
  msa->A = sataf_sa_add_reference (sa);
  msa->initial = initial;

  *pmsa = msa;

  MSA_MANAGER->nb_elements++;

  if (MUST_INCREASE_TABLE (MSA_MANAGER))
    s_msa_manager_resize_table ((MSA_MANAGER->table_size << 1) + 13);

  MSA_MANAGER->current = msa;

  return msa;
}

			/* --------------- */

void
sataf_msa_ut_statistics (ccl_log_type log)
{
  int i;
  uint32_t nb_queue = 0;
  uint32_t nb_refcount = 0;
  uint32_t min_refcount = 0xFFFFFFFF;
  uint32_t max_refcount = 0;

  for (i = 0; i < MSA_MANAGER->table_size; i++)
    {
      sataf_msa *msa = MSA_MANAGER->table[i];

      if (msa == NULL)
	continue;

      nb_queue++;
      for (; msa != NULL; msa = msa->next)
	{
	  nb_refcount += msa->refcount;
	  if (msa->refcount < min_refcount)
	    min_refcount = msa->refcount;
	  if (msa->refcount > max_refcount)
	    max_refcount = msa->refcount;
	}
    }

  ccl_log (log, "MSA uniqueness table statistics :\n");
  ccl_log (log, "---------------------------------\n");
  ccl_log (log, "init size = %d\n", MSA_MANAGER->init_table_size);
  ccl_log (log, "fill degree = %d\n", MSA_MANAGER->fill_degree);
  ccl_log (log, "table size = %d\n", MSA_MANAGER->table_size);
  ccl_log (log, "nb entries = %d\n", MSA_MANAGER->nb_elements);
  ccl_log (log, "mean collision list len = %4.2f\n",
	   (float) MSA_MANAGER->nb_elements / (float) nb_queue);
  ccl_log (log, "mean sharing count = %4.2f\n",
	   (float) nb_refcount / (float) MSA_MANAGER->nb_elements);
  ccl_log (log, "refcount range = [%d, %d]\n", min_refcount, max_refcount);
  ccl_log (log, "\n");
}

			/* --------------- */

static void
s_msa_manager_resize_table (uint32_t new_size)
{
  uint32_t i;
  sataf_msa **new_table = ccl_new_array (sataf_msa *, new_size);
  sataf_msa *msa;
  sataf_msa *next;
  uint32_t index;

  if (new_table == NULL)
    return;

  for (i = 0; i < MSA_MANAGER->table_size; i++)
    {
      for (msa = MSA_MANAGER->table[i]; msa; msa = next)
	{
	  next = msa->next;
	  if (LAZY_DEL_REF && msa->refcount == 0)
	    {
	      sataf_sa_del_reference (msa->A);
	      ccl_pool_release (MSA_MANAGER->pool, msa);
	    }
	  else
	    {
	      ccl_assert (msa->refcount > 0);
	      index = HVALUE (msa->A, msa->initial) % new_size;
	      msa->next = new_table[index];
	      new_table[index] = msa;
	    }
	}
    }
  MSA_MANAGER->table_size = new_size;
  ccl_delete (MSA_MANAGER->table);
  MSA_MANAGER->table = new_table;
}

			/* --------------- */
