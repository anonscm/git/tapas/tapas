/*
 * sa-homomorphism-checking.c -- add a comment about this file
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include "sataf-p.h"

# define H_UNDEF 0xFFFFFFFF

uint32_t
check_homomorphism (sataf_ea *ea, sataf_sa **bind_sa, uint32_t *bind_init, 
		    uint32_t src, uint32_t letter, uint32_t *stack, 
		    uint32_t *h)
{
  uint32_t q, initial, top, a, N, found_h;
  uint32_t p, u, du, v, dv, asize = ea->alphabet_size;
  sataf_sa *SA = bind_sa[0];
  sataf_ea *A = SA->automaton;

  N = A->nb_local_states;

  initial = sataf_ea_encode_succ_as_local_state (bind_init[0]);
  found_h = 0;

  for (q = 0; q < N && !found_h; q++)
    {
      if (sataf_ea_get_successor (A, q, letter) != initial ||
	  A->is_final[q] != ea->is_final[src])
	continue;

      memset (h, H_UNDEF, sizeof (uint32_t) * ea->nb_local_states);

      top = 0;
      found_h = 1;
      h[src] = q;
      stack[top++] = src;

      while (top > 0 && found_h)
	{
	  p = stack[--top];

	  for (a = 0; a < asize && found_h; a++)
	    {
	      u = sataf_ea_get_successor (ea, p, a);
	      du = sataf_ea_decode_succ_state (u);
	      v = sataf_ea_get_successor (A, h[p], a);
	      dv = sataf_ea_decode_succ_state (v);

	      if (sataf_ea_is_local_state (u))
		{
		  found_h = sataf_ea_is_local_state (v);
		  if (found_h)
		    {
		      if (h[du] == H_UNDEF &&
			  ea->is_final[du] == A->is_final[dv])
			{
			  h[du] = dv;
			  stack[top++] = du;
			}
		      else
			{
			  found_h = (h[du] == dv);
			}
		    }
		}
	      else if (sataf_ea_is_local_state (v))
		{
		  if (bind_init[du] != dv || bind_sa[du] != SA)
		    found_h = 0;
		}
	      else
		{
		  if (bind_sa[du] != SA->bind[dv]->A ||
		      bind_init[du] != SA->bind[dv]->initial)
		    found_h = 0;
		}
	    }
	}
    }

  return found_h;
}

			/* --------------- */

uint32_t
check_homomorphism1 (sataf_ea *ea, sataf_sa ** bind_sa, uint32_t *bind_init, 
		     uint32_t letter, uint32_t *stack, uint32_t *h)
{
  uint32_t q, initial, a, N, found_h;
  uint32_t u, du, v, dv, asize = ea->alphabet_size;
  sataf_sa *SA = bind_sa[0];
  sataf_ea *A = SA->automaton;

  ccl_pre (ea->nb_local_states == 1);

  N = A->nb_local_states;

  initial = sataf_ea_encode_succ_as_local_state (bind_init[0]);
  found_h = 0;

  for (q = 0; q < N && !found_h; q++)
    {
      if (sataf_ea_get_successor (A, q, letter) != initial ||
	  A->is_final[q] != ea->is_final[0])
	continue;

      found_h = 1;
      h[0] = q;

      for (a = 0; a < asize && found_h; a++)
	{
	  u = sataf_ea_get_successor (ea, 0, a);
	  du = sataf_ea_decode_succ_state (u);
	  v = sataf_ea_get_successor (A, q, a);
	  dv = sataf_ea_decode_succ_state (v);

	  if (sataf_ea_is_local_state (u))
	    {
	      found_h = sataf_ea_is_local_state (v) && (dv == q);
	    }
	  else if (sataf_ea_is_local_state (v))
	    {
	      if (bind_init[du] != dv || bind_sa[du] != SA)
		found_h = 0;
	    }
	  else
	    {
	      if (bind_sa[du] != SA->bind[dv]->A ||
		  bind_init[du] != SA->bind[dv]->initial)
		found_h = 0;
	    }
	}
    }

  return found_h;
}
