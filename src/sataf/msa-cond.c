/*
 * msa-cond.c -- Change acceptance condition of a shared automaton
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-bittable.h>
#include <ccl/ccl-memory.h>
#include "sataf-p.h"

struct cache_entry
{
  sataf_sa *sa;
  uint32_t *h;
};

			/* --------------- */

static sataf_ea *
s_change_ea_condition (const sataf_ea *ea, ccl_bittable *final_states);

static sataf_sa *
s_change_sa_acceptance (sataf_sa *a, uint32_t *p_initial, ccl_hash *cond,
			ccl_hash *cache);

static void
s_delete_cache_entry (void *p);

			/* --------------- */

sataf_msa *
sataf_msa_change_acceptance (sataf_msa *a, ccl_hash *cond)
{
  sataf_sa *sa;
  sataf_msa *result;
  uint32_t initial = a->initial;
  ccl_hash *cache =
    ccl_hash_create (NULL, NULL, (ccl_delete_proc *)sataf_sa_del_reference,
		     s_delete_cache_entry);

  sa = s_change_sa_acceptance (a->A, &initial, cond, cache);
  result = sataf_msa_find_or_add (sa, initial);
  sataf_sa_del_reference (sa);
  ccl_hash_delete (cache);

  return result;
}

			/* --------------- */

static sataf_sa *
s_change_acceptance_for_terminal (sataf_sa *a, uint32_t *p_initial, 
				  ccl_bittable *final, ccl_hash *cache)
{
  sataf_sa *result;
  struct cache_entry *e = ccl_new (struct cache_entry);
  uint32_t *h = ccl_new_array (uint32_t, a->automaton->nb_local_states);
  sataf_ea *ea = s_change_ea_condition (a->automaton, final);
  sataf_ea *minea = sataf_ea_minimize (ea, h);
  sataf_ea *uminea = sataf_ea_find_or_add (minea);

  result = sataf_sa_find_or_add (uminea, NULL, NULL);
  e->sa = sataf_sa_add_reference (result);
  e->h = h;

  a = sataf_sa_add_reference (a);
  ccl_hash_insert (cache, e);
  
  *p_initial = h[*p_initial];
  
  sataf_ea_del_reference (minea);
  sataf_ea_del_reference (uminea);
  sataf_ea_del_reference (ea);      

  return result;
}

			/* --------------- */

static int
s_compare_msa (const void *p1, const void *p2)
{
  int result;
  sataf_msa *a1 = (sataf_msa *) p1;
  sataf_msa *a2 = (sataf_msa *) p2;

  if (a1->A->depth == a2->A->depth)
    {
      if (a2->A == a1->A)
	{
	  ccl_assert (a2->initial != a1->initial);

	  result = (intptr_t) a2->initial - (intptr_t) a1->initial;
	}
      else
	{
	  result = (intptr_t) a2->A - (intptr_t) a1->A;
	}
    }
  else
    {
      result = (int32_t) a2->A->depth - (int32_t) a1->A->depth;
    }

  return result;
}

			/* --------------- */

static sataf_sa *
s_change_acceptance_for_non_terminal (sataf_sa *a, uint32_t *p_initial, 
				      ccl_bittable *final, ccl_hash *cond, 
				      ccl_hash *cache)
{
  int i;
  int homo_found;
  ccl_pair *p;
  sataf_ea *minea;
  sataf_sa *result;
  sataf_ea *ea;
  int new_nb_exits;
  int no_exit_change = 1;
  int nb_exits = a->automaton->nb_exit_states;
  ccl_list *new_exits = ccl_list_create ();
  int *change_exits = ccl_new_array (int, nb_exits);
  sataf_msa **binding = ccl_new_array (sataf_msa *, nb_exits);
  uint32_t *h = ccl_new_array (uint32_t, a->automaton->nb_local_states);
  sataf_sa **bind_sa;
  uint32_t *bind_init;

  for (i = 0; i < nb_exits; i++)
    {
      uint32_t initial = a->bind[i]->initial;
      sataf_sa *sa = 
	s_change_sa_acceptance (a->bind[i]->A, &initial, cond, cache);
      binding[i] = sataf_msa_find_or_add (sa, initial);
      if (! ccl_list_has (new_exits, binding[i]))
	ccl_list_add (new_exits, binding[i]);
      else
	sataf_msa_del_reference (binding[i]);
      sataf_sa_del_reference (sa);
    }

  ccl_list_sort (new_exits, s_compare_msa);
  new_nb_exits = ccl_list_get_size (new_exits);
  for (i = 0; i < nb_exits; i++)
    {
      change_exits[i] = ccl_list_get_index (new_exits, binding[i], NULL);
      no_exit_change = (change_exits[i] == i) && no_exit_change;
    }

  bind_sa = ccl_new_array (sataf_sa *, new_nb_exits);
  bind_init = ccl_new_array (uint32_t, new_nb_exits);
  for (i = 0, p = FIRST (new_exits); p; p = CDR(p), i++)
    {
      sataf_msa *msa = CAR (p);
      bind_sa[i] = sataf_sa_add_reference (msa->A);
      bind_init[i] = msa->initial;
      sataf_msa_del_reference (msa);
    }
  ccl_list_delete (new_exits);
  ccl_delete (binding);

  if (no_exit_change)
    ea = sataf_ea_add_reference (a->automaton);
  else
    {
      int i;
      int trans_rel_size = 
	a->automaton->alphabet_size * a->automaton->nb_local_states;
      uint32_t *successors = ccl_new_array (uint32_t, trans_rel_size);

      ccl_memcpy (successors, a->automaton->successor, 
		  (sizeof (uint32_t) * trans_rel_size));
      
      for (i = 0; i < trans_rel_size; i++)
	{
	  if (sataf_ea_is_exit_state (successors[i]))
	    {
	      uint32_t s = sataf_ea_decode_succ_state (successors[i]);
	      ccl_assert (change_exits[s] < new_nb_exits);
	      s = sataf_ea_encode_succ_as_exit_state (change_exits[s]);
	      successors[i] = s;
	    }
	}
      ea = sataf_ea_create_with_arrays (a->automaton->nb_local_states,
					new_nb_exits,
					a->automaton->alphabet_size,
					a->automaton->is_final,
					successors);
      ccl_delete (successors);
    }
  ccl_delete (change_exits);

  minea = s_change_ea_condition (ea, final);
  sataf_ea_del_reference (ea);
  ea = minea;

  {
    int i;
    int src = -1;
    int letter = -1;
    int trans_rel_size = ea->alphabet_size * ea->nb_local_states;
    uint32_t *stack = ccl_new_array (uint32_t, 2 * ea->nb_local_states);

    for (i = 0; i < trans_rel_size; i++)
      {
	if (sataf_ea_is_exit_state (ea->successor[i]) &&
	    sataf_ea_decode_succ_state (ea->successor[i]) == 0)
	  break;
      }

    ccl_assert (i < trans_rel_size);
    src = i / ea->alphabet_size;
    letter = i % ea->alphabet_size;
	
    homo_found = 
      check_homomorphism (ea, bind_sa, bind_init, src, letter, stack, h);
    ccl_delete (stack);
  }

  if (homo_found)
    {
      sataf_ea_del_reference (ea);
      result = sataf_sa_add_reference (bind_sa[0]);
      *p_initial = h[*p_initial];
    }
  else
    {    
      minea = sataf_ea_minimize (ea, h);
      sataf_ea_del_reference (ea);
      ea = sataf_ea_find_or_add (minea);
      sataf_ea_del_reference (minea);
      result = sataf_sa_find_or_add (ea, bind_sa, bind_init);
      *p_initial = h[*p_initial];
      sataf_ea_del_reference (ea);
    }

  {
    struct cache_entry *e = ccl_new (struct cache_entry);
    ccl_hash_find (cache, a);
    ccl_hash_insert (cache, e);
    a = sataf_sa_add_reference (a);
    e->sa = sataf_sa_add_reference (result);
    e->h = h;
  }
  
  for (i = 0; i < new_nb_exits; i++)
    sataf_sa_del_reference (bind_sa[i]);
  ccl_delete (bind_sa);
  ccl_delete (bind_init);

  return result;
}

			/* --------------- */

static sataf_sa *
s_change_sa_acceptance (sataf_sa *a, uint32_t *p_initial, ccl_hash *cond,
			ccl_hash *cache)
{
  sataf_sa *result;
  ccl_bittable *final = NULL;

  if (ccl_hash_find (cache, a))
    {
      struct cache_entry *e = ccl_hash_get (cache);
      result = sataf_sa_add_reference (e->sa);
      *p_initial = e->h[*p_initial];
    }
  else
    {
      if (ccl_hash_find (cond, a))
	final = (ccl_bittable *) ccl_hash_get (cond);
      
      if (a->depth > 0)
	result = s_change_acceptance_for_non_terminal (a, p_initial, final,
						       cond, cache);
      else
	result = s_change_acceptance_for_terminal (a, p_initial, final, cache);
    }

  return result;
}

			/* --------------- */

static sataf_ea *
s_change_ea_condition (const sataf_ea *ea, ccl_bittable *final_states)
{
  int i;
  sataf_ea *result;
  uint8_t *is_final = ccl_new_array (uint8_t, ea->nb_local_states);

  if (final_states != NULL)
    {
      for (i = ccl_bittable_get_first (final_states); 
	   i>= 0;
	   i = ccl_bittable_get_next (final_states, i))
	is_final[i] = 1;
    }

  result = 
    sataf_ea_create_with_arrays (ea->nb_local_states, ea->nb_exit_states,
				 ea->alphabet_size, is_final, ea->successor);

  ccl_delete (is_final);

  return result;
}

			/* --------------- */

static void
s_delete_cache_entry (void *p)
{
  struct cache_entry *e = p;

  sataf_sa_del_reference (e->sa);
  ccl_delete (e->h);
  ccl_delete (e);
}
