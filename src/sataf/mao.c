/*
 * mao.c -- Basic operations on Marked Automaton Objects
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-pool.h>
#include <ccl/ccl-string.h>
#include <ccl/ccl-memory.h>
#include "sataf.h"

# define MAX_SA_SIZE_DIV_2 100
# define SA_POOL_PAGE_SIZE 10000

typedef struct sea_automaton_st
{
  sataf_mao super;
  sataf_ea *ea;
  uint32_t s;
} sea_automaton;

typedef struct memo_st MEMO;
struct memo_st
{
  sataf_mao_memo super;
  int visited_and_id;
  MEMO *next;
};

			/* --------------- */

static size_t
s_sea_size (sataf_mao *self);

static void
s_sea_destroy (sataf_mao *self);

static uint32_t
s_sea_get_alphabet_size (sataf_mao *self);

#define s_sea_no_cache NULL

#define s_sea_is_root_of_scc NULL

#define s_sea_simplify NULL

static char *
s_sea_to_string (sataf_mao *self);

static sataf_mao * 
s_sea_succ (sataf_mao *self, uint32_t letter);

static int
s_sea_is_final (sataf_mao *self);

static int
s_sea_is_equal_to (sataf_mao *self, sataf_mao *other);

static unsigned int
s_sea_hashcode (sataf_mao *self);

# define s_sea_to_dot NULL

			/* --------------- */

static sataf_mao * 
s_create_from_exit_automaton (sataf_ea *ea, uint32_t s0);

static void *
s_allocate_automaton (uint32_t size);

static void
s_delete_automaton (sataf_mao *a);

			/* --------------- */

static const sataf_mao_methods SEA_METHODS = 
  {
    "SA-SEA",
    s_sea_size,
    s_sea_destroy,
    s_sea_no_cache,
    s_sea_is_root_of_scc,
    s_sea_simplify,
    s_sea_get_alphabet_size,
    s_sea_to_string,
    s_sea_succ,
    s_sea_is_final,
    s_sea_is_equal_to,
    s_sea_hashcode,
    s_sea_to_dot
  };

			/* --------------- */

static ccl_pool *POOLS[MAX_SA_SIZE_DIV_2];

			/* --------------- */

int
sataf_mao_init (void)
{
  int i;

  for (i = 0; i < MAX_SA_SIZE_DIV_2; i++)
    POOLS[i] = NULL;

  return 1;
}

			/* --------------- */

void
sataf_mao_terminate (void)
{
  int i;

  for (i = 0; i < MAX_SA_SIZE_DIV_2; i++)
    ccl_zdelete (ccl_pool_delete, POOLS[i]);
}

			/* --------------- */

sataf_mao * 
sataf_mao_create (size_t size, const sataf_mao_methods *methods)
{
  sataf_mao *result;

  ccl_pre (size >= sizeof (struct sataf_mao_st));
  ccl_pre (methods != NULL);

  if (methods->size == NULL)
    result = ccl_calloc (size, 1);
  else
    result = s_allocate_automaton (size);

  result->methods = methods;
  result->refcount = 1;

  return result;
}

			/* --------------- */

void
sataf_mao_destroy (sataf_mao *a)
{
  if (a->methods->destroy != NULL)
    a->methods->destroy (a);

  s_delete_automaton (a);
}

			/* --------------- */

char *
sataf_mao_to_string (sataf_mao *self)
{
  char *result;

  ccl_pre (self != NULL);

  if (self->methods->to_string == NULL)
    {
      result = 
	ccl_string_format_new ("%s@%p", sataf_mao_get_type (self), self);
    }
  else
    {
      result = self->methods->to_string (self);
    }

  return result;
}

			/* --------------- */

int
sataf_mao_no_cache (sataf_mao *self)
{
  ccl_pre (self != NULL);

  if (self->methods->no_cache == NULL)
    return 0;

  return self->methods->no_cache (self);
}

			/* --------------- */

void
sataf_mao_is_root_of_scc (sataf_mao *self, sataf_sa *sa, int q0)
{
  ccl_pre (self != NULL);

  if (self->methods->is_root_of_scc != NULL)
    self->methods->is_root_of_scc (self, sa, q0);
}

			/* --------------- */

sataf_msa *
sataf_mao_simplify (sataf_mao *self)
{
  struct sataf_msa_st *result;

  ccl_pre (self != NULL);

  if (self->methods->simplify == NULL)
    return NULL;

  result = self->methods->simplify (self);

  return result;
}

			/* --------------- */

uint32_t
sataf_mao_get_alphabet_size (sataf_mao *self)
{
  ccl_pre (self != NULL);
  ccl_pre (self->methods->get_alphabet_size != NULL);

  return self->methods->get_alphabet_size (self);
}

			/* --------------- */

sataf_mao * 
sataf_mao_succ (sataf_mao *self, uint32_t letter)
{
  ccl_pre (self != NULL);
  ccl_pre (letter < sataf_mao_get_alphabet_size (self));
  ccl_pre (self->methods->succ != NULL);

  return self->methods->succ (self, letter);
}

			/* --------------- */

int
sataf_mao_is_final (sataf_mao *self)
{
  ccl_pre (self != NULL);
  if (self->methods->is_final == NULL)
    return 0;

  return self->methods->is_final (self);
}

			/* --------------- */

int
sataf_mao_equals (sataf_mao *self, sataf_mao *other)
{
  ccl_pre (self != NULL);
  ccl_pre (other != NULL);

  if (sataf_mao_get_type (self) != sataf_mao_get_type (other))
    return 0;

  if (sataf_mao_get_alphabet_size (self) !=
      sataf_mao_get_alphabet_size (other))
    return 0;

  if (self->methods->equals == NULL)
    return self == other;

  return self->methods->equals (self, other);
}

			/* --------------- */

unsigned int
sataf_mao_hashcode (sataf_mao *self)
{
  unsigned int val;

  ccl_pre (self != NULL);
  if (self->methods->hashcode == NULL)
    val = (uintptr_t) self;
  else
    val = self->methods->hashcode (self);

  return val;
}

			/* --------------- */

void
sataf_mao_to_dot (ccl_log_type log, sataf_mao *self, const char **alphabet,
		  const char *graph_name, const char *graph_type)
{
  ccl_pre (self != NULL);

  if (self->methods->to_dot == NULL)
    sataf_mao_default_to_dot (log, self, alphabet, graph_name, graph_type);
  else
    self->methods->to_dot (log, self, alphabet, graph_name, graph_type);
}


			/* --------------- */

static void
s_init_memo (sataf_mao_memo * m, void *data)
{
  MEMO *mptr = (MEMO *) m;
  uint32_t *idptr = (uint32_t *) data;

  mptr->next = NULL;
  mptr->visited_and_id = *idptr << 1;
  (*idptr)++;
}

			/* --------------- */

void
sataf_mao_default_to_dot (ccl_log_type log, sataf_mao *self, 
			  const char **sigma, const char *graph_name,
			  const char *graph_type)
{
  MEMO *to_visit;
  uint32_t sigma_size;
  uint32_t state_id;
  sataf_mao_memorizer *M;

  if (graph_name == NULL)
    ccl_log (log, "%s sataf_mao_%p {\n", graph_type, self);
  else
    ccl_log (log, "%s %s {\n", graph_type, graph_name);

  state_id = 0;
  M = sataf_mao_memorizer_create (sizeof (MEMO), s_init_memo, &state_id, NULL);
  to_visit = (MEMO *) sataf_mao_memorizer_remind (M, self);
  to_visit->visited_and_id |= 1;
  sigma_size = sataf_mao_get_alphabet_size (self);

  while (to_visit != NULL)
    {
      uint32_t label;
      uint32_t l;
      MEMO *m = to_visit;

      to_visit = m->next;
      m->next = NULL;

      label = m->visited_and_id >> 1;

      if (self->methods->to_string != NULL)
	{
	  char *s = sataf_mao_to_string (m->super.A);
	  ccl_log (log, "N%p[shape=circle,label=\"%s\"", m, s);
	  ccl_delete (s);
	}
      else
	{
	  ccl_log (log, "N%p[shape=circle,label=\"%d\"", m,
		   label);
	}

      if (m->super.A == self)
	ccl_log (log, ",style=filled,fillcolor=grey");
      if (sataf_mao_is_final (m->super.A))
	ccl_log (log, ",peripheries=2");

      ccl_log (log, "]\n");

      for (l = 0; l < sigma_size; l++)
	{
	  MEMO *succ = (MEMO *) sataf_mao_memorizer_succ (M, m->super.A, l);

	  if (sigma == NULL)
	    ccl_log (log, "N%p->N%p[label=\"%d\"]\n", m, succ, l);
	  else
	    ccl_log (log, "N%p->N%p[label=\"%s\"]\n", m, succ, sigma[l]);

	  if ((succ->visited_and_id & 0x1) == 0)
	    {
	      /* not visited and not in the queue */
	      succ->visited_and_id |= 1;
	      succ->next = to_visit;
	      to_visit = succ;
	    }
	}
    }

  sataf_mao_memorizer_delete (M);

  ccl_log (log, "}\n");
}

			/* --------------- */

sataf_mao * 
sataf_mao_create_zero (uint32_t alphabet_size)
{
  uint32_t i;
  sataf_ea *ea = sataf_ea_create (1, 0, alphabet_size);
  sataf_mao *R;

  ea->is_final[0] = 0;
  for (i = 0; i < alphabet_size; i++)
    sataf_ea_set_successor (ea, 0, i, 0, 0);

  R = s_create_from_exit_automaton (ea, 0);
  sataf_ea_del_reference (ea);

  return R;
}

			/* --------------- */

sataf_mao * 
sataf_mao_create_one (uint32_t alphabet_size)
{
  uint32_t i;
  sataf_ea *ea = sataf_ea_create (1, 0, alphabet_size);
  sataf_mao *R;

  ea->is_final[0] = 1;
  for (i = 0; i < alphabet_size; i++)
    sataf_ea_set_successor (ea, 0, i, 0, 0);

  R = s_create_from_exit_automaton (ea, 0);
  sataf_ea_del_reference (ea);

  return R;
}

			/* --------------- */

sataf_mao * 
sataf_mao_create_epsilon (uint32_t alphabet_size)
{
  uint32_t i;
  sataf_ea *ea = sataf_ea_create (2, 0, alphabet_size);
  sataf_mao *R;

  ea->is_final[0] = 1;
  ea->is_final[1] = 0;
  for (i = 0; i < alphabet_size; i++)
    {
      sataf_ea_set_successor (ea, 0, i, 1, 0);
      sataf_ea_set_successor (ea, 1, i, 1, 0);
    }

  R = s_create_from_exit_automaton (ea, 0);
  sataf_ea_del_reference (ea);

  return R;
}

			/* --------------- */

sataf_mao * 
sataf_mao_create_letter (uint32_t alphabet_size, uint32_t a)
{
  uint32_t i;
  sataf_ea *ea = sataf_ea_create (3, 0, alphabet_size);
  sataf_mao *R;

  ccl_pre (a < alphabet_size);

  ea->is_final[0] = 0;
  ea->is_final[1] = 1;
  ea->is_final[2] = 0;

  for (i = 0; i < alphabet_size; i++)
    {
      if (i == a)
	sataf_ea_set_successor (ea, 0, i, 1, 0);
      else
	sataf_ea_set_successor (ea, 0, i, 2, 0);
      sataf_ea_set_successor (ea, 1, i, 2, 0);
      sataf_ea_set_successor (ea, 2, i, 2, 0);
    }

  R = s_create_from_exit_automaton (ea, 0);
  sataf_ea_del_reference (ea);

  return R;
}

			/* --------------- */

sataf_mao * 
sataf_mao_create_alphabet (uint32_t alphabet_size)
{
  uint32_t i;
  sataf_ea *ea = sataf_ea_create (3, 0, alphabet_size);
  sataf_mao *R;

  ea->is_final[0] = 0;
  ea->is_final[1] = 1;
  ea->is_final[2] = 0;

  for (i = 0; i < alphabet_size; i++)
    {
      sataf_ea_set_successor (ea, 0, i, 1, 0);
      sataf_ea_set_successor (ea, 1, i, 2, 0);
      sataf_ea_set_successor (ea, 2, i, 2, 0);
    }

  R = s_create_from_exit_automaton (ea, 0);
  sataf_ea_del_reference (ea);

  return R;

}

			/* --------------- */

sataf_mao * 
sataf_mao_create_from_arrays (uint32_t nb_states, uint32_t alphabet_size,
			      uint32_t s0, const uint8_t * is_final,
			      const uint32_t * succ)
{
  uint32_t i;
  sataf_ea *ea = sataf_ea_create (nb_states, 0, alphabet_size);
  sataf_mao *R = s_create_from_exit_automaton (ea, s0);

  ccl_pre (s0 < nb_states);
  ccl_memcpy (ea->is_final, is_final, nb_states);
  for (i = 0; i < alphabet_size * nb_states; i++)
    ea->successor[i] = sataf_ea_encode_succ_as_local_state (succ[i]);

  sataf_ea_del_reference (ea);

  return R;
}

			/* --------------- */

static size_t
s_sea_size (sataf_mao *self)
{
  return sizeof (sea_automaton);

}

			/* --------------- */

static void
s_sea_destroy (sataf_mao *self)
{
  sea_automaton *sea = (sea_automaton *) self;

  sataf_ea_del_reference (sea->ea);
}

			/* --------------- */

static uint32_t
s_sea_get_alphabet_size (sataf_mao *self)
{
  sea_automaton *sea = (sea_automaton *) self;

  return sea->ea->alphabet_size;
}

			/* --------------- */

static char *
s_sea_to_string (sataf_mao *self)
{
  sea_automaton *sea = (sea_automaton *) self;
  return ccl_string_format_new ("%d", sea->s);
}

			/* --------------- */

static sataf_mao * 
s_sea_succ (sataf_mao *self, uint32_t letter)
{
  sea_automaton *sea = (sea_automaton *) self;
  uint32_t succ = sataf_ea_get_successor (sea->ea, sea->s, letter);

  succ = sataf_ea_decode_succ_state (succ);

  return s_create_from_exit_automaton (sea->ea, succ);
}

			/* --------------- */
static int
s_sea_is_final (sataf_mao *self)
{
  sea_automaton *sea = (sea_automaton *) self;

  return sea->ea->is_final[sea->s];
}

			/* --------------- */

static int
s_sea_is_equal_to (sataf_mao *self, sataf_mao *other)
{
  sea_automaton *sea1 = (sea_automaton *) self;
  sea_automaton *sea2 = (sea_automaton *) other;

  return sea1->ea == sea2->ea && sea1->s == sea2->s;
}

			/* --------------- */

static unsigned int
s_sea_hashcode (sataf_mao *self)
{
  sea_automaton *sea = (sea_automaton *) self;

  return 11 * (uintptr_t) sea->ea + 13 * (uint32_t) sea->s;
}

			/* --------------- */

static sataf_mao * 
s_create_from_exit_automaton (sataf_ea *ea, uint32_t s0)
{
  sea_automaton *result = (sea_automaton *)
    sataf_mao_create (sizeof (struct sea_automaton_st), &SEA_METHODS);

  ccl_pre (ea->nb_exit_states == 0);

  result->ea = sataf_ea_add_reference (ea);
  result->s = s0;

  return (sataf_mao *) result;
}

			/* --------------- */

static void *
s_allocate_automaton (uint32_t size)
{
  void *r;

  uint32_t szd2 = (size + 1) >> 1;
  ccl_pre (size != 0);
  ccl_pre (szd2 < MAX_SA_SIZE_DIV_2);

  if (POOLS[szd2] == NULL)
    POOLS[szd2] = ccl_pool_create ("SA-Pool", size, SA_POOL_PAGE_SIZE);

  r = ccl_pool_alloc (POOLS[szd2]);
  ccl_memzero (r, size);

  return r;
}

			/* --------------- */

static void
s_delete_automaton (sataf_mao *a)
{
  if (a->methods->size != NULL)
    {
      uint32_t size = (a->methods->size (a) + 1) >> 1;

      ccl_assert (POOLS[size] != NULL);
      ccl_pool_release (POOLS[size], a);
    }
  else
    {
      ccl_delete (a);
    }
}
