/*
 * sa-closure.c -- Closure operation on shared automata
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include "sataf-p.h"

static void
s_close (sataf_ea *ea, sataf_sa ** bind_sa, uint32_t *bind_init, int type);

			/* --------------- */

void
sataf_sa_exists_closure (sataf_ea *ea, sataf_sa **bind_sa, uint32_t *bind_init)
{
  s_close (ea, bind_sa, bind_init, 1);
}

			/* --------------- */

void
sataf_sa_forall_closure (sataf_ea *ea, sataf_sa **bind_sa, uint32_t *bind_init)
{
  s_close (ea, bind_sa, bind_init, 0);
}

			/* --------------- */

static void
s_close (sataf_ea *ea, sataf_sa **bind_sa, uint32_t *bind_init, int type)
{
  uint32_t *set = ccl_new_array (uint32_t, ea->nb_local_states);
  char *done = ccl_new_array (char, ea->nb_local_states);
  uint32_t set_size = 0;
  uint32_t in;
  uint32_t q, p, s;

  p = ea->nb_local_states;
  while (p--)
    {
      if (done[p] || ea->is_final[p] == type)
	continue;

      s = p;
      q = sataf_ea_encode_succ_as_local_state (p);

      while (sataf_ea_is_local_state (q) && ea->is_final[p] != type && 
	     !done[s])
	{
	  set[set_size++] = s;
	  done[s] = 1;
	  q = sataf_ea_get_successor (ea, s, 0);
	  s = sataf_ea_decode_succ_state (q);
	}

      in = sataf_ea_is_exit_state (q)
	? bind_sa[s]->automaton->is_final[bind_init[s]] : ea->is_final[s];

      if ((in && type) || (!in && !type))
	{
	  for (s = 0; s < set_size; s++)
	    ea->is_final[set[s]] = type;
	}
      memset (set, 0, set_size * sizeof (uint32_t));
      set_size = 0;
    }
  ccl_delete (done);
  ccl_delete (set);
}

			/* --------------- */

