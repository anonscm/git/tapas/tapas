/*
 * sa.c -- Shared Automaton data structure
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "sataf-p.h"

typedef struct sa_manager_st
{
  uint32_t init_table_size;
  uint32_t fill_degree;
  uint32_t table_size;
  sataf_sa **table;
  uint32_t nb_elements;
} sa_manager;

			/* --------------- */

#define MUST_INCREASE_TABLE(m)					\
  ((m)->nb_elements > (m)->table_size * (m)->fill_degree)

#define MUST_DECREASE_TABLE(m)					\
  ((m)->nb_elements < (m)->table_size * (m)->fill_degree &&	\
   (m)->table_size > (m)->init_table_size)


			/* --------------- */

static sataf_sa * 
s_create_shared_automaton (uint32_t depth, sataf_ea *ea, sataf_sa **bind_sa, 
			   uint32_t *bind_init);

static void
s_sa_manager_resize_table (uint32_t new_size);

static uint32_t
HVALUE1 (sataf_ea *ea, sataf_sa **bind_sa, uint32_t *bind_init);

static uint32_t
HVALUE2 (sataf_ea *ea, sataf_msa **bind);

static void
s_display_as_dot (sataf_sa *sa, ccl_log_type log, uint32_t marked_state,
		  const char **alphabet, ccl_list * visited,
		  ccl_list * to_visit, int *pstate);

			/* --------------- */

static sa_manager *SA_MANAGER;

			/* --------------- */

int
sataf_sa_init (int ut_table_size, int ut_max_fill_degree)
{
  SA_MANAGER = ccl_new (sa_manager);

  SA_MANAGER->init_table_size = ut_table_size;
  SA_MANAGER->fill_degree = ut_max_fill_degree;
  SA_MANAGER->table_size = ut_table_size;
  SA_MANAGER->table = ccl_new_array (sataf_sa *, SA_MANAGER->table_size);
  SA_MANAGER->nb_elements = 0;

  return 1;
}

			/* --------------- */

void
sataf_sa_terminate (void)
{
  ccl_pre ("SA uniqueness table still contains elements" &&
	   (SA_MANAGER->nb_elements == 0));

  if (SA_MANAGER->nb_elements != 0)
    {
      int i = SA_MANAGER->table_size;

      while (i--)
	{
	  sataf_sa *sa;
	  sataf_sa *next;

	  for (sa = SA_MANAGER->table[i]; sa; sa = next)
	    {
	      int j = sa->automaton->nb_exit_states;

	      next = sa->next;
	      sataf_ea_del_reference (sa->automaton);
	      while (j--)
		sataf_msa_del_reference (sa->bind[j]);
	      ccl_delete (sa);
	    }
	}
    }
  ccl_delete (SA_MANAGER->table);
  ccl_delete (SA_MANAGER);
}

			/* --------------- */

sataf_sa * 
sataf_sa_create_one (uint32_t alphabet_size)
{
  sataf_sa *R;
  sataf_ea *ea = sataf_ea_create (1, 0, alphabet_size);
  sataf_ea *one;

  ea->is_final[0] = 1;

  one = sataf_ea_find_or_add (ea);
  R = sataf_sa_find_or_add (one, NULL, NULL);

  sataf_ea_del_reference (ea);
  sataf_ea_del_reference (one);

  return R;
}

			/* --------------- */

sataf_sa * 
sataf_sa_create_zero (uint32_t alphabet_size)
{
  sataf_sa *R;
  sataf_ea *ea = sataf_ea_create (1, 0, alphabet_size);
  sataf_ea *zero;

  ea->is_final[0] = 0;

  zero = sataf_ea_find_or_add (ea);
  R = sataf_sa_find_or_add (zero, NULL, NULL);

  sataf_ea_del_reference (ea);
  sataf_ea_del_reference (zero);

  return R;
}

			/* --------------- */


void
sataf_sa_destroy (sataf_sa *sa)
{
  sataf_sa **psa;
  uint32_t index = HVALUE2 (sa->automaton, sa->bind) % SA_MANAGER->table_size;

  for (psa = SA_MANAGER->table + index; *psa && *psa != sa;
       psa = &((*psa)->next))
    /* do nothing */ ;

  ccl_assert (*psa == sa);

  *psa = sa->next;

  for (index = 0; index < sa->automaton->nb_exit_states; index++)
    sataf_msa_del_reference (sa->bind[index]);
  sataf_ea_del_reference (sa->automaton);
  ccl_delete (sa);

  SA_MANAGER->nb_elements--;

  if (MUST_DECREASE_TABLE (SA_MANAGER))
    s_sa_manager_resize_table ((SA_MANAGER->table_size - 13) >> 1);
}

			/* --------------- */

sataf_sa * 
sataf_sa_find_or_add (sataf_ea *ea, sataf_sa ** bind_sa, uint32_t * bind_init)
{
  sataf_sa *R;
  sataf_sa **psa;
  uint32_t i;
  uint32_t ne;
  uint32_t index;
  uint32_t depth;
  uint32_t hval;

  ne = ea->nb_exit_states;
  hval = HVALUE1 (ea, bind_sa, bind_init) ;
  index = hval % SA_MANAGER->table_size;
  depth = ne > 0 ? bind_sa[0]->depth + 1 : 0;

  ccl_assert (index < SA_MANAGER->table_size);

  for (psa = SA_MANAGER->table + index; *psa; psa = &((*psa)->next))
    {
      ccl_assert ((*psa)->refcount > 0);

      if ((*psa)->depth != depth || (*psa)->automaton != ea)
	continue;
      i = 0;
      while (i < ne && bind_sa[i] == (*psa)->bind[i]->A &&
	     bind_init[i] == (*psa)->bind[i]->initial)
	i++;
      if (i == ne)
	return sataf_sa_add_reference (*psa);
    }

  R = s_create_shared_automaton (depth, ea, bind_sa, bind_init);
  index = hval % SA_MANAGER->table_size;
  R->next = SA_MANAGER->table[index];
  SA_MANAGER->table[index] = R;

  SA_MANAGER->nb_elements++;

  if (MUST_INCREASE_TABLE (SA_MANAGER))
    s_sa_manager_resize_table ((SA_MANAGER->table_size << 1) + 13);
  ccl_post (R != NULL);

  return R;
}


			/* --------------- */

void
sataf_sa_display_as_dot (sataf_sa *sa, ccl_log_type log, int marked_state, 
			 const char **alphabet, int with_info)
{
  uint32_t m;
  int state_count = 0;
  int *pstate = with_info ? NULL : &state_count;
  ccl_list *to_visit = ccl_list_create ();
  ccl_list *visited = ccl_list_create ();

  ccl_log (log, "digraph sataf_sa_%p {\n", sa);
  ccl_list_add (to_visit, sa);
  while (ccl_list_get_size (to_visit) > 0)
    {
      sataf_sa *a = (sataf_sa *) ccl_list_take_first (to_visit);
      if (a == sa)
	m = marked_state;
      else
	m = 0xFFFFFFFF;
      s_display_as_dot (a, log, m, alphabet, visited, to_visit, pstate);
    }
  ccl_log (log, "}\n");
  ccl_list_delete (to_visit);
  ccl_list_delete (visited);
}

			/* --------------- */

void
sataf_sa_display_scc_as_dot (sataf_sa *sa, ccl_log_type log)
{
  ccl_list *to_visit = ccl_list_create ();
  ccl_list *visited = ccl_list_create ();

  ccl_log (log, "digraph sataf_sa_%p {\n", sa);

  ccl_list_add (to_visit, sa);
  while (ccl_list_get_size (to_visit) > 0)
    {
      uint32_t i;
      sataf_sa *a = (sataf_sa *) ccl_list_take_first (to_visit);

      ccl_list_add (visited, a);
      if (sataf_sa_is_zero (a))
	ccl_log (log, "N%p [label=\"[0]\"];\n", a);
      else if (sataf_sa_is_one (a))
	ccl_log (log, "N%p [label=\"[1]\"];\n", a);
      else
	for (i = 0; i < a->automaton->nb_exit_states; i++)
	  {
	    ccl_log (log, "N%p -> N%p [label=\"%d\"];\n", a, a->bind[i]->A,
		     a->bind[i]->initial);
	    if (!ccl_list_has (to_visit, a->bind[i]->A) &&
		!ccl_list_has (visited, a->bind[i]->A))
	      ccl_list_add (to_visit, a->bind[i]->A);
	  }
    }
  ccl_log (log, "}\n");

  ccl_list_delete (to_visit);
  ccl_list_delete (visited);
}

			/* --------------- */

static sataf_sa * 
s_create_shared_automaton (uint32_t depth, sataf_ea *ea,
			   sataf_sa ** bind_sa, uint32_t * bind_init)
{
  uint32_t i;
  size_t to_alloc;
  sataf_sa *result;

  ccl_pre (ea != NULL);

  to_alloc = sizeof (sataf_sa);
  if (ea->nb_exit_states > 1)
    to_alloc += sizeof (sataf_msa *) * (ea->nb_exit_states - 1);

  result = (sataf_sa *) ccl_calloc (to_alloc, 1);
  result->next = NULL;
  result->refcount = 1;
  result->automaton = sataf_ea_add_reference (ea);
  result->depth = depth;
  for (i = 0; i < ea->nb_exit_states; i++)
    result->bind[i] = sataf_msa_find_or_add (bind_sa[i], bind_init[i]);

  return result;
}

			/* --------------- */


static void
s_sa_manager_resize_table (uint32_t new_size)
{
  uint32_t i;
  sataf_sa **new_table = ccl_new_array (sataf_sa *, new_size);
  sataf_sa *sa;
  sataf_sa *next;
  uint32_t index;

  if (new_table == NULL)
    return;

  for (i = 0; i < SA_MANAGER->table_size; i++)
    {
      for (sa = SA_MANAGER->table[i]; sa; sa = next)
	{
	  next = sa->next;
	  index = HVALUE2 (sa->automaton, sa->bind) % new_size;
	  sa->next = new_table[index];
	  new_table[index] = sa;
	}
    }
  SA_MANAGER->table_size = new_size;
  ccl_delete (SA_MANAGER->table);
  SA_MANAGER->table = new_table;
}

			/* --------------- */

static uint32_t
HVALUE1 (sataf_ea *ea, sataf_sa **bind_sa, uint32_t *bind_init)
{
  uint32_t i;
  uint32_t r = (uintptr_t) ea;

  for (i = 0; i < ea->nb_exit_states; i++)
    r = 19 * r + MSA_HVALUE (bind_sa[i], bind_init[i]);

  return r;
}

			/* --------------- */

static uint32_t
HVALUE2 (sataf_ea *ea, sataf_msa **bind)
{
  uint32_t i;
  uint32_t r = (uintptr_t) ea;

  for (i = 0; i < ea->nb_exit_states; i++)
    r = 19 * r + MSA_HVALUE (bind[i]->A, bind[i]->initial);

  return r;
}

			/* --------------- */

static void
s_display_as_dot (sataf_sa *sa, ccl_log_type log, uint32_t marked_state,
		  const char **alphabet, ccl_list *visited,
		  ccl_list * to_visit, int *pstate)
{
  uint32_t state, letter;

  ccl_list_add (visited, sa);

  for (state = 0; state < sa->automaton->nb_local_states; state++)
    {
      const char *color = (state == marked_state) ? "grey" : "white";

      ccl_log (log, "\"%p-%d\"[style=filled,fillcolor=%s,shape=ellipse", sa,
	       sataf_ea_encode_succ_as_local_state (state), color);
      if (sa->automaton->is_final[state])
	ccl_log (log, ",peripheries=2");
      if (pstate == NULL)
	ccl_log (log, ",label=\"L@%d-%d\\nsa=%p\\nea=%p\"", 
		 sa->depth, state, sa, sa->automaton);
      else 
	ccl_log (log, ",label=\"%d\"", (*pstate)++);

      ccl_log (log, "];\n");
    }

  for (state = 0; state < sa->automaton->nb_local_states; state++)
    {
      for (letter = 0; letter < sa->automaton->alphabet_size; letter++)
	{
	  uint32_t succ =
	    sataf_ea_get_successor (sa->automaton, state, letter);
	  const char *fmt = "\"%p-%d\" -> \"%p-%d\"";

	  if (sataf_ea_is_exit_state (succ))
	    {
	      sataf_msa *msa = sa->bind[sataf_ea_decode_succ_state (succ)];

	      if (sataf_msa_is_zero (msa))
		continue;
	      ccl_log (log, fmt, sa, 
		       sataf_ea_encode_succ_as_local_state (state),msa->A,
		       sataf_ea_encode_succ_as_local_state (msa->initial));
	    }
	  else
	    {
	      ccl_log (log, fmt, sa, 
		       sataf_ea_encode_succ_as_local_state (state), sa, succ);
	    }

	  if (alphabet == NULL)
	    ccl_log (log, "[label=\"%d", letter);
	  else
	    ccl_log (log, "[label=\"%s", alphabet[letter]);
	  if (pstate == NULL && sataf_ea_is_exit_state (succ))
	    ccl_log (log, "/%d", sataf_ea_decode_succ_state (succ));
	  ccl_log (log, "\"];\n");
	}
    }

  for (state = 0; state < sa->automaton->nb_exit_states; state++)
    {
      if (!sataf_msa_is_zero (sa->bind[state]) && 
	  !ccl_list_has (visited, sa->bind[state]->A) &&
	  !ccl_list_has (to_visit, sa->bind[state]->A)) 
	ccl_list_add (to_visit, sa->bind[state]->A);
    }
}

			/* --------------- */

void
sataf_sa_table_statistics (ccl_log_type log)
{
  int i;
  uint32_t nb_queue = 0;
  uint32_t nb_refcount = 0;
  uint32_t min_refcount = 0xFFFFFFFF;
  uint32_t max_refcount = 0;

  for (i = 0; i < SA_MANAGER->init_table_size; i++)
    {
      sataf_sa *sa = SA_MANAGER->table[i];

      if (sa == NULL)
	continue;
      nb_queue++;
      for (; sa != NULL; sa = sa->next)
	{
	  nb_refcount += sa->refcount;
	  if (sa->refcount < min_refcount)
	    min_refcount = sa->refcount;
	  if (sa->refcount > max_refcount)
	    max_refcount = sa->refcount;
	}
    }

  ccl_log (log, "SA uniqueness table statistics :\n");
  ccl_log (log, "--------------------------------\n");
  ccl_log (log, "init size = %d\n", SA_MANAGER->init_table_size);
  ccl_log (log, "fill degree = %d\n", SA_MANAGER->fill_degree);
  ccl_log (log, "table size = %d\n", SA_MANAGER->table_size);
  ccl_log (log, "nb entries = %d\n", SA_MANAGER->nb_elements);
  ccl_log (log, "mean collision list len = %4.2f\n",
	   (float) SA_MANAGER->nb_elements / (float) nb_queue);
  ccl_log (log, "mean sharing count = %4.2f\n",
	   (float) nb_refcount / (float) SA_MANAGER->nb_elements);
  ccl_log (log, "refcount range = [%d, %d]\n", min_refcount, max_refcount);
  ccl_log (log, "\n");
}
