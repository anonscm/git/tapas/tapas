/*
 * msa-cache.c -- Cache for MSA operations
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include "sataf-p.h"

# define H(a1, a2, a3, aux)				\
  ((117 * ((uintptr_t) a1) +				\
    1043 * ((uintptr_t) a2) +				\
    2049 * ((uintptr_t) a3) +				\
    4041 * ((uintptr_t) aux)) % MSA_CACHE_SIZE)

/**
 *
 */
typedef struct msa_cache_record_st msa_cache_record;
struct msa_cache_record_st
{
  sataf_msa *arg1;
  sataf_msa *arg2;
  sataf_msa *arg3;
  const void *aux;
  sataf_msa *R;
};

			/* --------------- */

static void
s_clear_record (msa_cache_record * rec);

			/* --------------- */

static int cache_is_enabled;
static uint32_t cache_filled_entries;
static uint32_t number_of_collisions;
static uint32_t number_of_insertions;
static uint32_t number_of_successful_searches;
static uint32_t number_of_searches;

static size_t MSA_CACHE_SIZE;
static msa_cache_record *MSA_CACHE;


			/* --------------- */

int
sataf_msa_cache_init (size_t cache_size)
{
  cache_is_enabled = (cache_size != 0);

  if (!cache_is_enabled)
    return 1;

  cache_filled_entries = 0;
  number_of_collisions = 0;
  number_of_insertions = 0;
  number_of_successful_searches = 0;
  number_of_searches = 0;
  MSA_CACHE_SIZE = cache_size;
  MSA_CACHE = ccl_new_array (msa_cache_record, MSA_CACHE_SIZE);

  return 1;
}

			/* --------------- */

void
sataf_msa_cache_terminate (void)
{
  if (cache_is_enabled)
    {
      int i;
      msa_cache_record *rec = MSA_CACHE;

      for (i = 0; i < MSA_CACHE_SIZE; i++, rec++)
	s_clear_record (rec);
      ccl_delete (MSA_CACHE);
    }
}

			/* --------------- */

sataf_msa * 
sataf_msa_cache_get (sataf_msa *a1, sataf_msa *a2, sataf_msa *a3,
		     const void *aux)
{
  sataf_msa *R = NULL;

  if (cache_is_enabled)
    {
      uint32_t index = H (a1, a2, a3, aux);
      msa_cache_record *rec = MSA_CACHE + index;

      number_of_searches++;

      if (rec->arg1 == a1 && rec->arg2 == a2 && rec->arg3 == a3 &&
	  rec->aux == aux)
	{
	  R = sataf_msa_add_reference (rec->R);
	  number_of_successful_searches++;
	}
    }

  return R;
}

			/* --------------- */

void
sataf_msa_cache_put (sataf_msa *a1, sataf_msa *a2, sataf_msa *a3,
		     const void *aux, sataf_msa *R)
{
  if (cache_is_enabled)
    {
      uint32_t index = H (a1, a2, a3, aux);
      msa_cache_record *rec = MSA_CACHE + index;

      number_of_insertions++;

      if (rec->arg1 == NULL && rec->arg2 == NULL && rec->arg3 == NULL &&
	  rec->aux == NULL)
	cache_filled_entries++;
      else
	{
	  number_of_collisions++;
	  s_clear_record (rec);
	}

      if (a1 != NULL)
	{
	  rec->arg1 = sataf_msa_add_reference (a1);
	  if (a2 != NULL)
	    {
	      rec->arg2 = sataf_msa_add_reference (a2);
	      if (a3 != NULL)
		{
		  rec->arg3 = sataf_msa_add_reference (a3);
		}
	    }
	}

      rec->aux = aux;

      rec->R = sataf_msa_add_reference (R);
    }
}

			/* --------------- */

void
sataf_msa_cache_statistics (ccl_log_type log)
{
  if (!cache_is_enabled)
    return;

  ccl_log (log, "Usage of cache 'MSA operations'\n");
  ccl_log (log, " size       : %d\n", MSA_CACHE_SIZE);
  ccl_log (log, " entries    : %d (%.2f%%)\n", cache_filled_entries,
	   100.0 * (float) cache_filled_entries / (float) MSA_CACHE_SIZE);
  ccl_log (log, " requests   : %d\n", number_of_searches);
  ccl_log (log, " success    : %d (%f %%)\n", number_of_successful_searches,
	   100.0 * (float) number_of_successful_searches /
	   (float) number_of_searches);
  ccl_log (log, " collisions : %d\n", number_of_collisions);
  ccl_log (log, "\n");
}

			/* --------------- */

static void
s_clear_record (msa_cache_record * rec)
{
  ccl_zdelete (sataf_msa_del_reference, rec->arg1);
  ccl_zdelete (sataf_msa_del_reference, rec->arg2);
  ccl_zdelete (sataf_msa_del_reference, rec->arg3);
  ccl_zdelete (sataf_msa_del_reference, rec->R);
  ccl_memzero (rec, sizeof (msa_cache_record));
}

