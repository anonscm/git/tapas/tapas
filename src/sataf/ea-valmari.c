/*
 * ea-valmari.c -- add a comment about this file
 *
 * This file is a part of the SATAF library.
 *
 * Copyright (C) 2018 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include "sataf-p.h"

typedef struct partition {
  int nb_subsets;  /* z */
  int *reference_set;  /* E */
  int *first_in_subset; /* F */
  int *last_in_subset;  /* P; P[i] = F[i] + size of the subset */
  int *element_index_in_refset; /* L */
  int *subset_of_element; /* S */
  int array[1]; /* should contain 5 * §refset cardinality) + 1 int */
} partition;

static partition *
s_partition_new (int set_size)
{
  int i;
  partition *result;
  size_t memsize = sizeof(partition) + (5 * set_size + 1) * sizeof (int);

  ccl_pre (set_size > 0);

  result = ccl_malloc (memsize);
  ccl_memzero (result, memsize);
  result->nb_subsets = set_size ? 1 : 0;
  result->reference_set = result->array;
  result->first_in_subset = result->reference_set + set_size;
  result->last_in_subset = result->first_in_subset + set_size;
  result->element_index_in_refset = result->last_in_subset + set_size;
  result->subset_of_element = result->element_index_in_refset + set_size;

  for (i = 0; i < set_size; i++)
    {
      result->reference_set[i] = i;
      result->element_index_in_refset[i] = i;
      result->subset_of_element[i] = 0;
    }

  if (result->nb_subsets)
    {
      result->first_in_subset[0] = 0;
      result->last_in_subset[0] = set_size;
    }

  return result;
}

static void
s_partition_delete (partition *P)
{
  ccl_delete (P);
}

static void
s_partition_mark (partition *P, int e, int *nb_marked,
		  int *touched_sets, int *p_nb_touched_sets)
{
  int subset = P->subset_of_element[e];
  int i = P->element_index_in_refset[e];
  int j = P->first_in_subset[subset] + nb_marked[subset];
  int ej = P->reference_set[j];

  /* move element ej located at e into cell at i */
  P->reference_set[i] = ej;
  P->element_index_in_refset[ej] = i;
  /* move element e located at i into cell at ej */
  P->reference_set[j] = e;
  P->element_index_in_refset[e] = j;

  if (nb_marked[subset] == 0) /* add the subset of e to touched sets if 
				 necessary */
    {
      touched_sets[*p_nb_touched_sets] = subset;
      (*p_nb_touched_sets)++;
    }
  nb_marked[subset]++;
}

static int
s_cmp (const void *i1, const void *i2)
{
  return *(int *)i1 - *(int *)i2;
}

static void
s_partition_split (partition *P,
		   int *nb_marked,
		   int *touched_sets, int *p_nb_touched_sets)
{
  int nb_touched_sets = *p_nb_touched_sets;
  qsort (touched_sets, nb_touched_sets, sizeof (int), s_cmp);
  while (nb_touched_sets)
    {
      int i, first, last;
      int subset = touched_sets[--nb_touched_sets];
      int last_marked = P->first_in_subset[subset] + nb_marked[subset];

      if (last_marked == P->last_in_subset[subset])
	{
	  /* all elements are marked; no split is necessary.
	     we mark them as unmarked and continue */
	  nb_marked[subset] = 0;
	  continue;
	}

      /* select the smallest subset to create the new subset */
      if (nb_marked[subset] <= P->last_in_subset[subset] - last_marked)
	{
	  first = P->first_in_subset[P->nb_subsets] =
	    P->first_in_subset[subset];
	  last = P->last_in_subset[P->nb_subsets] = last_marked;
	  P->first_in_subset[subset] = last_marked;
	}
      else
	{
	  first = P->first_in_subset[P->nb_subsets] = last_marked;
	  last = P->last_in_subset[P->nb_subsets] = P->last_in_subset[subset];
	  P->last_in_subset[subset] = last_marked;
	}
      for (i = first; i < last; i++)
	P->subset_of_element[P->reference_set[i]] = P->nb_subsets;
      nb_marked[subset] = 0;
      nb_marked[P->nb_subsets] = 0;
      P->nb_subsets++;
    }

  *p_nb_touched_sets = 0;
}

static void
s_make_adjacent_trans (int nb_states, int nb_trans, 
		       int *ends_of_tr, int *adjacent_tr, int *first_adj_tr)
{
  int q;
  int t;

  for (q = 0; q <= nb_states; q++)
    first_adj_tr[q] = 0;
  /* first we count the number of adjacent transitions */  
  for (t = 0; t < nb_trans; t++) 
    first_adj_tr[ends_of_tr[t]]++;
  /* update the array in order to represent indices in adjacent_tr */
  for (q = 0; q < nb_states; q++)
    first_adj_tr[q + 1] += first_adj_tr[q];
  /* add adjacent transitions from the end of adjacency list */
  for (t = nb_trans; t--;)
    {
      int end = ends_of_tr[t];
      int nb_adjacent = first_adj_tr[end];
      adjacent_tr[nb_adjacent - 1] = t;
      first_adj_tr[end] = nb_adjacent - 1;
    }
}

static void
s_mark_as_reached (partition *P, int q, int *p_nb_reached)
{
  int i = P->element_index_in_refset[q];
  if (i >= *p_nb_reached)
    {
      P->reference_set[i] = P->reference_set[*p_nb_reached];
      P->element_index_in_refset[P->reference_set[i]] = i;
      P->reference_set[*p_nb_reached] = q;
      P->element_index_in_refset[q] = *p_nb_reached;
      (*p_nb_reached)++;
    }
}

static void
s_rem_unreachable (int nb_states, partition *P,
		   int *p_nb_trans,
		   int *adjavacent_tr, int *first_adj_tr,
		   int *src, int *labels, int *tgt, int *p_nb_reached)
{
  int i, j, t;

  s_make_adjacent_trans (nb_states, *p_nb_trans, src, adjavacent_tr,
			 first_adj_tr);

  /* BFS to mark reachable states */
  for (i = 0; i < *p_nb_reached; i++)
    {
      int first = first_adj_tr[P->reference_set[i]];;
      int last = first_adj_tr[P->reference_set[i] + 1];
      for (j = first; j < last; j++)
	s_mark_as_reached (P, tgt[adjavacent_tr[j]], p_nb_reached);
    }

  /* Update the transition function wrt to reachable states
     Reachable transitions are gathered at the begin of arrays by erasing
     existing (but unreachable) ones
   */
  j = 0;
  for (t = 0; t < *p_nb_trans; t++)
    {
      if (P->element_index_in_refset[src[t]] < *p_nb_reached)
	{
	  tgt[j] = tgt[t];
	  labels[j] = labels[t];
	  src[j] = src[t];
	  j++;
	}
    }
  *p_nb_trans = j;
  ccl_assert (P->nb_subsets <= 1);
  P->last_in_subset[0] = *p_nb_reached;
  *p_nb_reached = 0;
}

static void
s_split_blocks_and_cords (partition *B, partition *C,
			  int nb_states, int nb_trans, 
			  int *src, int *tgt,
			  int *adjavacent_tr, int *first_adj_tr,
			  int *nb_marked,
			  int *touched_sets, int *p_nb_touched_sets)
{
  int b, c, i, j;
  
  s_make_adjacent_trans (nb_states, nb_trans, tgt, adjavacent_tr,
			 first_adj_tr);
  b = 1;
  c = 0;

  while (c < C->nb_subsets)
    {
      for (i = C->first_in_subset[c]; i < C->last_in_subset[c]; i++)
	s_partition_mark (B, src[C->reference_set[i]], nb_marked,
			  touched_sets, p_nb_touched_sets);
      s_partition_split (B, nb_marked, touched_sets, p_nb_touched_sets);
      c++;

      while (b < B->nb_subsets)
	{
	  for (i = B->first_in_subset[b]; i < B->last_in_subset[b]; i++)
	    {
	      int s = B->reference_set[i];
	      for (j = first_adj_tr[s]; j < first_adj_tr[s + 1]; j++)
		s_partition_mark (C, adjavacent_tr[j], nb_marked, 
				  touched_sets, p_nb_touched_sets);
	    }
	  s_partition_split (C, nb_marked, touched_sets, p_nb_touched_sets); 
	  b++;
	}
    }
}

static sataf_ea *
s_build_minimal_ea (sataf_ea *ea, partition *B, uint32_t *h)
{
  uint32_t s;
  uint32_t a;
  sataf_ea *result;
  uint32_t nb_classes = B->nb_subsets;
  uint32_t *aci = ccl_new_array (uint32_t, nb_classes);
  uint32_t k = 0;

  /* assign indices to classes of local states; ignore exits */
  for (s = 0; s < nb_classes; s++)
    {
      if (B->reference_set[B->first_in_subset[s]] < ea->nb_local_states)
	aci[s] = k++;
    }
  nb_classes -= ea->nb_exit_states;

  result = sataf_ea_create (nb_classes, ea->nb_exit_states, ea->alphabet_size);

  for (s = 0; s < ea->nb_local_states; s++)
    {
      int classid = B->subset_of_element[s];
      uint32_t src = aci[classid];
      h[s] = src;

      result->is_final[src] = ea->is_final[s];

      for (a = 0; a < ea->alphabet_size; a++)
	{
	  uint32_t succ = sataf_ea_get_successor (ea, s, a);
	  uint32_t index = sataf_ea_decode_succ_state (succ);

	  if (sataf_ea_is_exit_state (succ))
	    sataf_ea_set_successor (result, src, a, index, 1);
	  else
	    {
	      int succ_class_id = B->subset_of_element[index];
	      int tgt = aci[succ_class_id];
	      sataf_ea_set_successor (result, src, a, tgt, 0);
	    }
	}
    }

  return result;
}

sataf_ea *
sataf_ea_minimize_valmari (sataf_ea *ea, uint32_t *h)
{
  int s, a, t;
  partition *B;
  partition *C;
  int nb_labels = sataf_ea_get_alphabet_size (ea);
  int nb_states = sataf_ea_get_nb_states (ea);
  int nb_exits = sataf_ea_get_nb_exits (ea);
  int nb_trans = nb_labels * nb_states;
  int *src = ccl_new_array (int, nb_trans);
  int *labels = ccl_new_array (int, nb_trans);
  int *tgt = ccl_new_array (int, nb_trans);
  int *adjacent_trans = ccl_new_array (int, nb_trans);
  int *first_adjacent_trans = ccl_new_array (int, nb_states + nb_exits + 1);
  int nb_reached = 0; 
  int *nb_marked;
  int *touched_sets;
  int nb_touched_sets;
  sataf_ea *R;
  
  B = s_partition_new (nb_states + nb_exits);
  t = 0;
  for (a = 0; a < nb_labels; a++)
    {
      for (s = 0; s < nb_states; s++)
	{
	  int esucc = sataf_ea_get_successor (ea, s, a);
	  int succ = sataf_ea_decode_succ_state (esucc);
	  if (sataf_ea_is_exit_state (esucc))
	    succ += nb_states;
	  src[t] = s;
	  labels[t] = a;
	  tgt[t] = succ;
	  t++;
	}
    }
  ccl_assert (t == nb_trans);

  nb_marked = ccl_new_array (int, nb_trans + 1);
  touched_sets = ccl_new_array (int, nb_trans + 1);
  nb_touched_sets = 0;
  /* make initial partition for states */
  for (s = 0; s < nb_states; s++)
    {
      if (sataf_ea_is_final (ea, s))
	s_partition_mark (B, s, nb_marked, touched_sets, &nb_touched_sets);
    }
  s_partition_split (B, nb_marked, touched_sets, &nb_touched_sets);

  for (s = 0; s < nb_exits; s++)
    {
      s_partition_mark (B, nb_states + s, nb_marked, touched_sets,
			&nb_touched_sets);
      s_partition_split (B, nb_marked, touched_sets, &nb_touched_sets);
    }


  /* make initial partition for transitions */
  C = s_partition_new (nb_trans);
  if (nb_trans)
    {
      int i;

      /* C.reference_set should sorted wrt labels */
      C->nb_subsets = 0;
      nb_marked[0] = 0;
      a = labels[C->reference_set[0]];
      ccl_assert (a == 0);
      for (i = 0; i < nb_trans; i++)
	{
	  int t = C->reference_set[i];
	  if (labels[t] != a)
	    {
	      ccl_assert (a < labels[t]);
	      a = labels[t];
	      C->last_in_subset[C->nb_subsets] = i;
	      C->nb_subsets++;
	      C->first_in_subset[C->nb_subsets] = i;
	      nb_marked[C->nb_subsets] = 0;
	    }
	  C->subset_of_element[t] = C->nb_subsets;
	  C->element_index_in_refset[t] = i;
	}
      C->last_in_subset[C->nb_subsets] = nb_trans;
      C->nb_subsets++;
    }

  s_split_blocks_and_cords (B, C, nb_states + nb_exits, nb_trans,
			    src, tgt, adjacent_trans, first_adjacent_trans,
			    nb_marked, touched_sets, &nb_touched_sets);

  R = s_build_minimal_ea (ea, B, h);

  s_partition_delete (B);
  s_partition_delete (C);
  ccl_delete (src);
  ccl_delete (labels);
  ccl_delete (tgt);
  ccl_delete (adjacent_trans);
  ccl_delete (first_adjacent_trans);
  ccl_delete (nb_marked);
  ccl_delete (touched_sets);

  return R;
}
