/*
 * msa-detection.c -- add a comment about this file
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-list.h>
#include <ccl/ccl-bittable.h>
#include "sataf.h"

int
sataf_msa_is_detectable (sataf_msa *to_detect, sataf_msa *A, ccl_list *acc)
{
  ccl_hash *done;
  int result;
  ccl_list *todo;
  uint32_t asize = sataf_msa_get_alphabet_size (to_detect);

  ccl_pre (asize == sataf_msa_get_alphabet_size (A));

  done = 
    ccl_hash_create (NULL, NULL, 
		     (ccl_delete_proc *) sataf_msa_del_reference,
		     (ccl_delete_proc *) sataf_msa_del_reference);
  result = 1;
  todo = ccl_list_create ();

  ccl_hash_find (done, sataf_msa_add_reference (A));
  ccl_hash_insert (done, sataf_msa_add_reference (to_detect));
  ccl_list_add (todo, A);

  while (result && !ccl_list_is_empty (todo))
    {
      uint32_t b;
      sataf_msa *s_to_detect;
      sataf_msa *s_A = ccl_list_take_first (todo);

      ccl_assert (ccl_hash_find (done, s_A));

      ccl_hash_find (done, s_A);
      s_to_detect = ccl_hash_get (done);
      if (sataf_msa_is_final (s_to_detect) && acc != NULL)
	{
	  s_A = sataf_msa_add_reference (s_A);
	  ccl_list_add (acc, s_A);
	}

      for (b = 0; result && b < asize; b++)
	{
	  sataf_msa *succ_A = sataf_msa_succ (s_A, b);
	  sataf_msa *succ_to_detect = sataf_msa_succ (s_to_detect, b);

	  if (ccl_hash_find (done, succ_A))
	    {
	      sataf_msa *t = ccl_hash_get (done);
	      result = (t == succ_to_detect);
	    }
	  else
	    {
	      ccl_list_add (todo, succ_A);

	      succ_A = sataf_msa_add_reference (succ_A);
	      succ_to_detect = sataf_msa_add_reference (succ_to_detect);
	      ccl_hash_insert (done, succ_to_detect);
	    }
	  sataf_msa_del_reference (succ_A);
	  sataf_msa_del_reference (succ_to_detect);
	}
    }

  if (!result && acc != NULL)
    ccl_list_clear (acc, (ccl_delete_proc *)sataf_msa_del_reference);
  ccl_list_delete (todo);
  ccl_hash_delete (done);

  return result;
}
