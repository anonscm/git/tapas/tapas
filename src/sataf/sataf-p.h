/*
 * sataf-p.h -- Internal routines
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __SATAF_P_H__
# define __SATAF_P_H__

# include "sataf.h"

/**
 *
 */
extern int
sataf_mao_init (void);

/**
 *
 */
extern void 
sataf_mao_terminate (void);

/**
 *
 */
extern int
sataf_ea_init (int ut_table_size, int ut_max_fill_degree);

/**
 *
 */
extern void
sataf_ea_terminate (void);


extern int
sataf_sa_init (int ut_table_size, int ut_max_fill_degree);

/**
 *
 */
extern void
sataf_sa_terminate (void);

/**
 *
 */
extern int
sataf_msa_init (int op_cache_size, int sharing_cache_size,
		int ut_table_size, int ut_max_fill_degree);

/**
 *
 */
extern void
sataf_msa_terminate (void);

/**
 *
 */
#define MSA_IS_FINAL(msa) ((msa)->A->automaton->is_final[(msa)->initial])

/**
 *
 */
#define MSA_HVALUE(sa,init) (((19 * (uintptr_t) (sa)) << 3) + 13 * (init))

/**
 *
 */
#define MSAI(msa) ((sataf_msa_internal) (msa))

/**
 *
 */
extern int
sataf_msa_unique_table_init (int ut_table_size, int ut_max_fill_degree);

/**
 *
 */
extern void
sataf_msa_unique_table_terminate (void);

/**
 *
 */
extern int
sataf_msa_sharing_init (size_t cache_size);

/**
 *
 */
extern void
sataf_msa_sharing_terminate (void);

/**
 *
 */
extern int
sataf_msa_cache_init (size_t cache_size);

/**
 *
 */
extern void
sataf_msa_cache_terminate (void);

extern void
sataf_msa_ut_statistics (ccl_log_type log);

/**
 *
 */
extern void
sataf_msa_cache_statistics (ccl_log_type log);

/**
 *
 */
extern void
sataf_msa_sharing_cache_statistics (ccl_log_type log);

/**
 *
 */
extern int
hopcroft_init (void);

/**
 *
 */
extern void
hopcroft_terminate (void);

/**
 *
 */
extern int
hopcroft_init2 (void);

/**
 *
 */
extern void
hopcroft_terminate2 (void);

/**
 *
 */
extern sataf_ea * 
sataf_ea_minimize_hopcroft (sataf_ea *ea, uint32_t *h);

extern sataf_ea * 
sataf_ea_minimize2 (sataf_ea *ea, uint32_t *h);

extern sataf_ea * 
sataf_ea_minimize_valmari (sataf_ea *ea, uint32_t *h);

extern sataf_ea * 
sataf_ea_cached_minimize (sataf_ea *ea, uint32_t *h,
			  sataf_ea * (*minfunc) (sataf_ea *ea, uint32_t *h));

/**
 * check if the shared automaton 'sa' is homomorphic to its first bound
 * marked_shared_automaton M. 'src' and 'letter' are such that 
 * succ(src,letter) yields to the exit state mapped to M.
 * 
 * The function returns a non-null value if an homomorphism is found.
 *
 * The size of tmp and h must be sa->nb_local_states
 */

extern uint32_t 
check_homomorphism (sataf_ea *ea, sataf_sa **bind_sa, uint32_t *bind_initial, 
		    uint32_t src, uint32_t letter, uint32_t *tmp, uint32_t *h);

extern uint32_t 
check_homomorphism1 (sataf_ea *ea, sataf_sa **bind_sa, uint32_t *bind_initial, 
		     uint32_t letter, uint32_t *tmp, uint32_t *h);

/**
 *
 */
extern sataf_msa * 
sataf_msa_project (sataf_msa *a, int i, int n, sataf_sa_transformer *T);


#endif /* ! __SATAF_P_H__ */
