/*
 * mao-quantifiers.c -- Boolean combination of residues of a MAO
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include "sataf.h"

sataf_mao *
sataf_mao_create_forall (sataf_mao *a)
{
  uint32_t i;
  uint32_t len = sataf_mao_get_alphabet_size (a);
  sataf_mao *tmp2;
  sataf_mao *R = sataf_mao_succ (a, 0);
  sataf_mao *tmp1;

  for (i = 1; i < len; i++)
    {
      tmp1 = sataf_mao_succ (a, i);
      tmp2 = sataf_mao_create_and (R, tmp1);
      sataf_mao_del_reference (tmp1);
      sataf_mao_del_reference (R);
      R = tmp2;
    }

  return R;
}

			/* --------------- */

sataf_mao *
sataf_mao_create_exists (sataf_mao *a)
{
  uint32_t i;
  uint32_t len = sataf_mao_get_alphabet_size (a);
  sataf_mao *tmp2;
  sataf_mao *R = sataf_mao_succ (a, 0);
  sataf_mao *tmp1;

  for (i = 1; i < len; i++)
    {
      tmp1 = sataf_mao_succ (a, i);
      tmp2 = sataf_mao_create_or (R, tmp1);
      sataf_mao_del_reference (tmp1);
      sataf_mao_del_reference (R);
      R = tmp2;
    }

  return R;
}

			/* --------------- */
