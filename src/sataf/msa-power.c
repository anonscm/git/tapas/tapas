/*
 * msa-power.c -- Exponentiation of a MSA
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-array.h>
#include <ccl/ccl-string.h>
#include <ccl/ccl-assert.h>
#include "sataf.h"

typedef struct power_st
{
  sataf_msa *L;
  int p;
} power;

typedef CCL_ARRAY(power) power_array;

typedef struct power_automaton_st
{
  sataf_mao super;
  power_array operands;
  sataf_msa *A;
} power_automaton;

			/* --------------- */

static sataf_mao * 
s_create_power (sataf_msa *a, int N);

static size_t
s_power_automaton_size (sataf_mao *self);

static void
s_power_automaton_destroy (sataf_mao *self);

#define s_power_automaton_no_cache NULL

#define s_power_automaton_is_root_of_scc NULL

#define s_power_automaton_simplify NULL

static uint32_t
s_power_automaton_get_alphabet_size (sataf_mao *self);

static char *
s_power_automaton_to_string (sataf_mao *self);

static sataf_mao * 
s_power_automaton_succ (sataf_mao *self, uint32_t letter);

static int
s_power_automaton_is_final (sataf_mao *self);

static int
s_power_automaton_equals (sataf_mao *self, sataf_mao *other);

static unsigned int
s_power_automaton_hashcode (sataf_mao *self);

# define s_power_automaton_to_dot NULL

			/* --------------- */

static int
s_power_compare (const power * c1, const power * c2);

static void
s_power_add_in_array (power * a, int *size, const power * c);

			/* --------------- */
static const sataf_mao_methods POWER_METHODS = 
  {
    "SA-POWER-OP",
    s_power_automaton_size,
    s_power_automaton_destroy,
    s_power_automaton_no_cache,
    s_power_automaton_is_root_of_scc,
    s_power_automaton_simplify,
    s_power_automaton_get_alphabet_size,
    s_power_automaton_to_string,
    s_power_automaton_succ,
    s_power_automaton_is_final,
    s_power_automaton_equals,
    s_power_automaton_hashcode,
    s_power_automaton_to_dot
  };

			/* --------------- */

sataf_msa *
sataf_msa_power (sataf_msa *a, int N)
{
  sataf_msa *R;

  ccl_pre (N >= 0);

  if (sataf_msa_is_zero (a))
    R = sataf_msa_add_reference (a);
  else if (N == 0)
    R = sataf_msa_epsilon (sataf_msa_get_alphabet_size (a));
  else if (sataf_msa_is_one (a) || N == 1)
    R = sataf_msa_add_reference (a);
  else if (N == 2)
    R = sataf_msa_concat (a, a);
  else
    {
      sataf_mao *aut = s_create_power (a, N);
      R = sataf_msa_compute (aut);
      sataf_mao_del_reference (aut);
    }

  return R;
}

			/* --------------- */

static sataf_mao * 
s_create_power (sataf_msa *a, int N)
{
  power_automaton *R = (power_automaton *)
    sataf_mao_create (sizeof (power_automaton), &POWER_METHODS);

  ccl_assert (N >= 2);

  if (sataf_msa_is_final (a))
    {
      int i;

      ccl_array_init_with_size (R->operands, N);
      for (i = 0; i < N; i++)
	{
	  R->operands.data[i].L = sataf_msa_add_reference (a);
	  R->operands.data[i].p = i;
	}
    }
  else
    {
      ccl_array_init_with_size (R->operands, 1);
      R->operands.data->L = sataf_msa_add_reference (a);
      R->operands.data->p = N - 1;
    }
  R->A = sataf_msa_add_reference (a);

  return (sataf_mao *) R;
}

			/* --------------- */

static size_t
s_power_automaton_size (sataf_mao *self)
{
  return sizeof (power_automaton);
}

			/* --------------- */

static void
s_power_automaton_destroy (sataf_mao *self)
{
  int i;
  power_automaton *a = (power_automaton *) self;

  for (i = 0; i < a->operands.size; i++)
    sataf_msa_del_reference (a->operands.data[i].L);
  ccl_array_delete (a->operands);
  sataf_msa_del_reference (a->A);
}

			/* --------------- */

static uint32_t
s_power_automaton_get_alphabet_size (sataf_mao *self)
{
  power_automaton *a = (power_automaton *) self;

  return sataf_msa_get_alphabet_size (a->A);
}

			/* --------------- */

static char *
s_power_automaton_to_string (sataf_mao *self)
{
  int i;
  power_automaton *a = (power_automaton *) self;
  char *result = NULL;

  for (i = 0; i < a->operands.size; i++)
    {
      ccl_string_format_append (&result,
				"(%p,%d) ", a->operands.data[i].L,
				a->operands.data[i].p);
    }
  return result;
}

			/* --------------- */

static sataf_mao * 
s_power_automaton_succ (sataf_mao *self, uint32_t letter)
{
  power s;
  int i, nb_c = 0;
  power_automaton *a = (power_automaton *) self;
  power *c = a->operands.data;
  power_array operands;
  sataf_mao *R;

  ccl_array_init (operands);

  for (i = a->operands.size; i; i--, c++)
    {
      s.L = sataf_msa_succ (c->L, letter);
      s.p = c->p;

      ccl_array_ensure_size_plus_one (operands);
      s_power_add_in_array (operands.data, &nb_c, &s);

      if (c->p != 0 && sataf_msa_is_final (c->L))
	{
	  if (sataf_msa_is_final (a->A))
	    {
	      int j;

	      for (j = 0; j < c->p - 1; j++)
		{
		  s.L = sataf_msa_succ (a->A, letter);
		  s.p = j;
		  ccl_array_ensure_size_plus_one (operands);
		  s_power_add_in_array (operands.data, &nb_c, &s);
		}
	    }

	  s.L = sataf_msa_succ (a->A, letter);
	  s.p = c->p - 1;
	  ccl_array_ensure_size_plus_one (operands);
	  s_power_add_in_array (operands.data, &nb_c, &s);
	}
    }

  ccl_assert (0 < nb_c && nb_c <= 2 * a->operands.size);

  {
    power_automaton *pa = (power_automaton *)
      sataf_mao_create (sizeof (power_automaton), &POWER_METHODS);

    ccl_array_trim (operands, nb_c);
    ccl_assert (operands.size == nb_c);
    pa->operands = operands;
    pa->A = sataf_msa_add_reference (a->A);
    R = (sataf_mao *) pa;
  }


  return (R);
}

			/* --------------- */

static int
s_power_automaton_is_final (sataf_mao *self)
{
  power_automaton *a = (power_automaton *) self;
  int nb_c = a->operands.size;
  power *c = a->operands.data;

  for (; nb_c--; c++)
    {
      if (sataf_msa_is_final (c->L) && c->p == 0)
	return 1;
    }

  return 0;
}

			/* --------------- */

static int
s_power_automaton_equals (sataf_mao *self, sataf_mao *other)
{
  power_automaton *a1 = (power_automaton *) self;
  power_automaton *a2 = (power_automaton *) other;
  int nb_c = a1->operands.size;
  power *c1 = a1->operands.data;
  power *c2 = a2->operands.data;

  if (nb_c != a2->operands.size)
    return 0;

  if (a1->A != a2->A)
    return 0;

  while (nb_c--)
    {
      if (c1->L != c2->L || c1->p != c2->p)
	return 0;
      c1++;
      c2++;
    }

  return 1;
}

			/* --------------- */

static unsigned int
s_power_automaton_hashcode (sataf_mao *self)
{
  power_automaton *a = (power_automaton *) self;
  int nb_c = a->operands.size;
  unsigned int r = nb_c;
  power *c = a->operands.data;

  while (nb_c--)
    {
      r = 19 * r + 717 * (uintptr_t) c->L + 31415 * c->p;
      c++;
    }

  return r;
}

			/* --------------- */

static int
s_power_compare (const power * c1, const power * c2)
{
  if (c1->L < c2->L)
    return -1;
  else if (c1->L > c2->L)
    return 1;
  else
    return c1->p - c2->p;
}

			/* --------------- */

static void
s_power_add_in_array (power * a, int *size, const power * c)
{
  int i;
  int cmp = 0;
  int sz = *size;

  for (i = 0; i < sz && (cmp = s_power_compare (a + i, c)) < 0; i++)
    continue;

  if (i == sz)
    a[sz++] = *c;
  else if (cmp == 0)
    sataf_msa_del_reference (c->L);
  else
    {
      int k;

      for (k = sz; k > i; k--)
	a[k] = a[k - 1];
      a[i] = *c;
      sz++;
    }

  *size = sz;
}
