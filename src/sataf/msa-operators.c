/*
 * msa-operators.c -- Boolean operations on MSA
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-memory.h>
#include "sataf-p.h"

# define DUP(a)  sataf_msa_add_reference (a)
# define DEL(a)  sataf_msa_del_reference (a)
# define ONE(a)  sataf_msa_is_one (a)
# define ZERO(a) sataf_msa_is_zero (a)
# define SZ(a)   ((a)->A->automaton->alphabet_size)

			/* --------------- */


typedef struct boolean_msa_operator_automaton_st
{
  sataf_mao super;
  sataf_msa *operands[3];
} boolean_msa_operator_automaton;

			/* --------------- */

static sataf_mao * 
s_create_boolean_automaton (sataf_msa **args, const sataf_mao_methods *m);

static sataf_mao * 
s_create_ternary (sataf_msa *arg1, sataf_msa *arg2, sataf_msa *arg3, 
		  const sataf_mao_methods *m);

static sataf_mao * 
s_create_binary (sataf_msa *arg1, sataf_msa *arg2, const sataf_mao_methods *m);

static sataf_mao * 
s_create_unary (sataf_msa *arg1, const sataf_mao_methods *m);

			/* --------------- */

static size_t
s_boolean_msa_operator_size (sataf_mao *self);

static void
s_boolean_msa_operator_destroy (sataf_mao *self);

static int s_boolean_msa_operator_no_cache (sataf_mao *self)
{
  return 0;
}

#define s_boolean_msa_operator_is_root_of_scc NULL

#define s_boolean_msa_operator_simplify_none NULL

static sataf_msa *
s_boolean_msa_operator_simplify_or (sataf_mao *self);

static sataf_msa *
s_boolean_msa_operator_simplify_xor (sataf_mao *self);

static sataf_msa *
s_boolean_msa_operator_simplify_and (sataf_mao *self);

static uint32_t
s_boolean_msa_operator_get_alphabet_size (sataf_mao *self);

#define s_boolean_msa_operator_to_string NULL

static sataf_mao * 
s_boolean_msa_operator_succ_not_commutative (sataf_mao *self, uint32_t letter);

static sataf_mao * 
s_boolean_msa_operator_succ_commutative (sataf_mao *self, uint32_t letter);

static int
s_boolean_msa_operator_is_final_OR (sataf_mao *self);

static int
s_boolean_msa_operator_is_final_XOR (sataf_mao *self);

static int
s_boolean_msa_operator_is_final_AND (sataf_mao *self);

static int
s_boolean_msa_operator_is_final_IMPLY (sataf_mao *self);

static int
s_boolean_msa_operator_is_final_EQUIV (sataf_mao *self);

static int
s_boolean_msa_operator_is_final_NOT (sataf_mao *self);

static int
s_boolean_msa_operator_is_final_ITE (sataf_mao *self);

static int
s_boolean_msa_operator_is_equal_to (sataf_mao *self, sataf_mao *other);

static unsigned int
s_boolean_msa_operator_hashcode (sataf_mao *self);

# define s_boolean_msa_operator_to_dot NULL

			/* --------------- */

# define DECL_OP_METHODS(ID,succ,simp)			    \
static const sataf_mao_methods ID ## _METHODS = { \
    "SA-BOOLEAN-OP", \
    s_boolean_msa_operator_size, \
    s_boolean_msa_operator_destroy, \
    s_boolean_msa_operator_no_cache, \
    s_boolean_msa_operator_is_root_of_scc, \
    s_boolean_msa_operator_simplify_ ## simp, \
    s_boolean_msa_operator_get_alphabet_size, \
    s_boolean_msa_operator_to_string, \
    s_boolean_msa_operator_succ_ ## succ, \
    s_boolean_msa_operator_is_final_ ## ID, \
    s_boolean_msa_operator_is_equal_to, \
    s_boolean_msa_operator_hashcode, \
    s_boolean_msa_operator_to_dot \
}

			/* --------------- */

DECL_OP_METHODS (OR, commutative, or);
DECL_OP_METHODS (XOR, commutative, xor);
DECL_OP_METHODS (AND, commutative, and);
DECL_OP_METHODS (IMPLY, not_commutative, none);
DECL_OP_METHODS (EQUIV, commutative, none);
DECL_OP_METHODS (NOT, not_commutative, none);
DECL_OP_METHODS (ITE, not_commutative, none);

			/* --------------- */

sataf_msa *
sataf_msa_ite (sataf_msa *i, sataf_msa *t, sataf_msa *e)
{
  sataf_msa *tmp;
  sataf_msa *R;

  if (ONE (i))
    R = DUP (t);
  else if (ZERO (i))
    R = DUP (e);
  else if (ONE (t))
    R = sataf_msa_or (i, e);
  else if (ZERO (t))
    {
      tmp = sataf_msa_not (i);
      R = sataf_msa_and (tmp, e);
      DEL (tmp);
    }
  else if (ZERO (e))
    R = sataf_msa_and (i, t);
  else if (ONE (e))
    {
      tmp = sataf_msa_not (i);
      R = sataf_msa_or (tmp, t);
      DEL (tmp);
    }
  else if ((R = sataf_msa_cache_get (i, t, e, &ITE_METHODS)) == NULL)
    {
      sataf_mao *ite = s_create_ternary (i, t, e, &ITE_METHODS);

      R = sataf_msa_compute (ite);

      sataf_mao_del_reference (ite);

      sataf_msa_cache_put (i, t, e, &ITE_METHODS, R);
    }

  return R;
}

			/* --------------- */

sataf_msa *
sataf_msa_not (sataf_msa *a)
{
  sataf_msa *R;

  if (ONE (a))
    R = sataf_msa_zero (SZ (a));
  else if (ZERO (a))
    R = sataf_msa_one (SZ (a));
  else if ((R = sataf_msa_cache_get (a, NULL, NULL, &NOT_METHODS)) == NULL)
    {
      sataf_mao *aut = s_create_unary (a, &NOT_METHODS);

      R = sataf_msa_compute (aut);

      sataf_mao_del_reference (aut);
      sataf_msa_cache_put (a, NULL, NULL, &NOT_METHODS, R);
    }

  return R;
}

			/* --------------- */


static sataf_msa *
s_apply_binary (sataf_msa *a1, sataf_msa *a2, const sataf_mao_methods *m)
{
  sataf_msa *R = sataf_msa_cache_get (a1, a2, NULL, m);

  if (R == NULL)
    {
      sataf_mao *sop = s_create_binary (a1, a2, m);

      R = sataf_msa_compute (sop);
      sataf_mao_del_reference (sop);
      sataf_msa_cache_put (a1, a2, NULL, m, R);
    }

  return R;
}

			/* --------------- */

static sataf_msa *
s_simplify_or (sataf_msa *a1, sataf_msa *a2)
{
  sataf_msa *R = NULL;

  if (ZERO (a1))
    R = DUP (a2);
  else if (ZERO (a2))
    R = DUP (a1);
  else if (ONE (a1))
    R = sataf_msa_one (SZ (a1));
  else if (ONE (a2))
    R = sataf_msa_one (SZ (a1));
  else if (a1 == a2)
    R = DUP (a1);

  return R;
}

			/* --------------- */

sataf_msa *
sataf_msa_or (sataf_msa *a1, sataf_msa *a2)
{
  sataf_msa *R = s_simplify_or (a1, a2);

  if (R == NULL)
    {
      if (a2 < a1)
	{
	  sataf_msa *tmp = a1;
	  a1 = a2;
	  a2 = tmp;
	}
      R = s_apply_binary (a1, a2, &OR_METHODS);
    }

  return R;
}

			/* --------------- */

static sataf_msa *
s_simplify_xor (sataf_msa *a1, sataf_msa *a2)
{
  sataf_msa *R = NULL;

  if (a1 == a2)
    R = sataf_msa_zero (SZ (a1));
  else if (ZERO (a1))
    R = DUP (a2);
  else if (ZERO (a2))
    R = DUP (a1);
  else if (ONE (a1))
    R = sataf_msa_not (a2);
  else if (ONE (a2))
    R = sataf_msa_not (a1);

  return R;
}

			/* --------------- */

sataf_msa *
sataf_msa_xor (sataf_msa *a1, sataf_msa *a2)
{
  sataf_msa *R = s_simplify_xor (a1, a2);

  if (R == NULL)
    {
      if (a2 < a1)
	{
	  sataf_msa *tmp = a1;
	  a1 = a2;
	  a2 = tmp;
	}
      R = s_apply_binary (a1, a2, &XOR_METHODS);
    }

  return R;
}

			/* --------------- */

static int
s_ea_is_complement (sataf_ea *ea1, sataf_ea *ea2)
{
  int result = 0;

  if (ea1->nb_local_states == ea2->nb_local_states &&
      ea1->nb_exit_states == ea2->nb_exit_states)
    {
      int i;

      result = 
	ccl_memcmp (ea1->successor, ea2->successor, 
		    (sizeof (uint32_t) * ea1->alphabet_size * 
		     ea1->nb_local_states)) == 0;

      for (i = 0; i < ea1->nb_local_states && result; i++)
	result = ((ea1->is_final[i] && ! ea2->is_final[i])
		  || (!ea1->is_final[i] && ea2->is_final[i]));
    }

  return result;
}

			/* --------------- */

static int
s_msa_is_complement (sataf_msa *a1, sataf_msa *a2);

static int
s_sa_is_complement (sataf_sa *a1, sataf_sa *a2)
{
  int i;
  int result = s_ea_is_complement (a1->automaton, a2->automaton);

  for (i = 0; result && i < a1->automaton->nb_exit_states; i++)
    result = s_msa_is_complement (a1->bind[i], a2->bind[i]);

  return result;
}

			/* --------------- */


static int
s_msa_is_complement (sataf_msa *a1, sataf_msa *a2)
{
  if (a1->initial != a2->initial)
    return 0;
  return s_sa_is_complement (a1->A, a2->A);
}

			/* --------------- */

int
sataf_msa_are_complements (sataf_msa *a1, sataf_msa *a2)
{
  return s_msa_is_complement (a1, a2);
}

			/* --------------- */

static sataf_msa *
s_simplify_and (sataf_msa *a1, sataf_msa *a2)
{
  sataf_msa *R = NULL;

  if (ZERO (a1))
    R = sataf_msa_zero (SZ (a1));
  else if (ZERO (a2))
    R = sataf_msa_zero (SZ (a1));
  else if (ONE (a1))
    R = DUP (a2);
  else if (ONE (a2))
    R = DUP (a1);
  else if (a1 == a2)
    R = DUP (a1);

  return R;
}

			/* --------------- */

sataf_msa *
sataf_msa_and (sataf_msa *a1, sataf_msa *a2)
{
  sataf_msa *R = s_simplify_and (a1, a2);

  if (R == NULL)
    {
      if (a2 < a1)
	{
	  sataf_msa *tmp = a1;
	  a1 = a2;
	  a2 = tmp;
	}
      R = s_apply_binary (a1, a2, &AND_METHODS);
    }

  return R;
}

			/* --------------- */

sataf_msa *
sataf_msa_imply (sataf_msa *a1, sataf_msa *a2)
{
  sataf_msa *R;

  if (ZERO (a1))
    R = sataf_msa_one (SZ (a1));
  else if (ZERO (a2))
    R = sataf_msa_not (a1);
  else if (ONE (a1))
    R = DUP (a2);
  else if (ONE (a2))
    R = sataf_msa_one (SZ (a1));
  else if (a1 == a2)
    R = sataf_msa_one (SZ (a1));
  else
    R = s_apply_binary (a1, a2, &IMPLY_METHODS);

  return R;
}

			/* --------------- */

sataf_msa *
sataf_msa_equiv (sataf_msa *a1, sataf_msa *a2)
{
  sataf_msa *R;

  if (ZERO (a1))
    R = sataf_msa_not (a2);
  else if (ONE (a1))
    R = DUP (a2);
  else if (ZERO (a2))
    R = sataf_msa_not (a1);
  else if (ONE (a2))
    R = DUP (a1);
  else if (a1 == a2)
    R = sataf_msa_one (SZ (a1));
  else
    {
      if (a2 < a1)
	{
	  sataf_msa *tmp = a1;
	  a1 = a2;
	  a2 = tmp;
	}
      R = s_apply_binary (a1, a2, &EQUIV_METHODS);
    }

  return R;
}

			/* --------------- */

sataf_msa * 
sataf_msa_forall (sataf_msa *a)
{
  sataf_msa *R = sataf_msa_cache_get (a, NULL, NULL, (void *) 0x2);

  if (R == NULL)
    {
      uint32_t i;
      uint32_t len = sataf_msa_get_alphabet_size (a);
      sataf_msa *tmp2;
      sataf_msa *tmp1;

      sataf_msa *R = sataf_msa_succ (a, 0);
      for (i = 1; i < len; i++)
	{
	  tmp1 = sataf_msa_succ (a, i);
	  tmp2 = sataf_msa_and (R, tmp1);
	  sataf_msa_del_reference (tmp1);
	  sataf_msa_del_reference (R);
	  R = tmp2;
	}

      sataf_msa_cache_put (a, NULL, NULL, (void *) 0x2, R);
    }

  return R;
}

			/* --------------- */

sataf_msa * 
sataf_msa_exists (sataf_msa *a)
{
  sataf_msa *R = sataf_msa_cache_get (a, NULL, NULL, (void *) 0x1);

  if (R == NULL)
    {
      uint32_t i;
      uint32_t len = sataf_msa_get_alphabet_size (a);
      sataf_msa *tmp2;
      sataf_msa *tmp1;

      R = sataf_msa_succ (a, 0);
      for (i = 1; i < len; i++)
	{
	  tmp1 = sataf_msa_succ (a, i);
	  tmp2 = sataf_msa_or (R, tmp1);
	  sataf_msa_del_reference (tmp1);
	  sataf_msa_del_reference (R);
	  R = tmp2;
	}
      sataf_msa_cache_put (a, NULL, NULL, (void *) 0x1, R);
    }

  return R;
}

			/* --------------- */

static sataf_mao * 
s_create_boolean_automaton (sataf_msa **args, const sataf_mao_methods *m)
{
  boolean_msa_operator_automaton *R;

  R = (boolean_msa_operator_automaton *)
    sataf_mao_create (sizeof (struct boolean_msa_operator_automaton_st),
			    m);


  R->operands[0] = sataf_msa_add_reference (args[0]);
  if (args[1])
    {
      R->operands[1] = sataf_msa_add_reference (args[1]);
      if (args[2])
	R->operands[2] = sataf_msa_add_reference (args[2]);
    }

  return (sataf_mao *) R;
}

			/* --------------- */

static sataf_mao * 
s_create_ternary (sataf_msa *arg1, sataf_msa *arg2, sataf_msa *arg3,
		  const sataf_mao_methods *m)
{
  sataf_msa *args[3];
  sataf_mao *R;

  args[0] = arg1;
  args[1] = arg2;
  args[2] = arg3;
  R = s_create_boolean_automaton (args, m);

  return R;
}

			/* --------------- */

static sataf_mao * 
s_create_binary (sataf_msa *arg1, sataf_msa *arg2, const sataf_mao_methods *m)
{
  return s_create_ternary (arg1, arg2, NULL, m);
}

			/* --------------- */


static sataf_mao * 
s_create_unary (sataf_msa *arg1, const sataf_mao_methods *m)
{
  return s_create_binary (arg1, NULL, m);
}

			/* --------------- */

static size_t
s_boolean_msa_operator_size (sataf_mao *self)
{
  return sizeof (boolean_msa_operator_automaton);
}

			/* --------------- */

static void
s_boolean_msa_operator_destroy (sataf_mao *self)
{
  boolean_msa_operator_automaton *boa = 
    (boolean_msa_operator_automaton *) self;

  if (boa->operands[0] != NULL)
    DEL (boa->operands[0]);

  if (boa->operands[1] != NULL)
    DEL (boa->operands[1]);

  if (boa->operands[2] != NULL)
    DEL (boa->operands[2]);
}

			/* --------------- */

static sataf_msa * 
s_boolean_msa_operator_simplify_or (sataf_mao *self)
{
  boolean_msa_operator_automaton *boa = 
    (boolean_msa_operator_automaton *) self;

  return s_simplify_or (boa->operands[0], boa->operands[1]);
}

			/* --------------- */

static sataf_msa * 
s_boolean_msa_operator_simplify_xor (sataf_mao *self)
{
  boolean_msa_operator_automaton *boa = 
    (boolean_msa_operator_automaton *) self;

  return s_simplify_xor (boa->operands[0], boa->operands[1]);
}

			/* --------------- */

static sataf_msa * 
s_boolean_msa_operator_simplify_and (sataf_mao *self)
{
  boolean_msa_operator_automaton *boa = 
    (boolean_msa_operator_automaton *) self;

  return s_simplify_and (boa->operands[0], boa->operands[1]);
}

			/* --------------- */

static uint32_t
s_boolean_msa_operator_get_alphabet_size (sataf_mao *self)
{
  boolean_msa_operator_automaton *boa = 
    (boolean_msa_operator_automaton *) self;

  return boa->operands[0]->A->automaton->alphabet_size;
}

			/* --------------- */

static sataf_mao * 
s_boolean_msa_operator_succ_not_commutative (sataf_mao *self,
					     uint32_t letter)
{
  boolean_msa_operator_automaton *boa = 
    (boolean_msa_operator_automaton *) self;
  boolean_msa_operator_automaton *R = (boolean_msa_operator_automaton *)
    sataf_mao_create (sizeof (struct boolean_msa_operator_automaton_st),
			    self->methods);



  R->operands[0] = sataf_msa_succ (boa->operands[0], letter);
  if (boa->operands[1])
    {
      R->operands[1] = sataf_msa_succ (boa->operands[1], letter);
      if (boa->operands[2])
	R->operands[2] = sataf_msa_succ (boa->operands[2], letter);
    }

  return (sataf_mao *) R;
}

			/* --------------- */

static sataf_mao * 
s_boolean_msa_operator_succ_commutative (sataf_mao *self, uint32_t letter)
{
  boolean_msa_operator_automaton *boa = 
    (boolean_msa_operator_automaton *) self;
  boolean_msa_operator_automaton *R = (boolean_msa_operator_automaton *)
    sataf_mao_create (sizeof (boolean_msa_operator_automaton),
		      self->methods);

  ccl_pre (boa->operands[1] && !boa->operands[2]);

  R->operands[0] = sataf_msa_succ (boa->operands[0], letter);
  R->operands[1] = sataf_msa_succ (boa->operands[1], letter);
  if (R->operands[0] > R->operands[1])
    {
      sataf_msa *tmp = R->operands[0];
      R->operands[0] = R->operands[1];
      R->operands[1] = tmp;
    }
  return (sataf_mao *) R;
}

			/* --------------- */

static int
s_boolean_msa_operator_is_final_OR (sataf_mao *self)
{
  boolean_msa_operator_automaton *boa = 
    (boolean_msa_operator_automaton *) self;

  return MSA_IS_FINAL (boa->operands[0]) || MSA_IS_FINAL (boa->operands[1]);
}

			/* --------------- */

static int
s_boolean_msa_operator_is_final_XOR (sataf_mao *self)
{
  boolean_msa_operator_automaton *boa = 
    (boolean_msa_operator_automaton *) self;

  return (MSA_IS_FINAL (boa->operands[0]) && !MSA_IS_FINAL (boa->operands[1]))
    || (!MSA_IS_FINAL (boa->operands[0]) && MSA_IS_FINAL (boa->operands[1]));
}

			/* --------------- */

static int
s_boolean_msa_operator_is_final_AND (sataf_mao *self)
{
  boolean_msa_operator_automaton *boa = 
    (boolean_msa_operator_automaton *) self;

  return MSA_IS_FINAL (boa->operands[0]) && MSA_IS_FINAL (boa->operands[1]);
}


			/* --------------- */

static int
s_boolean_msa_operator_is_final_NOT (sataf_mao *self)
{
  boolean_msa_operator_automaton *boa = 
    (boolean_msa_operator_automaton *) self;

  return !MSA_IS_FINAL (boa->operands[0]);
}

			/* --------------- */

static int
s_boolean_msa_operator_is_final_IMPLY (sataf_mao *self)
{
  boolean_msa_operator_automaton *boa = 
    (boolean_msa_operator_automaton *) self;

  return !MSA_IS_FINAL (boa->operands[0]) || MSA_IS_FINAL (boa->operands[1]);
}

			/* --------------- */

static int
s_boolean_msa_operator_is_final_EQUIV (sataf_mao *self)
{
  boolean_msa_operator_automaton *boa = 
    (boolean_msa_operator_automaton *) self;
  int v0 = MSA_IS_FINAL (boa->operands[0]);
  int v1 = MSA_IS_FINAL (boa->operands[1]);

  return (v0 && v1) || (!v0 && !v1);
}

			/* --------------- */

static int
s_boolean_msa_operator_is_final_ITE (sataf_mao *self)
{
  boolean_msa_operator_automaton *boa = 
    (boolean_msa_operator_automaton *) self;

  if (MSA_IS_FINAL (boa->operands[0]))
    return MSA_IS_FINAL (boa->operands[1]);
  return MSA_IS_FINAL (boa->operands[2]);
}

			/* --------------- */

static int
s_boolean_msa_operator_is_equal_to (sataf_mao *self, sataf_mao *other)
{
  boolean_msa_operator_automaton *boa1 = 
    (boolean_msa_operator_automaton *) self;
  boolean_msa_operator_automaton *boa2 =
    (boolean_msa_operator_automaton *) other;

  return self->methods->is_final == other->methods->is_final
    && boa1->operands[0] == boa2->operands[0]
    && boa1->operands[1] == boa2->operands[1]
    && boa1->operands[2] == boa2->operands[2];
}

			/* --------------- */

static unsigned int
s_boolean_msa_operator_hashcode (sataf_mao *self)
{
  boolean_msa_operator_automaton *boa = 
    (boolean_msa_operator_automaton *) self;
  return (unsigned int) ((uintptr_t) self->methods->is_final)
    + 21911 * (uintptr_t) (boa->operands[0])
    + 2729 * (uintptr_t) (boa->operands[1])
    + 163 * (uintptr_t) (boa->operands[2]);
}
