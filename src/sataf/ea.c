/*
 * ea.c -- Exit Automata Implementation
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include <ccl/ccl-log.h>
#include <ccl/ccl-memory.h>
#include "sataf-p.h"

			/* --------------- */

typedef struct msa_manager_st
{
  int init_table_size;
  int fill_degree;
  int table_size;
  sataf_ea **table;
  int nb_elements;
} ea_manager;

			/* --------------- */

#define MUST_INCREASE_TABLE(man) \
  ((man)->nb_elements > (man)->table_size * (man)->fill_degree)

#define MUST_DECREASE_TABLE(man) \
((man)->nb_elements < (man)->table_size * (man)->fill_degree && \
 (man)->table_size > (man)->init_table_size)

			/* --------------- */


static void
s_ea_manager_resize_table (uint32_t new_size);

			/* --------------- */

static ea_manager *EA_MANAGER;

			/* --------------- */

int
sataf_ea_init (int ut_table_size, int ut_max_fill_degree)
{
  int result;

  ccl_pre (ut_table_size > 0);
  ccl_pre (ut_max_fill_degree > 1);

  result = hopcroft_init ();
  if (result)
    {
      EA_MANAGER = ccl_new (ea_manager);
      EA_MANAGER->init_table_size = EA_MANAGER->table_size = ut_table_size;
      EA_MANAGER->fill_degree = ut_max_fill_degree;
      EA_MANAGER->table = ccl_new_array (sataf_ea *, EA_MANAGER->table_size);
      EA_MANAGER->nb_elements = 0;
    }

  return result;
}

			/* --------------- */

void
sataf_ea_terminate (void)
{
  hopcroft_terminate ();

  if (EA_MANAGER->nb_elements != 0)
    {
      int i = EA_MANAGER->table_size;

      ccl_warning ("WARNING: EA uniqueness table still contains elements\n");
      while (i--)
	{
	  sataf_ea *ea;
	  sataf_ea *next;

	  for (ea = EA_MANAGER->table[i]; ea; ea = next)
	    {
	      next = ea->next;
	      ccl_delete (ea);
	    }
	}
    }
  ccl_delete (EA_MANAGER->table);
  ccl_delete (EA_MANAGER);
}

			/* --------------- */

sataf_ea *
sataf_ea_create (uint32_t nb_states, uint32_t nb_exits, uint32_t alphabet_size)
{
  size_t to_alloc;
  sataf_ea *result;

  ccl_pre (nb_states > 0 && alphabet_size > 0);

  to_alloc = sizeof (struct sataf_ea_st) +
    (nb_states / 4 + 1) * sizeof (uint32_t) +
    (nb_states * alphabet_size) * sizeof (uint32_t);

  result = ccl_malloc (to_alloc);

  ccl_memzero (result, to_alloc);
  result->refcount = 1;
  result->alphabet_size = alphabet_size;
  result->nb_local_states = nb_states;
  result->nb_exit_states = nb_exits;
  result->is_final =
    (uint8_t *) (result->successor + (nb_states * alphabet_size));

  return result;
}

			/* --------------- */

sataf_ea *
sataf_ea_complement (sataf_ea *ea)
{
  uint32_t i = ea->nb_local_states;
  sataf_ea *R = sataf_ea_create (ea->nb_local_states, ea->nb_exit_states,
				 ea->alphabet_size);
  while (i--)
    R->is_final[i] = !ea->is_final[i];

  ccl_memcpy (R->successor, ea->successor,
	      sizeof (uint32_t) * ea->nb_local_states * ea->alphabet_size);

  return R;
}

			/* --------------- */

sataf_ea *
sataf_ea_create_with_arrays (uint32_t nb_states, uint32_t nb_exits,
			     uint32_t alphabet_size, const uint8_t *is_final,
			     const uint32_t *succ)
{
  sataf_ea *R = sataf_ea_create (nb_states, nb_exits, alphabet_size);

  ccl_pre (is_final != NULL);
  ccl_pre (succ != NULL);

  ccl_memcpy (R->is_final, is_final, sizeof (uint8_t) * nb_states);
  ccl_memcpy (R->successor, succ,
	      sizeof (uint32_t) * nb_states * alphabet_size);

  return R;
}

			/* --------------- */

sataf_ea *
sataf_ea_add_reference (sataf_ea *ea)
{
  ccl_pre (ea != NULL);
  ccl_pre ((ea->refcount & 0x7FFFFFFF) > 0);

  ea->refcount++;

  return ea;
}

			/* --------------- */

static sataf_ea **
s_find_sataf_ea (sataf_ea *ea)
{
  sataf_ea **pea;
  int index = sataf_ea_hashcode (ea) % EA_MANAGER->table_size;


  for (pea = EA_MANAGER->table + index; *pea && *pea != ea &&
	 ! sataf_ea_equals (*pea, ea); pea = &((*pea)->next))
    /* do nothing */ ;

  return pea;
}

			/* --------------- */


void
sataf_ea_del_reference (sataf_ea *ea)
{
  ccl_pre (ea != NULL);
  ccl_pre ((ea->refcount & 0x7FFFFFFF) > 0);

  ea->refcount--;

  if ((ea->refcount & 0x7FFFFFFF) != 0)
    return;

  if ((ea->refcount & 0x80000000) != 0)
    {
      sataf_ea **pea = s_find_sataf_ea (ea);

      ccl_assert (*pea != NULL);

      *pea = ea->next;
      EA_MANAGER->nb_elements--;
      if (MUST_DECREASE_TABLE (EA_MANAGER))
	s_ea_manager_resize_table ((EA_MANAGER->table_size - 13) >> 1);
    }

  ccl_delete (ea);
}


			/* --------------- */

void
sataf_ea_set_successor (sataf_ea *ea, uint32_t src, uint32_t a, uint32_t tgt, 
			int is_exit)
{
  ccl_pre (ea != NULL);
  ccl_pre (src < ea->nb_local_states);
  ccl_pre (ccl_imply (is_exit, tgt < ea->nb_exit_states));
  ccl_pre (ccl_imply (! is_exit, tgt < ea->nb_local_states));
  ccl_pre (a < ea->alphabet_size);

  if (is_exit)
    ea->successor[ea->alphabet_size * src + a] =
      sataf_ea_encode_succ_as_exit_state (tgt);
  else
    ea->successor[ea->alphabet_size * src + a] =
      sataf_ea_encode_succ_as_local_state (tgt);
}

			/* --------------- */

void
sataf_ea_display_as_dot (sataf_ea *ea, ccl_log_type log, 
			       const char **alphabet)
{
  size_t state, letter;

  ccl_log (log, "digraph sataf_ea_%p {\n", ea);

  for (state = 0; state < ea->nb_local_states; state++)
    {
      const char *fmt = ea->is_final[state]
	? "L%d[shape=ellipse,peripheries=2];\n" : "L%d[shape=ellipse];\n";
      ccl_log (log, fmt, state);
    }
  for (state = 0; state < ea->nb_exit_states; state++)
    {
      ccl_log (log, "E%d[shape=octagon];\n", state);
    }

  for (state = 0; state < ea->nb_local_states; state++)
    {
      for (letter = 0; letter < ea->alphabet_size; letter++)
	{
	  uint32_t succ = sataf_ea_get_successor (ea, state, letter);
	  const char *fmt = "L%d -> L%d";

	  if (sataf_ea_is_exit_state (succ))
	    {
	      fmt = "L%d -> E%d ";
	    }

	  ccl_log (log, fmt, state, sataf_ea_decode_succ_state (succ));

	  if (alphabet == NULL)
	    ccl_log (log, "[label=\"%d\"];\n", letter);
	  else
	    ccl_log (log, "[label=\"%s\"];\n", alphabet[letter]);
	}
    }

  ccl_log (log, "}\n");
}

			/* --------------- */

sataf_ea *
sataf_ea_find_or_add (sataf_ea *ea)
{
  sataf_ea *result;
  sataf_ea **pea;

  ccl_pre (ea != NULL);

  if ((ea->refcount & 0x80000000) != 0)
    result = sataf_ea_add_reference (ea);
  else if (*(pea = s_find_sataf_ea (ea)) != NULL)
    result = sataf_ea_add_reference (*pea);
  else
    {
      result = *pea = sataf_ea_add_reference (ea);
      result->refcount |= 0x80000000;
      EA_MANAGER->nb_elements++;
      if (MUST_INCREASE_TABLE (EA_MANAGER))
	s_ea_manager_resize_table ((EA_MANAGER->table_size << 1) + 13);
    }

  return result;
}

			/* --------------- */

sataf_ea *
sataf_ea_find (sataf_ea *ea)
{
  sataf_ea *R = *s_find_sataf_ea (ea);

  if (R != NULL)
    R = sataf_ea_add_reference (R);

  return R;
}

			/* --------------- */

int
sataf_ea_equals (sataf_ea *ea1, sataf_ea *ea2)
{
  ccl_pre (ea1 != NULL);
  ccl_pre (ea2 != NULL);

  if (ea1 == ea2)
    return 1;

  return ea1->nb_local_states == ea2->nb_local_states
    && ea1->alphabet_size == ea2->alphabet_size
    && ea1->nb_exit_states == ea2->nb_exit_states
    && ccl_memcmp (ea1->successor, ea2->successor,
		   sizeof (uint8_t) * ea1->nb_local_states +
		   sizeof (uint32_t) * ea1->nb_local_states *
		   ea1->alphabet_size) == 0;
}

			/* --------------- */

unsigned int
sataf_ea_hashcode (sataf_ea *ea)
{
  unsigned int N, s, r = 0;

  ccl_pre (ea != NULL);
  N = ea->nb_local_states * ea->alphabet_size;

  for (s = 0; s < ea->nb_local_states; s++)
    r = 87719 * r + 19 * ea->is_final[s];
  for (s = 0; s < N; s++)
    r = 10949 * r + 17 * ea->successor[s];

  return r;
}

			/* --------------- */

void
sataf_ea_ut_statistics (ccl_log_type log)
{
  int i;
  uint32_t nb_queue = 0;
  uint32_t nb_refcount = 0;
  uint32_t min_refcount = 0xFFFFFFFF;
  uint32_t max_refcount = 0;
  uint32_t max_e_size = 0;

  for (i = 0; i < EA_MANAGER->table_size; i++)
    {
      sataf_ea *ea = EA_MANAGER->table[i];

      if (ea == NULL)
	continue;
      nb_queue++;
      for (; ea != NULL; ea = ea->next)
	{
	  uint32_t r = ea->refcount & 0x7FFFFFFF;

	  nb_refcount += r;
	  if (r < min_refcount)
	    min_refcount = r;
	  if (r > max_refcount)
	    max_refcount = r;
	  if (ea->nb_exit_states > max_e_size)
	    max_e_size = ea->nb_exit_states;
	}
    }

  ccl_log (log, "EA uniqueness table statistics :\n");
  ccl_log (log, "--------------------------------\n");
  ccl_log (log, " init size = %d\n", EA_MANAGER->init_table_size);
  ccl_log (log, " fill degree = %d\n", EA_MANAGER->fill_degree);
  ccl_log (log, " table size = %d\n", EA_MANAGER->table_size);
  ccl_log (log, " nb entries = %d\n", EA_MANAGER->nb_elements);
  ccl_log (log, " mean collision list len = %4.2f\n",
	   (float) EA_MANAGER->nb_elements / (float) nb_queue);
  ccl_log (log, " mean sharing count = %4.2f\n",
	   (float) nb_refcount / (float) EA_MANAGER->nb_elements);
  ccl_log (log, " refcount range = [%d, %d]\n", min_refcount, max_refcount);
  ccl_log (log, " max number of exit states  = %d\n", max_e_size);
  ccl_log (log, "\n");
}

			/* --------------- */

static void
s_ea_manager_resize_table (uint32_t new_size)
{
  uint32_t i = EA_MANAGER->table_size;
  sataf_ea **new_table = ccl_new_array (sataf_ea *, new_size);
  sataf_ea **entry = EA_MANAGER->table;
  sataf_ea *ea;
  sataf_ea *next;
  uint32_t index;

  if (new_table == NULL)
    return;

  while (i--)
    {
      for (ea = *(entry++); ea; ea = next)
	{
	  next = ea->next;

	  index = sataf_ea_hashcode (ea) % new_size;
	  ea->next = new_table[index];
	  new_table[index] = ea;
	}
    }

  EA_MANAGER->table_size = new_size;
  ccl_delete (EA_MANAGER->table);
  EA_MANAGER->table = new_table;
}
