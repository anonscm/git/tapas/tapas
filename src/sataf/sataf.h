/*
 * sataf.h -- SATAF library main header file
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!  
 * \page sataf The SATAF Library
 *
 * \defgroup maogrp Marked Automaton Objects 
 * @{
 * @}
 *
 * \defgroup eagrp Exit Automata
 * @{
 * @}
 *
 * \defgroup sagrp Shared Automata
 * @{
 * @}
 *
 * \defgroup msagrp Marked Shared Automata
 * @{
 * @}
 */
#ifndef __SATAF_H__
# define __SATAF_H__

# include <tapas-config.h>

# include <ccl/ccl-list.h>
# include <ccl/ccl-hash.h>
# include <ccl/ccl-log.h>

BEGIN_C_DECLS 

/*!\addtogroup maogrp
 * @{
 */

/*!
 * \brief Type of a Marked Automaton Object (MAO).
 *
 * This type is used as an abstraction of derived objects. In order to create
 * a new class of MAO, the first member of the derived class must be a field
 * of type \ref sataf_mao.
 */
typedef struct sataf_mao_st sataf_mao;

/*!
 * \brief Type of the data structure gathering methods that a Marked Automaton
 * Object has to implement. This structure can be embedded in a larger one (as 
 * the first field) in order to create an inheritance of class interfaces.
 */
typedef struct sataf_mao_methods_st sataf_mao_methods;
/*! @} */

/*!\addtogroup msagrp
 * @{
 */
/*!
 * \brief Type for the data structure encoding Marked Shared Automata.
 */
typedef struct sataf_msa_st sataf_msa;
/*! @} */

/*!\addtogroup sagrp
 * @{
 */
/*!
 * \brief Type for the data structure encoding Shared Automata.
 */
typedef struct sataf_sa_st sataf_sa;
/*! @} */

/*!\addtogroup eagrp
 * @{
 */
/*!
 * \brief Type for the data structure encoding Exit Automata.
 */
typedef struct sataf_ea_st sataf_ea;
/*! @} */

/*!\addtogroup maogrp
 * @{
 */

/*!
 * \brief Structure gathering the methods applied to a Marked Automaton Object.
 */
struct sataf_mao_methods_st
{
  /*!
   * \brief A constant string of characters indicating the type of the MAO.
   * 
   * This field is used, in particular, to check if two MAOs are equal.
   *
   * \see sataf_mao_equals
   */
  const char *type;

  /*!
   * \brief Returnsthe actual size of the object \a self. 
   *
   * This method is used to store MAOs by pages (see ccl-pool.h).
   * \pre self != NULL
   */
  size_t (*size) (sataf_mao *self);

  /*!
   * \brief Free ressources allocated to this MAO.
   *
   * If this method is implemented it is called when the reference counter
   * of \a self falls to zero.
   * \pre self != NULL
   * \see sataf_mao_del_reference
   */
  void (*destroy) (sataf_mao *self);

  /*!
   * \brief Caching this MAO is not relevant. 
   *
   * This method indicates to the sharing algorithm that it does not have to 
   * put \a self in its computation cache. If this method is not implemented 
   * all instances of this MAO class are cached.
   * \pre self != NULL
   */
  int (*no_cache) (sataf_mao *self);

  /*!
   * \brief Notify the MAO that it has been identified as the root of a SCC.
   *
   * This method indicates to the implementation of the MAO that the sharing
   * algorithm has identified this node to be the root of some SCC i.e. a
   * shared automaton.
   *
   * \pre self != NULL
   * \pre sa != NULL
   */
  void (*is_root_of_scc) (sataf_mao *self, sataf_sa *sa, int q0);

  /*!
   * \brief Direct computation of the equivalent MSA.
   *
   * This function is used by the sharing algorithm to check if it is possible
   * to obtain directly the MSA equivalent to \a self without using the 
   * canonicalization algorithm. This is particularly useful when \a self
   * is known to be equal to the empty or full set.
   * \pre self != NULL
   */
  sataf_msa *(*simplify) (sataf_mao *self);

  /*!
   * \brief Size of the reference alphabet.
   * 
   * This method is called to get the number of letters of the alphabet used
   * by \a self. 
   * \remark This function MUST be implemented.
   * \pre self != NULL
   * \post result > 0
   */
  uint32_t (*get_alphabet_size) (sataf_mao *self);

  /*!
   * \brief Human readable version of \a self
   * 
   * This function is used when \a self is displayed to a user.
   * \pre self != NULL
   */ 
  char *(*to_string) (sataf_mao *self);

  /*!
   * \brief Compute the successor MAO of \a self by the letter \a a.
   *
   * This function returns the MAO corresponding to the underlying automaton of
   * \a self but marked by the successor by \a a of the state marking \a self.
   * \remark
   * - This method MUST be implemented.
   * - This method MUST verify the following constraint: 
   *  sataf_mao_equals (sataf_mao_succ (self, a), sataf_mao_succ (self, a))
   * \pre self != NULL 
   * \pre letter <= sataf_mao_get_alphabet_size(self)
   */
  sataf_mao *(*succ) (sataf_mao *self, uint32_t a);

  /*!
   * \brief Indicates if \a self is accepting.
   *
   * This function returns a non-null value iff the state defining \a self
   * is a final state of the automaton \a self.
   *
   * \remark This method MUST be implemented.
   * \pre self != NULL
   */
  int (*is_final) (sataf_mao *self);

  /*!
   * \brief Check if two instance are equal.
   * 
   * This function returns a non-null value iff the language recognized from
   * \a self and the one recognized from \a other are equal.
   *
   * \remark This method MUST be implemented.
   * \pre self != NULL && other != NULL
   * \pre self->type == other->type
   * \pre sataf_mao_get_alphabet_size (self) == 
   * sataf_mao_get_alphabet_size (other)
   */
  int (*equals) (sataf_mao *self, sataf_mao *other);

  /*!
   * \brief Hashcode for the MAO \a self.
   *
   * \pre self != NULL
   */
  unsigned int (*hashcode) (sataf_mao *self);

  /*!
   * \brief Display the automaton in DOT file format.
   *
   * \pre self != NULL
   */
  void (*to_dot) (ccl_log_type log, sataf_mao *self, const char **alphabet,
		  const char *graph_name, const char *graph_type);
};
/*! @} */

/*!\addtogroup msagrp
 * @{
 */

/*!
 * \brief Data structure gathering quantitative informations related to a MSA.
 * This structure is filled by the function ::sataf_msa_get_info.
 */
typedef struct 
{
  /*!
   * \brief Number of Shared Automata used by the MSA. This number represents
   * the number of strongly connected components of the minimal automaton.
   */
  uint32_t nb_shared_automata; 

  /*!
   * \brief Number of Exit Automata in this MSA. This is the number of 
   * non isomorphic strongly connected components: if there is any component   
   * is not isomorphic to another one then this field is identical to 
   * \ref sataf_msa_info::nb_shared_automata "nb_shared_automata".
   */
  uint32_t nb_exit_automata;

  /*!
   * \brief Total number of states. This is exactly the number of states of 
   * the minimal automaton. 
   */
  uint32_t nb_states;

  /*!
   * \brief The height of the MSA in the DAG of SCCs.
   */
  uint32_t depth;
  uint32_t to0;

  /*!
   * \brief The number of references to the MSA.
   */
  uint32_t refcount;
} sataf_msa_info;
/*! @} */


/*!\addtogroup msagrp
 * @{
 */
typedef void sataf_sa_transformer (sataf_ea *ea, sataf_sa **bind_sa,
				   uint32_t *bind_init);
/*! @} */				      

			/* --------------- */

/**
 *
 */
typedef struct sataf_mao_memorizer_st sataf_mao_memorizer;

/**
 *
 */
typedef struct {
  sataf_mao *A;
} sataf_mao_memo;

/**
 *
 */
typedef void sataf_mao_memo_init_proc (sataf_mao_memo *m, void *data);

/**
 *
 */
typedef void sataf_mao_memo_clean_proc (sataf_mao_memo *m);

			/* --------------- */

/*!
 * \brief Initialization of the SATAF library.
 * 
 * \param msa_op_cache_size The maximal number of operations on MSAs that can 
 * be cached.
 * \param mao_sharing_cache_size The maximal number of MAO to MSA association
 * couples that can be cached.
 * \param ea_ut_table_size The initial size of the unicity table of
 * \param ea_ut_max_fill_degree
 * \param sa_ut_table_size
 * \param sa_ut_max_fill_degree
 * \return a non-null value if the initialization procedure terminates 
 * successfully.
 */
extern int
sataf_init (int msa_op_cache_size, int mao_sharing_cache_size, 
	    int ea_ut_table_size, int ea_ut_max_fill_degree, 
	    int sa_ut_table_size, int sa_ut_max_fill_degree,
	    int msa_ut_table_size, int msa_ut_max_fill_degree);

/*!
 * \brief Clean-up the resources allocated to this library.
 */
extern void
sataf_terminate (void);

/*!
 * \brief Display statistics on the MSA data struture and its related caches.
 *
 * \param log the CCL output stream
 */
extern void
sataf_log_statistics (ccl_log_type log);

/*! \addtogroup maogrp
 * @{
 */

/*!
 * \brief Allocation and initialization of a MAO.
 *
 * \param size the size in bytes required to store the object
 * \param methods the pointers to the MAO methods
 * \pre size >= sizeof (sataf_mao)
 * \pre methods != NULL
 *
 * \return a newly allocated 
 */
extern sataf_mao * 
sataf_mao_create (size_t size, const sataf_mao_methods *methods);

/*!
 * \brief Increments the number of references to this MAP
 *
 * This macro increments the counter of references of the considered object
 * and returns the address of the object.
 *
 * \param a the referenced MAO
 * \pre a != NULL
 *
 * \return a 
 */
static inline sataf_mao *
sataf_mao_add_reference(sataf_mao *a);

/*!
 * \brief Decrements the number of references to the given MAO
 *
 * This macro decrements the counter of references of the considered object.
 * If this counter falls to zero then, if it is implemented, the \ref 
 * sataf_mao_methods.destroy "destroy" method is called on the object. The 
 * memory occupied by the object is then freed.
 *
 * \param a the dereferenced MAO
 * \pre a != NULL
 */
static inline void
sataf_mao_del_reference(sataf_mao *a);

/*!
 * \brief Translate the MAO into a human readable form. 
 * 
 * This funciton returns the result of the \ref sataf_mao_methods.to_string 
 * "to_string" method applied to \a self. If the MAO does not implement it then
 * the string "\a type@ \a addr" is returned, where \a type is the result of 
 * \ref sataf_mao_get_type and \a addr is the address of \a self.
 * 
 * \param self the MAO argument
 * \pre self != NULL
 *
 * \return a "human-readable" corresponding to \a self
 */
extern char * 
sataf_mao_to_string (sataf_mao *self);

/*!
 * \brief Return the type of the MAO \a self
 * 
 * The type of a MAO is a constant string assigned to the static member \ref 
 * sataf_mao_methods.type "type" of the object. This type is used to compare
 * objects (see \ref sataf_mao_equals).
 *
 * \param self the MAO argument
 * \pre self != NULL
 *
 * \return the string encoding the type of \a self
 */
static inline const char *
sataf_mao_get_type (sataf_mao *self);

/*!
 * \brief Notify the MAO that it has been identified as the root of a SCC.
 *
 * This method indicates to the implementation of the MAO that the sharing
 * algorithm has identified this node to be the root of some SCC i.e. a
 * shared automaton.
 *
 * \param self the MAO argument
 * \param sa the shared automaton pointing the SCC
 * \param q0 the entry point in the SCC
 *
 * \pre self != NULL
 * \pre sa != NULL
 */
extern void
sataf_mao_is_root_of_scc (sataf_mao *self, sataf_sa *sa, int q0);

/*! 
 * \brief Return if caching this object is interesting or not.
 * 
 * This function returns the result of the \ref sataf_mao_methods.no_cache 
 * "no_cache" method or 0 if it is no implemented.
 *
 * \param self the MAO argument
 * \pre self != NULL
 *
 * \return a non-null value if \a self has not to be cached.
 */
extern int 
sataf_mao_no_cache (sataf_mao *self);

/*!
 * \brief Return the MSA corresponding to the MAO \a self if it is computable
 * without the canonization algorithm.
 * This function returns the result of the \ref sataf_mao_methods.simplify 
 * "simplify" method if it is implemented or NULL if not.
 *
 * \param self the simplified MAO
 * \pre self != NULL
 *
 * \return the MSA equivalent to \a self or NULL if the result is unknown.
 */
extern sataf_msa * 
sataf_mao_simplify (sataf_mao *self);

/*!
 * \brief The size of the alphabet used by \a self.
 * \param self the MAO argument
 * \pre self != NULL
 *
 * \return the size of the alphabet
 */
extern uint32_t 
sataf_mao_get_alphabet_size (sataf_mao *self);

/*!
 * \brief Return the successor MAO of \a self by \a letter 
 *
 * \param self the MAO argument
 * \param letter the letter labelling the transition
 *
 * \pre letter < sataf_mao_get_alphabet_size (self)
 * \pre self implements the \ref sataf_mao_methods.succ "succ" method.
 *
 * \return a non-null value if the state of the automaton corresponding to the 
 * MAO \a self is accepting.
 */
extern sataf_mao * 
sataf_mao_succ (sataf_mao *self, uint32_t letter);

/*!
 * \brief Return if the Marked Automaton is accepting.
 * \param self the MAO argument
 * \pre self != NULL
 *
 * \return a non-null value if the state of the automaton corresponding to the 
 * MAO \a self is accepting.
 */
extern int 
sataf_mao_is_final (sataf_mao *self);

/*!
 * \brief Check if two MAO are equal.
 * 
 * Two MAOs are equal:
 * - if they have the same \ref sataf_mao_get_type "type", and
 * - if they have the same alphabet size, and
 * - if the \ref sataf_mao_methods.equals "equals" method returns a non-null 
 *   value.
 * If the method is not implemented then the function returns \a self == other.
 *
 * \param self the MAO argument
 * \param other the MAO argument
 *
 * \pre self != NULL && otehr != NULL
 *
 * \return a non-null value if \a self and \a other recognizes are the same 
 * objects.
 */
extern int 
sataf_mao_equals (sataf_mao *self, sataf_mao *other);

/*!
 * \brief Compute an hashcode for this MAO.
 * 
 * If the MAO does not implement the \ref sataf_mao_methods.hashcode "hashcode"
 * function the address \a self is returned.

 * \param self the MAO argument
 * \pre self != NULL
 *
 * \return an hashcode for \a self
 */
extern unsigned int 
sataf_mao_hashcode (sataf_mao *self);

/*!
 * \brief Display a MAO in DOT file format.
 *
 * If the object does not implement the \ref sataf_mao_methods.to_dot "to_dot"
 * method then the \ref sataf_mao_default_to_dot is called.
 *
 * \param log the CCL output stream
 * \param self the MAO translated into DOT
 * \param alphabet an array of strings used as letters 
 * \param graph_name used to name the graph in the DOT file format
 * \param graph_type the DOT keyword defining the type of the graph (digraph or
 * graph).
 */
extern void 
sataf_mao_to_dot (ccl_log_type log, sataf_mao *self, const char **alphabet,
		  const char *graph_name, const char *graph_type);

/*!
 * \brief Display a MAO in DOT file format.
 * 
 * The nodes are labelled by the result of the \ref sataf_mao_methods.to_string
 * "to_string" method; if this one is not implemented then a unique integer is 
 * used. The array \a alphabet is used to label transition; if the array is
 * NULL then the index of the letter is used.
 * 
 * \param log the CCL output stream
 * \param self the MAO translated into DOT
 * \param alphabet an array of strings used as letters 
 * \param graph_name used to name the graph in the DOT file format
 * \param graph_type the DOT keyword defining the type of the graph (digraph or
 * graph).
 */
extern void 
sataf_mao_default_to_dot (ccl_log_type log, sataf_mao *self, 
			  const char **alphabet, const char *graph_name, 
			  const char *graph_type);

/*!
 * \brief Create a MAO encoding the empty language \f$\emptyset \subset 
 * \Sigma^*\f$ for a reference alphabet \f$\Sigma\f$ of size \a alphabet_size.
 * \param alphabet_size the size of the reference alphabet
 * \pre alphabet_size > 0
 *
 * \return \f$\emptyset\f$
 */
extern sataf_mao * 
sataf_mao_create_zero (uint32_t alphabet_size);

/*!
 * \brief Create a MAO recognizing \f$\Sigma^*\f$
 * \param alphabet_size the size of the alphabet \f$\Sigma\f$
 * \pre alphabet_size > 0
 *
 * \return \f$\Sigma^*\f$ with \f$|\Sigma| = \mbox{alphabet\_size}\f$
 */
extern sataf_mao * 
sataf_mao_create_one (uint32_t alphabet_size);

/*!
 * \brief Create a MAO recognizing the singleton \f$\{\epsilon\}
 * \subset \Sigma^*\f$ for a reference alphabet \f$\Sigma\f$ of size \a 
 * alphabet_size.
 * \param alphabet_size the size of the alphabet
 * \pre alphabet_size > 0
 *
 * \return \f$\{\epsilon\}\f$
 */
extern sataf_mao * 
sataf_mao_create_epsilon (uint32_t alphabet_size);

/*!
 * \brief Create a MAO recognizing the singleton \f$\{a\} \subset \Sigma^*\f$ 
 * for a reference alphabet \f$\Sigma\f$ of size \a alphabet_size and one of 
 * its letter \a a.
 * \param alphabet_size the size of the alphabet
 * \param a the recognized letter
 * \pre alphabet_size > 0
 * \pre a < alphabet_size
 *
 * \return \f$\{a\}\f$
 */
extern sataf_mao * 
sataf_mao_create_letter (uint32_t alphabet_size, uint32_t a);

/*!
 * \brief Create a MAO recognizing the entire alphabet \f$\Sigma\f$ that
 * has a size equal to \a alphabet_size.
 * \param alphabet_size the size of the alphabet
 * \pre alphabet_size > 0
 *
 * \return \f$\Sigma\f$
 */
extern sataf_mao * 
sataf_mao_create_alphabet (uint32_t alphabet_size);

/*!
 * \brief Create a MAO encoding an automaton given its transition function and 
 * its acceptance condition.
 *
 * This function creates a MAO encoding an automaton with \a nb_states, 
 * recognizing from the state \a s0 words over an alphabet of size 
 * \a alphabet_size. The acceptance condition is given by an array of Booleans
 * \a is_final: the state \a i is accepting iff \a "is_final[i]" is non-null.
 * The transition function of the resulting automaton is given by the array
 * \a succ of size \a nb_states\f$\times\f$\a alphabet_size and such that
 * <em>succ[s\f$\times\f$ alphabet_size+a]</em> is the successor of the state 
 * \a s by the letter \a a.
 * 
 * \param alphabet_size the size of the reference alphabet 
 * \param is_final the acceptance condition 
 * \param nb_states the number of states if the automaton
 * \param s0 the index of the initial state
 * \param succ the transition function
 * \pre is_final != NULL
 * \pre nb_states > 0
 * \pre s0 < nb_states
 * \pre succ != NULL
 *
 * \return the MAO encoding of the explicitly given automaton
 */
extern sataf_mao * 
sataf_mao_create_from_arrays (uint32_t nb_states, uint32_t alphabet_size,
			      uint32_t s0, const uint8_t *is_final,
			      const uint32_t *succ);

/*!
 * \brief Create a MAO encoding the language <em>if-then-else(i,t,e)</em>
 *
 * \param i the 'if' argument
 * \param t the 'then' argument
 * \param e the 'else' argument
 *
 * \pre i != NULL && t != NULL && e != NULL
 * \pre sataf_mao_get_alphabet_size (i) == sataf_mao_get_alphabet_size (t)
 * \pre sataf_mao_get_alphabet_size (i) == sataf_mao_get_alphabet_size (e)
 *
 * \return a MAO encoding then <em>if-then-else(i,t,e)</em>
 */
extern sataf_mao * 
sataf_mao_create_ite (sataf_mao *i, sataf_mao *t, sataf_mao *e);

/*!
 * \brief Create a MAO encoding the complement of \a a
 *
 * \param a the complemented MAO 
 *
 * \pre a != NULL
 *
 * \return a MAO encoding then \f$\Sigma^*\setminus L_a\f$
 */
extern sataf_mao * 
sataf_mao_create_not (sataf_mao *a);

/*!
 * \brief Create a MAO encoding the union of two languages
 *
 * \param a1 the first language
 * \param a2 the second language
 *
 * \pre a1 != NULL && a2 != NULL
 * \pre sataf_mao_get_alphabet_size (a1) == sataf_mao_get_alphabet_size (a2)
 *
 * \return a MAO encoding the \f$L_{a_1}\cup L_{a_2}\f$
 */
extern sataf_mao * 
sataf_mao_create_or (sataf_mao *a1, sataf_mao *a2);

/*!
 * \brief Create a MAO encoding the intersection of two languages
 *
 * \param a1 the first language
 * \param a2 the second language
 *
 * \pre a1 != NULL && a2 != NULL
 * \pre sataf_mao_get_alphabet_size (a1) == sataf_mao_get_alphabet_size (a2)
 *
 * \return a MAO encoding the \f$L_{a_1}\cap L_{a_2}\f$
 */
extern sataf_mao * 
sataf_mao_create_and (sataf_mao *a1, sataf_mao *a2);

/*!
 * \brief Create a MAO encoding the union of a finite set of languages
 *
 * \param operands the set of languages
 *
 * \pre operands != NULL
 * \pre ccl_list_get_size (operands) > 0
 *
 * \return a MAO encoding \f$\bigcup_{a\in operands} L_a\f$
 */
extern sataf_mao * 
sataf_mao_create_multi_or (ccl_list *operands);

/*!
 * \brief Create a MAO encoding the intersection of a finite set of languages
 *
 * \param operands the set of languages
 *
 * \pre operands != NULL
 * \pre ccl_list_get_size (operands) > 0
 *
 * \return a MAO encoding \f$\bigcap_{a\in operands} L_a\f$
 */
extern sataf_mao * 
sataf_mao_create_multi_and (ccl_list *operands);

/*!
 * \brief Create a MAO encoding \f$L_{a_1} \Rightarrow L_{a_2}\f$
 *
 * \param a1 the first MAO argument 
 * \param a2 the first MAO argument 
 *
 * \pre a1 != NULL && a2 != NULL
 * \pre sataf_mao_get_alphabet_size (a1) == sataf_mao_get_alphabet_size (a2)
 *
 * \return a MAO encoding the \f$L_{a_1}\Rightarrow L_{a_2}\f$
 */
extern sataf_mao * 
sataf_mao_create_imply (sataf_mao *a1, sataf_mao *a2);

/*!
 * \brief Create a MAO encoding \f$L_{a_1} \Leftrightarrow L_{a_2}\f$
 *
 * \param a1 the first MAO argument 
 * \param a2 the first MAO argument 
 *
 * \pre a1 != NULL && a2 != NULL
 * \pre sataf_mao_get_alphabet_size (a1) == sataf_mao_get_alphabet_size (a2)
 *
 * \return a MAO encoding the \f$L_{a_1}\Leftrightarrow L_{a_2}\f$
 */
extern sataf_mao * 
sataf_mao_create_equiv (sataf_mao *a1, sataf_mao *a2);

/*!
 * \brief Create a MAO encoding the intersection of residues of \f$L_a\f$
 *
 * \param a the MAO argument
 *
 * \pre a != NULL
 *
 * \return a MAO encoding \f$\bigcap_{\alpha\in \Sigma} \alpha^{-1}L_a\f$
 */
extern sataf_mao * 
sataf_mao_create_forall (sataf_mao *a);

/*!
 * \brief Create a MAO encoding the union of residues of \f$L_a\f$
 *
 * \param a the MAO argument
 *
 * \pre a != NULL
 *
 * \return a MAO encoding \f$\bigcup_{\alpha\in \Sigma} \alpha^{-1}L_a\f$
 */
extern sataf_mao * 
sataf_mao_create_exists (sataf_mao *a);


/*!
 * \brief Create a new memorizer.
 *
 * \param memo_size the size of the entries
 * \param init a pointer to the function initializing entries
 * \param init_data a pointer to data used bu the \a init function
 * \param clean a pointer to a function used to free resources allocated to an
 * entry.
 *
 * \return the newly created memorizer
 */
extern sataf_mao_memorizer *
sataf_mao_memorizer_create (size_t memo_size, sataf_mao_memo_init_proc *init,
			    void *init_data, sataf_mao_memo_clean_proc *clean);

/*!
 *
 */
extern void
sataf_mao_memorizer_delete (sataf_mao_memorizer *M);

/**
 *
 */
extern sataf_mao_memo *
sataf_mao_memorizer_remind (sataf_mao_memorizer *M, sataf_mao *A);

/**
 *
 */
extern sataf_mao_memo *
sataf_mao_memorizer_succ (sataf_mao_memorizer *M, sataf_mao *A, int a);

/*! @} */


/*!\addtogroup eagrp
 * @{
 */

/*!
 * \brief Encode the successor state \a index as a local state.
 */
static inline uint32_t
sataf_ea_encode_succ_as_local_state (uint32_t index);


/*!
 * \brief Encode the successor state \a index as an exit state.
 */
static inline uint32_t 
sataf_ea_encode_succ_as_exit_state (uint32_t index);

/*!
 * \brief Decode the successor state \a index.
 */
static inline uint32_t
sataf_ea_decode_succ_state (uint32_t index);

/*!
 * \brief Check if the given \a successor corresponds to an exit state or not.
 */
static inline int
sataf_ea_is_exit_state (uint32_t successor);


/*!
 * \brief Check if the given \a successor corresponds to a local state or not.
 */
static inline int
sataf_ea_is_local_state (uint32_t successor);

/*!
 * \brief Return the size of this alphabet used by the exit automaton \a ea.
 * \param ea the exit automaton
 * \pre ea != NULL
 */
static uint32_t
sataf_ea_get_alphabet_size (sataf_ea *ea);

/*!
 * \brief Return the number of local states of \a ea.
 * \param ea the exit automaton
 * \pre ea != NULL
 */
static inline uint32_t
sataf_ea_get_nb_states (sataf_ea *ea);

/*!
 * \brief Return the number of exit states of \a ea.
 * \param ea the exit automaton
 * \pre ea != NULL
 */
static inline uint32_t
sataf_ea_get_nb_exits (sataf_ea *ea);

/*!
 * \brief Return a non null value is the local state \a s of \a ea is 
 * accepting or not.
 * \param ea the exit automaton
 * \param s the tested state
 * \pre ea != NULL
 * \pre s < sataf_ea_get_nb_states(ea)
 */
static inline int
sataf_ea_is_final (sataf_ea *ea, uint32_t s);

/*!
 * \brief Get the successor of \a state by the letter \a a.
 *
 * As mentionned above, the returned integer encodes both the successor state
 * the flag indicating if it is a local or an exit state.
 * \param ea the exit automaton
 * \param state the considered state
 * \param a the later used to go out of \a state.
 * \pre ea != NULL
 * \pre state < sataf_ea_get_nb_states(ea)
 * \pre a < sataf_ea_get_alphabet_size(ea)
 */
static inline uint32_t
sataf_ea_get_successor (sataf_ea *ea, uint32_t state, uint32_t a);

/*!
 * \brief Check if \a ea accepts nothing.
 * \param ea the exit automaton
 * \pre ea != NULL
 */
static inline int
sataf_ea_is_zero (sataf_ea *ea);

/*!
 * \brief Check if \a ea accepts any word.
 * \param ea the exit automaton
 * \pre ea != NULL
 */
static inline int
sataf_ea_is_one (sataf_ea *ea);


/*!
 * \brief Check if \a ea recognizes anything or nothing.
 * \param ea the exit automaton
 * \pre ea != NULL
 */
static inline int
sataf_ea_is_zero_or_one (sataf_ea *ea);

/*!
 * \brief Build an empty Exit Automaton data structure.
 * 
 * This function allocates a new EA with \a nb_states local states, \a nb_exits
 * exit states. The automaton reads word over the alphabet. The transition
 * function and the acceptance condition are filled with zero thus the 
 * automaton is an automaton (not necessarily minimal) recognizing the empty
 * language.
 *
 * \param nb_states the number of local states
 * \param nb_exits the number of exit states
 * \param alphabet_size the size of the alphabet
 * \pre nb_states > 0 
 * \pre alphabet_size > 0
 */
extern sataf_ea *
sataf_ea_create (uint32_t nb_states, uint32_t nb_exits, 
		 uint32_t alphabet_size);

/*!
 * \brief Build an EA isomorph to \a ea up to the acceptance condition 
 * which is negated. 
 * 
 * This function returns an EA strictly equal to \a ea except for the 
 * acceptance condition; this latter is inversed for each state. Thus, if \a ea
 * is an automaton with no exit states, the resulting automaton recognizes 
 * exactly the complement of the language recognized by \a ea.
 *
 * \param ea the exit automaton
 * \pre ea != NULL
 */
extern sataf_ea *
sataf_ea_complement (sataf_ea *ea);

/*!
 * \brief Build an EA with the transition function \a succ and the acceptance
 * condition \a is_final.
 *
 * This function constructs an EA like sataf_ea_create (nb_states, nb_exits,
 * alphabet_size) but it fills the transition function and the acceptance
 * condition with the arrays \a succ and \a is_final.
 *
 * The array \a succ must have \a nb_states \f$\times\f$ \a alphabet_size 
 * entries. Each entry \a succ[alphabet_size*s+a] represents the encoded index
 * of the successor of \a s following the letter \a a. If \a t is the index
 * of the successor of \a s by \a a then the entry \a succ[alphabet_size*s+a] 
 * is:
 * - sataf_ea_encode_succ_as_exit_state(t) if \a t is an exit state
 * - sataf_ea_encode_succ_as_local_state(t) if \a t is a local state
 *
 * The array \a is_final is assumed to have \a nb_states entries. 
 * \a is_final[i] must be non-null if and only if the local state \a i is 
 * accepting.
 *
 * \param nb_states the number of local states of the new EA
 * \param nb_exits its number of exit states
 * \param alphabet_size the cardinal of the alphabet
 * \param is_final the acceptance condition
 * \param succ the transition function  
 * \pre succ != NULL && is_final != NULL
 * \pre nb_states > 0 && alphabet_size > 0
 */
extern sataf_ea *
sataf_ea_create_with_arrays (uint32_t nb_states, uint32_t nb_exits,
			     uint32_t alphabet_size, const uint8_t *is_final,
			     const uint32_t *succ);

/*!
 * \brief Increment the counter of references of \a ea.
 *
 * This function is used to inform the library that \a ea is referenced one 
 * more time.
 * \param ea the exit automaton
 * \pre ea != NULL
 * \return \a ea
 */
extern sataf_ea *
sataf_ea_add_reference (sataf_ea *ea);

/*!
 * \brief Decrement the counter of references of \a ea.
 * 
 * If the counter of references of \a ea falls to zero, the memory allocated to
 * it is freed.
 *
 * \param ea the exit automaton
 * \pre ea != NULL
 */
extern void
sataf_ea_del_reference (sataf_ea *ea);

/*!
 * \brief Change the successor of the given state.
 * 
 * The procedure changes the successor by the letter \a of the state \a src.
 * The new successor is \a tgt which is considered as an exit state if 
 * \a is_exit is non-null. Note that, oppositely to 
 * sataf_ea_create_with_arrays, the state \a tgt is the actual index of the
 * successor, not its encoded version.
 *
 * \param ea the exit automaton
 * \param src the source state
 * \param a the letter labelling the transition
 * \param tgt the index of the new successor
 * \param is_exit a Boolean indicating if tgt is an exit state.
 * \pre ea != NULL
 * \pre src < ea->nb_local_states
 * \pre is_exit \f$\Rightarrow\f$ tgt < ea->nb_exit_states
 * \pre \f$\neg\f$ is_exit \f$\Rightarrow\f$ tgt < ea->nb_local_states
 * \pre a < ea->alphabet_size
 */
extern void
sataf_ea_set_successor (sataf_ea *ea, uint32_t src, uint32_t a, uint32_t tgt, 
			int is_exit);

/*!
 * \brief Change the acceptance condition for the state \a s
 *
 * \param ea the exit automaton
 * \param s the state
 * \param is_final a Boolean indicating if \a s is accepting or not
 * \pre ea != NULL
 * \pre s < ea->nb_local_states
 */
static inline void
sataf_ea_set_final (sataf_ea *ea, uint32_t s, int is_final);

/*!
 * \brief Display the exit automaton in DOT graph format.
 *
 * Accepting states are drawn with a double circle and exit states with 
 * octagon. If an \a alphabet is given (i.e. not NULL) then its letters are
 * used to label transitions in place of their index.
 * 
 * \param ea the exit automaton
 * \param log the CCL output stream
 * \param alphabet an optional array of strings used for letters
 *
 * \pre ea != NULL
 *
 * \see http://www.graphviz.org
 */
extern void
sataf_ea_display_as_dot (sataf_ea *ea, ccl_log_type log, 
			 const char **alphabet);

/*!
 * \brief Minimization of \a ea.
 * 
 * This function computes the minimal exit automaton equivalent to \a ea.
 * The array \a h must be of size sataf_ea_get_alphabet_size (ea). \a h is
 * filled with the homomorphism mapping each state of \a ea to its equivalent
 * class in the minimal EA.
 *
 * \param ea the exit automaton
 * \param h the homomorphism from \a ea to the minimal automaton.
 *
 * \pre ea != NULL && h != NULL
 *
 * \return the minimal EA recognizing the state language than \a ea
 */
extern sataf_ea *
sataf_ea_minimize (sataf_ea *ea, uint32_t *h);

/*!
 * \brief Find or add \a ea in the unicity table.
 *
 * In order to share EAs, the library maintains a table containing a unique
 * version of each minimal EAs computed by the sharing algorithm. Two check
 * is two unique EAs are equal it is sufficient to test the equality of their
 * address.
 * 
 * \param ea the exit automaton
 * \pre ea != NULL
 *
 * \return \a ea or its unique equivalent.
 */
extern sataf_ea *
sataf_ea_find_or_add (sataf_ea *ea);

/*!
 * \brief Return the unique version of \a ea if it exists.
 *
 * \param ea the exit automaton
 * \pre ea != NULL
 *
 * \return the unique minimal EA isomorphic to \a ea or NULL if it is not 
 * present in the unicity table.
 */
extern sataf_ea *
sataf_ea_find (sataf_ea *ea);

/*!
 * \brief Check that \a ea1 and \a ea2 are equal.
 * 
 * Two EAs are equal if they have the same number of (local and exit) states,
 * the same alphabet size and if the transition function and acceptance table
 * are equal.
 *
 * \param "ea1, ea2" the compared EAs
 * \pre ea1 != NULL && ea2 != NULL
 * \return an non-null value if the content of \a ea1 and \a ea2 are equal.
 */
extern int
sataf_ea_equals (sataf_ea *ea1, sataf_ea *ea2);

/*!
 * \brief Hashcode of an EA
 * The integer value is computed according to the transition function and the
 * acceptance table of the EA.
 *
 * \param ea the exit automaton argument
 * \pre ea != NULL
 *
 * \return an hashcode for \a ea
 */
extern unsigned int
sataf_ea_hashcode (sataf_ea *ea);

/*!
 * \brief Display internal statistics about the use of EAs. In particular 
 * informations related to the unicity table.
 *
 * \param log the CCL output stream
 */
extern void
sataf_ea_ut_statistics (ccl_log_type log);

/*! @} */

/*!\addtogroup sagrp
 * @{
 */

/*! 
 * \brief Create the unique SA recognizing all words on an alphabet of size 
 * \a alphabet_size.
 * 
 * \param alphabet_size the size of the alphabet.
 * \pre alphabet_size > 0
 */
extern sataf_sa * 
sataf_sa_create_one (uint32_t alphabet_size);

/*! 
 * \brief Create the unique SA recognizing no words on an alphabet of size 
 * \a alphabet_size.
 * 
 * \param alphabet_size the size of the alphabet.
 * \pre alphabet_size > 0
 */
extern sataf_sa * 
sataf_sa_create_zero (uint32_t alphabet_size);

/*! 
 * \brief Add one the counter of references to \a sa.
 *
 * \param sa the shared automaton
 * \pre sa != NULL
 * \return sa
 */
static inline sataf_sa *
sataf_sa_add_reference (sataf_sa *sa);

/*! 
 * \brief Decrement the number of references to \a sa.
 *
 * When this counter falls to zero the resources allocated to \a sa are freed.
 *
 * \param sa the shared automaton
 * \pre sa != NULL
 */
static inline void
sataf_sa_del_reference (sataf_sa *sa);

/*!
 * \brief Look-up in the SA-unicity-table for a shared automaton defined
 * by \a ea and the binding function (\a bind_sa, \a bind_init).
 *
 * The library maintains a unicity table for shared automata. This function
 * looks up in this table for a shared automaton binding each couple of
 * (\a bind_sa[i], \a bind_init[i]) to the exit state \a i of \a ea.
 *
 * \param ea the exit automaton of the SA
 * \param bind_sa the SAs associated to exit states of \a ea
 * \param bind_init the initial states associated to exit states of \a ea
 *
 * \pre ea != NULL && bind_sa != NULL && bind_init != NULL
 * \pre bind_sa and bind_init must have sataf_ea_get_nb_exits (ea).
 *
 * \return a pointer to the unique SA (ea,bind_sa,bind_init).
 */
extern sataf_sa * 
sataf_sa_find_or_add (sataf_ea *ea, sataf_sa **bind_sa, uint32_t *bind_init);

/*!
 * \brief Display the shared automaton sa in DOT file format.
 *
 * Each node represents a state of the represented automaton \a sa i.e. without
 * exit states. Each node is labelled with:
 * - a string L\@d-s where d is the depth of the SA and s the state in this SA.
 * - the address of the SA the node belong to.
 * - the address of the EA underlying the SA the node belong to.
 *
 * A state (typically an initial state) \a marked_state of the top-level shared
 * automaton \a sa can be distinguished using different color. If 
 * \a marked_state is negative then all states are displayed in white.
 *
 * \param sa the displayed SA
 * \param log the CCL output stream
 * \param marked_state a differenciated state
 * \param alphabet an array of strings used as letters.
 *
 * \pre sa != NULL
 *
 * \see http://www.graphviz.org/
 */
extern void
sataf_sa_display_as_dot (sataf_sa *sa, ccl_log_type log, int marked_state, 
			 const char **alphabet, int with_info);

/*!
 * \brief Display the graph of SCCs composing the shared automaton \a sa.
 * 
 * Each node displayed in the graph represents a SA. Edge between nodes are
 * labelled by integers. An edge \f$N_1 \stackrel{i}{\longrightarrow} N_2\f$
 * indicates there exists an entry in the binding function of \f$N_1\f$ 
 * yielding into the state \a i of \f$N_2\f$.
 *
 * \param sa the displayed SA
 * \param log the CCL output stream
 * \pre sa != NULL
 */
extern void
sataf_sa_display_scc_as_dot (sataf_sa *sa, ccl_log_type log);

/*!
 * \brief Return if \a sa is the shared automaton accepting no word.
 * 
 * \param sa the shared automaton
 * \pre sa != NULL
 *
 * \return a non-null value if \a sa recognize no word.
 */
static inline int
sataf_sa_is_zero (sataf_sa *sa);

/*!
 * \brief Return the size of the alphabet.
 * \param sa the considered shared automaton
 * \pre sa != NULL
 * \return the size of the underlying alphabet 
 */
static inline uint32_t
sataf_sa_get_alphabet_size (sataf_sa *sa);

/*!
 * \brief Return if \a sa is the shared automaton accepting any word.
 * 
 * \param sa the shared automaton
 * \pre sa != NULL
 *
 * \return a non-null value if \a sa recognize all words.
 */
static inline int
sataf_sa_is_one (sataf_sa *sa);

/*! 
 * \brief Check if \a sa accepts nothing or accepts nothing.
 *
 * \param sa the shared automaton
 * \pre sa != NULL
 *
 * \return sataf_sa_is_one (sa) || sataf_sa_is_zero (sa)
 */
static inline int
sataf_sa_is_zero_or_one (sataf_sa *sa);

/*! 
 * \brief Display internal statistics about the use of SAs. In particular 
 * informations related to the unicity table.
 * \param log the CCL ouptut stream
 */
extern void
sataf_sa_table_statistics (ccl_log_type log);

extern void
sataf_sa_exists_closure (sataf_ea *ea, sataf_sa **bind_sa, 
			 uint32_t *bind_init);

extern void
sataf_sa_forall_closure (sataf_ea *ea, sataf_sa **bind_sa, 
			 uint32_t *bind_init);
/* @} */

/*!\addtogroup msagrp
 * @{
 */

/*!
 * \brief Getting the unique MSA defined by the couple (\a sa,\a initial).
 *
 * This function looks up in the unicity table of MSAs for the one defined by
 * (\a sa, \a initial). The state \a initial must be a valid state of the
 * strongly connected component defining \a sa. 
 * \param sa the shared automaton defining the result.
 * \param initial the state to mark in \a sa
 * \pre sa != NULL 
 * \return MSA(\a sa, \a initial).
 */
extern sataf_msa * 
sataf_msa_find_or_add (sataf_sa *sa, uint32_t initial);

/*!
 * \brief Return the marked state defining \a msa.
 *
 * \param msa the considered MSA
 * \pre msa != NULL
 * \return the marked state defining \a msa.
 */
static inline uint32_t
sataf_msa_get_state (sataf_msa *msa);

/*!
 * \brief Return the shared automaton defining \a msa.
 *
 * \param msa the considered MSA
 * \pre msa != NULL
 * \return the shared automaton defining \a msa.
 */
static inline sataf_sa *
sataf_msa_get_sa (sataf_msa *msa);

/*!
 * \brief Return the shared automaton defining \a msa but without incrementing
 * its counter of references.
 *
 * \param msa the considered MSA
 * \pre msa != NULL
 * \return the shared automaton defining \a msa.
 */
static inline sataf_sa *
sataf_msa_get_sa_no_ref (sataf_msa *msa);

/*!
 * \brief Minimize, canonicalize and share the given MAO \a A.
 *
 * This function is equivalent to \ref sataf_msa_compute_with_transformer 
 * (A, NULL).
 * \param A the MAO to canonicalize
 * \pre A != NULL
 * \return the unique MSA encoding the same language than \a A.
 */
static inline sataf_msa *
sataf_msa_compute (sataf_mao *A);

/*!
 * \brief Minimize, canonicalize and share the given MAO \a A.
 *
 * This is the function transforming a generic automaton given as a MAO into
 * the shared and canonical data structure called MSA. 
 *
 * Prior the call to the minimization algorithm the function \a T is called
 * with the exit automaton and the binding function used to create the minimal
 * shared component. This function can be used to change the acceptance
 * condition.
 *
 * \param A the MAO to canonicalize
 * \param T a transformation function
 * \pre A != NULL
 * \return the unique MSA encoding the language recognized by \a A.
 */
extern sataf_msa *
sataf_msa_compute_with_transformer (sataf_mao *A, sataf_sa_transformer *T);

/*!
 * \brief Create a MAO from \a msa.
 *
 * \param msa the considered MSA
 * \pre msa != NULL
 * \return a MAO having the same behaviors then \a msa
 */
extern sataf_mao *
sataf_msa_to_mao (sataf_msa * msa);

/*! 
 * \brief Display on \a log quantitative informations about the structure of 
 * \a msa.
 *
 * \param msa the considered MSA
 * \param log the CCL output stream
 * \pre msa != NULL
 */
extern void
sataf_msa_log_info (ccl_log_type log, sataf_msa *msa);

/*!
 * \brief Retrieve quantitative informations related to \a msa.
 *
 * This function fills the structure pointed by \a info.
 * \param msa the considered MSA
 * \param info structure receiving values
 * \pre msa != NULL && info != NULL
 */
extern void
sataf_msa_get_info (sataf_msa *msa, sataf_msa_info *info);

/*!
 * \brief Return the successor of \a msa by the letter \a a.
 *
 * \param msa the considered MSA
 * \param a the letter
 * \pre msa != NULL
 * \pre a < sataf_msa_get_alphabet_size (msa)
 * \return the successor MSA of \a msa by \a a
 */
extern sataf_msa * 
sataf_msa_succ (sataf_msa *msa, uint32_t a);

/*!
 * \brief Return if \a msa accepts the empty word.
 *
 * This function checks if the state of the automaton underlying to \a msa
 * is accepting or not. 
 * \param msa the considered MSA
 * \pre msa != NULL
 * \return a non-null value if \a msa accepts \f$\epsilon\f$.
 */
static inline int
sataf_msa_is_final (sataf_msa *msa);

/*!
 * \brief Return the size of the alphabet.
 *
 * \param msa the considered marked shared automaton
 * \pre msa != NULL
 * \return the size of the underlying alphabet 
 */
static inline uint32_t
sataf_msa_get_alphabet_size (sataf_msa *msa);

/*!
 * \brief Increment the counter of references of \a msa.
 *
 * \param msa the marked shared automaton
 * \pre msa != NULL
 * \return msa
 */
static inline sataf_msa *
sataf_msa_add_reference (sataf_msa *msa);

/*!
 * \brief Decrement the counter of references of \a msa.
 *
 * When the counter of references of \a msa falls to zero the resources 
 * allocated to it are freed.
 * \param msa the marked shared automaton
 * \pre msa != NULL
 */
static inline void
sataf_msa_del_reference (sataf_msa *msa);

/*!
 * \brief Display the given MSA into the DOT file format.
 *
 * \param msa the displayed MSA.
 * \param log the CCL output stream
 * \param alphabet an array of labels used as letters.
 * \pre msa != NULL
 * \see http://www.graphviz.org
 */
extern void
sataf_msa_display_as_dot (sataf_msa *msa, ccl_log_type log,
			  const char **alphabet, int with_info);

/*!
 * \brief Another function used to display the given MSA into the DOT file 
 * format.
 *
 * \param msa the displayed MSA.
 * \param log the CCL output stream
 * \param alphabet an array of labels used as letters.
 * \pre msa != NULL
 * \see http://www.graphviz.org
 */
extern void
sataf_msa_display_as_olddot (sataf_msa *msa, ccl_log_type log,
			     const char **alphabet);

/*!
 * \brief Check if \a msa is the one recognizing nothing.
 *
 * \param msa the marked shared automaton
 * \pre msa != NULL
 * \return a non-null value if \a msa recognizes no word.
 */
static inline int
sataf_msa_is_zero (sataf_msa *msa);

/*!
 * \brief Check if \a msa is the one recognizing everything.
 *
 * \param msa the marked shared automaton
 * \pre msa != NULL
 * \return a non-null value if \a msa recognizes all words.
 */
static inline int
sataf_msa_is_one (sataf_msa *msa);

/*!
 * \brief Check if \a msa is one of the two elementary MSAs i.e. either the one
 * recognizing everything or the one recognizing nothing.
 *
 * \param msa the marked shared automaton
 * \pre msa != NULL
 * \return sataf_msa_is_one (msa) || sataf_msa_is_zero (msa)
 */
static inline int
sataf_msa_is_zero_or_one (sataf_msa *msa);

/*! 
 * \brief Check the inclusion of \f$L_{a_1}\f$ in \f$L_{a_2}\f$.
 *
 * \param "a1, a2" the marked shared automata
 * \pre a1 != NULL && a2 != NULL
 * \return a non-null value iff \f$L_{a_1}\subseteq L_{a_2}\f$.
 */
extern int
sataf_msa_is_included_in (sataf_msa *a1, sataf_msa *a2);


/*!
 * \brief Return the empty language
 * \param alphabet_size the size of the reference alphabet
 * \pre alphabet_size > 0
 * \return the empty language \f$\emptyset\subset\Sigma^\star\f$ for an 
 * alphabet \f$\Sigma\f$ of size \a alphabet_size.
 */
extern sataf_msa * 
sataf_msa_zero (uint32_t alphabet_size);

/*! 
 * \brief Alias for the \ref sataf_msa_zero function
 */
# define sataf_msa_empty sataf_msa_zero

/*!
 * \brief Compute \f$\Sigma^\star\f$ 
 * \param alphabet_size the size of the alphabet \f$\Sigma\f$
 * \pre alphabet_size > 0
 * \return the msa encoding \f$\Sigma^\star\f$ with 
 * \f$|\Sigma|\f$=alphabet_size
 */
extern sataf_msa * 
sataf_msa_one (uint32_t alphabet_size);

/*! 
 * \brief Alias for the \ref sataf_msa_one function
 */
# define sataf_msa_all sataf_msa_one

/*!
 * \brief Create the singleton containing the empty word.
 * \param alphabet_size the size of the alphabet \f$\Sigma\f$
 * \pre alphabet_size > 0
 * \return \f$\{\epsilon\}\f$
 */
extern sataf_msa * 
sataf_msa_epsilon (uint32_t alphabet_size);

extern int
sataf_msa_is_epsilon (sataf_msa *msa);

/*!
 * \brief Create the singleton containing the letter \a a.
 * \param alphabet_size the size of the alphabet \f$\Sigma\f$
 * \param a the letter
 * \pre alphabet_size > 0
 * \pre a < alphabet_size 
 * \return \f$\{a\}\f$
 */
extern sataf_msa * 
sataf_msa_letter (uint32_t alphabet_size, uint32_t a);

/*!
 * \brief Create the language equal to the alphabet of size \a alphabet_size.
 * \param alphabet_size the size of the alphabet \f$\Sigma\f$
 * \pre alphabet_size > 0
 * \return \f$\Sigma\f$ such that \f$|\Sigma|\f$= alphabet_size
 */
extern sataf_msa * 
sataf_msa_alphabet (uint32_t alphabet_size);

/*!
 * \brief Boolean operation : if-then-else
 *
 * \param i the \a if MSA operand
 * \param t the \a then MSA operand
 * \param e the \a else MSA operand
 * \pre a1 != NULL && a2 != NULL && a3 != NULL
 * \return the MSA encoding 
 * \f$L_{a_1}\cap L_{a_2}\cup \overline{L_{a_1}}\cap L_{a_3}\f$
 */
extern sataf_msa * 
sataf_msa_ite (sataf_msa *i, sataf_msa *t, sataf_msa *e);

/*!
 * \brief Boolean operation : complement 
 *
 * \param a the marked shared automata
 * \pre a != NULL
 * \return the MSA encoding \f$\Sigma^\star\setminus L_a\f$
 */
extern sataf_msa * 
sataf_msa_not (sataf_msa *a);

/*!
 * \brief Boolean operation : disjunction
 *
 * \param "a1, a2" the marked shared automata
 * \pre a1 != NULL && a2 != NULL
 * \return the MSA encoding \f$L_{a_1}\cup L_{a_2}\f$
 */
extern sataf_msa * 
sataf_msa_or (sataf_msa *a1, sataf_msa *a2);

/*!
 * \brief Boolean operation : mutual exclusion
 *
 * \param "a1, a2" the marked shared automata
 * \pre a1 != NULL && a2 != NULL
 * \return the MSA encoding \f$L_{a_1}\oplus L_{a_2}\f$
 */
extern sataf_msa * 
sataf_msa_xor (sataf_msa *a1, sataf_msa *a2);

/*!
 * \brief Boolean operation : conjunction
 *
 * \param "a1, a2" the marked shared automata
 * \pre a1 != NULL && a2 != NULL
 * \return the MSA encoding \f$L_{a_1}\cap L_{a_2}\f$
 */
extern sataf_msa * 
sataf_msa_and (sataf_msa *a1, sataf_msa *a2);

/*!
 * \brief Boolean operation : implication
 *
 * \param "a1, a2" the marked shared automata
 * \pre a1 != NULL && a2 != NULL
 * \return the MSA encoding \f$\overline{L_{a_1}}\cup L_{a_2}\f$
 */
extern sataf_msa * 
sataf_msa_imply (sataf_msa *a1, sataf_msa *a2);

/*!
 * \brief Boolean operation : equivalence
 *
 * \param "a1, a2" the marked shared automata
 * \pre a1 != NULL && a2 != NULL
 * \return the MSA encoding 
 * \f$\overline{L_{a_1}}\cap \overline{L_{a_2}}\cup L_{a_1}\cap L_{a_2}\f$
 */
extern sataf_msa * 
sataf_msa_equiv (sataf_msa *a1, sataf_msa *a2);

/*!
 * \brief Create the MSA encoding the intersection of residues of \f$L_a\f$
 *
 * \param a the MAO argument
 * \pre a != NULL
 * \return the MSA encoding \f$\bigcap_{\alpha\in \Sigma} \alpha^{-1}L_a\f$
 */
extern sataf_msa * 
sataf_msa_forall (sataf_msa *a);

/*!
 * \brief Create the MSA encoding the union of residues of \f$L_a\f$
 *
 * \param a the MAO argument
 * \pre a != NULL
 * \return the MSA encoding \f$\bigcup_{\alpha\in \Sigma} \alpha^{-1}L_a\f$
 */
extern sataf_msa * 
sataf_msa_exists (sataf_msa *a);

/*!
 * \brief Concatenation of \f$L_{a_1}\f$ and \f$L_{a_2}\f$.
 *
 * \param "a1, a2" the marked shared automata
 * \pre a1 != NULL && a2 != NULL
 * \return the MSA encoding \f$L_{a_1}.L_{a_2}\f$.
 */
extern sataf_msa * 
sataf_msa_concat (sataf_msa *a1, sataf_msa *a2);

/*!
 * \brief Exponentiation of \f$L_a\f$.
 *
 * \param a the marked shared automaton 
 * \param N the exponent
 * \pre a != NULL
 * \pre N >= 0
 * \return the MSA encoding \f$L_a^N\f$
 */
extern sataf_msa * 
sataf_msa_power (sataf_msa *a, int N);

/*!
 * \brief Transitive closure of \f$L_a\f$.
 *
 * \param a the marked shared automaton 
 * \pre a != NULL
 * \return the MSA encoding \f$L_a^*\f$
 */
extern sataf_msa * 
sataf_msa_star (sataf_msa *a);

extern sataf_msa *
sataf_msa_change_acceptance (sataf_msa *a, ccl_hash *cond);

/*!
 * \brief check if \a a1 is detectable in \a a2
 *
 * This function checks if \a a1 is detectable in \a a2 i.e. if it is possible
 * to change the acceptance condition of \a a2 in such a way that the obtained
 * automaton recognize the same language than \a a1.
 *
 * \param a1 automaton for which the detectability is checked
 * \param a2 second operand
 * \param acc the list of states of \a a2 that have to accepting in order 
 *        to obtain a2
 * \pre a1 != NULL
 * \pre a2 != NULL
 * \return a non null value if \a a1 is detectable in \a a2.
 */
extern int
sataf_msa_is_detectable (sataf_msa *a1, sataf_msa *a2, ccl_list *acc);

/*!
 * \brief Look for a result in the cache over MSA operations.
 * 
 * This function finds an entry (a1,a2,a3,aux,R) then it returns a new
 * reference to \a R. \a aux is a generic pointer which can be used to
 * identify the cached operation (by using, for instance, the address of the
 * function which implements the operation).
 *
 * \param a1 the first MSA operand of the cached operation
 * \param a2 the second MSA but optional operand
 * \param a3 the third MSA but optional operand
 * \param aux a generic identifier.
 * \pre a1 != NULL
 *
 * \return the MSA R if the cache contains an entry (a1,a2,a3,aux,R).
 */
extern sataf_msa * 
sataf_msa_cache_get (sataf_msa *a1, sataf_msa *a2, sataf_msa *a3,
		     const void *aux);

/*!
 * \brief Add en entry into the cache for MSA operations.
 * 
 * This function puts a new record into the cache for MSA operations. 
 * The counter of references is incremented for each non-null \f$a_i\f$.
 * The fourth parameter aux is copied as is. The new entry might overwrite an
 * existing one; in this case, the replaced \f$a_i\f$ and \f$R\f$ are 
 * dereferenced.
 *
 * \param a1 the first MSA operand
 * \param a2 the second MSA but optional operand
 * \param a3 the third MSA but optional operand
 * \param aux a generic identifier
 * \param R the result of the cached operation
 * \pre a1 != NULL
 */
extern void
sataf_msa_cache_put (sataf_msa *a1, sataf_msa *a2, sataf_msa *a3,
		     const void *aux, sataf_msa *R);

/*! @} */

# include <sataf/sataf-inline.h>

extern int
sataf_msa_are_complements (sataf_msa *a1, sataf_msa *a2);

END_C_DECLS


#endif /* ! __SATAF_H__ */
