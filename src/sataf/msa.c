/*
 * msa.c -- Basic operations on MSA
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include <ccl/ccl-hash.h>
#include <ccl/ccl-pool.h>
#include "sataf-p.h"

typedef struct sataf_shared_automaton_st SSA;
struct sataf_shared_automaton_st {
  sataf_mao super;
  sataf_sa *A;
  uint32_t initial;
};


			/* --------------- */

# define DUP(msa) sataf_msa_add_reference (msa)
# define DEL(msa) sataf_msa_del_reference (msa)

			/* --------------- */

static SSA *
s_new_ssa (sataf_sa *sa, uint32_t initial);

static size_t
s_ssa_size (sataf_mao *self);

static void
s_ssa_destroy (sataf_mao *self);

#define s_ssa_no_cache NULL

#define s_ssa_is_root_of_scc NULL

static sataf_msa *
s_ssa_simplify (sataf_mao *self);

static uint32_t
s_ssa_get_alphabet_size (sataf_mao *self);

#define s_ssa_to_string NULL

static sataf_mao *
s_ssa_succ (sataf_mao *self, uint32_t letter);

static int
s_ssa_is_final (sataf_mao *self);

static int
s_ssa_equals (sataf_mao *self, sataf_mao *other);

static unsigned int
s_ssa_hashcode (sataf_mao *self);

# define s_ssa_to_dot NULL

			/* --------------- */

static const sataf_mao_methods SSA_METHODS = 
  {
    "SA-MSA",
    s_ssa_size,
    s_ssa_destroy,
    s_ssa_no_cache,
    s_ssa_is_root_of_scc,
    s_ssa_simplify,
    s_ssa_get_alphabet_size,
    s_ssa_to_string,
    s_ssa_succ,
    s_ssa_is_final,
    s_ssa_equals,
    s_ssa_hashcode,
    s_ssa_to_dot
  };

			/* --------------- */

static sataf_msa *ZERO_FOR_BOOLEANS = NULL;
static sataf_msa *ONE_FOR_BOOLEANS = NULL;

			/* --------------- */

int
sataf_msa_init (int op_cache_size, int sharing_cache_size,
		int ut_table_size, int ut_max_fill_degree)
{
  sataf_sa *a;
  int result = 
    (sataf_msa_unique_table_init (ut_table_size, ut_max_fill_degree) &&
     sataf_msa_sharing_init (sharing_cache_size) &&
     sataf_msa_cache_init (op_cache_size));

  if (result)
    {
      a = sataf_sa_create_zero (2);
      ZERO_FOR_BOOLEANS = sataf_msa_find_or_add (a, 0);
      sataf_sa_del_reference (a);
      
      a = sataf_sa_create_one (2);
      ONE_FOR_BOOLEANS = sataf_msa_find_or_add (a, 0);
      sataf_sa_del_reference (a);
    }

  return result;
}

			/* --------------- */

void
sataf_msa_terminate (void)
{
  sataf_msa_del_reference (ZERO_FOR_BOOLEANS);
  sataf_msa_del_reference (ONE_FOR_BOOLEANS);
  sataf_msa_cache_terminate ();
  sataf_msa_sharing_terminate ();
  sataf_msa_unique_table_terminate ();
}

			/* --------------- */

sataf_msa * 
sataf_msa_zero (uint32_t alphabet_size)
{
  sataf_msa *R;

  if (alphabet_size == 2)
    R = sataf_msa_add_reference (ZERO_FOR_BOOLEANS);
  else
    {
      sataf_mao *a = sataf_mao_create_zero (alphabet_size);
      R = sataf_msa_compute (a);
      sataf_mao_del_reference (a);
    }

  return R;
}

			/* --------------- */

sataf_msa * 
sataf_msa_one (uint32_t alphabet_size)
{
  sataf_msa *R;

  if (alphabet_size == 2)
    R = sataf_msa_add_reference (ONE_FOR_BOOLEANS);
  else
    {
      sataf_mao *a = sataf_mao_create_one (alphabet_size);
      R = sataf_msa_compute (a);
      sataf_mao_del_reference (a);
    }

  return R;
}

			/* --------------- */

sataf_msa * 
sataf_msa_epsilon (uint32_t alphabet_size)
{
  sataf_mao *a = sataf_mao_create_epsilon (alphabet_size);
  sataf_msa *R = sataf_msa_compute (a);
  sataf_mao_del_reference (a);

  return R;
}

			/* --------------- */
int
sataf_msa_is_epsilon (sataf_msa *msa)
{ 
  int i;
  sataf_ea *ea = msa->A->automaton;
  int result = (ea->nb_local_states == 1 && ea->nb_exit_states == 1 &&
		ea->is_final[0] && sataf_msa_is_zero (msa->A->bind[0]));
  int lz = sataf_ea_encode_succ_as_exit_state (0);

  for (i = 0; result && i < ea->alphabet_size; i++)
    result = (ea->successor[i] == lz);

  return result;
}


			/* --------------- */

sataf_msa * 
sataf_msa_letter (uint32_t alphabet_size, uint32_t l)
{
  sataf_mao *a = sataf_mao_create_letter (alphabet_size, l);
  sataf_msa *R = sataf_msa_compute (a);
  sataf_mao_del_reference (a);

  return R;
}

			/* --------------- */

sataf_msa * 
sataf_msa_alphabet (uint32_t alphabet_size)
{
  sataf_mao *a = sataf_mao_create_alphabet (alphabet_size);
  sataf_msa *R = sataf_msa_compute (a);
  sataf_mao_del_reference (a);

  return R;
}

			/* --------------- */

sataf_mao *
sataf_msa_to_mao (sataf_msa *msa)
{
  return (sataf_mao *) s_new_ssa (msa->A, msa->initial);
}

			/* --------------- */

sataf_msa * 
sataf_msa_succ (sataf_msa *msa, uint32_t a)
{
  sataf_msa *R;
  uint32_t index;
  uint32_t succ;

  ccl_pre (msa != NULL);
  ccl_pre (a < sataf_msa_get_alphabet_size (msa));

  succ = sataf_ea_get_successor (msa->A->automaton, msa->initial, a);
  index = sataf_ea_decode_succ_state (succ);

  if (sataf_ea_is_local_state (succ))
    {
      if (index == msa->initial)
	R = DUP (msa);
      else
	R = sataf_msa_find_or_add (msa->A, index);
    }
  else
    {
      R = DUP (msa->A->bind[index]);
    }

  return R;
}

			/* --------------- */

void
sataf_msa_display_as_dot (sataf_msa *msa, ccl_log_type log,
			  const char **alphabet, int with_info)
{
  sataf_sa_display_as_dot (msa->A, log, msa->initial, alphabet, with_info);
}

			/* --------------- */

void
sataf_msa_display_as_olddot (sataf_msa *msa,
			     ccl_log_type log, const char **alphabet)
{
  const char *fmt;

  ccl_log (log, "digraph system { \n");
  if (sataf_msa_is_zero (msa))
    ccl_log (log, "0 [label=zero]\n");
  else if (sataf_msa_is_one (msa))
    ccl_log (log, "0 [label=one]\n");
  else
    {
      ccl_list *todo;
      ccl_hash *done = ccl_hash_create (NULL, NULL, NULL, NULL);
      int countSA = 1;
      int countStates = 0;
      sataf_sa *A;
      int Ai;

      ccl_hash_find (done, msa->A);
      ccl_hash_insert (done, (void *) 0);
      todo = ccl_list_create ();
      ccl_list_put_first (todo, msa->A);

      ccl_log (log, "I -> C0I%d\n", msa->initial);

      while (!ccl_list_is_empty (todo))
	{
	  uint32_t i;

	  A = (sataf_sa *) ccl_list_take_first (todo);
	  ccl_assert (ccl_hash_find (done, A));
	  ccl_hash_find (done, A);
	  Ai = (uintptr_t) ccl_hash_get (done);

	  for (i = 0; i < A->automaton->nb_local_states; i++)
	    {
	      uint32_t a;

	      if (A->automaton->is_final[i])
		fmt = "C%dI%d [label=A%d]\n";
	      else
		fmt = "C%dI%d [label=%d]\n";

	      ccl_log (log, fmt, Ai, i, countStates);
	      countStates++;

	      for (a = 0; a < A->automaton->alphabet_size; a++)
		{
		  uint32_t j = sataf_ea_get_successor (A->automaton, i, a);
		  uint32_t succ = sataf_ea_decode_succ_state (j);

		  ccl_log (log, "C%dI%d->", Ai, i);
		  if (sataf_ea_is_local_state (j))
		    ccl_log (log, "C%dI%d", Ai, succ);
		  else
		    {
		      sataf_msa *M = A->bind[succ];

		      if (sataf_msa_is_one (M))
			ccl_log (log, "one");
		      else if (sataf_msa_is_zero (M))
			ccl_log (log, "zero");
		      else
			{
			  int indexTgt;

			  if (!ccl_hash_find (done, M->A))
			    {
			      indexTgt = countSA++;
			      ccl_hash_insert (done, 
					       (void *) (uintptr_t) indexTgt);
			      ccl_list_put_first (todo, M->A);
			    }
			  else
			    {
			      indexTgt = (uintptr_t) ccl_hash_get (done);
			    }
			  ccl_log (log, "C%dI%d", indexTgt, M->initial);
			}
		    }

		  if (alphabet != NULL)
		    ccl_log (log, " [label=%s]\n", alphabet[a]);
		  else
		    ccl_log (log, " [label=%d]\n", a);
		}
	    }
	}
      ccl_list_delete (todo);
      ccl_hash_delete (done);
    }
  ccl_log (log, "}\n");
}

			/* --------------- */

int
sataf_msa_is_included_in (sataf_msa *a1, sataf_msa *a2)
{
  sataf_msa *intersection = sataf_msa_and (a1, a2);
  int result = (intersection == a1);
  sataf_msa_del_reference (intersection);

  return result;
}

			/* --------------- */

void
sataf_msa_log_info (ccl_log_type log, sataf_msa *msa)
{
  sataf_msa_info info;

  sataf_msa_get_info (msa, &info);
  ccl_log (log, "#SA=%d #EA=%d #S=%d #D=%d #Refs=%d #to0=%d/%d",
	   info.nb_shared_automata,
	   info.nb_exit_automata, info.nb_states, info.depth, info.refcount,
	   info.to0, 2*info.nb_states);
}

			/* --------------- */

void
sataf_msa_get_info (sataf_msa *msa, sataf_msa_info *info)
{
  ccl_pointer_iterator *pi;
  ccl_hash *ea = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_list *todo = ccl_list_create ();
  ccl_hash *done = ccl_hash_create (NULL, NULL, NULL, NULL);

  ccl_pre (msa != NULL && info != NULL);

  ccl_hash_find (done, msa->A);
  ccl_hash_insert (done, msa->A);
  ccl_list_add (todo, msa->A);

  while (!ccl_list_is_empty (todo))
    {
      uint32_t b;
      sataf_sa *sa = ccl_list_take_first (todo);

      if (!ccl_hash_find (ea, sa->automaton))
	ccl_hash_insert (ea, sa->automaton);

      for (b = 0; b < sa->automaton->nb_exit_states; b++)
	{
	  if (!ccl_hash_find (done, sa->bind[b]->A))
	    {
	      ccl_hash_insert (done, sa->bind[b]->A);
	      ccl_list_add (todo, sa->bind[b]->A);
	    }
	}
    }

  info->nb_shared_automata = ccl_hash_get_size (done);
  info->nb_exit_automata = ccl_hash_get_size (ea);
  info->nb_states = 0;
  info->to0 = 0;

  pi = ccl_hash_get_keys (done);
  while (ccl_iterator_has_more_elements (pi))
    {
      int i;
      sataf_sa *sa = (sataf_sa *) ccl_iterator_next_element (pi);
      info->nb_states += sa->automaton->nb_local_states;
      for (i = 0; i < sa->automaton->nb_exit_states; i++)
	if (sataf_msa_is_zero (sa->bind[i]))
	  info->to0++;
    }
  ccl_iterator_delete (pi);
  info->depth = msa->A->depth;
  info->refcount = msa->refcount;

  ccl_list_delete (todo);
  ccl_hash_delete (done);
  ccl_hash_delete (ea);
}

			/* --------------- */

static SSA *
s_new_ssa (sataf_sa *sa, uint32_t initial)
{
  SSA *R = (SSA *) sataf_mao_create (sizeof (SSA), &SSA_METHODS);

  R->A = sataf_sa_add_reference (sa);
  R->initial = initial;

  return R;
}


			/* --------------- */

static size_t
s_ssa_size (sataf_mao *self)
{
  return sizeof (SSA);
}

			/* --------------- */

static void
s_ssa_destroy (sataf_mao *self)
{
  SSA *ssa = (SSA *) self;

  sataf_sa_del_reference (ssa->A);
}

			/* --------------- */

static sataf_msa *
s_ssa_simplify (sataf_mao *self)
{
  SSA *ssa = (SSA *) self;

  return sataf_msa_find_or_add (ssa->A, ssa->initial);
}


static uint32_t
s_ssa_get_alphabet_size (sataf_mao *self)
{
  SSA *ssa = (SSA *) self;

  return ssa->A->automaton->alphabet_size;
}

			/* --------------- */

static sataf_mao *
s_ssa_succ (sataf_mao *self, uint32_t letter)
{
  SSA *ssa = (SSA *) self;
  uint32_t index, succ;
  sataf_mao *R;

  ccl_pre (self != NULL);

  succ = sataf_ea_get_successor (ssa->A->automaton, ssa->initial, letter);
  index = sataf_ea_decode_succ_state (succ);

  if (sataf_ea_is_local_state (succ))
    {
      if (index == ssa->initial)
	R = sataf_mao_add_reference (self);
      else
	R = (sataf_mao *) s_new_ssa (ssa->A, index);
    }
  else
    {
      R = (sataf_mao *) s_new_ssa (ssa->A->bind[index]->A,
				   ssa->A->bind[index]->initial);
    }

  return R;
}

			/* --------------- */

static int
s_ssa_is_final (sataf_mao *self)
{
  SSA *ssa = (SSA *) self;

  return ssa->A->automaton->is_final[ssa->initial];
}

			/* --------------- */

static int
s_ssa_equals (sataf_mao *self, sataf_mao *other)
{
  SSA *ssa = (SSA *) self;
  SSA *ssa_other = (SSA *) other;

  return ssa->A == ssa_other->A && ssa->initial == ssa_other->initial;
}

			/* --------------- */

static unsigned int
s_ssa_hashcode (sataf_mao *self)
{
  SSA *ssa = (SSA *) self;

  return 5 * (uintptr_t) ssa->A + 13 * ssa->initial;
}
