/*
 * msa-project.c -- Deletion of some position in a MSA-encoded language
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-array.h>
#include "sataf-p.h"

typedef CCL_ARRAY(sataf_msa *) msa_array;

typedef struct projection_st
{
  sataf_mao super;
  int i;
  int n;
  msa_array autos;
  sataf_sa_transformer *T;
} projection;

			/* --------------- */

# define DUP(a)  sataf_msa_add_reference(a)
# define DEL(a)  sataf_msa_del_reference(a)
# define ONE(a)  sataf_msa_is_one(a)
# define ZERO(a) sataf_msa_is_zero(a)
# define SZ(a)   ((a)->A->automaton->alphabet_size)

			/* --------------- */

static sataf_mao *
s_create_projection (sataf_msa *a, int i, int n, sataf_sa_transformer *T);

static sataf_mao *
s_project (int i, int n, msa_array autos, sataf_sa_transformer *T);

static size_t
s_projection_size (sataf_mao *self);

static void
s_projection_destroy (sataf_mao *self);

#define s_projection_no_cache NULL

#define s_projection_is_root_of_scc NULL

static sataf_msa *
s_projection_simplify (sataf_mao *self);

static uint32_t
s_projection_get_alphabet_size (sataf_mao *self);

#define s_projection_to_string NULL

static sataf_mao *
s_projection_succ (sataf_mao *self, uint32_t letter);

static int
s_projection_is_final (sataf_mao *self);

static int
s_projection_is_equal_to (sataf_mao *self, sataf_mao *other);

static unsigned int
s_projection_hashcode (sataf_mao *self);

# define s_projection_to_dot NULL

			/* --------------- */

static const int PRIME_TABLE[] = {
  1, 3, 7, 17, 37, 79, 163, 331, 673, 1361,
  2729, 5471, 10949, 21911, 43853, 87719, 175447, 350899, 701819, 1403641,
  2807303, 5614657, 11229331
};

static const size_t PRIME_TABLE_SIZE =
  (sizeof (PRIME_TABLE) / sizeof(PRIME_TABLE[0]));


static const sataf_mao_methods PROJECTION_METHODS = {
  "SA-PROJECTION",
  s_projection_size,
  s_projection_destroy,
  s_projection_no_cache,
  s_projection_is_root_of_scc,
  s_projection_simplify,
  s_projection_get_alphabet_size,
  s_projection_to_string,
  s_projection_succ,
  s_projection_is_final,
  s_projection_is_equal_to,
  s_projection_hashcode,
  s_projection_to_dot
};

			/* --------------- */

sataf_msa *
sataf_msa_project (sataf_msa *a, int i, int n, sataf_sa_transformer *T)
{
  sataf_msa *result;

  if (ONE (a) || ZERO (a))
    result = DUP (a);
  else if (n < 2)
    result = sataf_msa_one (SZ (a));
  else
    {
      sataf_mao *aut = s_create_projection (a, i, n, T);
      result = sataf_msa_compute_with_transformer (aut, T);
      sataf_mao_del_reference (aut);
    }

  return result;
}

			/* --------------- */

static sataf_mao *
s_create_projection (sataf_msa *a, int i, int n, sataf_sa_transformer *T)
{
  msa_array autos;

  ccl_array_init_with_size (autos, 2);
  autos.data[0] = sataf_msa_zero (SZ (a));
  autos.data[1] = sataf_msa_add_reference (a);

  return s_project (i, n, autos, T);
}

			/* --------------- */

static size_t
s_projection_size (sataf_mao *self)
{
  return sizeof (projection);
}

			/* --------------- */

static void
s_projection_destroy (sataf_mao *self)
{
  int i;
  projection *p = (projection *) self;

  for (i = 0; i < p->autos.size; i++)
    sataf_msa_del_reference (p->autos.data[i]);
  ccl_array_delete (p->autos);
}

			/* --------------- */

static uint32_t
s_projection_get_alphabet_size (sataf_mao *self)
{
  projection *p = (projection *) self;

  return SZ (p->autos.data[0]);
}

			/* --------------- */

static sataf_msa *
s_projection_simplify (sataf_mao *self)
{
  projection *p = (projection *) self;

  if (p->autos.size == 1)
    return sataf_msa_add_reference (p->autos.data[0]);
  return NULL;
}

			/* --------------- */

static sataf_mao *
s_projection_succ (sataf_mao *self, uint32_t letter)
{
  sataf_mao *result;
  projection *p = (projection *) self;

  if (p->autos.size == 1)
    {
      sataf_msa *succ = sataf_msa_succ (p->autos.data[0], letter);
      result = sataf_msa_to_mao (succ);
      sataf_msa_del_reference (succ);
    }
  else
    {
      msa_array new_autos;

      ccl_array_init_with_size (new_autos, p->autos.size);

      new_autos.data[0] = sataf_msa_succ (p->autos.data[0], letter);
      if (ONE (new_autos.data[0]))
	ccl_array_trim (new_autos, 1);
      else
	{
	  int j;
	  int stop = 0, k = 0;
	  sataf_sa *A = p->autos.data[1]->A;


	  for (j = 1; j < p->autos.size && !stop; j++)
	    {
	      sataf_msa *succ = sataf_msa_succ (p->autos.data[j], letter);

	      if (ONE (succ))
		{
		  int z;

		  for (z = 0; z < k; z++)
		    sataf_msa_del_reference (new_autos.data[1 + z]);
		  ccl_array_trim (new_autos, 1);
		  stop = 1;
		}
	      else if (succ->A != A)
		{
		  sataf_msa *psucc =
		    sataf_msa_project (succ, p->i - 1, p->n, p->T);
		  sataf_msa *aux = sataf_msa_or (new_autos.data[0], psucc);

		  sataf_msa_del_reference (psucc);
		  sataf_msa_del_reference (new_autos.data[0]);
		  new_autos.data[0] = aux;

		  if (ONE (aux))
		    {
		      int z;

		      for (z = 0; z < k; z++)
			sataf_msa_del_reference (new_autos.data[z + 1]);
		      ccl_array_trim (new_autos, 1);
		      stop = 1;
		    }
		}
	      else if (!ZERO (succ))
		{
		  int p;
		  sataf_msa *aux = sataf_msa_add_reference (succ);

		  for (p = 0; p < k; p++)
		    {
		      if (aux < new_autos.data[p + 1])
			{
			  sataf_msa *tmp = new_autos.data[p + 1];
			  new_autos.data[p + 1] = aux;
			  aux = tmp;
			}
		      else if (aux == new_autos.data[p + 1])
			{
			  sataf_msa_del_reference (aux);
			  break;
			}
		    }

		  if (p == k)
		    {
		      new_autos.data[1 + k] = aux;
		      k++;
		    }
		}

	      sataf_msa_del_reference (succ);

	    }
	  ccl_array_trim (new_autos, k + 1);
	}

      result = s_project (p->i - 1, p->n, new_autos, p->T);
    }

  return result;
}

			/* --------------- */

static int
s_projection_is_final (sataf_mao *self)
{
  int i;
  int result = 0;
  projection *p = (projection *) self;

  for (i = 0; !result && i < p->autos.size; i++)
    result = sataf_msa_is_final (p->autos.data[i]);

  return result;
}

			/* --------------- */

static int
s_projection_is_equal_to (sataf_mao *self, sataf_mao *other)
{
  projection *p1 = (projection *) self;
  projection *p2 = (projection *) other;

  if (p1->i != p2->i || p1->n != p2->n || p1->autos.size != p2->autos.size)
    return 0;

  return
    ccl_memcmp (p1->autos.data, p2->autos.data,
		sizeof (sataf_msa) * p1->autos.size) == 0;
}

			/* --------------- */

static unsigned int
s_projection_hashcode (sataf_mao *self)
{
  int i;
  projection *p = (projection *) self;
  int max = p->autos.size;
  unsigned int result = 175447 * p->i + 43853 * p->n + 350899 * max;

  for (i = 0; i < max; i++)
    result = 175447 * result +
      PRIME_TABLE[i % PRIME_TABLE_SIZE] * (uintptr_t) p->autos.data[i];

  return result;
}

			/* --------------- */

static sataf_mao *
s_project (int i, int n, msa_array autos, sataf_sa_transformer *T)
{
  sataf_mao *result;

  if (autos.size == 1)
    {
      result = sataf_msa_to_mao (autos.data[0]);

      sataf_msa_del_reference (autos.data[0]);
      ccl_array_delete (autos);
    }
  else if (i == 0)
    {
      sataf_sa *A;
      int stop, k, j, l, sz = SZ (autos.data[0]);
      msa_array new_autos;

      ccl_array_init_with_size (new_autos,
				1 + (SZ (autos.data[0]) * (autos.size - 1)));

      new_autos.data[0] = sataf_msa_add_reference (autos.data[0]);
      A = autos.data[1]->A;
      k = 0;
      stop = 0;

      for (j = 1; j < autos.size && !stop; j++)
	{
	  for (l = 0; l < sz && !stop; l++)
	    {
	      sataf_msa *succ = sataf_msa_succ (autos.data[j], l);

	      if (ONE (succ))
		{
		  int p;

		  for (p = 0; p < k; p++)
		    sataf_msa_del_reference (new_autos.data[p + 1]);
		  ccl_array_trim (new_autos, 1);
		  stop = 1;
		}
	      else if (succ->A != A)
		{
		  sataf_msa *psucc = sataf_msa_project (succ, n - 1, n, T);
		  sataf_msa *aux = sataf_msa_or (new_autos.data[0], psucc);

		  sataf_msa_del_reference (psucc);
		  sataf_msa_del_reference (new_autos.data[0]);
		  new_autos.data[0] = aux;

		  if (ONE (aux))
		    {
		      int p;

		      for (p = 0; p < k; p++)
			sataf_msa_del_reference (new_autos.data[p + 1]);
		      ccl_array_trim (new_autos, 1);
		      stop = 1;
		    }
		}
	      else if (!ZERO (succ))
		{
		  int p;
		  sataf_msa *aux = sataf_msa_add_reference (succ);

		  for (p = 0; p < k; p++)
		    {
		      if (aux < new_autos.data[p + 1])
			{
			  sataf_msa *tmp = new_autos.data[p + 1];
			  new_autos.data[p + 1] = aux;
			  aux = tmp;
			}
		      else if (aux == new_autos.data[p + 1])
			{
			  sataf_msa_del_reference (aux);
			  break;
			}

		    }

		  if (p == k)
		    {
		      new_autos.data[1 + k] = aux;
		      k++;
		    }
		}

	      sataf_msa_del_reference (succ);
	    }
	}

      for (j = 0; j < autos.size; j++)
	sataf_msa_del_reference (autos.data[j]);
      ccl_array_delete (autos);

      ccl_array_trim (new_autos, k + 1);

      result = s_project (n - 1, n, new_autos, T);
    }
  else
    {
      projection *p = (projection *)
	sataf_mao_create (sizeof (projection), &PROJECTION_METHODS);

      p->i = i;
      p->n = n;
      p->autos = autos;
      p->T = T;

      result = (sataf_mao *) p;
    }

  return result;
}
