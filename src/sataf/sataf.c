/*
 * sataf.c -- Initialization and termination of the SATAF library
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-init.h>
#include <ccl/ccl-pool.h>
#include "sataf-p.h"

			/* --------------- */

static int init_counter = 0;

			/* --------------- */

int
sataf_init (int msa_op_cache_size, int mao_sharing_cache_size, 
	    int ea_ut_table_size, int ea_ut_max_fill_degree, 
	    int sa_ut_table_size, int sa_ut_max_fill_degree,
	    int msa_ut_table_size, int msa_ut_max_fill_degree)
{
  int result = 1;

  if (init_counter == 0)
    {
      result = (sataf_mao_init () &&
		sataf_ea_init (ea_ut_table_size, ea_ut_max_fill_degree) &&
		sataf_sa_init (sa_ut_table_size, sa_ut_max_fill_degree) &&
		sataf_msa_init (msa_op_cache_size, mao_sharing_cache_size,
				msa_ut_table_size, msa_ut_max_fill_degree));
    }
  init_counter++;

  return result;
}

			/* --------------- */

void
sataf_terminate (void)
{
  init_counter--;

  if (init_counter == 0)
    {
      sataf_msa_terminate ();
      sataf_sa_terminate ();
      sataf_ea_terminate ();
      sataf_mao_terminate ();
    }
}

			/* --------------- */

void
sataf_log_statistics (ccl_log_type log)
{
  sataf_ea_ut_statistics (log);
  sataf_sa_table_statistics (log);
  sataf_msa_ut_statistics (log);
  sataf_msa_cache_statistics (log);
  sataf_msa_sharing_cache_statistics (log);
  ccl_pools_display_info (log);
}
