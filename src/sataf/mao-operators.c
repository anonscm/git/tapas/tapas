/*
 * mao-operators.c -- Boolean operations on Marked Automaton Object
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-list.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "sataf.h"

# define DUP(_a) sataf_mao_add_reference (_a)
# define DEL(_a) sataf_mao_del_reference (_a)


			/* --------------- */

typedef struct boolean_operator_automaton_st
{
  sataf_mao super;
  sataf_mao **operands;
} boolean_operator_automaton;

typedef struct boolean_operator_methods_st
{
  sataf_mao_methods super;
  uint32_t width;
  uint32_t refcount;
} boolean_operator_methods;

			/* --------------- */

static sataf_mao *
s_create_boolean_automaton (ccl_list *args, const boolean_operator_methods *m);

static sataf_mao *
s_create_ternary (sataf_mao *arg1, sataf_mao *arg2, sataf_mao *arg3, 
		  const boolean_operator_methods *m);

static sataf_mao * 
s_create_binary (sataf_mao *arg1, sataf_mao *arg2, 
		 const boolean_operator_methods *m);

static sataf_mao * 
s_create_unary (sataf_mao *arg1, const boolean_operator_methods *m);

			/* --------------- */

static size_t
s_boolean_operator_size (sataf_mao *self);

static void
s_boolean_operator_destroy (sataf_mao *self);

# define s_boolean_operator_to_string NULL

# define s_boolean_operator_no_cache NULL

# define s_boolean_operator_is_root_of_scc NULL

# define s_boolean_operator_simplify NULL

static uint32_t
s_boolean_operator_get_alphabet_size (sataf_mao *self);

static sataf_mao * 
s_boolean_operator_succ (sataf_mao *self, uint32_t letter);

static int
s_boolean_operator_is_final_OR (sataf_mao *self);

static int
s_boolean_operator_is_final_AND (sataf_mao *self);

static int
s_boolean_operator_is_final_IMPLY (sataf_mao *self);

static int
s_boolean_operator_is_final_EQUIV (sataf_mao *self);

static int
s_boolean_operator_is_final_NOT (sataf_mao *self);

static int
s_boolean_operator_is_final_ITE (sataf_mao *self);

static int
s_boolean_operator_is_equal_to (sataf_mao *self, sataf_mao *other);

static unsigned int
s_boolean_operator_hashcode (sataf_mao *self);

# define s_boolean_operator_to_dot NULL


			/* --------------- */

# define DECL_OP_METHODS(ID) \
static struct boolean_operator_methods_st ID ## _METHODS = { \
  { \
    "SA-BOOLEAN-OP", \
    s_boolean_operator_size, \
    s_boolean_operator_destroy, \
    s_boolean_operator_no_cache, \
    s_boolean_operator_simplify, \
    s_boolean_operator_is_root_of_scc, \
    s_boolean_operator_get_alphabet_size, \
    s_boolean_operator_to_string, \
    s_boolean_operator_succ, \
    s_boolean_operator_is_final_ ## ID, \
    s_boolean_operator_is_equal_to, \
    s_boolean_operator_hashcode, \
    s_boolean_operator_to_dot \
  }, \
  0, \
  0 \
}

			/* --------------- */

DECL_OP_METHODS (OR);
DECL_OP_METHODS (AND);
DECL_OP_METHODS (IMPLY);
DECL_OP_METHODS (EQUIV);
DECL_OP_METHODS (NOT);
DECL_OP_METHODS (ITE);

			/* --------------- */

sataf_mao * 
sataf_mao_create_ite (sataf_mao *i, sataf_mao *t, sataf_mao *e)
{
  ccl_pre (sataf_mao_get_alphabet_size (i) ==
	   sataf_mao_get_alphabet_size (t));
  ccl_pre (sataf_mao_get_alphabet_size (t) ==
	   sataf_mao_get_alphabet_size (e));

  return s_create_ternary (i, t, e, &ITE_METHODS);
}

			/* --------------- */

sataf_mao * 
sataf_mao_create_not (sataf_mao *a)
{
  ccl_pre (a != NULL);

  return s_create_unary (a, &NOT_METHODS);
}

			/* --------------- */

sataf_mao * 
sataf_mao_create_or (sataf_mao *a1, sataf_mao *a2)
{
  ccl_list *args = ccl_list_create ();
  sataf_mao *R;

  ccl_pre (sataf_mao_get_alphabet_size (a1) ==
	   sataf_mao_get_alphabet_size (a2));

  ccl_list_add (args, a1);
  ccl_list_add (args, a2);
  R = sataf_mao_create_multi_or (args);
  ccl_list_delete (args);

  return R;
}

			/* --------------- */

sataf_mao * 
sataf_mao_create_multi_or (ccl_list * a)
{
  ccl_pre (ccl_list_get_size (a) > 0);

  return s_create_boolean_automaton (a, &OR_METHODS);
}

			/* --------------- */

sataf_mao * 
sataf_mao_create_and (sataf_mao *a1, sataf_mao *a2)
{
  ccl_list *args = ccl_list_create ();
  sataf_mao *R;

  ccl_pre (sataf_mao_get_alphabet_size (a1) ==
	   sataf_mao_get_alphabet_size (a2));

  ccl_list_add (args, a1);
  ccl_list_add (args, a2);
  R = sataf_mao_create_multi_and (args);
  ccl_list_delete (args);

  return R;
}

			/* --------------- */

sataf_mao * 
sataf_mao_create_multi_and (ccl_list * a)
{
  return s_create_boolean_automaton (a, &AND_METHODS);
}

			/* --------------- */

sataf_mao * 
sataf_mao_create_imply (sataf_mao *a1, sataf_mao *a2)
{
  ccl_pre (sataf_mao_get_alphabet_size (a1) ==
	   sataf_mao_get_alphabet_size (a2));

  return s_create_binary (a1, a2, &IMPLY_METHODS);
}

			/* --------------- */

sataf_mao * 
sataf_mao_create_equiv (sataf_mao *a1, sataf_mao *a2)
{
  ccl_pre (sataf_mao_get_alphabet_size (a1) ==
	   sataf_mao_get_alphabet_size (a2));

  return s_create_binary (a1, a2, &EQUIV_METHODS);
}

			/* --------------- */

static sataf_mao *
s_create_boolean_automaton (ccl_list *args, 
			    const boolean_operator_methods *m)
{
  int i;
  ccl_pair *p;
  boolean_operator_automaton *R;
  boolean_operator_methods *M = ccl_new (boolean_operator_methods);

  *M = *m;
  M->refcount = 1;
  M->width = ccl_list_get_size (args);

  R = (boolean_operator_automaton *)
    sataf_mao_create (sizeof (boolean_operator_automaton), 
		      (sataf_mao_methods *) M);
  R->operands = ccl_new_array (sataf_mao *, M->width);

  for (i = 0, p = FIRST (args); p; p = CDR (p), i++)
    R->operands[i] = sataf_mao_add_reference (CAR (p));

  return (sataf_mao *) R;
}

			/* --------------- */

static sataf_mao *
s_create_ternary (sataf_mao *arg1, sataf_mao *arg2, sataf_mao *arg3, 
		  const boolean_operator_methods *m)
{
  ccl_list *args = ccl_list_create ();
  sataf_mao *R;

  ccl_list_add (args, arg1);
  if (arg2 != NULL)
    {
      ccl_list_add (args, arg2);
      if (arg3 != NULL)
	ccl_list_add (args, arg3);;
    }
  R = s_create_boolean_automaton (args, m);
  ccl_list_delete (args);

  return R;
}

			/* --------------- */

static sataf_mao *
s_create_binary (sataf_mao *arg1, sataf_mao *arg2, 
		 const boolean_operator_methods *m)
{
  return s_create_ternary (arg1, arg2, NULL, m);
}

			/* --------------- */


static sataf_mao *
s_create_unary (sataf_mao *arg1, const boolean_operator_methods *m)
{
  return s_create_binary (arg1, NULL, m);
}

			/* --------------- */

static size_t
s_boolean_operator_size (sataf_mao *self)
{
  return sizeof (boolean_operator_automaton);
}

			/* --------------- */

static void
s_boolean_operator_destroy (sataf_mao *self)
{
  uint32_t i;
  boolean_operator_automaton *boa = (boolean_operator_automaton *) self;
  boolean_operator_methods *m = (boolean_operator_methods *) self->methods;

  for (i = 0; i < m->width; i++)
    DEL (boa->operands[i]);
  ccl_delete (boa->operands);

  if (--m->refcount == 0)
    ccl_delete (m);
}

			/* --------------- */

static uint32_t
s_boolean_operator_get_alphabet_size (sataf_mao *self)
{
  boolean_operator_automaton *boa = (boolean_operator_automaton *) self;

  return sataf_mao_get_alphabet_size (boa->operands[0]);
}

			/* --------------- */

static sataf_mao *
s_boolean_operator_succ (sataf_mao *self, uint32_t letter)
{
  uint32_t i;
  boolean_operator_automaton *boa = (boolean_operator_automaton *) self;
  boolean_operator_methods *m = (boolean_operator_methods *) self->methods;
  boolean_operator_automaton *R = (boolean_operator_automaton *)
    sataf_mao_create (sizeof (boolean_operator_automaton), self->methods);

  R->operands = ccl_new_array (sataf_mao *, m->width);
  m->refcount++;

  for (i = 0; i < m->width; i++)
    R->operands[i] = sataf_mao_succ (boa->operands[i], letter);

  return (sataf_mao *) R;
}

			/* --------------- */

static int
s_boolean_operator_is_final_OR (sataf_mao *self)
{
  int r;
  uint32_t i;
  boolean_operator_automaton *boa = (boolean_operator_automaton *) self;
  boolean_operator_methods *m = (boolean_operator_methods *) self->methods;

  r = sataf_mao_is_final (boa->operands[0]);
  for (i = 1; !r && i < m->width; i++)
    r = sataf_mao_is_final (boa->operands[i]);

  return r;
}

			/* --------------- */

static int
s_boolean_operator_is_final_AND (sataf_mao *self)
{
  int r;
  uint32_t i;
  boolean_operator_automaton *boa = (boolean_operator_automaton *) self;
  boolean_operator_methods *m = (boolean_operator_methods *) self->methods;

  r = sataf_mao_is_final (boa->operands[0]);
  for (i = 1; r && i < m->width; i++)
    r = sataf_mao_is_final (boa->operands[i]);

  return r;
}

			/* --------------- */

static int
s_boolean_operator_is_final_NOT (sataf_mao *self)
{
  boolean_operator_automaton *boa = (boolean_operator_automaton *) self;

  ccl_pre (((boolean_operator_methods *) self->methods)->width == 1);

  return !sataf_mao_is_final (boa->operands[0]);
}

			/* --------------- */

static int
s_boolean_operator_is_final_IMPLY (sataf_mao *self)
{
  boolean_operator_automaton *boa = (boolean_operator_automaton *) self;

  ccl_pre (((boolean_operator_methods *) self->methods)->width == 2);

  return !sataf_mao_is_final (boa->operands[0])
    || sataf_mao_is_final (boa->operands[1]);
}

			/* --------------- */

static int
s_boolean_operator_is_final_EQUIV (sataf_mao *self)
{
  boolean_operator_automaton *boa = (boolean_operator_automaton *) self;
  int v0 = sataf_mao_is_final (boa->operands[0]);
  int v1 = sataf_mao_is_final (boa->operands[1]);

  ccl_pre (((boolean_operator_methods *) self->methods)->width == 2);

  return (v0 && v1) || (!v0 && !v1);
}

			/* --------------- */

static int
s_boolean_operator_is_final_ITE (sataf_mao *self)
{
  boolean_operator_automaton *boa = (boolean_operator_automaton *) self;

  ccl_pre (((boolean_operator_methods *) self->methods)->width == 3);

  if (sataf_mao_is_final (boa->operands[0]))
    return sataf_mao_is_final (boa->operands[1]);
  return sataf_mao_is_final (boa->operands[2]);
}

			/* --------------- */

static int
s_boolean_operator_is_equal_to (sataf_mao *self, sataf_mao *other)
{
  uint32_t i;
  boolean_operator_automaton *boa1 = (boolean_operator_automaton *) self;
  boolean_operator_automaton *boa2 = (boolean_operator_automaton *) other;
  boolean_operator_methods *m1 = (boolean_operator_methods *) self->methods;
  boolean_operator_methods *m2 = (boolean_operator_methods *) self->methods;

  if (m1->super.is_final != m2->super.is_final)
    return 0;

  if (m1->width != m2->width)
    return 0;

  for (i = 0; i < m1->width; i++)
    {
      if (!sataf_mao_equals (boa1->operands[i], boa2->operands[i]))
	return 0;
    }

  return 1;
}

			/* --------------- */


static unsigned int
s_boolean_operator_hashcode (sataf_mao *self)
{
  uint32_t i;
  boolean_operator_automaton *boa = (boolean_operator_automaton *) self;
  boolean_operator_methods *m = (boolean_operator_methods *) self->methods;
  unsigned int r = m->width;

  for (i = 0; i < m->width; i++)
    r += 19 * r + sataf_mao_hashcode (boa->operands[i]);

  return r;
}

			/* --------------- */
