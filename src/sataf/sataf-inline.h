/*
 * sataf-inline.h -- add a comment about this file
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __SATAF_INLINE_H__
# define __SATAF_INLINE_H__

# include <ccl/ccl-assert.h>
# include <sataf/sataf.h>

BEGIN_C_DECLS

struct sataf_mao_st
{
  const sataf_mao_methods *methods;
  uint32_t refcount;
};

struct sataf_msa_st
{
  uint32_t refcount;
  sataf_sa *A;
  uint32_t initial;
  sataf_msa *next;
};

struct sataf_ea_st
{
  sataf_ea *next;
  uint32_t refcount;
  uint32_t alphabet_size;
  uint32_t nb_local_states;
  uint32_t nb_exit_states;
  uint8_t *is_final;
  uint32_t successor[1];
};

struct sataf_sa_st
{
  sataf_sa *next;
  uint32_t refcount;
  sataf_ea *automaton;
  uint32_t depth;
  sataf_msa *bind[1];
};


			/* --------------- */

static inline sataf_mao *
sataf_mao_add_reference (sataf_mao *a)
{
  ccl_pre (a != NULL);
  a->refcount++;
  return a;
}

			/* --------------- */

extern void 
sataf_mao_destroy (sataf_mao *a);

static inline void
sataf_mao_del_reference (sataf_mao *a)
{
  ccl_pre (a != NULL);
  --a->refcount;
  if (a->refcount == 0)
    sataf_mao_destroy (a);
}

			/* --------------- */

static inline const char *
sataf_mao_get_type (sataf_mao *self)
{
  ccl_pre (self != NULL);
  return self->methods->type;
}

			/* --------------- */

static inline uint32_t
sataf_ea_encode_succ_as_local_state (uint32_t index)
{
  return index << 1;
}

			/* --------------- */

static inline uint32_t 
sataf_ea_encode_succ_as_exit_state (uint32_t index)
{
  return 1 + (index << 1);
}

			/* --------------- */

static inline uint32_t
sataf_ea_decode_succ_state (uint32_t index)
{
  return index >> 1;
}

			/* --------------- */

static inline int
sataf_ea_is_exit_state (uint32_t successor)
{
  return (successor & 0x1) != 0;
}

			/* --------------- */

static inline int
sataf_ea_is_local_state (uint32_t successor)
{
  return ! sataf_ea_is_exit_state (successor);
}

			/* --------------- */

static inline uint32_t
sataf_ea_get_alphabet_size (sataf_ea *ea)
{
  ccl_pre (ea != NULL);
  return ea->alphabet_size;
}

			/* --------------- */

static inline uint32_t
sataf_ea_get_nb_states (sataf_ea *ea)
{
  ccl_pre (ea != NULL);
  return ea->nb_local_states;
}

			/* --------------- */

static inline uint32_t
sataf_ea_get_nb_exits (sataf_ea *ea)
{
  ccl_pre (ea != NULL);
  return ea->nb_exit_states;
}

			/* --------------- */

static inline int
sataf_ea_is_final (sataf_ea *ea, uint32_t s)
{
  ccl_pre (s < sataf_ea_get_nb_states(ea));
  return ea->is_final[s];
}

			/* --------------- */

static inline uint32_t
sataf_ea_get_successor (sataf_ea *ea, uint32_t state, uint32_t a)
{
  ccl_pre (ea != NULL);
  ccl_pre (state < ea->nb_local_states);
  ccl_pre (a < ea->alphabet_size);

  return ea->successor[ea->alphabet_size * state + a];
}

			/* --------------- */

static inline int
sataf_ea_is_zero (sataf_ea *ea)
{
  return sataf_ea_is_zero_or_one (ea) && !ea->is_final[0];
}

			/* --------------- */

static inline int
sataf_ea_is_one (sataf_ea *ea) 
{
  return sataf_ea_is_zero_or_one (ea) && (ea)->is_final[0];
}

			/* --------------- */

static inline int
sataf_ea_is_zero_or_one (sataf_ea *ea)
{
  ccl_pre (ea != NULL);
  return ea->nb_local_states == 1 && ea->nb_exit_states == 0;
}

			/* --------------- */

static inline void
sataf_ea_set_final (sataf_ea *ea, unsigned int s, int is_final)
{
  ccl_pre (ea != NULL);
  ccl_pre (s < ea->nb_local_states);

  ea->is_final[s] = is_final?1:0;
}

			/* --------------- */

static inline sataf_sa *
sataf_sa_add_reference (sataf_sa *sa)
{
  ccl_pre (sa != NULL);
  sa->refcount++;
  return sa;
}

			/* --------------- */


extern void
sataf_sa_destroy (sataf_sa *sa);

static inline void
sataf_sa_del_reference (sataf_sa *sa)
{
  ccl_pre (sa != NULL);
  ccl_pre (sa->refcount > 0);
  sa->refcount--;
  if (sa->refcount == 0)
    sataf_sa_destroy (sa);
}

			/* --------------- */

static inline uint32_t
sataf_sa_get_alphabet_size (sataf_sa *sa)
{
  ccl_pre (sa != NULL);
  return sataf_ea_get_alphabet_size (sa->automaton);
}

			/* --------------- */

static inline int
sataf_sa_is_zero (sataf_sa *sa)
{
  ccl_pre (sa != NULL);
  return sataf_ea_is_zero (sa->automaton);
}

			/* --------------- */

static inline int
sataf_sa_is_one (sataf_sa *sa)
{
  ccl_pre (sa != NULL);
  return sataf_ea_is_one (sa->automaton);
}

			/* --------------- */

static inline int
sataf_sa_is_zero_or_one (sataf_sa *sa)
{
  ccl_pre (sa != NULL);
  return sataf_ea_is_zero_or_one (sa->automaton);
}

			/* --------------- */

static inline uint32_t
sataf_msa_get_alphabet_size (sataf_msa *msa)
{
  ccl_pre (msa != NULL);
  return sataf_sa_get_alphabet_size (msa->A);
}

			/* --------------- */

static inline sataf_msa *
sataf_msa_add_reference (sataf_msa *msa)
{
  ccl_pre (msa != NULL);
  msa->refcount++;
  return msa;
}

			/* --------------- */

extern void
sataf_msa_destroy (sataf_msa *msa);


static inline void
sataf_msa_del_reference (sataf_msa *msa)
{
  ccl_pre (msa != NULL);
  ccl_pre (msa->refcount > 0);
  msa->refcount--;
  if (msa->refcount == 0)
    sataf_msa_destroy (msa);
}

			/* --------------- */

static inline int
sataf_msa_is_final (sataf_msa *msa)
{
  ccl_pre (msa != NULL);

  return sataf_ea_is_final (msa->A->automaton, msa->initial);
}


			/* --------------- */

static inline uint32_t
sataf_msa_get_state (sataf_msa *msa) 
{
  ccl_pre (msa != NULL);
  return msa->initial;
}

			/* --------------- */

static inline sataf_sa *
sataf_msa_get_sa (sataf_msa *msa)
{
  return sataf_sa_add_reference (sataf_msa_get_sa_no_ref (msa));
}

			/* --------------- */

static inline sataf_sa *
sataf_msa_get_sa_no_ref (sataf_msa *msa)
{
  ccl_pre (msa != NULL);
  return msa->A;
}

			/* --------------- */

static inline sataf_msa *
sataf_msa_compute (sataf_mao *A)
{
  return sataf_msa_compute_with_transformer (A, NULL);
}

			/* --------------- */

static inline int
sataf_msa_is_zero (sataf_msa *msa)
{
  ccl_pre (msa != NULL);

  return sataf_sa_is_zero (msa->A);
}

			/* --------------- */

static inline int
sataf_msa_is_one (sataf_msa *msa)
{
  ccl_pre (msa != NULL);

  return sataf_sa_is_one (msa->A);
}

			/* --------------- */

static inline int
sataf_msa_is_zero_or_one (sataf_msa *msa)
{
  ccl_pre (msa != NULL);

  return sataf_sa_is_zero_or_one (msa->A);
}

			/* --------------- */


END_C_DECLS

#endif /* ! __SATAF_INLINE_H__ */
