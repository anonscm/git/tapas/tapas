/*
 * mao-memo.c -- Memoizatin for MAO
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-assert.h>
#include <ccl/ccl-iterator.h>
#include <ccl/ccl-hash.h>
#include <ccl/ccl-pool.h>
#include <ccl/ccl-memory.h>
#include "sataf.h"

# define MEMO_POOL_PAGE_SIZE 1000

struct sataf_mao_memorizer_st
{
  ccl_hash *ht;
  size_t memo_size;
  sataf_mao_memo_init_proc *init;
  void *init_data;
  sataf_mao_memo_clean_proc *clean;
  ccl_pool *memo_pool;
};

			/* --------------- */

static unsigned int
s_hash_automaton (const void *key);

static int
s_compare_automata (const void *a1, const void *a2);

static sataf_mao_memo *
s_new_memo (sataf_mao_memorizer *M);

static void
s_delete_memo (sataf_mao_memorizer *M, sataf_mao_memo * m);

			/* --------------- */

sataf_mao_memorizer *
sataf_mao_memorizer_create (size_t memo_size, sataf_mao_memo_init_proc * init,
			void * init_data, sataf_mao_memo_clean_proc * clean)
{
  sataf_mao_memorizer *result;

  ccl_pre (memo_size >= sizeof (sataf_mao_memo));

  result = ccl_new (sataf_mao_memorizer);
  result->ht =
    ccl_hash_create (s_hash_automaton, s_compare_automata, NULL, NULL);
  result->memo_size = memo_size;
  result->init = init;
  result->init_data = init_data;
  result->clean = clean;
  result->memo_pool = ccl_pool_create ("Memo Pool", memo_size,
				       MEMO_POOL_PAGE_SIZE);

  return result;
}

			/* --------------- */

void
sataf_mao_memorizer_delete (sataf_mao_memorizer *M)
{
  ccl_pointer_iterator *i = ccl_hash_get_elements (M->ht);

  while (ccl_iterator_has_more_elements (i))
    {
      sataf_mao_memo *m = (sataf_mao_memo *) ccl_iterator_next_element (i);
      s_delete_memo (M, m);
    }
  ccl_iterator_delete (i);
  ccl_hash_delete (M->ht);
  ccl_pool_delete (M->memo_pool);
  ccl_delete (M);
}

			/* --------------- */

sataf_mao_memo *
sataf_mao_memorizer_remind (sataf_mao_memorizer *M, sataf_mao *A)
{
  sataf_mao_memo *result;

  if (ccl_hash_find (M->ht, A))
    result = (sataf_mao_memo *) ccl_hash_get (M->ht);
  else
    {
      result = s_new_memo (M);
      result->A = sataf_mao_add_reference (A);
      if (M->init != NULL)
	M->init (result, M->init_data);

      ccl_hash_insert (M->ht, result);
    }

  return result;
}

			/* --------------- */

sataf_mao_memo *
sataf_mao_memorizer_succ (sataf_mao_memorizer *M, sataf_mao *A, int letter)
{
  sataf_mao *tmp = sataf_mao_succ (A, letter);
  sataf_mao_memo *R = sataf_mao_memorizer_remind (M, tmp);

  sataf_mao_del_reference (tmp);

  return R;
}

			/* --------------- */

static unsigned int
s_hash_automaton (const void *key)
{
  ccl_pre (key != NULL);

  return sataf_mao_hashcode ((sataf_mao *) key);
}

			/* --------------- */

static int
s_compare_automata (const void *a1, const void *a2)
{
  ccl_pre (a1 != NULL);
  ccl_pre (a2 != NULL);

  if (sataf_mao_equals ((sataf_mao *) a1, (sataf_mao *) a2))
    return 0;
  else if (a1 < a2)
    return -1;
  else
    return 1;
}

			/* --------------- */

static sataf_mao_memo *
s_new_memo (sataf_mao_memorizer *M)
{
  sataf_mao_memo *r = (sataf_mao_memo *) ccl_pool_alloc (M->memo_pool);

  ccl_memzero (r, sizeof (sataf_mao_memo));

  return r;
}

			/* --------------- */

static void
s_delete_memo (sataf_mao_memorizer *M, sataf_mao_memo *m)
{
  if (M->clean != NULL)
    M->clean (m);
  sataf_mao_del_reference (m->A);

  ccl_pool_release (M->memo_pool, m);
}
