/*
 * z3-plugin.c -- 
 * 
 * This file is a part of the GENeric GENEPI plugin for natural numbers. 
 * 
 * Copyright (C) 2013 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <z3.h>

#ifndef GENEPI_PLUGIN_PREFIX      
# warning "GENEPI_PLUGIN_PREFIX is not defined"
# define GENEPI_PLUGIN_PREFIX(_f) z3 ## _f 
#endif /* ! GENEPI_PLUGIN_NAME */

#include <genepi/genepi-plugin.h>

#define CONF_FRESH_PREFIX "fresh_prefix"
#define CONF_LOGIC "logic"

			/* --------------- */

#define ENABLE_AUTOTEST 1

#if 0
# define SIMPLIFY(p,f) 	Z3_simplify ((p)->ctx, (f))
#else
# define SIMPLIFY(p,f) 	(f)
#endif 

#define IMPL(_s) ((genepi_set_impl *) (_s))

struct genepi_plugin_impl_st
{
  Z3_config cfg;
  Z3_context ctx;
  Z3_tactic tactic;
  Z3_solver solver;
  const char *freshprefix;
};

struct genepi_set_impl_st
{
  int refcount;
  Z3_ast F;
  int dim;
};

			/* --------------- */

static const char *OPTIONS[] = {  
  "model", "true",
  "rewriter.algebraic_number_evaluator", "true", 
  "rewriter.arith_lhs", "true", 
  "smt.pull_nested_quantifiers", "true", 
  "smt.macro_finder", "true", 
  NULL, NULL
};

static Z3_tactic 
s_create_tactic (genepi_plugin_impl *plugin);

genepi_plugin_impl *
GENEPI_PLUGIN_FUNC(init) (genepi_plugconf *conf)
{ 
  const char **opts;
  genepi_plugin_impl *result = NULL;
  const char *logicname = genepi_plugconf_get_entry (conf, "logic", NULL);
  
  if (logicname == NULL)
    {
      fprintf (stderr, "%s: SMTlib logic is not defined in this Z3 plugin.\n",
	       genepi_plugconf_get_entry (conf, "name", NULL));
      return NULL;
    }

  for (opts = OPTIONS; *opts; opts += 2)
    Z3_global_param_set (opts[0], opts[1]);

  result = malloc (sizeof (struct genepi_plugin_impl_st));
  result->cfg = Z3_mk_config ();
  result->ctx = Z3_mk_context (result->cfg);  
  result->freshprefix = 
    genepi_plugconf_get_entry (conf, CONF_FRESH_PREFIX, "qv");

  result->tactic = s_create_tactic (result);
  Z3_tactic_inc_ref (result->ctx, result->tactic);

  result->solver = Z3_mk_solver_from_tactic (result->ctx, result->tactic);
  Z3_solver_inc_ref (result->ctx, result->solver);

  return result;
}

void
GENEPI_PLUGIN_FUNC(terminate) (genepi_plugin_impl *plugin)
{  
  Z3_context ctx = plugin->ctx;

  Z3_solver_dec_ref (ctx, plugin->solver);
  Z3_tactic_dec_ref (ctx, plugin->tactic);
  Z3_del_context (ctx);
  Z3_del_config (plugin->cfg);
  free (plugin);
}

			/* --------------- */

static int
s_check_set (genepi_plugin_impl *plugin, const char *descF, genepi_set_impl *X, 
	     const char *er)
{
  Z3_string fstr = Z3_ast_to_string (plugin->ctx, X->F);
  int result = (strcmp (fstr, er) == 0);

  printf ("check %s against\n%s", descF, er);

  if (result)
    printf ("... PASS\n");
  else 
    printf ("... FAIL\n%s\n", fstr);
  printf("\n");

  return result;
}

#if ENABLE_AUTOTEST
int
GENEPI_PLUGIN_FUNC(autotest) (genepi_plugin_impl *plugin)
{

#define N  5
  int result;
  genepi_set_impl *int5 = GENEPI_PLUGIN_FUNC(top) (plugin, N);
  genepi_set_impl *nat5 = GENEPI_PLUGIN_FUNC(top_N) (plugin, N);
  genepi_set_impl *bot5 = GENEPI_PLUGIN_FUNC(bot) (plugin, N);
  int alpha[N] = { 1, 2, 3, 5, 7 };
  genepi_set_impl *geq = 
    GENEPI_PLUGIN_FUNC(linear_operation) (plugin, alpha, N, GENEPI_GEQ, N);
  int sel1[N] = { 0, 1, 0, 1, 0 };
  genepi_set_impl *project1 = 
    GENEPI_PLUGIN_FUNC(project) (plugin, geq, sel1, N);
  genepi_set_impl *project2 = 
    GENEPI_PLUGIN_FUNC(project) (plugin, project1, sel1, 3);
  genepi_set_impl *invproject1 = 
    GENEPI_PLUGIN_FUNC(invproject) (plugin, project2, sel1, 3);
  genepi_set_impl *invproject2 = 
    GENEPI_PLUGIN_FUNC(invproject) (plugin, invproject1, sel1, N);
  
  if (! (result = s_check_set (plugin, "int^5", int5, "true")))
    goto end;
  
  if (! (result = s_check_set (plugin, "nat^5", nat5, 
    "(and (>= k!0 0) (>= k!1 0) (>= k!2 0) "
    "(>= k!3 0) (>= k!4 0))")))
    goto end;

  if (! (result = s_check_set (plugin, "bot^5", bot5, "false")))
    goto end;

  if (! (result = s_check_set (plugin, 
         "geq =def= x0 + 2*x2 + 3*x2 + 5*x3 + 7*x4 >= 5", geq, 
         "(>= (+ (* 1 k!0) (* 2 k!1) (* 3 k!2) (* 5 k!3) (* 7 k!4)) 5)")))
    goto end;

  if (! (result = s_check_set (plugin, "project1 =def= <0,1,0,1,0> geq", 
         project1, 
         "(exists ((qv!0 Int) (qv!1 Int))\n"
         "  (>= (+ k!0 (* 2 qv!0) (* 3 k!1) (* 5 qv!1) (* 7 k!2)) 5))"
    )))
    goto end;

  if (! (result = s_check_set (plugin, "project2 =def= <0,1,0> project1", 
         project2, 
    "(exists ((qv!2 Int))\n"
    "  (exists ((qv!0 Int) (qv!1 Int))\n"
    "    (>= (+ k!0 (* 2 qv!0) (* 3 qv!2) (* 5 qv!1) (* 7 k!1)) 5)))"
    )))
    goto end;

  if (! (result = s_check_set (plugin, 
         "invproject1 =def= <0,1,0> project2", 
         invproject1, 
         "(exists ((qv!2 Int) (qv!0 Int) (qv!1 Int))\n"
         "  (>= (+ k!0 (* 2 qv!0) (* 3 qv!2) (* 5 qv!1) (* 7 k!2)) 5))"
    )))
    goto end;

  if (! (result = s_check_set (plugin, 
         "invproject2 =def= <0,1,0,1,0> invproject1", 
         invproject2, 
         "(exists ((qv!2 Int) (qv!0 Int) (qv!1 Int))\n"
         "  (>= (+ k!0 (* 2 qv!0) (* 3 qv!2) (* 5 qv!1) (* 7 k!4)) 5))"
    )))
    goto end;


  
 end:
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, invproject2);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, invproject1);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, project2);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, project1);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, geq);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, bot5);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, nat5);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, int5);

  return result;
}
#endif
			/* --------------- */


genepi_solver_type 
GENEPI_PLUGIN_FUNC(get_solver_type) (genepi_plugin_impl *plugin)
{
  return GENEPI_Z_SOLVER;
}


			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(add_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X)
{
  X->refcount++;

  return X;
}

			/* --------------- */

void
GENEPI_PLUGIN_FUNC(del_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X)
{
  X->refcount--;

  if (X->refcount == 0)
    {      
      free (X);
    }
}

			/* --------------- */

static genepi_set_impl *
s_mk_set_impl (Z3_ast F, int dim)
{
  genepi_set_impl *R = malloc (sizeof (genepi_set_impl));

  R->refcount = 1;
  R->F = F;
  R->dim = dim;

  return R;
}

genepi_set_impl *
GENEPI_PLUGIN_FUNC(top) (genepi_plugin_impl *plugin, int n)
{
  return s_mk_set_impl (Z3_mk_true (plugin->ctx), n);
}

			/* --------------- */

static Z3_ast 
s_mk_int (genepi_plugin_impl *plugin, int value)
{
  Z3_sort intsort = Z3_mk_int_sort (plugin->ctx);

  return Z3_mk_int (plugin->ctx, value, intsort);
}

static Z3_ast 
s_mk_var (genepi_plugin_impl *plugin, int index)
{
  Z3_symbol id = Z3_mk_int_symbol (plugin->ctx, index);
  Z3_sort intsort = Z3_mk_int_sort (plugin->ctx);
  Z3_ast result = Z3_mk_const (plugin->ctx, id, intsort);

  return (result);
}

static Z3_ast 
s_mk_fresh_var (genepi_plugin_impl *plugin)
{
  Z3_sort intsort = Z3_mk_int_sort (plugin->ctx);
  Z3_ast result = Z3_mk_fresh_const (plugin->ctx, plugin->freshprefix, intsort);

  return (result);
}

static Z3_ast 
s_mk_ge (genepi_plugin_impl *plugin, Z3_ast var, int value)
{
  Z3_ast c = s_mk_int (plugin, value);
  Z3_ast result = Z3_mk_ge (plugin->ctx, var, c);

  return result;
}

genepi_set_impl *
GENEPI_PLUGIN_FUNC(top_N) (genepi_plugin_impl *plugin, int n)
{
  int i;
  Z3_ast F;
  Z3_ast *ops = calloc (n, sizeof (Z3_ast));

  assert (n > 0);

  for (i = 0; i < n; i++)
    ops[i] = s_mk_ge (plugin, s_mk_var (plugin, i), 0);
  F = Z3_mk_and (plugin->ctx, n, ops);
  free (ops);

  return s_mk_set_impl (F, n);
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_Z) (genepi_plugin_impl *plugin, int n)
{
  return GENEPI_PLUGIN_FUNC (top) (plugin, n);
}

			/* --------------- */

/*
genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_P) (int n);
*/

			/* --------------- */

/* 
genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_R) (int n);
*/

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(bot) (genepi_plugin_impl *plugin, int n)
{
  return s_mk_set_impl (Z3_mk_false (plugin->ctx), n);
}

			/* --------------- */

static Z3_ast 
s_mk_linear_term (genepi_plugin_impl *plugin, const int *alpha, int alpha_size)
{
  int i;
  Z3_ast result;
  int nb_monomials = 0;
  Z3_ast *monomials = calloc (alpha_size, sizeof (Z3_ast));

  for (i = 0; i < alpha_size; i++)
    {
      if (alpha[i] != 0)
	{
	  Z3_ast coef = s_mk_int (plugin, alpha[i]);
	  Z3_ast var = s_mk_var (plugin, i);
	  Z3_ast ops [] = { coef, var };

	  monomials[nb_monomials] = Z3_mk_mul (plugin->ctx, 2, ops);
	  nb_monomials++;
	}
    }
  assert (nb_monomials > 0);
  result = Z3_mk_add (plugin->ctx, nb_monomials, monomials);

  free (monomials);

  return result;
}

genepi_set_impl *
GENEPI_PLUGIN_FUNC(linear_operation) (genepi_plugin_impl *plugin, 
				      const int *alpha, int alpha_size, 
				      genepi_comparator op, int c)
{
  Z3_ast linearterm = s_mk_linear_term (plugin, alpha, alpha_size);
  Z3_ast cst = s_mk_int (plugin, c);
  Z3_ast F = NULL;

  switch (op)
    {
    case GENEPI_EQ: F = Z3_mk_eq (plugin->ctx, linearterm, cst); break;
    case GENEPI_LT: F = Z3_mk_lt (plugin->ctx, linearterm, cst); break;
    case GENEPI_GT: F = Z3_mk_gt (plugin->ctx, linearterm, cst); break;
    case GENEPI_LEQ: F = Z3_mk_le (plugin->ctx, linearterm, cst); break;
    case GENEPI_GEQ: F = Z3_mk_ge (plugin->ctx, linearterm, cst); break;
    default: abort ();
    }
  
  return s_mk_set_impl (F, alpha_size);
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_union) (genepi_plugin_impl *plugin, genepi_set_impl *X1,
			       genepi_set_impl *X2)
{
  Z3_ast ops[] = { X1->F, X2->F };

  return s_mk_set_impl (Z3_mk_or (plugin->ctx, 2, ops), X1->dim);
}


			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_intersection) (genepi_plugin_impl *plugin, 
				      genepi_set_impl *X1, genepi_set_impl *X2)
{
  Z3_ast ops[] = { X1->F, X2->F };

  return s_mk_set_impl (Z3_mk_and (plugin->ctx, 2, ops), X1->dim);
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_complement) (genepi_plugin_impl *plugin, 
				    genepi_set_impl *X)
{
  return s_mk_set_impl (Z3_mk_not (plugin->ctx, X->F), X->dim);
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(project) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
			     const int *selection, int size)
{
  int i;
  Z3_ast qF = X->F;
  Z3_ast *oldnames = calloc (2 * size, sizeof (Z3_ast));
  Z3_ast *newnames = oldnames + size;
  Z3_app *qvars = calloc (size, sizeof (Z3_app));
  int dim = 0;
  int nbqvars = 0;

  for (i = 0; i < size; i++)
    {
      oldnames[i] = s_mk_var (plugin, i);
      if (selection[i])
	{
	  newnames[i] = s_mk_fresh_var (plugin);
	  qvars[nbqvars] = Z3_to_app (plugin->ctx, newnames[i]);
	  nbqvars++;
	}
      else
	{
	  newnames[i] = s_mk_var (plugin, dim);
	  dim++;
	}
    }
  
  qF = Z3_substitute (plugin->ctx, X->F, size, oldnames, newnames);
  qF = Z3_mk_exists_const (plugin->ctx, 0, nbqvars, qvars, 0, NULL, qF);
  qF = SIMPLIFY (plugin, qF);

  free (oldnames);
  free (qvars);

  return s_mk_set_impl (qF, dim);
}

			/* --------------- */


genepi_set_impl *
GENEPI_PLUGIN_FUNC(invproject) (genepi_plugin_impl *plugin, genepi_set_impl *X,
				const int *selection, int size)
{
  int i, j;
  Z3_ast *oldnames = calloc (2 * size, sizeof (Z3_ast));
  Z3_ast *newnames = oldnames + size;
  Z3_ast newF;

  assert (size >= X->dim);

  for (i = j = 0; i < size; i++)
    {
      if (selection[i] == 0)
	{	  
	  oldnames[j] = s_mk_var (plugin, j);
	  newnames[j] = s_mk_var (plugin, i);
	  j++;
	}
    }
  assert (j == X->dim);

  newF = Z3_substitute (plugin->ctx, X->F, j, oldnames, newnames);
  
  free (oldnames);

  return s_mk_set_impl (newF, size);
}


static int
s_check_satisfiability (genepi_plugin_impl *plugin, Z3_ast F, 
			Z3_lbool expected_value)
{
  int result;
  Z3_lbool z3res;
  Z3_context ctx = plugin->ctx;
  Z3_solver solver = plugin->solver;
  
  Z3_solver_reset (ctx, solver);
  Z3_solver_assert (ctx, solver, F);
  z3res = Z3_solver_check (ctx, solver);
  if (z3res == Z3_L_UNDEF)
    {
      fprintf (stderr, "cannot check satisfiability of formula:\n%s\n",
	       Z3_ast_to_string (ctx, F));
      fprintf (stderr, "reason is:\n%s\n",
	       Z3_solver_get_reason_unknown (ctx, solver));
      abort ();
    }

  result = (z3res == expected_value);

  return result;  
}

#define M_SAT(P, F) s_check_satisfiability (P, F, Z3_L_TRUE)
#define M_UNSAT(P, F) s_check_satisfiability (P, F, Z3_L_FALSE)

static Z3_ast 
s_mk_diff (genepi_plugin_impl *plugin, genepi_set_impl *X, genepi_set_impl *Y)
{
  Z3_ast ops[2];
  ops[0] = X->F;
  ops[1] = Z3_mk_not (plugin->ctx, Y->F);
  
  return Z3_mk_and (plugin->ctx, 2, ops);
}

int 
GENEPI_PLUGIN_FUNC(compare) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
			     genepi_comparator op, genepi_set_impl *X2)
{
  int result;

  assert (X1->dim == X2->dim);

  if (op == GENEPI_EQ)    
    result = (M_UNSAT (plugin, s_mk_diff (plugin, X1, X2)) &&
	      M_UNSAT (plugin, s_mk_diff (plugin, X2, X1)));
  else if (op == GENEPI_LEQ)
    result = M_UNSAT (plugin, s_mk_diff (plugin, X1, X2));
  else if (op == GENEPI_GEQ)
    result = M_UNSAT (plugin, s_mk_diff (plugin, X2, X1));
  else if (op == GENEPI_LT)
    result = (M_UNSAT (plugin, s_mk_diff (plugin, X1, X2)) &&
	      M_SAT (plugin, s_mk_diff (plugin, X2, X1)));
  else
    result = (M_SAT (plugin, s_mk_diff (plugin, X1, X2)) &&
	      M_UNSAT (plugin, s_mk_diff (plugin, X2, X1)));
  
  return result;
}

			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(is_empty) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return M_UNSAT (plugin, X->F);
}



/*
int 
GENEPI_PLUGIN_FUNC(is_full) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return genepi_set_is_full (plugin->solver, (genepi_set *) X);
}
*/

/*
int 
GENEPI_PLUGIN_FUNC(is_finite) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return genepi_set_is_finite (plugin->solver, (genepi_set *) X);
}
*/

/*
int
GENEPI_PLUGIN_FUNC(depend_on) (genepi_set_impl *X, const int *sel, int selsize);
*/


			/* --------------- */
/*
void 
GENEPI_PLUGIN_FUNC(get_solutions) (genepi_set_impl *X, int ***psolutions, 
				   int *psize,
				   int max);
*/

/*
void 
GENEPI_PLUGIN_FUNC(display_all_solutions) (genepi_plugin_impl *plugin, 
					   genepi_set_impl *X, FILE *output,
					   const char * const *varnames)
{
}
*/

void 
GENEPI_PLUGIN_FUNC(display_data_structure) (genepi_plugin_impl *plugin, 
					    genepi_set_impl *X, FILE *output)
{
  Z3_string fstr = Z3_ast_to_string (plugin->ctx, X->F);
  fprintf (output, "%s", fstr);
}

int 
GENEPI_PLUGIN_FUNC(get_width) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return X->dim;
}

/*
int 
GENEPI_PLUGIN_FUNC(get_data_structure_size) (genepi_plugin_impl *plugin, 
					     genepi_set_impl *X)
{
}
*/

/*
int
GENEPI_PLUGIN_FUNC(is_solution) (genepi_plugin_impl *plugin, genepi_set_impl *X,
				 const int *x, int x_size, int xden)
{
}
*/


int
GENEPI_PLUGIN_FUNC (get_one_solution) (genepi_plugin_impl *plugin, 
				       genepi_set_impl *X, int *x, int x_size)
{  
  int result = M_SAT (plugin, X->F);

  if (result)
    {
      int i;
      Z3_context ctx = plugin->ctx;
      Z3_model m = Z3_solver_get_model (ctx, plugin->solver);
      Z3_model_inc_ref (ctx, m);
      for (i = 0; i < x_size; i++)
	{
	  Z3_ast v = s_mk_var (plugin, i);
	  Z3_ast res = NULL;
	  Z3_bool ok = Z3_model_eval (ctx, m, v, Z3_TRUE, &res);

	  assert (ok);

	  ok = Z3_get_numeral_int (ctx, res, &x[i]);
	  assert (ok);
	}
      Z3_model_dec_ref (ctx, m);
    }

  return 1;
}

#if ENABLE_ALL_PROTOS
unsigned int
GENEPI_PLUGIN_FUNC(cache_hash) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return 0;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC(cache_equal) (genepi_plugin_impl *plugin, 
				 genepi_set_impl *X1, genepi_set_impl *X2)
{
  return 0;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC (write_set) (genepi_plugin_impl *plugin, FILE *output, 
				genepi_set_impl *set)
{
  return GENEPI_RW_ERR_IO;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC (read_set) (genepi_plugin_impl *plugin, FILE *input, 
			       genepi_set_impl **pset)
{
  return GENEPI_RW_ERR_IO;
}
#endif 

			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC (preorder) (genepi_plugin_impl *plugin, genepi_set_impl *X1,
			       genepi_set_impl *X2)
{
  return 0;
}
#endif 

static Z3_tactic 
s_create_tactic (genepi_plugin_impl *plugin)
{
  static const char *tactic_sequence[] = 
    { "simplify", "solve-eqs", "qe", "smt"};
  static const size_t nb_tactics = 
    sizeof (tactic_sequence) / sizeof (tactic_sequence[0]);
  Z3_context ctx = plugin->ctx;
  int s;
  Z3_tactic result = Z3_mk_tactic (ctx, tactic_sequence[0]);
  Z3_tactic_inc_ref (ctx,  result);

  for (s = 1; s < nb_tactics; s++)
    {
      Z3_tactic t, new_t;

      t = Z3_mk_tactic (ctx, tactic_sequence[s]);
      Z3_tactic_inc_ref (ctx,  t);
      new_t = Z3_tactic_and_then (ctx, result, t);
      Z3_tactic_inc_ref (ctx,  new_t);

      assert (new_t != result);
      assert (new_t != t);
      Z3_tactic_dec_ref (ctx,  result);
      Z3_tactic_dec_ref (ctx,  t);
      result = new_t;
    }
  return result;
}
