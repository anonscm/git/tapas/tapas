dnl
dnl Process this file with autoconf in order to generate the configure script
dnl used by clients.
dnl
dnl $Revision: 1.6 $
dnl

AC_PREREQ(2.59)
AC_INIT([genepi-z3-plugin], [0.1], [labri.altarica-bugs@diff.u-bordeaux.fr])
AM_INIT_AUTOMAKE([foreign])
AC_CONFIG_SRCDIR([z3-plugin.c])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_MACRO_DIR([m4])

# Checks for programs.
AC_PROG_CC
AC_PROG_LIBTOOL
if test "${exec_prefix}" != "NONE"; then
    eval PKG_CONFIG_PATH=\"'$'PKG_CONFIG_PATH:${libdir}/pkgconfig\"
    eval LDFLAGS=\"'$'LDFLAGS -L${libdir}\"
else
    PKG_CONFIG_PATH="$PKG_CONFIG_PATH:${prefix}/lib/pkgconfig"
    LDFLAGS="$LDFLAGS -L${prefix}/lib"
fi
export PKG_CONFIG_PATH
AC_MSG_NOTICE([setting PKG_CONFIG_PATH to '${PKG_CONFIG_PATH}'])

PKG_CHECK_MODULES([GENEPI],[genepi >= 3.1])
CPPFLAGS="${CPPFLAGS} ${GENEPI_CFLAGS}"
LDFLAGS="${LDFLAGS} ${GENEPI_LIBS}"

AC_SUBST(GENEPI_PLUGIN_DIR)
GENEPI_PLUGIN_DIR=`pkg-config --variable=plugin_directory genepi`

AC_ARG_WITH([z3-include],
            [AS_HELP_STRING([--with-z3-include=DIR],
            [specify that z3.h header file is located in directory DIR.])])

AC_ARG_WITH([z3-lib],
            [AS_HELP_STRING([--with-z3-lib=DIR],
            [specify that Z3 library is located in directory DIR.])])

AS_IF([test "x${with_z3_include}" != "x" -a -d "${with_z3_include}"],
      [CPPFLAGS="${CPPFLAGS} -I{with_z3_include}])

AS_IF([test "x${with_z3_lib}" != "x" -a -d "${with_z3_lib}"],
      [LDFLAGS="${LDFLAGS} -L{with_z3_lib}])

AC_CHECK_HEADER([z3.h],
                [],
	        [AC_MSG_ERROR([[cannot find z3.h header file. Consider using --with-z3-header=dir option.]])])

AC_CHECK_LIB([z3],
	     [Z3_mk_config],
             [],
	     [AC_MSG_ERROR([[cannot find Z3 library. Consider using --with-z3-lib=dir option.]])])

AC_CONFIG_FILES([Makefile \
                 z3-lia-plugin.conf \
                 genepi-z3-plugin.pc])
AC_OUTPUT
