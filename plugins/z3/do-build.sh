#!/bin/sh

CFLAGS="-Wall -ggdb"

loc=$(dirname $0)
case "$loc" in
    "/"*)
	CONFIGURE=${loc}/configure
	;;
    *)
	CONFIGURE=${PWD}/${loc}/configure
esac


TOLOWER="tr '[A-Z]' '[a-z]'"
cpu=`(uname -p 2>/dev/null || uname -m 2>/dev/null || echo unknown) | \
        ${TOLOWER}`
os=`uname -s | ${TOLOWER}`

case "$os" in
    darwin*) 
#	CFLAGS="${CFLAGS} -no_pie"
	;;
esac

case "$cpu" in
    unknown|*" "*)
        cpu=`uname -m | ${TOLOWER}` ;;
esac

BUILDDIR=${os}-${cpu}

[ -d ${BUILDDIR} ] || mkdir ${BUILDDIR}

cd ${BUILDDIR}

CFLAGS="$CFLAGS" ${CONFIGURE} --prefix=$INST "$@"

exec make 
