/*
 * lira-plugin.cc -- LIRA Plugin implementation
 * 
 * This file is a part of the LIRA-based plugin for GENEPI. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <cassert>
#include <iostream>
#include <fstream>
#include <LIRA/LIRA.h>
#include <genepi/genepi-plugin.h>
#include <sstream>


using namespace std;

			/* --------------- */

struct genepi_plugin_impl_st {
  rva::Manager *manager;
};

struct genepi_set_impl_st {
  rva::Manager *manager;
  rva::automaton* a;
  int number_reference;
  int dim;
};

			/* --------------- */

static rva::automaton * 
s_minimizer (rva::Manager *mgr, rva::automaton *a);

			/* --------------- */

genepi_plugin_impl *
GENEPI_PLUGIN_FUNC(init) (genepi_plugconf *conf) {
  genepi_plugin_impl *result = new genepi_plugin_impl;

  result->manager = new rva::Manager;
  
  return result;
}

			/* --------------- */


void
GENEPI_PLUGIN_FUNC(terminate) (genepi_plugin_impl *plugin) {
  plugin->manager->deref ();
  delete plugin;
}


			/* --------------- */

int
GENEPI_PLUGIN_FUNC(autotest) (genepi_plugin_impl *plugin) {
  return 1;
}

			/* --------------- */

genepi_solver_type 
GENEPI_PLUGIN_FUNC(get_solver_type) (genepi_plugin_impl *plugin) {
  return GENEPI_R_SOLVER;
}


			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(add_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X) {
  X->number_reference++;

  return X;
}

			/* --------------- */


void
GENEPI_PLUGIN_FUNC(del_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X) {
  X->number_reference--;

  if (X->number_reference == 0) {
    delete X->a;
    delete X;
  }
}
 
			/* --------------- */


genepi_set_impl *
GENEPI_PLUGIN_FUNC(top) (genepi_plugin_impl *plugin, int n) {
  genepi_set_impl *R = new genepi_set_impl;

  R->manager = plugin->manager;
  R->number_reference = 1;
  R->dim = n;
  R->a = rva::accept_all (plugin->manager, true, rva::RVA);

  return R;
}

			/* --------------- */

static rva::automaton *
s_mk_integer_set (genepi_plugin_impl *plugin, int n) {
  rva::automaton* sol = rva::accept_all (plugin->manager, true, rva::RVA);

  sol->flags_ |= rva::DONTCARES;

  for (int i = 0; i < n; i++) {
    rva::automaton *tmp;
    rva::automaton *tmp1 = rva::integer (plugin->manager, i);
    
    tmp1->flags_ |= rva::DONTCARES;
    tmp = rva::product (plugin->manager, sol, tmp1, rva::And);
    delete tmp1;
    delete sol;
    sol=tmp;
      
    tmp = s_minimizer (plugin->manager, sol);
    delete sol;
    sol=tmp;
  }

  return sol;
}

			/* --------------- */

static rva::automaton *
s_mk_positive_set (genepi_plugin_impl *plugin, int n) {
  rva::automaton *sol = rva::accept_all (plugin->manager, true, rva::RVA);
  
  sol->flags_ |= rva::DONTCARES;
  std::vector<int> coeff (n);

  for (int i = 0; i < n; i++)
    coeff[i] = 0;

  for (int i = 0; i < n; i++) {
    rva::automaton *tmp;
    rva::automaton *tmp1;
    
    coeff[i] = -1;
    
    tmp1 = rva::inequation (plugin->manager, coeff, 0);
    tmp1->flags_ |= rva::DONTCARES;
    coeff[i] = 0;    
    tmp = rva::product (plugin->manager, sol, tmp1, rva::And);
    delete tmp1;
    delete sol;
    sol = tmp;
    tmp = s_minimizer (plugin->manager, sol);
    delete sol;
    sol = tmp;
  }

  return sol;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(top_N) (genepi_plugin_impl *plugin, int n) {
  rva::automaton *tmp1;
  rva::automaton *tmp2;
  rva::automaton* tmp;
  genepi_set_impl *R = new genepi_set_impl;

  R->number_reference = 1;
  R->dim = n;
  tmp1 = s_mk_integer_set (plugin, n);
  tmp2 = s_mk_positive_set (plugin, n);  
  tmp = rva::product (plugin->manager, tmp1, tmp2, rva::And);
  delete tmp1;
  delete tmp2;
  R->a = s_minimizer (plugin->manager, tmp);
  delete tmp;

  R->manager = plugin->manager;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_Z) (genepi_plugin_impl *plugin, int n) {
  genepi_set_impl *R = new genepi_set_impl;

  R->manager = plugin->manager;
  R->number_reference = 1;
  R->dim = n;
  R->a = s_mk_integer_set(plugin, n);

  return R;
}


			/* --------------- */


genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_P) (genepi_plugin_impl *plugin, int n) {
  genepi_set_impl *R = new genepi_set_impl;
  
  R->manager = plugin->manager;
  R->number_reference = 1;
  R->dim = n;
  R->a = s_mk_positive_set(plugin, n);

  return R;
}


			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_R) (genepi_plugin_impl *plugin, int n) {
  genepi_set_impl *R = new genepi_set_impl;

  R->manager = plugin->manager;
  R->number_reference = 1;
  R->dim = n;
  R->a = rva::accept_all (plugin->manager, true, rva::RVA);
  R->a->flags_ |= rva::DONTCARES;

  return R;  
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(bot) (genepi_plugin_impl *plugin, int n) {
  genepi_set_impl *R = new genepi_set_impl;

  R->manager = plugin->manager;
  R->number_reference = 1;
  R->dim = n;
  R->a = rva::accept_all (plugin->manager, false, rva::RVA);
  R->a->flags_ |= rva::DONTCARES;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(linear_operation) (genepi_plugin_impl *plugin, 
				      const int *alpha, int alpha_size, 
				      genepi_comparator op, int c) {  
  std::vector<int> coeff (alpha_size);
  
  if ((op == GENEPI_GEQ) || (op == GENEPI_LT)) {
    for (int i=0; i < alpha_size; i++)
      coeff[i] = -alpha[i];
    c = -c;
    if (op == GENEPI_GEQ)
      op = GENEPI_LEQ;
    else
      op = GENEPI_GT;
  } else  {      
    for (int i = 0; i < alpha_size; i++)
      coeff[i] = alpha[i];
    c = c;
  }
  
  genepi_set_impl *R = new genepi_set_impl;
  R->manager = plugin->manager;
  R->number_reference = 1;
  R->dim = alpha_size;
  if (op == GENEPI_EQ)
    R->a = rva::equation (plugin->manager, coeff, c);
  else
    R->a = rva::inequation (plugin->manager, coeff, c);

  if (op == GENEPI_GT) {
    rva::automaton* tmp = rva::negate (R->a);
    delete R->a;
    R->a = tmp;
  }

  R->a->flags_ |= rva::DONTCARES;
  rva::automaton *tmp = s_minimizer(plugin->manager, R->a);
  delete R->a;
  R->a = tmp;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_union) (genepi_plugin_impl *plugin, genepi_set_impl *X1,
			       genepi_set_impl *X2) {
  genepi_set_impl *R = new genepi_set_impl;

  R->manager = plugin->manager;
  R->number_reference=1;
  R->dim = X1->dim;
  rva::automaton *tmp = rva::product(plugin->manager, X1->a,X2->a,rva::Or);
  R->a = s_minimizer(plugin->manager, tmp);
  delete tmp;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_intersection) (genepi_plugin_impl *plugin, 
				      genepi_set_impl *X1, 
				      genepi_set_impl *X2) {
  genepi_set_impl *R = new genepi_set_impl;
  R->manager = plugin->manager;
  R->number_reference = 1;
  R->dim = X1->dim;
  rva::automaton *tmp = rva::product (plugin->manager, X1->a, X2->a, rva::And);
  R->a = s_minimizer (plugin->manager, tmp);
  delete tmp;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_complement) (genepi_plugin_impl *plugin, 
				    genepi_set_impl *X) {
  genepi_set_impl *R = new genepi_set_impl;

  R->manager = plugin->manager;
  R->number_reference = 1;
  R->dim = X->dim;
  R->a = rva::negate (X->a);

  return R;
}

			/* --------------- */


genepi_set_impl *
GENEPI_PLUGIN_FUNC(project) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
			     const int *selection, int size) {
  rva::automaton *set = new rva::automaton (* (X->a));
  std::map<unsigned,unsigned> map;
  genepi_set_impl *R = new genepi_set_impl;
  R->number_reference = 1;
  R->dim = size;
  //std::vector<unsigned> vars;

  for(int i = 0, off = 0; i < size; i++) {
    if (selection[i] == 1) {
      R->dim--;
      map[i] = R->dim;
      // We quantify variable i
      int var = i;
      rva::AutoUncompress uc_callback (var);
      rva::automaton *uncompressed = 
	rva::uncompress (plugin->manager, set, var);
      delete set;
      rva::automaton *normalized = rva::walk_scc (uncompressed, uc_callback);
      delete uncompressed;
      rva::automaton *minimized = rva::minimize (plugin->manager, normalized);
      delete normalized;
      // quantify the variable
      std::vector<unsigned> vars;
      vars.push_back (var);
      rva::automaton *quantified = 
	rva::exists (plugin->manager, vars, minimized);
      delete minimized;
      rva::AutoNormalizeDC dc_callback;
      normalized = rva::walk_scc (quantified, dc_callback);
      delete quantified;
      set = rva::minimize (plugin->manager, normalized);
      delete normalized;
      // Done
      off--;
    } else {
      map[i] = i + off;
    }
  }
  //rva::automaton *tmp=rva::exists(vars,X->a);
  //rva::automaton *min=minimizer(tmp);
  //delete tmp;
  R->manager = plugin->manager;
  R->a = rva::swap_variables (plugin->manager, set, map);
  delete set;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(invproject) (genepi_plugin_impl *plugin, genepi_set_impl *X,
				const int *selection, int size) {
  std::map<unsigned,unsigned> map;
  std::vector<rva::bdd::DdNode*> nodes;
  genepi_set_impl *R = new genepi_set_impl;

  R->manager = plugin->manager;
  R->dim = size;
  R->number_reference = 1;

  for (int i = 0, off = 0, dim = size; i < size; i++) {
    rva::bdd::DdNode *node = 
      rva::bdd::Cudd_addIthVar (plugin->manager->manager_, i + 1);
    rva::bdd::Cudd_Ref (node);
    nodes.push_back (node);
    if (selection[i] == 0)
      map[i + off] = i;
    else {
      dim--;
      map[dim] = i;
      off--;
    }
  }

  rva::automaton *tmp = rva::swap_variables (plugin->manager, X->a,map);
  //R->a=rva::presburger_fixup(tmp,size);
  //delete tmp;
  R->a = tmp;
  for (int i = 0; i < size; i++)
    rva::bdd::Cudd_RecursiveDeref(plugin->manager->manager_,nodes[i]);

  return R;
}

			/* --------------- */

static int
s_lira_genepi_set_equal (genepi_set_impl *X1, genepi_set_impl *X2)
{
  assert (X1->manager == X2->manager);
  rva::automaton *tmp = rva::product (X1->manager, X1->a, X2->a, rva::ExOr);
  rva::automaton *min = s_minimizer (X1->manager, tmp);
  delete tmp;
  int result = rva::is_empty(min);
  delete min;

  return result;
}

			/* --------------- */

static int
s_lira_genepi_set_is_included_in (genepi_set_impl *X1, genepi_set_impl *X2)
{
  assert (X1->manager == X2->manager);
  rva::automaton *tmp = rva::product (X1->manager, X1->a, X2->a, rva::Minus);
  rva::automaton *min = s_minimizer (X1->manager, tmp);
  delete tmp;
  //rva::dump_dot(min,cout);
  //fprintf(stdout,oss.str().c_str());
  int result = rva::is_empty (min);
  delete min;

  return result;
}

			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(compare) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
			     genepi_comparator op, genepi_set_impl *X2)
{
  if (X1->dim != X2->dim)
    return 0;

  switch (op) {
  case GENEPI_EQ:
    return s_lira_genepi_set_equal (X1,X2);
  case GENEPI_LEQ:
    return s_lira_genepi_set_is_included_in (X1,X2);
  case GENEPI_GEQ:
    return s_lira_genepi_set_is_included_in (X2,X1);
  case GENEPI_LT:
    return (s_lira_genepi_set_is_included_in (X1,X2) && 
	    !s_lira_genepi_set_equal(X1,X2));
  case GENEPI_GT:
    return (s_lira_genepi_set_is_included_in(X2,X1) && 
	    !s_lira_genepi_set_equal(X1,X2));
  }

  return 0;
}

			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(is_empty) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return rva::is_empty (X->a);
}


			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(is_full) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  rva::automaton *tmp = rva::negate (X->a);
  bool full = rva::is_empty(tmp);
  delete tmp;

  return full;
}

			/* --------------- */



void 
GENEPI_PLUGIN_FUNC(display_data_structure) (genepi_plugin_impl *plugin, 
					    genepi_set_impl *X, FILE *output)
{
  ostringstream oss;

  rva::dump_dot (X->a,oss);

  fprintf(output, oss.str ().c_str ());
}


			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(get_width) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return X->dim;
}


			/* --------------- */


int 
GENEPI_PLUGIN_FUNC(get_data_structure_size) (genepi_plugin_impl *plugin, 
					     genepi_set_impl *X)
{
  return X->a->transitions_.size ();
}



	/* --------------- */


static int 
s_power (int n)
{
  int d = 1;

  assert (n >= 0);

  while (n > 0) {
    n--;
    d = 2 * d;
  }

  return d;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC (get_one_solution) (genepi_plugin_impl *plugin, 
				       genepi_set_impl *X, int *x, int x_size)
{
  if (is_empty (X->a))
    return 0;
  
  std::vector<std::string> example;
  rva::dump_example (X->a, example);
  
  //We compute the integers k, l and n such that 
  //the examples are i1...ik.a_1...a_l(b_1...b_n)^\omega
  //We assume that k,l,n are uniform over the variables
  int k,l,n;
  {    
    std::string s = example[0];
    int pos_init;
    int pos = 0;
    pos_init = pos;
    while (s[pos] != '.')
      pos++;

    k = pos-pos_init;
    pos++;
    pos_init = pos;
    while (s[pos] != '(')
      pos++;
    l = pos-pos_init;
    pos++;
    pos_init = pos;
    while (s[pos] != ')')
      pos++;
    n = pos-pos_init;
    //cerr<<"k="<<k<<"l="<<l<<"n="<<n<<endl;  
  }
  
  //cout<<k<<endl<<l<<endl<<n<<endl;
  int denominator = s_power (l) * (s_power (n) - 1);
  //cout<<denominator<<endl;
  

  for(int var = 0; var < x_size; var++) {
    std::string s = example[var];
    //cout<<s<<endl;

    int z;
    
    //Sign bit
    if (s[0] == '0')
      z = 0;
    else if (s[0] == '1')
      z = -1;
    else {
      cerr << "Internal error" << endl;
      exit (0);
    }
    
    //Integer part
    for (int pos = 0; pos < k; pos++)
      if (s[pos] == '0')
	z = 2 * z;
      else if (s[pos] == '1')
	z = 2 * z + 1;
      else {
	cerr << "Internal error" << endl;
	exit (0);
      }
    
    //Decimal part corresponding to the finite part
    int da = 0;
    for(int pos =k + 1; pos < k + 1 + l; pos++)
      if (s[pos] == '0')
	da = 2 * da;
      else if (s[pos] == '1')
	da = 2 * da + 1;
      else {
	cerr << "Internal error" << endl;
	exit (0);
      }
    
    //Decimal part corresponding to the repeated part
    int db = 0;
    for(int pos = k + 1 + l + 1; pos < k + 1 + l + 1 + n; pos++)
      if (s[pos] == '0')
	db= 2 * db;
      else if (s[pos] == '1')
	db=2 * db + 1;
      else {
	cerr << "Internal error" << endl;
	exit (0);
      }  
    //cout <<z<<endl<<da<<endl<<db<<endl;
    
    x[var] = denominator * z + (s_power (n) - 1) * da + db;
    
    //cout<<denominator;

  }
  
  return denominator;
}

			/* --------------- */

static rva::automaton * 
s_minimizer (rva::Manager *mgr, rva::automaton *a) {
  rva::AutoNormalizeDC local_clbk;
  rva::automaton *tmp = rva::walk_scc (a, local_clbk);
  rva::automaton *sol = rva::minimize (mgr, tmp);
  delete tmp;

  return sol;
}
