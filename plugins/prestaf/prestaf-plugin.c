/*
 * prestaf-plugin.c -- implementation of GENEPI spec with the PRESTAF library.
 * 
 * This file is a part of the PresTAF project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-time.h>
#include <stdio.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-hash.h>
#include <ccl/ccl-bittable.h>
#include <ccl/ccl-init.h>
#include <ccl/ccl-log.h>
#include <ccl/ccl-time.h>
#include <sataf/sataf.h>
#include <prestaf/prestaf-automata.h>
#include <prestaf/prestaf-batof.h>
#include <genepi/genepi-plugin.h>
#include <genepi/genepi-io.h>
#include <armoise/armoise-interp.h>

			/* --------------- */

#ifndef MSA_OP_CACHE_SIZE
# define MSA_OP_CACHE_SIZE 1000457
#endif /* MSA_OP_CACHE_SIZE */

#ifndef MAO_SHARING_CACHE_SIZE
# define MAO_SHARING_CACHE_SIZE 11229331
#endif /* ! MAO_SHARING_CACHE_SIZE */

#ifndef EA_UT_INIT_TABLE_SIZE
# define EA_UT_INIT_TABLE_SIZE 1403641
#endif /* ! EA_UT_INIT_TABLE_SIZE */

#ifndef EA_UT_FILL_DEGREE 
# define EA_UT_FILL_DEGREE 5
#endif /* ! EA_UT_FILL_DEGREE */

#ifndef SA_UT_INIT_TABLE_SIZE
# define SA_UT_INIT_TABLE_SIZE 1403641
#endif /* ! SA_UT_INIT_TABLE_SIZE */

#ifndef SA_UT_FILL_DEGREE
# define SA_UT_FILL_DEGREE 5
#endif /* ! SA_UT_FILL_DEGREE */

#ifndef MSA_UT_INIT_TABLE_SIZE 
# define MSA_UT_INIT_TABLE_SIZE 1403641
#endif /* ! MSA_UT_INIT_TABLE_SIZE */

#ifndef MSA_UT_FILL_DEGREE 
# define MSA_UT_FILL_DEGREE 5
#endif /* ! MSA_UT_FILL_DEGREE */

#ifndef ENUMERATE_PREFIXES
# define ENUMERATE_PREFIXES 0
#endif /* ! ENUMERATE_PREFIXES */

#ifndef CHECK_GENERATED_FORMULA
# define CHECK_GENERATED_FORMULA 1
#endif /* ! CHECK_GENERATED_FORMULA */

#define USE_GENERIC_APPLY
#define CACHE_SIZE_INDEX 10
#define CCL_DEBUG_MAX_LEVEL 0
#define USE_AMONG 0
#define DISPLAY_MSA_INFO 0

			/* --------------- */

struct genepi_set_impl_st
{
  int refcount;
  int width;
  sataf_msa *msa;
};

			/* --------------- */

static genepi_set_impl *
s_new_set (int width);

static void
s_log_listener (ccl_log_type type, const char *msg, void *data);

static int **
s_get_solutions (sataf_msa *msa, int n, int *psize, int max);

static void
s_log_to_file (ccl_log_type type, const char *msg, void *data);

			/* --------------- */

static int use_logproc = 0;
static int init_counter = 0;

static const char *CCL_DEBUG_MAX_LEVEL_PROP = "ccl-debug-max-level";
static const char *MSA_OP_CACHE_SIZE_PROP = "msa-op-cache-size";
static const char *MAO_SHARING_CACHE_SIZE_PROP = "mao-sharing-cache-size";
static const char *EA_UT_INIT_TABLE_SIZE_PROP = "ea-ut-init-table-size";
static const char *EA_UT_FILL_DEGREE_PROP = "ea-ut-fill-degree";
static const char *SA_UT_INIT_TABLE_SIZE_PROP = "sa-ut-init-table-size";
static const char *SA_UT_FILL_DEGREE_PROP = "sa-ut-fill-degree";
static const char *MSA_UT_INIT_TABLE_SIZE_PROP = "msa-ut-init-table-size";
static const char *MSA_UT_FILL_DEGREE_PROP = "msa-ut-fill-degree";
static const char *ENUMERATE_PREFIXES_PROP = "batof-enumerate-prefixes";
static const char *CHECK_GENERATED_FORMULA_PROP = "batof-check-formula";
static const char *USE_AMONG_PROP = "use-among";
static const char *DISPLAY_MSA_INFO_PROP = "display-structure-display-msa-info";

#define IPROP(conf, prop) \
  genepi_plugconf_get_int (((genepi_plugconf *) conf), prop ## _PROP, prop)

#define BPROP(conf, prop) \
 genepi_plugconf_get_bool (((genepi_plugconf *) conf), prop ## _PROP, prop)

			/* --------------- */

# define WIDTH(p,X) GENEPI_PLUGIN_FUNC(get_width) (p, X)

genepi_plugin_impl *
GENEPI_PLUGIN_FUNC (init) (genepi_plugconf *conf)
{
  if (init_counter == 0)
    {

      ccl_init ();

      use_logproc = !ccl_log_has_listener ();

      if (use_logproc)
	{
	  int dbgl = IPROP (conf, CCL_DEBUG_MAX_LEVEL);
	  ccl_debug_max_level (dbgl);

	  ccl_log_add_listener (s_log_listener, NULL);
	}

      sataf_init (IPROP (conf, MSA_OP_CACHE_SIZE), 
		  IPROP (conf, MAO_SHARING_CACHE_SIZE),
		  IPROP (conf, EA_UT_INIT_TABLE_SIZE),
		  IPROP (conf, EA_UT_FILL_DEGREE),
		  IPROP (conf, SA_UT_INIT_TABLE_SIZE), 
		  IPROP (conf, SA_UT_FILL_DEGREE),
		  IPROP (conf, MSA_UT_INIT_TABLE_SIZE), 
		  IPROP (conf, MSA_UT_FILL_DEGREE));
    }
  init_counter++;

  return (genepi_plugin_impl *) genepi_plugconf_duplicate (conf);
}

			/* --------------- */

#include <time.h>
static unsigned long nb_calls = 0;
static unsigned long total_time = 0;

void
GENEPI_PLUGIN_FUNC(terminate) (genepi_plugin_impl *plugin)
{
  genepi_plugconf *conf = (genepi_plugconf *) plugin;

  genepi_plugconf_del_reference (conf);

  init_counter--;
  if (init_counter == 0)
    {
      sataf_log_statistics (CCL_LOG_DEBUG);
      sataf_terminate ();
      if (use_logproc)
	ccl_log_remove_listener (s_log_listener);
      ccl_terminate ();
    }
}

			/* --------------- */

#if 0
int
GENEPI_PLUGIN_FUNC(autotest) (genepi_plugin_impl *plugin)
{
}
#endif
			/* --------------- */

genepi_solver_type 
GENEPI_PLUGIN_FUNC(get_solver_type) (genepi_plugin_impl *plugin)
{
  return GENEPI_N_SOLVER;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(add_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X)
{
  ccl_pre (X != NULL);
  X->refcount++;

  return X;
}

			/* --------------- */

void 
GENEPI_PLUGIN_FUNC(del_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X)
{
  ccl_pre (X != NULL);

  X->refcount--;
  if (X->refcount == 0)
    {
      sataf_msa_del_reference (X->msa);
      ccl_delete (X);
    }
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(top) (genepi_plugin_impl *plugin, int n)
{
  genepi_set_impl *result = s_new_set (n);
  result->msa = sataf_msa_one (2);

  return result;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(bot) (genepi_plugin_impl *plugin, int n)
{
  genepi_set_impl *result = s_new_set (n);
  result->msa = sataf_msa_zero (2);

  return result;
}

			/* --------------- */

static int
lca_op (genepi_comparator op)
{
  switch (op)
    {
    case GENEPI_EQ:
      return LCA_EQ;
    case GENEPI_LT:
      return LCA_LT;
    case GENEPI_GT:
      return LCA_GT;
    case GENEPI_LEQ:
      return LCA_LEQ;
    default:
      /* case GENEPI_GEQ: */
      return LCA_GEQ;
    };
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(linear_operation) (genepi_plugin_impl *plugin, 
				      const int *alpha, int alpha_size, 
				      genepi_comparator op, int c)
{
  sataf_mao *saut =
    prestaf_crt_linear_constraint_automaton (lca_op (op), alpha, alpha_size, 
					     c);
  genepi_set_impl *result = s_new_set (alpha_size);
  result->msa = sataf_msa_compute (saut);
  sataf_mao_del_reference (saut);

  return result;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_union) (genepi_plugin_impl *plugin, genepi_set_impl *X1,
			       genepi_set_impl *X2)
{
  genepi_set_impl *result = s_new_set (X1->width);

  ccl_pre (WIDTH (plugin, X1) == WIDTH (plugin, X2));

  result->msa = sataf_msa_or (X1->msa, X2->msa);

  return result;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_intersection) (genepi_plugin_impl *plugin, 
				      genepi_set_impl *X1, genepi_set_impl *X2)
{
  genepi_set_impl *result = s_new_set (X1->width);

  ccl_pre (WIDTH (plugin, X1) == WIDTH (plugin, X2));

  result->msa = sataf_msa_and (X1->msa, X2->msa);

  return result;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_complement) (genepi_plugin_impl *plugin,
				    genepi_set_impl *X)
{
  genepi_set_impl *result = s_new_set (X->width);

  ccl_pre (X != NULL);

  result->msa = sataf_msa_not (X->msa);

  return result;
}

			/* --------------- */

static sataf_msa *
s_compute_exists (sataf_msa *msa, uint32_t i, uint32_t n)
{
  uint32_t e = ((i << 16) | (0x0000FFFF & n)) + 1;
  sataf_msa *R = sataf_msa_cache_get (msa, NULL, NULL, (void *) (intptr_t) e);

  ccl_assert (i < 0xFFFF);
  ccl_assert (n < 0xFFFF);

  ccl_assert (e > 2);

  if (R == NULL)
    {
      sataf_mao *aux = prestaf_crt_quantifier_automaton (0, i, n, msa);
      R = sataf_msa_compute_with_transformer (aux, sataf_sa_exists_closure);
      sataf_mao_del_reference (aux);
      sataf_msa_cache_put (msa, NULL, NULL, (void *) (intptr_t) e, R);
    }

  return R;
}

			/* --------------- */

static sataf_msa *
s_compute_exists2 (sataf_msa *msa, uint32_t i, uint32_t n)
{
  sataf_mao *aux = prestaf_crt_quantifier_automaton2 (0, i, n, msa);
  sataf_msa *R = 
    sataf_msa_compute_with_transformer (aux, sataf_sa_exists_closure);
  sataf_mao_del_reference (aux);

  return R;
}

			/* --------------- */

static int
s_is_one_among_two01 (const int *sel, int selsize)
{
  int s = 0;
  int r = (selsize > 0) && (selsize % 2 == 0);

  while (r && selsize--)
    {
      if (s == *(sel++))
	s = (s + 1) % 2;
      else
	r = 0;
    }

  return r;
}

static int
s_is_one_among_two10 (const int *sel, int selsize)
{
  int s = 1;
  int r = (selsize > 0) && (selsize % 2 == 0);

  while (r && selsize--)
    {
      if (s == *(sel++))
	s = (s + 1) % 2;
      else
	r = 0;
    }

  return r;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(project) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
			     const int *selection, int size)
{
  int i, nb_T;
  genepi_set_impl *result;
  sataf_msa *msa;
  int among = BPROP (plugin, USE_AMONG);

  for (i = 0, nb_T = 0; i < X->width; i++)
    {
      if (selection[i])
	nb_T++;
    }

  if (nb_T == 0)
    return GENEPI_PLUGIN_FUNC(add_reference) (plugin, X);
  else if (nb_T == X->width)
    {
      if (sataf_msa_is_zero (X->msa))
	return GENEPI_PLUGIN_FUNC(bot) (plugin, 0);
      else
	return GENEPI_PLUGIN_FUNC(top) (plugin, 1);
    }

  result = s_new_set (X->width - nb_T);

  if (among && s_is_one_among_two01 (selection, size))
    {
      msa = s_compute_exists (X->msa, 1, 2);
    }
  else if (among && s_is_one_among_two10 (selection, size))
    {
      msa = s_compute_exists (X->msa, 0, 2);
    }
  else
    {
      int w = X->width;
      int *newsel = ccl_new_array (int, X->width);

      msa = sataf_msa_add_reference (X->msa);
      for (i = 0; i < X->width; i++)
	newsel[i] = selection[i];
      
      while (w > 0)
	{
	  sataf_msa *aux;
	  for (i = 0; i < w && !newsel[i]; i++)
	    /* do nothing */ ;
	  
	  if (i == w)
	    break;
	  
	  aux = s_compute_exists (msa, i, w);
	  sataf_msa_del_reference (msa);
	  msa = aux;

	  for (i++; i < w; i++)
	    newsel[i - 1] = newsel[i];
	  
	  w--;
	}
      ccl_delete (newsel);
    }

  result->msa = msa;
  
  if (0)
    {
      sataf_msa_log_info (CCL_LOG_DISPLAY, X->msa);
      ccl_log (CCL_LOG_DISPLAY, " - project -> ");
      sataf_msa_log_info (CCL_LOG_DISPLAY, result->msa);
    }
  
  return result;
}

genepi_set_impl *
GENEPI_PLUGIN_FUNC(invproject) (genepi_plugin_impl *plugin, genepi_set_impl *X,
				const int *selection, int size)
{
  genepi_set_impl *result = s_new_set (size);
  sataf_mao *aux =
    prestaf_crt_inv_project_automaton (X->msa, X->width, selection, size);

  ccl_pre (X->width <= size);

  result->msa = sataf_msa_compute (aux);
  sataf_mao_del_reference (aux);

  return result;
}

			/* --------------- */

#ifndef USE_GENERIC_APPLY
genepi_set_impl *
GENEPI_PLUGIN_FUNC(apply) (genepi_plugin_impl *plugin, genepi_set_impl *R, 
			   genepi_set_impl *A)
{
  genepi_set_impl *result;
  sataf_mao *aux;

  ccl_pre (R != NULL && A != NULL && R->width == 2 * A->width);

  result = s_new_set (A->width);
  aux = prestaf_crt_apply_automaton (R->msa, A->msa);
  result->msa = sataf_msa_compute_with_transformer (aux, 
						    sataf_sa_exists_closure);

  sataf_mao_del_reference (aux);

  return result;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(applyinv) (genepi_plugin_impl *plugin, genepi_set_impl *R, 
			      genepi_set_impl *A)
{
  genepi_set_impl *result;
  sataf_mao *aux;

  ccl_pre (R != NULL && A != NULL && R->width == 2 * A->width);

  result = s_new_set (A->width);
  aux = prestaf_crt_invapply_automaton (R->msa, A->msa);
  result->msa = sataf_msa_compute_with_transformer (aux, 
						    sataf_sa_exists_closure);
  sataf_mao_del_reference (aux);

  return result;
}
#endif
			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(compare) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
			     genepi_comparator op, genepi_set_impl *X2)
{
  ccl_pre (WIDTH (plugin, X1) == WIDTH (plugin, X2));

  if (op == GENEPI_EQ)
    return X1->msa == X2->msa;
  else if (op == GENEPI_LEQ)
    return sataf_msa_is_included_in (X1->msa, X2->msa);
  else if (op == GENEPI_GEQ)
    return sataf_msa_is_included_in (X2->msa, X1->msa);
  else if (op == GENEPI_LT)
    return X1->msa != X2->msa && sataf_msa_is_included_in (X1->msa, X2->msa);
  else
    {
      ccl_assert (op == GENEPI_GT);
      return X1->msa != X2->msa
	&& sataf_msa_is_included_in (X2->msa, X1->msa);
    }
}

			/* --------------- */


int 
GENEPI_PLUGIN_FUNC(is_empty) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  ccl_pre (X != NULL);

  return sataf_msa_is_zero (X->msa);
}

			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(is_full) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  ccl_pre (X != NULL);

  return sataf_msa_is_one (X->msa);
}

			/* --------------- */

#if 0
int 
GENEPI_PLUGIN_FUNC(is_finite) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  ccl_warning (" 'genepi_set_is_finite' not yet implemented \n");
  return 0;
}
#endif
			/* --------------- */

#if 0
int
GENEPI_PLUGIN_FUNC(depend_on) (genepi_plugin_impl *plugin, enepi_set_impl *X, const int *sel, int selsize)
{
}
#endif

			/* --------------- */

void 
GENEPI_PLUGIN_FUNC(get_solutions) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X, int ***psolutions, 
				   int *psize, int max)
{
  ccl_pre (X != NULL);

  *psolutions = s_get_solutions (X->msa, X->width, psize, max);
}


			/* --------------- */

static int
s_count_node (armoise_tree *tF)
{
  int result;

  if (tF == NULL) 
    result = 0;
  else 
    {
      result = 1;
      for (tF= tF->child; tF; tF = tF->next)
	result += s_count_node (tF);
    }

  return result;
}

			/* --------------- */

static uint32_t succ[] = {
  1, 1,
  0, 2,
  3, 3,
  7, 4,
  5, 3,
  2, 6,
  5, 5,
  3, 1
};

static uint8_t final[] = {
  1, 1, 0, 0, 
  0, 0, 0, 0
};

static void
jj (genepi_plugin_impl *impl)
{
  sataf_mao *mao = sataf_mao_create_from_arrays (8, 2, 1, final, succ);
  sataf_msa *msa = sataf_msa_compute (mao);
  armoise_tree *tF = 
    btf_msa_to_formula (2, msa, NULL, BPROP (impl, ENUMERATE_PREFIXES));

  ccl_assert (tF != NULL);

  ccl_try (armoise_interp_exception)
    {
      if (BPROP (impl, CHECK_GENERATED_FORMULA))
	{
	  ccl_tree *F = armoise_interp_predicate (NULL, tF);
	  armoise_predicate_log_tree (CCL_LOG_DISPLAY, F);
	  ccl_tree_del_reference (F);
	}

      ccl_display (";\n");
      ccl_display ("// Automata size : ");
      sataf_msa_log_info (CCL_LOG_DISPLAY, msa);
      ccl_display ("\n"
		   "// Formula size  : %d nodes\n", 
		   s_count_node (tF));
      ccl_log_pop_redirection (CCL_LOG_DISPLAY);
      fflush (stdout);
    }
  ccl_no_catch;
  ccl_parse_tree_delete_tree (tF);

}

			/* --------------- */

void 
GENEPI_PLUGIN_FUNC(display_all_solutions) (genepi_plugin_impl *plugin, 
					   genepi_set_impl *X, FILE *output,
					   const char * const *varnames)
{
  uint32_t t0 = ccl_time_get_cpu_time_in_milliseconds ();  
  armoise_tree *tF;
  
  ccl_pre (X != NULL);
  ccl_pre (output != NULL);

  CCL_DEBUG_START_TIMED_BLOCK (("formula synthesis\n"));
  tF = btf_msa_to_formula (X->width, X->msa, varnames, 
			   BPROP (plugin, ENUMERATE_PREFIXES));
  t0 = ccl_time_get_cpu_time_in_milliseconds () - t0;
  CCL_DEBUG_END_TIMED_BLOCK ();

  ccl_assert (tF != NULL);

  ccl_try (armoise_interp_exception)
    {
      ccl_log_push_redirection (CCL_LOG_DISPLAY, s_log_to_file, output);
      if (BPROP (plugin, CHECK_GENERATED_FORMULA))
	{
	  ccl_tree *F = armoise_interp_predicate (NULL, tF);

	  armoise_predicate_log_tree (CCL_LOG_DISPLAY, F);
	  ccl_tree_del_reference (F);
	  ccl_display (";\n");
	}
      else
	{
	  armoise_tree_log (CCL_LOG_DISPLAY, tF);
	}
      ccl_display ("// Automata size             : ");
      sataf_msa_log_info (CCL_LOG_DISPLAY, X->msa);
      ccl_display ("\n"
		   "// Formula size              : %d nodes\n", 
		   s_count_node (tF));
      ccl_display ("// Time to synthesis formula : %d ms\n", 
		   t0);
      ccl_log_pop_redirection (CCL_LOG_DISPLAY);
      fflush (output);
    }
  ccl_no_catch;
  ccl_parse_tree_delete_tree (tF);
}

			/* --------------- */

void 
GENEPI_PLUGIN_FUNC(display_data_structure) (genepi_plugin_impl *plugin, 
					    genepi_set_impl *X, FILE *output)
{
  ccl_pre (X != NULL);
  ccl_pre (output != NULL);

  ccl_log_push_redirection (CCL_LOG_DISPLAY, s_log_to_file, output);
  sataf_msa_display_as_dot (X->msa, CCL_LOG_DISPLAY, NULL, 
			    BPROP (plugin, DISPLAY_MSA_INFO));
  ccl_log_pop_redirection (CCL_LOG_DISPLAY);
}

			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(get_width) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  ccl_pre (X != NULL);

  return X->width;
}

			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(get_data_structure_size) (genepi_plugin_impl *plugin, 
					     genepi_set_impl *X)
{
  sataf_msa_info info;

  ccl_pre (X != NULL);

  sataf_msa_get_info (X->msa, &info);

  return info.nb_states;
}


			/* --------------- */

int
GENEPI_PLUGIN_FUNC(is_solution) (genepi_plugin_impl *plugin, genepi_set_impl *X,
				 const int *x, int x_size, int xden)
{
  int i;
  int result = 1;

  if (sataf_msa_is_zero (X->msa))
    result = 0;
  else if (sataf_msa_is_one (X->msa))
    {
      if (xden != 1)
	{
	  for(i = 0; result && i < x_size; i++)
	    result =  (x[i] % xden == 0);
	}
    }
  else
    {
      int *xaux = ccl_new_array (int, x_size);

      for(i = 0; result && i < x_size; i++)
	{
	  result = (x[i] % xden == 0);
	  xaux[i] = x[i] / xden;
	}
      
      if (result)
	{
	  int done = 0;
	  sataf_msa *s = sataf_msa_add_reference (X->msa); 

	  while (! done)
	    {
	      int all_are_null = 1;

	      for (i = 0; i < x_size && ! done; i++)
		{
		  int b = (xaux[i] & 0x1);
		  sataf_msa *succ = sataf_msa_succ (s,b);
		  sataf_msa_del_reference (s);
		  s = succ;
		  all_are_null = all_are_null && xaux[i] == 0;
		  done = sataf_msa_is_zero_or_one (s);
		  xaux[i] >>= 1;
		}

	      if (done)
		result = sataf_msa_is_one (s);
	      else if (all_are_null)
		{
		  done = 1;
		  result = sataf_msa_is_final (s);
		}
	    }
	  sataf_msa_del_reference (s);
	}
      ccl_delete (xaux);
    }

  return result?1:0;
}

			/* --------------- */

static void
s_delete_msa (void *p)
{
  sataf_msa_del_reference ((sataf_msa *) p);
}

			/* --------------- */

static ccl_list *
s_compute_accepting_word (sataf_msa *msa)
{
  int i;
  sataf_msa *accepting_state = NULL;
  ccl_list *l = ccl_list_create ();
  ccl_hash *done =
    ccl_hash_create (NULL, NULL, NULL,
 		     (ccl_delete_proc *) sataf_msa_del_reference);
  ccl_hash *preds = 
    ccl_hash_create (NULL, NULL, NULL, NULL);
		     
  ccl_list_add (l, msa);
  ccl_hash_find (done, msa);
  ccl_hash_insert (done, sataf_msa_add_reference(msa));

  while (! ccl_list_is_empty (l) && accepting_state == NULL)
    {
      sataf_msa *s = ccl_list_take_first (l);

      if (sataf_msa_is_final (s))
	accepting_state = s;
      else
	{
	  int b;

	  for (b = 0; b < 2; b++)
	    {
	      sataf_msa *succ = sataf_msa_succ (s, b);

	      if (ccl_hash_find (done, succ))
		sataf_msa_del_reference (succ);
	      else
		{
		  sataf_msa *pred = s;

		  ccl_list_add (l, succ);
		  ccl_hash_insert (done, succ);
		  ccl_assert (! ccl_hash_find (preds, succ));
		  ccl_hash_find (preds, succ);
		  if (b)
		    pred  = CCL_BITPTR (sataf_msa *, pred);
		  ccl_hash_insert (preds, pred);
		}
	    }
	}
    }
  ccl_list_clear (l, NULL);

  ccl_assert (accepting_state != NULL);

  while (accepting_state != msa)
    {
      int b = 0;
      sataf_msa *p;

      ccl_assert (ccl_hash_find (preds, accepting_state));
      ccl_hash_find (preds, accepting_state);
      p = ccl_hash_get (preds);
      if (CCL_PTRHASBIT (p))
	{
	  b = 1;
	  p = CCL_BITPTR2PTR (sataf_msa *, p);
	}

      accepting_state = p;
      ccl_list_add (l, (void *) (intptr_t) b); 
    }

  ccl_hash_delete (preds);
  ccl_hash_delete (done);

  return l;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(get_one_solution) (genepi_plugin_impl *plugin, 
				      genepi_set_impl *X, int *x, int x_size)
{
  int varindex;
  ccl_list *aw;

  if (sataf_msa_is_zero (X->msa))
    return 0;

  ccl_memzero (x, sizeof (int) * x_size);

  if (sataf_msa_is_one (X->msa))
    return 1;

  aw = s_compute_accepting_word (X->msa);

  while ((ccl_list_get_size (aw) % x_size) != 0)
    ccl_list_put_first (aw, (void *) 0);

  varindex = x_size - 1;
  while (! ccl_list_is_empty (aw))
    {
      int b = (intptr_t) ccl_list_take_first (aw);
      x[varindex] = (x[varindex]<<1)+b;
      if (varindex == 0)
	varindex = x_size - 1;
      else
	varindex--;
    }
  ccl_list_delete (aw);

  return 1;
}

			/* --------------- */

unsigned int
GENEPI_PLUGIN_FUNC(cache_hash) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  ccl_pre (X != NULL);

  return 791 * (uintptr_t) X->msa + 1023 * X->width;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(cache_equal) (genepi_plugin_impl *plugin, 
				 genepi_set_impl *X1, genepi_set_impl *X2)
{
  return X1->msa == X2->msa && X1->width == X2->width;
}

			/* --------------- */

static int
s_write_ea (FILE *output, sataf_ea *ea, int *perr)
{
  int i;
  int result = genepi_io_write_uint32 (output, ea->nb_local_states, perr);
  result += genepi_io_write_uint32 (output, ea->nb_exit_states, perr);
  result += genepi_io_write_buffer (output, ea->is_final, ea->nb_local_states,
				    perr);
  for (i = 0; i < 2*ea->nb_local_states && !*perr; i++)
    result += genepi_io_write_uint32 (output, ea->successor[i], perr);

  if(0) printf("write ea %d bytes\n", result);

  return result;
}

			/* --------------- */

static int
s_write_components (FILE *output, sataf_sa *SA, sataf_ea **ea_comps,
		    sataf_sa **sa_comps, ccl_hash *ea_table,
		    ccl_hash *sa_table, int *perr) 
{  
  int result = 0;
  int sa_id;
  int ea_id;
  int i;

  if (*perr < 0)
    return 0;

  if (ccl_hash_find (sa_table, SA))
    return 0;

  if (SA->automaton->nb_exit_states > 0)
    {
      for (i = 0; i < SA->automaton->nb_exit_states; i++)
	result += s_write_components (output, SA->bind[i]->A, ea_comps, 
				      sa_comps, ea_table, sa_table, perr);
    }
  
  if (*perr < 0)
    return 0;

  if (ccl_hash_find (ea_table, SA->automaton))
    {
      result += genepi_io_write_uint32 (output, 0, perr);
      ea_id = (intptr_t) ccl_hash_get (ea_table);
      result += genepi_io_write_uint32 (output, ea_id, perr);
    }
  else
    {
      result += genepi_io_write_uint32 (output, 1, perr);
      result += s_write_ea (output, SA->automaton, perr);

      if (*perr < 0)
	return 0;

      ea_id = ccl_hash_get_size (ea_table);
      ccl_hash_insert (ea_table, (void *) (intptr_t) ea_id);
      ea_comps[ea_id] = SA->automaton;
    }

  for (i = 0; i < SA->automaton->nb_exit_states && !*perr; i++)
    {
      ccl_assert (ccl_hash_find (sa_table, SA->bind[i]->A));

      ccl_hash_find (sa_table, SA->bind[i]->A);
      sa_id = (intptr_t) ccl_hash_get (sa_table);
      result += genepi_io_write_uint32 (output, sa_id, perr);
      result += genepi_io_write_uint32 (output, SA->bind[i]->initial, perr);
    }

  if (*perr < 0) 
    result = 0;
  else
    {
      sa_id = ccl_hash_get_size (sa_table);
      ccl_hash_find (sa_table, SA);
      ccl_hash_insert (sa_table, (void *) (intptr_t) sa_id);
      sa_comps[sa_id] = SA;
      if(0) printf("write sa %d bytes\n", result);
    }

  return result;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC (write_set) (genepi_plugin_impl *plugin, FILE *output, 
				genepi_set_impl *set)
{
  int result = 0;
  int error = 0;
  sataf_ea **ea_comps; 
  sataf_sa **sa_comps;
  ccl_hash *ea_table = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_hash *sa_table = ccl_hash_create (NULL, NULL, NULL, NULL);
  sataf_msa_info info;

  /*!
   * First we collect all EAs and SAs
   */
  sataf_msa_get_info (set->msa, &info);
  ea_comps = ccl_new_array (sataf_ea *, info.nb_exit_automata);
  sa_comps = ccl_new_array (sataf_sa *, info.nb_shared_automata);
  result += genepi_io_write_uint32 (output, info.nb_exit_automata, &error);
  result += genepi_io_write_uint32 (output, info.nb_shared_automata, &error);

  result += s_write_components (output, set->msa->A, ea_comps, sa_comps, 
				ea_table, sa_table, &error);


  ccl_assert (ccl_hash_find (sa_table, set->msa->A));
  
  /*
   * Now all the components are serialized and an index is given by sa_table
   * for each one. We serialize the msa corresponding to 'set' by serializing
   * the index of its shared automaton and its initial state.
   */
  if (!error && ccl_hash_find (sa_table, set->msa->A))
    {
      int said = (intptr_t) ccl_hash_get(sa_table);
      result += genepi_io_write_uint32 (output, set->width, &error);
      result += genepi_io_write_uint32 (output, said, &error);
      result += genepi_io_write_uint32 (output, set->msa->initial, &error);
    }

  ccl_delete (ea_comps);
  ccl_delete (sa_comps);
  ccl_hash_delete (ea_table);
  ccl_hash_delete (sa_table);

  if (error)
    result = error;

  return result;
}

			/* --------------- */

static int
s_read_ea (FILE *input, sataf_ea **pea, int *perr)
{
  uint32_t i;
  uint32_t nb_states;
  uint32_t nb_exits;
  sataf_ea *ea;
  int result = genepi_io_read_uint32 (input, &nb_states, perr);

  result += genepi_io_read_uint32 (input, &nb_exits, perr);
  if (*perr)
    return 0;

  ea = sataf_ea_create (nb_states, nb_exits, 2);
  result += genepi_io_read_buffer (input, ea->is_final, nb_states, perr);
  
  for (i = 0; i < 2*nb_states && *perr == 0; i++)
    result += genepi_io_read_uint32 (input, &ea->successor[i], perr);

  if (*perr)
    {
      sataf_ea_del_reference (ea);
      return 0;
    }
  *pea = sataf_ea_find_or_add (ea);
  sataf_ea_del_reference (ea);

  if(0) printf("read ea %d bytes\n", result);

  return result;
}

			/* --------------- */

static int
s_read_sa (FILE *input, sataf_sa **psa, int *perr,
	   uint32_t *p_i_ea, uint32_t nb_ea, sataf_ea **ea_comps, 
	   uint32_t nb_sa, sataf_sa **sa_comps)
{
  int i;
  uint32_t read_ea;
  sataf_ea *ea = NULL;
  sataf_sa **bind_sa = NULL;
  uint32_t *bind_init = NULL;
  int result = genepi_io_read_uint32 (input, &read_ea, perr);

  if (*perr)
    return 0;

  if (read_ea)
    {
      result += s_read_ea (input, &ea, perr);
      ea_comps[*p_i_ea] = sataf_ea_add_reference (ea);
      (*p_i_ea)++;
    }
  else
    { 
      uint32_t ea_id;

      result += genepi_io_read_uint32 (input, &ea_id, perr);
      if (ea_id >= nb_ea || ea_comps[ea_id] == NULL)
	*perr = GENEPI_R_ERR_DATA_ERROR;
      else
	ea = sataf_ea_add_reference (ea_comps[ea_id]);
    }

  if( !*perr && ea->nb_exit_states > 0)
    {
      bind_sa = ccl_new_array (sataf_sa *, ea->nb_exit_states);
      bind_init = ccl_new_array (uint32_t, ea->nb_exit_states);
      for (i = 0; i < ea->nb_exit_states && !*perr; i++)
	{
	  uint32_t sa_id, init;

	  result += genepi_io_read_uint32 (input, &sa_id, perr);
	  result += genepi_io_read_uint32 (input, &init, perr);
	  if (!perr && (sa_id >= nb_sa || sa_comps[sa_id] == NULL))
	    *perr = GENEPI_R_ERR_DATA_ERROR;
	  else
	    {
	      bind_sa[i] = sataf_sa_add_reference (sa_comps[sa_id]);
	      bind_init[i] = init;
	    }
	}
    }

  if (*perr)
    result = 0;
  else
    {
      *psa = sataf_sa_find_or_add (ea, bind_sa, bind_init);
    }

  if (bind_sa != NULL)
    {
      for (i = 0; i < ea->nb_exit_states; i++)
	ccl_zdelete (sataf_sa_del_reference, bind_sa[i]);
      ccl_delete (bind_sa);
    }

  ccl_zdelete (ccl_delete, bind_init);
  ccl_zdelete (sataf_ea_del_reference, ea);

  if(0) printf("read sa %d bytes\n", result);

  return result;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC (read_set) (genepi_plugin_impl *plugin, FILE *input, 
			       genepi_set_impl **pset)
{
  int error = 0;
  uint32_t nb_ea, i_ea;
  uint32_t nb_sa, i_sa;
  uint32_t width, initial, sa_id;
  sataf_ea **ea_comps; 
  sataf_sa **sa_comps;
  int result = genepi_io_read_uint32 (input, &nb_ea, &error);
  
  result += genepi_io_read_uint32 (input, &nb_sa, &error);

  if (error)
    return error;

  i_ea = 0;
  ea_comps = ccl_new_array (sataf_ea *, nb_ea);
  i_sa = 0;
  sa_comps = ccl_new_array (sataf_sa *, nb_sa);

  while (i_sa != nb_sa && !error)
    {
      result += s_read_sa (input, &sa_comps[i_sa], &error, &i_ea, nb_ea,
			   ea_comps, nb_sa, sa_comps);
      i_sa++;
    }
  result += genepi_io_read_uint32 (input, &width, &error);
  result += genepi_io_read_uint32 (input, &sa_id, &error);
  result += genepi_io_read_uint32 (input, &initial, &error);
  
  if (error)
    result = error;
  else
    {
      *pset = s_new_set (width);
      (*pset)->msa = sataf_msa_find_or_add (sa_comps[sa_id], initial);
      ccl_assert (sa_id == nb_sa - 1);
    }

  while (nb_ea--)
    ccl_zdelete (sataf_ea_del_reference, ea_comps[nb_ea]);
  while (nb_sa--)
    ccl_zdelete (sataf_sa_del_reference, sa_comps[nb_sa]);

  ccl_delete (ea_comps);
  ccl_delete (sa_comps);
  
  return result;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC (preorder) (genepi_plugin_impl *plugin, genepi_set_impl *X1,
			       genepi_set_impl *X2)
{
  return X1->msa <= X2->msa;
}

			/* --------------- */

static void
s_log_listener (ccl_log_type type, const char *msg, void *data)
{
  if (type == CCL_LOG_DISPLAY)
    fprintf (stdout, "%s", msg);
  else
    fprintf (stderr, "%s", msg);
}

			/* --------------- */

static genepi_set_impl *
s_new_set (int width)
{
  genepi_set_impl *result = ccl_new (genepi_set_impl);

  ccl_pre (width >= 0);

  result->refcount = 1;
  result->width = width;
  result->msa = NULL;

  return result;
}

			/* --------------- */

typedef struct display_stack_st
{
  sataf_msa *rel;
  int letter;
} display_stack;

typedef struct prefix_tree_st prefix_tree;
struct prefix_tree_st
{
  uint32_t val;
  prefix_tree *right;
  prefix_tree *down;
};

			/* --------------- */

static int
s_is_in_stack (display_stack *S, int top, sataf_msa *node)
{
  while (top >= 0)
    if (S[top--].rel == node)
      return 1;
  return 0;
}

			/* --------------- */

static prefix_tree *
s_add_assignment (int width, uint32_t * values, prefix_tree * T)
{
  int i;
  prefix_tree *result = T;
  prefix_tree **pT = &result;

  for (i = 0; i < width; i++, values++)
    {
      while (*pT != NULL && (*pT)->val < *values)
	pT = &((*pT)->down);

      if (*pT == NULL || (*pT)->val > *values)
	{
	  prefix_tree *t = ccl_new (prefix_tree);

	  t->val = *values;
	  t->right = NULL;
	  t->down = *pT;

	  *pT = t;
	}
      pT = &((*pT)->right);
    }

  return result;
}

			/* --------------- */

struct dlist
{
  int val;
  struct dlist *next;
};

			/* --------------- */

static void
s_assign_prefix_tree_rec (int var, int width, prefix_tree * T,
			  struct dlist *dl, ccl_list *solutions, int max)
{
  if (var == width)
    {
      int i = width - 1;
      int *sol = ccl_new_array (int, width);

      while (dl != NULL)
	{
	  sol[i--] = dl->val;
	  dl = dl->next;
	}
      ccl_list_add (solutions, sol);
    }
  else
    {
      struct dlist data;

      data.next = dl;

      while (T != NULL)
	{
	  data.val = T->val;
	  s_assign_prefix_tree_rec (var + 1, width, T->right, &data,
				    solutions, max);
	  if (ccl_list_get_size (solutions) == max)
	    return;
	  T = T->down;
	}
    }
}

			/* --------------- */

static int **
s_get_assignments (int width, int max, prefix_tree * T, int *psize)
{
  int i;
  ccl_pair *p;
  int **result;
  ccl_list *solutions = ccl_list_create ();

  s_assign_prefix_tree_rec (0, width, T, NULL, solutions, max);
  *psize = ccl_list_get_size (solutions);
  result = ccl_new_array (int *, *psize);
  for (i = 0, p = FIRST (solutions); i < *psize; i++, p = CDR (p))
    result[i] = CAR (p);
  ccl_list_delete (solutions);

  return result;
}

			/* --------------- */

static int
s_compute_range_tree (int width, sataf_msa *R, prefix_tree ** pT)
{
  int result = 1;
  int nb_bits = 0;
  int var = 0;
  int nb_vars = width;
  int stack_size = 33 * nb_vars;
  uint32_t *values = ccl_new_array (uint32_t, nb_vars);
  int top = 0;
  display_stack *stack = ccl_new_array (display_stack, stack_size);

  stack[top].rel = sataf_msa_add_reference (R);
  stack[top].letter = 0;

  while (result && top >= 0)
    {
      sataf_msa *node = stack[top].rel;
      uint32_t letter = stack[top].letter;

      if (letter <= 1)
	{
	  sataf_msa *succ;

	  if (sataf_msa_is_final (node) && letter == 0)
	    *pT = s_add_assignment (width, values, *pT);

	  succ = sataf_msa_succ (node, letter);

	  if (s_is_in_stack (stack, top, succ))
	    {
	      if (succ != node)
		result = 0;
	      else
		{
		  sataf_msa *succ2;

		  ccl_assert (letter == 0);

		  succ2 = sataf_msa_succ (node, 1);

		  if (s_is_in_stack (stack, top, succ2))
		    {
		      if ((succ2 != node || sataf_msa_is_final (node)))
			result = 0;
		      else
			{
			  ccl_assert (sataf_msa_is_zero (node));
			  sataf_msa_del_reference (node);
			  top--;
			  var--;
			  if (var < 0)
			    {
			      var = nb_vars - 1;
			      nb_bits--;
			    }
			}
		    }
		  else if (sataf_msa_is_zero (succ2))
		    {
		      sataf_msa_del_reference (node);
		      top--;
		      var--;
		      if (var < 0)
			{
			  var = nb_vars - 1;
			  nb_bits--;
			}
		    }
		  else
		    {
		      result = 0;
		    }

		  sataf_msa_del_reference (succ2);
		}
	    }
	  else			/* succ isn't in stack */
	    {
	      if (letter == 1)
		values[var] += (1 << nb_bits);

	      stack[top].letter++;

	      top++;

	      stack[top].rel = sataf_msa_add_reference (succ);
	      stack[top].letter = 0;
	      var++;

	      if (var == nb_vars)
		{
		  var = 0;
		  nb_bits++;
		}
	    }

	  sataf_msa_del_reference (succ);
	}
      else			/* letter > 1 */
	{
	  values[var] -= (1 << nb_bits);
	  sataf_msa_del_reference (node);
	  top--;
	  var--;
	  if (var < 0)
	    {
	      var = nb_vars - 1;
	      nb_bits--;
	    }
	}
    }

  while (top >= 0)
    sataf_msa_del_reference (stack[top--].rel);

  ccl_assert (top == -1);

  ccl_delete (values);
  ccl_delete (stack);

  return result;
}

			/* --------------- */

static void
s_delete_tree (prefix_tree * T)
{
  if (T == NULL)
    return;
  s_delete_tree (T->right);
  s_delete_tree (T->down);
  ccl_delete (T);
}

			/* --------------- */
static int **
s_get_solutions (sataf_msa *msa, int n, int *psize, int max)
{
  prefix_tree *T = NULL;
  int **result = NULL;

  s_compute_range_tree (n, msa, &T);
  result = s_get_assignments (n, max, T, psize);

  if (T != NULL)
    s_delete_tree (T);

  return result;
}

			/* --------------- */

static void
s_log_to_file (ccl_log_type type, const char *msg, void *data)
{
  fprintf ((FILE *) data, "%s", msg);
}

			/* --------------- */

