/*
 * mona-plugin.c -- Implementation of GENEPI using MONA
 * 
 * This file is a part of the MONA-based plugin for GENEPI. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#define USE_MALLOC
#include <mona/dfa.h>
#include <mona/mem.h>
#include <stdlib.h>
#include <assert.h>
#include <genepi/genepi-plugin.h>

			/* --------------- */

struct genepi_plugin_impl_st
{
  genepi_set_impl *set_plus;	/* x[0] + x[1] = x[2] */
  genepi_set_impl *set_eq;	/* x[0] = x[1] */
};
			/* --------------- */

struct genepi_set_impl_st
{
  DFA *a;
  int number_reference;
  int dim;
};

			/* --------------- */

static int mona_init_count = 0;

			/* --------------- */

genepi_plugin_impl *
GENEPI_PLUGIN_FUNC(init) (genepi_plugconf *conf)
{
  int indices[3];
  genepi_plugin_impl *result;

  if (mona_init_count == 0)
    bdd_init ();
  mona_init_count++;

  result = malloc (sizeof *result);

  /* Construction de set_plus */
  indices[0] = 0;
  indices[1] = 1;
  indices[2] = 2;
  dfaSetup (4, 3, indices);
  dfaAllocExceptions (0);
  dfaStoreState (1);
  dfaAllocExceptions (4);
  dfaStoreException (1, "000");
  dfaStoreException (1, "011");
  dfaStoreException (1, "101");
  dfaStoreException (3, "110");
  dfaStoreState (2);
  dfaAllocExceptions (0);
  dfaStoreState (2);
  dfaAllocExceptions (4);
  dfaStoreException (3, "111");
  dfaStoreException (3, "100");
  dfaStoreException (3, "010");
  dfaStoreException (1, "001");
  dfaStoreState (2);

  result->set_plus = malloc (sizeof (genepi_set_impl));
  result->set_plus->a = dfaBuild ("0+--");
  result->set_plus->number_reference = 1;
  result->set_plus->dim = 3;

  /* Construction de DFAeq */
  indices[0] = 0;
  indices[1] = 1;
  dfaSetup (3, 2, indices);
  dfaAllocExceptions (0);
  dfaStoreState (1);
  dfaAllocExceptions (2);
  dfaStoreException (1, "00");
  dfaStoreException (1, "11");
  dfaStoreState (2);
  dfaAllocExceptions (0);
  dfaStoreState (2);

  result->set_eq = malloc (sizeof (genepi_set_impl));
  result->set_eq->a = dfaBuild ("0+-");
  result->set_eq->number_reference = 1;
  result->set_eq->dim = 2;

  return result;
}

			/* --------------- */

void
GENEPI_PLUGIN_FUNC(terminate) (genepi_plugin_impl *plugin)
{
  mona_init_count--;

  GENEPI_PLUGIN_FUNC(del_reference) (plugin, plugin->set_plus);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, plugin->set_eq);
  free (plugin);
}

			/* --------------- */


genepi_solver_type
GENEPI_PLUGIN_FUNC(get_solver_type) (genepi_plugin_impl *plugin)
{
  return GENEPI_N_SOLVER;
}


			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(add_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl * X)
{
  X->number_reference++;
  return X;
}

			/* --------------- */

void
GENEPI_PLUGIN_FUNC(del_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl * X)
{
  X->number_reference--;
  if (X->number_reference == 0)
    {
      dfaFree (X->a);
      free (X);
    }
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(top) (genepi_plugin_impl *plugin, int n)
{
  genepi_set_impl *R = malloc (sizeof (genepi_set_impl));

  R->a = dfaTrue ();
  R->number_reference = 1;
  R->dim = n;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(bot) (genepi_plugin_impl *plugin, int n)
{
  genepi_set_impl *R = malloc (sizeof (genepi_set_impl));

  R->a = dfaFalse ();
  R->number_reference = 1;
  R->dim = n;

  return R;
}

			/* --------------- */


/*  return the set that defines the predicate a.p_0=p_1 where a>0 */
static genepi_set_impl *
s_linear_multiply_simple (genepi_plugin_impl *plugin, int a)
{
  int a1, a2;
  int proj[4];
  genepi_set_impl *X[6];

  if (a <= 0)
    {
      printf ("Internal error in linear equality");
      exit (0);
    }
  if (a == 1)
    {
      GENEPI_PLUGIN_FUNC(add_reference) (plugin, plugin->set_eq);
      return plugin->set_eq;
    }

  if ((a % 2) == 0)
    {
      a1 = a / 2;
      a2 = a / 2;
    }
  else
    {
      a1 = a - 1;
      a2 = 1;
    }

  X[0] = s_linear_multiply_simple (plugin, a1);
  proj[0] = 0;
  proj[1] = 0;
  proj[2] = 1;
  proj[3] = 1;
  X[1] = GENEPI_PLUGIN_FUNC(invproject) (plugin, X[0], proj, 4);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, X[0]);
  X[0] = s_linear_multiply_simple (plugin, a2);
  proj[0] = 0;
  proj[1] = 1;
  proj[2] = 0;
  proj[3] = 1;
  X[2] = GENEPI_PLUGIN_FUNC(invproject) (plugin, X[0], proj, 4);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, X[0]);
  X[3] = GENEPI_PLUGIN_FUNC(set_intersection) (plugin, X[1], X[2]);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, X[1]);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, X[2]);
  proj[0] = 1;
  proj[1] = 0;
  proj[2] = 0;
  proj[3] = 0;
  X[4] = GENEPI_PLUGIN_FUNC(invproject) (plugin, plugin->set_plus, proj, 4);
  X[5] = GENEPI_PLUGIN_FUNC(set_intersection) (plugin, X[3], X[4]);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, X[3]);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, X[4]);
  proj[0] = 0;
  proj[1] = 1;
  proj[2] = 1;
  proj[3] = 0;
  X[0] = GENEPI_PLUGIN_FUNC(project) (plugin, X[5], proj, 4);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, X[5]);

  return X[0];
}

			/* --------------- */

/* We return the set that defines the predicate a.p_0+p_1=p_2 where a>0 */
static genepi_set_impl *
s_linear_multiply (genepi_plugin_impl *plugin, int a)
{
  int proj[4];
  genepi_set_impl *X;
  genepi_set_impl *X0;
  genepi_set_impl *X1;
  genepi_set_impl *X2;

  if (a <= 0)
    {
      printf ("Internal error in linear equality");
      exit (0);
    }
  X = s_linear_multiply_simple (plugin, a);
  proj[0] = 0;
  proj[1] = 0;
  proj[2] = 1;
  proj[3] = 1;
  X0 = GENEPI_PLUGIN_FUNC(invproject) (plugin, X, proj, 4);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, X);
  proj[0] = 1;
  proj[1] = 0;
  proj[2] = 0;
  proj[3] = 0;
  X1 = GENEPI_PLUGIN_FUNC(invproject) (plugin, plugin->set_plus, proj, 4);
  X2 = GENEPI_PLUGIN_FUNC(set_intersection) (plugin, X0, X1);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, X0);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, X1);
  proj[0] = 0;
  proj[1] = 1;
  proj[2] = 0;
  proj[3] = 0;
  X = GENEPI_PLUGIN_FUNC(project) (plugin, X2, proj, 4);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, X2);

  return X;
}

			/* --------------- */


/* returns the 1-dim set representing x[0]=c where c>=0 */
static genepi_set_impl *
s_constante (int c)
{
  genepi_set_impl *R;

  if (c < 0)
    {
      printf ("internal error in constante");
      exit (0);
    }
  R = malloc (sizeof (genepi_set_impl));
  R->a = dfaPresbConst (0, c);
  R->number_reference = 1;
  R->dim = 1;
  return R;
}



static genepi_set_impl *
s_linear_equality (genepi_plugin_impl *plugin, const int *alpha, int size, 
		   int c)
{
  /*  We first compute the (n+2)-dim set X that defines  */
  /*  a[0].x[0]+...a[n-1].x[n-1]+x[n]=x[n+1] */
  /*  where a[0]=...=a[n-1]=0 and n=size */
  /* FILE *f;  */
  /* genepi_set_impl_display_data_structure(plugin->set_plus,f); */
  /* genepi_set_impl_display_data_structure(plugin->set_eq,f); */
  genepi_set_impl *X;
  int *proj = calloc (sizeof (int), (size + 3));
  int i;
  int k;

  for (i = 0; i < size; i++)
    proj[i] = 1;
  proj[size] = 0;
  proj[size + 1] = 0;
  X = GENEPI_PLUGIN_FUNC(invproject) (plugin, plugin->set_eq, proj, size + 2);


  for (k = 0; k < size; k++)
    {
      /*  We replace in X the coef a[k] by a[k]+alpha[k] */
      /* genepi_set_impl_display_data_structure(X,f); */
      if (alpha[k] > 0)
	{
	  genepi_set_impl *Y0;
	  genepi_set_impl *Y1;
	  genepi_set_impl *Z;
	  genepi_set_impl *Y2;
	  int i;
	  for (i = 0; i < size + 3; i++)
	    proj[i] = 0;
	  proj[size] = 1;
	  Y0 = GENEPI_PLUGIN_FUNC(invproject) (plugin, X, proj, size + 3);
	  GENEPI_PLUGIN_FUNC(del_reference) (plugin, X);

	  for (i = 0; i < size + 3; i++)
	    proj[i] = 1;
	  proj[k] = 0;
	  proj[size] = 0;
	  proj[size + 1] = 0;
	  Z = s_linear_multiply (plugin, alpha[k]);
	  Y1 = GENEPI_PLUGIN_FUNC(invproject) (plugin, Z, proj, size + 3);
	  GENEPI_PLUGIN_FUNC(del_reference) (plugin, Z);
	  Y2 = GENEPI_PLUGIN_FUNC(set_intersection) (plugin, Y0, Y1);
	  GENEPI_PLUGIN_FUNC(del_reference) (plugin, Y0);
	  GENEPI_PLUGIN_FUNC(del_reference) (plugin, Y1);
	  for (i = 0; i < size + 3; i++)
	    proj[i] = 0;
	  proj[size + 1] = 1;
	  X = GENEPI_PLUGIN_FUNC(project) (plugin, Y2, proj, size + 3);
	  GENEPI_PLUGIN_FUNC(del_reference) (plugin, Y2);
	}
      if (alpha[k] < 0)
	{
	  genepi_set_impl *Y0;
	  genepi_set_impl *Y1;
	  genepi_set_impl *Z;
	  genepi_set_impl *Y2;
	  int i;
	  for (i = 0; i < size + 3; i++)
	    proj[i] = 0;
	  proj[size + 1] = 1;
	  Y0 = GENEPI_PLUGIN_FUNC(invproject) (plugin, X, proj, size + 3);
	  GENEPI_PLUGIN_FUNC(del_reference) (plugin, X);

	  for (i = 0; i < size + 3; i++)
	    proj[i] = 1;
	  proj[k] = 0;
	  proj[size + 1] = 0;
	  proj[size + 2] = 0;
	  Z = s_linear_multiply (plugin, -alpha[k]);
	  Y1 = GENEPI_PLUGIN_FUNC(invproject) (plugin, Z, proj, size + 3);
	  GENEPI_PLUGIN_FUNC(del_reference) (plugin, Z);
	  /* genepi_set_impl_display_data_structure(Y0,f); */
	  /* genepi_set_impl_display_data_structure(Y1,f); */
	  Y2 = GENEPI_PLUGIN_FUNC(set_intersection) (plugin, Y0, Y1);
	  GENEPI_PLUGIN_FUNC(del_reference) (plugin, Y0);
	  GENEPI_PLUGIN_FUNC(del_reference) (plugin, Y1);
	  for (i = 0; i < size + 3; i++)
	    proj[i] = 0;
	  proj[size + 2] = 1;
	  X = GENEPI_PLUGIN_FUNC(project) (plugin, Y2, proj, size + 3);
	  GENEPI_PLUGIN_FUNC(del_reference) (plugin, Y2);
	}
    }
  /* genepi_set_impl_display_data_structure(X,f); */

  {
    genepi_set_impl *Rn;
    genepi_set_impl *Ran;
    genepi_set_impl *Rp;
    genepi_set_impl *Rap;
    genepi_set_impl *Rconst;
    genepi_set_impl *XX;
    int an, ap;
    if (c >= 0)
      {
	an = 0;
	ap = c;
      }
    else
      {
	an = -c;
	ap = 0;
      }


    for (i = 0; i < size + 2; i++)
      proj[i] = 1;
    /* proj is full of 1. */
    proj[size] = 0;
    Rn = s_constante (an);
    Ran = GENEPI_PLUGIN_FUNC(invproject) (plugin, Rn, proj, size + 2);
    GENEPI_PLUGIN_FUNC(del_reference) (plugin, Rn);
    proj[size] = 1;
    proj[size + 1] = 0;
    Rp = s_constante (ap);
    Rap = GENEPI_PLUGIN_FUNC(invproject) (plugin, Rp, proj, size + 2);
    GENEPI_PLUGIN_FUNC(del_reference) (plugin, Rp);
    proj[size + 1] = 1;
    Rconst = GENEPI_PLUGIN_FUNC(set_intersection) (plugin, Ran, Rap);
    GENEPI_PLUGIN_FUNC(del_reference) (plugin, Ran);
    GENEPI_PLUGIN_FUNC(del_reference) (plugin, Rap);
    /* genepi_set_impl_display_data_structure(Rconst,f); */
    XX = GENEPI_PLUGIN_FUNC(set_intersection) (plugin, X, Rconst);
    GENEPI_PLUGIN_FUNC(del_reference) (plugin, X);
    GENEPI_PLUGIN_FUNC(del_reference) (plugin, Rconst);
    for (i = 0; i < size + 2; i++)
      proj[i] = 0;
    proj[size] = 1;
    proj[size + 1] = 1;
    X = GENEPI_PLUGIN_FUNC(project) (plugin, XX, proj, size + 2);
    GENEPI_PLUGIN_FUNC(del_reference) (plugin, XX);
  }

  free (proj);

  return X;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(linear_operation) (genepi_plugin_impl *plugin, 
				      const int *alpha, int alpha_size, 
				      genepi_comparator op, int cst)
{
  /* We modify alpha and cst into alpha_LEQ and cst_LEQ such that op can  */
  /*  be assume to be GENEPI_LEQ */
  int i;
  int *alpha_LEQ;
  int cst_LEQ;
  genepi_set_impl *result;

  if (op == GENEPI_EQ)
    return s_linear_equality (plugin, alpha, alpha_size, cst);

  alpha_LEQ = calloc (sizeof (int), alpha_size);
  if (op == GENEPI_LT)
    {
      for (i = 0; i < alpha_size; i++)
	alpha_LEQ[i] = alpha[i];
      cst_LEQ = cst - 1;
    }
  else if (op == GENEPI_GT)
    {
      for (i = 0; i < alpha_size; i++)
	alpha_LEQ[i] = -alpha[i];
      cst_LEQ = -cst - 1;
    }
  else if (op == GENEPI_LEQ)
    {
      for (i = 0; i < alpha_size; i++)
	alpha_LEQ[i] = alpha[i];
      cst_LEQ = cst;
    }
  else
    {
      assert (op == GENEPI_GEQ);
      for (i = 0; i < alpha_size; i++)
	alpha_LEQ[i] = -alpha[i];
      cst_LEQ = -cst;
    }
  /* We build alpha_LEQ[0].x[0]+...+ 
     alpha_LEQ[alpha_size-1].x[alpha_size-1]+x[alpha_size]=cst_LEQ */
  {
    genepi_set_impl *set1;
    int *val = calloc (sizeof (int), alpha_size + 1);
    int *selection = calloc (sizeof (int), alpha_size + 1);
    for (i = 0; i < alpha_size; i++)
      val[i] = alpha_LEQ[i];
    val[alpha_size] = 1;
    set1 = s_linear_equality (plugin, val, alpha_size + 1, cst_LEQ);
    /* The free variable x[alpha_size] is projected away */

    for (i = 0; i < alpha_size; i++)
      selection[i] = 0;
    selection[alpha_size] = 1;
    result = GENEPI_PLUGIN_FUNC(project) (plugin, set1, selection, 
					  alpha_size + 1);
    GENEPI_PLUGIN_FUNC(del_reference) (plugin, set1);
    free (val);
    free (selection);
  }

  free (alpha_LEQ);


  return result;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_union) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
			       genepi_set_impl *X2)
{
  genepi_set_impl *R;
  DFA *a;

  if (X1->dim != X2->dim)
    {
      printf ("Dimension not equal in union");
      exit (0);
    }

  R = malloc (sizeof (genepi_set_impl));
  a = dfaProduct (X1->a, X2->a, dfaOR);
  R->a = dfaMinimize (a);
  dfaFree (a);
  R->number_reference = 1;
  R->dim = X1->dim;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_intersection) (genepi_plugin_impl *plugin, 
				      genepi_set_impl *X1, 
				      genepi_set_impl *X2)
{
  DFA *a;
  genepi_set_impl *R;

  if (X1->dim != X2->dim)
    {
      printf ("Dimension not equal in intersection");
      exit (0);
    }

  R = malloc (sizeof (genepi_set_impl));
  R->number_reference = 1;
  R->dim = X1->dim;
  a = dfaProduct (X1->a, X2->a, dfaAND);
  R->a = dfaMinimize (a);
  dfaFree (a);

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_complement) (genepi_plugin_impl *plugin, 
				    genepi_set_impl *X)
{
  genepi_set_impl *R;

  R = malloc (sizeof (genepi_set_impl));
  R->a = dfaCopy (X->a);
  dfaNegation (R->a);
  R->number_reference = 1;
  R->dim = X->dim;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(project) (genepi_plugin_impl *plugin, genepi_set_impl * X, 
			     const int *selection, int size)
{
  DFA *a = dfaCopy (X->a);
  int *map = calloc (sizeof (int), size);
  int pos = 0;
  int i;
  genepi_set_impl *R;

  if (X->dim != size)
    {
      printf ("Dimension not equal in projection");
      exit (0);
    }

  for (i = 0; i < size; i++)
    if (selection[i] == 1)
      {
	DFA *b;
	dfaRightQuotient (a, i);
	b = dfaProject (a, i);
	dfaFree (a);
	a = dfaMinimize (b);
	dfaFree (b);
      }
    else
      {
	map[i] = pos;
	pos++;
      }
  dfaReplaceIndices (a, map);

  R = malloc (sizeof (genepi_set_impl));
  R->a = dfaMinimize (a);
  dfaFree (a);
  R->number_reference = 1;
  R->dim = pos;
  free (map);

  return R;
}


			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(invproject) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
				const int *selection, int size)
{
  int dim;
  int i;
  int *map;
  genepi_set_impl *R;

  dim = 0;

  for (i = 0; i < size; i++)
    if (selection[i] == 0)
      dim++;
  if (X->dim != dim)
    {
      printf ("Dimension not equal in projection");
      exit (0);
    }
  map = calloc (sizeof (int), X->dim);
  dim = 0;
  for (i = 0; i < size; i++)
    if (selection[i] == 0)
      {
	map[dim] = i;
	dim++;
      }

  R = malloc (sizeof (genepi_set_impl));
  R->a = dfaCopy (X->a);
  dfaReplaceIndices (R->a, map);
  R->number_reference = 1;
  R->dim = size;
  free (map);

  return R;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(is_empty) (genepi_plugin_impl *plugin, genepi_set_impl * X)
{
  int i;
  for (i = 1; i < X->a->ns; i++)
    if (X->a->f[i] == 1)
      return 0;
  return 1;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(is_full) (genepi_plugin_impl *plugin, genepi_set_impl * X)
{
  int i;
  for (i = 1; i < X->a->ns; i++)
    if (X->a->f[i] != 1)
      return 0;
  return 1;
}

			/* --------------- */


static int
s_is_included_in (genepi_plugin_impl *plugin, genepi_set_impl * X1, 
		  genepi_set_impl * X2)
{
  genepi_set_impl *X2bar;
  genepi_set_impl *X1capX2bar;
  int r;

  if (X1->dim != X2->dim)
    {
      printf ("Dimension not equal in inclusion");
      exit (0);
    }

  X2bar = GENEPI_PLUGIN_FUNC(set_complement) (plugin, X2);
  X1capX2bar = GENEPI_PLUGIN_FUNC(set_intersection) (plugin, X1, X2bar);
  r = GENEPI_PLUGIN_FUNC(is_empty) (plugin, X1capX2bar);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, X1capX2bar);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, X2bar);

  return r;
}



static int
s_equal (genepi_plugin_impl *plugin, genepi_set_impl * X1, genepi_set_impl * X2)
{
  int r;
  genepi_set_impl *R;

  if (X1->dim != X2->dim)
    {
      printf ("Dimension not equal in equal");
      exit (0);
    }

  R = malloc (sizeof (genepi_set_impl));
  R->number_reference = 1;
  R->dim = X1->dim;
  R->a = dfaProduct (X1->a, X2->a, dfaBIIMPL);
  r = GENEPI_PLUGIN_FUNC(is_full) (plugin, R);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, R);

  return r;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(compare) (genepi_plugin_impl *plugin, genepi_set_impl * X1, 
			     genepi_comparator op, genepi_set_impl * X2)
{
  assert (X1->dim == X2->dim);

  if (op == GENEPI_EQ)
    return s_equal (plugin, X1, X2);
  else if (op == GENEPI_LEQ)
    return s_is_included_in (plugin, X1, X2);
  else if (op == GENEPI_GEQ)
    return s_is_included_in (plugin, X2, X1);
  else if (op == GENEPI_LT)
    return (s_is_included_in (plugin, X1, X2) && (!s_equal (plugin, X1, X2)));
  else
    {
      assert (op == GENEPI_GT);

      return (s_is_included_in (plugin, X2, X1) && (!s_equal (plugin, X2, X1)));
    }
}

			/* --------------- */

static void
s_dfa_to_dot(DFA *a, int no_free_vars, unsigned *offsets, FILE *out)
{
  paths state_paths, pp;
  trace_descr tp;
  int i, j, k, l;
  char **buffer;
  int *used, *allocated;
  
  fprintf (out, "digraph MONA_DFA {\n"
	   " rankdir = LR;\n"
	   " center = true;\n"
	   " size = \"7.5,10.5\";\n"
	   " edge [fontname = Courier];\n"
	   " node [height = .5, width = .5];\n"
	   " node [shape = doublecircle];");
  for (i = 0; i < a->ns; i++)
    if ( (a->f[i] == 1) && (a->s!=i) )
      fprintf (out, " %d;", i);
  fprintf (out, "\n node [shape = circle];");
  for (i = 0; i < a->ns; i++)
    if ( (a->f[i] == -1) && (a->s!=i))
      fprintf (out, " %d;", i);
  fprintf (out, "\n node [shape = box];");
  for (i = 0; i < a->ns; i++)
    if ( (a->f[i] == 0) && (a->s!=i)) 
      fprintf (out, " %d;", i);
  //fprintf (out, "\n init [shape = plaintext, label = \"\"];\n"
  //	 " init -> %d;\n", a->s);
  
  buffer = (char **) mem_alloc(sizeof(char *) * a->ns);
  used = (int *) mem_alloc(sizeof(int) * a->ns);
  allocated = (int *) mem_alloc(sizeof(int) * a->ns);
  
  for (i = 0; i < a->ns; i++) {
      state_paths = pp = make_paths(a->bddm, a->q[i]);
      for (j = 0; j < a->ns; j++) {
	buffer[j] = 0;
	used[j] = allocated[j] = 0;
      }
      
      while (pp) {
	if (used[pp->to] >= allocated[pp->to]) {
	  allocated[pp->to] = allocated[pp->to]*2+2;
	  buffer[pp->to] = 
	    (char *) mem_resize(buffer[pp->to], sizeof(char) * 
				allocated[pp->to] * no_free_vars);
	}
	
	for (j = 0; j < no_free_vars; j++) {
	  char c;
	  for (tp = pp->trace; tp && (tp->index != offsets[j]); tp = tp->next);
	  
	  if (tp) {
	    if (tp->value) 
	      c = '1';
	    else 
	      c = '0';
	  }
	  else
	    c = 'X';
	  
	  buffer[pp->to][no_free_vars*used[pp->to] + j] = c;
	}
	used[pp->to]++;
	pp = pp->next;
      }
      
      if (i==a->s) {
	for (j = 0; j < a->ns; j++) 
	  if (buffer[j]) {
	    fprintf (out, "\n init [shape = plaintext, label = \"\"];\n"
		     " init -> %d;\n", j);
	    
	  }} 
      else
    for (j = 0; j < a->ns; j++) 
      if (buffer[j]) {
	fprintf (out, " %d -> %d [label=\"", i, j);
	for (k = 0; k < no_free_vars; k++) {
	  for (l = 0; l < used[j]; l++) {
	    fputc(buffer[j][no_free_vars*l+k], out);
	    if (l+1 < used[j]) {
	      if (k+1 == no_free_vars)
		fputc(',', out );
	      else
		fputc(' ', out);
	    }
	  }
	  if (k+1 < no_free_vars)
	  fprintf (out, "\\n");
	}
	fprintf (out, "\"];\n");
	mem_free(buffer[j]);
      }

    kill_paths(state_paths);
  }

  mem_free(allocated);
  mem_free(used);
  mem_free(buffer);

  fprintf (out, "}\n");
}

			/* --------------- */

void
GENEPI_PLUGIN_FUNC(display_data_structure) (genepi_plugin_impl *plugin, 
					    genepi_set_impl * X, FILE * output)
{
   unsigned int *indices = calloc (sizeof (int), X->dim);
  int i;
  for (i = 0; i < X->dim; i++)
    indices[i] = i;
  s_dfa_to_dot (X->a, X->dim, indices, output);

  free (indices);
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(get_width) (genepi_plugin_impl *plugin, genepi_set_impl * X)
{
  return X->dim;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(get_data_structure_size) (genepi_plugin_impl *plugin, 
					     genepi_set_impl * X)
{
  return bdd_size (X->a->bddm);
}

			/* --------------- */

static int
mona_next_state (DFA * a, unsigned curr_state, int *bit_vector,
		 int size_bit_vector)
{
  bdd_ptr b;
  bdd_manager *bddm = a->bddm;

  if (curr_state >= (unsigned) a->ns)
    {
      printf ("Internal error in is_solution");
      exit (0);
    }

  b = a->q[curr_state];
  while (!bdd_is_leaf (bddm, b))
    {
      unsigned index = bdd_ifindex (bddm, b);
      if (index >= (unsigned) size_bit_vector)
	{
	  printf ("Internal error in is_solution");
	  exit (0);
	}
      if ((bit_vector[index] % 2) == 1)
	{
	  b = bdd_then (bddm, b);
	}
      else
	{
	  b = bdd_else (bddm, b);
	}
    }
  return bdd_leaf_value (bddm, b);
}



int
GENEPI_PLUGIN_FUNC(is_solution) (genepi_plugin_impl *plugin, genepi_set_impl *X,
				 const int *x, int x_size, int vden)
{
  int i;
  DFA *a;
  unsigned curr_state;
  int *curr_x;
  int terminate;

  /* Non yet implemented */
  if (X->dim != x_size)
    {
      printf ("Dimension not equal in is_solution");
      exit (0);
    }

  if (x_size == 0)
    return 1;



  for (i = 0; i < x_size; i++)
    if (x[i] < 0)
      return 0;

  a = X->a;
  curr_state = a->s;
  curr_x = calloc (sizeof (int), x_size);

  for (i = 0; i < x_size; i++)
    {
      /* two times since the DFA start with a useless bitvector */
      curr_x[i] = 2 * x[i]; 
    }


  do
    {
      curr_state = mona_next_state (a, curr_state, curr_x, x_size);
      terminate = 1;
      for (i = 0; i < x_size; i++)
	{
	  if (curr_x[i] != 0)
	    terminate = 0;
	  curr_x[i] = curr_x[i] / 2;
	}
    }
  while (!terminate);
  free (curr_x);

  return (a->f[curr_state] == 1);
}



			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(get_one_solution) (genepi_plugin_impl *plugin, 
				      genepi_set_impl *X, int *x, int x_size)
{
  char *exmp;
  unsigned *indices = (unsigned *) malloc (X->dim * sizeof (unsigned));
  int i;
  int size;
  int v;

  for (i = 0; i < X->dim; i++)
    indices[i] = i;

  exmp = dfaMakeExample (X->a, 1, X->dim, indices);
  /* fprintf(stderr,"%s",exmp); */
  /* genepi_set_impl_display_data_structure(X); */
  free (indices);

  if (exmp == NULL)
    return 0;

  size = strlen (exmp) / (X->dim + 1);
  for (v = 0; v < X->dim; v++)
    {
      int bin = 1;
      int p;

      x[v] = 0;
      for (p = 1; p < size; p++)
	{
	  if (exmp[v * size + p] == '1')
	    x[v] = x[v] + bin;
	  bin = bin * 2;
	}
    }
  mem_free (exmp);

  return 1;
}

			/* --------------- */

unsigned int
GENEPI_PLUGIN_FUNC(cache_hash) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return 791 * (uintptr_t) X->a + 1023 * X->dim;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(cache_equal) (genepi_plugin_impl *plugin, 
				 genepi_set_impl *X1, genepi_set_impl *X2)
{
  return X1->a == X2->a && X1->dim == X2->dim;
}

			/* --------------- */

