/*
 * genz-plugin.c -- 
 * 
 * This file is a part of the GENeric GENEPI plugin for natural numbers. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#ifndef GENEPI_PLUGIN_PREFIX      
# warning "GENEPI_PLUGIN_PREFIX is not defined"
# define GENEPI_PLUGIN_PREFIX(_f) genz ## _f 
#endif /* ! GENEPI_PLUGIN_NAME */

#include <genepi/genepi-plugin.h>

#define CONF_CACHE_SIZE "cache_size"
#define CONF_PLUGIN_NAME "nat_plugin"
#define CONF_XN_AT_THE_END "xn_at_end"
#define CONF_ADD_XN_XP_CONSTRAINT "add_xn_xp_constraint"

			/* --------------- */

#define IMPL(_s) ((genepi_set_impl *) (_s))

struct genepi_plugin_impl_st
{
  int cache_size;
  genepi_set **cache;
  genepi_solver *solver;
  int xn_at_the_end;
  int add_xp_xn_constraint;
};

			/* --------------- */

static genepi_solver *
s_get_solver (genepi_plugconf *conf)
{
  genepi_solver *result = NULL;
  const char *plugin_name = 
    genepi_plugconf_get_entry (conf, CONF_PLUGIN_NAME, NULL);

  if (plugin_name == NULL)
    fprintf(stderr, "genz-plugin: error: No plugin name specified for the "
	    "GENZ plugin. Please set the configuration option '%s'.\n",
	    CONF_PLUGIN_NAME);
  else
    {
      genepi_plugin *plugin = genepi_loader_get_plugin (plugin_name);
      if (plugin == NULL)
	fprintf (stderr, "genz: can't find plugin '%s'.\n", plugin_name);
      else 
	{
	  result = genepi_solver_create (plugin, 0, 10000, 100000);
	  genepi_plugin_del_reference (plugin);
	}
    }

  return result;
}

			/* --------------- */

genepi_plugin_impl *
GENEPI_PLUGIN_FUNC(init) (genepi_plugconf *conf)
{  
  genepi_plugin_impl *result;
  genepi_solver *solver = s_get_solver (conf);

  if (solver == NULL)
    result = NULL;
  else
    {
      result = malloc (sizeof (struct genepi_plugin_impl_st));
      result->solver = solver;
      result->cache_size = 
	genepi_plugconf_get_unsigned_int (conf, CONF_CACHE_SIZE, 0);
      if (result->cache_size > 0)
	result->cache = calloc (sizeof (genepi_set *), result->cache_size);
      else
	result->cache = NULL;

      result->add_xp_xn_constraint = 
	genepi_plugconf_get_bool (conf, CONF_ADD_XN_XP_CONSTRAINT, 0);
      result->xn_at_the_end = 
	genepi_plugconf_get_bool (conf, CONF_XN_AT_THE_END, 0);
    }

  return result;
}

			/* --------------- */


void
GENEPI_PLUGIN_FUNC(terminate) (genepi_plugin_impl *plugin)
{
  int i;

  if (plugin->cache != NULL)
    {
      for (i = 0; i < plugin->cache_size; i++)
	if (plugin->cache[i] != NULL)
	  genepi_set_del_reference (plugin->solver, plugin->cache[i]);
      free (plugin->cache);
    }
  genepi_solver_delete (plugin->solver);
  free (plugin);
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(autotest) (genepi_plugin_impl *plugin)
{
  return 1;
}

			/* --------------- */


genepi_solver_type 
GENEPI_PLUGIN_FUNC(get_solver_type) (genepi_plugin_impl *plugin)
{
  return GENEPI_Z_SOLVER;
}


			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(add_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X)
{
  return IMPL(genepi_set_add_reference (plugin->solver, (genepi_set *)X));
}

			/* --------------- */

void
GENEPI_PLUGIN_FUNC(del_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X)
{
  genepi_set_del_reference (plugin->solver, (genepi_set *)X);
}

			/* --------------- */

static genepi_set *
s_xp_or_xn_is_null (genepi_plugin_impl *plugin, int n, int x)
{
  const int xp = plugin->xn_at_the_end ? (x) : (2 * x);
  const int xn = plugin->xn_at_the_end ? (x + n) : (2 * x + 1);
  genepi_set *tmp[2];
  genepi_set *result;
  int *alpha = (int *) calloc (sizeof (int), 2 * n);
  
  alpha[xp] = 1;
  tmp[0] = genepi_set_linear_equality (plugin->solver, alpha, 2 * n, 0);
  alpha[xp] = 0;
  alpha[xn] = 1;
  tmp[1] = genepi_set_linear_equality (plugin->solver, alpha, 2 * n, 0);
  result = genepi_set_union (plugin->solver, tmp[0], tmp[1]);

  genepi_set_del_reference (plugin->solver, tmp[0]);
  genepi_set_del_reference (plugin->solver, tmp[1]);
  free (alpha);

  return result;
}

			/* --------------- */

static genepi_set *
s_compute_xp_xn_constraint (genepi_plugin_impl *plugin, int w)
{
  genepi_solver *solver = plugin->solver;
  genepi_set *result = plugin->cache [w];

  if (result == NULL)
    {
      int i;
      fprintf(stderr, "cache[%d] plugin->xn_at_the_end=%d", w, 
	      plugin->xn_at_the_end);
      
      result = s_xp_or_xn_is_null (plugin, w, 0);
      for (i = 1; i < w; i++)
	{
	  genepi_set *constraint = s_xp_or_xn_is_null (plugin, w, i);
	  genepi_set *tmp = genepi_set_intersection (solver, constraint, 
						     result);
	  genepi_set_del_reference (solver, constraint);
	  genepi_set_del_reference (solver, result);
	  result = tmp;
	}
      plugin->cache[w] = genepi_set_add_reference (solver, result);
      fprintf(stderr, " done\n");
    }
  else
    {
      result = genepi_set_add_reference (solver, result);
    }

  return result;
}

			/* --------------- */

static void
s_add_xp_xn_constraint (genepi_plugin_impl *plugin, genepi_set **pS)
{
  if (plugin->add_xp_xn_constraint)
    {
      genepi_solver *solver = plugin->solver;
      int w = genepi_set_get_width (solver, *pS) >> 1;
      genepi_set *constraint = s_compute_xp_xn_constraint (plugin, w);
      genepi_set *S = genepi_set_intersection (solver, constraint, *pS);
      genepi_set_del_reference (solver, constraint);
      genepi_set_del_reference (solver, *pS);
      *pS = S;
    }
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(top) (genepi_plugin_impl *plugin, int n)
{
  genepi_set *result = genepi_set_top (plugin->solver, 2 * n);
  s_add_xp_xn_constraint (plugin, &result);

  return IMPL(result);
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(top_N) (genepi_plugin_impl *plugin, int n)
{
  int i;
  int *alpha = calloc (sizeof (int), 2 * n);
  genepi_solver *solver = plugin->solver;
  genepi_set *result = genepi_set_top (solver, 2 * n);

  s_add_xp_xn_constraint (plugin, &result);
  for (i = 0; i < n; i++)
    {
      const int xp = plugin->xn_at_the_end ? (i) : (2 * i);
      const int xn = plugin->xn_at_the_end ? (i + n) : (2 * i + 1);
      genepi_set *xp_geq_xn;
      genepi_set *tmp;

      alpha[xp] = 1;
      alpha[xn] = -1;
      xp_geq_xn = 
	genepi_set_linear_operation (solver, alpha, 2 * n, GENEPI_GEQ, 0);
      tmp = genepi_set_intersection (solver, xp_geq_xn, result);
      genepi_set_del_reference (solver, xp_geq_xn);
      genepi_set_del_reference (solver, result);
      result = tmp;
      alpha[xp] = 0;      
      alpha[xn] = 0;      
    }
  free (alpha);

  return IMPL(result);
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_Z) (genepi_plugin_impl *plugin, int n)
{
  return GENEPI_PLUGIN_FUNC (top) (plugin, n);
}

			/* --------------- */

/*
genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_P) (int n);
*/

			/* --------------- */

/* 
genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_R) (int n);
*/

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(bot) (genepi_plugin_impl *plugin, int n)
{
  return IMPL (genepi_set_bot (plugin->solver, 2 * n));
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(linear_operation) (genepi_plugin_impl *plugin, 
				      const int *alpha, int alpha_size, 
				      genepi_comparator op, int c)
{
  int i;
  int *a_alpha = calloc (sizeof (int), 2 * alpha_size);
  genepi_set *lin;

  if (plugin->xn_at_the_end)
    {
      for (i = 0; i < alpha_size; i++)
	a_alpha[i] = alpha[i];
      for (i = 0; i < alpha_size; i++)
	a_alpha[alpha_size + i] = -alpha[i];
    }
  else
    {
      for (i = 0; i < alpha_size; i++)
	{
	  a_alpha[2 * i] = alpha[i];
	  a_alpha[2 * i + 1] = -alpha[i];
	}
    }
  lin = 
    genepi_set_linear_operation (plugin->solver, a_alpha, 2 * alpha_size, 
				 op, c);
  s_add_xp_xn_constraint (plugin, &lin);
  free (a_alpha);

  return IMPL (lin);
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_union) (genepi_plugin_impl *plugin, genepi_set_impl *X1,
			       genepi_set_impl *X2)
{
  return IMPL (genepi_set_union (plugin->solver, (genepi_set *) X1, 
				 (genepi_set *) X2));
}


			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_intersection) (genepi_plugin_impl *plugin, 
				      genepi_set_impl *X1, genepi_set_impl *X2)
{
  return IMPL (genepi_set_intersection (plugin->solver, (genepi_set *) X1, 
					(genepi_set *) X2));
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_complement) (genepi_plugin_impl *plugin, 
				    genepi_set_impl *X)
{
  genepi_set *c = genepi_set_complement (plugin->solver, (genepi_set *) X);
  s_add_xp_xn_constraint (plugin, &c);

  return IMPL (c);
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(project) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
			     const int *selection, int size)
{
  int i;
  int *a_sel = calloc (sizeof (int), 2 * size);
  genepi_set *result;

  if (plugin->xn_at_the_end)
    {
      for (i = 0; i < size; i++)
	a_sel[i] = selection[i];
      for (i = 0; i < size; i++)
	a_sel[size + i] = selection[i];
    }
  else
    {
      for (i = 0; i < size; i++)
	a_sel[2 * i] = a_sel[2 * i + 1] = selection[i];
    }
  result = genepi_set_project (plugin->solver, (genepi_set *) X, a_sel, 
			       2 * size);
  s_add_xp_xn_constraint (plugin, &result);
  free (a_sel);

  return IMPL (result);
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(invproject) (genepi_plugin_impl *plugin, genepi_set_impl *X,
				const int *selection, int size)
{
  int i;
  int *a_sel = calloc (sizeof (int), 2 * size);
  genepi_set *result;

  if (plugin->xn_at_the_end)
    {
      for (i = 0; i < size; i++)
	a_sel[i] = selection[i];
      for (i = 0; i < size; i++)
	a_sel[size + i] = selection[i];
    }
  else
    {
      for (i = 0; i < size; i++)
	a_sel[2 * i] = a_sel[2 * i + 1] = selection[i];
    }
  result = genepi_set_invproject (plugin->solver, (genepi_set *) X, a_sel, 
				  2 * size);  
  free (a_sel);
  s_add_xp_xn_constraint (plugin, &result);

  return IMPL (result);
}

			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(compare) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
			     genepi_comparator op, genepi_set_impl *X2)
{
  return genepi_set_compare (plugin->solver, (genepi_set *) X1, op, 
			     (genepi_set *) X2);
}

			/* --------------- */


int 
GENEPI_PLUGIN_FUNC(is_empty) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return genepi_set_is_empty (plugin->solver, (genepi_set *) X);
}


			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(is_full) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return genepi_set_is_full (plugin->solver, (genepi_set *) X);
}

			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(is_finite) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return genepi_set_is_finite (plugin->solver, (genepi_set *) X);
}

			/* --------------- */

/*
int
GENEPI_PLUGIN_FUNC(depend_on) (genepi_set_impl *X, const int *sel, int selsize);
*/


			/* --------------- */
/*
void 
GENEPI_PLUGIN_FUNC(get_solutions) (genepi_set_impl *X, int ***psolutions, 
				   int *psize,
				   int max);
*/

			/* --------------- */

void 
GENEPI_PLUGIN_FUNC(display_all_solutions) (genepi_plugin_impl *plugin, 
					   genepi_set_impl *X, FILE *output,
					   const char * const *varnames)
{
  int i;
  genepi_solver *solver = plugin->solver;
  int w = genepi_set_get_width (solver, (genepi_set *) X) >> 1;
  char **d_varnames = (char **) calloc (sizeof (char *), 2 * w);

  for (i = 0; i < w; i++)
    {
      int len = strlen (varnames[i]);
      const int xp = plugin->xn_at_the_end ? (i) : (2 * i);
      const int xn = plugin->xn_at_the_end ? (i + w) : (2 * i + 1);

      d_varnames[xp] = calloc (sizeof (char), len + 2 + 1);
      strcpy (d_varnames[xp], varnames[i]);
      d_varnames[xp][len] = '_';
      d_varnames[xp][len + 1] = 'p';

      d_varnames[xn] = calloc (sizeof (char), len + 2 + 1);
      strcpy (d_varnames[xn], varnames[i]);
      d_varnames[xn][len] = '_';
      d_varnames[xn][len + 1] = 'n';
    }

  genepi_set_display_all_solutions (solver, (genepi_set *)X, output, 
				    (const char * const *) d_varnames);
  for (i = 0; i < 2 * w; i++)
    free (d_varnames[i]);
  free (d_varnames);
}

			/* --------------- */

void 
GENEPI_PLUGIN_FUNC(display_data_structure) (genepi_plugin_impl *plugin, 
					    genepi_set_impl *X, FILE *output)
{
  genepi_set_display_data_structure (plugin->solver, (genepi_set *) X, output);
}

			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(get_width) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return genepi_set_get_width (plugin->solver, (genepi_set *) X) >> 1;
}

			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(get_data_structure_size) (genepi_plugin_impl *plugin, 
					     genepi_set_impl *X)
{
  return genepi_set_get_data_structure_size (plugin->solver, (genepi_set *) X);
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(is_solution) (genepi_plugin_impl *plugin, genepi_set_impl *X,
				 const int *x, int x_size, int xden)
{
  int i;
  int *a_x = calloc (sizeof (int), 2 * x_size);
  int result;

  if (xden != 1)
    abort();
  
  if (plugin->xn_at_the_end)
    {
      for (i = 0; i < x_size; i++)
	{
	  if (x[i] >= 0) a_x[i] = x[i];
	  else a_x[x_size + i] = -x[i];
	}
    }
  else
    {
      for (i = 0; i < x_size; i++)
	{
	  if (x[i] >= 0) a_x[2 * i] = x[i];
	  else a_x[2 * i + 1] = -x[i];
	}
    }
  result = genepi_set_is_solution (plugin->solver, (genepi_set *) X, a_x, 
				   2 * x_size, xden);
  free (a_x);

  return result;
}


			/* --------------- */

int
GENEPI_PLUGIN_FUNC(get_one_solution) (genepi_plugin_impl *plugin, 
				      genepi_set_impl *X, int *x, int x_size)
{  
  int i;
  int *a_x = calloc (sizeof (int), 2 * x_size);
  int den = genepi_set_get_one_solution (plugin->solver, (genepi_set *) X, a_x, 
					 2 * x_size);
  if (plugin->xn_at_the_end)
    {
      for (i = 0; i < x_size; i++)
	x[i] = a_x[i] - a_x[x_size + i];
    }
  else
    {
      for (i = 0; i < x_size; i++)
	x[i] = a_x[2 * i] - a_x[2 * i + 1];
    }
  free (a_x);

  return den;
}
 

			/* --------------- */

#if ENABLE_ALL_PROTOS
unsigned int
GENEPI_PLUGIN_FUNC(cache_hash) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return 0;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC(cache_equal) (genepi_plugin_impl *plugin, 
				 genepi_set_impl *X1, genepi_set_impl *X2)
{
  return 0;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC (write_set) (genepi_plugin_impl *plugin, FILE *output, 
				genepi_set_impl *set)
{
  return GENEPI_RW_ERR_IO;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC (read_set) (genepi_plugin_impl *plugin, FILE *input, 
			       genepi_set_impl **pset)
{
  return GENEPI_RW_ERR_IO;
}
#endif 

			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC (preorder) (genepi_plugin_impl *plugin, genepi_set_impl *X1,
			       genepi_set_impl *X2)
{
  return 0;
}
#endif 
