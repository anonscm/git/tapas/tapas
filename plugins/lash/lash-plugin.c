/*
 * lash-plugin.c -- the NDD plugin
 * 
 * This file is a part of the LASH-based plugins for GENEPI. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ndd.h>
#include <lash-auto-io.h>
#include <diag.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <genepi/genepi-plugin.h>


			/* --------------- */

struct genepi_plugin_impl_st
{
  int basis;
  int msdf;
};

struct genepi_set_impl_st
{
  ndd *a;
  int number_reference;
  int dim;
};

			/* --------------- */

static const char *CONF_BASIS = "decomposition_basis";
static const char *CONF_MSDF = "most_significant_digit_first";

			/* --------------- */

static int lash_init_count = 0;

			/* --------------- */

genepi_plugin_impl *
GENEPI_PLUGIN_FUNC(init) (genepi_plugconf *conf)
{
  genepi_plugin_impl *result = NULL;

  if (lash_init () < 0 && lash_errno != LASH_ERR_ALREADY)
    {
      fprintf (stderr, "LASH init error:");
      switch (lash_errno)
	{
	case LASH_ERR_BAD_ARCH: 
	  fprintf (stderr, "architecture not supported.");
	  break;
	case LASH_ERR_IN_USE:
	  fprintf (stderr, "internal inconsistency.");
	  break;
	case LASH_ERR_LOGFILE:
	  fprintf (stderr, "cannot create logfile.");
	  break;
	};
      fprintf (stderr, "\n");

      return NULL;
    }

  result = malloc (sizeof *result);
  result->basis = genepi_plugconf_get_unsigned_int (conf, CONF_BASIS, 2);
  result->msdf = genepi_plugconf_get_bool (conf, CONF_MSDF, 0);
  lash_init_count++;

  return result;
}

			/* --------------- */

void
GENEPI_PLUGIN_FUNC(terminate) (genepi_plugin_impl *plugin)
{
  assert (lash_init_count > 0);
  lash_init_count--;
  if (lash_init_count == 0)
    lash_end ();
  free (plugin);
}

			/* --------------- */

genepi_solver_type
GENEPI_PLUGIN_FUNC(get_solver_type) (genepi_plugin_impl *plugin)
{
  return GENEPI_Z_SOLVER;
}


			/* --------------- */
genepi_set_impl *
GENEPI_PLUGIN_FUNC(add_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl * X)
{
  X->number_reference++;
  return X;
}

			/* --------------- */

void
GENEPI_PLUGIN_FUNC(del_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl * X)
{
  X->number_reference--;
  if (X->number_reference == 0)
    {
      ndd_free (X->a);
      free (X);
    }
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(top) (genepi_plugin_impl *plugin, int n)
{
  genepi_set_impl *R = malloc (sizeof (genepi_set_impl));

  R->number_reference = 1;
  R->dim = n;

  if (plugin->msdf)
    R->a = ndd_create_zn_msdf (plugin->basis, n);
  else
    R->a = ndd_create_zn_lsdf (plugin->basis, n);
  /* on ajoute >=0 a chaque fois. */
  assert (R->a != NULL);

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(bot) (genepi_plugin_impl *plugin, int n)
{
  genepi_set_impl *R = malloc (sizeof (genepi_set_impl));

  R->number_reference = 1;
  R->dim = n;
  if (plugin->msdf)
    R->a = ndd_create_empty_msdf (plugin->basis, n);
  else
    R->a = ndd_create_empty_lsdf (plugin->basis, n);
  assert (R->a != NULL);

  return R;
}

			/* --------------- */




genepi_set_impl *
GENEPI_PLUGIN_FUNC(linear_operation) (genepi_plugin_impl *plugin, 
				      const int *calpha, int alpha_size, 
				      genepi_comparator op, int c)
{
  genepi_set_impl *R;
  int *alpha = calloc (sizeof (int), alpha_size);

  {
    int i;

    for (i = 0; i < alpha_size; i++)
      alpha[i] = calpha[i];
  }

  R = malloc (sizeof (genepi_set_impl));
  R->number_reference = 1;
  R->dim = alpha_size;
  if (op == GENEPI_EQ)
    if (plugin->msdf)
      R->a =
	ndd_create_equation_msdf (plugin->basis, alpha_size, alpha, c);
    else
      R->a =
	ndd_create_equation_lsdf (plugin->basis, alpha_size, alpha, c);
  else if (op == GENEPI_LEQ)
    if (plugin->msdf)
      R->a =
	ndd_create_inequation_msdf (plugin->basis, alpha_size, alpha,
				    c);
    else
      R->a =
	ndd_create_inequation_lsdf (plugin->basis, alpha_size, alpha,
				    c);
  else if (op == GENEPI_GEQ)
    {
      int *negalpha = calloc (sizeof (int), alpha_size);
      int i;
      for (i = 0; i < alpha_size; i++)
	negalpha[i] = -alpha[i];
      if (plugin->msdf)
	R->a =
	  ndd_create_inequation_msdf (plugin->basis, alpha_size,
				      negalpha, -c);
      else
	R->a =
	  ndd_create_inequation_lsdf (plugin->basis, alpha_size,
				      negalpha, -c);
      free (negalpha);
    }
  else if (op == GENEPI_LT)
    if (plugin->msdf)
      R->a =
	ndd_create_inequation_msdf (plugin->basis, alpha_size, alpha,
				    c - 1);
    else
      R->a =
	ndd_create_inequation_lsdf (plugin->basis, alpha_size, alpha,
				    c - 1);
  else
    {
      int i;
      int *negalpha = calloc (sizeof (int), alpha_size);

      assert (op == GENEPI_GT);
      for (i = 0; i < alpha_size; i++)
	negalpha[i] = -alpha[i];
      if (plugin->msdf)
	R->a =
	  ndd_create_inequation_msdf (plugin->basis, alpha_size,
				      negalpha, -c - 1);
      else
	R->a =
	  ndd_create_inequation_lsdf (plugin->basis, alpha_size,
				      negalpha, -c - 1);
      free (negalpha);
    }
  free (alpha);
  assert (R->a != NULL);

  return R;
}

		       /* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_union) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
			       genepi_set_impl * X2)
{
  genepi_set_impl *R;

  assert (X1->dim == X2->dim);

  R = malloc (sizeof (genepi_set_impl));
  R->number_reference = 1;
  R->dim = X1->dim;
  R->a = ndd_union (X1->a, X2->a);
  assert (R->a != NULL);

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_intersection) (genepi_plugin_impl *plugin, 
				      genepi_set_impl * X1, 
				      genepi_set_impl * X2)
{
  genepi_set_impl *R;

  assert (X1->dim == X2->dim);

  R = malloc (sizeof (genepi_set_impl));
  R->number_reference = 1;
  R->dim = X1->dim;
  R->a = ndd_intersection (X1->a, X2->a);
  assert (R->a != NULL && "memory exhauted");

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_complement) (genepi_plugin_impl *plugin, 
				    genepi_set_impl * X)
{
  genepi_set_impl *R;
  genepi_set_impl *Top;

  R = malloc (sizeof (genepi_set_impl));
  R->number_reference = 1;
  R->dim = X->dim;
  Top = GENEPI_PLUGIN_FUNC(top) (plugin, X->dim);
  R->a = ndd_difference (Top->a, X->a);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, Top);
  assert (R->a != NULL);

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(project) (genepi_plugin_impl *plugin, genepi_set_impl * X, 
			     const int *selection, int size)
{
  int dim;
  int i;
  genepi_set_impl *R;
  ndd *a;

  assert (X->dim == size);

  dim = 0;
  for (i = 0; i < size; i++)
    if (selection[i] == 0)
      dim++;

  R = malloc (sizeof (genepi_set_impl));
  R->number_reference = 1;
  R->dim = dim;
  a = ndd_copy (X->a);
  for (i = size - 1; i >= 0; i--)
    if (selection[i] == 1)
      {
	ndd *b = ndd_projection (a, i);
	ndd_free (a);
	a = b;
      }
  R->a = a;
  assert (R->a != NULL);

  return R;
}

			/* --------------- */


static genepi_set_impl *
s_invproject_single (genepi_set_impl * X, int pos)
{
  genepi_set_impl *R;
  int dim = X->dim;
  ndd *a;

  /* As it is not possible to add a single variable, we need to multiply by 2 
     the number of variables and then project away all the variables expect the
     interesting one. */

  if (pos == 0)
    {
      int i;
      a = ndd_interleave_z (X->a, 0, 1);

      for (i = dim - 1; i >= 1; i--)
	{
	  ndd *b = ndd_projection (a, 2 * i);
	  ndd_free (a);
	  a = b;
	}
    }
  else
    {
      int i;
      a = ndd_interleave_z (X->a, 1, 0);

      for (i = dim - 1; i >= 0; i--)
	if (i != pos - 1)
	  {
	    ndd *b = ndd_projection (a, 2 * i + 1);
	    ndd_free (a);
	    a = b;
	  }
    }
  dim++;

  R = malloc (sizeof (genepi_set_impl));
  R->number_reference = 1;
  R->dim = dim;
  R->a = a;

  assert (R->a != NULL);

  return R;
}



genepi_set_impl *
GENEPI_PLUGIN_FUNC(invproject) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
				const int *selection, int size)
{
  int dim;
  int i;
  int nr = 0;
  int nl = 0;
  genepi_set_impl *R;

  dim = 0;
  for (i = 0; i < size; i++)
    if (selection[i] == 0)
      dim++;

  assert (X->dim == dim);

  /* we check if selection has the form 0,1,0,1,0,1... */
  if ((size > 0) && (size % 2 == 0))
    {
      if (selection[0] == 0)
	{
	  int i;
	  nl = 1;

	  for (i = 0; (i < size / 2) && (nl != 0); i++)
	    if ((selection[2 * i] != 0) || (selection[2 * i + 1] != 1))
	      nl = 0;
	}
      else
	{
	  int i;
	  nr = 1;

	  for (i = 0; (i < size / 2) && (nr != 0); i++)
	    if ((selection[2 * i] != 1) || (selection[2 * i + 1] != 0))
	      nr = 0;
	}
    }

  if ((nl != 0) || (nr != 0))
    {
      R = malloc (sizeof (genepi_set_impl));
      R->a = ndd_interleave_z (X->a, nl, nr);
      R->number_reference = 1;
      R->dim = size;
    }
  else
    {
      R = X;
      GENEPI_PLUGIN_FUNC(add_reference) (plugin, R);
      for (i = 0; i < size; i++)
	if (selection[i] == 1)
	  {
	    genepi_set_impl *S = s_invproject_single (R, i);
	    GENEPI_PLUGIN_FUNC(del_reference) (plugin, R);
	    R = S;
	  }
    }

  assert (R->a != NULL);

  return R;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(compare) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
			     genepi_comparator op, genepi_set_impl *X2)
{
  assert (X1->dim == X2->dim);
  assert (X1->a != NULL);
  assert (X2->a != NULL);

  if (op == GENEPI_EQ)
    return ndd_equality (X1->a, X2->a);
  else if (op == GENEPI_LEQ)
    return ndd_inclusion (X1->a, X2->a);
  else if (op == GENEPI_GEQ)
    return ndd_inclusion (X2->a, X1->a);
  else if (op == GENEPI_LT)
    return (ndd_inclusion (X1->a, X2->a) && (!ndd_equality (X1->a, X2->a)));
  else
    {
      assert (op == GENEPI_GT);
      return (ndd_inclusion (X2->a, X1->a) && (!ndd_equality (X2->a, X1->a)));
    }
}

			/* --------------- */


int
GENEPI_PLUGIN_FUNC(is_empty) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  assert (X->a != NULL);

  return ndd_is_empty (X->a);
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(get_width) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  assert (X->a != NULL);

  return X->dim;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(get_data_structure_size) (genepi_plugin_impl *plugin, 
					     genepi_set_impl *X)
{
  assert (X->a != NULL);

  return X->a->automaton->nb_states;
}

			/* --------------- */

static int
s_file_exists (const char *filename)
{
  FILE *f = fopen (filename, "r");
  if (f == NULL)
    return 0;
  fclose (f);

  return 1;
}

			/* --------------- */

#define bufsize 100

void 
GENEPI_PLUGIN_FUNC(display_data_structure) (genepi_plugin_impl *plugin, 
					    genepi_set_impl *X, FILE *output)
{
  int i;
  char buf[bufsize];
  char filename[bufsize];
  
  assert (X->a != NULL);

  for (i = 0; i < bufsize; i++)
    {
      sprintf (filename, "_tmpauto_%d", i);
      if (!s_file_exists (filename))
	break;
    }

  if (i < bufsize)
    {      
      FILE *in;
      
      auto_serialize_write_dot_file (X->a->automaton, filename, LASH_EXP_DIGIT);
      in = fopen (filename, "r");
      if (in != NULL)
	{
	  int done = 0;
	  while (!done)
	    {
	      int readed = fread (buf, sizeof (char), bufsize, in);
	      if (readed > 0)
		fwrite (buf, sizeof (char), readed, output);
	      else
		done = 1;
	    }
	  fflush (output);
	  fclose (in);	  
	}
      unlink (filename);
    }  
}

			/* --------------- */

static int
s_get_one_solution_rec_msdf (automaton *A, int basis, int s, int varindex, 
			     int *x, 
			     int x_size, int *visited, int sign)
{
  int result;

  if (auto_accepting_state (A, s))
    result = 1;
  else if (visited[s])
    result = 0;
  else
    {
      int succsign = (varindex + 1 < x_size) && sign;
      int succindex = (varindex + 1) % x_size;
      state *ps = A->states+s;
      uint4 ti;
      
      visited[s] = 1;
      result = 0;
      for (ti = 0; ti < ps->nb_trans && !result; ti++)
	{
	  tran *t = ps->trans+ti;
	  int succ = auto_transition_dest (t);
	  uint1 *b = auto_transition_label_ptr(t, A->alphabet_nbytes);

	  if (sign)
	    x[varindex] = -(*b);
	  else
	    x[varindex] = (x[varindex] * basis + (*b));
	  result = s_get_one_solution_rec_msdf (A, basis, succ, succindex, x, 
						x_size, visited, succsign);
	  if (!result)
	    {
	      if (sign)
		x[varindex] = 0;
	      else
		x[varindex] /= basis;
	    }
	}
    }

  return result;
}

			/* --------------- */

struct path {
  int bit;
  struct path *next;
};


static int
s_get_one_solution_rec_lsdf(automaton *A, int basis, int s, int varindex, 
			    int *x, int x_size, int *visited, 
			    struct path *path)
{
  int result;

  if (auto_accepting_state (A, s))
    {
      int i;
      struct path *sign = path;
      assert (varindex == 0);

      /* skip sign bit */
      for (i = x_size - 1; i >=0; i--)
	path = path->next;
      i = x_size - 1;
      while (path != NULL)
	{
	  x[i] = (x[i] * basis)+path->bit;
	  i--;
	  if (i < 0)
	    i = x_size - 1;
	  path = path->next;
	}
      for (i = x_size - 1; i >=0; i--, sign = sign->next)
	{
	  if (sign->bit)
	    x[i] *= -1;
	}

      result = 1;
    }
  else if (visited[s])
    result = 0;
  else
    {
      int succindex = (varindex + 1) % x_size;
      state *ps = A->states+s;
      uint4 ti;
      struct path bit;

      bit.next = path;
      visited[s] = 1;
      result = 0;

      for (ti = 0; ti < ps->nb_trans && !result; ti++)
	{
	  tran *t = ps->trans+ti;
	  int succ = auto_transition_dest (t);
	  uint1 *b = auto_transition_label_ptr(t, A->alphabet_nbytes);

	  bit.bit = *b;
	  result = s_get_one_solution_rec_lsdf (A, basis, succ, succindex, x, 
						x_size, visited, &bit);
	}
    }

  return result;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC (get_one_solution) (genepi_plugin_impl *plugin, 
				       genepi_set_impl *X, int *x, int x_size)
{
  int i;
  automaton *A;
  int *visited;
  int result = 0;
  uint4 is;

  assert (X->a != NULL);

  if (ndd_is_empty (X->a))
    return 0;

  A = X->a->automaton;
  visited = (int *) calloc (sizeof (int), A->nb_states);
  for (i = 0; i < x_size; i++)
    x[i] = 0;
  for (i = 0; i < A->nb_states; i++)
    visited[i] = 0;

  if (plugin->msdf)
    {
      for (is = 0; is < A->nb_i_states && result == 0; is++)
	result = s_get_one_solution_rec_msdf (A, X->a->base, A->i_states[is], 
					      0, x, x_size, visited, 1);
    }
  else
    {
      for (is = 0; is < A->nb_i_states && result == 0; is++)
	result = s_get_one_solution_rec_lsdf (A, X->a->base, A->i_states[is], 
					      0, x, x_size, visited, NULL);
    }
  free (visited);

  return 1;
}
