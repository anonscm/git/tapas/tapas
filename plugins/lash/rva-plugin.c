/*
 * rva-plugin.c -- the RVA plugin
 * 
 * This file is a part of the LASH-based plugins for GENEPI. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <rva.h>
#include <lash-auto-io.h>
#include <diag.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <genepi/genepi-plugin.h>

			/* --------------- */

struct genepi_plugin_impl_st
{
  int basis;
};

struct genepi_set_impl_st
{
  rva *a;
  int refcount;
  int dim;
};

static const char *CONF_BASIS = "decomposition_basis";
static int lash_init_count = 0;


			/* --------------- */

genepi_plugin_impl *
GENEPI_PLUGIN_FUNC(init) (genepi_plugconf *conf)
{
  genepi_plugin_impl *result;

  if (lash_init () < 0 && lash_errno != LASH_ERR_ALREADY)
    {
      fprintf (stderr, "LASH init error:");
      switch (lash_errno)
	{
	case LASH_ERR_BAD_ARCH: 
	  fprintf (stderr, "architecture not supported.");
	  break;
	case LASH_ERR_IN_USE:
	  fprintf (stderr, "internal inconsistency.");
	  break;
	case LASH_ERR_LOGFILE:
	  fprintf (stderr, "cannot create logfile.");
	  break;
	};
      fprintf (stderr, "\n");

      return NULL;
    }

  lash_init_count++;
  result = malloc (sizeof *result);
  result->basis = genepi_plugconf_get_unsigned_int (conf, CONF_BASIS, 2);

  return result;
}

			/* --------------- */

void
GENEPI_PLUGIN_FUNC(terminate) (genepi_plugin_impl *plugin)
{
  assert (lash_init_count > 0);
  lash_init_count--;
  if (lash_init_count == 0)
    lash_end ();
  free (plugin);
}

			/* --------------- */

genepi_solver_type
GENEPI_PLUGIN_FUNC(get_solver_type) (genepi_plugin_impl *plugin)
{
  return GENEPI_R_SOLVER;
}


			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(add_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl * X)
{
  X->refcount++;
  return X;
}

			/* --------------- */

void
GENEPI_PLUGIN_FUNC(del_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl * X)
{
  X->refcount--;
  if (X->refcount == 0)
    {
      rva_free (X->a);
      free (X);
    }
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(top) (genepi_plugin_impl *plugin, int n)
{
  genepi_set_impl *R = malloc (sizeof (genepi_set_impl));

  R->refcount = 1;
  R->dim = n;
  R->a = rva_create_rn (plugin->basis, n);

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(top_Z) (genepi_plugin_impl *plugin, int n)
{
  genepi_set_impl *R = malloc (sizeof (genepi_set_impl));

  R->refcount = 1;
  R->dim = n;
  R->a = rva_create_zn (plugin->basis, n);

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(bot) (genepi_plugin_impl *plugin, int n)
{
  genepi_set_impl *R = malloc (sizeof (genepi_set_impl));

  R->refcount = 1;
  R->dim = n;
  R->a = rva_create_empty (plugin->basis, n);

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(linear_operation) (genepi_plugin_impl *plugin, 
				      const int *calpha, int alpha_size, 
				      genepi_comparator op, int c)
{
  genepi_set_impl *R;
  int *alpha = calloc (sizeof (int), alpha_size);

  {
    int i;

    for (i = 0; i < alpha_size; i++)
      alpha[i] = calpha[i];
  }

  R = malloc (sizeof (genepi_set_impl));
  R->refcount = 1;
  R->dim = alpha_size;
  if (op == GENEPI_EQ)
    R->a = rva_create_equation (plugin->basis, alpha_size, alpha, c);
  else if (op == GENEPI_LEQ)
    R->a = rva_create_inequation (plugin->basis, alpha_size, alpha, c);
  else if (op == GENEPI_GEQ)
    {
      int *negalpha = calloc (sizeof (int), alpha_size);
      int i;
      for (i = 0; i < alpha_size; i++)
	negalpha[i] = -alpha[i];
      R->a = rva_create_inequation (plugin->basis, alpha_size, negalpha, -c);
      free (negalpha);
    }
  else if (op == GENEPI_LT)
    { 
      rva *leq = rva_create_inequation (plugin->basis, alpha_size, alpha, c);
      rva *eq =  rva_create_equation (plugin->basis, alpha_size, alpha, c);
      R->a = rva_difference (leq, eq);
      rva_free (leq);
      rva_free (eq);
    }
  else
    {
      int i;
      int *negalpha = calloc (sizeof (int), alpha_size);
      rva *geq;
      rva *eq;

      assert (op == GENEPI_GT);
      for (i = 0; i < alpha_size; i++)
	negalpha[i] = -alpha[i];

      geq = rva_create_inequation (plugin->basis, alpha_size, negalpha, -c);
      eq =  rva_create_equation (plugin->basis, alpha_size, negalpha, -c);
      R->a = rva_difference (geq, eq);
      rva_free (geq);
      rva_free (eq);
      free (negalpha);
    }
  free (alpha);

  return R;
}

		       /* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_union) (genepi_plugin_impl *plugin, 
			       genepi_set_impl *X1, genepi_set_impl *X2)
{
  genepi_set_impl *R;

  assert (X1->dim == X2->dim);

  R = malloc (sizeof (genepi_set_impl));
  R->refcount = 1;
  R->dim = X1->dim;
  R->a = rva_union (X1->a, X2->a);
  assert (R->a != NULL);

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_intersection) (genepi_plugin_impl *plugin, 
				      genepi_set_impl *X1, 
				      genepi_set_impl *X2)
{
  genepi_set_impl *R;

  assert (X1->dim == X2->dim);

  R = malloc (sizeof (genepi_set_impl));
  R->refcount = 1;
  R->dim = X1->dim;
  R->a = rva_intersection (X1->a, X2->a);
  assert (R->a != NULL);

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_complement) (genepi_plugin_impl *plugin, 
				    genepi_set_impl *X)
{
  genepi_set_impl *R;
  genepi_set_impl *Top;

  R = malloc (sizeof (genepi_set_impl));
  R->refcount = 1;
  R->dim = X->dim;
  Top = GENEPI_PLUGIN_FUNC(top) (plugin, X->dim);
  R->a = rva_difference (Top->a, X->a);
  GENEPI_PLUGIN_FUNC(del_reference) (plugin, Top);

  assert (R->a != NULL);

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(project) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
			     const int *selection, int size)
{
  int dim;
  int i;
  genepi_set_impl *R;
  rva *a;

  assert (X->dim == size);

  dim = 0;
  for (i = 0; i < size; i++)
    if (selection[i] == 0)
      dim++;

  R = malloc (sizeof (genepi_set_impl));
  R->refcount = 1;
  R->dim = dim;
  a = rva_copy (X->a);
  for (i = size - 1; i >= 0; i--)
    if (selection[i] == 1)
      {
	rva *b = rva_projection (a, i);
	rva_free (a);
	a = b;
      }
  R->a = a;
  assert (R->a != NULL);

  return R;
}

			/* --------------- */


static genepi_set_impl *
s_invproject_single (genepi_set_impl *X, int pos)
{
  genepi_set_impl *R;
  int dim = X->dim;
  rva *a;

  /* As it is not possible to add a single variable, we need to multiply by 2 
     the number of variables and then project away all the variables expect the
     interesting one. */

  if (pos == 0)
    {
      int i;
      a = rva_interleave_rn (X->a, 0, 1);

      for (i = dim - 1; i >= 1; i--)
	{
	  rva *b = rva_projection (a, 2 * i);
	  rva_free (a);
	  a = b;
	}
    }
  else
    {
      int i;
      a = rva_interleave_rn (X->a, 1, 0);

      for (i = dim - 1; i >= 0; i--)
	if (i != pos - 1)
	  {
	    rva *b = rva_projection (a, 2 * i + 1);
	    rva_free (a);
	    a = b;
	  }
    }
  dim++;

  R = malloc (sizeof (genepi_set_impl));
  R->refcount = 1;
  R->dim = dim;
  R->a = a;
  assert (R->a != NULL);

  return R;
}



genepi_set_impl *
GENEPI_PLUGIN_FUNC(invproject) (genepi_plugin_impl *plugin, genepi_set_impl *X,
				const int *selection, int size)
{
  int dim;
  int i;
  int nr = 0;
  int nl = 0;
  genepi_set_impl *R;

  dim = 0;
  for (i = 0; i < size; i++)
    if (selection[i] == 0)
      dim++;

  assert (X->dim == dim);

  /* we check if selection has the form 0,1,0,1,0,1... */
  if ((size > 0) && (size % 2 == 0))
    {
      if (selection[0] == 0)
	{
	  int i;
	  nl = 1;

	  for (i = 0; (i < size / 2) && (nl != 0); i++)
	    if ((selection[2 * i] != 0) || (selection[2 * i + 1] != 1))
	      nl = 0;
	}
      else
	{
	  int i;
	  nr = 1;

	  for (i = 0; (i < size / 2) && (nr != 0); i++)
	    if ((selection[2 * i] != 1) || (selection[2 * i + 1] != 0))
	      nr = 0;
	}
    }

  if ((nl != 0) || (nr != 0))
    {
      R = malloc (sizeof (genepi_set_impl));
      R->a = rva_interleave_rn (X->a, nl, nr);
      R->refcount = 1;
      R->dim = size;
    }
  else
    {
      R = X;
      GENEPI_PLUGIN_FUNC(add_reference) (plugin, R);
      for (i = 0; i < size; i++)
	if (selection[i] == 1)
	  {
	    genepi_set_impl *S = s_invproject_single (R, i);
	    GENEPI_PLUGIN_FUNC(del_reference) (plugin, R);
	    R = S;
	  }
    }

  assert (R->a != NULL);

  return R;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(compare) (genepi_plugin_impl *plugin, 
			     genepi_set_impl *X1, genepi_comparator op,
			     genepi_set_impl *X2)
{
  assert (X1->dim == X2->dim);

  if (op == GENEPI_EQ)
    return rva_equality (X1->a, X2->a);
  else if (op == GENEPI_LEQ)
    return rva_inclusion (X1->a, X2->a);
  else if (op == GENEPI_GEQ)
    return rva_inclusion (X2->a, X1->a);
  else if (op == GENEPI_LT)
    return (rva_inclusion (X1->a, X2->a) && (!rva_equality (X1->a, X2->a)));
  else
    {
      assert (op == GENEPI_GT);
      return (rva_inclusion (X2->a, X1->a) && (!rva_equality (X2->a, X1->a)));
    }
}

			/* --------------- */


int
GENEPI_PLUGIN_FUNC(is_empty) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return rva_is_empty (X->a);
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(get_width) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return X->dim;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(get_data_structure_size) (genepi_plugin_impl *plugin, 
					     genepi_set_impl *X)
{
  return X->a->automaton->nb_states;
}

			/* --------------- */

static int
s_file_exists (const char *filename)
{
  FILE *f = fopen (filename, "r");
  if (f == NULL)
    return 0;
  fclose (f);

  return 1;
}

			/* --------------- */

#define bufsize 100

void 
GENEPI_PLUGIN_FUNC(display_data_structure) (genepi_plugin_impl *plugin, 
					    genepi_set_impl *X, FILE *output)
{
  int i;
  char buf[bufsize];
  char filename[bufsize];
  
  for (i = 0; i < bufsize; i++)
    {
      sprintf (filename, "_tmpauto_%d", i);
      if (!s_file_exists (filename))
	break;
    }

  if (i < bufsize)
    {      
      FILE *in;
      
      auto_serialize_write_dot_file (X->a->automaton, filename, LASH_EXP_DIGIT);
      in = fopen (filename, "r");
      if (in != NULL)
	{
	  int done = 0;
	  while (!done)
	    {
	      int readed = fread (buf, sizeof (char), bufsize, in);
	      if (readed > 0)
		fwrite (buf, sizeof (char), readed, output);
	      else
		done = 1;
	    }
	  fflush (output);
	  fclose (in);	  
	}
      unlink (filename);
    }  
}

			/* --------------- */

typedef struct word_bit_st
{
  int bit;
  int state;
  struct word_bit_st *next;
  struct word_bit_st *prev;
} word_bit;

			/* --------------- */

static int
s_eval_words (int basis, int s, word_bit *w, int *x, int x_size)
{
  word_bit *word = w;
  int i, l, n;
  int *z_part = calloc (sizeof (int), x_size);
  int *finite_frac_part = calloc (sizeof (int), x_size);
  int *repeated_frac_part = calloc (sizeof (int), x_size);
  int result = 0;

  /* read bit  sign */
  for (i = 0; i < x_size; i++, word = word->next)
    z_part[i] = word->bit == 0 ? 0 : -1;

  /* read integer part */
  while (word->bit != basis)
    {
      int bit = -1;

      for (i = 0; i < x_size && word->bit != basis; i++, word = word->next)
	{
	  bit = word->bit;
	  z_part[i] = basis * z_part[i] + bit;
	}
      assert (bit != -1);
      for(; i < x_size; i++)
	z_part[i] = basis * z_part[i] + bit;		
    }
  assert (word != NULL);

  /* skip integer/real parts separator */
  word = word->next;
  l = 1; 
  if (word->state != s)
    {
      /* read not repeated part of the fractional part */
      while (word->state != s)
	{
	  int bit = -1;

	  l *= basis;
	  for (i = 0; i < x_size && word->state != s; i++, word = word->next)
	    {
	      bit = word->bit;
	      finite_frac_part[i] = basis * finite_frac_part[i] + bit;
	    }
	  assert (bit != -1);
	  for(; i < x_size; i++)
	    finite_frac_part[i] = basis * finite_frac_part[i] + bit;		
	}      
    }

  assert (word != NULL);
  n = 1;
  while (word != NULL)
    {
      int bit = -1;

      n *= basis;

      for (i = 0; i < x_size && word != NULL; i++, word = word->next)
	{
	  bit = word->bit;
	  repeated_frac_part[i] = basis * repeated_frac_part[i] + bit;
	}
      assert (bit != -1);
      for(; i < x_size; i++)
	repeated_frac_part[i] = basis * repeated_frac_part[i] + bit;		
    }

  assert (n > 1);

  n--;
  result = l * n;
  for (i = 0; i < x_size; i++)
    x[i] = result * z_part[i] + n * finite_frac_part[i] + repeated_frac_part[i];

  free (z_part);
  free (finite_frac_part);
  free (repeated_frac_part);

  return result;
}

			/* --------------- */

#define VISITED  (0x01<<0)
#define ON_STACK (0x01<<1)

static int
s_get_one_solution_rec (automaton *A, int basis, int s, int *x, 
			int x_size, int *flags, word_bit *word)
{
  int result = 0;

  if (auto_accepting_state (A, s) && (flags[s] & ON_STACK) != 0)
    {
      /* go backward to the head of the word */
      while (word->prev)
	word = word->prev;
      result = s_eval_words (basis, s, word, x, x_size);
    }
  else if ((flags[s] & VISITED) == 0)
    {
      word_bit wb;
      state *ps = A->states+s;
      uint4 ti;

      if (word != NULL)
	word->next = &wb;
      wb.prev = word;
      wb.next = NULL;
      wb.state = s;
      result = 0;
      flags[s] = VISITED|ON_STACK;

      for (ti = 0; ti < ps->nb_trans && !result; ti++)
	{
	  tran *t = ps->trans + ti;
	  int succ = auto_transition_dest (t);
	  wb.bit = *auto_transition_label_ptr (t, A->alphabet_nbytes);

	  result = s_get_one_solution_rec (A, basis, succ, x, x_size, flags, 
					   &wb);
	}

      if (word != NULL)
	word->next = NULL;
      flags[s] = VISITED;
    }

  return result;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC (get_one_solution) (genepi_plugin_impl *plugin, 
				       genepi_set_impl *X, int *x, int x_size)
{
  int i;
  automaton *A;
  int *flags;
  int result = 0;
  uint4 is;

  assert (X->a->automaton->word_type == AUTO_WORDS_INFINITE);
  assert (X->a->automaton->accept_type == AUTO_ACCEPT_WEAK);

  if (rva_is_empty (X->a))
    return 0;

  A = X->a->automaton;
  flags = (int *) calloc (sizeof (int), A->nb_states);

  for (i = 0; i < x_size; i++)
    x[i] = 0;

  for (i = 0; i < A->nb_states; i++)
    flags[i] = 0;

  for (is = 0; is < A->nb_i_states && result == 0; is++)
    result = s_get_one_solution_rec (A, plugin->basis, A->i_states[is], x, 
				     x_size, flags, NULL);
  assert (result != 0);

  free (flags);

  return result;
}

