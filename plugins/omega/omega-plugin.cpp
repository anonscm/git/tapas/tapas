/*
 * omega-plugin.cpp -- Implementation of the OMEGA plugin for GENEPI
 * 
 * This file is a part of the GENEPI plugin based on OMEGA. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <omega.h>
#include <stdlib.h>
#include <assert.h>
#include <genepi/genepi-plugin.h>
#include <fstream>
using namespace std;

			/* --------------- */

struct genepi_plugin_impl_st {
  genepi_plugconf *conf;
};

struct genepi_set_impl_st {
  Relation S;
  int number_reference;
  int dim;
};

			/* --------------- */

namespace omega {
  void Exit(int code) {
    exit (code);
  }
};

			/* --------------- */

genepi_plugin_impl *
GENEPI_PLUGIN_FUNC(init) (genepi_plugconf *conf) {
  genepi_plugin_impl *result = (genepi_plugin_impl *) malloc (sizeof *result);

  result->conf = genepi_plugconf_add_reference (conf);
  setOutputFile (stderr);

  return result;
}

			/* --------------- */

void
GENEPI_PLUGIN_FUNC(terminate) (genepi_plugin_impl *plugin) {
  genepi_plugconf_del_reference (plugin->conf);
  free (plugin);
}

			/* --------------- */

genepi_solver_type
GENEPI_PLUGIN_FUNC(get_solver_type) (genepi_plugin_impl *plugin) {
  return GENEPI_Z_SOLVER;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(add_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X) {
  X->number_reference++;

  return X;
}

			/* --------------- */

void
GENEPI_PLUGIN_FUNC(del_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X) {
  if (--X->number_reference == 0)
    delete X;
}



			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(top) (genepi_plugin_impl *plugin, int n) {
  genepi_set_impl *R = new genepi_set_impl;

  R->S = Relation::True (n);
  R->number_reference = 1;
  R->dim = n;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(bot) (genepi_plugin_impl *plugin, int n) {
  genepi_set_impl *R = new genepi_set_impl;

  R->S = Relation::False (n);
  R->number_reference = 1;
  R->dim = n;

  return R;
}

			/* --------------- */


genepi_set_impl *
GENEPI_PLUGIN_FUNC(linear_operation) (genepi_plugin_impl *plugin, 
				      const int *alpha, int alpha_size, 
				      genepi_comparator op, int cst) {
  genepi_set_impl *R;

  R = new genepi_set_impl;
  R->number_reference = 1;
  R->dim = alpha_size;

  if (op == GENEPI_EQ) {
      R->S = Relation (alpha_size);
      F_And *S_root = R->S.add_and ();
      EQ_Handle lin = S_root->add_EQ ();
      for (int i = 0; i < alpha_size; i++)
	lin.update_coef (R->S.set_var (i + 1), alpha[i]);
      lin.update_const (-cst);
      lin.finalize ();
      R->S.finalize ();
      return R;
  }

  //We modify alpha and cst into alpha_GEQ and cst_GEQ such that op can 
  // be assume to be GENEPI_GEQ
  int i;
  int alpha_GEQ[alpha_size];
  int cst_GEQ;

  if (op == GENEPI_GT) {
    for (i = 0; i < alpha_size; i++)
      alpha_GEQ[i] = alpha[i];
    cst_GEQ = cst + 1;
  } else if (op == GENEPI_LT) {
    for (i = 0; i < alpha_size; i++)
      alpha_GEQ[i] = -alpha[i];
    cst_GEQ = -cst + 1;
  } else if (op == GENEPI_GEQ) {
    for (i = 0; i < alpha_size; i++)
      alpha_GEQ[i] = alpha[i];
    cst_GEQ = cst;
  } else {
    assert (op == GENEPI_LEQ);
    for (i = 0; i < alpha_size; i++)
      alpha_GEQ[i] = -alpha[i];
    cst_GEQ = -cst;
  }

  R->S = Relation (alpha_size);
  F_And *S_root = R->S.add_and ();
  GEQ_Handle lin = S_root->add_GEQ ();

  for (int i = 0; i < alpha_size; i++)
    
    
    lin.update_coef (R->S.set_var (i + 1), alpha_GEQ[i]);
  lin.update_const (-cst_GEQ);
  lin.finalize ();
  R->S.finalize ();

  return R;
}


			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_union) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
			       genepi_set_impl *X2) {
  if (X1->dim != X2->dim) {
    printf ("Dimension not equal in union");
    exit (0);
  }

  genepi_set_impl *R;
  R = new genepi_set_impl;
  R->S = Union (copy (X1->S), copy (X2->S));
  R->S.simplify ();
  R->number_reference = 1;
  R->dim = X1->dim;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_intersection) (genepi_plugin_impl *plugin, 
				      genepi_set_impl *X1, 
				      genepi_set_impl *X2) {
  if (X1->dim != X2->dim) {
    printf ("Dimension not equal in intersection");
    exit (0);
  }

  genepi_set_impl *R;
  R = new genepi_set_impl;
  R->S = Intersection (copy (X1->S), copy (X2->S));
  R->S.simplify ();
  R->number_reference = 1;
  R->dim = X1->dim;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_complement) (genepi_plugin_impl *plugin, 
				    genepi_set_impl *X) {
  genepi_set_impl *R;

  R = new genepi_set_impl;
  R->S = Complement (copy (X->S));
  R->S.simplify ();
  R->number_reference = 1;
  R->dim = X->dim;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(project) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
			     const int *selection, int size) {
  assert (X->dim == size);

  genepi_set_impl *R;
  R = new genepi_set_impl;
  R->number_reference = 1;

  R->S = copy (X->S);
  Mapping map (size, 0);
  int pos = 0;  
  for (int i = 0; i < size; i++)
    if (selection[i] == 1)  {
	map.set_map (Input_Var, i + 1, Exists_Var, -1);
    } else {
      pos++;
      map.set_map (Input_Var, i + 1, Input_Var, pos);
    }

  MapRel1 (R->S, map, Comb_Id, pos, 0);
  R->S.simplify ();
  R->dim = pos;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(invproject) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
				const int *selection, int size) {
  int dim;
  int i;
  dim = 0;
  for (i = 0; i < size; i++)
    if (selection[i] == 0)
      dim++;
  if (X->dim != dim) {
    fprintf (stderr, "Dimension not equal in invprojection\n"
	     "Xdim=%d dim=%d\n", X->dim, dim);
    X->S.print ();
    fprintf (stderr, "\n");
    exit (0);
  }

  genepi_set_impl *R;
  R = new genepi_set_impl;

  R->S = copy (X->S);
  Mapping map (dim, 0);
  dim = 0;
  for (i = 0; i < size; i++)
    if (selection[i] == 0) {
      dim++;
      map.set_map (Input_Var, dim, Input_Var, i + 1);
    }

  MapRel1 (R->S, map, Comb_Id, size, 0);
  R->S.simplify ();
  R->number_reference = 1;
  R->dim = size;
  return R;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(is_empty) (genepi_plugin_impl *plugin, genepi_set_impl * X)
{
  return (!(copy (X->S).is_satisfiable ()));
}


			/* --------------- */

int
GENEPI_PLUGIN_FUNC(is_full) (genepi_plugin_impl *plugin, genepi_set_impl * X)
{
  return (!(Complement (copy (X->S))).is_satisfiable ());
}


			/* --------------- */


int
GENEPI_PLUGIN_FUNC(compare) (genepi_plugin_impl *plugin, genepi_set_impl *X1,
			     genepi_comparator op, genepi_set_impl * X2)
{
  assert (X1->dim == X2->dim);

  if (op == GENEPI_EQ)
    return (!(Difference (copy (X1->S), copy (X2->S)).is_satisfiable ()))
      &&   (!(Difference (copy (X2->S), copy (X1->S)).is_satisfiable ()));
      /*

(Must_Be_Subset (copy (X1->S), copy (X2->S))
&& Must_Be_Subset (copy (X2->S), copy (X1->S)));*/
  else if (op == GENEPI_LEQ)
    return Must_Be_Subset (copy (X1->S), copy (X2->S));
  else if (op == GENEPI_GEQ)
    return Must_Be_Subset (copy (X2->S), copy (X1->S));
  else if (op == GENEPI_LT)
    return (Must_Be_Subset (copy (X1->S), copy (X2->S))
	    && (!Must_Be_Subset (copy (X2->S), copy (X1->S))));
  else
    {
      assert (op == GENEPI_GT);
      return (Must_Be_Subset (copy (X2->S), copy (X1->S))
	      && (!Must_Be_Subset (copy (X1->S), copy (X2->S))));
    }
}




			/* --------------- */
static void
s_display_varnames (FILE *output, int size, const char * const *varnames)
{
  if (varnames == NULL)
    fprintf (output, "x");
  else {
    if (size > 1)
      fprintf (output, "(");
    fprintf (output, "%s", varnames[0]);
    for (int i = 1; i < size; i++) {
      fprintf (output, ", %s", varnames[i]);
    }
    if (size > 1)
      fprintf (output, ")");
  }
}

static void
s_display_set_header (const char *prefix, FILE *output, int size, 
		      const char * const *varnames)
{
  fprintf (output, "%s := { ", prefix);
  s_display_varnames (output, size, varnames);
  if (size == 1)
    fprintf (output, " in int | ");
  else
    fprintf (output, " in int%d | " , size);
}

			/* --------------- */

static void
s_display_var (FILE *output, int i, int size, const char * const *varnames)
{
  assert (0 <= i && i < size);
  if (varnames == NULL)
    {
      if (size == 1)
	fprintf (output, "x");
      else
	fprintf (output, "x[%d]", i);
    }
  else
    fprintf (output, "%s", varnames[i]);
}

static void
s_display_formula (genepi_plugin_impl *plugin, genepi_set_impl *X, 
		   FILE *output, const char * const *varnames)
{
  fprintf (output,"let\n");
  if (X->dim > 1)
    {
      fprintf (output," int%d := (", X->dim);
      
      for (int i = 0; i < X->dim; i++)
	{
	  fprintf (output, "int");
	  if (i + 1 < X->dim)
	    fprintf (output, ", ");
	}
      fprintf (output, ");\n");
    }

  int ndisjunct=0;
  for (DNF_Iterator di ( X->S.query_DNF ()) ; di ; di++) {
    fprintf (output, " S%d := let\n" , ndisjunct);
    int firstand;
    
    s_display_set_header ("    E", output, X->dim, varnames);

    firstand=1;
    for(EQ_Iterator ei= (*di)->EQs() ; ei ; ei++)
      {
	// We check if the contraint contains wildcard variables (modular constraint)
	int wildcard=0;
	for(Constr_Vars_Iter cvi(*ei) ; cvi ;cvi++)
	  if ((*cvi).var->kind()==Wildcard_Var)
	    wildcard=1;

	if (!wildcard) 
	  {
	    if (firstand)
	      firstand=0;
	    else
	      fprintf(output," and ");	  	
	    {
	      int possomething=0;
	      //We first write positive coefficients
	      for(Constr_Vars_Iter cvi(*ei) ; cvi ;cvi++)
		if ((*cvi).coef>0)
		  {
		    if (possomething!=0)
		      fprintf(output,"+ ");
		    possomething=1;
		    if ((*cvi).coef!=1)
		      fprintf(output,"%d*",(*cvi).coef);
		    s_display_var (output, (*cvi).var->get_position()-1,
				   X->dim, varnames);
		  }
	      if ((*ei).get_const()>0)
		{
		  if (possomething!=0)
		    fprintf(output,"+ ");
		  possomething=1;
		  fprintf(output, "%d ",(*ei).get_const());
		}
	      if (possomething==0)
		fprintf(output,"0 ");
	    }
	    
	    fprintf(output,"= ");
	    
	    {
	      int negsomething=0; 
	      //Next, we write negative coefficients
	      for(Constr_Vars_Iter cvi(*ei) ; cvi ;cvi++)
		if (-(*cvi).coef>0)
		  {
		    if (negsomething!=0)
		      fprintf(output,"+ ");
		    negsomething=1;
		    if (-(*cvi).coef!=1)
		      fprintf(output,"%d*",-(*cvi).coef);
		    s_display_var (output, (*cvi).var->get_position()-1,
				   X->dim, varnames);
		  }
	      if (-(*ei).get_const()>0)
		{
		  if (negsomething!=0)
		    fprintf(output,"+ ");
		  negsomething=1;
		  fprintf(output, "%d ",-(*ei).get_const());
		}
	      if (negsomething==0)
		fprintf(output,"0 ");
	    }
	  }
      }
    if (firstand)
      fprintf (output,"true");
    fprintf(output," };\n");

    s_display_set_header ("    M", output, X->dim, varnames);
    firstand=1;
    for(EQ_Iterator ei= (*di)->EQs() ; ei ; ei++)
      {
	// We check if the contraints contains wildcard variables (modular constraint)
	int wildcard=0;
	for(Constr_Vars_Iter cvi(*ei) ; cvi ;cvi++)
	  if ((*cvi).var->kind()==Wildcard_Var)
	    wildcard=1;
	if (wildcard) 
	  {
	    if (firstand)
	      firstand=0;
	    else
	      fprintf(output," and ");	
	    {
 	      //We first write non-wildcard coefficients
	      int something=0;
	      for(Constr_Vars_Iter cvi(*ei) ; cvi ;cvi++)
		if ((*cvi).var->kind()!=Wildcard_Var) 
		  {
		    if ((something!=0) && ((*cvi).coef>0))
		      fprintf(output,"+ ");
		    something=1;
		    if ((*cvi).coef==-1)
		      fprintf(output,"-");
		    else
		      if ((*cvi).coef!=1)
			fprintf(output,"%d*",(*cvi).coef);
		    s_display_var (output, (*cvi).var->get_position()-1,
				   X->dim, varnames);
		  }
	      if ((*ei).get_const()!=0)
		{
		  if ((something!=0) && ((*ei).get_const()>0))
		    fprintf(output,"+ ");
		  something=1;
		  fprintf(output, "%d ",(*ei).get_const());
		}
	      else if (something==0) 
		{
		  fprintf(output,"0 ");
		  something=1;
		}
	    }
	    
	    fprintf(output," in ");
	    
	    {
	      //We write wildcard coefficients
	      int wildsomething=0;
	      for(Constr_Vars_Iter cvi(*ei) ; cvi ;cvi++)
		if ((*cvi).var->kind()==Wildcard_Var) 
		  {
		    if ((wildsomething!=0))
		      fprintf(output,"+ ");
		    wildsomething=1;
		    int coef=(*cvi).coef;
		    if (coef<0)
		      coef=-coef;
		    if (coef!=1)
		      fprintf(output,"%d*",coef);
		    fprintf(output, "int ");
		  }
	    }
	  }
      }
    if (firstand)
      fprintf (output,"true");
    fprintf(output," };\n");
    
    s_display_set_header ("    I", output, X->dim, varnames);
    firstand=1;
    for(GEQ_Iterator ei= (*di)->GEQs() ; ei ; ei++)
      {
	
	// We check if the contraint contains wildcard variables (modular constraint)
	{
	  int wildcard=0;
	  for(Constr_Vars_Iter cvi(*ei) ; cvi ;cvi++)
	    if ((*cvi).var->kind()==Wildcard_Var)
	      wildcard=1;
	  assert(wildcard==0);
	}
	
	if (firstand)
	  firstand=0;
	else
	  fprintf(output," and ");
	{
	  int possomething=0;
	  //We first write positive coefficients
	  for(Constr_Vars_Iter cvi(*ei) ; cvi ;cvi++)
	    if ((*cvi).coef>0)
	      {
		if (possomething!=0)
		  fprintf(output,"+ ");
		possomething=1;
		if ((*cvi).coef!=1)
		  fprintf(output,"%d*",(*cvi).coef);
		s_display_var (output, (*cvi).var->get_position()-1,
			       X->dim, varnames);
	      }
	  if ((*ei).get_const()>0)
	    {
	      if (possomething!=0)
		fprintf(output,"+ ");
	      possomething=1;
	      fprintf(output, "%d ",(*ei).get_const());
	    }
	  if (possomething==0)
	    fprintf(output,"0 ");
	}
	
	fprintf(output,">= ");
	
	{
	  int negsomething=0; 
	  //Next, we write negative coefficients
	  for(Constr_Vars_Iter cvi(*ei) ; cvi ;cvi++)
	    if (-(*cvi).coef>0)
	      {
		if (negsomething!=0)
		  fprintf(output,"+ ");
		negsomething=1;
		if (-(*cvi).coef!=1)
		  fprintf(output,"%d*",-(*cvi).coef);
		s_display_var (output, (*cvi).var->get_position()-1,
			       X->dim, varnames);
	      }
	  if (-(*ei).get_const()>0)
	    {
	      if (negsomething!=0)
		fprintf(output,"+ ");
	      negsomething=1;
	      fprintf(output, "%d ",-(*ei).get_const());
	    }
	  if (negsomething==0)
	    fprintf(output,"0 ");
	}
      }
    if (firstand)
      fprintf (output,"true");
    fprintf(output," };\n");
    
    fprintf(output," in\n");
    fprintf(output,"  E && M && I;\n"); 
    
    ndisjunct++;
  }
  fprintf(output,"in\n  ");
  for(int i=0 ; i<ndisjunct ; i++)
    {
      fprintf(output,"S%d",i);
      if (i+1<ndisjunct)
	fprintf(output," || ");
    }
  fprintf(output,";\n");
}

			/* --------------- */

void
GENEPI_PLUGIN_FUNC(display_data_structure) (genepi_plugin_impl *plugin, 
					    genepi_set_impl *X, FILE *output)
{
  s_display_formula (plugin, X, output, NULL);
}

			/* --------------- */

void 
GENEPI_PLUGIN_FUNC (display_all_solutions) (genepi_plugin_impl *plugin, 
                                            genepi_set_impl *X, FILE *output,
					    const char * const *varnames)
{
  s_display_formula (plugin, X, output, varnames);
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(get_width) (genepi_plugin_impl *plugin, genepi_set_impl * X)
{
  return X->dim;
}

			/* --------------- */

int
GENEPI_PLUGIN_FUNC(get_data_structure_size) (genepi_plugin_impl *plugin, 
					     genepi_set_impl * X)
{
  int size=0;
  for(DNF_Iterator di( X->S.query_DNF()) ; di ; di++) {
    for(EQ_Iterator ei= (*di)->EQs() ; ei ; ei++)
      size++;
    for(GEQ_Iterator ei= (*di)->GEQs() ; ei ; ei++)
      size++;
  }
  return size;
}

			/* --------------- */


int
GENEPI_PLUGIN_FUNC(is_solution) (genepi_plugin_impl *plugin, 
				 genepi_set_impl *X, const int *x, int x_size,
				 int xden)
{
  if (X->dim != x_size) {
    printf ("Dimension not equal in is_solution");
    exit (0);
  }

  if (x_size == 0)
    return 1;

  Relation r = Relation (x_size);
  F_And *S_root = r.add_and ();
  int j;
  for (j = 0; j < x_size; j++) {
    EQ_Handle lin = S_root->add_EQ ();
    lin.update_coef (r.set_var (j + 1), 1);
    lin.update_const (-x[j]/xden);
  }
  r.finalize ();

  return Must_Be_Subset (r, copy (X->S));
}

			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(get_one_solution) (genepi_plugin_impl *plugin, 
				      genepi_set_impl * X, int *x, int x_size)
{
  int dimension = X->dim;
  if (dimension == 0)
    return 0;

  Relation s = Sample_Solution (copy (X->S));
  if (!(copy (s).is_satisfiable ()))
    return 0;

  int i;
  for (i = 0; i < x_size; i++)
    x[i] = 0;

  for (DNF_Iterator di (s.query_DNF ()); di; di++)
    for (EQ_Iterator ei = (*di)->EQs (); ei; ei++) {
      Constr_Vars_Iter cvi = (*ei);
      x[(*cvi).var->get_position () - 1] = -(*ei).get_const () / (*cvi).coef;
    }

  return 1;
}


			/* --------------- */
