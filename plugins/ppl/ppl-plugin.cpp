/*
 * ppl-plugin.cpp -- PPL plugin implementation
 * 
 * This file is a part of the PPL-based plugin for GENEPI. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <iostream>
#include <fstream>
#include <ppl.hh>
#include <genepi/genepi-plugin.h>


#if PPL_VERSION_MAJOR > 0 || PPL_VERSION_MINOR >= 11
# define GET_ELEMENT(i) ((i)->pointset ())
#else
# define GET_ELEMENT(i) ((i)->element ())
#endif

using namespace Parma_Polyhedra_Library;
using namespace std;
using namespace IO_Operators;

typedef Pointset_Powerset<NNC_Polyhedron> DisjointPoly;

			/* --------------- */

struct genepi_plugin_impl_st {
  genepi_plugconf *conf;
};

struct genepi_set_impl_st {
  DisjointPoly S;
  int number_reference;
  int dim;
};

			/* --------------- */

genepi_plugin_impl *
GENEPI_PLUGIN_FUNC(init) (genepi_plugconf *conf) {
  genepi_plugin_impl *result = (genepi_plugin_impl *) malloc (sizeof *result);

  result->conf = genepi_plugconf_add_reference (conf);

  return result;
}

			/* --------------- */


void
GENEPI_PLUGIN_FUNC(terminate) (genepi_plugin_impl *plugin) {
  genepi_plugconf_del_reference (plugin->conf);
  free (plugin);
}


			/* --------------- */

genepi_solver_type 
GENEPI_PLUGIN_FUNC(get_solver_type) (genepi_plugin_impl *plugin) {
  return GENEPI_R_SOLVER;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(add_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X) {
  X->number_reference++;
  return X;
}

			/* --------------- */


void
GENEPI_PLUGIN_FUNC(del_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X)
{
  if (--X->number_reference == 0)
    delete X;
}
 
			/* --------------- */


genepi_set_impl *
GENEPI_PLUGIN_FUNC(top) (genepi_plugin_impl *plugin, int n)
{
  genepi_set_impl *R = new genepi_set_impl;

  R->S = DisjointPoly (n, UNIVERSE);
  R->number_reference = 1;
  R->dim = n;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_P) (genepi_plugin_impl *plugin, int n)
{
  genepi_set_impl *R = new genepi_set_impl;
  Constraint_System cs;

  for(int i=0 ; i<n ; i++) {
    Variable x (i);
    cs.insert (x >= 0);
  }

  R->S = DisjointPoly (cs);
  R->number_reference = 1;
  R->dim = n;

  return R;
}


			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_R) (genepi_plugin_impl *plugin, int n)
{
  return GENEPI_PLUGIN_FUNC (top) (plugin, n);
}



genepi_set_impl *
GENEPI_PLUGIN_FUNC(bot) (genepi_plugin_impl *plugin, int n)
{
  genepi_set_impl *R = new genepi_set_impl;

  R->S = DisjointPoly (n, EMPTY);
  R->number_reference = 1;
  R->dim = n;

  return R;
}


			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(linear_operation) (genepi_plugin_impl *plugin, 
				      const int *alpha, int alpha_size, 
				      genepi_comparator op, int c)
{
  genepi_set_impl *R = new genepi_set_impl;
  Linear_Expression l;

  for(int i=0 ; i<alpha_size ; i++) {
    Linear_Expression le;
    le += Variable (i);
    le *= alpha[i];
    l = l + le;
  }

  Constraint_System cs;
  switch (op) {
  case GENEPI_EQ:
    cs.insert (l == c);
    break;
  case GENEPI_LT:
    cs.insert (l < c);
    break;
  case GENEPI_GT:
    cs.insert (l > c);
    break;
  case GENEPI_LEQ:
    cs.insert (l <= c);
    break;
  case GENEPI_GEQ: 
    cs.insert(l >= c);
    break;
  }

  R->S = DisjointPoly (cs);
  R->number_reference = 1;
  R->dim = alpha_size;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_union) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
			       genepi_set_impl *X2)
{
  genepi_set_impl *R = new genepi_set_impl;

  R->S = X1->S;
  for (DisjointPoly::iterator i = (X2->S).begin () ; i != (X2->S).end () ; i++ )
    (R->S).add_disjunct (GET_ELEMENT (i));

  (R->S).pairwise_reduce ();
  R->number_reference = 1;
  R->dim = X1->dim;

  return R;
}


			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_intersection) (genepi_plugin_impl *plugin, 
				      genepi_set_impl *X1, genepi_set_impl *X2)
{
  genepi_set_impl *R = new genepi_set_impl;

  R->S = DisjointPoly (X1->dim, UNIVERSE);
  (R->S).intersection_assign (X1->S);
  (R->S).intersection_assign (X2->S);
  (R->S).pairwise_reduce ();
  R->number_reference = 1;
  R->dim = X1->dim;

  return R;
}

			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_complement) (genepi_plugin_impl *plugin, 
				    genepi_set_impl *X)
{
  genepi_set_impl *R = new genepi_set_impl;

  R->S = DisjointPoly (X->dim, UNIVERSE);
  (R->S).difference_assign (X->S);
  (R->S).pairwise_reduce ();  
  R->number_reference = 1;
  R->dim = X->dim;

  return R;
}

			/* --------------- */

/*
class Pproject
{
  int *select;
  int codim;
  int *function;
  
public:
  Pproject(const int *selection, int size)
  {
    function =(int *)malloc(size*sizeof(int));
    select = (int *)malloc(size*sizeof(int));
    codim=0;
    for (int i=0 ; i<size; i++){
      select[i]=selection[i];
      if (selection[i]==0){
	function[i]=codim;
	codim++;
      }
    }
    cerr<<"size="<<size<<endl;
    cerr<<"codim ="<<codim<<endl;
    cerr<<"function="<<endl;
    for(int i=0; i<size; i++){
      cerr<<"f("<<i<<")=";
      if (select[i]==0)
	cerr<<function[i];
      cerr<<endl;
    }
    
  }
  
  ~Pproject()
  {
    free(select);
    free(function);
  }
  
  bool has_empty_codomain() const
  {
    return (codim==0);
  }
  
  dimension_type max_in_codomain() const
  {
    return codim;
  }
  
  bool maps(dimension_type i, dimension_type &j) const
  {
    if (select[i]==1)
      return false;
    j=function[i];
    return true;
  }
};
*/



genepi_set_impl *
GENEPI_PLUGIN_FUNC(project) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
			     const int *selection, int size)
{
  Variables_Set V;
  int dim = 0;

  for(int i = 0 ; i < size ; i++)
    if (selection[i] == 1)
      V.insert (Variable (i));
    else
      dim++;

  genepi_set_impl *R = new genepi_set_impl;
  R->S = X->S;
  (R->S).remove_space_dimensions (V);
  (R->S).pairwise_reduce ();
  //
  //cerr<<"size======"<<size<<endl;
  //for(int i=0; i<size ; i++)
  //  cerr<<"selection("<<i<<")="<<selection[i]<<endl;
  // 
  //Pproject pmaps(selection, size);
  //(R->S).map_space_dimensions(pmaps);
  //(R->S).pairwise_reduce();
  R->number_reference = 1;
  R->dim = dim;

  return R;
}

			/* --------------- */

class Pinvproject
{
  int dim;
  int add;
  int *function;
  
public:
  Pinvproject (const int *selection, int size) {
    function = (int *) malloc (size * sizeof (int *));
    dim = size;
    int codim = 0;
    for (int i = 0 ; i < size; i++)
      if (selection[i] == 0) {
	function[codim] = i;
	codim++;
      }

    add = size-codim;

    for (int i = 0 ; i < size; i++)
      if (selection[i] == 1){
	function[codim] = i;
	codim++;
      }
  }
  
  ~Pinvproject () {
    free (function);
  }
  
  bool has_empty_codomain () const {
    return (dim == 0);
  }
  
  dimension_type max_in_codomain () const {
    return dim - 1;
  }
  
  bool maps (dimension_type i, dimension_type &j) const {
    if ((int) i >= dim)
      return false;
    j = function[i];
    return true;
  }
  
  int added () const {
    return add;
  }
};


			/* --------------- */

genepi_set_impl *
GENEPI_PLUGIN_FUNC(invproject) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
				const int *selection, int size) {
  genepi_set_impl *R = new genepi_set_impl;
  R->S =X->S;
  Pinvproject pmaps (selection,size);
  (R->S).add_space_dimensions_and_embed (pmaps.added ());
  (R->S).map_space_dimensions (pmaps);    
  R->number_reference = 1;
  R->dim = pmaps.max_in_codomain () + 1;
  return R;
}



			/* --------------- */


int 
GENEPI_PLUGIN_FUNC(compare) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
			     genepi_comparator op, genepi_set_impl *X2)
{
  switch (op) {
  case GENEPI_EQ:
    return ((X1->S).geometrically_equals (X2->S));

  case GENEPI_LT:
    return ((!((X1->S).geometrically_equals (X2->S))) &&
	    ((X2->S).geometrically_covers (X1->S)));

  case GENEPI_GT:
    return ((!((X2->S).geometrically_equals (X1->S))) && 
	    ((X1->S).geometrically_covers(X2->S)));

  case GENEPI_LEQ:
    return ((X2->S).geometrically_covers (X1->S));

  case GENEPI_GEQ: 
    return ((X1->S).geometrically_covers (X2->S));
  }

  cerr << "Error in compare function: unknown operator" << endl;
  exit(0);
}

			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(is_empty) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return (X->S).geometrically_equals (DisjointPoly (X->dim, EMPTY));
}


			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(is_full) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return (X->S).geometrically_equals (DisjointPoly (X->dim, UNIVERSE));
}

			/* --------------- */



void 
GENEPI_PLUGIN_FUNC(display_data_structure) (genepi_plugin_impl *plugin, 
					    genepi_set_impl *X, FILE *output)
{
  ostringstream oss;

  oss << (X->S) << endl;
  fprintf (output, oss.str ().c_str ());
}


			/* --------------- */

int 
GENEPI_PLUGIN_FUNC(get_width) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return X->dim;
}


			/* --------------- */


int 
GENEPI_PLUGIN_FUNC(get_data_structure_size) (genepi_plugin_impl *plugin, 
					     genepi_set_impl *X)
{
  ostringstream oss;

  oss << (X->S);

  return (oss.str ().length ());
}

			/* --------------- */

static void
s_display_armoise_proto (genepi_plugin_impl *plugin, genepi_set_impl *X, 
			 FILE *output, const char * const *varnames)
{
  int i;
  const char *opar = X->dim == 1 ? "" : "(";  
  const char *cpar = X->dim == 1 ? "" : ")";  

  fprintf (output, "{ %s", opar);
  for (i = 0; i < X->dim - 1; i++)
    fprintf (output, "%s, ", varnames[i]);
  fprintf (output, "%s%s | ", varnames[i], cpar);
}

			/* --------------- */

static void
s_display_one_disjunct (genepi_plugin_impl *plugin, genepi_set_impl *X, 
			FILE *output, const char * const *varnames,
			DisjointPoly::iterator &P)
{
  const Constraint_System &cs = GET_ELEMENT (P).constraints ();
  Constraint_System::const_iterator i = cs.begin ();
  Constraint_System::const_iterator cs_end = cs.end ();

  s_display_armoise_proto (plugin, X, output, varnames);

  while (i != cs_end)
    {
      long val;
      int first = 1;
      int v;
      const char *rel;
      const Constraint &c = *i;

      for (v = 0; v < X->dim; v++)
	{
	  val = c.coefficient (Variable(v)).get_si ();

	  if (val == 0)
	    continue;

	  if (first)
	    {
	      const char *op = "";

	      if (val < 0)
		{
		  op = "-";
		  val = -val;
		}
	      fprintf (output, "%s", op);
	      if (val != 1)
		fprintf (output, "%ld *" , val);
	      fprintf (output, "%s", varnames[v]);

	      first = 0;
	    }
	  else
	    {
	      const char *op = "+";

	      if (val < 0)
		{
		  op = "-";
		  val = -val;
		}
	      fprintf (output, " %s", op);
	      if (val != 1)
		fprintf (output, " %ld *", val);
	      fprintf (output, " %s", varnames[v]);
	    }
	}

      if (c.is_equality ()) 
	rel = "=";
      else if (c.is_strict_inequality ()) 
	rel = ">";
      else 
	rel = ">=";

      val = -c.inhomogeneous_term ().get_si ();
      fprintf (output, " %s %ld", rel, val);

      i++;
      if (i != cs_end)
	fprintf (output, " and ");
    }
  fprintf (output, " };\n");
}

			/* --------------- */

void 
GENEPI_PLUGIN_FUNC(display_all_solutions) (genepi_plugin_impl *plugin, 
					   genepi_set_impl *X, FILE *output,
					   const char *varnames[])
{
  X->S.omega_reduce ();

  if (X->S.is_bottom () || X->S.is_top ())
    {
      const char *bval = X->S.is_top () ? "true" : "false";

      s_display_armoise_proto (plugin, X, output, varnames);
      fprintf (output, "%s }\n", bval);
    }
  else
    {
      DisjointPoly::iterator i = X->S.begin ();

      if (X->S.size () == 1)
	s_display_one_disjunct (plugin, X, output, varnames, i);
      else
	{	  
	  int pp, p = 1;
	  DisjointPoly::iterator e = X->S.end ();
	  fprintf (output, "let\n");
	  
	  while (i != e)
	    {
	      fprintf (output, "  P%d := ", p++);
	      s_display_one_disjunct (plugin, X, output, varnames, i);
	      fprintf (output, ";\n");
	      i++;
	    }
	  fprintf (output, "in\n  (");
	  for (pp = 1; pp < p - 1; pp++)
	    fprintf (output, "P%d || ", pp);
	  fprintf (output, "P%d);\n", pp);
	}
    }
}

			/* --------------- */
