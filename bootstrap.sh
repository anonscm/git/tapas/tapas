#!/bin/bash 

HERE=$PWD
basedir=$(dirname $0)

if test ! -d ${basedir}/ccl/.git ; then
    git submodule init
fi
git submodule update --remote --checkout


usage() {
    echo "USAGE: bootstrap.sh"
}


TOLOWER="tr '[A-Z]' '[a-z]'"
mycpu=`(uname -p 2>/dev/null || uname -m 2>/dev/null || echo unknown) | \
        ${TOLOWER}`
myos=`uname -s | ${TOLOWER}`

case "$mycpu" in
    unknown|*" "*)
        mycpu=`uname -m | ${TOLOWER}` ;;
esac

if test "x${LIBTOOLIZE}" = "x"; 
then
    LIBTOOLIZE=libtoolize
    case ${myos} in
	darwin*)
	    $(which -s glibtoolize) && LIBTOOLIZE=glibtoolize
	    ;;
    esac
fi

if test $# != 0
then
    case "$1" in
	--help) usage; exit 0;; 	
	*) echo "$0: error: invalid argument $1" 1>&2; exit 1;;
    esac
fi


#
# GENEPI Bootstrap
#
echo "running autotools reconfiguration..."
rm -fr libltdl
LIBTOOLIZE="${LIBTOOLIZE} --force --ltdl -i" autoreconf -f -i . 
exec autoreconf -f -i libltdl
