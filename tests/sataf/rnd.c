/*
 * rnd.c -- add a comment about this file
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#define THE_m ((int) 2147483647)
#define THE_a ((int) 16807)
#define THE_q ((int) 127773)
#define THE_r ((int) 2836)

static int THE_SEED = 1234567891;

int
rnd_modulo (int modvalue)
{
  THE_SEED = THE_a * (THE_SEED % THE_q) - THE_r * (THE_SEED / THE_q);
  if (THE_SEED < 0)
    THE_SEED += THE_m;
  return (THE_SEED % modvalue);
}

			/* --------------- */
double
rnd_probability (void)
{
  double result;

  THE_SEED = THE_a * (THE_SEED % THE_q) - THE_r * (THE_SEED / THE_q);
  if (THE_SEED < 0)
    THE_SEED += THE_m;
  result = (((double) THE_SEED) / ((double) THE_m));

  return result;
}

			/* --------------- */

void
rnd_set_seed (int seed)
{
  THE_SEED = seed;
}

			/* --------------- */

int
rnd_get_seed (void)
{
  return THE_SEED;
}

			/* --------------- */
