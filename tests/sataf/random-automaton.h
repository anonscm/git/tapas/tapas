/*
 * random-automaton.h -- add a comment about this file
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __RANDOM_AUTOMATON_H__
# define __RANDOM_AUTOMATON_H__

# include <sataf/sataf.h>

extern sataf_mao *
random_automaton_create (uint32_t size, uint32_t alphabet_size);

extern sataf_ea *
random_exit_automaton_create (uint32_t size, uint32_t alphabet_size);

#endif /* ! __RANDOM_AUTOMATON_H__ */
