/*
 * sataf-tester.c -- Test suite for the SATAF library
 * 
 * This file is a part of the SATAF library. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include "rnd.h"
#include <ccl/ccl-memory.h>
#include <sataf/sataf.h>
#include "random-automaton.h"

typedef struct rnd_automaton_st
{
  sataf_mao super;
  sataf_ea *ea;
  uint32_t s;
} RND_AUTOMATON;


			/* --------------- */

static size_t
s_rnd_size (sataf_mao *self);

static void
s_rnd_destroy (sataf_mao *self);

#define s_rnd_no_cache NULL

#define s_rnd_is_root_of_scc NULL

#define s_rnd_simplify NULL

static uint32_t
s_rnd_get_alphabet_size (sataf_mao *self);

#define s_rnd_to_string NULL

static sataf_mao * 
s_rnd_succ (sataf_mao *self, uint32_t letter);

static int
s_rnd_is_final (sataf_mao *self);

static int
s_rnd_is_equal_to (sataf_mao *self, sataf_mao *other);

static unsigned int
s_rnd_hashcode (sataf_mao *self);

# define s_rnd_to_dot NULL

			/* --------------- */

static const sataf_mao_methods RND_METHODS = 
  {
    "SA-RND",
    s_rnd_size,
    s_rnd_destroy,
    s_rnd_no_cache,
    s_rnd_is_root_of_scc,
    s_rnd_simplify,
    s_rnd_get_alphabet_size,
    s_rnd_to_string,
    s_rnd_succ,
    s_rnd_is_final,
    s_rnd_is_equal_to,
    s_rnd_hashcode,
    s_rnd_to_dot
  };

			/* --------------- */

static RND_AUTOMATON *
s_create_rnd_automaton (sataf_ea *ea, uint32_t s);

static sataf_ea *
s_generate_random_ea (uint32_t size, uint32_t alphabet_size);

			/* --------------- */

sataf_mao * 
random_automaton_create (uint32_t size, uint32_t alphabet_size)
{
  RND_AUTOMATON *R =
    s_create_rnd_automaton (s_generate_random_ea (size, alphabet_size), 0);

  return (sataf_mao *) R;
}

			/* --------------- */

sataf_ea *
random_exit_automaton_create (uint32_t size, uint32_t alphabet_size)
{
  return s_generate_random_ea (size, alphabet_size);
}

			/* --------------- */

static size_t
s_rnd_size (sataf_mao *self)
{
  return sizeof (RND_AUTOMATON);
}

			/* --------------- */

static void
s_rnd_destroy (sataf_mao *self)
{
  RND_AUTOMATON *ra = (RND_AUTOMATON *) self;

  sataf_ea_del_reference (ra->ea);
}

			/* --------------- */

static uint32_t
s_rnd_get_alphabet_size (sataf_mao *self)
{
  RND_AUTOMATON *ra = (RND_AUTOMATON *) self;

  return ra->ea->alphabet_size;
}

			/* --------------- */

static sataf_mao * 
s_rnd_succ (sataf_mao *self, uint32_t a)
{
  RND_AUTOMATON *ra = (RND_AUTOMATON *) self;
  uint32_t succ =
    sataf_ea_decode_succ_state (sataf_ea_get_successor (ra->ea, ra->s, a));

  ra->ea = sataf_ea_add_reference (ra->ea);
  return (sataf_mao *) s_create_rnd_automaton (ra->ea, succ);
}

			/* --------------- */

static int
s_rnd_is_final (sataf_mao *self)
{
  RND_AUTOMATON *ra = (RND_AUTOMATON *) self;

  return ra->ea->is_final[ra->s];
}

			/* --------------- */

static int
s_rnd_is_equal_to (sataf_mao *self, sataf_mao *other)
{
  RND_AUTOMATON *ra = (RND_AUTOMATON *) self;
  RND_AUTOMATON *ra_other = (RND_AUTOMATON *) other;

  return sataf_ea_equals (ra->ea, ra_other->ea) && ra->s == ra_other->s;
}

			/* --------------- */

static unsigned int
s_rnd_hashcode (sataf_mao *self)
{
  RND_AUTOMATON *ra = (RND_AUTOMATON *) self;

  return sataf_ea_hashcode (ra->ea) + 19 * ra->s;
}

			/* --------------- */

static RND_AUTOMATON *
s_create_rnd_automaton (sataf_ea *ea, uint32_t s)
{
  RND_AUTOMATON *result = (RND_AUTOMATON *)
    sataf_mao_create (sizeof (RND_AUTOMATON), &RND_METHODS);

  result->ea = ea;
  result->s = s;

  return result;
}

			/* --------------- */

static void
s_randomize_array (uint32_t * a, uint32_t len)
{
  int i;
  uint32_t t;

  if (len == 0)
    return;
  i = rnd_modulo (len);
  t = a[i];
  a[i] = a[0];
  a[0] = t;
  s_randomize_array (a + 1, len - 1);
}

			/* --------------- */

static uint32_t *
s_rnd_path (uint32_t size)
{
  uint32_t i;
  uint32_t *R = ccl_new_array (uint32_t, size);
  uint32_t *path = ccl_new_array (uint32_t, size);

  for (i = 0; i < size; i++)
    R[i] = i;

  s_randomize_array (R, size);

  for (i = 0; i < size - 1; i++)
    path[R[i]] = R[i + 1];

  path[R[i]] = R[0];
  ccl_delete (R);

  return path;
}

			/* --------------- */

static sataf_ea * 
s_generate_random_ea (uint32_t size, uint32_t alphabet_size)
{
  uint32_t s;
  uint32_t a;
  sataf_ea *ea = sataf_ea_create (size, 0, alphabet_size);
  uint32_t *path = s_rnd_path (size);

  for (s = 0; s < size; s++)
    {
      uint32_t path_letter = rnd_modulo (alphabet_size);
      ea->is_final[s] = rnd_probability () < 0.5;
      for (a = 0; a < alphabet_size; a++)
	{
	  uint32_t succ;

	  if (a == path_letter)
	    succ = path[s];
	  else
	    succ = rnd_modulo (size);

	  sataf_ea_set_successor (ea, s, a, succ, 0);
	}
    }
  ccl_delete (path);

  return ea;
}
