#!/bin/bash

test_file="$1"
cat <<EOF 
let
  X1:=`distiller -s ${test_file}`
  X2:=`cat ${test_file}`
in 
  X1^X2;
EOF
