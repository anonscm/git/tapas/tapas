#!/bin/bash 

here=${PWD}
DISTILLER=${builddir}/tools/distiller/distiller
export DISTILLER

source ${tests_dir}/solvers.sh

function failure_if()
{
    if eval $1; then
	if test "x$2" != "x" ; then
	    echo $2 >> ${logfile}
	fi
	echo "${logfile}:0: error: failed tests; have a look to this file."
	exit 1
    fi
}

function runcmd() 
{
    eval "echo \"$1\"" >> ${logfile}
    eval "$1" 2>> ${logfile}

    return $?
}

function plugin_for_file()
{
    echo "looking for a GENEPI plugin for file $1" >> ${logfile}
    solver_types=`runcmd "${DISTILLER} -x '$1'"`
    echo ${solver_types} >> ${logfile}

    if test "$?" != "0" ; then
	return 1;
    fi
    
    for st in "R" "P" "Z" "N"; do
	if `echo ${solver_types} | grep -q -e "${st}"` ; then
	    case ${st} in
		N) echo "${N_SOLVER}" ;;
		Z) echo "${Z_SOLVER}" ;;
		P) echo "${P_SOLVER}" ;;
		R) echo "${R_SOLVER}" ;;
	    esac
	    break;
	fi
    done

    return 0
}


function check_emptyness()
{
    predicate_file_1="$1"
    predicate_file_2="$2"
    tmpfile="emptyness-$$.arm"
    echo "checking equality of sets defined in ${predicate_file_1} and ${predicate_file_2}" >> ${logfile}
    echo "generating predicate ${tmpfile}" >> ${logfile}
    cat > ${tmpfile} <<EOF
let
   // predicate taken from ${predicate_file_1}
   P1_$$ := `cat ${predicate_file_1}`
   // predicate taken from ${predicate_file_2}
   P2_$$ := `cat ${predicate_file_2}`
in
   P1_$$ ^ P2_$$;
EOF
    cat ${tmpfile} >> ${logfile}

    plugin=`plugin_for_file ${tmpfile}`
    if test "$?" != "0" ; then
	rm -f ${tmpfile}
	failure_if true
    fi

    runcmd '${DISTILLER} -e -q --plugin=${plugin} ${tmpfile}'
    result="$?"
    if test "${result}" != "0"; then
	echo "non empty result !" >> ${logfile}
    fi
    rm -f ${tmpfile}
    return ${result}
}


progname=`echo "$0" | sed 's,^.*/,,'`
testname=`echo "${progname}" | sed 's,[.]test,,'` 
logfile="${here}/${testname}.out"
testkinds="check-solutions batof syntax solver-error domains"

testkind="unknown"
for k in ${testkinds}; do
    if `echo ${testname} | grep -q -e "^$k-.*"`; then
	testkind="$k"
	testname=`echo "${testname}" | sed "s/$k-//g"`
    fi
done

if test "${testkind}" = "unknown"; then
    echo "don't known how to use this file '${progname}'." 
    echo "don't known how to use this file '${progname}'." 1>&2 > ${logfile}
    exit 1
fi

predicate_file="${tests_dir}/${testname}.arm"
expected_error="${srcdir}/${testkind}-${testname}.err"

if test "${testkind}" = "syntax" ; then
    echo "checking ARMOISE syntax interpreter against ${predicate_file} " >> ${logfile}
    echo "${DISTILLER} ${predicate_file}" >> ${logfile}
    error="${here}/${testkind}-${testname}.$$.err"
    output=`cat ${predicate_file} | ${DISTILLER} 2> ${error}`

    if test "x${output}" != "x" ; then
	rm -f "${error}"
	echo "unexpected output of ${DISTILLER} :" >> ${logfile}
	echo "${output}" >> ${logfile}
	echo "$logfile:0: error: failed test; have a look to this file"
	exit 1
    fi

    if cmp -s ${expected_error} ${error}; then
	:
    else
	echo "error output differences:" >> ${logfile}
	echo "diff ${expected_error} ${error}" >> ${logfile}
	diff ${expected_error} ${error} 2>> ${logfile} 1>&2
	rm -f ${error}
	echo "$logfile:0: error: failed test; have a look to this file"
	exit 1
    fi

    rm -f ${error}
fi

if test "${testkind}" = "solver-error" ; then
    echo "checking ALAMBIC/GENEPI solver against ${predicate_file} " >> ${logfile}
    plugin=`plugin_for_file ${predicate_file}`
    failure_if 'test "$?" != "0"'
    cmd="${DISTILLER} -s --plugin=${plugin} ${predicate_file}"
    echo $cmd >> ${logfile}
    error="${here}/${testkind}-${testname}.$$.err"
    output=`$cmd 2> ${error}`

    if test "x${output}" != "x" ; then
	rm -f "${error}"
	echo "unexpected output of ${DISTILLER} :" >> ${logfile}
	echo "${output}" >> ${logfile}
	echo "$logfile:0: error: failed test; have a look to this file"
	exit 1
    fi

    if cmp -s ${expected_error} ${error}; then
	:
    else
	echo "error output differences:" >> ${logfile}
	echo "diff ${expected_error} ${error}" >> ${logfile}
	diff ${expected_error} ${error} 2>> ${logfile} 1>&2
	rm -f ${error}
	echo "$logfile:0: error: failed test; have a look to this file"
	exit 1
    fi

    rm -f ${error}
fi

if test "${testkind}" = "check-solutions" ; then    
    echo "checking ARMOISE/ALAMBIC/GENEPI pipeline against ${predicate_file} " >> ${logfile}
    solution_file="${srcdir}/${testname}-sol.arm"
    runcmd "check_emptyness ${predicate_file} ${solution_file}"
    result="$?"    
    failure_if 'test "${result}" != "0"'
fi

if test "${testkind}" = "batof" ; then
    echo "checking BATOF formula synthetizer against ${predicate_file} " >> ${logfile}
    predicate_solver_type=`runcmd '${DISTILLER} -x ${predicate_file}'`
    failure_if 'test "$?" != "0"'
    
    failure_if 'test "${predicate_solver_type}" != "N"' "can't use prestaf on the predicate ${predicate_file}; it requires a ${predicate_solver_type} solver."
    x_tmpfile="${here}/x$$_${test_name}.tmp"

    runcmd '${DISTILLER} -s --plugin=prestaf ${predicate_file} > ${x_tmpfile}'
    if test "$?" != "0"; then
	rm -f ${x_tmpfile}
	failure_if true
    fi
    runcmd "check_emptyness ${predicate_file} ${x_tmpfile}"
    result="$?"
    rm -f ${x_tmpfile}
    failure_if 'test "${result}" != "0"'
fi

if test "${testkind}" = "domains"; then
    echo "checking computed domains of variables and solver type against ${predicate_file}" >> ${logfile}
    cmd="${DISTILLER} -vx ${predicate_file}"
    expected_result="${srcdir}/${testkind}-${testname}.doms"
    result="${testkind}-${testname}.$$.doms"
    runcmd "$cmd" > ${result}
    if test "$?" != "0"; then
	rm -f ${result}
	exit 1;
    fi

    if cmp -s ${expected_result} ${result}; then
	:
    else
	echo "output differences:" >> ${logfile}
	echo "diff ${expected_result} ${result}" >> ${logfile}
	diff ${expected_result} ${result} 2>> ${logfile} 1>&2
	rm -f ${result}
	echo "$logfile:0: error: failed test; have a look to this file"
	exit 1
    fi

    rm -f ${result}
fi

rm -f ${logfile}
exit 0
