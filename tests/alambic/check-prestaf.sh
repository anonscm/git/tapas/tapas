#!/bin/bash

test_file="$1"
other_plugin="$2"
$PWD/make-checkform.sh "$1" | distiller -e --plugin=${other_plugin}
