MAINTAINERCLEANFILES = 

TESTS_ENVIRONMENT = builddir=${top_builddir} tests_dir=${srcdir}/.. \
                    srcdir=${srcdir}

CLEANFILES = ${TESTS} ${TESTS:%.test=%.log}

${TESTS}: Makefile 
	rm -f $@ ; ${LN_S} ${srcdir}/../alambic-tester.sh $@
	touch $@

