##
##
##
set -xue
progname=`echo "$0" | sed 's,^.*/,,'`
testname=`echo "$progname" | sed 's,[.]test,,'` 
inputfilename="${srcdir}/${testname}.pres"
outputfilename="${srcdir}/${testname}.out"
export PRESTAF_INCLUDE="${srcdir}:${builddir}"
runprestaf="${prestaf} -z"

${runprestaf} ${inputfilename} > ${testname}.out

if cmp -s ${testname}.out ${outputfilename}; then
    result=0
else
    diff ${testname}.out ${outputfilename} 
    result=1
fi

exit $result
