#include <stdio.h>
#include <ccl/ccl-assert.h>
#include <talalib/talalib.h>

static int
s_hnf_test (void);

static void
s_log (ccl_log_type type, const char  *msg, void *data);

			/* --------------- */

int 
main (int argc, char **argv)
{
  int result;

  tla_init ();
  ccl_log_add_listener (s_log, NULL);
  result = s_hnf_test () ? EXIT_SUCCESS : EXIT_FAILURE;
  tla_terminate ();

  return result;
}

			/* --------------- */

static int
s_hnf_test (void)
{
  gmp_randstate_t s;

  gmp_randinit_default (s);
  
  if(1) {
    int size;
    tla_vsp_t Vec;    
    tla_group_t GV;    
    tla_group_t G;    
    tla_vec_t v,lambda;
    tla_matrix_t S;

    printf("Type in a dimension \n");
    scanf("%d", &size);

    tla_group_init(G,size);
    tla_vec_init(v,size);
    tla_vec_init(lambda,size);
    tla_vsp_init(Vec,size);
    tla_group_init(GV,size);
    tla_matrix_init (S, size, size + 1);
    
    while (1) {
      int i;

      printf("new vector [\n");

      for(i=0;i<size;i++){
	int value;
	scanf("%d", &value);
	mpz_set_si(v->num[i],value);
      }
      printf("]\n");
     
      if (tla_group_has(G,v,lambda))
	{
	  printf("The group G already contains the vector\n");
	  printf("lambda=\n");
	  tla_display_vec (CCL_LOG_DISPLAY,lambda);
	  printf("\n");
	}
      else
	{
          printf("The group G does not contain the vector\n");
	  tla_group_add_generator (G, G, v);
	  printf("The hermite matrice is:\n");
	  tla_display_matrix (CCL_LOG_DISPLAY, G->Hermite);
	  
	  printf("The smith matrice is:\n");
	  tla_matrix_set (S, G->Hermite);
	  tla_matrix_compute_snf (S, S, NULL, NULL, NULL, NULL, NULL);
	  tla_display_matrix (CCL_LOG_DISPLAY, S);
	  
	  tla_vsp_add_generator (Vec, Vec, v);
	  printf("Vec(G) /\\ Z^n=\n");
	  tla_display_vsp_equations(CCL_LOG_DISPLAY,Vec);           
	  
	  tla_group_Z_cap_vsp(GV,Vec);
	  printf("The hermite groupe GV is:\n");
	  tla_display_matrix (CCL_LOG_DISPLAY, GV->Hermite);
	} 
    }
    tla_matrix_clear (S);
    tla_group_clear(GV);
    tla_vsp_clear(Vec);
    tla_vec_clear(lambda);
    tla_vec_clear(v);
    tla_group_clear(G);    
  }

  return 1;
}



			/* --------------- */

/*  Check that A=H.U */
/*  where A and H have a size equal to (m, n) */
/*  where U has a size equal to (n, n) */
static void
s_check_hnf_result (tla_matrix_t A, tla_matrix_t H, int *dim, tla_matrix_t U, 
		    tla_matrix_t V)
{
  int i;
  tla_matrix_t tmp;

  /* check if HxU == A */
  tla_matrix_init (tmp, H->m, U->n);
  tla_matrix_mul_matrix (tmp, H, U);

  for (i = 0; i < H->m * H->n; i++)
    {
      if (!tla_matrix_are_equal (tmp, A))
	{
	  tla_display_matrix (CCL_LOG_ERROR, tmp);
	  tla_display_matrix (CCL_LOG_ERROR, H);
	  
	  ccl_assert (tla_matrix_are_equal (tmp, A));
	}
    }


  /* check if UxV = Id */
  tla_matrix_resize (tmp, H->n, H->n);
  tla_matrix_mul_matrix (tmp, U, V);
  ccl_assert (tla_matrix_is_identity (tmp));

  /* check if VxU = Id */
  tla_matrix_mul_matrix (tmp, V, U);
  ccl_assert (tla_matrix_is_identity (tmp));

  tla_matrix_clear (tmp);
}

			/* --------------- */

static void
s_check_snf_result (tla_matrix_t A, tla_matrix_t S, int *dim, tla_matrix_t X, 
		    tla_matrix_t Y, tla_matrix_t U, tla_matrix_t V)
{
  {
    tla_matrix_t UV, VU;

    tla_matrix_init (UV, S->n, S->n);
    tla_matrix_mul_matrix (UV, U, V);
    tla_matrix_init (VU, S->n, S->n);
    tla_matrix_mul_matrix (VU, V, U);
  
    if (! tla_matrix_is_identity (UV) || ! tla_matrix_is_identity (VU))
      {
	ccl_error("U=");
	tla_display_matrix (CCL_LOG_ERROR, U);
	ccl_error("\nV=");
	tla_display_matrix (CCL_LOG_ERROR, V);
	ccl_error("\n");
      }
  
    ccl_assert (tla_matrix_is_identity (UV));
    ccl_assert (tla_matrix_is_identity (VU));
    tla_matrix_clear (UV);
    tla_matrix_clear (VU);
  }
  {
    tla_matrix_t XY, YX;
    tla_matrix_init (XY, S->m, S->m);
    tla_matrix_mul_matrix (XY, X, Y);
    tla_matrix_init (YX, S->m, S->m);
    tla_matrix_mul_matrix (YX, Y, X);
  
    if (! tla_matrix_is_identity (XY) || ! tla_matrix_is_identity (YX))
      {
	ccl_error("X=");
	tla_display_matrix (CCL_LOG_ERROR, X);
	ccl_error("\nY=");
	tla_display_matrix (CCL_LOG_ERROR, Y);
	ccl_error("\n");
      }
  
    ccl_assert (tla_matrix_is_identity (XY));
    ccl_assert (tla_matrix_is_identity (YX));
    tla_matrix_clear (XY);
    tla_matrix_clear (YX);
  }

  {
    tla_matrix_t XS, XSU;

    tla_matrix_init (XS, S->m, S->n);
    tla_matrix_mul_matrix (XS, X, S);
    tla_matrix_init (XSU, S->m, S->n);
    tla_matrix_mul_matrix (XSU, XS, U);
    tla_matrix_clear (XS);
    
    ccl_assert (tla_matrix_are_equal (A, XSU));
    
    tla_matrix_clear (XSU);
  }

  {
    int i, min = S->m < S->n ? S->m : S->n;

    for (i = 2; i <= min && mpz_sgn (TLA_M_NUM (S, i, i)) != 0; i++)      
      ccl_assert (mpz_divisible_p (TLA_M_NUM (S, i, i), 
				   TLA_M_NUM (S, i - 1, i - 1)));
  }
}

			/* --------------- */


static void
s_log (ccl_log_type type, const char  *msg, void *data)
{  
  if (type == CCL_LOG_DISPLAY)
    printf ("%s", msg);
  else
    fprintf (stderr, "%s", msg);
}

