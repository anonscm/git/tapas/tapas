/*
 * armoise-tester.c -- add a comment about this file
 * 
 * This file is a part of the ARMOISE language library
. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#include <ccl/ccl-init.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-exception.h>
#include <armoise/armoise.h>
#include <armoise/armoise-input.h>
#include <armoise/armoise-interp.h>

struct distiller_option
{
  char shortname;
  char *name;
  enum { BOOL_OPT, INT_OPT, STRING_OPT } kind;
  void *p_value;
  void *defvalue;
  char *helpmsg;
};

			/* --------------- */

#define HELP_MSG_TAB 22

			/* --------------- */

static void
s_init (void);

static void
s_terminate (void);

static void
s_usage (ccl_log_type log, const char *cmd);

static void
s_parse_args (int argc, char **argv);

static int
s_compile_stream (FILE *input, const char *filename);

static int
s_compile_file (const char *filename);

			/* --------------- */

static ccl_list *filenames;
static int help_opt;
static int debug_level_opt;
static int types_opt;
static int domains_opt;
static int solver_type_opt;

static struct distiller_option OPTIONS[] = 
  {    
    { 'k', "types", BOOL_OPT, &types_opt, (void *) 0,
      "display the type of the formulas" },
    { 'v', "domains", BOOL_OPT, &domains_opt, (void *) 0,
      "display the domains of variables" },
    { 0, "debug", INT_OPT, &debug_level_opt, (void *) -1,
      "set the CCL debug level" },
    { 'x', "solver-type", BOOL_OPT, &solver_type_opt, (void *) 0,
      "returns the minimal solver type required to solve\n\t\t\tpredicates" },
    { 'h', "help", BOOL_OPT, &help_opt, (void *) 0, "this help message" },
    { 0, NULL }
  };


			/* --------------- */

int 
main (int argc, char **argv)
{
  int exitcode = 0;

  s_init ();
  filenames = ccl_list_create ();
  ccl_try (exception)
    {
      s_parse_args (argc, argv);

      if (help_opt)
	s_usage (CCL_LOG_DISPLAY, argv[0]);
      else
	{
	  int ok = 1;
	  
	  if (debug_level_opt >= 0)
	    ccl_debug_max_level (debug_level_opt);

	  if (ccl_list_is_empty (filenames))
	    ok = s_compile_stream (stdin, "<stdin>");
	  else 
	    {
	      ccl_pair *p;
	      for (p = FIRST (filenames); p; p = CDR (p))
		{
		  char *filename = (char *) CAR(p);
		  ok = s_compile_file (filename);
		}
	    }

	  if (! ok)
	    exitcode = 1;
	}
    }
  ccl_catch
    {
      if (ccl_exception_is_raised (runtime_exception))
	ccl_rethrow ();
      else if (! ccl_exception_is_raised (armoise_interp_exception))
	{
	  ccl_error ("%s\n", ccl_exception_current_message);
	  s_usage (CCL_LOG_ERROR, argv[0]);
	  exitcode = 1;
	}
    }
  ccl_end_try;

  ccl_list_delete (filenames);
  s_terminate ();
  
  return exitcode;
}

			/* --------------- */

static void
s_usage (ccl_log_type log, const char *cmd)
{
  struct distiller_option *opt;

  ccl_log (log, 
	   "USAGE:%s [options] [armoise input filenames]\n"
	   "with options:\n", cmd);
  
  for (opt = &OPTIONS[0]; opt->shortname != 0 || opt->name != NULL; opt++)
    {
      int tab = 0;
      if (opt->shortname != 0)
	{
	  ccl_log (log, "-%c", opt->shortname);
	  tab += 2;
	  if (opt->kind == INT_OPT) 
	    { 
	      ccl_log (log, " <n>"); 
	      tab += 4; 
	    }
	  else if (opt->kind == STRING_OPT) 
	    { 
	      ccl_log (log, " <string>"); 
	      tab += 9; 
	    }
	  if (opt->name != NULL)
	    {
	      ccl_log (log, ", ");
	      tab += 2;
	    }
	}

      if (opt->name != NULL)
	{
	  tab += strlen (opt->name) + 2;
	  ccl_log (log, "--%s", opt->name);
	  if (opt->kind == INT_OPT) 
	    { 
	      ccl_log (log, "=<n>"); 
	      tab += 4; 
	    }
	  else if (opt->kind == STRING_OPT) 
	    { 
	      ccl_log (log, "=<string>"); 
	      tab += 9; 
	    }
	}
      for (; tab < HELP_MSG_TAB; tab++)
	ccl_log (log, " ");
      ccl_log (log, "\t%s", opt->helpmsg);
      if (opt->shortname != 'h')
	{
	  ccl_log (log, " (default=");
	  if (opt->kind == INT_OPT) 
	    ccl_log (log, "%d", (intptr_t) opt->defvalue);
	  else if (opt->kind == STRING_OPT) 
	    ccl_log (log, "%s", (char *) opt->defvalue);
	  else 
	    ccl_log (log, "%s", opt->defvalue ? "true" : "false");
	  ccl_log (log, ")");
	}
      ccl_log (log, ".\n");
    }
}

			/* --------------- */

static void
s_listener (ccl_log_type type, const char *msg, void *data)
{
  if (type == CCL_LOG_DISPLAY)
    fprintf (stdout, "%s", msg);
  else if (type != CCL_LOG_DEBUG || debug_level_opt >= 0)
    fprintf (stderr, "%s", msg);
}

			/* --------------- */

static void
s_init (void)
{
  ccl_init ();
  ccl_log_add_listener (s_listener, NULL);

  armoise_init ();
}

			/* --------------- */

static void
s_terminate (void)
{
  armoise_terminate ();
  ccl_terminate ();
}

			/* --------------- */

static void
s_parse_args (int argc, char **argv)
{
  int i = 0;
  struct distiller_option *opt;

  for (opt = &OPTIONS[0]; opt->shortname != 0 || opt->name != NULL; opt++)
    {
      if (opt->kind == BOOL_OPT || opt->kind == INT_OPT)
	{
	  int *opt_addr = opt->p_value;
	  *opt_addr = (intptr_t) opt->defvalue;
	}
      else 
	{
	  char **opt_addr  = opt->p_value;
	  *opt_addr = opt->defvalue;
	}
    }

  for (i = 1; i < argc; i++)
    {
      if (argv[i][0] == '-')
	{
	  char *s;

	  if (argv[i][1] == '-')
	    {
	      int len;
	      char *optname = argv[i] + 2;

	      for (opt = &OPTIONS[0]; opt->shortname != 0 || opt->name != NULL;
		   opt++)
		{
		  if (opt->name != NULL)
		    {
		      len = strlen (opt->name);
		      if (strncmp (optname, opt->name,len) == 0)
			break;
		    }
		}

	      if (opt->shortname == 0 && opt->name == NULL)
		ccl_throw (exception, "unknown option\n");

	      if (opt->kind == INT_OPT || opt->kind == STRING_OPT)		
		{
		  if (optname[len] != '=' || optname[len + 1] == '\0')
		    {
		      ccl_error ("missing argument to option --%s.\n", 
				 opt->name);
		      ccl_throw_no_msg (exception);
		    }
		  optname += len + 1;
		}

	      if (opt->kind == BOOL_OPT)
		{
		  int *opt_addr = opt->p_value;
		  *opt_addr = ! (intptr_t) opt->defvalue;
		} 
	      else if (opt->kind == INT_OPT)
		{
		  int *opt_addr  = opt->p_value;
		  *opt_addr = atoi (optname);
		}
	      else
		{
		  char **opt_addr  = opt->p_value;
		  *opt_addr = optname;
		}
	    }
	  else
	    {
	      for (s = argv[i] + 1; *s; s++)
		{
		  struct distiller_option *opt;
		  
		  for (opt = &OPTIONS[0]; 
		       opt->shortname != 0 || opt->name != NULL; 
		       opt++)
		    {
		      if (*s == opt->shortname)
			break;
		    }

		  if (opt->shortname == 0 && opt->name == NULL)
		    ccl_throw (exception, "unknown option\n");

		  if ((opt->kind == INT_OPT || opt->kind == STRING_OPT) && 
		      i == argc - 1)
		    {
		      ccl_error ("missing argument to option -%c.\n",
				 opt->shortname);
		      ccl_throw_no_msg (exception);
		    }

		  if (opt->kind == BOOL_OPT)
		    {
		      int *opt_addr = opt->p_value;
		      *opt_addr = ! (intptr_t) opt->defvalue;
		    }
		  else if (opt->kind == INT_OPT)
		    {
		      int *opt_addr  = opt->p_value;
		      *opt_addr = atoi (argv[i+1]);
		      i++;
		    }
		  else
		    {
		      char **opt_addr  = opt->p_value;
		      *opt_addr = argv[i+1];
		      i++;
		    }
		}
	    }
	}
      else
	{
	  ccl_list_add (filenames, argv[i]);
	}
    }
}

			/* --------------- */

struct subtype_position 
{
  int pos;
  const struct subtype_position *prev;
};

			/* --------------- */

static void
s_generate_varnames_rec (armoise_variable *var, const armoise_type *type, 
			 char **varnames, int *pi, 
			 const struct subtype_position *ppos)
{
  if (armoise_type_is_scalar (type))
    {
      int i = *pi;

      varnames[i] = armoise_variable_get_name (var);
      varnames[i] = ccl_string_dup (varnames[i]);
      for(; ppos != NULL; ppos = ppos->prev)
	ccl_string_format_append (&(varnames[i]), "_%d", ppos->pos);
      *pi = i+1;
    }
  else
    {
      struct subtype_position pos;
      const int w = armoise_type_get_width (type);

      pos.prev = ppos;
      for (pos.pos = 0; pos.pos < w; pos.pos++)
	{
	  const armoise_type *st = armoise_type_get_subtype (type, pos.pos);
	  s_generate_varnames_rec (var, st, varnames, pi, &pos);
	}
    }
}

			/* --------------- */

static void
s_generate_varnames (armoise_variable *var, char **varnames, int *pi)
{
  armoise_type *type = armoise_variable_get_type (var);
  s_generate_varnames_rec (var, type, varnames, pi, NULL);
  armoise_type_del_reference (type);
}

			/* --------------- */

static void
s_compile_predicate_tuple (ccl_tree *t)
{
  if (ccl_tree_is_leaf (t))
    {
      armoise_predicate *P = t->object;
    
      if (types_opt)
	{
	  armoise_type *type = armoise_predicate_get_type (P);
	  armoise_type_log (CCL_LOG_DISPLAY, type);
	  ccl_display ("\n");
	  armoise_type_del_reference (type);
	}

      if (domains_opt)
	{
	  armoise_domain *dom = armoise_predicate_get_domain (P);
	  armoise_domain_log (CCL_LOG_DISPLAY, dom);
	  ccl_display ("\n");
	  armoise_domain_del_reference (dom);
	}

      if (solver_type_opt)
	{
	  const char *solvertype = "R";
	  armoise_domain_attr attr = armoise_predicate_get_largest_domain (P);

	  if (attr == ARMOISE_DOMAIN_ATTR_FOR_NATURALS)
	    solvertype = "N";
	  else if (attr == ARMOISE_DOMAIN_ATTR_FOR_INTEGERS)
	    solvertype = "Z";
	  else if (attr == ARMOISE_DOMAIN_ATTR_FOR_POSITIVES)
	    solvertype = "P";
 	  ccl_display ("%s\n", solvertype); 
	}
    }
  else
    {
      ccl_tree *c;

      for (c = t->childs; c; c = c->next)
	s_compile_predicate_tuple (c);
    }
}

			/* --------------- */

static int
s_compile_tree (const char *filename, armoise_tree *T)
{   
  int result = 1;
  armoise_context *ctx = armoise_context_create_for_predicates (NULL);
  armoise_context *nctx = armoise_context_create_for_predicates (NULL);

  ccl_try (armoise_interp_exception)
    {
      while (T != NULL)
	{
	  if (T->node_type == ARMOISE_TREE_PREDICATE)
	    {
	      ccl_tree *t = armoise_interp_predicate (ctx, T);
	      s_compile_predicate_tuple (t);
	      ccl_zdelete (ccl_tree_del_reference, t);
	    }
	  else if (T->node_type == ARMOISE_TREE_DEFINITION)
	    {
	      ccl_pair *p;
	      ccl_list *ids = ccl_list_create ();

	      ccl_try (armoise_interp_exception)
	        {
		  armoise_interp_definition (ctx, T, ids);
		} 
	      ccl_catch
		{
		  ccl_list_delete (ids);
		  ccl_rethrow ();
		}
	      ccl_end_try;

	      for (p = FIRST (ids); p; p = CDR (p))
		{
		  ccl_ustring id = CAR (p);
		  ccl_tree *t = armoise_context_get_predicate_tree (ctx, id);

		  armoise_context_add_predicate_tree (nctx, id, t);
		  ccl_tree_del_reference (t);
		}
	      ccl_list_delete (ids);
	    }
	  else 
	    ccl_throw (internal_error, "invalid node type");

	  T = T->next;
	}
    }
  ccl_catch
    {
      armoise_context_del_reference (ctx);
      armoise_context_del_reference (nctx);
      ccl_rethrow ();
    }
  ccl_end_try;

  armoise_context_del_reference (ctx);
  armoise_context_del_reference (nctx);

  return result;
}

			/* --------------- */

static int
s_compile_stream (FILE *input, const char *filename)
{
  int result = 0;
  armoise_tree *T = armoise_read_stream (input, filename);

  if (T != NULL)
    {
      result = s_compile_tree (filename, T);
      ccl_parse_tree_delete_tree (T);
    }

  return result;
}

			/* --------------- */

static int
s_compile_file (const char *filename)
{
  int result = 0;
  FILE *f = fopen (filename, "r");

  if (f == NULL)
    ccl_error ("can't open file '%s'.\n", filename);
  else
    {
      result = s_compile_stream (f, filename);
      fclose (f);
    }

  return result;
}

			/* --------------- */

static int
s_file_exists (const char *filename)
{
  FILE *f = fopen (filename, "r");

  if (f != NULL)
    fclose(f);

  return (f != NULL);
}

			/* --------------- */

