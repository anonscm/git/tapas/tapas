#!/bin/bash 

here=${PWD}
N_SOLVER="prestaf"
Z_SOLVER="lash"
P_SOLVER="no-P-solver-available"
R_SOLVER="no-R-solver-available"

function failure_if()
{
    if eval $1; then
	if test "x$2" != "x" ; then
	    echo $2 >> ${logfile}
	fi
	echo "have a look to `basename ${logfile}` file."
	exit 1
    fi
}

function runcmd() 
{
    eval "echo \"$1\"" >> ${logfile}
    eval "$1" 2>> ${logfile}

    return $?
}

progname=`echo "$0" | sed 's,^.*/,,'`
testname=`echo "${progname}" | sed 's,[.]test,,'` 
logfile="${here}/${testname}.out"
testkinds="syntax domains"

testkind="unknown"
for k in ${testkinds}; do
    if `echo ${testname} | grep -q -e "^$k-.*"`; then
	testkind="$k"
	testname=`echo "${testname}" | sed "s/$k-//g"`
    fi
done

if test "${testkind}" = "unknown"; then
    echo "don't known how to use this file '${progname}'." 
    echo "don't known how to use this file '${progname}'." 1>&2 > ${logfile}
    exit 1
fi

predicate_file="${testname}.arm"
expected_error="${testkind}-${testname}.err"
cd ${srcdir}

if test "${testkind}" = "syntax" ; then
    echo "checking ARMOISE syntax interpreter against ${predicate_file} " >> ${logfile}
    echo "${here}/armoise-tester ${predicate_file}" >> ${logfile}
    error="${here}/${testkind}-${testname}.$$.err"
    output=`${here}/armoise-tester ${predicate_file} 2> ${error}`

    if test "x${output}" != "x" ; then
	rm -f "${error}"
	echo "unexpected output of ${here}/armoise-tester :" >> ${logfile}
	echo "${output}" >> ${logfile}
	echo "have a look to `basename ${logfile}` file."
	exit 1
    fi

    if cmp -s ${expected_error} ${error}; then
	:
    else
	echo "error output differences:" >> ${logfile}
	echo "diff ${expected_error} ${error}" >> ${logfile}
	diff ${expected_error} ${error} 2>> ${logfile} 1>&2
	rm -f ${error}
	echo "have a look to `basename ${logfile}` file."
	exit 1
    fi

    rm -f ${error}
fi

if test "${testkind}" = "domains"; then
    echo "checking computed domains of variables and solver type against ${predicate_file}" >> ${logfile}
    cmd="${here}/armoise-tester -vx ${predicate_file}"
    expected_result="${testkind}-${testname}.doms"
    result="${here}/${testkind}-${testname}.$$.doms"
    runcmd "$cmd" > ${result}
    if test "$?" != "0"; then
	rm -f ${result}
	exit 1;
    fi

    if cmp -s ${expected_result} ${result}; then
	:
    else
	echo "output differences:" >> ${logfile}
	echo "diff ${expected_result} ${result}" >> ${logfile}
	diff ${expected_result} ${result} 2>> ${logfile} 1>&2
	rm -f ${result}
	echo "have a look to `basename ${logfile}` file."
	exit 1
    fi

    rm -f ${result}
fi

rm -f ${logfile}
exit 0
