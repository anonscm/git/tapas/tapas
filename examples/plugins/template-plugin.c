/*
 * template-plugin.c -- Functions to implement to build a valid GENEPI plugin.
 * 
 * This file is a part of the GENEric Presburger programming Interface. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!\example examples/plugins/template-plugin.c
 * \brief This file is a template of a GENEPI plugin. 
 */

#include <stdio.h>
#include <stdlib.h>

#ifndef GENEPI_PLUGIN_PREFIX      
/* replace the 'template' prefix by the one of the module name */
# warning "GENEPI_PLUGIN_PREFIX is not defined"
# define GENEPI_PLUGIN_PREFIX(_f) template ## _f 
#endif /* ! GENEPI_PLUGIN_NAME */

#include <genepi/genepi-plugin.h>


#define ENABLE_ALL_PROTOS 1

struct genepi_plugin_impl_st
{
  /* fill it with your own fields */
  genepi_plugconf *conf; 
};

struct genepi_set_impl_st
{
  /* fill it with your own fields */
  int refcount;
};

			/* --------------- */

genepi_plugin_impl *
GENEPI_PLUGIN_FUNC(init) (genepi_plugconf *conf)
{
  genepi_plugin_impl *result = malloc (sizeof *result);

  result->conf = genepi_plugconf_add_reference (conf);

  return result;
}

			/* --------------- */


void
GENEPI_PLUGIN_FUNC(terminate) (genepi_plugin_impl *plugin)
{
  genepi_plugconf_del_reference (plugin->conf);
  free (plugin);
}


			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC(autotest) (genepi_plugin_impl *plugin)
{
  return 1;
}
#endif
			/* --------------- */


genepi_solver_type
GENEPI_PLUGIN_FUNC(get_solver_type) (genepi_plugin_impl *plugin)
{
  return GENEPI_N_SOLVER;
}


			/* --------------- */

#if ENABLE_ALL_PROTOS
genepi_set_impl *
GENEPI_PLUGIN_FUNC(add_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X)
{
  X->refcount++;

  return X;
}
#endif
			/* --------------- */

#if ENABLE_ALL_PROTOS
void
GENEPI_PLUGIN_FUNC(del_reference) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X)
{
  X->refcount--;
  if (X->refcount == 0)
    {
      free (X);
    }
}
#endif 
			/* --------------- */

#if ENABLE_ALL_PROTOS
genepi_set_impl *
GENEPI_PLUGIN_FUNC(top) (genepi_plugin_impl *plugin, int n)
{
  return NULL;
}
#endif
			/* --------------- */

#if ENABLE_ALL_PROTOS
genepi_set_impl *
GENEPI_PLUGIN_FUNC(top_N) (genepi_plugin_impl *plugin, int n)
{
  return NULL;
}
#endif
			/* --------------- */

#if ENABLE_ALL_PROTOS
genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_Z) (genepi_plugin_impl *plugin, int n)
{
  return NULL;
}
#endif 

			/* --------------- */

#if ENABLE_ALL_PROTOS
genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_P) (genepi_plugin_impl *plugin, int n)
{
return NULL;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
genepi_set_impl *
GENEPI_PLUGIN_FUNC (top_R) (genepi_plugin_impl *plugin, int n)
{
  return NULL;
}
#endif

#if ENABLE_ALL_PROTOS
genepi_set_impl *
GENEPI_PLUGIN_FUNC(bot) (genepi_plugin_impl *plugin, int n)
{
  return NULL;
}
#endif
			/* --------------- */

#if ENABLE_ALL_PROTOS
genepi_set_impl *
GENEPI_PLUGIN_FUNC(linear_operation) (genepi_plugin_impl *plugin, 
				      const int *alpha, int alpha_size, 
				      genepi_comparator op, int c)
{
  return NULL;
}
#endif
			/* --------------- */

#if ENABLE_ALL_PROTOS
genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_union) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
			       genepi_set_impl *X2)
{
  return NULL;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_intersection) (genepi_plugin_impl *plugin, 
				      genepi_set_impl *X1, genepi_set_impl *X2)
{
  return NULL;
}
#endif
			/* --------------- */

#if ENABLE_ALL_PROTOS
genepi_set_impl *
GENEPI_PLUGIN_FUNC(set_complement) (genepi_plugin_impl *plugin, 
				    genepi_set_impl *X)
{
  return NULL;
}
#endif
			/* --------------- */

#if ENABLE_ALL_PROTOS
genepi_set_impl *
GENEPI_PLUGIN_FUNC(project) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
			     const int *selection, int size)
{
  return NULL;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
genepi_set_impl *
GENEPI_PLUGIN_FUNC(invproject) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
				const int *selection,  int size)
{
  return NULL;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
genepi_set_impl *
GENEPI_PLUGIN_FUNC(apply) (genepi_plugin_impl *plugin, genepi_set_impl *R, 
			   genepi_set_impl *A)
{
  return NULL;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
genepi_set_impl *
GENEPI_PLUGIN_FUNC(applyinv) (genepi_plugin_impl *plugin, genepi_set_impl *R, 
			      genepi_set_impl *A)
{
  return NULL;
}
#endif
			/* --------------- */

#if ENABLE_ALL_PROTOS
int 
GENEPI_PLUGIN_FUNC(compare) (genepi_plugin_impl *plugin, genepi_set_impl *X1, 
			     genepi_comparator op, genepi_set_impl *X2)
{
  return 0;
}
#endif
			/* --------------- */

#if ENABLE_ALL_PROTOS
int 
GENEPI_PLUGIN_FUNC(is_empty) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return 0;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int 
GENEPI_PLUGIN_FUNC(is_full) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return 0;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int 
GENEPI_PLUGIN_FUNC(is_finite) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return 0;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC(depend_on) (genepi_plugin_impl *plugin, genepi_set_impl *X, 
			       const int *sel, int selsize)
{
  return 0;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
void 
GENEPI_PLUGIN_FUNC(get_solutions) (genepi_plugin_impl *plugin, 
				   genepi_set_impl *X, int ***psolutions, 
				   int *psize, int max)
{
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
void 
GENEPI_PLUGIN_FUNC(display_all_solutions) (genepi_plugin_impl *plugin, 
					   genepi_set_impl *X, FILE *output,
					   const char * const *varname)
{
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
void 
GENEPI_PLUGIN_FUNC(display_data_structure) (genepi_plugin_impl *plugin, 
					    genepi_set_impl *X, FILE *output)
{
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int 
GENEPI_PLUGIN_FUNC(get_width) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return 0;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int 
GENEPI_PLUGIN_FUNC(get_data_structure_size) (genepi_plugin_impl *plugin, 
					     genepi_set_impl *X)
{
  return 0;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC(is_solution) (genepi_plugin_impl *plugin, 
				 genepi_set_impl *X, const int *x, int x_size,
				 int xden)
{
  return 0;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC(get_one_solution) (genepi_plugin_impl *plugin, 
				      genepi_set_impl *X, int *x, int x_size)
{  
  return 0;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
unsigned int
GENEPI_PLUGIN_FUNC(cache_hash) (genepi_plugin_impl *plugin, genepi_set_impl *X)
{
  return 0;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC(cache_equal) (genepi_plugin_impl *plugin, 
				 genepi_set_impl *X1, genepi_set_impl *X2)
{
  return 0;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC (write_set) (genepi_plugin_impl *plugin, FILE *output, 
				genepi_set_impl *set)
{
  return GENEPI_RW_ERR_IO;
}
#endif

			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC (read_set) (genepi_plugin_impl *plugin, FILE *input, 
			       genepi_set_impl **pset)
{
  return GENEPI_RW_ERR_IO;
}
#endif 

			/* --------------- */

#if ENABLE_ALL_PROTOS
int
GENEPI_PLUGIN_FUNC (preorder) (genepi_plugin_impl *plugin, genepi_set_impl *X1,
			       genepi_set_impl *X2)
{
  return 0;
}
#endif 
