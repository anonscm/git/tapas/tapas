#! /bin/sh

BENCHMARKS="benchmarks.lst"
SOLVERS=`distiller -q --solvers`
BENCHFILES=`cat ${BENCHMARKS}` 

function emptyness_results {
    echo -n "input"
    for S in ${SOLVERS}; do
	echo -n "; ${S}"
    done
    
    echo

    for B in ${BENCHFILES}; do
	prefix=`basename ${B} .arm`
	
	echo -n "${prefix}"
	for S in ${SOLVERS}; do
	    result="${prefix}-${S}.result"
	    echo -n ";"
	    
	    if ! test -f ${result}; then
		continue
	    fi
	    errors="${result}.err"
	    time="${result}.time"
	    if test "x`cat ${result}`" != "xnot empty set" -o -s ${errors} ; 
	    then
		echo -n "ERR"
	    else
		echo -n `grep "user" ${time} | sed 's/user //g'`
	    fi
	done
	echo
    done
}

function batof_results {
    echo "input; formula size in nodes; formula size in chars; automata size; total time; synthesis time"

    for B in ${BENCHFILES}; do
	prefix=`basename ${B} .arm`
	result="batof-${prefix}.result"
	errors="${result}.err"
	time="${result}.time"
	
	# input
	echo -n "${prefix};"
	
	if test \( ! -f ${result} \) -o -s ${errors}; then
	    echo "ERR"
	    continue
	fi

	# formula size in nodes
	FSIN=`grep -e "// Formula size *:" ${result} | sed -e 's,//.*: \([0-9][0-9]*\) nodes,\1,g'`

	# formula size in chars
	FSIC=`wc -c ${result} | awk '{ print $1 }'`

	# automata size
	AS=`grep -e "// Automata size *:" ${result} | sed -e 's,//.*#S=\([0-9][0-9]*\) .*,\1,g'`
	# total time
	TT=`grep "user" ${time} | sed 's/user //g'`
	# synthesis time
	ST=`grep -e "// Time to synthesis formula *:" ${result} | sed -e 's,//.*: \([0-9][0-9]*\) ms,\1,g' | awk '{ print $1/1000 }'`
	echo "${FSIN};${FSIC};${AS};${TT};${ST}"
    done
}

emptyness_results > emptyness-results.csv
batof_results > batof-results.csv


exit 0
