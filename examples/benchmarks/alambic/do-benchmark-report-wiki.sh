#! /bin/sh

BENCHMARKS="benchmarks.lst"
IGNORED_SOLVERS="\\(genz\\)\\|\\(ppl\\)"
SOLVERS=`distiller -q --solvers | sed "s/${IGNORED_SOLVERS}//g"` 

SOLVERS=`(for s in ${SOLVERS}; do echo $s; done) | sort`
BENCHFILES=`cat ${BENCHMARKS}` 
BENCH_URL='http://altarica.labri.fr/CVSweb/cvsweb.cgi/libraries/alambic/benchmark/@BENCH@.arm?rev=HEAD'

function bench_url {
    bench="$1"
    echo ${BENCH_URL} | sed -e "s/@BENCH@/${bench}/g"
}

function emptyness_results {
    echo -n "^  File"
    for S in ${SOLVERS}; do
	echo -n "  ^  ${S}"
    done
    
    echo "  ^"

    for B in ${BENCHFILES}; do
	prefix=`basename ${B} .arm`
	
	echo -n "^  [[`bench_url ${prefix}`|${prefix}]]"
	for S in ${SOLVERS}; do
	    result="results/${prefix}-${S}.result"
	    echo -n "  |  "
	    
	    if ! test -f ${result}; then
		continue
	    fi
	    errors="${result}.err"
	    time="${result}.time"
	    if test "x`cat ${result}`" != "xnot empty set" -o -s ${errors} ; 
	    then
		echo -n "ERR"
	    else
		echo -n `grep "user" ${time} | sed 's/user //g'`
	    fi
	done
	echo "  |"
    done
}

function batof_results {
    echo "^  File  ^  <html>formula size<br> in nodes</html>  ^  <html>formula size<br> in chars</html>  ^  <html>automata<br> size</html>  ^  <html>total<br> time</html>  ^  <html>synthesis<br> time</html>  ^"

    for B in ${BENCHFILES}; do
	prefix=`basename ${B} .arm`
	result="results/batof-${prefix}.result"
	errors="${result}.err"
	time="${result}.time"
	
	# input
	echo -n "^  [[`bench_url ${prefix}`|${prefix}]]  |  "
	
	if test \( ! -f ${result} \) -o -s ${errors}; then
	    echo "ERR  |||||"
	    continue
	fi

	# formula size in nodes
	FSIN=`grep -e "// Formula size *:" ${result} | sed -e 's,//.*: \([0-9][0-9]*\) nodes,\1,g'`

	# formula size in chars
	FSIC=`wc -c ${result} | awk '{ print $1 }'`

	# automata size
	AS=`grep -e "// Automata size *:" ${result} | sed -e 's,//.*#S=\([0-9][0-9]*\) .*,\1,g'`
	# total time
	TT=`grep "user" ${time} | sed 's/user //g'`
	# synthesis time
	ST=`grep -e "// Time to synthesis formula *:" ${result} | sed -e 's,//.*: \([0-9][0-9]*\) ms,\1,g' | awk '{ print $1/1000 }'`
	echo "${FSIN}  |  ${FSIC}  |  ${AS}  |  ${TT}  |  ${ST}  |"
    done
}

emptyness_results > emptyness-results.dokuwiki
batof_results > batof-results.dokuwiki


exit 0
