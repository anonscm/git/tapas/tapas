#!/bin/sh 

MEMORY_USAGE_LIMIT=`expr 9 \* 512 \* 1024`
TIMEOUT_IN_MINUTES=5
TIMEOUT=`expr ${TIMEOUT_IN_MINUTES} '*' 60`
BENCHMARKS="benchmarks.lst"
SOLVERS=`distiller -q --solvers`
BENCHFILES=`cat ${BENCHMARKS}` 

cat > GNUmakefile <<EOF
##
## This makefile has been generated automatically by $0
## 
BENCHFILES=`echo ${BENCHFILES}`

EOF

(echo "RESULTFILES= \\"
    for B in ${BENCHFILES}; do
	bench=`basename ${B} .arm`
	for S in ${SOLVERS}; do
	    res="${bench}-${S}.result"
	    echo "  $res \\"
	done
	res="batof-${bench}.result"
	echo "  $res \\"
    done
    echo "  ${nop}") >> GNUmakefile

cat >> GNUmakefile <<EOF
all : emptyness-results.csv batof-results.csv 

emptyness-results.csv batof-results.csv : \${RESULTFILES}
	./do-benchmark-report.sh

clean :
	rm -f \${RESULTFILES}
	rm -f \${RESULTFILES:%=%.err}
	rm -f \${RESULTFILES:%=%.time}

GNUmakefile : ${BENCHMARKS} do-benchmark-makefile.sh
	@ echo "Regenerating GNUmakefile"
	@ rm -f GNUmakefile
	@ ./do-benchmark-makefile.sh

EOF
    
for S in ${SOLVERS}; do
    cat >> GNUmakefile <<EOF
bench%-${S}.result : bench%.arm GNUmakefile
	@ echo "\$@"
	@ (ulimit  -v ${MEMORY_USAGE_LIMIT} -t ${TIMEOUT} ; time -p distiller -e --plugin="${S}" \$< > \$@ 2> \$@.err ) 2> \$@.time  || true

EOF
done

cat >> GNUmakefile <<EOF
batof-bench%.result : bench%.arm GNUmakefile
	@ echo "\$@"
	@ (ulimit  -v ${MEMORY_USAGE_LIMIT} -t ${TIMEOUT} ; time -p distiller -s --plugin=prestaf \$< > \$@ 2> \$@.err ) 2> \$@.time  || true

EOF

exit 0
